<map version="freeplane 1.12.1">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="2024" OBJECT="java.lang.Long|2024" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1740088252570" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="1.1">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_tags="UNDER_NODES" associatedTemplateLocation="template:/standard-1.6.mm" show_note_icons="true" fit_to_viewport="false" show_icons="BESIDE_NODES"/>
    <tags category_separator="::"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent TYPE="DETAILS" CONTENT-TYPE="plain/auto"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.tags">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.flower" COLOR="#ffffff" BACKGROUND_COLOR="#255aba" STYLE="oval" TEXT_ALIGN="CENTER" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="22 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#f9d71c" BORDER_DASH_LIKE_EDGE="false" BORDER_DASH="CLOSE_DOTS" MAX_WIDTH="6 cm" MIN_WIDTH="3 cm"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="2" RULE="ON_BRANCH_CREATION"/>
<node TEXT="SimpleTree" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="top_or_left" ID="ID_970508490" CREATED="1729201350436" MODIFIED="1729454284327">
<edge COLOR="#00ffff"/>
<node TEXT="struct" ID="ID_1113619964" CREATED="1729201366564" MODIFIED="1729201370238">
<node TEXT="Node" ID="ID_766119424" CREATED="1729201370253" MODIFIED="1729292013060">
<node TEXT="elements" ID="ID_430805778" CREATED="1729454964922" MODIFIED="1729454968377">
<node TEXT="index" ID="ID_1011756932" CREATED="1729454970039" MODIFIED="1729454974360"/>
</node>
<node TEXT="impl" ID="ID_678855914" CREATED="1729454468783" MODIFIED="1729454472141">
<node TEXT="root" ID="ID_1768037585" CREATED="1729454473850" MODIFIED="1729454589183">
<node TEXT="no parent" ID="ID_1273638640" CREATED="1729454595999" MODIFIED="1729454620958"/>
<node TEXT="children empty" ID="ID_868853191" CREATED="1729454627975" MODIFIED="1729454637327"/>
</node>
</node>
</node>
<node TEXT="Root" ID="ID_898745973" CREATED="1729454674750" MODIFIED="1729454683961">
<node TEXT="elements" ID="ID_1638507087" CREATED="1729454691053" MODIFIED="1729454695387">
<node TEXT="nodes" ID="ID_233309788" CREATED="1729454696498" MODIFIED="1729454704762">
<node TEXT="root node is always index 0" ID="ID_1058949630" CREATED="1729454712852" MODIFIED="1729454725908"/>
</node>
<node TEXT="internal struct" ID="ID_1707011818" CREATED="1729454878666" MODIFIED="1729454898038">
<node TEXT="Node" ID="ID_1621237650" CREATED="1729454898693" MODIFIED="1729454901629">
<node TEXT="elements" POSITION="top_or_left" ID="ID_1500854462" CREATED="1729454442426" MODIFIED="1729454449485">
<node TEXT="parent" POSITION="top_or_left" ID="ID_1317874154" CREATED="1729292013070" MODIFIED="1729292020191"/>
<node TEXT="children" POSITION="top_or_left" ID="ID_730401046" CREATED="1729292031066" MODIFIED="1729292033945"/>
<node TEXT="value" ID="ID_595685095" CREATED="1729292023859" MODIFIED="1729292028106"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Day x" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_1503466741" CREATED="1680804513994" MODIFIED="1680884624652">
<edge COLOR="#00ffff"/>
<node TEXT="Story" ID="ID_420725208" CREATED="1680804624799" MODIFIED="1680804631466"/>
<node TEXT="a" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1699405723" CREATED="1680804555104" MODIFIED="1680804558605">
<node TEXT="what to do" ID="ID_28601554" CREATED="1680806818168" MODIFIED="1680884609651"/>
<node TEXT="solve" ID="ID_797681717" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_692835062" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_696334603" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_1456875690" CREATED="1680811766973" MODIFIED="1680884573457"/>
<node TEXT="structs" ID="ID_274203939" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="(name)" ID="ID_364922106" CREATED="1680895369243" MODIFIED="1680895380613">
<node TEXT="elements" ID="ID_8687586" CREATED="1680812286923" MODIFIED="1680884592900"/>
<node TEXT="impl" ID="ID_470331961" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_801410678" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_810305720" CREATED="1680813884793" MODIFIED="1680813889062"/>
<node TEXT="task" ID="ID_1662850708" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
</node>
<node TEXT="Day 9" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="bottom_or_right" ID="ID_1133610879" CREATED="1680804513994" MODIFIED="1738447327102">
<edge COLOR="#00ffff"/>
<node TEXT="Story" ID="ID_1503324142" CREATED="1680804624799" MODIFIED="1680804631466"/>
<node TEXT="b" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_133013656" CREATED="1680804555104" MODIFIED="1738501700933">
<node TEXT="what to do" ID="ID_1823912477" CREATED="1680806818168" MODIFIED="1680884609651">
<node TEXT="disk map" ID="ID_480484267" CREATED="1738501727146" MODIFIED="1738501765931">
<node TEXT="file size" ID="ID_987619997" CREATED="1738501731888" MODIFIED="1738501749921"/>
<node TEXT="free size" ID="ID_1172428637" CREATED="1738501750519" MODIFIED="1738501754392"/>
</node>
<node TEXT="disk blocks" ID="ID_225390384" CREATED="1738501770240" MODIFIED="1738501779732">
<node TEXT="disk id * file size" ID="ID_848824999" CREATED="1738501779737" MODIFIED="1738501798033"/>
<node TEXT="free * free size" ID="ID_218062470" CREATED="1738501802501" MODIFIED="1738501814810"/>
</node>
<node TEXT="keep track" ID="ID_1532793015" CREATED="1738501826059" MODIFIED="1738501831707">
<node TEXT="file" ID="ID_1559905873" CREATED="1738501856937" MODIFIED="1738501859429">
<node TEXT="id?" ID="ID_459604384" CREATED="1738501911499" MODIFIED="1738501913637"/>
<node TEXT="start" ID="ID_1099592304" CREATED="1738501859433" MODIFIED="1738501863068"/>
<node TEXT="size" ID="ID_307848158" CREATED="1738501863498" MODIFIED="1738501866381"/>
</node>
<node TEXT="free" ID="ID_869801486" CREATED="1738501831712" MODIFIED="1738501848534">
<node TEXT="start" ID="ID_314785114" CREATED="1738501848539" MODIFIED="1738501852008"/>
<node TEXT="size" ID="ID_574661500" CREATED="1738501852661" MODIFIED="1738501854479"/>
</node>
</node>
<node TEXT="loop files from right" ID="ID_1784778243" CREATED="1738501995770" MODIFIED="1738502024478">
<node TEXT="if file.start &lt; free[0].start" ID="ID_288231603" CREATED="1738502024482" MODIFIED="1738502056757">
<node TEXT="done" ID="ID_1501307291" CREATED="1738502056761" MODIFIED="1738502061941"/>
</node>
<node TEXT="loop free from left" ID="ID_1832527639" CREATED="1738502067368" MODIFIED="1738502096604">
<node TEXT="if free.size &gt;= file.size" ID="ID_1200750275" CREATED="1738502096608" MODIFIED="1738502118725">
<node TEXT="move disk free pos" ID="ID_434213945" CREATED="1738502118730" MODIFIED="1738502174021">
<node TEXT="update disk blocks" ID="ID_1187220195" CREATED="1738502174026" MODIFIED="1738502179877"/>
<node TEXT="update free.size and .start" ID="ID_1874107092" CREATED="1738502183048" MODIFIED="1738502227948"/>
<node TEXT="if free.size == 0" ID="ID_819968730" CREATED="1738502229951" MODIFIED="1738502241298">
<node TEXT="remove free block" ID="ID_343576639" CREATED="1738502241303" MODIFIED="1738502250067"/>
</node>
<node TEXT="next right" ID="ID_281733546" CREATED="1738517998260" MODIFIED="1738518006417"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="solve" ID="ID_1308231724" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_1044956041" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_844074569" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_617981175" CREATED="1680811766973" MODIFIED="1680884573457"/>
<node TEXT="structs" ID="ID_524199510" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="(name)" ID="ID_1631312673" CREATED="1680895369243" MODIFIED="1680895380613">
<node TEXT="elements" ID="ID_852038764" CREATED="1680812286923" MODIFIED="1680884592900"/>
<node TEXT="impl" ID="ID_1805944487" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_325404727" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_743606887" CREATED="1680813884793" MODIFIED="1680813889062"/>
<node TEXT="task" ID="ID_791781877" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
</node>
<node TEXT="Day 10" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="bottom_or_right" ID="ID_1842813865" CREATED="1680804513994" MODIFIED="1738616997397">
<edge COLOR="#00ffff"/>
<node TEXT="Story" ID="ID_361626726" CREATED="1680804624799" MODIFIED="1680804631466">
<node TEXT="find trailheads" POSITION="bottom_or_right" ID="ID_1271230828" CREATED="1738617022704" MODIFIED="1738617037537">
<node TEXT="paths from 0-9 with 1 level up for each step" ID="ID_1604968555" CREATED="1738617037542" MODIFIED="1738617062569"/>
<node TEXT="count score for each TH" ID="ID_638099043" CREATED="1738617118194" MODIFIED="1738617129195">
<node TEXT="how many different 9 can be reached from one 0?" ID="ID_248381219" CREATED="1738617129200" MODIFIED="1738617162280"/>
</node>
</node>
</node>
<node TEXT="a" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_712101197" CREATED="1680804555104" MODIFIED="1680804558605">
<node TEXT="what to do" ID="ID_710665009" CREATED="1680806818168" MODIFIED="1680884609651">
<node TEXT="scan the map for 0" ID="ID_1464433395" CREATED="1738617185473" MODIFIED="1738617201000"/>
<node TEXT="for each 0" ID="ID_1007822792" CREATED="1738617203438" MODIFIED="1738617214802">
<node TEXT="run bfs to find 9" ID="ID_1408663201" CREATED="1738617214805" MODIFIED="1738617232219">
<node TEXT="limit: each step must go 1 level up" ID="ID_1607611162" CREATED="1738617232223" MODIFIED="1738617248687"/>
<node TEXT="paths are not counted, it&apos;s the 9&apos;s" ID="ID_387927401" CREATED="1738617256143" MODIFIED="1738617289001"/>
</node>
</node>
</node>
<node TEXT="solve" ID="ID_597748979" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_686514441" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_1065311218" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_483798473" CREATED="1680811766973" MODIFIED="1680884573457"/>
<node TEXT="structs" ID="ID_497040747" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="(name)" ID="ID_14657466" CREATED="1680895369243" MODIFIED="1680895380613">
<node TEXT="elements" ID="ID_1369923371" CREATED="1680812286923" MODIFIED="1680884592900"/>
<node TEXT="impl" ID="ID_1820428634" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_1059782191" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_915029001" CREATED="1680813884793" MODIFIED="1680813889062"/>
<node TEXT="task" ID="ID_1789961081" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
</node>
<node TEXT="Day 11" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="bottom_or_right" ID="ID_374421953" CREATED="1680804513994" MODIFIED="1739038384935">
<edge COLOR="#00ffff"/>
<node TEXT="Story" ID="ID_429690102" CREATED="1680804624799" MODIFIED="1680804631466"/>
<node TEXT="b" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1083237990" CREATED="1680804555104" MODIFIED="1739038390196">
<node TEXT="cache" ID="ID_751895031" CREATED="1739038409782" MODIFIED="1739038420380">
<node TEXT="HashMap&lt;number, HashMap&lt;depth, stones&gt;&gt;" ID="ID_137480926" CREATED="1739038420381" MODIFIED="1739038631261"/>
</node>
<node TEXT="what to do" ID="ID_1703767971" CREATED="1680806818168" MODIFIED="1680884609651"/>
<node TEXT="solve" ID="ID_798608174" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_1724435663" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_1733896587" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_1330709918" CREATED="1680811766973" MODIFIED="1680884573457"/>
<node TEXT="structs" ID="ID_1132530580" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="mem" ID="ID_1015106783" CREATED="1680895369243" MODIFIED="1739038554677">
<node TEXT="elements" ID="ID_1459963008" CREATED="1680812286923" MODIFIED="1680884592900">
<node TEXT="(number)" ID="ID_401132756" CREATED="1739038446936" MODIFIED="1739038452042"/>
<node TEXT="depth" ID="ID_1743852110" CREATED="1739038456707" MODIFIED="1739038485952"/>
<node TEXT="stones" ID="ID_523953448" CREATED="1739038487002" MODIFIED="1739038496149"/>
</node>
<node TEXT="impl" ID="ID_838808471" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_1785916830" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_1669460998" CREATED="1680813884793" MODIFIED="1680813889062"/>
<node TEXT="task" ID="ID_788369532" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
</node>
<node TEXT="Day 13" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="bottom_or_right" ID="ID_1812491145" CREATED="1680804513994" MODIFIED="1739461349526">
<edge COLOR="#00ffff"/>
<node TEXT="Story" ID="ID_676546927" CREATED="1680804624799" MODIFIED="1680804631466">
<node TEXT="knappar som flyttar en klo i x- och y-led" ID="ID_434631215" CREATED="1739461750877" MODIFIED="1739461773004"/>
<node TEXT="A kostar 3, B kostar 1" ID="ID_1642326276" CREATED="1739461778015" MODIFIED="1739461795812"/>
<node TEXT="hur många tryck på A o B för att komma till priset (om det går)?" ID="ID_1389257029" CREATED="1739461804526" MODIFIED="1739461824697"/>
</node>
<node TEXT="a" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1938708076" CREATED="1680804555104" MODIFIED="1680804558605">
<node TEXT="what to do" ID="ID_326769378" CREATED="1680806818168" MODIFIED="1680884609651">
<node TEXT="formel" ID="ID_1307184204" CREATED="1739461866559" MODIFIED="1739461871325">
<node TEXT="prize.x = a.x * n + b.x * m&#xa;prize.y = a.y * n + b.y * m" ID="ID_34781001" CREATED="1739461871329" MODIFIED="1739462149408"/>
<node TEXT="prize.x + prize.y = a.x * n + b.x * m + a.y * n + b.y * m" ID="ID_807652074" CREATED="1739464541201" MODIFIED="1739464594743">
<node TEXT="prize.x + prize.y = (a.x + a.y) * n + (b.x + b.y) * m" ID="ID_905182217" CREATED="1739464644114" MODIFIED="1739464691590"/>
</node>
<node TEXT="a⋅x+b⋅y=c&#xa;d⋅x+e⋅y=f" ID="ID_843049387" CREATED="1739482001058" MODIFIED="1739482072124">
<node TEXT="a⋅x+b⋅y-c = d⋅x+e⋅y-f" ID="ID_1703041391" CREATED="1739482614264" MODIFIED="1739482679925"/>
<node TEXT="a⋅x+b⋅y-d⋅x+e⋅y = c-f" ID="ID_298070005" CREATED="1739482689701" MODIFIED="1739482717727"/>
<node TEXT="(a⋅x+b⋅y)×e−(d⋅x+e⋅y)×b=c⋅e−f⋅b" ID="ID_129795512" CREATED="1739482096390" MODIFIED="1739482101908"/>
</node>
</node>
</node>
<node TEXT="solve" ID="ID_1966529591" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_1874639901" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_1180044999" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_1967395818" CREATED="1680811766973" MODIFIED="1680884573457"/>
<node TEXT="structs" ID="ID_1582059460" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="(name)" ID="ID_1445450128" CREATED="1680895369243" MODIFIED="1680895380613">
<node TEXT="elements" ID="ID_1272281959" CREATED="1680812286923" MODIFIED="1680884592900"/>
<node TEXT="impl" ID="ID_1032218785" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_1836781182" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_1550291963" CREATED="1680813884793" MODIFIED="1680813889062"/>
<node TEXT="task" ID="ID_1960839311" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
</node>
<node TEXT="Day 20" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="bottom_or_right" ID="ID_1287474718" CREATED="1680804513994" MODIFIED="1741038804064">
<edge COLOR="#00ffff"/>
<node TEXT="Story" ID="ID_697615213" CREATED="1680804624799" MODIFIED="1680804631466">
<node TEXT="find best path between S and E" ID="ID_1435379531" CREATED="1741038811229" MODIFIED="1741038831976"/>
<node TEXT="cheating is allowed" ID="ID_78889658" CREATED="1741038832858" MODIFIED="1741038852481">
<node TEXT="two steps to get through a wall" ID="ID_442294200" CREATED="1741038852492" MODIFIED="1741038883814"/>
</node>
<node TEXT="find all cheats that saves at least 100 steps" ID="ID_283454721" CREATED="1741038886490" MODIFIED="1741038905954"/>
</node>
<node TEXT="a" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_448388366" CREATED="1680804555104" MODIFIED="1680804558605">
<node TEXT="what to do" ID="ID_447201881" CREATED="1680806818168" MODIFIED="1680884609651">
<node TEXT="run a-star to find the path" ID="ID_556740277" CREATED="1741038910046" MODIFIED="1741038936072">
<node TEXT="use came_from to be able to create the path" ID="ID_804935074" CREATED="1741038936083" MODIFIED="1741038961676"/>
<node TEXT="(store how many steps is taken at each position)" ID="ID_462073891" CREATED="1741039120682" MODIFIED="1741040108241"/>
<node TEXT="also use a HashMap with position as key and # steps as value" ID="ID_580948763" CREATED="1741039151238" MODIFIED="1741040041891"/>
</node>
<node TEXT="analyze the path" ID="ID_93006537" CREATED="1741038970180" MODIFIED="1741038978422">
<node TEXT="find all positions where" ID="ID_1050698658" CREATED="1741038978430" MODIFIED="1741039012412">
<node TEXT="the position in the path is two steps away" ID="ID_833350006" CREATED="1741039012423" MODIFIED="1741039471838"/>
<node TEXT="# steps is larger than for the test position" ID="ID_1301208905" CREATED="1741039472846" MODIFIED="1741039524380"/>
<node TEXT="a wall is inbetween" ID="ID_1067785818" CREATED="1741039312192" MODIFIED="1741039332694"/>
</node>
</node>
<node TEXT="calc how many cheats that will save at least 100 steps" ID="ID_940447255" CREATED="1741039539296" MODIFIED="1741039578901"/>
</node>
<node TEXT="solve" ID="ID_1182239040" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_123270045" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_963860663" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_1380505097" CREATED="1680811766973" MODIFIED="1680884573457">
<node TEXT="Tile" ID="ID_1693648655" CREATED="1741039669694" MODIFIED="1741039673138">
<node TEXT="Path" ID="ID_64575013" CREATED="1741039673143" MODIFIED="1741039677157"/>
<node TEXT="Wall" ID="ID_1845463631" CREATED="1741039677686" MODIFIED="1741039680080"/>
</node>
</node>
<node TEXT="structs" ID="ID_1556782137" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="Map" ID="ID_1258479943" CREATED="1680895369243" MODIFIED="1741039624233">
<node TEXT="elements" ID="ID_1156036194" CREATED="1680812286923" MODIFIED="1680884592900">
<node TEXT="start" ID="ID_1252347734" CREATED="1741039626163" MODIFIED="1741039630269"/>
<node TEXT="end" ID="ID_832224060" CREATED="1741039631127" MODIFIED="1741039632305"/>
<node TEXT="tiles" ID="ID_1004497069" CREATED="1741039635681" MODIFIED="1741039661322"/>
</node>
<node TEXT="impl" ID="ID_281300891" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_732418135" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_1004969412" CREATED="1680813884793" MODIFIED="1680813889062">
<node TEXT="a-star" ID="ID_877043869" CREATED="1741039851170" MODIFIED="1741039854861">
<node TEXT="returns" ID="ID_1050729313" CREATED="1741039872057" MODIFIED="1741039878364">
<node TEXT="# steps" ID="ID_1181909405" CREATED="1741039878510" MODIFIED="1741039883047"/>
<node TEXT="path" ID="ID_330109639" CREATED="1741039883566" MODIFIED="1741039885236"/>
<node TEXT="hashmap" ID="ID_259124625" CREATED="1741040048356" MODIFIED="1741040171213"/>
</node>
</node>
<node TEXT="find_cheats" ID="ID_878868161" CREATED="1741040176894" MODIFIED="1741040184572">
<node TEXT="for each pos in path" ID="ID_1079396534" CREATED="1741040184577" MODIFIED="1741040201808">
<node TEXT="get the positions two steps away in each direction" ID="ID_468870879" CREATED="1741040202832" MODIFIED="1741040285669"/>
<node TEXT="for each pos_step" ID="ID_1774230341" CREATED="1741040286759" MODIFIED="1741040302426">
<node TEXT="is it on the path?" ID="ID_959277379" CREATED="1741040302432" MODIFIED="1741040318969"/>
<node TEXT="are # steps larger than for pos?" ID="ID_1156703288" CREATED="1741040344039" MODIFIED="1741040364600"/>
<node TEXT="is there a wall inbetween?" ID="ID_1816609166" CREATED="1741040368707" MODIFIED="1741040377806"/>
<node TEXT="calc saved steps" ID="ID_130975096" CREATED="1741040387846" MODIFIED="1741040401602"/>
</node>
</node>
</node>
</node>
<node TEXT="task" ID="ID_231588381" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
<node TEXT="b" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1468513429" CREATED="1741126560415" MODIFIED="1741126562719">
<node TEXT="story" ID="ID_1116051717" CREATED="1741126567114" MODIFIED="1741126626274">
<node TEXT="possible to cheat by using up to 20 pico seconds" ID="ID_340861957" CREATED="1741126575346" MODIFIED="1741126602247"/>
</node>
<node TEXT="what to do" POSITION="bottom_or_right" ID="ID_1190949951" CREATED="1680806818168" MODIFIED="1680884609651">
<node TEXT="create a local map with bool&apos;s" ID="ID_836802693" CREATED="1741127384321" MODIFIED="1741201201601" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_BOTTOM">
<node TEXT="use this map below" POSITION="bottom_or_right" ID="ID_1252773273" CREATED="1741127422766" MODIFIED="1741127429242"/>
</node>
<node TEXT="start point" ID="ID_816903408" CREATED="1741192630283" MODIFIED="1741192637848">
<node TEXT="create a matrix with tiles in path that are up to 20 manhattan steps away" ID="ID_1946361836" CREATED="1741192637853" MODIFIED="1741192731881"/>
<node TEXT="find cheats *" ID="ID_456052893" CREATED="1741192873610" MODIFIED="1741192927229"/>
</node>
<node TEXT="for each following step in path" ID="ID_321476186" CREATED="1741126804271" MODIFIED="1741192746543">
<node TEXT="modify the matrix to fit 20 manhattan steps away" ID="ID_302718129" CREATED="1741126703219" MODIFIED="1741192784780"/>
<node TEXT="find cheats *" ID="ID_1234870290" CREATED="1741192892038" MODIFIED="1741192931890"/>
</node>
<node TEXT="* find cheats" ID="ID_578939833" CREATED="1741192910380" MODIFIED="1741201011264" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_BOTTOM">
<node TEXT="for candidates" POSITION="bottom_or_right" ID="ID_1894730953" CREATED="1741126752099" MODIFIED="1741126766178">
<node TEXT="if candidate isn&apos;t already in saved results" ID="ID_1766069712" CREATED="1741192982124" MODIFIED="1741193067113">
<node TEXT="run find_best_path to the candidate" POSITION="bottom_or_right" ID="ID_917314409" CREATED="1741126766183" MODIFIED="1741127175693">
<node TEXT="allow walls only in this path" ID="ID_1399557512" CREATED="1741126833770" MODIFIED="1741126853795"/>
<node TEXT="when an open tile is found, check if it is in path, save result" ID="ID_710014300" CREATED="1741192254389" MODIFIED="1741193037600">
<node TEXT="keep track of start and end positions, thus registering same start &amp; end pos as one cheat, regardless of the path between them." POSITION="bottom_or_right" ID="ID_115568891" CREATED="1741126989014" MODIFIED="1741193161172"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="solve" POSITION="bottom_or_right" ID="ID_1300767311" CREATED="1680811003307" MODIFIED="1680811029068">
<node TEXT="data" ID="ID_661527808" CREATED="1680811047516" MODIFIED="1680811064813">
<node TEXT="types" ID="ID_1384483157" CREATED="1680812253857" MODIFIED="1680884570419"/>
<node TEXT="enums" ID="ID_1715184646" CREATED="1680811766973" MODIFIED="1680884573457"/>
<node TEXT="structs" ID="ID_237073763" CREATED="1680811242308" MODIFIED="1680895366527">
<node TEXT="(name)" ID="ID_1991188633" CREATED="1680895369243" MODIFIED="1680895380613">
<node TEXT="elements" ID="ID_1106276910" CREATED="1680812286923" MODIFIED="1680884592900"/>
<node TEXT="impl" ID="ID_432513199" CREATED="1680811736409" MODIFIED="1680811739269"/>
</node>
</node>
</node>
<node TEXT="algorithm" ID="ID_1474864887" CREATED="1680811065219" MODIFIED="1680811192362">
<node TEXT="functions" ID="ID_1167633090" CREATED="1680813884793" MODIFIED="1680813889062"/>
<node TEXT="task" ID="ID_454086426" CREATED="1680813910488" MODIFIED="1680813914073"/>
</node>
</node>
</node>
</node>
</node>
</map>

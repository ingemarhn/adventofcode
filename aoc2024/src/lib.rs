pub mod maze {
    use priority_queue::PriorityQueue;
    use std::cmp::Reverse;
    use std::collections::HashMap;

    type Position = (usize, usize);
    pub type Positions = Vec<Position>;

    #[derive(Clone, Debug, PartialEq)]
    pub enum Tile {
        Open,
        Wall,
    }

    #[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
    pub enum Direction {
        East,
        West,
        North,
        South,
    }

    #[derive(Clone, Debug)]
    pub struct Maze {
        start: Position,
        end: Position,
        height: usize,
        width: usize,
        tiles: Vec<Vec<Tile>>,
    }

    impl Maze {
        pub fn neighbors(&self, p: Position) -> Vec<(Position, Direction)> {
            self.neighbors_n_steps(p, 1, false)
        }

        pub fn neighbors_all(&self, p: Position) -> Vec<(Position, Direction)> {
            self.neighbors_n_steps(p, 1, true)
        }

        pub fn neighbors_steps(&self, p: Position, steps: usize) -> Vec<(Position, Direction)> {
            self.neighbors_n_steps(p, steps, false)
        }

        fn neighbors_n_steps(&self, p: Position, steps: usize, allow_walls: bool) -> Vec<(Position, Direction)> {
            let mut nbrs = Vec::new();
            let s1 = steps - 1;
            if p.0 > s1 && (allow_walls || self.tiles[p.0 - steps][p.1] != Tile::Wall) {
                nbrs.push(((p.0 - steps, p.1), Direction::North));
            }
            if p.0 < self.height - steps && (allow_walls || self.tiles[p.0 + steps][p.1] != Tile::Wall) {
                nbrs.push(((p.0 + steps, p.1), Direction::South));
            }
            if p.1 > s1 && (allow_walls || self.tiles[p.0][p.1 - steps] != Tile::Wall) {
                nbrs.push(((p.0, p.1 - steps), Direction::West));
            }
            if p.1 < self.width - steps && (allow_walls || self.tiles[p.0][p.1 + steps] != Tile::Wall) {
                nbrs.push(((p.0, p.1 + steps), Direction::East));
            }

            nbrs
        }

        pub fn get_start(&self) -> Position {
            self.start
        }

        pub fn set_start(&mut self, pos: Position) {
            self.start = pos;
        }

        pub fn get_end(&self) -> Position {
            self.end
        }

        pub fn set_end(&mut self, pos: Position) {
            self.end = pos;
        }

        pub fn get_height(&self) -> usize {
            self.height
        }

        pub fn get_width(&self) -> usize {
            self.width
        }

        pub fn is_open(&self, pos: Position) -> bool {
            self.tiles[pos.0][pos.1] == Tile::Open
        }

        pub fn parse_input(lines: &[String]) -> (Maze, MazePosDir) {
            let h = lines.len();
            let w = lines[0].len();
            let mut s = (0, 0);
            let mut e = (0, 0);
            let mut map = Vec::new();
            for (r, line) in lines.iter().enumerate() {
                map.push(
                    line
                        .chars()
                        .enumerate()
                        .map(|(c, ch)| {
                            match ch {
                                '.' => Tile::Open,
                                '#' => Tile::Wall,
                                'S' => {
                                    s = (r, c);
                                    Tile::Open
                                },
                                'E' => {
                                    e = (r, c);
                                    Tile::Open
                                }
                                _ => panic!("ch = {ch}")
                            }
                        })
                        .collect::<Vec<_>>()
                );
            }

            (
                Maze { start: s, end: e, height: h, width: w, tiles: map },
                MazePosDir { position: s, direction: Direction::East }
            )
        }

        pub fn find_lowest_cost<F>(&self, dir_pos: &mut MazePosDir, add_cost: F) -> usize
        where
            F: Fn(Direction, Direction) -> usize,
        {
            self.find_best_cost_n_path(dir_pos, add_cost, false).0
        }

        pub fn find_best_path<F>(&self, dir_pos: &mut MazePosDir, add_cost: F) -> Positions
        where
            F: Fn(Direction, Direction) -> usize,
        {
            self.find_best_cost_n_path(dir_pos, add_cost, true).1
        }

        fn find_best_cost_n_path<F>(&self, dir_pos: &mut MazePosDir, add_cost: F, ret_path: bool) -> (usize, Positions)
        where
            F: Fn(Direction, Direction) -> usize,
        {
            /*
            [Python]
            frontier = PriorityQueue()  (priority is estimated cost to the goal)
            frontier.put(start, 0)      (here the priority isn't interesting since there only is one element in the queue)
            came_from = dict()          (only needed if path shall be returned)
            cost_so_far = dict()
            came_from[start] = None
            cost_so_far[start] = 0

            while not frontier.empty():
                current = frontier.get()

                if current == goal:
                    break

                for next in graph.neighbors(current):
                    new_cost = cost_so_far[current] + graph.cost(current, next)
                    if next not in cost_so_far or new_cost < cost_so_far[next]:
                        cost_so_far[next] = new_cost
                        priority = new_cost + heuristic(goal, next)
                        frontier.put(next, priority)
                        came_from[next] = current
            */
            // A*, A-star, a_star

            let mut queue = PriorityQueue::new();
            queue.push((self.get_start(), dir_pos.get_dir()), Reverse(0));
            let mut cost_so_far = HashMap::new();
            cost_so_far.insert(self.get_start(), 0);
            let mut came_from = HashMap::from([(self.get_start(), None)]);
            let mut path = Vec::new();

            while let Some(((curr_pos, dir), _pr)) = queue.pop() {
                let cost = cost_so_far[&curr_pos];
                if curr_pos == self.get_end() {
                    if ret_path {
                        path.push(curr_pos);
                        let mut pos = curr_pos;
                        while let Some(p) = came_from[&pos] {
                            path.push(p);
                            pos = p;
                        }
                        path.reverse();
                    }
                    return (cost, path);
                }

                dir_pos.set_pos(curr_pos);
                dir_pos.set_dir(dir);
                let neigbrs = self.neighbors(dir_pos.position);
                for next in neigbrs {
                    let cost_new = cost + add_cost(next.1, dir_pos.get_dir());

                    if !cost_so_far.contains_key(&next.0) || cost_new <= cost_so_far[&next.0] {
                        cost_so_far.insert(next.0, cost_new);
                        let prio = cost_new + self.get_end().0.abs_diff(next.0.0) + self.get_end().1.abs_diff(next.0.1);
                        queue.push(next, Reverse(prio));
                        came_from.insert(next.0, Some(curr_pos));
                    }
                }
            }

            (0, path)
        }
    }

    #[derive(Clone, Copy, Debug)]
    pub struct MazePosDir {
        position: Position,
        direction: Direction,
    }

    impl MazePosDir {
        pub fn get_pos(&self) -> Position {
            self.position
        }

        pub fn set_pos(&mut self, pos: Position) {
            self.position = pos;
        }

        pub fn get_dir(&self) -> Direction {
            self.direction
        }

        pub fn set_dir(&mut self, dir: Direction) {
            self.direction = dir
        }

    }
}

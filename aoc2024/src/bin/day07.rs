mod internal {
    pub type Num = u64;
    type Oper = fn(Num, Num) -> Num;

    #[derive(Debug)]
    pub struct Equation {
        test_value: Num,
        parameters: Vec<Num>,
    }

    impl Equation {
        pub fn new(value: &str, params: &str) -> Self {
            let test_value = value.parse().unwrap();
            let parameters = params.split_ascii_whitespace().map(|p| p.parse().unwrap()).collect();
            Self { test_value, parameters }
        }

        fn concat(num1: Num, num2: Num) -> Num {
            num1 * (10 as Num).pow(1 + num2.ilog10()) + num2
        }

        fn calc_sum(&self, ind: usize, sum: Num, operators: &[Oper]) -> bool {
            for op in operators {
                let dsum = op(sum, self.parameters[ind]);
                if ind < self.parameters.len() - 1 {
                    if self.calc_sum(ind + 1, dsum, operators) {
                        return true;
                    }
                } else if dsum == self.test_value {
                    return true;
                }
            }

            false
        }

        pub fn try_equation(&self, do_concat: bool) -> bool {
            use std::ops::{Add, Mul};

            let operators = if !do_concat { vec![Add::add as Oper, Mul::mul as Oper] } else { vec![Add::add as Oper, Mul::mul as Oper, Self::concat] };
            let sum = self.parameters[0];
            if self.calc_sum(1, sum, &operators) {
                return true;
            }

            false
        }

        pub fn get_value(&self) -> Num {
            self.test_value
        }
    }
}
use internal::*;

fn get_sum(equations: &[Equation], do_concat: bool) -> Num {
        equations
        .iter()
        .map(|eq|
            if eq.try_equation(do_concat) {
                eq.get_value()
            } else {
                0
            }
        )
        .sum()
}

fn task_a(lines: &[String]) {
    let equations = parse_input(lines);
    let sum: Num = get_sum(&equations, false);

    println!("Sum = {sum}");
}

fn task_b(lines: &[String]) {
    let equations = parse_input(lines);
    let sum: Num = get_sum(&equations, true);

    println!("Sum = {sum}");
}

fn parse_input(lines: &[String]) -> Vec<Equation> {
    lines
        .iter()
        .map(|line| {
            let mut parts = line.split(": ");
            Equation::new(parts.next().unwrap(), parts.next().unwrap())
        })
        .collect()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {
        let input = common::get_input_from_file("07_sample-1".to_string());
        let equations = parse_input(&input);
        let s = equations[0].try_equation(false);
        println!("s = {}", s);
        let s = equations[1].try_equation(false);
        println!("s = {}", s);
        let s = equations[2].try_equation(false);
        println!("s = {}", s);
    }
}


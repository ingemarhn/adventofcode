use std::{cmp::Reverse, collections::HashMap};
use priority_queue::PriorityQueue;

type Position = [usize; 2];
type Positions = Vec<Position>;

#[derive(Clone, Copy, Debug, PartialEq)]
enum Tile {
    Path,
    Crash,
}

#[derive(Clone, Debug)]
struct Map {
    start: Position,
    end: Position,
    height: usize,
    width: usize,
    byte_positions: Positions,
    map: Vec<Vec<Tile>>,
}

fn get_std_bytes_to_drop() -> usize {
    if common::sample_input_used() {
        12
    } else {
        1024
    }
}

fn drop_bytes(map: &mut Map, start: usize, n_bytes: Option<usize>) {
    let take_bytes = if let Some(nb) = n_bytes {
        nb
    } else {
        get_std_bytes_to_drop()
    };

    for [y, x] in map.byte_positions.iter().skip(start).take(take_bytes) {
        map.map[*y][*x] = Tile::Crash;
    }
}

/*
[Python]
frontier = PriorityQueue()  (priority is estimated cost to the goal)
frontier.put(start, 0)      (here the priority isn't interesting since there only is one element in the queue)
came_from = dict()          (only needed if path shall be returned)
cost_so_far = dict()
came_from[start] = None
cost_so_far[start] = 0

while not frontier.empty():
    current = frontier.get()

    if current == goal:
        break

    for next in graph.neighbors(current):
        new_cost = cost_so_far[current] + graph.cost(current, next)
        if next not in cost_so_far or new_cost < cost_so_far[next]:
            cost_so_far[next] = new_cost
            priority = new_cost + heuristic(goal, next)
            frontier.put(next, priority)
            came_from[next] = current
*/
// A*, A-star, a_star
fn find_shortest_path(map: &Map) -> usize {
    let mut queue = PriorityQueue::new();
    queue.push(map.start, Reverse(0));
    let mut cost_so_far = HashMap::new();
    cost_so_far.insert(map.start, 0);

    let get_nebrs = |[y, x]: Position|
        [(-1, 0), (0, 1), (1, 0), (0, -1)]
        .iter()
        .filter_map(|(iy, ix)| {
            let yn = y as isize + *iy;
            let xn = x as isize + *ix;
            if yn >= 0 &&
               yn < map.height as isize &&
               xn >= 0 &&
               xn < map.width as isize &&
               map.map[y][x] != Tile::Crash
            {
                Some([yn as usize, xn as usize])
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
    ;

    while let Some((curr_pos, curr_path_len)) = queue.pop() {
        if curr_pos == map.end {
            return curr_path_len.0;
        }

        for nbr in get_nebrs(curr_pos) {
            let path_len_new = curr_path_len.0 + 1;
            if cost_so_far.contains_key(&nbr) && path_len_new >= cost_so_far[&nbr] {
                continue;
            }

            cost_so_far.insert(nbr, path_len_new);
            queue.push(nbr, Reverse(path_len_new));
        }
    }

    0
}

fn find_blocking_byte(map: &Map) -> Position {
    let mut map_base = map.clone();
    let mut n_dropped_bytes = get_std_bytes_to_drop();
    let mut n_bytes_to_drop = (map.byte_positions.len() - n_dropped_bytes) / 2;
    let mut map_test = map_base.clone();

    loop {
        drop_bytes(&mut map_test, n_dropped_bytes, Some(n_bytes_to_drop));
        let path_len = find_shortest_path(&map_test);
        if path_len != 0 {
            // A path is still found
            map_base = map_test.clone();
            n_dropped_bytes += n_bytes_to_drop;
        } else {
            if n_bytes_to_drop == 1 {
                break;
            }

            map_test = map_base.clone();
        }
        if n_bytes_to_drop > 1 {
            n_bytes_to_drop /= 2;
        }
    }

    map.byte_positions[n_dropped_bytes]
}

fn task_a(lines: &[String]) {
    let mut map = parse_input(lines);
    drop_bytes(&mut map, 0, None);
    let path_len = find_shortest_path(&map);

    println!("Shortest path: {}", path_len);
}

fn task_b(lines: &[String]) {
    let mut map = parse_input(lines);
    drop_bytes(&mut map, 0, None);
    let blocking = find_blocking_byte(&map);

    println!("Blocking byte: {},{}", blocking[0], blocking[1]);
}

fn parse_input(lines: &[String]) -> Map {
    let start = [0; 2];
    let grid_sz = if common::sample_input_used() {
        7
    } else {
        71
    };
    let end = [grid_sz - 1, grid_sz - 1];

    let byte_positions =
        lines
            .iter()
            .map(|line|{
                let mut xy = line.split(',');
                [xy.next().unwrap().parse().unwrap(), xy.next().unwrap().parse().unwrap()]
            })
            .collect();

    let map = vec![vec![Tile::Path; grid_sz]; grid_sz];

    Map { start, end, width: grid_sz, height: grid_sz, byte_positions, map }
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


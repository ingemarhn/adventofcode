use regex::Regex;

fn find_mul(lines: &[String]) -> u64 {
    let mut prod = 0;

    let line_reg: Regex = Regex::new(r"mul\(\d{1,3},\d{1,3}\)").unwrap();
    for line in lines {
        let matches = line_reg.find_iter(line).map(|m| m.as_str());

        for m in matches {
            let digs = &m[4..m.len()-1].split(',').collect::<Vec<_>>();
            prod += digs[0].parse::<u64>().unwrap() * digs[1].parse::<u64>().unwrap();
        }
    }

    prod
}

fn find_mulb(lines: &[String]) -> u64 {
    let mut prod = 0;

    let line_reg: Regex = Regex::new(r"mul\(\d{1,3},\d{1,3}\)|do\(\)|don't\(\)").unwrap();
    let mut is_mul_enabled = true;
    for line in lines {
        let matches = line_reg.find_iter(line).map(|m| m.as_str());

        for m in matches {
            if m == "do()" {
                is_mul_enabled = true;
                continue;
            }
            if m == "don't()" {
                is_mul_enabled = false;
                continue;
            }
            if !is_mul_enabled {
                continue;
            }
            let digs = &m[4..m.len()-1].split(',').collect::<Vec<_>>();
            prod += digs[0].parse::<u64>().unwrap() * digs[1].parse::<u64>().unwrap();
        }
    }

    prod
}

fn task_a(lines: &[String]) {
    let prod = find_mul(lines);

    println!("Sum of mul: {}", prod);
}

fn task_b(lines: &[String]) {
    let prod = find_mulb(lines);

    println!("Sum of mul: {}", prod);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

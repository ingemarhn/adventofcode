#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

mod internal {

}
use internal::*;

fn task_a(lines: &[String]) {
    let _ = parse_input(lines);
}

fn task_b(lines: &[String]) {
    let _ = parse_input(lines);
}

fn parse_input(lines: &[String]) -> usize {
    // lines
    //     .iter()
    //     .map(|line|
    //         line
    //         .split(", ")
    //     )
    //     .collect::<Vec<_>>()

    // lines[0]
    //     .split(',') | .chars()
    //     .map(|l| l.parse().unwrap())
    //     .collect()

    // lines[0].chars()

    // let path_chars = lines
    //     .iter()
    //     .map(|l| l
    //         .chars()
    // -possibly: .map(|c| c as u8)
    //         .collect::Vec<_>>()
    //     )
    //     .collect::Vec<_>>();

    // lines.iter().fold(Vec::new(), |mut splits, line| {
    //     splits.push(line.split('\t'));
    //     splits
    // })

    // for line in lines {
    // }

    0
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


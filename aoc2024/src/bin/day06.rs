use std::collections::HashSet;

type Position = [usize; 2];
type PositionsUnique = HashSet<Position>;
type PositionsAll = Vec<Position>;

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Tile {
    Path,
    Obstruction,
}

#[derive(Clone, Debug)]
struct Map {
    start: Position,
    height: usize,
    width: usize,
    map: Vec<Vec<Tile>>,
}

fn get_path(map: &Map) -> PositionsUnique {
    let mut positions = PositionsUnique::new();
    let mut direction = Direction::Up;
    let mut position = map.start;
    positions.insert(position);

    loop {
        match direction {
            Direction::Up => {
                if position[0] == 0 { break }
                if map.map[position[0] - 1][position[1]] != Tile::Obstruction {
                    position[0] -= 1;
                } else {
                    direction = Direction::Right;
                    continue;
                }
            },
            Direction::Down => {
                if position[0] == map.height - 1 { break }
                if map.map[position[0] + 1][position[1]] != Tile::Obstruction {
                    position[0] += 1;
                } else {
                    direction = Direction::Left;
                    continue;
                }
            },
            Direction::Left => {
                if position[1] == 0 { break }
                if map.map[position[0]][position[1] - 1] != Tile::Obstruction {
                    position[1] -= 1;
                } else {
                    direction = Direction::Up;
                    continue;
                }
            },
            Direction::Right => {
                if position[1] == map.width - 1 { break }
                if map.map[position[0]][position[1] + 1] != Tile::Obstruction {
                    position[1] += 1;
                } else {
                    direction = Direction::Down;
                    continue;
                }
            },
        }

        positions.insert(position);
    }

    positions
}

fn find_loops(map: &Map) -> usize {
    let mut map_try = map.clone();
    let mut try_pos = map.start;
    let mut count = 0;

    for r in 0 .. map.height {
        for c in 0 .. map.width {
            if map_try.map[r][c] != Tile::Path && [r, c] != map.start {
                continue;
            }

            // Restore previous try
            map_try.map[try_pos[0]][try_pos[1]] = Tile::Path;
            // Try a new obstruction
            try_pos = [r, c];
            map_try.map[try_pos[0]][try_pos[1]] = Tile::Obstruction;

            let mut positions = PositionsAll::new();
            let mut direction = Direction::Up;
            let mut position = map.start;
            positions.push(position);
            const N_LOOPS: usize = 20000;
            let mut loop_count = 0;

            for _ in 0 .. N_LOOPS {
                loop_count += 1;
                match direction {
                    Direction::Up => {
                        if position[0] == 0 { break }
                        if map_try.map[position[0] - 1][position[1]] != Tile::Obstruction {
                            position[0] -= 1;
                        } else {
                            direction = Direction::Right;
                            continue;
                        }
                    },
                    Direction::Down => {
                        if position[0] == map.height - 1 { break }
                        if map_try.map[position[0] + 1][position[1]] != Tile::Obstruction {
                            position[0] += 1;
                        } else {
                            direction = Direction::Left;
                            continue;
                        }
                    },
                    Direction::Left => {
                        if position[1] == 0 { break }
                        if map_try.map[position[0]][position[1] - 1] != Tile::Obstruction {
                            position[1] -= 1;
                        } else {
                            direction = Direction::Up;
                            continue;
                        }
                    },
                    Direction::Right => {
                        if position[1] == map.width - 1 { break }
                        if map_try.map[position[0]][position[1] + 1] != Tile::Obstruction {
                            position[1] += 1;
                        } else {
                            direction = Direction::Down;
                            continue;
                        }
                    },
                }

                positions.push(position);
            }

            if loop_count == N_LOOPS {
                count += 1;
            }
        }
    }

    count
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let pos = get_path(&map);

    println!("# Positions: {}", pos.len());
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let pos = find_loops(&map);

    println!("# Positions: {}", pos);
}

fn parse_input(lines: &[String]) -> Map {
    let height = lines.len();
    let width = lines[0].len();
    let mut start = [0; 2];

    let map =
    lines
        .iter()
        .enumerate()
        .map(|(r, line)|
            line
            .chars()
            .enumerate()
            .map(|(c, ch)|
                match ch {
                    '.' => Tile::Path,
                    '^' => {
                        start = [r, c];
                        Tile::Path
                    },
                    '#' => Tile::Obstruction,
                    _ => panic!()
                }
            )
            .collect()
        )
        .collect();

    Map { start, width, height, map }
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


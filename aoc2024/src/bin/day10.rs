use std::collections::HashSet;

fn count_trailheads_scores(positions: &[Vec<u8>]) -> (usize, usize) {
    let mut score_sum = 0;
    let mut rating_sum = 0;
    for (ri, row) in positions.iter().enumerate() {
        for (ci, dig) in row.iter().enumerate() {
            if *dig != 0 {
                continue;
            }

            let mut queue = vec![(ri, ci)];
            let mut score = 0;
            let mut rating = 0;
            let mut nine_pos = HashSet::new();
            while let Some((r, c)) = queue.pop() {
                if positions[r][c] == 9 {
                    rating += 1;
                    if nine_pos.insert((r, c)) {
                        score += 1;
                    }
                    continue;
                }

                let nebrs =
                    [(-1, 0), (0, 1), (1, 0), (0, -1)]
                    .iter()
                    .filter_map(|(ir, ic)| {
                        let rn = r as isize + *ir;
                        let cn = c as isize + *ic;
                        if rn >= 0 && rn < positions.len() as isize && cn >= 0 && cn < positions[0].len() as isize {
                            Some((rn as usize, cn as usize))
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>()
                ;

                for nb in nebrs.iter() {
                    if positions[nb.0][nb.1] == positions[r][c] + 1 {
                        queue.push((nb.0, nb.1));
                    }
                }
            }

            score_sum += score;
            rating_sum += rating;
        }
    }

    (score_sum, rating_sum)
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let sum = count_trailheads_scores(&map).0;

    println!("Score sum: {}", sum);
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let sum = count_trailheads_scores(&map).1;

    println!("Score sum: {}", sum);
}

fn parse_input(lines: &[String]) -> Vec<Vec<u8>> {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|c| c.to_digit(10).unwrap() as u8)
            .collect()
        )
        .collect()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


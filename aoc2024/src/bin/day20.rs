use std::collections::{HashMap, HashSet};
use aoc2024::maze::{Direction, Maze, Positions};

fn find_best_cheats(map: &Maze, path: &Positions) -> usize {
    // store how many steps is taken at each position
    // also use a HashMap with position as key and # steps as value, so it easily can be decided if sought position is later in path
    let mut pos_index = HashMap::new();
    let path_n_steps =
        path
        .iter()
        .enumerate()
        .map(|(i, p)| {
            // side effect
            pos_index.insert(p, i);
            (p, i)
        })
        .collect::<Vec<_>>();

    let mut cheats = Vec::new();
    // find all positions where
    // the position in the path is two steps away
    // # steps is larger than for the test position
    // a wall is inbetween
    // calc how many cheats that will save at least 100 steps
    let lim = if common::sample_input_used() { 2 } else { 100 };
    for ((y, x), n_steps) in path_n_steps {
        for (nbr, dir) in map.neighbors_steps((*y, *x), 2) {
            let Some(cheat_steps) = pos_index.get(&nbr) else { continue; };
            if *cheat_steps < n_steps { continue; }
            let possible_wall_pos = match dir {
                Direction::North => (*y - 1, *x),
                Direction::South => (*y + 1, *x),
                Direction::East => (*y, *x + 1),
                Direction::West => (*y, *x - 1),
            };
            if map.is_open(possible_wall_pos) { continue; }

            let diff = *cheat_steps - n_steps - 2;
            if diff >= lim {
                cheats.push(diff);
            }
        }
    }

    cheats.len()
}

fn find_best_cheats_long(path: &Positions) -> usize {
    // store how many steps is taken at each position
    // also use a HashMap with position as key and # steps as value, so it easily can be decided if sought position is later in path
    let mut pos_index = HashMap::new();
    let path_n_steps =
        path
        .iter()
        .enumerate()
        .map(|(i, p)| {
            // side effect
            pos_index.insert(p, i);
            (p, i)
        })
        .collect::<Vec<_>>();

    // Calc manhattan distances from (0, 0)
    let mut manhattan_distances = HashMap::new();
    for p in path.iter() {
        let dist = p.0 + p.1;
        manhattan_distances
            .entry(dist)
            .and_modify(|v: &mut Vec<(usize, usize)>| v.push(*p))
            .or_insert(vec![*p]);
    }

    let mut cheats = HashMap::new();
    // find all positions where
        // the position in the path is max 20 manhattan steps away
        // # steps is larger than for the test position
    let lim = if common::sample_input_used() { 50 } else { 100 };
    let max_steps = 20;
    for &(&(y, x), start_pos_steps) in path_n_steps.iter().take(path_n_steps.len() - 1) {
        let start_dist = y + x;
        for try_dist in start_dist.saturating_sub(max_steps) .. start_dist + max_steps {
            let Some(try_poss) = manhattan_distances.get(&try_dist) else { continue; };
            for &(dy, dx) in try_poss {
                // calc how many cheats that will save at least 100 steps for current candidates
                let Some(p_steps) = pos_index.get(&(dy, dx)) else { continue; };
                let manhattan = y.abs_diff(dy) + x.abs_diff(dx);
                if manhattan > 20 {
                    continue;
                }

                let save = p_steps.abs_diff(start_pos_steps) - manhattan;
                if save < lim {
                    continue;
                }

                let mut indices = [(y, x), (dy, dx)];
                indices.sort();
                #[allow(clippy::type_complexity)]
                cheats
                    .entry(save)
                    .and_modify(|e: &mut HashSet<[(usize, usize); 2]>|
                        if !e.contains(&indices) {
                            e.insert(indices);
                        }
                    )
                    .or_insert(HashSet::from([indices]));
            }
        }
    }

    cheats
        .values()
        .map(|p| p.len())
        .sum()
}

fn task_a(lines: &[String]) {
    let (map, mut dir_pos) = Maze::parse_input(lines);
    let add_cost = |_dir1, _dir2| 1;
    let path = map.find_best_path(&mut dir_pos, add_cost);
    let n_top_cheats = find_best_cheats(&map, &path);

    println!("# of cheats = {}", n_top_cheats);
}

fn task_b(lines: &[String]) {
    let (map, mut dir_pos) = Maze::parse_input(lines);
    let add_cost = |_dir1, _dir2| 1;
    let path = map.find_best_path(&mut dir_pos, add_cost);
    let n_top_cheats = find_best_cheats_long(&path);

    println!("# of cheats = {}", n_top_cheats);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


use std::collections::{HashMap, HashSet};
use std::sync::mpsc;
use std::thread;
use std::sync::mpsc::Sender;
use std::sync::mpsc::Receiver;

fn test_designs(patterns: &HashMap<char, Vec<String>>, designs: &[String]) -> usize {
    let mut count = 0;
    for des in designs.iter() {
        let mut queue = vec![des.to_owned()];
        let mut visited = HashSet::new();
        while let Some(d) = queue.pop() {
            if d.is_empty() {
                count += 1;
                break;
            }

            if visited.contains(&d) {
                continue;
            }
            visited.insert(d.clone());

            if let Some(ptrns) = patterns.get(&d.chars().next().unwrap()) {
                for p in ptrns.iter().filter(|pp| d.starts_with(*pp)) {
                    queue.push(d[p.len() .. ].to_string());
                }
            }
        }
    }

    count
}

fn match_all_threads(patterns: &HashMap<char, Vec<String>>, designs: &[String]) -> usize {
    fn traverse_design(patterns: &HashMap<char, Vec<String>>, design: &str, to_thr: &Sender<(usize, String, usize)>, from_thr: &Receiver<Option<usize>>) -> usize {
        if design.is_empty() {
            return 1;
        }
        to_thr.send((1, design.to_string(), 0)).unwrap();
        if let Some(count) = from_thr.recv().unwrap() {
            return count;
        }

        let Some(ptrns) = patterns.get(&design.chars().next().unwrap()) else {
            return 0;
        };
        let ptn_it = ptrns.iter().filter(|pp| design.starts_with(*pp));

        let mut count = 0;
        for ptn in ptn_it {
            let this_count = traverse_design(patterns, &design[ptn.len() .. ], to_thr, from_thr);
            count += this_count;
            if count != 0 {
                to_thr.send((2, design.to_string(), count)).unwrap();
                from_thr.recv().unwrap();
            }
        }

        if count == 0 {
            to_thr.send((2, design.to_string(), 0)).unwrap();
            from_thr.recv().unwrap();
        }

        count
    }

    let (to_thr, from_main) = mpsc::channel();
    let (to_main, from_thr) = mpsc::channel();

    thread::spawn(move || {
        let mut memory = HashMap::<String, usize>::new();

        loop {
            let Ok((req_type, design, count)) = from_main.recv() else {
                break;
            };
            match req_type {
                1 => {
                    let mem = memory.get(&design);
                    to_main.send(mem.cloned()).unwrap()
                },
                2 => {
                    memory.insert(design, count);
                    to_main.send(None).unwrap();
                },
                3 => memory.clear(),
                _ => panic!()
            }
        }
    });

    let mut count = 0;
    for des in designs.iter() {
        count += traverse_design(patterns, des, &to_thr, &from_thr);
    }

    drop(to_thr);

    count
}

fn match_all(patterns: &HashMap<char, Vec<String>>, designs: &[String]) -> u64 {
    fn traverse_design(patterns: &HashMap<char, Vec<String>>, design: &str, memory: &mut HashMap<String, u64>) -> u64 {
        if design.is_empty() {
            return 1;
        }
        if let Some(count) = memory.get(design) {
            return *count;
        }

        let Some(ptrns) = patterns.get(&design.chars().next().unwrap()) else {
            return 0;
        };
        let ptn_it = ptrns.iter().filter(|pp| design.starts_with(*pp));

        let mut count = 0;
        for ptn in ptn_it {
            let this_count = traverse_design(patterns, &design[ptn.len() .. ], memory);
            count += this_count;
            if count > 0 {
                memory.insert(design.to_string(), count);
            }
        }

        if count == 0 {
            memory.insert(design.to_string(), 0);
        }

        count
    }


    let mut count = 0;
    let mut mem = HashMap::new();
    for des in designs.iter() {
        count += traverse_design(patterns, des, &mut mem);
    }

    count
}

fn task_a(lines: &[String]) {
    let (patterns, designs) = parse_input(lines);
    let n_designs = test_designs(&patterns, &designs);

    println!("# possible designs: {}", n_designs);
}

fn task_b(lines: &[String]) {
    let (patterns, designs) = parse_input(lines);
    let n_designs = if common::get_extra_input().is_empty() {
        match_all(&patterns, &designs)
    } else {
        match_all_threads(&patterns, &designs) as u64
    };

    println!("# possible designs: {}", n_designs);
}

fn parse_input(lines: &[String]) -> (HashMap<char, Vec<String>>, Vec<String>) {
    let mut patterns = HashMap::new();
    for p in lines[0].split(", ") {
        patterns.entry(p.chars().next().unwrap())
            .and_modify(|pp: &mut Vec<String>| pp.push(p.to_string()))
            .or_insert(vec![p.to_string()]);
    }
    for p in patterns.iter_mut() {
        p.1.sort();
    }

    let designs =
        lines[2 .. ]
        .iter()
        .map(|line|
            line.to_owned()
        )
        .collect::<Vec<_>>();

    (patterns, designs)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


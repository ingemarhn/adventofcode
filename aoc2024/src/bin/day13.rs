use itertools::Itertools;

#[derive(Debug)]
struct Button {
    x: u64,
    y: u64,
}
type Prize = Button;

#[derive(Debug)]
struct Machine {
    button_a: Button,
    button_b: Button,
    prize: Prize,
}

impl Machine {
    fn new(xy: &[&str]) -> Self {
        let (axi, ayi, bxi, byi, pxi, pyi) =
            xy
            .iter()
            .map(|s| s.parse().unwrap())
            .collect_tuple()
            .unwrap();

        Self {
            button_a: Button { x: axi, y: ayi },
            button_b: Button { x: bxi, y: byi },
            prize: Prize { x: pxi, y: pyi}
        }
    }

    fn parse_input(lines: &[String]) -> Vec<Machine> {
        let mut strs = [""; 6];
        let mut ind = 0;
        let mut machines = Vec::new();

        for line in lines {
            if !line.is_empty() {
                let mut parts = line.split(", ");
                let si = if ind < 4 { 12 } else { 9 };
                strs[ind] = &parts.next().unwrap()[si ..];
                strs[ind + 1] = &parts.next().unwrap()[2 ..];
                ind += 2;
            } else {
                machines.push(Machine::new(&strs));
                ind = 0;
            }
        }
        machines.push(Machine::new(&strs));

        machines
    }
}

fn spend_tokens(machines: &[Machine]) -> u64 {
    /*
        Elimination and simplification

        - two linear Diophantine equations (A Diophantine equation is an equation where only integer solutions are allowed. These equations are named after the ancient Greek mathematician Diophantus.)
        - three options
            - A Unique Integer Solution Exists
            - Infinitely Many Integer Solutions Exist
            - No Integer Solutions Exist

        button_a.x * n + button_b.x * m = prize.x
        button_a.y * n + button_b.y * m = prize.y
            <=>
        m = (button_a.y * prize.x - button_a.x * prize.y) / (button_a.y * button_b.x - button_a.x * button_b.y)
        n = (button_b.y * prize.x - button_b.x * prize.y) / (button_b.y * button_a.x - button_b.x * button_a.y)

        For this task to be solveable it's not possible that there exist an infinite number of solutions. Either no solution at all or a unique solution.
    */

    let mut tokens = 0;
    for machine in machines {
        let a = (machine.button_b.y * machine.prize.x).abs_diff(machine.button_b.x * machine.prize.y) / (machine.button_b.y * machine.button_a.x).abs_diff(machine.button_b.x * machine.button_a.y);
        let b = (machine.button_a.y * machine.prize.x).abs_diff(machine.button_a.x * machine.prize.y) / (machine.button_a.y * machine.button_b.x).abs_diff(machine.button_a.x * machine.button_b.y);

        if a * machine.button_a.x + b * machine.button_b.x == machine.prize.x &&
           a * machine.button_a.y + b * machine.button_b.y == machine.prize.y {
            tokens += a * 3 + b;
           }
    }

    tokens
}

fn task_a(lines: &[String]) {
    let machines = Machine::parse_input(lines);
    let tokens = spend_tokens(&machines);

    println!("Tokens = {}", tokens);
}

fn task_b(lines: &[String]) {
    let mut machines = Machine::parse_input(lines);
    for machine in machines.iter_mut() {
        machine.prize.x += 10000000000000;
        machine.prize.y += 10000000000000;
    }
    let tokens = spend_tokens(&machines);

    println!("Tokens = {}", tokens);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


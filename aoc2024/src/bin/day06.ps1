#!/snap/bin/pwsh -NoProfile
param (
    $sample = $true
)
<#
use std::collections::HashSet;

type Position = [usize; 2];
type PositionsUnique = HashSet<Position>;
type PositionsAll = Vec<Position>;

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Tile {
    Path,
    Obstruction,
}

#[derive(Clone, Debug)]
struct Map {
    start: Position,
    height: usize,
    width: usize,
    map: Vec<Vec<Tile>>,
}

fn get_path(map: &Map) -> PositionsUnique {
    let mut positions = PositionsUnique::new();
    let mut direction = Direction::Up;
    let mut position = map.start;
    positions.insert(position);

    loop {
        match direction {
            Direction::Up => {
                if position[0] == 0 { break }
                if map.map[position[0] - 1][position[1]] != Tile::Obstruction {
                    position[0] -= 1;
                } else {
                    direction = Direction::Right;
                    continue;
                }
            },
            Direction::Down => {
                if position[0] == map.height - 1 { break }
                if map.map[position[0] + 1][position[1]] != Tile::Obstruction {
                    position[0] += 1;
                } else {
                    direction = Direction::Left;
                    continue;
                }
            },
            Direction::Left => {
                if position[1] == 0 { break }
                if map.map[position[0]][position[1] - 1] != Tile::Obstruction {
                    position[1] -= 1;
                } else {
                    direction = Direction::Up;
                    continue;
                }
            },
            Direction::Right => {
                if position[1] == map.width - 1 { break }
                if map.map[position[0]][position[1] + 1] != Tile::Obstruction {
                    position[1] += 1;
                } else {
                    direction = Direction::Down;
                    continue;
                }
            },
        }

        positions.insert(position);
    }

    positions
}

fn find_loops(map: &Map) -> usize {
    let mut map_try = map.clone();
    let mut try_pos = map.start;
    let mut count = 0;

    for r in 0 .. map.height {
        for c in 0 .. map.width {
            if map_try.map[r][c] != Tile::Path && [r, c] != map.start {
                continue;
            }

            // Restore previous try
            map_try.map[try_pos[0]][try_pos[1]] = Tile::Path;
            // Try a new obstruction
            try_pos = [r, c];
            map_try.map[try_pos[0]][try_pos[1]] = Tile::Obstruction;

            let mut positions = PositionsAll::new();
            let mut direction = Direction::Up;
            let mut position = map.start;
            positions.push(position);
            const N_LOOPS: usize = 20000;
            let mut loop_count = 0;

            for _ in 0 .. N_LOOPS {
                loop_count += 1;
                match direction {
                    Direction::Up => {
                        if position[0] == 0 { break }
                        if map_try.map[position[0] - 1][position[1]] != Tile::Obstruction {
                            position[0] -= 1;
                        } else {
                            direction = Direction::Right;
                            continue;
                        }
                    },
                    Direction::Down => {
                        if position[0] == map.height - 1 { break }
                        if map_try.map[position[0] + 1][position[1]] != Tile::Obstruction {
                            position[0] += 1;
                        } else {
                            direction = Direction::Left;
                            continue;
                        }
                    },
                    Direction::Left => {
                        if position[1] == 0 { break }
                        if map_try.map[position[0]][position[1] - 1] != Tile::Obstruction {
                            position[1] -= 1;
                        } else {
                            direction = Direction::Up;
                            continue;
                        }
                    },
                    Direction::Right => {
                        if position[1] == map.width - 1 { break }
                        if map_try.map[position[0]][position[1] + 1] != Tile::Obstruction {
                            position[1] += 1;
                        } else {
                            direction = Direction::Down;
                            continue;
                        }
                    },
                }

                positions.push(position);
            }

            if loop_count == N_LOOPS {
                count += 1;
            }
        }
    }

    count
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let pos = get_path(&map);

    println!("# Positions: {}", pos.len());
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let pos = find_loops(&map);

    println!("# Positions: {}", pos);
}

#>

$ErrorActionPreference = 'Stop'

$path = 1
$obstruction = 2

function parse_input($lines) {
    $height = $lines.length
    $width = $lines[0].length
    $start = 0, 0


    $map = New-Object System.Collections.ArrayList
    foreach ($i in 0 .. ($lines.length - 1)) {
        $mapRow = New-Object System.Collections.ArrayList
        $cc = 0
        foreach ($c in $lines[$i].GetEnumerator()) {
            switch ($c) {
                '.' { [void]$mapRow.Add($path) }
                '^' {
                    $start = $i, $cc
                    [void]$mapRow.Add($path)
                }
                '#' { [void]$mapRow.Add($obstruction) }
            }
            $cc++
        }
        [void]$map.Add($mapRow)
    }

    @{
        Start=$start
        Height=$height
        Width=$width
        map=$map
    }
}

function gs() {
    if ($sample) {
        6, 4
    } else {
        43, 72
    }
}
function find_loops {
    $up = 1
    $right = 2
    $down = 3
    $left = 4

    $try_pos = $map.Start
    $count = 0
    foreach ($r in 0 .. ($map.Height - 1)) {
        foreach ($c in 0 .. ($map.Width - 1)) {
            if (($map.map[$r][$c] -ne $path) -or ($r, $c -eq $map.Start)) {
                continue
            }

            $map.map[$try_pos[0]][$try_pos[1]] = $path
            $try_pos = $r, $c
            $map.map[$try_pos[0]][$try_pos[1]] = $obstruction
            $direction = $up
            $position = gs
            $Nloops = 10000
            $loopCount = 0

            :trying foreach ($q in 1 .. $Nloops) {
                $loopCount++
                switch ($direction) {
                    $up {
                        if ($position[0] -eq 0) { break trying }
                        if ($map.map[$position[0] - 1][$position[1]] -ne $obstruction) {
                            $position[0]--
                        } else {
                            $direction = $right
                            continue trying
                        }
                    }

                    $down {
                        if ($position[0] -eq $map.Height - 1) { break trying }
                        if ($map.map[$position[0] + 1][$position[1]] -ne $obstruction) {
                            $position[0]++
                        } else {
                            $direction = $left
                            continue trying
                        }
                    }

                    $left {
                        if ($position[1] -eq 0) { break trying }
                        if ($map.map[$position[0]][$position[1] - 1] -ne $obstruction) {
                            $position[1]--
                        } else {
                            $direction = $up
                            continue trying
                        }
                    }

                    $right {
                        if ($position[1] -eq $map.Width - 1) { break trying }
                        if ($map.map[$position[0]][$position[1] + 1] -ne $obstruction) {
                            $position[1]++
                        } else {
                            $direction = $down
                            continue trying
                        }
                    }
                }
            }

            if ($loopCount -eq $Nloops) {
                $count++
            }
        }
    }

    $count
}

if ($sample) {
    $lines = (Get-Content '/data/Misc/Code/adventofcode/aoc2024/input/day-06_sample-1.txt') -split "`n"
} else {
    $lines = (Get-Content '/data/Misc/Code/adventofcode/aoc2024/input/day-06.txt') -split "`n"
}
$map = parse_input $lines
$count = find_loops

"Count: $($count)"

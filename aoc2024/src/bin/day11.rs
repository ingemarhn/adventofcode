use counter::Counter;

type Stones = Vec<usize>;

/*
Chat GPT:
Frequency-Based Processing
Instead of storing and processing each number separately, track counts of unique numbers rather than the numbers themselves. This drastically reduces memory usage.

Use a Dictionary (Counter): Store each unique number and how many times it appears.
Process in Bulk: Instead of a queue of numbers, process each unique number by its count.
Iterate Efficiently: Only update the dictionary instead of handling a massive list.

Why This is More Efficient
✔ Avoids Storing Millions of Numbers: Only tracks counts of unique numbers.
✔ Reduces Processing Overhead: Each transformation step is applied in bulk, not per number.
✔ Scales Much Better: Works even when billions of numbers would exist in brute force.
*/
fn get_n_stones(stones: &Stones, n_blinks: usize) -> usize {
    let mut num_counts = stones.iter().copied().collect::<Counter<_>>();

    for _ in 0 .. n_blinks {
        let mut next_counts = Counter::new();

        for (num, count) in num_counts.iter() {
            /*
            Rules:
            - If the stone is engraved with the number 0, it is replaced by a stone engraved with the number 1.
            - If the stone is engraved with a number that has an even number of digits, it is replaced by two stones.
              The left half of the digits are engraved on the new left stone, and the right half of the digits are engraved on the new right stone.
              (The new numbers don't keep extra leading zeroes: 1000 would become stones 10 and 0.)
            - If none of the other rules apply, the stone is replaced by a new stone; the old stone's number multiplied by 2024 is engraved on the new stone.
            */

            // count is inherited from the "parent" stone. Regardless if the result is a single stone or a split, each new stone appears as many times as its parent.
            if *num == 0 {
                next_counts[&1] += count;
            } else if num.ilog10() % 2 == 1 { // ilog10 returns odd number for a number with even number of figures
                let n_split = 10_usize.pow((num.ilog10() + 1) / 2);
                let new_nums = ( num / n_split, num % n_split );
                next_counts[&new_nums.0] += count;
                next_counts[&new_nums.1] += count;
            } else {
                next_counts[&(num * 2024)] += count;
            }
        }

        num_counts = next_counts;
    }

    num_counts.values().sum()
}

fn task_a(lines: &[String]) {
    let stones = parse_input(lines);
    let n_bl = {
        let extra = common::get_extra_input();
        if !extra.is_empty() {
            extra.parse().unwrap()
        } else {
            25
        }
    };
    let n_stones = get_n_stones(&stones, n_bl);

    println!("# of stones: {}", n_stones);
}

fn task_b(lines: &[String]) {
    let stones = parse_input(lines);
    let n_stones = get_n_stones(&stones, 75);

    println!("# of stones: {}", n_stones);
}

fn parse_input(lines: &[String]) -> Stones {
    let stones =
        lines[0]
        .split_ascii_whitespace()
        .map(|n| n.parse().unwrap())
        .collect::<Vec<_>>();

    stones
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

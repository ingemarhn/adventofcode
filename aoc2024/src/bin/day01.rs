use itertools::Itertools;

type List = Vec<usize>;

fn get_distance(vec1: &[usize], vec2: &[usize]) -> usize {
    let mut v_it2 = vec2.iter();
    vec1
        .iter()
        .map(|n1| n1.abs_diff(*v_it2.next().unwrap()))
        .sum()
}

fn get_score(vec1: &[usize], vec2: &[usize]) -> usize {
    vec1
        .iter()
        .map(|n1| vec2.iter().filter(|n2| *n1 == **n2).count() * *n1)
        .sum()
}

fn task_a(lines: &[String]) {
    let (mut vec1, mut vec2) = parse_input(lines);
    vec1.sort();
    vec2.sort();

    let sum = get_distance(&vec1, &vec2);

    println!("Sum: {}", sum);
}

fn task_b(lines: &[String]) {
    let (vec1, vec2) = parse_input(lines);

    let score = get_score(&vec1, &vec2);

    println!("Similarity score: {}", score);
}

fn parse_input(lines: &[String]) -> (List, List) {
    let mut v1 = Vec::new();
    let mut v2 = Vec::new();

    for line in lines {
        let (n1, n2) = line
            .split("   ")
            .map(|n| n.parse::<usize>().unwrap())
            .collect_tuple()
            .unwrap();
        v1.push(n1);
        v2.push(n2);
    }

    (v1, v2)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


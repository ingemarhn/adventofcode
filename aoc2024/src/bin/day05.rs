use itertools::Itertools;

mod internal {
    use std::collections::HashMap;

    pub type Updates = Vec<Vec<usize>>;

    #[derive(Debug)]
    pub struct Rules (HashMap<usize, Vec<usize>>);

    impl Rules {
        pub fn new() -> Self {
            Self(HashMap::new())
        }

        pub fn add(&mut self, page: usize, page_after: usize) {
            self.0.entry(page).and_modify(|e| e.push(page_after)).or_insert(vec![page_after]);
        }

        pub fn is_after(&self, page: usize, page_after: usize) -> bool {
            if let Some(p_aft) = self.0.get(&page) {
                p_aft.iter().any(|e| *e == page_after)
            } else {
                false
            }
        }
    }

    pub fn sum_of_order(updates: &Updates, rules: &Rules) -> usize {
        let mut sum = 0;

        'o: for pages in updates {
            for (i, p_1st) in pages.iter().enumerate().take(pages.len() - 1) {
                for p_2nd in pages.iter().skip(i + 1) {
                    if !rules.is_after(*p_1st, *p_2nd) {
                        continue 'o;
                    }
                }
            }

            sum += pages[pages.len() / 2];
        }

        sum
    }

    pub fn sum_of_unorder(updates: &Updates, rules: &Rules) -> usize {
        let mut sum = 0;

        for mut pages in updates.clone() {
            let mut is_swapped = false;
            let mut i = 0;
            'o: while i < pages.len() - 1 {
                let p_1st = pages[i];
                for j in i + 1 .. pages.len() {
                    let p_2nd = pages[j];
                    if !rules.is_after(p_1st, p_2nd) {
                        pages.swap(i, j);
                        is_swapped = true;
                        continue 'o;
                    }
                }

                i += 1;
            }

            if is_swapped {
                sum += pages[pages.len() / 2];
            }
        }

        sum
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let (rules, updates) = parse_input(lines);
    let sum = sum_of_order(&updates, &rules);

    println!("Sum = {sum}");
}

fn task_b(lines: &[String]) {
    let (rules, updates) = parse_input(lines);
    let sum = sum_of_unorder(&updates, &rules);

    println!("Sum = {sum}");
}

fn parse_input(lines: &[String]) -> (Rules, Updates) {
    let mut lines_it = lines.iter();

    let mut rules = Rules::new();
    for line in lines_it.by_ref() {
        if line.is_empty() {
            break;
        }

        let (p_1st, p_2nd) = line.split('|').map(|p| p.parse().unwrap()).collect_tuple().unwrap();
        rules.add(p_1st, p_2nd);
    }

    let mut updates = Updates::new();
    for line in lines_it.by_ref() {
        updates.push(line.split(',').map(|p| p.parse().unwrap()).collect());
    }

    (rules, updates)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


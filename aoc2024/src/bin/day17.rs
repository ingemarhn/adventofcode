mod internal {
    use std::collections::HashMap;
    use std::fmt::Debug;
    use std::str::FromStr;

    pub type Value = u128;
    pub type Registers = HashMap<char, Value>;
    pub type Program = Vec<(OpCode, Value)>;

    #[derive(Clone, Copy, Debug)]
    pub enum OpCode {
        Adv,
        Bxl,
        Bst,
        Jnz,
        Bxc,
        Out,
        Bdv,
        Cdv,
    }

    impl FromStr for OpCode {
        type Err = std::num::ParseIntError;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let oc = match s {
                "0" => Self::Adv,
                "1" => Self::Bxl,
                "2" => Self::Bst,
                "3" => Self::Jnz,
                "4" => Self::Bxc,
                "5" => Self::Out,
                "6" => Self::Bdv,
                "7" => Self::Cdv,
                _ => panic!()
            };
            Ok( oc )
        }
    }

    /*
    The adv instruction (opcode 0) performs division. The numerator is the value in the A register. The denominator is found by raising 2 to the power of the instruction's combo operand.
    (So, an operand of 2 would divide A by 4 (2^2); an operand of 5 would divide A by 2^B.) The result of the division operation is truncated to an integer and then written to the A register.

    The bxl instruction (opcode 1) calculates the bitwise XOR of register B and the instruction's literal operand, then stores the result in register B.

    The bst instruction (opcode 2) calculates the value of its combo operand modulo 8 (thereby keeping only its lowest 3 bits), then writes that value to the B register.

    The jnz instruction (opcode 3) does nothing if the A register is 0. However, if the A register is not zero, it jumps by setting the instruction pointer to the value of its literal
    operand; if this instruction jumps, the instruction pointer is not increased by 2 after this instruction.

    The bxc instruction (opcode 4) calculates the bitwise XOR of register B and register C, then stores the result in register B. (For legacy reasons, this instruction reads an operand
    but ignores it.)

    The out instruction (opcode 5) calculates the value of its combo operand modulo 8, then outputs that value. (If a program outputs multiple values, they are separated by commas.)

    The bdv instruction (opcode 6) works exactly like the adv instruction except that the result is stored in the B register. (The numerator is still read from the A register.)

    The cdv instruction (opcode 7) works exactly like the adv instruction except that the result is stored in the C register. (The numerator is still read from the A register.)
     */
    pub fn run_program(program: &Program, registers: &mut Registers, break_on_value: bool) -> Vec<Value> {
        let mut outvalues = Vec::new();
        let mut instr_ptr = 0;

        loop {
            if instr_ptr >= program.len() {
                break;
            }

            let (opcode, operand_literal) = program[instr_ptr];
            let mut did_jmp = false;
            /*
            Combo operands 0 through 3 represent literal values 0 through 3.
            Combo operand 4 represents the value of register A.
            Combo operand 5 represents the value of register B.
            Combo operand 6 represents the value of register C.
            Combo operand 7 is reserved and will not appear in valid programs.
            */
            let reg_a = registers[&'A'];
            let operand_combo = match operand_literal {
                0 ..= 3 => operand_literal,
                4 => reg_a,
                5 => registers[&'B'],
                6 => registers[&'C'],
                _ => panic!("Illegal operand: {}", operand_literal)
            };
            let two_pow = || (2 as Value).pow(operand_combo as u32);

            match opcode {
                OpCode::Adv => { registers.entry('A').and_modify(|e| *e /= two_pow()); },
                OpCode::Bxl => { registers.entry('B').and_modify(|e| *e ^= operand_literal); },
                OpCode::Bst => { registers.entry('B').and_modify(|e| *e = operand_combo % 8); },
                OpCode::Jnz => if reg_a != 0 {
                    instr_ptr = operand_literal as usize;
                    did_jmp = true;
                },
                OpCode::Bxc => {
                    let rc = registers[&'C'];
                    registers.entry('B').and_modify(|e| *e ^= rc);
                    // registers.entry('B').and_modify(|e| *e = rc ^ operand_combo);
                },
                OpCode::Out => {
                    let outval = operand_combo % 8;
                    outvalues.push(outval);
                    if break_on_value {
                        break;
                    }
                },
                OpCode::Bdv => {
                    registers.entry('B').and_modify(|e| *e = reg_a / two_pow());
                },
                OpCode::Cdv => {
                    registers.entry('C').and_modify(|e| *e = reg_a / two_pow());
                },
            }

            if !did_jmp {
                instr_ptr += 1;
            }
        }

        outvalues
    }

    pub fn parse_input(lines: &[String]) -> (Registers, Program) {
        let mut registers = Registers::new();
        let mut lines_it = lines.iter();
        #[allow(clippy::while_let_on_iterator)]
        while let Some(line) = lines_it.next() {
            if line.is_empty() {
                break;
            }

            let mut chrs = line.chars();
            let reg = chrs.nth(9).unwrap();
            chrs.next();
            chrs.next();
            let reg_val = chrs.fold(0, |acc, v| acc * 10 + v.to_digit(10).unwrap() as Value);
            registers.insert(reg, reg_val);
        }

        let mut instructions = Vec::new();
        let mut program = lines_it.next().unwrap()[9 .. ].split(',');
        loop {
            let Some(opcode_s) = program.next() else { break; };
            let opcode = opcode_s.parse().unwrap();
            let operand = program.next().unwrap().parse().unwrap();
            instructions.push((opcode, operand));
        }

        (registers, instructions)
    }
}
use internal::*;

fn find_value(program: &Program, registers: &Registers, prog_string: &str) -> Value {
    let mut registers = registers.clone();
/*
backtrackingen från B till A handlar om 2 x 3 bitar
först (Bst, 4) lagrar de tre LSB från A till B
därefter görs en shift-right av A med B (Cdv, 5)
B xor C lagras i B (Bxc, 5)
B xor 6
print B's 3 LSB

för sista output-siffra så finns det 3 signifikanta bitar i A
för näst sista output-siffra så finns det 6 signifikanta bitar i A
osv...

sista siffran
A kan inte vara större än 7 för då hade inte A >> 3 lett till 0 och loopen hade fortsatt
Det innebär också att C alltid blir 0 för sista siffran

för nästa siffra
    A <<= 3
    testa alla A .. A + 7 för att se vilka A som genererar rätt output
    när någon av ovan genererar rätt kombination, gå vidare till nästa siffra
    om någon senare siffra inte kan generera rätt output, kom tillbaks hit
    fortsätt så här tills alla siffror är rätt
*/

    let program_vals =
        prog_string
            .chars()
            .filter(|c| *c != ',')
            .map(|c| c.to_digit(10).unwrap() as Value)
            .collect::<Vec<_>>();

    fn calc_value(program: &Program, registers: &mut Registers, program_vals: &[Value], pv_ind: usize, a_val: Value) -> Option<Value> {
        let mut ret_val = None;

        for a_add in 0 .. 8 {
            let a_try = a_val + a_add;
            registers.insert('A', a_try);
            let out = run_program(program, registers, true);
            if out[0] == program_vals[pv_ind] {
                registers.insert('A', a_try);
                let out_all = run_program(program, registers, false);
                if out_all == program_vals[pv_ind ..] {
                    if pv_ind == 0 {
                        return Some(a_try);
                    }
                    ret_val = calc_value(program, registers, program_vals, pv_ind - 1, a_try << 3);
                    if ret_val.is_some() {
                        break;
                    }
                }
            }
        }

        ret_val
    }

    let a_min = calc_value(program, &mut registers, &program_vals, program_vals.len() - 1, 0);

    a_min.unwrap()
}

fn task_a(lines: &[String]) {
    let (mut registers, program) = parse_input(lines);
    let out = run_program(&program, &mut registers, false);

    print!("Output: {}", out[0]);
    for o in out.iter().skip(1) {
        print!(",{}", o);
    }
    println!();
}

fn task_b(lines: &[String]) {
    let (registers, program) = parse_input(lines);
    let start_val = find_value(&program, &registers, &lines.last().unwrap()[9 .. ]);

    println!("Lowest init value = {}", start_val);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

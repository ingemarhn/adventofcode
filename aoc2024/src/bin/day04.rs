mod internal {
    pub type Matrix = Vec<Vec<char>>;

    pub fn find_xmas(matrix: &Matrix) -> usize {
        let mut count = 0;

        let empty = vec![' '; matrix.len() + 6];
        let fill = vec![' '; 3];
        let mut ext_matrix = Vec::new();
        for _ in 0 .. 3 {
            ext_matrix.push(empty.clone());
        }
        for row in matrix.iter() {
            ext_matrix.push(
                [fill.clone(), row.to_vec(), fill.clone()].concat()
            );
        }
        for _ in 0 .. 3 {
            ext_matrix.push(empty.clone());
        }

        let is_xmas = |r: usize, c: usize, ir: isize, ic: isize| {
            let iir1 = r as isize + ir;
            let iir2 = iir1 + ir;
            let iir3 = iir2 + ir;
            let iic1 = c as isize + ic;
            let iic2 = iic1 + ic;
            let iic3 = iic2 + ic;
            if
                ext_matrix[iir1 as usize][iic1 as usize] == 'M' &&
                ext_matrix[iir2 as usize][iic2 as usize] == 'A' &&
                ext_matrix[iir3 as usize][iic3 as usize] == 'S'
            {
                1
            } else {
                0
            }
        };

        for (ir, row) in ext_matrix.iter().enumerate().skip(3).take(matrix.len()) {
            for (ic, c) in row.iter().enumerate() {
                if *c != 'X' {
                    continue;
                }

                // straight
                count += is_xmas(ir, ic, 0, 1);
                // straight left
                count += is_xmas(ir, ic, 0, -1);
                // down
                count += is_xmas(ir, ic, 1, 0);
                // up
                count += is_xmas(ir, ic, -1, 0);
                // diag down left
                count += is_xmas(ir, ic, 1, -1);
                // diag down right
                count += is_xmas(ir, ic, 1, 1);
                // diag up left
                count += is_xmas(ir, ic, -1, -1);
                // diag up right
                count += is_xmas(ir, ic, -1, 1);
            }
        }

        count
    }

    pub fn find_x_mas(matrix: &Matrix) -> usize {
        let mut count = 0;

        let empty = vec![' '; matrix.len() + 6];
        let fill = vec![' '];
        let mut ext_matrix = Vec::new();
        ext_matrix.push(empty.clone());
        for row in matrix.iter() {
            ext_matrix.push(
                [fill.clone(), row.to_vec(), fill.clone()].concat()
            );
        }
        ext_matrix.push(empty.clone());

        for (ir, row) in ext_matrix.iter().enumerate().skip(1).take(matrix.len()) {
            for (ic, c) in row.iter().enumerate() {
                if *c != 'A' {
                    continue;
                }

                if
                    ((ext_matrix[ir - 1][ic - 1] == 'M' && ext_matrix[ir + 1][ic + 1] == 'S') ||
                     (ext_matrix[ir - 1][ic - 1] == 'S' && ext_matrix[ir + 1][ic + 1] == 'M'))
                     &&
                    ((ext_matrix[ir - 1][ic + 1] == 'M' && ext_matrix[ir + 1][ic - 1] == 'S') ||
                     (ext_matrix[ir - 1][ic + 1] == 'S' && ext_matrix[ir + 1][ic - 1] == 'M'))
                {
                    count += 1;
                }
            }
        }

        count
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let matrix = parse_input(lines);
    let count = find_xmas(&matrix);

    println!("XMAS count = {}", count);
}

fn task_b(lines: &[String]) {
    let matrix = parse_input(lines);
    let count = find_x_mas(&matrix);

    println!("X-MAS count = {}", count);
}

fn parse_input(lines: &[String]) -> Matrix {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .collect()
        )
    .collect()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


use std::cmp::Ordering;

type Report = Vec<u16>;
type Reports = Vec<Report>;

fn check_report(report: &Report) -> bool {
    let comp = if report[1] < report[0] {
        Ordering::Less
    } else {
        Ordering::Greater
    };

    let mut is_safe = true;
    for (i, n) in report.iter().enumerate().skip(1) {
        if (n.cmp(&report[i - 1])) != comp || n.abs_diff(report[i - 1]) > 3 {
            is_safe = false;
            break;
        }
    }
    is_safe
}

fn find_safe(reports: &Reports, use_dampener: bool) -> usize {
    reports
        .iter()
        .filter(|report| {
            let mut is_safe = check_report(report);
            if !is_safe && use_dampener {
                for i in 0 .. report.len() {
                    let mut report_p = report.to_vec();
                    report_p.remove(i);
                    is_safe = check_report(&report_p);
                    if is_safe {
                        break;
                    }
                }
            }
            is_safe
        })
        .count()
}

fn task_a(lines: &[String]) {
    let reports = parse_input(lines);
    let count = find_safe(&reports, false);

    println!("Count: {}", count);
}

fn task_b(lines: &[String]) {
    let reports = parse_input(lines);
    let count = find_safe(&reports, true);

    println!("Count: {}", count);
}

fn parse_input(lines: &[String]) -> Reports {
    lines
        .iter()
        .map(|line|
            line
            .split_ascii_whitespace()
            .map(|n| n.parse().unwrap())
            .collect()
        )
        .collect()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


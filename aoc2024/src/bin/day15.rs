mod internal {
    use std::ops::{Add, Sub};

    pub type Pos = (usize, usize);
    pub type Map = Vec<Vec<Tile>>;
    pub type Directions = Vec<Direction>;

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub enum Direction {
        Up,
        Down,
        Left,
        Right,
    }

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub enum TBox {
        Single,
        LBox,
        RBox,
    }
    #[derive(Clone, Copy, Debug, PartialEq)]
    pub enum Tile {
        Wall,
        FreeGround,
        Box(TBox),
        Robot,
    }

    fn new_position(dir: &Direction, pos: Pos) -> Pos {
        match *dir {
            Direction::Up => (pos.0 - 1, pos.1),
            Direction::Down => (pos.0 + 1, pos.1),
            Direction::Left => (pos.0, pos.1 - 1),
            Direction::Right => (pos.0, pos.1 + 1),
        }
    }

    pub fn robot_step(dir: &Direction, robot: Pos, map: &mut Map) -> Pos {
        let new_pos = new_position(dir, robot);

        match map[new_pos.0][new_pos.1] {
            Tile::Wall => robot,
            Tile::FreeGround => {
                map[new_pos.0][new_pos.1] = Tile::Robot;
                map[robot.0][robot.1] = Tile::FreeGround;
                new_pos
            },
            Tile::Box(TBox::Single) => {
                let mut test_pos = new_pos;
                loop {
                    test_pos = new_position(dir, test_pos);
                    if map[test_pos.0][test_pos.1] == Tile::Wall {
                        return robot;
                    }
                    if map[test_pos.0][test_pos.1] == Tile::FreeGround {
                        map[test_pos.0][test_pos.1] = Tile::Box(TBox::Single);
                        map[robot.0][robot.1] = Tile::FreeGround;
                        map[new_pos.0][new_pos.1] = Tile::Robot;
                        return new_pos;
                    }
                }
            }
            _ => panic!()
        }
    }

    pub fn robot_step_large_map(dir: &Direction, robot_pos: Pos, map: &mut Map) -> Pos {
        let robot_new_pos = new_position(dir, robot_pos);

        match map[robot_new_pos.0][robot_new_pos.1] {
            Tile::Wall => robot_pos,
            Tile::FreeGround => {
                map[robot_new_pos.0][robot_new_pos.1] = Tile::Robot;
                map[robot_pos.0][robot_pos.1] = Tile::FreeGround;
                robot_new_pos
            },
            Tile::Box(_) => {
                if *dir == Direction::Up || *dir == Direction::Down {
                    let box_2nd_pos = |pos: &(usize, usize)| {
                        if map[pos.0][pos.1] == Tile::Box(TBox::LBox) {
                            (pos.0, pos.1 + 1)
                        } else {
                            (pos.0, pos.1 - 1)
                        }
                    };
                    let mut test_pos = vec![robot_new_pos, box_2nd_pos(&robot_new_pos)];
                    let mut test_pos_stack = vec![test_pos.clone()];

                    loop {
                        // Move one row for all test positions
                        let mut test_pos_next = Vec::new();
                        for tp in test_pos.iter() {
                            let tpn = new_position(dir, *tp);
                            // If any of the new test positions is a wall, don't do anything
                            if map[tpn.0][tpn.1] == Tile::Wall {
                                return robot_pos;
                            }
                            // If any of the new test positions is not a free ground, collect new test positions
                            if map[tpn.0][tpn.1] != Tile::FreeGround && !test_pos_next.contains(&tpn) {
                                test_pos_next.push(tpn);
                                if map[tpn.0][tpn.1] != map[tp.0][tp.1] {
                                    test_pos_next.push(box_2nd_pos(&tpn));
                                }
                            }
                        }

                        // If test_pos_next is empty all test_pos are free ground. Start moving.
                        if test_pos_next.is_empty() {
                            while let Some(box_pos) = test_pos_stack.pop() {
                                for bp in box_pos {
                                    let bpn = new_position(dir, bp);
                                    map[bpn.0][bpn.1] = map[bp.0][bp.1];
                                    map[bp.0][bp.1] = Tile::FreeGround;
                                }
                            }
                            map[robot_new_pos.0][robot_new_pos.1] = Tile::Robot;
                            map[robot_pos.0][robot_pos.1] = Tile::FreeGround;

                            return robot_new_pos;
                        } else {
                            test_pos_stack.push(test_pos_next.clone());
                            test_pos = test_pos_next;
                        }
                    }
                } else {
                    let mut test_pos = new_position(dir, robot_new_pos);
                    loop {
                        test_pos = new_position(dir, test_pos);
                        if map[test_pos.0][test_pos.1] == Tile::Wall {
                            return robot_pos;
                        }
                        if map[test_pos.0][test_pos.1] == Tile::FreeGround {
                            let mut target_pos = test_pos;
                            let first_pos = robot_pos;
                            let op = match target_pos.cmp(&first_pos) {
                                std::cmp::Ordering::Less => Add::add,
                                std::cmp::Ordering::Greater => Sub::sub,
                                _ => panic!()
                            };
                            loop {
                                map[target_pos.0][target_pos.1] = map[target_pos.0][op(target_pos.1, 1)];
                                target_pos = (target_pos.0, op(target_pos.1, 1));
                                if target_pos == first_pos {
                                    break;
                                }
                            }
                            map[robot_pos.0][robot_pos.1] = Tile::FreeGround;

                            return robot_new_pos;
                        }
                    }
                }
            }
            _ => panic!("{:?}, {:?}", robot_new_pos, map[robot_new_pos.0][robot_new_pos.1])
        }
    }

    pub fn _pr(map: &Map) {
        for r in map.iter() {
            for t in r.iter() {
                match *t {
                    Tile::Wall => print!("#"),
                    Tile::FreeGround => print!("."),
                    Tile::Robot => print!("@"),
                    Tile::Box(TBox::Single) => print!("O"),
                    Tile::Box(TBox::LBox) => print!("["),
                    Tile::Box(TBox::RBox) => print!("]"),
                }
            }
            println!()
        }
    }

    pub fn parse_input(lines: &[String]) -> (Map, Directions, Pos) {
        let mut field = Map::new();

        let mut robot = (0, 0);
        let mut lines_it = lines.iter();
        let mut r = 0;
        loop {
            let line = lines_it.next().unwrap();
            if line.is_empty() {
                break;
            }

            let row =
                line
                .chars()
                .enumerate()
                .map(|(c, ch)|
                    match ch {
                        '.' => Tile::FreeGround,
                        '#' => Tile::Wall,
                        'O' => Tile::Box(TBox::Single),
                        '@' => {
                            robot = (r, c);
                            Tile::Robot
                        },
                        _ => panic!("{c}: {ch}")
                    }
                )
                .collect::<Vec<_>>();

            field.push(row);
            r += 1;
        }

        let mut directions = Directions::new();
        for line in lines_it {
            for dir in
                line
                .chars()
                .filter(|c| *c != '\n')
                .map(|ch|
                    match ch {
                        '^' => Direction::Up,
                        'v' => Direction::Down,
                        '<' => Direction::Left,
                        '>' => Direction::Right,
                        _ => panic!()
                    }
                )
            {
                directions.push(dir);
            }
        }

        (field, directions, robot)
    }
}
use internal::*;

fn robot_work(robot_start: Pos, directions: &Directions, map: &mut Map, is_large_map: bool) {
    let mut robot = robot_start;
    let robot_fn = if is_large_map {
        robot_step_large_map
    } else {
        robot_step
    };
    for d in directions {
        robot = robot_fn(d, robot, map);
// println!("d = {:?}", d);
// _pr(map); println!();
    }
}

fn sum_gps(map: &Map) -> u64 {
    map
        .iter()
        .enumerate()
        .map(|(r, row)|
            row
                .iter()
                .enumerate()
                .filter(|(_, t)| **t == Tile::Box(TBox::Single) || **t == Tile::Box(TBox::LBox))
                .map(|(c, _)| r as u64 * 100 + c as u64)
                .sum::<u64>()
        )
        .sum()
}

fn task_a(lines: &[String]) {
    let (mut map, directs, robo_pos) = parse_input(lines);
    robot_work(robo_pos, &directs, &mut map, false);
    let sum = sum_gps(&map);

    println!("GPS cooords sum = {}", sum);
}

fn enlarge_map(map: &Map) -> (Map, Pos) {
    let mut map_large = Map::new();
    let mut robot = (0, 0);

    for (ri, row) in map.iter().enumerate() {
        let mut row_new = Vec::new();
        for (ti, t) in row.iter().enumerate() {
            match *t {
                Tile::FreeGround |
                Tile::Wall => {
                    row_new.push(*t);
                    row_new.push(*t);
                },
                Tile::Box(TBox::Single) => {
                    row_new.push(Tile::Box(TBox::LBox));
                    row_new.push(Tile::Box(TBox::RBox));
                },
                Tile::Robot => {
                    robot = (ri, ti * 2);
                    row_new.push(*t);
                    row_new.push(Tile::FreeGround);
                },
                _ => panic!()
            }
        }

        map_large.push(row_new);
    }

    (map_large, robot)
}

fn task_b(lines: &[String]) {
    let (map, directs, _) = parse_input(lines);
    let (mut map, robo_pos) = enlarge_map(&map);

    robot_work(robo_pos, &directs, &mut map, true);
    let sum = sum_gps(&map);

    println!("GPS cooords sum = {}", sum);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {
        /*  ....@...
            ...[]##.
            ..[]....
            .[][]...
            ..[][]..
            ...[]...
            .....##.
            ........
        */
        let mut map = vec![
            vec![Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::Robot, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround],
            vec![Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::Wall, Tile::Wall, Tile::FreeGround],
            vec![Tile::FreeGround, Tile::FreeGround, Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround],
            vec![Tile::FreeGround, Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::FreeGround, Tile::FreeGround, Tile::FreeGround],
            vec![Tile::FreeGround, Tile::FreeGround, Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::FreeGround, Tile::FreeGround],
            vec![Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::Box(TBox::LBox), Tile::Box(TBox::RBox), Tile::FreeGround, Tile::FreeGround, Tile::FreeGround],
            vec![Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::Wall, Tile::Wall],
            vec![Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround, Tile::FreeGround],
        ];
        _pr(&map);
        let robo_pos = (0, 4);
        robot_work(robo_pos, &vec![Direction::Down], &mut map, true);
        println!();
        _pr(&map);
        }
}


// use std::{collections::{HashMap, HashSet, VecDeque}, vec};
use std::collections::{HashMap, HashSet};
use priority_queue::PriorityQueue;
use std::cmp::Reverse;
use aoc2024::maze::{Maze, MazePosDir};

fn _find_cheapest_path_cost(map: &Maze, dir_pos: &mut MazePosDir) -> usize {
    let mut queue = PriorityQueue::new();
    let mut visited = HashMap::new();
    queue.push((map.get_start(), dir_pos.get_dir()), Reverse(0));
    visited.insert(map.get_start(), usize::MAX);
    let mut cost_min = usize::MAX;

    while let Some(((pos, dir), cost)) = queue.pop() {
        let cost = cost.0;
        if pos == map.get_end() {
            cost_min = cost;
            break;
        }

        dir_pos.set_pos(pos);
        dir_pos.set_dir(dir);
        let neigbrs = map.neighbors(dir_pos.get_pos());
        for (np, d) in neigbrs {
            let cost_new = cost +
                if d == dir_pos.get_dir() {
                    1
                } else {
                    1001
                };

            if let Some(cost_vis) = visited.get(&np) {
                if cost_new >= *cost_vis {
                    continue;
                }
            }

            queue.push((np, d), Reverse(cost_new));
            visited.insert(np, cost_new);
        }
    }

    cost_min
}

fn _find_tiles_in_best_paths(map: &Maze, dir_pos: &mut MazePosDir) -> usize {
    // let mut queue = VecDeque::new();
    let mut queue = PriorityQueue::new();
    let mut visited = HashMap::new();
    visited.insert((map.get_start(), dir_pos.get_dir()), 0);
    // let visited = HashSet::from([map.get_start()]);
    // queue.push_back((map.get_start(), dir_pos.get_dir(), 0, vec![map.get_start()], visited));
    let mut cost_min = usize::MAX;
    queue.push((map.get_start(), dir_pos.get_dir(), vec![map.get_start()]), Reverse(0));
    let mut best_paths = Vec::new();

    // while let Some((pos, dir, cost, path, visited)) = queue.pop_back() {
    while let Some(((pos, dir, path), cost_rev)) = queue.pop() {
        let cost = cost_rev.0;
        if cost >= cost_min {
            continue;
        }
        if pos == map.get_end() {
            #[allow(clippy::comparison_chain)]
            if cost < cost_min {
                cost_min = cost;
                best_paths = vec![path];
            } else if cost == cost_min {
                best_paths.push(path);
            }

            continue;
        }

        dir_pos.set_pos(pos);
        dir_pos.set_dir(dir);
        let neigbrs = map.neighbors(dir_pos.get_pos());
        for (np, d) in neigbrs {
            let cost_new = cost +
                if d == dir_pos.get_dir() {
                    1
                } else {
                    1001
                };

            if let Some(cost_vis) = visited.get(&(np, d)) {
                if cost_new >= *cost_vis {
                    continue;
                }
            }

            let mut path_new = path.clone();
            path_new.push(np);
            queue.push((np, d, path_new), Reverse(cost_new));
        }
    }

    let tiles =
        best_paths.concat()
        .into_iter()
        .collect::<HashSet<_>>();
    tiles.len()
}

fn find_tiles_in_best_paths(map: &Maze, dir_pos: &mut MazePosDir) -> usize {
    /*
    [Python]
    frontier = PriorityQueue()  (priority is estimated cost to the goal)
    frontier.put(start, 0)      (here the priority isn't interesting since there only is one element in the queue)
    came_from = dict()          (only needed if path shall be returned)
    cost_so_far = dict()
    came_from[start] = None
    cost_so_far[start] = 0

    while not frontier.empty():
        current = frontier.get()

        if current == goal:
            break

        for next in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(goal, next)
                frontier.put(next, priority)
                came_from[next] = current
    */
    // A*, A-star, a_star
    // This implementation is slightly modified since all best paths must be found.
    //      came_from global tracking doesn't work
    //      Continue, instead of break, when the goal is reached

    use priority_queue::PriorityQueue;
    use std::cmp::Reverse;

    let mut queue = PriorityQueue::new();
    queue.push((map.get_start(), dir_pos.get_dir(), vec![map.get_start()]), Reverse(0));
    let mut cost_so_far = HashMap::new();
    cost_so_far.insert((map.get_start(), dir_pos.get_dir()), 0);
    let mut cost_min = usize::MAX;
    let mut best_paths = Vec::new();

    while let Some(((curr_pos, dir, path), _pr)) = queue.pop() {
        let cost = cost_so_far[&(curr_pos, dir)];
        if cost > cost_min {
            continue;
        }

        if curr_pos == map.get_end() {

            #[allow(clippy::comparison_chain)]
            if cost < cost_min {
                cost_min = cost;
                best_paths = vec![path];
            } else if cost == cost_min {
                best_paths.push(path);
            }

            continue;
        }

        dir_pos.set_pos(curr_pos);
        dir_pos.set_dir(dir);
        let neigbrs = map.neighbors(dir_pos.get_pos());
        for next in neigbrs {
            let cost_new = cost +
                if next.1 == dir_pos.get_dir() {
                    1
                } else {
                    1001
                };

            if (!cost_so_far.contains_key(&next) || cost_new <= cost_so_far[&next]) && !path.contains(&next.0) {
                cost_so_far.insert(next, cost_new);
                let mut path_next = path.clone();
                path_next.push(next.0);
                let prio = cost_new + map.get_end().0.abs_diff(next.0.0) + map.get_end().1.abs_diff(next.0.1);
                queue.push((next.0, next.1, path_next), Reverse(prio));
            }
        }
    }

    let tiles =
        best_paths.concat()
        .into_iter()
        .collect::<HashSet<_>>();
    tiles.len()
}

fn task_a(lines: &[String]) {
    let (map, mut dir_pos) = Maze::parse_input(lines);
    let add_cost = |dir1, dir2|
        if dir1 == dir2 {
            1
        } else {
            1001
        }
    ;
    let score = map.find_lowest_cost(&mut dir_pos, add_cost);

    println!("Score = {}", score);
}

fn task_b(lines: &[String]) {
    let (map, mut dir_pos) = Maze::parse_input(lines);
    let n_tiles = find_tiles_in_best_paths(&map, &mut dir_pos);

    println!("# tiles = {}", n_tiles);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


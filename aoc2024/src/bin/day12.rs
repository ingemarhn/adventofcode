use std::collections::{HashSet, VecDeque};

type Map = Vec<Vec<char>>;

#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
enum Border {
    Low(usize, usize),
    High(usize, usize),
}

// impl Border {
//     pub const fn is_row(&self) -> bool {
//         matches!(*self, Self::Low(_, _))
//     }
// }

fn area_and_perimeter(map: &Map) -> usize {
    let mut price = 0;
    let mut queue_area_start = VecDeque::new();
    queue_area_start.push_back((0, 0));
    let mut visited = HashSet::new();

    while let Some(area_start) = queue_area_start.pop_front() {
        if visited.contains(&area_start) {
            continue;
        }
        visited.insert(area_start);

        let mut queue_area = VecDeque::new();
        queue_area.push_back(area_start);
        let plant = map[area_start.0][area_start.1];

        let mut area_size = 0;
        let mut area_perimeter = 0;

        while let Some((r, c)) = queue_area.pop_front() {
            area_size += 1;
            if r == 0 || r == map.len() - 1 {
                area_perimeter += 1;
            }
            if c == 0 || c == map[0].len() - 1 {
                area_perimeter += 1;
            }
            let nebrs =
                [(-1, 0), (0, 1), (1, 0), (0, -1)]
                .iter()
                .filter_map(|(ir, ic)| {
                    let rn = r as isize + *ir;
                    let cn = c as isize + *ic;
                    if rn >= 0 && rn < map.len() as isize && cn >= 0 && cn < map[0].len() as isize {
                        Some((rn as usize, cn as usize))
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>()
            ;

            for nb in nebrs.iter() {
                if map[nb.0][nb.1] == plant {
                    if visited.contains(nb) {
                        continue;
                    }
                    queue_area.push_back((nb.0, nb.1));
                    visited.insert(*nb);
                } else {
                    area_perimeter += 1;
                    queue_area_start.push_back(*nb);
                }
            }
        }

        price += area_perimeter * area_size;
    }

    price
}

fn area_and_sides(map: &Map) -> usize {
    let mut price = 0;
    let mut queue_area_start = VecDeque::new();
    queue_area_start.push_back((0, 0));
    let mut visited = HashSet::new();

    while let Some(area_start) = queue_area_start.pop_front() {
        if visited.contains(&area_start) {
            continue;
        }
        visited.insert(area_start);
        let mut sides = HashSet::new();

        let mut queue_area = VecDeque::new();
        queue_area.push_back(area_start);
        let plant = map[area_start.0][area_start.1];

        let mut area_size = 0;
        const OFFSET: usize = 1000;

        while let Some((r, c)) = queue_area.pop_front() {
            area_size += 1;
            if r == 0 {
                sides.insert(Border::Low(r, c));
            }
            if r == map.len() - 1 {
                sides.insert(Border::High(r, c));
            }
            if c == 0 {
                sides.insert(Border::Low(r, c + OFFSET));
            }
            if c == map[0].len() - 1 {
                sides.insert(Border::High(r, c + OFFSET));
            }
            let nebrs =
                [(-1, 0), (0, 1), (1, 0), (0, -1)]
                .iter()
                .filter_map(|(ir, ic)| {
                    let rn = r as isize + *ir;
                    let cn = c as isize + *ic;
                    if rn >= 0 && rn < map.len() as isize && cn >= 0 && cn < map[0].len() as isize {
                        Some((rn as usize, cn as usize))
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>()
            ;

            for nb in nebrs.iter() {
                if map[nb.0][nb.1] == plant {
                    if visited.contains(nb) {
                        continue;
                    }
                    queue_area.push_back(*nb);
                    visited.insert(*nb);
                } else {
                    if nb.1 == c {
                        if nb.0 < r {
                            sides.insert(Border::Low(r, c));
                        } else {
                            sides.insert(Border::High(r, c));
                        }
                    } else {
                        let co = c + OFFSET;
                        if nb.1 < c {
                            sides.insert(Border::Low(r, co));
                        } else {
                            sides.insert(Border::High(r, co));
                        }
                    }
                    queue_area_start.push_back(*nb);
                }
            }
        }

        let is_row = |s|
            match s {
                Border::Low(_, c) => c,
                Border::High(_, c) => c,
            } < OFFSET
        ;
        let border_vals = |b|
            match b {
                Border::Low(r, c) => (0, r, c),
                Border::High(r, c) => (1, r, c),
            }
        ;

        let mut rows = sides.iter().filter(|s| is_row(**s)).collect::<Vec<_>>();
        let mut columns = sides.iter().filter(|s| !is_row(**s)).collect::<Vec<_>>();
        rows.sort_unstable_by(|a, b| {
            let (alh, ar, ac) = border_vals(**a);
            let (blh, br, bc) = border_vals(**b);
            alh.cmp(&blh).then(ar.cmp(&br)).then(ac.cmp(&bc))
        });
        columns.sort_unstable_by(|a, b| {
            let (alh, ar, ac) = border_vals(**a);
            let (blh, br, bc) = border_vals(**b);
            alh.cmp(&blh).then(ac.cmp(&bc)).then(ar.cmp(&br))
        });

        let n_sides =
            2 +
            {
                let (mut lh, mut row, mut col_comp) = border_vals(*rows[0]);
                rows.iter().skip(1).fold(0, |g, b| {
                    let (lh_comp, row_n, col_n) = border_vals(**b);
                    if lh == lh_comp && row_n == row && col_n == col_comp + 1 {
                        col_comp += 1;
                        g
                    } else {
                        row = row_n;
                        col_comp = col_n;
                        lh = lh_comp;
                        g + 1
                    }
                })
            }
            +
            {
                let (mut lh, mut comp_row, mut col) = border_vals(*columns[0]);
                columns.iter().skip(1).fold(0, |g, b| {
                    let (lh_comp, row_n, col_n) = border_vals(**b);
                    if lh == lh_comp && row_n == comp_row + 1 && col_n == col {
                        comp_row += 1;
                        g
                    } else {
                        comp_row = row_n;
                        col = col_n;
                        lh = lh_comp;
                        g + 1
                    }
                })
            }
        ;

        price += n_sides * area_size;
    }

    price
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let price = area_and_perimeter(&map);

    println!("Price: {}", price);
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let price = area_and_sides(&map);

    println!("Price: {}", price);
}

fn parse_input(lines: &[String]) -> Map {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


use std::{cmp::Ordering, vec};

type Coordinate = isize;
type Coordinates = [Coordinate; 2];
type Robots = Vec<Robot>;

#[derive(Clone, Copy, Debug, PartialEq)]
struct Robot {
    position: Coordinates,
    velocity: Coordinates,
}

impl Robot {
    fn new(pos_str: &str, vel_str: &str, ) -> Self {
        Self { position: Self::get_coordinates(pos_str), velocity: Self::get_coordinates(vel_str) }
    }

    fn get_coordinates(s: &str) -> Coordinates {
        let mut coordinates = s
            .split(",")
            .map(|ss| ss.trim().parse().unwrap());

        [
            coordinates.next().unwrap(),
            coordinates.next().unwrap(),
        ]
    }

    fn parse_input(lines: &[String]) -> Robots {
        lines
            .iter()
            .map(|line|
                line
                .split(&['=', 'p', 'v', ' '][..])
                .filter(|s| !s.is_empty())
                .collect::<Vec<_>>()
            )
            .map(|coo|
                Robot::new(coo[0], coo[1])
            )
            .collect::<Robots>()
    }
}

fn space_size() -> (Coordinate, Coordinate) {
    if !common::sample_input_used() {
        (101, 103)
    } else {
        (11, 7)
    }
}

fn move_robots(robots: &mut Robots, n_moves: Coordinate) {
    let (width, height) = space_size();
    for robot in robots.iter_mut() {
        robot.position[0] = ((robot.position[0] + robot.velocity[0] * n_moves) % width + width) % width;
        robot.position[1] = ((robot.position[1] + robot.velocity[1] * n_moves) % height + height) % height;
    }
}

fn _pr(robots: &Robots) {
    let (width, height) = space_size();
    let l = vec![0; width as usize];
    let mut f = vec![l.clone(); height as usize];

    for r in robots {
        f[r.position[1] as usize][r.position[0] as usize] += 1;
    }
    for l in f.iter() {
        for c in l.iter() {
            if *c == 0 {
                print!(".");
            } else {
                print!("{c}");
            }
        }
        println!();
    }
}

fn count_robots(robots: &Robots) -> usize {
    let (width, height) = space_size();
    let (width_middle, height_middle) = (width / 2, height / 2);

    let mut counts_quad = [0; 4];

    for robot in robots {
        match robot.position[0].cmp(&width_middle) {
            Ordering::Less =>
                match robot.position[1].cmp(&height_middle) {
                    Ordering::Less => counts_quad[0] += 1,
                    Ordering::Greater => counts_quad[1] += 1,
                    Ordering::Equal => {},
                },
            Ordering::Greater =>
                match robot.position[1].cmp(&height_middle) {
                    Ordering::Less => counts_quad[2] += 1,
                    Ordering::Greater => counts_quad[3] += 1,
                    Ordering::Equal => {},
                },
            Ordering::Equal => {},
        }
    }

    counts_quad.iter().fold(1, |acc, c| acc * *c)
}

fn detect_christmas_tree(robots_in: &Robots) -> usize {
    let mut count = 0;

    let mut robots = robots_in.clone();
    'l: loop {
        move_robots(&mut robots, 1);
        count += 1;
        if count > 10500 {
            break;
        }
        robots.sort_unstable_by(|a, b| a.position[1].cmp(&b.position[1]).then(a.position[0].cmp(&b.position[0])));
        let mut n_contig = 0;
        let mut prev_x = 0;
        for r in robots.iter() {
            if r.position[0] - 1 == prev_x {
                n_contig += 1;
            } else {
                n_contig = 0;
            }
            prev_x = r.position[0];

            if n_contig == 10 {
                break 'l
            }
        }
    }

    count
}

fn task_a(lines: &[String]) {
    let mut robots = Robot::parse_input(lines);
    move_robots(&mut robots, 100);
    let factor = count_robots(&robots);

    println!("Safety factor = {}", factor);
}

fn task_b(lines: &[String]) {
    let robots = Robot::parse_input(lines);
    let seconds = detect_christmas_tree(&robots);

    println!("# of seconds = {}", seconds);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

use itertools::Itertools;

type Digit = usize;
type Digits = Vec<Digit>;

#[derive(Debug)]
struct Block {
    id: usize,
    n_blocks: usize,
    start: usize,
}

impl Block {
    fn new(id: usize, n_blocks: usize, start: usize) -> Self {
        Self { id, n_blocks, start }
    }
}

fn calc_checksum(digits: &Digits) -> usize {
    let mut checksum = 0;
    let mut map_left_i = 0;
    let mut map_right_i = digits.len() - 1;
    let mut left_id = 0;
    let mut right_id = digits.len() / 2;
    let mut disk_blocks_pos = 0;
    let mut disk_right_len = digits[map_right_i];
    let n_blocks = digits.iter().enumerate().filter(|(i, _)| *i % 2 == 0).map(|(_, v)| *v).sum::<Digit>();

    'o: loop {
        // Calc checksum for each block on the current file, starting from left
        for _ in 0 .. digits[map_left_i] {
            checksum += disk_blocks_pos * left_id;
            disk_blocks_pos += 1;
            if disk_blocks_pos >= n_blocks {
                break 'o;
            }
        }

        // Move blocks from rightmost file till fill up empty space
        // Calc checksum for each moved block
        map_left_i += 1;
        for _ in 0 .. digits[map_left_i] {
            checksum += disk_blocks_pos * right_id;
            disk_blocks_pos += 1;
            if disk_blocks_pos >= n_blocks {
                break 'o;
            }
            disk_right_len -= 1;
            if disk_right_len == 0 {
                map_right_i -= 2;
                if map_right_i < map_left_i {
                    break 'o;
                }
                right_id -= 1;
                disk_right_len = digits[map_right_i];
            }
        }

        map_left_i += 1;
        left_id += 1;

        if left_id >= right_id {
            // break;
        }
    }

    checksum
}

fn calc_checksum_b(digits: &Digits) -> usize {
    const FREE: usize = usize::MAX;

    let mut disk_blocks =
        digits
        .iter()
        .enumerate()
        .map(|(i, v)| {
            let id = if i % 2 == 0 {
                i / 2
            } else {
                FREE
            };
            vec![id; *v]
        })
        .concat();

    let (files, mut free, _) =
        digits
            .iter()
            .enumerate()
            .fold((Vec::new(), Vec::new(), 0), |(mut fi, mut fr, mut di), (i, dig)| {
                let blk = Block::new(i / 2, *dig, di);
                di += *dig;
                if i % 2 == 0 {
                    fi.push(blk);
                } else if blk.n_blocks != 0 {
                    fr.push(blk);
                }

                (fi, fr, di)
            })
    ;

    for file in files.iter().rev() {
        if file.start < free[0].start {
            break;
        }

        let mut i = 0;
        while i < free.len() {
            if file.start < free[i].start {
                break;
            }
            if free[i].n_blocks >= file.n_blocks {
                for p in 0 .. file.n_blocks {
                    disk_blocks[free[i].start + p] = file.id;
                    disk_blocks[file.start + p] = FREE;
                }
                free[i].n_blocks -= file.n_blocks;
                free[i].start += file.n_blocks;
                if free[i].n_blocks == 0 {
                    free.remove(i);
                }

                break;
            }

            i += 1;
        }
    }

    disk_blocks
        .iter()
        .enumerate()
        .filter(|&(_, &v)| v != FREE)
        .map(|(p, &id)| p * id)
        .sum()
}

fn task_a(lines: &[String]) {
    let digits = parse_input(lines);
    let checksum = calc_checksum(&digits);

    println!("Checksum = {}", checksum);
}

fn task_b(lines: &[String]) {
    let digits = parse_input(lines);
    let checksum = calc_checksum_b(&digits);

    println!("Checksum = {}", checksum);
}

fn parse_input(lines: &[String]) -> Digits {
    lines[0]
        .chars()
        .map(|l| l.to_digit(10).unwrap() as Digit)
        .collect()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


mod internal {
    use std::collections::{HashMap, HashSet};

    #[derive(Debug)]
    enum Location {
        Antenna(char, usize, usize),
    }

    #[derive(Debug)]
    pub struct Map {
        height: usize,
        width: usize,
        locations: Vec<Location>,
        antinodes: HashSet<(usize, usize)>
    }

    impl Map {
        pub fn parse_input(lines: &[String]) -> Self {
            let mut locations = Vec::new();
            for (r, line) in lines.iter().enumerate() {
                for (c, ch) in line.chars().enumerate() {
                    if ch != '.' {
                        locations.push( Location::Antenna(ch, r, c));
                    }
                }
            }

            Map { height: lines.len(), width: lines[0].len(), locations, antinodes: HashSet::new() }
        }

        pub fn get_antinodes(&mut self) -> usize {
            let mut antennas: HashMap<char, Vec<(usize, usize)>> = HashMap::new();
            let calc_jump_pos = |p1, p2, lim| {
                let (p1i, p2i) = (p1 as isize, p2 as isize);
                let diff = p2i - p1i;
                let jmp1 = p1i - diff;
                let jmp2 = p2i + diff;
                let o1 = if jmp1 >= 0 && jmp1 < lim as isize { Some(jmp1 as usize) } else { None };
                let o2 = if jmp2 >= 0 && jmp2 < lim as isize { Some(jmp2 as usize) } else { None };
                (o1, o2)
            };

            for loc in self.locations.iter() {
                let Location::Antenna(freq, row, col) = *loc;
                if let Some(a) = antennas.get(&freq) {
                    for (ra, ca) in a.iter() {
                        let (or1, or2) = calc_jump_pos(row, *ra, self.height);
                        let (oc1, oc2) = calc_jump_pos(col, *ca, self.width);
                        if let Some(r1) = or1 {if let Some(c1) = oc1 {
                            self.antinodes.insert((r1, c1));
                        }}
                        if let Some(r2) = or2 {if let Some(c2) = oc2 {
                            self.antinodes.insert((r2, c2));
                        }}
                    }
                }

                antennas.entry(freq).and_modify(|e: &mut Vec<_>| e.push((row, col))).or_insert(vec![(row, col)]);
            }

            self.antinodes.len()
        }

        pub fn get_antinodes_b(&mut self) -> usize {
            let mut antennas: HashMap<char, Vec<(usize, usize)>> = HashMap::new();
            // let calc_some = |p1, p2, lim| {
            //     let (p1i, p2i) = (p1 as isize, p2 as isize);
            //     let diff = p2i - p1i;
            //     let fjmp = p1i - diff;
            //     let mut count = 1;
            //     while fjmp >= 0 && fjmp < lim as isize {
            //         fjmp -= diff;
            //         count += 1;
            //     }
            //     fjmp += diff;
            //     count -= 1;
            //     (fjmp, diff, count)
            // };
            let i = |u| u as isize;

            for loc in self.locations.iter() {
                let Location::Antenna(freq, row, col) = *loc;
                if let Some(a) = antennas.get(&freq) {
                    for (ra, ca) in a.iter() {
                        let (r1, r2, c1, c2) = if *ra > row {
                            (row, *ra, col, *ca)
                        } else {
                            (*ra, row, *ca, col)
                        };
                        let dr = r2 - r1;
                        let dc = i(c2) - i(c1);
                        let njr = r1 / dr;
                        let njc = if dc > 0 {
                            i(c1) / dc
                        } else {
                            i(self.width - 1 - c1) / dc.abs()
                        };
                        let nj = (njr as isize).min(njc);

                        let mut nxtr = i(r1) - nj * i(dr);
                        let mut nxtc = i(c1) - nj * dc;
                        loop {
                            self.antinodes.insert((nxtr as usize, nxtc as usize));
                            nxtr += i(dr);
                            nxtc += dc;
                            if nxtr < 0 || nxtr >= i(self.height) || nxtc < 0 || nxtc >= i(self.width) {
                                break;
                            }
                        }
                    }
                }

                antennas.entry(freq).and_modify(|e: &mut Vec<_>| e.push((row, col))).or_insert(vec![(row, col)]);
            }

            self.antinodes.len()
        }
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let map = &mut Map::parse_input(lines);
    let n_anti = map.get_antinodes();

    println!("# antinodes = {}", n_anti);
}

fn task_b(lines: &[String]) {
    let map = &mut Map::parse_input(lines);
    let n_anti = map.get_antinodes_b();

    println!("# antinodes = {}", n_anti);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}


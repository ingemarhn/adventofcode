#[cfg(debug_assertions)]
static mut DEBUG_LEVEL: i8 = i8::MIN;

#[cfg(debug_assertions)]
pub fn dbg_on(level: i8) {
    unsafe {DEBUG_LEVEL = level;}
    eprintln!("Debugging turned on up to level {}!", level);
}
#[cfg(not(debug_assertions))]
pub fn dbg_on(_: i8) {}

#[cfg(debug_assertions)]
pub fn dbg_off() {
    unsafe {
        if DEBUG_LEVEL == i8::MIN { return; }
        DEBUG_LEVEL = i8::MIN;
    }
    eprintln!("Debugging turned off!");
}
#[cfg(not(debug_assertions))]
pub fn dbg_off() {}

#[cfg(debug_assertions)]
pub fn dbg(s: String, level: i8) {
    unsafe {
        if DEBUG_LEVEL < level {
            return;
        }
    }

    eprintln!("{}", s);
}
#[cfg(not(debug_assertions))]
pub fn dbg(_: String, _: i8) {}

#[cfg(debug_assertions)]
pub fn dbgn(s: String, level: i8) {
    unsafe {
        if DEBUG_LEVEL < level {
            return;
        }
    }

    eprint!("{}", s);
}
#[cfg(not(debug_assertions))]
pub fn dbgn(_: String, _: i8) {}

#[cfg(debug_assertions)]
pub fn get_dbg_level() -> i8 {
    unsafe {
        DEBUG_LEVEL
    }
}
#[cfg(not(debug_assertions))]
pub fn get_dbg_level() -> i8 { 0 }

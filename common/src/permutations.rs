use std::collections::{HashSet, HashMap};
use std::fmt::Debug;
use std::hash::Hash;

use itertools::Itertools;

#[allow(clippy::type_complexity)]
#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct Permutations<I>
    where I: Iterator,
{
    buffer: Vec<I::Item>,
    perm_len: usize,
    queue: Vec<(Vec<I::Item>, Vec<I::Item>,)>,
}

impl<I> Permutations<I>
    where
        I: Iterator,
        I::Item: Debug + Clone,
{
    pub fn new(iter: I, perm_len: usize) -> Permutations<I> {
        let buffer = iter.collect_vec();

        let mut queue = Vec::new();

        if buffer.len() >= perm_len {
            queue.push((buffer.clone(), Vec::new()));
        }

        Permutations {
            buffer,
            perm_len,
            queue,
        }
    }
}

impl<I> PartialEq for Permutations<I>
where
    I: Iterator,
    I::Item: PartialEq
{
    fn eq(&self, other: &Self) -> bool
    {
        self.queue == other.queue
    }
}

impl<I> Iterator for Permutations<I>
where
    I: Iterator,
    I::Item: Clone + Copy + Debug + Eq + Hash,
{
    type Item = Vec<I::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        let Permutations { ref buffer, ref mut queue, ref perm_len } = self;

        if let Some((perm_items, permutation)) = queue.pop() {
            if perm_items.len() > buffer.len() - *perm_len {
                // Set of unique elements in the items list
                let mut uniq_set = HashSet::new();

                // Iterating over the items that will build the next permutation
                for i in 0 .. perm_items.len() {
                    if uniq_set.contains(&perm_items[i]) {
                        // This item has already been used in the next permutation
                        continue;
                    } else {
                        uniq_set.insert(perm_items[i]);
                    }

                    let perm_items_nxt = [perm_items[0 .. i].to_vec(), perm_items[i + 1 ..].to_vec()].concat();
                    let permutations_nxt = [permutation.clone(), perm_items[i..=i].to_vec()].to_vec().concat();
                    queue.push((perm_items_nxt, permutations_nxt));
                }
                self.next()
            } else {
                Some(permutation)
            }
        } else {
            None
        }
    }

    fn count(self) -> usize
        where
            Self: Sized,
    {
        let Permutations { buffer, perm_len, .. } = self;

        let mut elems = HashMap::new();
        for e in buffer.iter() {
            elems
                .entry(e)
                .and_modify(|e| { *e += 1 })
                .or_insert(1);
        }

        if perm_len > 1 {
            let len = buffer.len();
            let numerator = crate::math::factorial(len as u128);
            let denominator = elems
                .iter()
                .fold(1, |prod, (_, &n)| prod * crate::math::factorial(n));
            let count = numerator / denominator;
            if count > usize::MAX as u128 {
                panic!("Iterator count overflow!")
            }

            count as usize
        } else {
            elems.len()
        }
    }
}

impl<I> Clone for Permutations<I>
    where I: Clone + Iterator,
          I::Item: Clone,
{
    #[inline]
    fn clone(&self) -> Self {
        Self {
            buffer: self.buffer.clone(),
            queue: self.queue.clone(),
            perm_len: self.perm_len,
        }
    }
}

pub fn permutations_unique<I>(iter: I, k: usize) -> Permutations<I>
    where I: Iterator,
          I::Item: Debug + Clone,
{
    Permutations::new(iter, k)
}

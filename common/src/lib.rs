use std::fmt::Debug;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;

pub mod debug;
pub use crate::debug::*;
pub mod math;
mod permutations;
pub use crate::permutations::*;
pub mod timer;
pub use crate::timer::*;

pub trait IteratorExt : Iterator {
    fn permutations_unique(self, k: usize) -> Permutations<Self>
        where Self: Sized,
              Self::Item: Clone + Debug,
    {
        permutations::permutations_unique(self, k)
    }
}

impl<T: ?Sized> IteratorExt for T where T: Iterator { }

#[macro_export]
macro_rules! char_from_string {
    ($string:expr, $index:expr) => ({
        let bytes = $string.as_bytes();
        bytes[$index] as char
    });
}

static mut SAMPLE_INPUT: bool = false;
static mut EXTRA_INPUT: String = String::new();

pub fn parse_command_line(args_in: Vec<String>) -> (String, Option<String>) {
    let usage = || {
        panic!("Usage: {} [a|b|ab] [n | -t <test-val>] [-e <extra-input>] [-d debug-level\n{}\n{}\n{}\n{}\nArgs provided: {:?}", args_in[0],
            "       where",
            "           'a|b|ab' specifies which tasks to run",
            "           'n' specifies sample input file",
            "       ",
            &args_in[1 ..]
        );
    };

    let (args, test_parm, extra_parm) = {
        let mut args_cl = args_in[1 .. ].to_vec();

        if let Some(d_ind) = args_cl.iter().position(|arg| arg == &String::from("-d")) {
            args_cl.remove(d_ind);
            let d_val = args_cl.remove(d_ind).parse().unwrap();

            dbg_on(d_val);
        }

        let test_p = match args_cl.iter().position(|arg| arg == &String::from("-t")) {
            Some(t_ind) => {
                let t_val = args_cl.remove(t_ind + 1);
                args_cl.remove(t_ind);
                Some(t_val)
            },
            None => None,
        };

        let extra_p = match args_cl.iter().position(|arg| arg == &String::from("-e")) {
            Some(e_ind) => {
                let e_val = args_cl.remove(e_ind + 1);
                args_cl.remove(e_ind);
                Some(e_val)
            },
            None => None,
        };

        (args_cl, test_p, extra_p)
    };

    let (task, sample) = {
        let (tsk, samp) = match args.len() {
            0 => (String::from("ab"), None),
            1 => match args[0].parse::<u8>().is_err() {
                    true => (args[0].clone(), None),
                    false => (String::from("ab"), Some(args[0].clone())),
                },
            2 => (args[0].clone(), Some(args[1].clone())),
            _ => usage(),
        };

        if samp.is_some() && test_parm.is_some() {
            usage();
        }

        match &test_parm {
            Some(test_val) => (tsk, Some( format!("val:{}", test_val) )),
            None                    => (tsk, samp),
        }
    };

    if sample.is_some() {
        unsafe { SAMPLE_INPUT = true; }
    }

    if extra_parm.is_some() {
        unsafe { EXTRA_INPUT = extra_parm.unwrap() }
    }

    (task, sample)
}

pub fn sample_input_used() -> bool {
    unsafe { SAMPLE_INPUT }
}

pub fn run_task(file: &str, sample: Option<&String>, task: &str, task_a: fn(&[String]), task_b: fn(&[String])) {
    let func = match task {
        "a" => vec![&task_a],
        "b" => vec![&task_b],
        "ab" => vec![&task_a, &task_b],
        "ba" => vec![&task_b, &task_a],
        &_  => panic!("Bad task given: {}", task),
    };

    // base filename must be in the form dayN.rs where N is one or two figures
    let start_ind = file.chars().collect::<Vec<_>>().iter().rposition(|&c| c == '/').unwrap() + 4;
    let end_ind = start_ind + match char_from_string!(file, start_ind + 1).is_ascii_digit() {
        true  => 1,
        false => 0,
    };
    let day_num = file[start_ind ..= end_ind].parse::<u8>().unwrap();
    let day_string = format!("{:02}", day_num);

    let input = if let Some(sample) = sample {
        let sample_str = sample.as_str();
        if let Some(ind) = sample_str.find(':') {
            let ident = &sample_str[0..ind];
            match ident {
                "val" => sample_str[ind+1..].to_string().lines().map(|s| s.to_string()).collect::<Vec<String>>(),
                _ => panic!("Unknown sample identifier: {}", ident)
            }
        } else {
            let day_str = format!("{}_sample-{}", day_string, sample_str);
            get_input_from_file(day_str)
        }
    } else {
        get_input_from_file(day_string)
    };

    let now = start_timer();
    for fnc in func {
        fnc(&input);
        report_timer(now);
    }
}

pub fn get_input_from_file(day_num: String) -> Vec<String> {
    let prepend = match std::env::var("CARGO_MANIFEST_DIR") {
        Ok(dir) => dir,
        Err(_) => ".".to_string(),
    };
    let filename = format!("{}/input/day-{}.txt", prepend, day_num);
    let file = std::fs::File::open(&filename).unwrap_or_else(|err| {
        eprintln!("Cannot open file '{}': {}", filename, err);
        std::process::exit(1);
    });

    let reader = BufReader::new(&file);
    let lines: Vec<String> = reader.lines().collect::<Result<_, _>>().unwrap();

    lines
}

pub fn get_input_from_prompt(prompt: &str) -> String {
    let mut input = String::new();
    print!("{prompt}");
    std::io::stdout().flush().unwrap();
    std::io::stdin().read_line(&mut input).expect("ERROR: unable to read user input");
    input.trim_end_matches(['\r', '\n']).to_string()
}

pub fn halt_by_prompt() {
    get_input_from_prompt("Press Enter...");
}

#[allow(static_mut_refs)]
pub fn get_extra_input() -> String {
    unsafe { EXTRA_INPUT.clone() }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::math::factor;
    use std::env;
    use std::path::Path;


    #[test]
    fn timer() {
        let tmr = start_timer();
        report_timer(tmr);
    }
    #[test]
    fn fetch_input() {
        let tstdir = Path::new("/home/ingemar/adventofcode/aoc2017/src");
        env::set_current_dir(tstdir).unwrap();
        let _input = get_input_from_file(String::from("1"));
    }

    #[test]
    fn prime() {
        assert!( crate::math::is_prime(13u8) );
        assert!( !crate::math::is_prime(14u8) );
    }


    #[test]
    fn fctr() {
        for i in 2u64 ..= 20 {
            let fcts = factor(i);
            println!("fcts [{i}] = {:?}", fcts);
        }
        let i = 926u64; let fcts = factor(i);
        println!("fcts [{i}] = {:?}", fcts);
        let i = 10551326u64; let fcts = factor(i);
        println!("fcts [{i}] = {:?}", fcts);
    }

}

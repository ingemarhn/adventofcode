use num::traits::{pow, PrimInt, /* Signed, */ Unsigned};

pub fn is_prime<T: Unsigned + Copy + PartialOrd>(n: T) -> bool {
    let zero = T::zero();
    let one = T::one();
    let two = one + one;
    let three = two + one;
    let five = three + two;
    let six = three + three;

    if n == two || n == three {
        true
    } else if n % two == zero || n % three == zero {
        false
    } else {
        let mut i: T = five;
        while i * i <= n {
            if n % i == zero || n % (i + two) == zero {
                return false;
            }
            i = i + six
        }

        true
    }
}

pub fn triangular_number<T: Unsigned + Copy>(num: T) -> T {
    (num * (num + T::one())) / (T::one() + T::one())
}

// n!
pub fn factorial<T: PrimInt + std::ops::AddAssign + std::ops::MulAssign>(n: T) -> T {
    let mut res = T::one();
    let mut i = T::one();
    while i < n {
        i += T::one();
        res *= i;
    }

    res
}

// Split a number into its factors
// Proudly stolen from https://github.com/RogueWorm/factor. Generalization done here.
pub fn factor<T: Unsigned + Copy + PartialOrd>(input: T) -> Vec<T>{
    let zero = T::zero();
    let one = T::one();
    let mut count: T = one;
    let mut vector = vec![];

    loop {
        if input % count == zero {
            vector.push(count);
        }

        count = count + one;
        if count > input {
            break;
        }
    }

    vector
}


//  pq-formeln
// x2 + px + q = 0
//      p         p
// x = -- ± sqrt( - ^ 2 - q)
//      2         2
pub fn quadratic_equation_pq(p: i64, q: i64) -> (f64, f64) {
    let p_half = p as f64 / 2.0;
    let root = (pow(p_half, 2) - q as f64).sqrt();

    (-p_half + root, -p_half - root)
}
// pub fn quadratic_equation_pq<T: Signed + Copy>(p: T, q: T) -> f64 {
//     let one = T::one();
//     let two = one + one;

//     (p as f64).sqrt()
// }

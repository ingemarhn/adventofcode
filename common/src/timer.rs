use std::time::Instant;

pub fn start_timer() -> Instant {
    Instant::now()
}

pub fn report_timer(t_val: Instant) {
    let elapsed_time = t_val.elapsed().as_micros();
    eprintln!("Elapsed time: {} seconds.", elapsed_time as f64 / 1000000.0);
}

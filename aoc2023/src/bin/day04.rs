type Cards = Vec<Card>;

mod internal {
    #[derive(Clone, Debug)]
    pub struct Card {
        count: usize,
        winning: Vec<u8>,
        numbers: Vec<u8>,
    }

    impl Card {
        pub fn new() -> Self {
            Self { count: 1, winning: Vec::new(), numbers: Vec::new() }
        }

        pub fn add_winner(&mut self, num: u8) {
            self.winning.push(num);
        }

        pub fn add_number(&mut self, num: u8) {
            self.numbers.push(num);
        }

        pub fn n_winning_numbers(&self) -> usize {
            self.numbers.iter().filter(|n| self.winning.contains(*n)).count()
        }

        pub fn incr_card_count(&mut self, nc: usize) {
            self.count += nc;
        }

        pub fn get_count(&self) -> usize {
            self.count
        }
    }
}
use internal::*;

fn calc_cards(cards: Cards) -> usize {
    let mut cards = cards.clone();
    for i in 0 .. cards.len() {
        let n_wins = cards[i].n_winning_numbers();
        let nc = cards[i].get_count();
        for card in cards[i + 1 ..= i + n_wins].iter_mut() {
            card.incr_card_count(nc);
        }
    }

    cards
        .iter()
        .fold(0, |sum, card| sum + card.get_count() )
}

fn calc_points(cards: &[Card]) -> usize {
    cards
        .iter()
        .fold(0, |sum, card| {
            let n_wins = card.n_winning_numbers();
            if let Some(prod) = (1 ..= n_wins).reduce(|prod, _| prod * 2) {
                sum + prod
            } else {
                sum
            }
        })
}

fn task_a(lines: &[String]) {
    let cards = parse_input(lines);
    let points = calc_points(&cards);

    println!("Points: {}", points);
}

fn task_b(lines: &[String]) {
    let cards = parse_input(lines);
    let n_cards = calc_cards(cards);

    println!("# of cards: {}", n_cards);
}

fn parse_input(lines: &[String]) -> Cards {
    let mut cards = Cards::new();

    for line in lines.iter() {
        let mut card = Card::new();
        let mut numbers = line.split(&[':', '|']).skip(1);
        for num in numbers.next().unwrap().trim().split_ascii_whitespace() {
            card.add_winner(num.parse().unwrap());
        }
        for num in numbers.next().unwrap().trim().split_ascii_whitespace() {
            card.add_number(num.parse().unwrap());
        }
        cards.push(card);
    }

    cards
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


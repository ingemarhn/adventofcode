use std::{cmp::Ordering, collections::{HashSet, VecDeque}};
use itertools::Itertools;

#[derive(Clone, Debug, PartialEq)]
struct Position {
    x: usize,
    y: usize,
    z: usize,
}

impl Position {
    pub fn new(x: usize,y: usize, z: usize) -> Self {
        Self { x, y, z }
    }
}

#[derive(Clone, Debug, PartialEq)]
struct Brick {
    start: Position,
    end: Position,
    supported_bricks: HashSet<usize>,
    are_supported_by: HashSet<usize>,
}

impl Brick {
    fn new(coords: &str) -> Self {
        let mut ends = coords.split('~');
        let pos = |pos_str: &str| {
            let position = pos_str
                .split(',')
                .map(|d| d.parse().unwrap())
                .collect::<Vec<_>>();
            Position::new(position[0], position[1], position[2])
        };

        Self {
            start: pos(ends.next().unwrap()),
            end: pos(ends.next().unwrap()),
            supported_bricks: HashSet::new(),
            are_supported_by: HashSet::new(),
        }
    }

    fn update_z(&mut self, new_z: usize) {
        self.end.z = new_z + (self.end.z - self.start.z);
        self.start.z = new_z;
    }
}

fn settle_bricks(bricks: &mut [Brick]) {
    bricks.sort_by_key(|a| a.start.z);

    let mut maxy = (0, 0);
    for br in bricks.iter() {
        let e = &br.end;
        maxy = (maxy.0.max(e.x), maxy.1.max(e.y));
    }
    let mut high_z = Vec::new();
    for _ in 0 ..= maxy.0 {
        high_z.push(vec![(0, 0); maxy.1 + 1]);
    }

    let brks_len = bricks.len();
    let mut top_cubes = Vec::new();
    for ind in 0 .. brks_len {
        let mut this_highest_z = 0;
        top_cubes.clear();
        {
            let brick = bricks.get(ind).unwrap();
            for row in high_z[brick.start.x ..= brick.end.x].iter_mut() {
                for cube in row[brick.start.y ..= brick.end.y].iter_mut() {
                    if brick.start.z > 1 {
                        match cube.0.cmp(&this_highest_z) {
                            Ordering::Greater => {
                                this_highest_z = cube.0;
                                top_cubes = vec![*cube];
                            },
                            Ordering::Equal => if this_highest_z != 0 {
                                top_cubes.push(*cube)
                            },
                            Ordering::Less => (),
                        }
                    }
                }
            }

            for row in high_z[brick.start.x ..= brick.end.x].iter_mut() {
                for cube in row[brick.start.y ..= brick.end.y].iter_mut() {
                    *cube = (this_highest_z + brick.end.z - brick.start.z + 1, ind);
                }
            }
        }

        for cube in top_cubes.iter() {
            bricks[ind].are_supported_by.insert(cube.1);
            bricks[cube.1].supported_bricks.insert(ind);
        }
        let brick = bricks.get_mut(ind).unwrap();
        if this_highest_z + 1 != brick.start.z {
            brick.update_z(this_highest_z + 1);
        }
    }
}

fn find_removable(bricks: &[Brick]) -> usize {
    let mut count = 0;
    'bl: for brick in bricks {
        for supp_brick_ind in brick.supported_bricks.iter() {
            if bricks[*supp_brick_ind].are_supported_by.len() == 1 {
                continue 'bl;
            }
        }

        count += 1;
    }

    count
}

fn count_falling(bricks: &[Brick]) -> usize {
    let mut count = 0;

    for (i, brick) in bricks.iter().enumerate() {
        let mut disintigrated = HashSet::new();
        disintigrated.insert(i);
        let mut queue = VecDeque::from(brick.supported_bricks.iter().copied().collect_vec());
        'q: while let Some(s_brick) = queue.pop_front() {
            if disintigrated.contains(&s_brick) {
                continue;
            }
            for b in bricks[s_brick].are_supported_by.iter() {
                if !disintigrated.contains(b) {
                    continue 'q;
                }
            }
            disintigrated.insert(s_brick);
            for sb in bricks[s_brick].supported_bricks.iter() {
                if !queue.contains(sb) {
                    queue.push_back(*sb);
                }
            }
        }
        count += disintigrated.len() - 1;
    }

    count
}

fn task_a(lines: &[String]) {
    let mut bricks = parse_input(lines);
    settle_bricks(&mut bricks);
    let removable = find_removable(&bricks);

    println!("# removable bricks: {}", removable);
}

fn task_b(lines: &[String]) {
    let mut bricks = parse_input(lines);
    settle_bricks(&mut bricks);
    let falling = count_falling(&bricks);

    println!("# total falling bricks: {}", falling);
}

fn parse_input(lines: &[String]) -> Vec<Brick> {
    lines
        .iter()
        .map(|line| Brick::new(line))
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

use std::collections::HashMap;

use itertools::Itertools;

type Position = (usize, usize);
type Positions = Vec<Position>;

fn sum_lengths(positions: &Positions) -> usize {
    let mut sum = 0;

    for (n, pos1) in positions.iter().take(positions.len() - 1).enumerate() {
        for pos2 in positions.iter().skip(n + 1) {
            sum += pos2.0.abs_diff(pos1.0) + pos2.1.abs_diff(pos1.1);
        }
    }

    sum
}

fn expand_universe(lines: &[String]) -> Vec<String> {
    let (rows_with_galaxy, cols_with_galaxy) = find_galaxies(lines);

    let mut expanded_lines = lines.to_owned();
    let mut prev = expanded_lines[0].len();
    for c in cols_with_galaxy.iter().rev() {
        for _ in 1 .. prev - *c {
            for line in expanded_lines.iter_mut() {
                line.insert(*c + 1, '.');
            }
        }
        prev = *c;
    }

    prev = expanded_lines.len();
    for r in rows_with_galaxy.iter().rev() {
        for _ in 1 .. prev - *r {
            expanded_lines.insert(*r + 1, expanded_lines[*r + 1].clone());
        }
        prev = *r;
    }

    expanded_lines
}

fn find_galaxies(lines: &[String]) -> (Vec<usize>, Vec<usize>) {
    let mut cols_with_galaxy = Vec::new();
    let mut rows_with_galaxy = Vec::new();

    for (r, line) in lines.iter().enumerate() {
        for (c, ch) in line.chars().enumerate() {
            if ch == '#' {
                if !rows_with_galaxy.contains(&r) { rows_with_galaxy.push(r); }
                if !cols_with_galaxy.contains(&c) { cols_with_galaxy.push(c); }
            }
        }
    }
    cols_with_galaxy.sort();
    rows_with_galaxy.sort();

    (rows_with_galaxy, cols_with_galaxy)
}

fn expand_positions(positions_in: &Positions, multiply_with: usize) -> Positions {
    let mut pos_rows = HashMap::new();
    let mut pos_columns = HashMap::new();
    for pos in positions_in.iter() {
        pos_rows.entry(pos.0).and_modify(|p: &mut Vec<usize>| p.push(pos.1)).or_insert(vec![pos.1]);
        pos_columns.entry(pos.1).and_modify(|p: &mut Vec<usize>| p.push(pos.0)).or_insert(vec![pos.0]);
    }
    let mut cols_with_galaxy = pos_columns.keys().copied().collect_vec();
    let mut rows_with_galaxy = pos_rows.keys().copied().collect_vec();
    cols_with_galaxy.sort();
    rows_with_galaxy.sort();

    let n_rows = *rows_with_galaxy.last().unwrap() + 1;
    let mut expand_by = (multiply_with - 1) * (n_rows - rows_with_galaxy.len());
    let mut prev = n_rows;
    for r in rows_with_galaxy.iter().rev() {
        expand_by -= (multiply_with - 1) * (prev - 1 - *r);
        let cols = pos_rows.remove(r).unwrap();
        let new_key = *r + expand_by;
        pos_rows.insert(new_key, cols);

        for c in pos_rows[&new_key].iter() {
            pos_columns.entry(*c).and_modify(|e| for cr in e.iter_mut() { if *cr == *r { *cr += expand_by }});
        }
        prev = *r;
    }

    let n_cols = *cols_with_galaxy.last().unwrap() + 1;
    expand_by = (multiply_with - 1) * (n_cols - cols_with_galaxy.len());
    prev = n_cols;
    for r in cols_with_galaxy.iter().rev() {
        expand_by -= (multiply_with - 1) * (prev - 1 - *r);
        let cols = pos_columns.remove(r).unwrap();
        let new_key = *r + expand_by;
        pos_columns.insert(new_key, cols);

        prev = *r;
    }

    pos_columns
        .iter()
        .map(|(c, rows)| rows.iter().map(|r| (*r, *c)).collect_vec())
        .concat()
}

fn task_a(lines: &[String]) {
    let expanded_lines = expand_universe(lines);
    let positions = parse_input(&expanded_lines);
    let sum = sum_lengths(&positions);

    println!("Sum lengths: {}", sum);
}

fn task_b(lines: &[String]) {
    let positions = parse_input(lines);
    let positions = expand_positions(&positions, 1_000_000);
    let sum = sum_lengths(&positions);

    println!("Sum lengths: {}", sum);
}

fn parse_input(lines: &[String]) -> Positions {
    lines
        .iter()
        .enumerate()
        .map(|(r, line)|
            line
            .chars()
            .enumerate()
            .filter(|(_, p_elem)|
                *p_elem == '#'
            )
            .map(|(c, _)| (r, c))
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
        .concat()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
mod tests23_11 {
    use super::*;

    #[test]
    fn tests23_11_t0() {
        std::env::set_current_dir("src").unwrap();
        let input = common::get_input_from_file("11-1".to_string());
        let positions = parse_input(&input);
        let positions = expand_positions(&positions, 100);
        let sum = sum_lengths(&positions);

        println!("sum = {}", sum);
    }
}


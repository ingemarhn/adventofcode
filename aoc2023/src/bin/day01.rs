type Digits = Vec<Vec<u32>>;

fn calc_sum(digits: &Digits) -> u32 {
    digits.iter().fold(0, |summ, v|
        summ + v[0] * 10 + v[v.len() - 1]
    )
}

fn task_a(lines: &[String]) {
    let digits = parse_input(lines);
    let sum = calc_sum(&digits);

    println!("Sum = {}", sum);
}

fn task_b(lines: &[String]) {
    let digits = parse_input_b(lines);
    let sum = calc_sum(&digits);

    println!("Sum = {}", sum);
}

fn parse_input(lines: &[String]) -> Digits {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .filter_map(|chr|
                chr.to_digit(10)
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

fn parse_input_b(lines: &[String]) -> Digits {
    lines
        .iter()
        .map(|orgline| {
            let fig_repls = vec![("seven", "7"), ("eight", "8"), ("three", "3"), ("four", "4"), ("five", "5"), ("nine", "9"), ("two", "2"), ("one", "1"), ("six", "6")];

            let mut nline = orgline.clone();
            let mut ind = 0;
            'w1:
            while ind < nline.len() - 1 {
                for (str, dig) in fig_repls.iter() {
                    if nline[ind..].starts_with(str) {
                        nline = nline.replacen(str, dig, 1);
                        break 'w1;
                    }
                }
                ind += 1;
            }

            ind = nline.len();
            'w2:
            while ind > 0 {
                for (str, dig) in fig_repls.iter() {
                    if str.len() > ind {
                        continue;
                    }
                    let sind = ind - str.len();
                    if nline[sind .. ind] == **str {
                        let slc = nline[ind - 1..].as_mut_ptr();
                        unsafe {
                            *slc = dig.chars().next().unwrap() as u8;
                        }
                        break 'w2;
                    }
                }
                ind -= 1;
            }

            nline
            .chars()
            .filter_map(|chr|
                chr.to_digit(10)
            )
            .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


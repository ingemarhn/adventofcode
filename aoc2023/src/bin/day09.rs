use itertools::Itertools;

fn get_sum(history: &[Vec<i64>], add_to_end: bool) -> i64 {
    history
        .iter()
        .map(|hist| {
            let mut diffs = vec![hist.iter().skip(1).enumerate().map(|(i, v)| v - hist[i]).collect_vec()];
            while diffs.last().unwrap().iter().any(|&d| d != 0) {
                let last = diffs.last().unwrap();
                let this_diffs = last.iter().skip(1).enumerate().map(|(i, v)| v - last[i]).collect_vec();
                diffs.push(this_diffs);
            }
            let fn_mod_val = if add_to_end { i64::saturating_add } else { i64::saturating_sub };
            fn_mod_val(
                *if add_to_end {
                    hist.last()
                } else {
                    hist.first()
                }
                .unwrap()
                ,
                diffs.iter().rev().skip(1).fold(0, |acc, dfs|
                    fn_mod_val(
                        *if add_to_end {
                            dfs.last()
                        } else {
                            dfs.first()
                        }
                        .unwrap()
                        ,
                        acc
                    )
                )
            )
        })
        .sum::<i64>()
}

fn task_a(lines: &[String]) {
    let all_history = parse_input(lines);
    let sum = get_sum(&all_history, true);

    println!("Sum = {}", sum);
}

fn task_b(lines: &[String]) {
    let all_history = parse_input(lines);
    let sum = get_sum(&all_history, false);

    println!("Sum = {}", sum);
}

fn parse_input(lines: &[String]) -> Vec<Vec<i64>> {
    lines
        .iter()
        .map(|line|
            line
            .split(' ')
            .map(|p_elem|
                p_elem
                .parse::<i64>()
                .unwrap()
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


use std::collections::{HashMap, HashSet};
use rand::Rng;

type VertixRep = i32;
type Vertices = Vec<VertixRep>;
type VerticeMap = HashMap<VertixRep, Vertices>;
type Subsets = HashMap<i32, Subset>;

#[derive(Clone, PartialEq)]
struct Edge {
    start: VertixRep,
    end: VertixRep,
}

#[derive(Clone)]
struct Graph {
    n_vertices: usize,
    edges: Vec<Edge>,
}

struct Subset {
    parent: VertixRep,
    rank: usize,
}

use std::fmt;
impl fmt::Debug for Edge {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "Edge {{ start: \"{}\", end: \"{}\" }}", trn(&self.start), trn(&self.end))
    }
}
impl fmt::Debug for Graph {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        // Graph { n_vertices: 2, edges: [
        write!(formatter, "Graph {{ n_vertices: {}, edges: [\n    ", self.n_vertices)?;
        for (i, e) in self.edges.iter().enumerate() {
            write!(formatter, "{:?}", e)?;
            if i < self.edges.len() - 1 {
                write!(formatter, ", ")?;
            }
            if (i + 1) % 4 == 0 {
                writeln!(formatter)?;
                if i < self.edges.len() - 1 {
                    write!(formatter, "    ")?;
                }
            }
        }
        if self.edges.len() % 4 != 0 {
            writeln!(formatter)?;
        }
        write!(formatter, "] }}")
    }
}
impl fmt::Debug for Subset {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "Subset {{ parent: \"{}\", rank: {} }}", trn(&self.parent), self.rank)
    }
}

fn find(subsets: &mut Subsets, i: VertixRep) -> VertixRep {
	let parent = subsets[&i].parent;
	if parent != i {
        let new_parent = find(subsets, parent);
        subsets.entry(i).and_modify(|e| e.parent = new_parent);
    }

	subsets[&i].parent
}

fn union(subsets: &mut Subsets, x: VertixRep, y: VertixRep) {
    let xroot = find(subsets, x);
    let yroot = find(subsets, y);

    match subsets[&xroot].rank.cmp(&subsets[&yroot].rank) {
        std::cmp::Ordering::Greater => subsets.entry(yroot).and_modify(|e| e.parent = xroot),
        std::cmp::Ordering::Less => subsets.entry(xroot).and_modify(|e| e.parent = yroot),
        std::cmp::Ordering::Equal => {
            subsets.entry(yroot).and_modify(|e| e.rank += 1);
            subsets.entry(xroot).and_modify(|e| e.parent = yroot)
        }
    };
}

/*
Testa Karger's algoritm
ChatGPT:
    Vad är kontraktion?

    När vi kontraherar en kant, till exempel (A,B):
        Vi slår samman noderna A och B till en ny supernod, som vi kan kalla AB.
        Alla kanter som tidigare var kopplade till A eller B kopplas nu till AB.
        Självloopar (kanter som går tillbaka till samma supernod, t.ex. (AB,AB)) tas bort, eftersom de inte bidrar till grafens struktur.

        Utgångspunkten är listan över kanter:
            (A,B),(B,C),(C,D),(D,E),(A,E),(B,D),(C,E)
        Efter kontraktionen av (A, B) är listan över kanter:
            (AB,C),(C,D),(D,E),(AB,E),(AB,D),(C,E)
---

Välj slumpmässigt vilka noder som ska kontraheras
Kör kontraktionen tills bara två supernoder återstår
Behåll multipla kanter mellan supernoder
Kör flera gånger tills det återstår tre kanter mellan supernoderna (det borde bara hända en gång)

Algorithm
Step 1 − Choose any random edge [u, v] from the graph G to be contracted.
Step 2 − Merge the vertices to form a supernode and connect the edges of the other adjacent nodes of the vertices to the supernode formed.
         Remove the self nodes, if any.
Step 3 − Repeat the process until there’s only two nodes left in the contracted graph.
Step 4 − The edges connecting these two nodes are the minimum cut edges.

See: https://www.geeksforgeeks.org/introduction-and-implementation-of-kargers-algorithm-for-minimum-cut/
*/

fn trn(vertix: &VertixRep) -> String {
    let vtx = vertix.unsigned_abs();
    if vtx > 255 {
        [
            vtx as u8 as char,
            (vtx >> 8) as u8 as char,
            (vtx >> 16) as u8 as char,
        ]
        .iter()
        .collect()
    } else {
        (vtx as u8 as char).to_string()
    }
}

fn build_graph(vertices: &VerticeMap) -> Graph {
    let mut graph = Graph { n_vertices: 0, edges: Vec::new() };
    let mut vertix_ids = HashSet::new();
    for vertix in vertices {
        vertix_ids.insert(*vertix.0);
        for nb in vertix.1 {
            vertix_ids.insert(*nb);
            graph.edges.push(Edge { start: *vertix.0, end: *nb });
        }
    }
    graph.n_vertices = vertix_ids.len();

    graph
}

fn split_group(graph: &Graph, vertice_map: &VerticeMap) -> Option<usize> {
    let mut graph_c = graph.clone();
    let mut rnd = rand::thread_rng();

    let mut subsets = vertice_map.iter().map(|(&v, _)| (v, Subset { parent: v, rank:  0})).collect::<HashMap<_, _>>();
    for v in vertice_map.iter() {
        for n in v.1 {
            if !subsets.contains_key(n) {
                subsets.insert(*n, Subset { parent: *n, rank: 0 });
            }
        }
    }

    let mut n_vertices = graph.n_vertices;
    while n_vertices > 2 {
        let ind = rnd.gen_range(0 .. graph_c.edges.len());
        let edge = graph_c.edges.remove(ind);

        let subset1 = find(&mut subsets, edge.start);
        let subset2 = find(&mut subsets, edge.end);

        if subset1 == subset2 {
            continue;
        }

        union(&mut subsets, subset1, subset2);
        n_vertices -= 1;
    }

    let mut cutedges = Vec::new();
    for e in graph_c.edges.iter() {
        let subset1 = find(&mut subsets, e.start);
        let subset2 = find(&mut subsets, e.end);

        if subset1 != subset2 {
            cutedges.push(e.clone());
            if cutedges.len() > 3 {
                break;
            }
        }
    }

    if cutedges.len() != 3 {
        return None
    }

    let mut v_map = VerticeMap::new();
    for i in 0 .. graph.edges.len() {
        if !cutedges.contains(&graph.edges[i]) {
            let s: i32 = graph.edges[i].start;
            let e = graph.edges[i].end;
            v_map.entry(s).and_modify(|ent| if !ent.contains(&e) { ent.push(e) }).or_insert(vec![e]);
            v_map.entry(e).and_modify(|ent| if !ent.contains(&s) { ent.push(s) }).or_insert(vec![s]);
        }
    }

    let mut queue = vec![cutedges[0].start];
    let mut visited = vec![cutedges[0].start];

    while let Some(n) = queue.pop() {
        for nb in v_map[&n].iter() {
            if visited.contains(nb) {
                continue;
            }

            queue.push(*nb);
            visited.push(*nb);
        }
    }

    Some(visited.len() * (graph.n_vertices - visited.len()))
}

fn __split_group(graph: &Graph, vertice_map: &VerticeMap) -> Option<usize> {
    let mut graph_c = graph.clone();
    let mut vertices_c = vertice_map.clone();
    let mut rnd = rand::thread_rng();

    while graph_c.n_vertices > 2 {
        let ind = rnd.gen_range(0 .. graph_c.n_vertices);
        let edge = graph_c.edges.remove(ind);
        // Remove duplicate edges (otherwise they'll be edges with same start and end)
        while let Some(eind) = graph_c.edges.iter().position(|e|
            e.start == edge.start && e.end == edge.end ||
            e.start == edge.end   && e.end == edge.start
        ) {
            graph_c.edges.remove(eind);
        }
        // Remove edges between start and end vertices (duplicates may exist)
        while let Some(pos) = vertices_c[&edge.start].iter().position(|v| *v == edge.end) {
            let Some(nbrs) = vertices_c.get_mut(&edge.start) else {panic!()};
            nbrs.remove(pos);
        }

        // Redirect edges from the removed vertix to the super vertix
        for e in graph_c.edges.iter_mut() {
            if e.start == edge.end {
                e.start = edge.start;
            } else if e.end == edge.end {
                e.end = edge.start;
            }
if e.start == e.end { panic!("{:?}", e); }
        }

        // Move end vertix's neighbors to start vertix
        if vertices_c.contains_key(&edge.end) {
            for nind in 0 .. vertices_c[&edge.end].len() {
                let nbr = vertices_c[&edge.end][nind];
                if nbr != edge.start {
                    vertices_c.entry(edge.start).and_modify(|e| e.push(nbr));
                }
            }
        } else {
            for (_i, nbrs) in vertices_c.iter_mut() {
                for nbr in nbrs {
                    if *nbr == edge.end {
                        *nbr = edge.start;
                    }
                }
            }
        }

        vertices_c.remove(&edge.end);
        graph_c.n_vertices -= 1;
    }

    if graph_c.edges.len() == 3 {
eprintln!("{:?}", graph_c.edges);
        Some(0)
    } else {
eprint!("{} ", graph_c.edges.len());
        None
    }
}

fn _split_group(nodes: &VerticeMap) -> usize {
    let mut rem_connections = Vec::new();
    let mut bridge_nodes = HashMap::new();
    for node in nodes {
        for nb in node.1 {
            if nodes[nb].iter().any(|nnb| node.1.contains(nnb)) {
                continue;
            }

            let n0 = *node.0;
            let nb = *nb;
            let key = if n0 < nb { [n0, nb] } else { [nb, n0] };
            if rem_connections.contains(&key) {
                continue;
            }
            rem_connections.push(key);
            bridge_nodes.entry(key[0]).and_modify(|e| *e += 1).or_insert(1);
            bridge_nodes.entry(key[1]).and_modify(|e| *e += 1).or_insert(1);
        }
    }

    // rem_connections.sort_unstable_by(|a, b| nodes[&b[0]].len().cmp(&nodes[&a[0]].len()).then(nodes[&b[1]].len().cmp(&nodes[&a[1]].len())));
if common::sample_input_used() {
    println!("rem_connections = {:?}", rem_connections.iter().map(|e| [trn(&e[0]).clone(), trn(&e[1]).clone()]).collect::<Vec<_>>());
}
println!("n_removals = {}", rem_connections.len());

    let mut valid_connections = vec![true; rem_connections.len()];
    'rem: for (i, conn) in rem_connections.iter().enumerate() {
        let mut queue = Vec::new();
        let mut visited = vec![conn[0]];
        for n in nodes[&conn[0]].iter() {
            if !bridge_nodes.contains_key(n) {
                queue.push(*n);
                visited.push(*n);
            }
        }
if common::sample_input_used() {
    println!("testing rem_connection = {:?}", [trn(&conn[0]), trn(&conn[1])]);
    println!("queue = {:?}", queue.iter().map(trn).collect::<Vec<_>>());
    println!("visited = {:?}", visited.iter().map(trn).collect::<Vec<_>>());
}

        while let Some(n) = queue.pop() {
            for nb in nodes[&n].iter() {
                if visited.contains(nb) || bridge_nodes.contains_key(nb) {
                    continue;
                }
                if nodes[nb].contains(&conn[1]) {
                    valid_connections[i] = false;
                    continue 'rem;
                }

                queue.push(*nb);
                visited.push(*nb);
            }
        }
    }

println!("valid_connections = {:?}", valid_connections);
println!("rem_connections:");
println!("n valid: {}", valid_connections.iter().enumerate().filter(|x| *x.1).count());
if !valid_connections.is_empty() {
    return 0
}

    let mut tnodes = nodes.clone();
let mut c=0;
    let mut product = 0;
    for (i, n1) in rem_connections.iter().enumerate().take(rem_connections.len() - 2) {
        for (j, n2) in rem_connections.iter().enumerate().skip(i + 1).take(rem_connections.len() - (i + 1)) {
            'f3: for n3 in rem_connections.iter().skip(j + 1) {
c+=1; if c%1000==0 {eprint!("{} ",c);}; if c%50000==0 {eprintln!()}
                for n in [n1, n2, n3] {
                    tnodes.entry(n[0]).and_modify(|e| { let p = e.iter().position(|s| s.abs() == n[1].abs()).unwrap(); e[p] *= -1; });
                    tnodes.entry(n[1]).and_modify(|e| { let p = e.iter().position(|s| s.abs() == n[0].abs()).unwrap(); e[p] *= -1; });
                }
let exp = if common::sample_input_used() {
let x=[6583918, 7631210, 6452834, 6778211, 7109232, 7890536].iter().all(|x| [*n1, *n2, *n3].concat().contains(x));
if x {
    println!("[6583918, 7631210, 6452834, 6778211, 7109232, 7890536]");
    for tn in &tnodes {
        println!("{}: {:?}", tn.0, tn.1);
    }
}
x
} else {false};
                let mut queue = vec![n3[1]];
                let mut visited = vec![n3[1]];
                while let Some(n) = queue.pop() {
                    if n < 0 {
                        // The bridge is broken
                        continue;
                    }
                    if n == n3[0] {
                        // We've found another way ...
                        for n in [n1, n2, n3] {
                            tnodes.entry(n[0]).and_modify(|e| { let p = e.iter().position(|s| s.abs() == n[1].abs()).unwrap(); e[p] *= -1; });
                            tnodes.entry(n[1]).and_modify(|e| { let p = e.iter().position(|s| s.abs() == n[0].abs()).unwrap(); e[p] *= -1; });
                        }

                        continue 'f3;
                    }

                    for nb in tnodes[&n].iter() {
                        if *nb < 0 && n < 0 {
                            continue;
                        }
                        if !visited.contains(nb) {
                            queue.push(*nb);
                            visited.push(*nb);
                        }
                    }
if exp {
println!("queue = {:?}", queue);
}
                }
println!("visited.len() = {}", visited.len());
                product = visited.len() * (nodes.len() - visited.len());

                for n in [n1, n2, n3] {
                    tnodes.entry(n[0]).and_modify(|e| { let p = e.iter().position(|s| s.abs() == n[1].abs()).unwrap(); e[p] *= -1; });
                    tnodes.entry(n[1]).and_modify(|e| { let p = e.iter().position(|s| s.abs() == n[0].abs()).unwrap(); e[p] *= -1; });
                }
            }
        }
    }
product
}

fn task_a(lines: &[String]) {
    let vertices = parse_input(lines);
    let graph = build_graph(&vertices);
    let prod = loop {
        if let Some(p) = split_group(&graph, &vertices) {
            break p;
        }
    };

    println!("Groups product: {}", prod);
}

fn task_b(_lines: &[String]) {
    println!("No real task B on day 25");
}

fn parse_input(lines: &[String]) -> VerticeMap {
    let mut nodes = VerticeMap::new();
    let str_to_i = |s: &str| {
        let int = s.chars().enumerate().fold(0, |acc,(i, c)| {
            acc + ((c as u8 as VertixRep) << (i * 8))
        });
        int
    };

    for line in lines {
        let mut parts = line.split(&[' ', ':'][..]);
        let k_node = str_to_i(parts.next().unwrap());
        parts.next();
        let neighbors = parts.map(|n| {
            str_to_i(n)
        }).collect::<Vec<_>>();
        nodes
            .entry(k_node)
            .and_modify(|e| for nb in &neighbors {
                if !e.contains(nb) {
                    e.push(*nb);
                }
            })
            .or_insert(neighbors);
    }

    nodes
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests23_25 {
    use super::*;

    #[test]
    fn tests23_25_t0() {}
}


type Position = (usize, usize);

fn get_neighbors(lines: &[String], start: Position, end: usize) -> Vec<Position> {
    let mut neighbors = Vec::new();
    if start.0 > 0 {
        let line = start.0 - 1;
        if start.1 > 0 { neighbors.push((line, start.1 - 1)); }
        for i in start.1 ..= end {
            neighbors.push((line, i));
        }
        if end < lines[0].len() { neighbors.push((line, end + 1)); }
    }
    if start.1 > 0 { neighbors.push((start.0, start.1 - 1)); }
    if end < lines[0].len() { neighbors.push((start.0, end + 1)); }
    if start.0 < lines.len() - 1 {
        let line = start.0 + 1;
        if start.1 > 0 { neighbors.push((line, start.1 - 1)); }
        for i in start.1 ..= end {
            neighbors.push((line, i));
        }
        if end < lines[0].len() { neighbors.push((line, end + 1)); }
    }

    neighbors
}

fn task_a(lines: &[String]) {
    let sum = lines
        .iter()
        .enumerate()
        .fold(0, |mut sum, (lnum, line)| {
            let mut line_it = line.chars().enumerate();
            while let Some((cnum, c)) = line_it.next() {
                if let Some(mut dig) = c.to_digit(10) {
                    let start = cnum;
                    let (value, end) = loop {
                        if let Some((cnum, c)) = line_it.next() {
                            if let Some(ndig) = c.to_digit(10) {
                                dig = dig * 10 + ndig;
                            } else {
                                break (dig, cnum - 1);
                            }
                        } else {
                            break (dig, cnum - 1);
                        }
                    };

                    let neighbors = get_neighbors(lines, (lnum, start), end);
                    let mut is_part_num = false;
                    for nbr in neighbors {
                        let chr = common::char_from_string!(lines[nbr.0], nbr.1);
                        if !chr.is_ascii_alphanumeric() && chr != '.' {
                            is_part_num = true;
                            break;
                        }
                    }
                    if is_part_num {
                        sum += value;
                    }
                }
            }
            sum
        });

    println!("Sum = {}", sum);
}

fn task_b(lines: &[String]) {
    let is_digit = |c: u8| (0x30 .. 0x40).contains(&c);
    let sum = lines
        .iter()
        .enumerate()
        .fold(0, |mut sum, (lnum, line)| {
            for (cnum, c) in line.chars().enumerate() {
                if c == '*' {
                    let neighbors = get_neighbors(lines, (lnum, cnum), cnum);
                    let mut nbr_numbers: Vec<((usize, usize), usize, u32)> = Vec::new();
                    'f: for (y, x) in neighbors {
                        let line_bytes = lines[y].as_bytes();
                        if is_digit(line_bytes[x]) {
                            for (start, end, _) in nbr_numbers.iter() {
                                if y == start.0 && (start.1 ..= *end).contains(&x) {
                                    continue 'f;
                                }
                            }
                            let mut start = x;
                            for i in (0 .. start).rev() {
                                if !is_digit(line_bytes[i]) {
                                    break;
                                }
                                start = i;
                            }
                            let mut value = 0;
                            let mut end = 0;
                            for (x, b) in line_bytes[start .. ].iter().enumerate() {
                                if is_digit(*b) {
                                    value = value * 10 + (b - 0x30) as u32;
                                    end = start + x
                                } else {
                                    break;
                                }
                            }
                            nbr_numbers.push(((y, start), end, value));
                        }
                    }
                    if nbr_numbers.len() == 2 {
                        sum += nbr_numbers[0].2 * nbr_numbers[1].2;
                    }
                }
            }
            sum
        });

    println!("Sum = {}", sum);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


use itertools::Itertools;

type Races = Vec<Race>;

struct Race {
    time: u128,
    distance: u128,
}

fn task_a(lines: &[String]) {
    let races = parse_input(lines);
    let mut better = Vec::new();
    for race in races {
        let mut it1 = (1 .. race.time).skip_while(|i| (race.time - i) * i <= race.distance);
        let mut it2 = (1 .. race.time - 1).rev().skip_while(|i| (race.time - i) * i <= race.distance);
        better.push( it2.next().unwrap() - it1.next().unwrap() + 1 );
    }

    let prod: u128 = better.iter().product();
    println!("{}", prod);
}

fn task_b(lines: &[String]) {
    let races = parse_input(lines);
    let (time, distance) = races.iter().fold((0, 0), |s, e|
        (
            s.0 * 10_u128.pow(e.time.ilog10()) * 10 + e.time,
            s.1 * 10_u128.pow(e.distance.ilog10()) * 10 + e.distance
        )
    );

    let mut it1 = (1 .. time).skip_while(|i| (time - i) * i <= distance);
    let mut it2 = (1 .. time - 1).rev().skip_while(|i| (time - i) * i <= distance);

    println!("{}", it2.next().unwrap() - it1.next().unwrap() + 1);
}

fn parse_input(lines: &[String]) -> Races {
    let split = |i: usize|
        lines[i]
        .split(':')
        .nth(1)
        .unwrap()
        .trim()
        .split_ascii_whitespace()
        .map(|n| n.parse::<u128>().unwrap())
        .collect_vec()
    ;
    let times = split(0);
    let distances = split(1);
    let mut races = Vec::new();
    for i in 0 .. times.len() {
        races.push( Race { time: times[i], distance: distances[i] } );
    }

    races
}


fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


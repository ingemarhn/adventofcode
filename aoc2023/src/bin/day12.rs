use std::collections::{HashMap, VecDeque};
use itertools::Itertools;

type Springs = Vec<Spring>;

#[derive(Clone, Debug, PartialEq)]
enum Spring {
    Operational,
    Damaged,
    Unknown,
}

#[derive(Clone, Debug)]
struct Row {
    springs: Springs,
    damaged_start_positions: Vec<usize>,
    n_contiguous_damaged: Vec<usize>,
}

impl Row {
    pub fn new() -> Self {
        Self { springs: Vec::new(), damaged_start_positions: Vec::new(), n_contiguous_damaged: Vec::new() }
    }
}

type Field = Vec<Row>;
type Cache = HashMap<(usize, usize), usize>;

#[derive(Clone, Debug, PartialEq)]
enum CombinationResult {
    Combination(VecDeque<usize>),
    Skipped,
    Nothing,
}

fn find_possible_damaged_location(springs: &Springs, start: usize, damaged_len: usize) -> Option<usize> {
    if start >= springs.len() {
        return None;
    }

    let mut curr_pos = start;
    let mut damaged_start = start;
    'o: while curr_pos - damaged_start < damaged_len {
        for (i, s) in springs.iter().enumerate().skip(damaged_start) {
            match *s {
                Spring::Damaged |
                Spring::Unknown => curr_pos += 1,
                Spring::Operational => {
                    damaged_start = i + 1;
                    curr_pos = damaged_start;
                    continue 'o;
                },
            }

            if curr_pos - damaged_start == damaged_len {
                break;
            }
        }

        if curr_pos - damaged_start < damaged_len {
            return None;
        }

        if (damaged_start > 0 && springs[damaged_start - 1] == Spring::Damaged) || (curr_pos < springs.len() && springs[curr_pos] == Spring::Damaged) {
            damaged_start += 1;
            curr_pos = damaged_start;
            continue;
        }

        break;
    }

    Some(damaged_start)
}

fn check_damaged_after_last_pos(row: &Row, start_positions: &[usize], n_pos_to_test: usize) -> bool {
    if !row.damaged_start_positions.is_empty() {
        let start_pos_last = *start_positions.last().unwrap();
        if n_pos_to_test == row.n_contiguous_damaged.len() &&
           start_pos_last + row.n_contiguous_damaged.last().unwrap() < *row.damaged_start_positions.last().unwrap() {
            return false;
        }
    }

    true
}

fn get_a_combination(row: &Row, start: usize, dam_ind: usize, check_pos_start: usize) -> CombinationResult {
    if dam_ind >= row.n_contiguous_damaged.len() {
        return CombinationResult::Combination(VecDeque::new());
    }

    let Some(pos) = find_possible_damaged_location(&row.springs, start, row.n_contiguous_damaged[dam_ind]) else {
        return CombinationResult::Nothing
    };

    for sta in row.damaged_start_positions.iter() {
        if *sta >= pos {
            break;
        }
        if (check_pos_start .. pos).contains(sta) {
            return CombinationResult::Skipped;
        }
    }

    let result = get_a_combination(row, pos + row.n_contiguous_damaged[dam_ind] + 1, dam_ind + 1, pos + row.n_contiguous_damaged[dam_ind]);
    if let CombinationResult::Combination(mut positions) = result {
        positions.push_front(pos);

        CombinationResult::Combination(positions)
    } else if result == CombinationResult::Skipped {
        get_a_combination(row, pos + 1, dam_ind, check_pos_start)
    } else {
        result
    }
}

fn get_damaged_possibilities_row(row: &Row, start_positions_first: &[usize], dam_ind_in: usize, start: usize, cache: &mut Cache) -> usize {
    let key = (start, dam_ind_in);
    let test_pos = [start_positions_first.to_vec(), vec![start]].concat();
    if let Some(n_comb) = cache.get(&key) {
        if check_damaged_after_last_pos(row, &test_pos, test_pos.len()) {
            return *n_comb;
        }
    }

    let last_damaged = if let Some(last_pos) = start_positions_first.last() {
        last_pos + row.n_contiguous_damaged[dam_ind_in - 1] + 1
    } else {
        0
    };
    let start_posns = get_a_combination(row, start, dam_ind_in, last_damaged);
    if start_posns == CombinationResult::Nothing {
        return 0;
    }

    let CombinationResult::Combination(start_positions) = start_posns else {
        return 0;
    };
    let start_positions = Vec::from(start_positions);
    let all_start_positions = [start_positions_first, &start_positions].concat();
    let mut n_combinations = 0;
    if check_damaged_after_last_pos(row, &all_start_positions, all_start_positions.len()) {
        n_combinations += 1;
    }

    for (i, start_position) in start_positions.iter().rev().enumerate() {
        let dam_ind = row.n_contiguous_damaged.len() - 1 - i;
        let next_start_pos = *start_position + 1;
        let st_pos_1st = &all_start_positions[ .. all_start_positions.len() - 1 - i ];
        n_combinations += get_damaged_possibilities_row(row, st_pos_1st, dam_ind, next_start_pos, cache);
    }

    if n_combinations > 0 {
        cache.insert(key, n_combinations);
    }
    n_combinations
}

fn get_damaged_possibilities(field: &Field) -> usize {
    let mut n_combinations = 0;

    for row in field {
        let mut cache = Cache::new();
        n_combinations += get_damaged_possibilities_row(row, &[], 0, 0, &mut cache);
    }

    n_combinations
}

fn task_a(lines: &[String]) {
    let field = parse_input(lines);
    let n_combs = get_damaged_possibilities(&field);

    println!("Sum of arrangements: {}", n_combs);
}

fn task_b(lines: &[String]) {
    let lines5 = lines.iter().map(|l| {
        let pts = l.split(' ').collect_vec();
        let p1 = [pts[0], pts[0], pts[0], pts[0], pts[0]].join("?");
        let p2 = [pts[1], pts[1], pts[1], pts[1], pts[1]].join(",");
        [p1, p2].join(" ")
    })
    .collect_vec();
    let field = parse_input(&lines5);
    let n_combs = get_damaged_possibilities(&field);

    println!("Sum of arrangements: {}", n_combs);
}

fn parse_input(lines: &[String]) -> Field {
    let mut field = Field::new();
    for line in lines {
        let mut row = Row::new();
        let mut parts = line.split_whitespace();
        let mut pos = usize::MAX;
        for (i, c) in parts.next().unwrap().chars().enumerate() {
            match c {
                '.' => row.springs.push(Spring::Operational),
                '#' => row.springs.push(Spring::Damaged),
                '?' => row.springs.push(Spring::Unknown),
                _ => panic!("c={}", c)
            }
            if c == '#' && pos == usize::MAX {
                row.damaged_start_positions.push(i);
                pos = i;
            } else if c != '#' {
                pos = usize::MAX;
            }
        }

        for c in parts.next().unwrap().split(',') {
            row.n_contiguous_damaged.push(c.parse().unwrap());
        }

        field.push(row);
    }

    field
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


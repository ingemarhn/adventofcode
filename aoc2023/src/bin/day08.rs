use std::collections::HashMap;
use itertools::Itertools;
use num::integer;

fn steps(instrs: &str, map: HashMap<&str, [&str; 2]>) -> u32 {
    let mut instr_it = instrs.chars().map(|c| ((c as usize) & 0b10) >> 1).cycle();
    let mut key = "AAA";
    let mut count = 0;

    loop {
        key = map[key][instr_it.next().unwrap()];
        count += 1;
        if key == "ZZZ" {
            break;
        }
    }

    count
}

fn detect_pattern(data: &[Vec<&str>], col: usize) -> Option<(usize, usize)> {
    let get_ind = |end| {
        (0 .. end).rev().find(|i| data[*i][col].ends_with('Z'))
    };

    if let Some(ind) = get_ind(data.len()) {
        // This is incomplete, should be a loop instead. But I know that this will work here.
        if let Some(pind) = get_ind(ind) {
            let len = ind - pind;
            for i in pind - len + 1 ..= pind {
                if data[i][col] != data[i + len][col] {
                    return None
                }
            }
            return Some((ind % len, len));
        }
    }

    None
}

fn detect_patterns(keys_list: &[Vec<&str>]) -> Vec<(usize, usize)> {
    let mut pattern_inds = Vec::new();
    for c in 0 .. keys_list[0].len() {
        let Some(inds) = detect_pattern(keys_list, c) else {
            break;
        };
        pattern_inds.push(inds);
    }

    pattern_inds
}

fn steps_b(instrs: &str, map: HashMap<&str, [&str; 2]>) -> usize {
    let instr_inds = instrs.chars().map(|c| ((c as usize) & 0b10) >> 1).collect_vec();
    let mut instr_it = instr_inds.iter().cycle();
    let mut count = 0;
    let mut keys = Vec::new();
    for key in map.keys() {
        if key.ends_with('A') {
            keys.push(*key);
        }
    }

    let mut keys_list = Vec::new();
    loop {
        keys_list.push(keys.clone());
        let ind = *instr_it.next().unwrap();
        count += 1;
        if count > 100000 {
            break;
        }
        for (i, old_key) in keys.clone().iter().enumerate() {
            let new_key = map[old_key][ind];
            keys[i] = new_key;
        }
    }

    let patterns_inds = detect_patterns(&keys_list);
    patterns_inds.iter().skip(1).fold(patterns_inds[0].1, |lcm, (offs, length)| {
        if *offs != 0 {
            panic!()
        }
        integer::lcm(lcm, *length)
    })
}

fn task_a(lines: &[String]) {
    let (instrs, map) = parse_input(lines);
    let n_steps = steps(instrs, map);

    println!("# of steps = {}", n_steps);
}

fn task_b(lines: &[String]) {
    let (instrs, map) = parse_input(lines);
    let n_steps = steps_b(instrs, map);

    println!("# of steps = {}", n_steps);
}

fn parse_input(lines: &[String]) -> (&String, HashMap<&str, [&str; 2]>) {
    let instr = &lines[0];

    let map = lines[2 .. ]
        .iter()
        .map(|line| {
            let parts = line.split([' ', ',', '(', ')']).collect_vec();
            (parts[0], [parts[3], parts[5]])
        })
        .collect::<HashMap<_, _>>();

    (instr, map)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


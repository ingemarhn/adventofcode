use num::range_step_inclusive;

type Platform = Vec<Vec<char>>;

fn get_range(length: usize, is_positive: bool) -> num::iter::RangeStepInclusive<i32> {
    if is_positive {
        range_step_inclusive((length - 2) as i32, 0, -1)
    } else {
        range_step_inclusive(1, (length - 1) as i32, 1)
    }
}

fn get_index_mod(is_positive: bool) -> fn(usize, usize) -> usize {
    if is_positive {
        usize::saturating_add
    } else {
        usize::saturating_sub
    }
}

fn get_limit(is_positive: bool, upper: usize) -> usize {
    if is_positive {
        upper - 1
    } else {
        0
    }
}

fn tilt_north_or_south(platform: &mut Platform, is_north: bool) {
    let platform_len = platform.len();
    let range = get_range(platform_len, is_north);
    let row_mod = get_index_mod(is_north);
    let limit = get_limit(is_north, platform_len);

    for ri in range {
        let r = ri as usize;
        for c in 0 .. platform[r].len() {
            if platform[r][c] == 'O' && platform[row_mod(r, 1)][c] == '.' {
                let mut rn = row_mod(r, 1);
                loop {
                    if rn == limit || platform[row_mod(rn, 1)][c] != '.' {
                        break;
                    }
                    rn = row_mod(rn, 1);
                }
                platform[rn][c] = 'O';
                platform[r][c] = '.';
            }
        }
    }
}

fn tilt_east_west(platform: &mut Platform, is_east: bool) {
    let platform_columns = platform[0].len();
    let range = get_range(platform_columns, is_east);
    let col_mod = get_index_mod(is_east);
    let limit = get_limit(is_east, platform_columns);

    for ci in range {
        let c = ci as usize;
        for platform_row in platform.iter_mut() {
            if platform_row[c] == 'O' && platform_row[col_mod(c, 1)] == '.' {
                let mut cn = col_mod(c, 1);
                loop {
                    let cnn = col_mod(cn, 1);
                    if cn == limit || platform_row[cnn] != '.' {
                        break;
                    }
                    cn = cnn;
                }
                platform_row[cn] = 'O';
                platform_row[c] = '.';
            }
        }
    }
}

fn detect_pattern(values: &[usize]) -> Option<(usize, usize)> {
    let mut ret_val = None;
    let test_value = values.last().unwrap();
    let indices = values
        .iter()
        .enumerate()
        .skip(values.len() / 2 + 1)
        .filter(|(_, v)| *v == test_value)
        .map(|(i, _)| i);

    let mut segment_len;
    let mut prev_segment_len = 0;
    for ind in indices.rev().skip(1) {
        segment_len = values.len() - 1 - ind;
        if segment_len > ind || segment_len == prev_segment_len * 2 {
            break;
        }
        prev_segment_len = segment_len;

        let mut c_iter = values.rchunks(segment_len).peekable();
        let segment = c_iter.next().unwrap();
        'l: loop {
            let seg = c_iter.next()?;
            if seg != segment {

                let diff_l = segment_len - seg.len();
                for (i, v) in seg.iter().enumerate().rev() {
                    if *v != segment[i + diff_l] {
                        ret_val = Some((i + 1 + c_iter.map(|s| s.len()).sum::<usize>(), segment_len));
                        break 'l;
                    }
                }
            }
        }
    }

    ret_val
}

fn calc_load(platform: &Platform) -> usize {
    platform
    .iter()
    .enumerate()
    .map(|(r, pl_row)| (r + 1) * pl_row.iter().filter(|c| **c == 'O').count())
    .sum()
}

fn task_a(lines: &[String]) {
    let mut platform = parse_input(lines);
    tilt_north_or_south(&mut platform, true);
    let load = calc_load(&platform);

    println!("Load = {}", load);
}

fn task_b(lines: &[String]) {
    let mut platform = parse_input(lines);
    let mut loads = Vec::new();
    loop {
        for _ in 0 .. 50 {
            tilt_north_or_south(&mut platform, true);
            tilt_east_west(&mut platform, false);
            tilt_north_or_south(&mut platform, false);
            tilt_east_west(&mut platform, true);
            loads.push(calc_load(&platform));
        }
        if let Some((start, segment_len)) = detect_pattern(&loads) {
            let n_loops = 1_000_000_000;
            let n_left = (n_loops - start) % segment_len;

            println!("Load = {}", loads[start + n_left-1]);
            break;
        }
    }
}

fn parse_input(lines: &[String]) -> Platform {
    lines
        .iter()
        .rev()
        .map(|line|
            line
            .chars()
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


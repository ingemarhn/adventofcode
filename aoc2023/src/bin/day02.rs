use regex::Regex;

fn task_a(lines: &[String]) {
    let reg = Regex::new(r"(\d+) (\w+)(,|;|$)").unwrap();

    let game_sum = lines
        .iter()
        .enumerate()
        .map(|(n, line)| {
            let caps: Vec<_> = reg.captures_iter(line).map(|m| (m.get(1).unwrap().as_str().parse::<u8>().unwrap(), m.get(2).unwrap().as_str(), m.get(3).unwrap().as_str())).collect();
            let is_valid = caps
                .iter()
                .fold(true, |is_val, cap|
                    match cap.1 {
                        "red" if cap.0 > 12 => false,
                        "green" if cap.0 > 13 => false,
                        "blue" if cap.0 > 14 => false,
                        _ => is_val
                    }
                );
            if is_valid {
                n + 1
            } else {
                0
            }
        })
        .sum::<usize>();

    println!("Game sum = {}", game_sum);
}

fn task_b(lines: &[String]) {
    let reg = Regex::new(r"(\d+) (\w+)(,|;|$)").unwrap();

    let power_sum = lines
        .iter()
        .map(|line| {
            let caps: Vec<_> = reg.captures_iter(line).map(|m| (m.get(1).unwrap().as_str().parse::<usize>().unwrap(), m.get(2).unwrap().as_str(), m.get(3).unwrap().as_str())).collect();
            let game_max = caps
                .iter()
                .fold((0, 0, 0), |mut max, cap| {
                    match cap.1 {
                        "red" if cap.0 > max.0 => max.0 = cap.0,
                        "green" if cap.0 > max.1 => max.1 = cap.0,
                        "blue" if cap.0 > max.2 => max.2 = cap.0,
                        _ => {}
                    }
                    max
                });
            game_max.0 * game_max.1 * game_max.2
        })
        .sum::<usize>();

    println!("Power sum = {}", power_sum);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


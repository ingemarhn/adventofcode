use itertools::Itertools;

fn calc_hash(chars: &[u8]) -> usize {
    let mut hash = 0u8;
    for c in chars {
        hash = hash.wrapping_add(*c);
        hash = hash.wrapping_mul(17);
    }
    hash as usize
}

fn hash_str(chars: &[u8]) -> usize {
    let chunks = chars.split(|&c| c == b',');
    chunks
        .map(|step| {
            calc_hash(step)
        })
        .sum()
}

fn focusing_power(chars: &[u8]) -> usize {
    let chunks = chars.split(|&c| c == b',');
    let mut boxes = (0 .. 256).map(|_| Vec::new()).collect_vec();
    for chunk in chunks {
        let mut ch_iter = chunk.iter();
        let chunk_chrs = ch_iter.take_while_ref(|c| c.is_ascii_alphabetic()).copied().collect_vec();
        let hash = calc_hash(&chunk_chrs);
        let this_box = &mut boxes[hash];
        let o_pos = this_box.iter().position(|l: &(Vec<u8>, &u8)| l.0 == chunk_chrs);
        if b'=' == *ch_iter.next().unwrap() {
            let lens = (chunk_chrs, ch_iter.next().unwrap());
            if let Some(pos) = o_pos {
                this_box[pos] = lens;
            } else {
                this_box.push(lens);
            }
        } else if let Some(pos) = o_pos {
            this_box.remove(pos);
        }
    }

    boxes
        .iter()
        .enumerate()
        .map(|(i, cbox)|
            cbox
                .iter()
                .enumerate()
                .map(|(bi, (_, &focal))| (i + 1) * (bi + 1) * (focal as usize - 48))
                .sum::<usize>()
        )
        .sum()
}

fn task_a(lines: &[String]) {
    let digits = parse_input(lines);
    let hash = hash_str(&digits);

    println!("hash = {}", hash);
}

fn task_b(lines: &[String]) {
    let digits = parse_input(lines);
    let fp = focusing_power(&digits);

    println!("Focusing power: {}", fp);
}

fn parse_input(lines: &[String]) -> Vec<u8> {
    lines[0]
        .chars()
        .map(|l| l as u8)
        .collect()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


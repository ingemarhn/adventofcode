use std::collections::{HashMap, HashSet, VecDeque};

type Position = (usize, usize);

#[derive(Debug, PartialEq)]
enum Tile {
    Path,
    Forest,
    SlopeN,
    SlopeE,
    SlopeS,
    SlopeW,
}

#[derive(Debug)]
struct Map {
    start: Position,
    end: Position,
    height: usize,
    width: usize,
    map: Vec<Vec<Tile>>,
}

fn find_longest_path_naive(map: &Map) -> usize {
    let neibrs = |pos: Position| {
        match map.map[pos.0][pos.1] {
            Tile::SlopeN => return vec![(pos.0 - 1, pos.1)],
            Tile::SlopeE => return vec![(pos.0, pos.1 + 1)],
            Tile::SlopeS => return vec![(pos.0 + 1, pos.1)],
            Tile::SlopeW => return vec![(pos.0, pos.1 - 1)],
            _ => {},
        }

        let p = (pos.0 as isize, pos.1 as isize);
        [(-1, 0), (0, 1), (1, 0), (0, -1)].iter().fold(Vec::new(), |mut coords, mov| {
            let pp = (p.0 + mov.0, p.1 + mov.1);
            if pp.0 >= 0 && pp.0 < map.height as isize && p.1 >= 0 && p.1 < map.width as isize {
                let ppu = (pp.0 as usize, pp.1 as usize);
                if map.map[ppu.0][ppu.1] != Tile::Forest {
                    coords.push(ppu);
                }
            }
            coords
        })
    };

    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();

    visited.insert(map.start);
    queue.push_back((map.start, Vec::new(), visited));

    let mut longest_path = 0;
    while let Some((pos, mut path, visited)) = queue.pop_back() {
        if pos == map.end {
            longest_path = longest_path.max(path.len());
            continue;
        }

        path.push(pos);

        for coord in neibrs(pos).iter() {
            if !visited.contains(coord) {
                let mut vis = visited.clone();
                vis.insert(*coord);
                queue.push_back((*coord, path.clone(), vis));
            }
        }
    }

    longest_path
}

fn find_longest_path(map: &Map) -> usize {
    let neibrs = |pos: Position, from: Position| {
        let p = (pos.0 as isize, pos.1 as isize);
        [(-1, 0), (0, 1), (1, 0), (0, -1)].iter().fold(Vec::new(), |mut coords, mov| {
            let pp = (p.0 + mov.0, p.1 + mov.1);
            if pp.0 >= 0 && pp.0 < map.height as isize && p.1 >= 0 && p.1 < map.width as isize {
                let ppu = (pp.0 as usize, pp.1 as usize);
                if map.map[ppu.0][ppu.1] != Tile::Forest && ppu != from {
                    coords.push(ppu);
                }
            }
            coords
        })
    };

    #[derive(Debug)]
    struct TileInfo {
        start: Position,
        current: Position,
        previous: Position,
        steps: usize,
    }
    let mut nodes = HashMap::new();
    nodes.insert(map.start, HashMap::new());

    let mut queue = vec![TileInfo { start: map.start, current: (map.start.0 + 1, map.start.1), previous: map.start, steps: 1 }];
    let mut visited = HashSet::new();
    visited.insert(map.start);

    while let Some(tile_info) = queue.pop() {
        if visited.contains(&tile_info.current) {
            continue;
        }

        let nbrs = neibrs(tile_info.current, tile_info.previous);
        let (start, n_steps) = if nbrs.len() > 1 || tile_info.current == map.end {
            // Cross road or end of map found
            nodes
                .entry(tile_info.start)
                .and_modify(|e| { e.insert(tile_info.current, tile_info.steps); });
            nodes
                .entry(tile_info.current)
                .and_modify(|e| { e.insert(tile_info.start, tile_info.steps); })
                .or_insert(HashMap::from([(tile_info.start, tile_info.steps)]));

            if tile_info.current == map.end {
                continue;
            }

            (tile_info.current, 1)
        } else {
            visited.insert(tile_info.current);
            (tile_info.start, tile_info.steps + 1)
        };

        for coord in nbrs.iter() {
            queue.push(TileInfo { start, current: *coord, previous: tile_info.current, steps: n_steps });
        }
    }

    let mut queue = vec![(map.start, 0, vec![map.start])];
    let mut longest_path = 0;
    while let Some((pos, path_len, visited)) = queue.pop() {
        if pos == map.end {
            longest_path = longest_path.max(path_len);
            continue;
        }

        if let Some(next_nodes) = nodes.get(&pos) {
            for (next, steps) in next_nodes {
                if !visited.contains(next) {
                    let new_path_len = path_len + *steps;
                    let mut vis = visited.clone();
                    vis.push(*next);
                    queue.push((*next, new_path_len, vis));
                }
            }
        }
    }

    longest_path
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let longest = find_longest_path_naive(&map);

    println!("Longest path: {}", longest);
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let longest = find_longest_path(&map);

    println!("Longest path: {}", longest);
}

fn parse_input(lines: &[String]) -> Map {
    let height = lines.len();
    let width = lines[0].len();
    let start = (0, lines[0].chars().position(|c| c == '.').unwrap());
    let end   = (height - 1, lines[height - 1].chars().position(|c| c == '.').unwrap());

    let map =
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|c|
                match c {
                    '.' => Tile::Path,
                    '#' => Tile::Forest,
                    '^' => Tile::SlopeN,
                    '>' => Tile::SlopeE,
                    'v' => Tile::SlopeS,
                    '<' => Tile::SlopeW,
                    _ => panic!()
                }
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>();

    Map { start, end, width, height, map }
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

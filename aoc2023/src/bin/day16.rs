use std::collections::HashSet;
use std::ops::Add;
use num::{CheckedAdd, Zero, range_step};

#[derive(Clone, Copy, Debug)]
enum Tile {
    Empty,
    MirrorForward,
    MirrorBackward,
    SplitterHorizontal,
    SplitterVertical,
}

#[derive(Clone, Debug)]
struct Contraption (Vec<Vec<(Tile, HashSet<Direction>)>>);

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

type PositionBaseType = isize;
type PositionType = (PositionBaseType, PositionBaseType);

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
struct Position (PositionType);

impl Add for Position {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self((
            self.0.0 + other.0.0,
            self.0.1 + other.0.1,
        ))
    }
}

impl CheckedAdd for Position {
    // #[inline]
    fn checked_add(&self, v: &Self) -> Option<Self> {
        Some(self.add(*v))
    }
}

impl Zero for Position {
    fn zero() -> Position {
        Position((0, 0))
    }
    fn is_zero(&self) -> bool {
        *self == Position((0, 0))
    }
}

#[derive(Debug)]
struct Beam {
    position: Position,
    direction: Direction,
}

impl Beam {
    fn new(position: PositionType, direction: Direction) -> Self {
        Self { position: Position(position), direction }
    }

    fn row(&self) -> PositionBaseType {
        self.position.0.0
    }

    fn col(&self) -> PositionBaseType {
        self.position.0.1
    }

    pub fn step(&mut self, contraption: &mut Contraption) -> (bool, Option<Direction>) {
        self.position = match self.direction {
            Direction::Up => {
                if self.row() == 0 { return (false, None) }
                Position((self.row() - 1, self.col()))
            },
            Direction::Down => {
                if self.row() as usize == contraption.0.len() - 1 { return (false, None) }
                Position((self.row() + 1, self.col()))
            },
            Direction::Left => {
                if self.col() == 0 { return (false, None) }
                Position((self.row(), self.col() - 1))
            },
            Direction::Right => {
                if self.col() as usize == contraption.0[0].len() - 1 { return (false, None) }
                Position((self.row(), self.col() + 1))
            },
        };

        // Set possible additional direction in case of split
        // Set new direction of current beam
        let mut opt_dir = None;
        match contraption.0[self.row() as usize][self.col() as usize].0 {
            Tile::Empty => {},
            Tile::MirrorForward => {
                self.direction = match self.direction {
                    Direction::Up => Direction::Right,
                    Direction::Down => Direction::Left,
                    Direction::Left => Direction::Down,
                    Direction::Right => Direction::Up,
                };
            },
            Tile::MirrorBackward => {
                self.direction = match self.direction {
                    Direction::Up => Direction::Left,
                    Direction::Down => Direction::Right,
                    Direction::Left => Direction::Up,
                    Direction::Right => Direction::Down,
                };
            },
            Tile::SplitterHorizontal => {
                if self.direction == Direction::Up || self.direction == Direction::Down {
                    self.direction = Direction::Left;
                    opt_dir = Some(Direction::Right);
                }
            },
            Tile::SplitterVertical => {
                if self.direction == Direction::Left || self.direction == Direction::Right {
                    self.direction = Direction::Up;
                    opt_dir = Some(Direction::Down);
                }
            }
        };

        let tile_in = &mut contraption.0[self.row() as usize][self.col() as usize];
        if tile_in.1.contains(&self.direction) {
            return (false, opt_dir)
        }
        tile_in.1.insert(self.direction);
        if  (self.direction == Direction::Up    && self.row() == 0) ||
            (self.direction == Direction::Down  && self.row() == (contraption.0.len() - 1) as PositionBaseType) ||
            (self.direction == Direction::Left  && self.col() == 0) ||
            (self.direction == Direction::Right && self.col() == (contraption.0[0].len() - 1) as PositionBaseType) {
                return (false, opt_dir)
        }

        (true, opt_dir)
    }

    pub fn _move_row(&mut self) {
        self.position.0.1 += 1;
    }

    pub fn _move_col(&mut self) {
        self.position.0.0 += 1;
    }
}

fn detect_energized(contraption_in: &Contraption, start_position: (PositionBaseType, PositionBaseType), start_direction: Direction) -> usize {
    let mut contraption = contraption_in.clone();
    let mut beams = vec![Beam::new(start_position, start_direction)];
    while !beams.is_empty() {
        for i in (0 .. beams.len()).rev() {
            let (beam_running, op_dir) = beams[i].step(&mut contraption);
            let beam_pos = beams[i].position;
            if !beam_running {
                beams.remove(i);
            }
            if let Some(dir) = op_dir {
                beams.push(Beam {position: beam_pos, direction: dir });
            }
        }
    }

    contraption.0
        .iter()
        .map(|cr| cr.iter().filter(|(_, e)| !e.is_empty()).count())
        .sum()
}

fn task_a(lines: &[String]) {
    let contraption = parse_input(lines);
    let energized = detect_energized(&contraption, (0, -1), Direction::Right);

    println!("# energized = {}", energized);
}

#[allow(unused,unused_mut)]
fn task_b(lines: &[String]) {
    let contraption = parse_input(lines);
    let sqr_side = contraption.0.len() as PositionBaseType;
    let energized = [
            (Direction::Down,  (-1, 0), (-1, sqr_side), (0, 1)),
            (Direction::Left,  (0, sqr_side), (sqr_side, sqr_side), (1, 0)),
            (Direction::Right, (0, -1), (sqr_side, -1), (1, 0)),
            (Direction::Up,    (sqr_side, 0), (sqr_side, sqr_side), (0, 1)),
        ]
        .iter()
        .map(|(direction, mut start_pos, end_pos, adder)|
            range_step(Position(start_pos), Position(*end_pos), Position(*adder))
                .map(|p| detect_energized(&contraption, p.0, *direction))
                // .inspect(|e| )
                .max()
                .unwrap()
        )
        .max()
        .unwrap();

    println!("Largest # energized = {:?}", energized);
}

fn parse_input(lines: &[String]) -> Contraption {
    let contrap = lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|c| {
                let t = match c {
                    '.' => Tile::Empty,
                    '/' => Tile::MirrorForward,
                    '\\' => Tile::MirrorBackward,
                    '-' => Tile::SplitterHorizontal,
                    '|' => Tile::SplitterVertical,
                    _ => panic!("c={c}")
                };
                (t, HashSet::new())
            })
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>();
    Contraption(contrap)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


use std::collections::HashMap;
use itertools::Itertools;

#[derive(Clone, Debug)]
struct Hand {
    cards: [u8; 5],
    bid: u32,
}

fn play(hands_in: &[Hand]) -> u32 {
    let mut hands = hands_in.to_vec();
    hands.sort_by(|a, b| {
        let mut card_count = [
            HashMap::new(),
            HashMap::new(),
        ];

        for (i, h) in [a, b].iter().enumerate() {
            for c in h.cards {
                card_count[i].entry(c).and_modify(|e| *e += 1).or_insert(1_u32);
            }

            let mut cc_sorted = card_count[i].iter().map(|e| (*e.0, *e.1)).collect_vec();
            cc_sorted.sort_by(|a, b| b.1.cmp(&a.1));

            if card_count[i].contains_key(&1) && cc_sorted[0].1 != 5 {
                let mut n_jokers = 0;
                for i in 0 .. cc_sorted.len() {
                    if cc_sorted[i].0 == 1 {
                        n_jokers = cc_sorted[i].1;
                        cc_sorted.remove(i);
                        break;
                    }
                }
                cc_sorted[0].1 += n_jokers;
                cc_sorted.sort_by(|a, b| b.1.cmp(&a.1));
            }

            card_count[i].insert(99, if cc_sorted[0].1 >= 4 {
                6 - cc_sorted[0].1
            } else if cc_sorted[0].1 == 3 && cc_sorted[1].1 == 2 {
                3
            } else if cc_sorted[0].1 == 3 {
                4
            } else if cc_sorted[0].1 == 2 && cc_sorted[1].1 == 2 {
                5
            } else if cc_sorted[0].1 == 2 {
                6
            } else {
                7
            });
        }

        if card_count[0][&99] == card_count[1][&99] {
            for i in 0 .. a.cards.len() {
                if a.cards[i] == b.cards[i] { continue; }
                if a.cards[i] > b.cards[i] {
                    card_count[1].entry(99).and_modify(|e| *e += 1);
                } else {
                    card_count[0].entry(99).and_modify(|e| *e += 1);
                }
                break;
            }
        }
        card_count[1][&99].cmp(&card_count[0][&99])
    });

    hands
        .iter()
        .enumerate()
        .map(|(i, h)| h.bid * (i as u32 + 1))
        .sum()
}

fn task_a(lines: &[String]) {
    let hands = parse_input(lines, false);
    let winnings = play(&hands);

    println!("Winnings = {}", winnings);
}

fn task_b(lines: &[String]) {
    let hands = parse_input(lines, true);
    let winnings = play(&hands);

    println!("Winnings = {}", winnings);
}

fn parse_input(lines: &[String], use_joker: bool) -> Vec<Hand> {
    let j_val = if use_joker { 1 } else { 11 };
    let map = HashMap::from([('A', 14), ('K', 13), ('Q', 12), ('J', j_val), ('T', 10), ('9', 9), ('8', 8), ('7', 7), ('6', 6), ('5', 5), ('4', 4), ('3', 3), ('2', 2)]);
    lines
        .iter()
        .map(|line|{
            let mut line_it = line.split(' ');
            Hand {
                cards: line_it.next().unwrap().chars().map(|c| map[&c]).collect::<Vec<u8>>().try_into().unwrap(),
                bid: line_it.next().unwrap().parse().unwrap(),
            }
        })
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


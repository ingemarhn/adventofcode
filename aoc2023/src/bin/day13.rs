use itertools::Itertools;

/* #[derive(Debug)]
struct Line {
    number: usize,
    line: String,
}

impl Line {
    pub fn new() {

    }
}
type Lines = Vec<Line>;
*/

type Lines = Vec<(usize, String)>;

#[derive(Clone, Debug)]
struct Pattern {
    rows: Lines,
    columns: Lines,
    rows_only: bool,
    iter_pos: Option<(usize, usize)>,
}

impl Pattern {
    pub fn new(src_lines: &[String], rows_only: bool) -> Self {
        let mut rows = Vec::new();
        let mut columns = (0 .. src_lines[0].len()).map(|i| (i + 1, String::new())).collect_vec();
        for (i, l) in src_lines.iter().enumerate() {
            if !rows_only {
                for (j, c) in l.chars().enumerate() {
                    columns[j].1.push(c);
                }
            }
            rows.push((i + 1, l.clone()));
        }

        Self { rows, columns, rows_only, iter_pos: None }
    }

    fn sort(&mut self) {
        self.rows.sort_by(|a, b| a.1.cmp(&b.1));
        if !self.rows_only {
            self.columns.sort_by(|a, b| a.1.cmp(&b.1));
        }
    }

    pub fn find_reflection(&self, prev_refl: usize) -> usize {
        let mut pattern = self.clone();
        pattern.sort();
        for (i, r1) in pattern.rows.iter().enumerate().take(pattern.rows.len() - 1) {
            if r1.1 == pattern.rows[i + 1].1 && r1.0 + 1 == pattern.rows[i + 1].0 {
                let mut lo = r1.0 - 1;
                let mut hi = pattern.rows[i + 1].0 - 1;
                loop {
                    if lo == 0 || hi == self.rows.last().unwrap().0 - 1 {
                        let refl = r1.0 * 100;
                        if refl != prev_refl {
                            return refl
                        }
                        break;
                    }
                    lo -= 1;
                    hi += 1;
                    if self.rows[lo].1 != self.rows[hi].1 {
                        break;
                    }
                }
            }
        }
        if !self.rows_only {
            for (i, r1) in pattern.columns.iter().enumerate().take(pattern.columns.len() - 1) {
                if r1.1 == pattern.columns[i + 1].1 && r1.0 + 1 == pattern.columns[i + 1].0 {
                    let mut lo = r1.0 - 1;
                    let mut hi = pattern.columns[i + 1].0 - 1;
                    loop {
                        if lo == 0 || hi == self.columns.last().unwrap().0 - 1 {
                            if r1.0 != prev_refl {
                                return r1.0;
                            }
                            break;
                        }
                        lo -= 1;
                        hi += 1;
                        if self.columns[lo].1 != self.columns[hi].1 {
                            break;
                        }
                    }
                }
            }
        }

        0
    }

    pub fn find_reflection_smudged(&mut self, prev_refl: usize) -> usize {
        let position = if let Some(mut pos) = self.iter_pos {
            if pos.1 < self.rows[0].1.len() - 1 {
                pos.1 += 1;
            } else {
                pos.0 += 1;
                if pos.0 == self.rows.len() {
                    return usize::MAX;
                }
                pos.1 = 0;
            }
            pos
        } else {
            (0, 0)
        };

        let mut selfie = self.clone();
        let mut row = selfie.rows[position.0].1.chars().collect_vec();
        row[position.1] = if row[position.1] == '.' { '#' } else { '.' };
        selfie.rows[position.0].1 = row.iter().collect::<String>();
        let mut col = selfie.columns[position.1].1.chars().collect_vec();
        col[position.0] = if col[position.0] == '.' { '#' } else { '.' };
        selfie.columns[position.1].1 = col.iter().collect::<String>();
        self.iter_pos = Some(position);

        selfie.find_reflection(prev_refl)
    }
}

fn find_reflections(patterns: &[Pattern]) -> usize {
    patterns.iter().map(|p| p.find_reflection(0)).sum()
}

fn find_reflections_smudged(patterns: &[Pattern]) -> usize {
    patterns
    .iter()
    .map(|p| {
        let prev_refl = p.find_reflection(0);
        let mut pp = p.clone();
        loop {
            let val = pp.find_reflection_smudged(prev_refl);
            if val != 0 {
                break if val != usize::MAX {
                    val
                } else {
                    0
                }
            }
        }
    })
    .sum()
}

fn task_a(lines: &[String]) {
    let patterns = parse_input(lines, false);
    let sum = find_reflections(&patterns);

    println!("Sum = {}", sum);
}

fn task_b(lines: &[String]) {
    let patterns = parse_input(lines, false);
    let sum = find_reflections_smudged(&patterns);

    println!("Sum = {}", sum);
}

fn parse_input(lines: &[String], rows_only: bool) -> Vec<Pattern> {
    let mut patterns = Vec::new();
    let mut start = 0;
    for i in 0 .. lines.len() {
        if lines[i].is_empty() {
            patterns.push(
                Pattern::new(&lines[start .. i], rows_only)
            );
            start = i + 1;
        }
    }
    patterns.push(
        Pattern::new(&lines[start .. ], rows_only)
    );

    patterns
}


fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


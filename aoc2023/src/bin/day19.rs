use std::{cmp::Ordering, collections::HashMap};

mod internal {
    use std::cmp::Ordering;
    use std::ops::Range;

    #[derive(Clone, Debug)]
    pub struct Rule {
        rate: char,
        condition: Ordering,
        value: usize,
        wflow: String,
    }

    impl Rule {
        fn new(rule_str: &str) -> Self {
            let mut r_it = rule_str.chars();
            let rate = r_it.next().unwrap();
            let c2 = if rule_str.len() > 1 {
                r_it.next().unwrap()
            } else {
                'A'
            };
            if ['<', '>'].contains(&c2) {
                let condition = if c2 == '<' {
                    Ordering::Less
                } else {
                    Ordering::Greater
                };
                let mut value = 0;
                for d in r_it.by_ref() {
                    if d == ':' {
                        break;
                    }
                    value = value * 10 + d.to_digit(10).unwrap() as usize;
                }
                let wflow = r_it.collect::<String>();

                Self { rate, condition, value, wflow }
            } else {
                Self { rate: 'A', condition: Ordering::Equal, value: 0, wflow: rule_str.to_string() }
            }
        }

        pub fn rate(&self) -> char {
            self.rate
        }

        pub fn condition(&self) -> Ordering {
            self.condition
        }

        pub fn value(&self) -> usize {
            self.value
        }

        pub fn wflow(&self) -> String {
            self.wflow.clone()
        }
    }

    #[derive(Clone, Debug)]
    pub struct Workflow(Vec<Rule>);

    impl Workflow {
        pub fn new(wrkflw: &str) -> Self {
            Self (
                wrkflw.split(',')
                    .map(Rule::new)
                    .collect::<Vec<_>>()
            )
        }

        pub fn flow(&self) -> Vec<Rule> {
            self.0.clone()
        }
    }

    #[derive(Debug)]
    pub struct MachinePartRating {
        x: usize,
        m: usize,
        a: usize,
        s: usize,
    }

    impl MachinePartRating {
        pub fn new(r_str: &str) -> Self {
            let parts = r_str.split(&['=', ','][..]).collect::<Vec<_>>();
            Self {
                x: parts[1].parse::<usize>().unwrap(),
                m: parts[3].parse::<usize>().unwrap(),
                a: parts[5].parse::<usize>().unwrap(),
                s: parts[7].parse::<usize>().unwrap()
            }
        }

        pub fn val(&self, el: char) -> usize {
            match el {
                'x' => self.x,
                'm' => self.m,
                'a' => self.a,
                's' => self.s,
                'A' => 0,
                _ => panic!()
            }
        }

        pub fn sum(&self) -> usize {
            self.x + self.m + self.a + self.s
        }
    }

    pub type RangeType = u128;

    #[derive(Clone, Debug)]
    pub struct RatingRange {
        x: Range<RangeType>,
        m: Range<RangeType>,
        a: Range<RangeType>,
        s: Range<RangeType>,
    }

    impl RatingRange {
        pub fn new() -> Self {
            const INIT: Range<RangeType> = 1 .. 4000;
            Self { x: INIT, m: INIT, a: INIT, s: INIT }
        }

        fn get_rate(&mut self, rate: char) -> &mut Range<RangeType> {
            match rate {
                'x' => &mut self.x,
                'm' => &mut self.m,
                'a' => &mut self.a,
                _   => &mut self.s,
            }
        }

        pub fn set_lower(&mut self, rate: char, val: RangeType) {
            let s_rate = self.get_rate(rate);
            s_rate.start = val;
        }

        pub fn set_upper(&mut self, rate: char, val: RangeType) {
            let s_rate = self.get_rate(rate);
            s_rate.end = val;
        }

        pub fn product(&self) -> RangeType {
            let len = |rng: &Range<RangeType>| rng.end - rng.start + 1;
            len(&self.x) * len(&self.m) * len(&self.a) * len(&self.s)
        }
    }
}
use internal::*;

fn run_workflows(workflows: &HashMap<String, Workflow>, ratings: &[MachinePartRating]) -> usize {
    let mut rating_sum = 0;

    'o:
    for rating in ratings {
        let mut next_flow = "in".to_string();
        loop {
            let flow = workflows[&next_flow].flow();
            for rule in flow.iter() {
                let rate = rule.rate();
                let value = match rate {
                    'A' => 0,
                    _ => rule.value(),
                };

                if rating.val(rate).cmp(&value) == rule.condition() {
                    next_flow = rule.wflow();
                    if next_flow == 'R'.to_string() {
                        continue 'o;
                    }
                    if next_flow == 'A'.to_string() {
                        rating_sum += rating.sum();
                        continue 'o;
                    }

                    break;
                }
            }
        }
    }

    rating_sum
}

fn find_distinct_combs(workflows: &HashMap<String, Workflow>) -> RangeType {
    let mut comb_counter = 0;
    let mut queue = vec![("in".to_string(), RatingRange::new())];
    while let Some((curr_flow, mut rating_limits)) = queue.pop() {
        if curr_flow == 'R'.to_string() {
            continue;
        }
        if curr_flow == 'A'.to_string() {
            comb_counter += rating_limits.product();
            continue;
        }
        let flow = workflows[&curr_flow].flow();

        for rule in flow.iter() {
            let next_flow = rule.wflow();

            let rate = rule.rate();
            if rate == 'A' {
                queue.push((next_flow, rating_limits.clone()));
                break;
            }

            let val = rule.value() as RangeType;
            let mut next_rating_limits = rating_limits.clone();
            if rule.condition() == Ordering::Less {
                next_rating_limits.set_upper(rate, val - 1);
                queue.push((next_flow, next_rating_limits.clone()));
                rating_limits.set_lower(rate, val);
            } else {
                next_rating_limits.set_lower(rate, val + 1);
                queue.push((next_flow, next_rating_limits.clone()));
                rating_limits.set_upper(rate, val);
            }
        }
    }

    comb_counter
}

fn task_a(lines: &[String]) {
    let (workflows, ratings) = parse_input(lines);
    let sum = run_workflows(&workflows, &ratings);

    println!("Workflows sum = {}", sum);
}

fn task_b(lines: &[String]) {
    let (workflows, _) = parse_input(lines);
    let combs = find_distinct_combs(&workflows);

    println!("# of combinations = {}", combs);
}

fn parse_input(lines: &[String]) -> (HashMap<String, Workflow>, Vec<MachinePartRating>) {
    let mut lines_it = lines.iter();
    let mut workflows  = HashMap::new();
    for line in lines_it.by_ref() {
        if line.is_empty() {
            break;
        }

        let parts = line.split(&['{', '}'][..]).collect::<Vec<_>>();
        workflows.insert(parts[0].to_string(), Workflow::new(parts[1]));
    }

    let mut ratings = Vec::new();
    for line in lines_it {
        let mut parts_it = line.split(&['{', '}'][..]).skip(1);
        ratings.push(MachinePartRating::new(parts_it.next().unwrap()));
    }

    (workflows, ratings)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

use std::collections::{HashMap, VecDeque};

type Field = Vec<Vec<Pipe>>;
type PipeLoop = Vec<(usize, usize, Direction, Pipe)>;

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum Pipe {
    NS,
    EW,
    NE,
    NW,
    SW,
    SE,
    Ground,
    Animal,
}
use itertools::Itertools;
use Pipe::*;

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum Direction {
    E, S, W, N, A,
}
use Direction::*;

fn find_main_loop(field: &Field, dont_go_to: Direction) -> PipeLoop {
    let animal_pos = field
        .iter()
        .enumerate()
        .fold((0, 0), |s_pos, (i_row, p_row)| {
            if let Some(pos) = p_row.iter().position(|p| *p == Animal) {
                (i_row, pos)
            } else {
                s_pos
            }
        });

    let mut pipe_loop = vec![(animal_pos.0, animal_pos.1, A, Ground)];

    // No need to have this closure, only used for first tile after animal position
    let get_neighbors = |(y, x): (usize, usize)| {
        [
            (y.overflowing_add(0), x.overflowing_add(1), E),
            (y.overflowing_add(1), x.overflowing_add(0), S),
            (y.overflowing_add(0), x.overflowing_sub(1), W),
            (y.overflowing_sub(1), x.overflowing_sub(0), N),
        ]
        .iter()
        .fold(Vec::new(), |mut coords, (y, x, d)|{
            if !y.1 && !x.1 && *d != dont_go_to {
                // None of the coordinates are outside the field
                coords.push((y.0, x.0, *d));
            }
            coords
        })
    };

    // Find first pipe connection
    let neibrs= get_neighbors(animal_pos);
    let (mut y, mut x, mut d, mut me) = (0, 0, A, Ground);
    for (ny, nx, nd) in neibrs {
        if nd == E && [EW, NW, SW].contains(&field[ny][nx]) ||
           nd == S && [NS, NE, NW].contains(&field[ny][nx]) ||
           nd == W && [EW, NE, SE].contains(&field[ny][nx]) ||
           nd == N && [NS, SW, SE].contains(&field[ny][nx])
        {
            (y, x, d, me) = (ny, nx, nd, field[ny][nx]);
            pipe_loop.push((ny, nx, nd, field[ny][nx]));
            break;
        }
    }

    let dir_map = HashMap::from([
        (E, HashMap::from([(EW, E), (NW, N), (SW, S)])),
        (S, HashMap::from([(NS, S), (NE, E), (NW, W)])),
        (W, HashMap::from([(EW, W), (NE, N), (SE, S)])),
        (N, HashMap::from([(NS, N), (SE, E), (SW, W)])),
    ]);

    loop {
        let next_d = dir_map[&d][&me];
        match next_d {
            E => x += 1,
            S => y += 1,
            W => x -= 1,
            N => y -= 1,
            _ => panic!()
        }

        d = next_d;
        me = field[y][x];

        if me == Animal {
            pipe_loop[0].2 = d;
            let d_out = pipe_loop[1].2;
            pipe_loop[0].3 = match d {
                E => match d_out {
                    E => EW,
                    N => NW,
                    S => SW,
                    _ => panic!()
                },
                S => match d_out {
                    S => NS,
                    E => NE,
                    W => NW,
                    _ => panic!()
                },
                W => match d_out {
                    W => EW,
                    N => NE,
                    S => SE,
                    _ => panic!()
                },
                N => match d_out {
                    N => NS,
                    E => SE,
                    W => SW,
                    _ => panic!()
                },
                _ => panic!()
            };
            break;
        }

        pipe_loop.push((y, x, d, me));
    }

    pipe_loop
}

fn get_n_contained_tiles(field: &Field) -> usize {
    let main_loop = find_main_loop(field, A);
    let main_loop_indices = main_loop.iter().map(|tile| (tile.0, tile.1)).collect_vec();
    let mut loose_tiles = field
        .iter()
        .enumerate()
        .map(|(r, field_row)|
            field_row
                .iter()
                .enumerate()
                .map(|(c, _)| (r, c))
                .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
        .concat();
    loose_tiles.retain(|tile| !main_loop_indices.iter().any(|l_tile| *tile == *l_tile));
    let n_loose = loose_tiles.len();

    // Find a north-most tile, which always must be a SE pipe
    let mut top_left_pos = (0, 0);
    'ml: for (r, row) in field.iter().enumerate() {
        for c in 0 .. row.len() {
            if main_loop_indices.iter().any(|tile| *tile == (r, c)) {
                top_left_pos = (r, c);
                break 'ml;
            }
        }
    }

    let mut temp_iter = main_loop.iter().cycle().peekable();
    for tile in temp_iter.by_ref() {
        if (tile.0, tile.1) == top_left_pos {
            break;
        }
    }
    let main_loop_rev;
    let main_iter = if temp_iter.peek().unwrap().2 == E {
        temp_iter
    } else {
        main_loop_rev = find_main_loop(field, main_loop[1].2);
        let mut tempr_iter = main_loop_rev.iter().cycle().peekable();
        for tile in tempr_iter.by_ref() {
            if (tile.0, tile.1) == top_left_pos {
                break;
            }
        }
        tempr_iter
    };

    let mut mark_as_inside = |y, x| {
        if !loose_tiles.contains(&(y, x)) { return }

        // Run BFS to find all insiders at this spot
        let mut queue = VecDeque::from([(y, x)]);
        let mut visited = Vec::from([(y, x)]);
        while let Some((yq, xq)) = queue.pop_front() {
            if xq == field[0].len() - 1 ||
               yq == field.len() - 1    ||
               xq == 0 ||
               yq == 0
            {
                return;
            }

            for neighbor in [(yq, xq + 1), (yq + 1, xq), (yq, xq - 1), (yq - 1, xq)] {
                // If a main_loop tile is found, skip that neighbor
                if visited.contains(&neighbor) || main_loop_indices.contains(&neighbor) {
                    continue;
                }

                queue.push_back(neighbor);
                visited.push(neighbor);
            }
        }
        for vis in visited {
            if let Some(index) = loose_tiles.iter().position(|yx| *yx == vis) {
                loose_tiles.remove(index);
            }
        }
    };

    for tile in main_iter {
        if (tile.0, tile.1) == top_left_pos {
            break;
        }

        match tile.2 {
            E => match tile.3 {
                    EW => mark_as_inside(tile.0 + 1, tile.1),
                    SW => {},
                    NW => {
                        mark_as_inside(tile.0 + 1, tile.1);
                        mark_as_inside(tile.0, tile.1 + 1);
                    },
                    _ => panic!()
                },
            S => match tile.3 {
                    NS => mark_as_inside(tile.0, tile.1 - 1),
                    NW => {},
                    NE => {
                        mark_as_inside(tile.0 + 1, tile.1);
                        mark_as_inside(tile.0, tile.1 - 1);
                    },
                    _ => panic!()
                },
            W => match tile.3 {
                    EW => mark_as_inside(tile.0 - 1, tile.1),
                    NE => {},
                    SE => {
                        mark_as_inside(tile.0 - 1, tile.1);
                        mark_as_inside(tile.0, tile.1 - 1);
                    },
                    _ => panic!()
                },
            N => match tile.3 {
                    NS => mark_as_inside(tile.0, tile.1 + 1),
                    SE => {},
                    SW => {
                        mark_as_inside(tile.0 - 1, tile.1);
                        mark_as_inside(tile.0, tile.1 + 1);
                    },
                    _ => panic!()
                },
            _ => panic!()
        };
    }

    n_loose - loose_tiles.len()
}

fn task_a(lines: &[String]) {
    let field = parse_input(lines);
    let main_loop = find_main_loop(&field, A);
    println!("# of steps = {}", main_loop.len() / 2);
}

fn task_b(lines: &[String]) {
    let field = parse_input(lines);
    let count = get_n_contained_tiles(&field);

    println!("# of tiles = {}", count);
}

/*
| is a vertical pipe connecting north and south.
- is a horizontal pipe connecting east and west.
L is a 90-degree bend connecting north and east.
J is a 90-degree bend connecting north and west.
7 is a 90-degree bend connecting south and west.
F is a 90-degree bend connecting south and east.
. is ground; there is no pipe in this tile.
S is the starting position of the animal */
fn parse_input(lines: &[String]) -> Field {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|chr|
                match chr {
                    '|' => NS,
                    '-' => EW,
                    'L' => NE,
                    'J' => NW,
                    '7' => SW,
                    'F' => SE,
                    '.' => Ground,
                    'S' => Animal,
                    _ => panic!()
                }
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


use std::collections::{HashMap, VecDeque};

type Index = isize;
type Position = (Index, Index);
type Positions = Vec<Position>;
type Map = Vec<Vec<char>>;

fn _get_plots_n_steps(steps_to_walk: usize, map: &Map, start: Position) -> HashMap<(isize, isize), usize> {
    let m_wid = map[0].len() as Index;
    let m_hei = map.len() as Index;
    let successors = |pos: &Position| -> Positions {
        let coords = [
            (pos.0 + 1, pos.1),
            (pos.0, pos.1 + 1),
            (pos.0 - 1, pos.1),
            (pos.0, pos.1 - 1),
        ];
        coords
        .iter()
        .filter(|p| map[((m_hei + p.0 % m_hei) % m_hei) as usize][((m_wid + p.1 % m_wid) % m_wid) as usize] != '#')
        .copied()
        .collect()
    };

    let mut queue = VecDeque::from([(start, 0)]);
    let mut visited  = HashMap::new();
    visited.insert(start, 0);
    while let Some((plot, steps)) = queue.pop_front() {
        if steps < steps_to_walk {
            let n_steps = steps + 1;
            for next in successors(&plot) {
                if let std::collections::hash_map::Entry::Vacant(e) = visited.entry(next) {
                    e.insert(n_steps);
                    queue.push_back((next, n_steps));
                }
            }
        }
    }

    visited
}

fn bfs(map: &Map, start: Position) -> HashMap<(isize, isize), usize> {
    let m_wid = map[0].len() as Index;
    let m_hei = map.len() as Index;
    let successors = |pos: &Position| -> Positions {
        let coords = [
            (pos.0 + 1, pos.1),
            (pos.0, pos.1 + 1),
            (pos.0 - 1, pos.1),
            (pos.0, pos.1 - 1),
        ];
        coords
        .iter()
        .filter(|p|
            p.0 >= 0 && p.0 < m_hei && p.1 >= 0 && p.1 < m_wid &&
            map[((m_hei + p.0 % m_hei) % m_hei) as usize][((m_wid + p.1 % m_wid) % m_wid) as usize] != '#'
        )
        .copied()
        .collect()
    };

    let mut queue = VecDeque::from([(start, 0)]);
    let mut visited  = HashMap::new();
    visited.insert(start, 0);
    while let Some((plot, steps)) = queue.pop_front() {
        let n_steps = steps + 1;
        for next in successors(&plot) {
            if let std::collections::hash_map::Entry::Vacant(e) = visited.entry(next) {
                e.insert(n_steps);
                queue.push_back((next, n_steps));
            }
        }
    }

    visited
}

fn walk_steps(steps_to_walk: usize, map: &Map, start: Position) -> usize {
    let visited = bfs(map, start);
    let parity = steps_to_walk % 2;

    let comp = |steps|
        if parity == 0 {
            steps % 2 == parity
        } else {
            steps % 2 != parity
        }
    ;
    // Idea stolen from someone at reddit
    visited.iter().filter(|(_, &steps)| steps <= steps_to_walk && comp(steps)).count()
}

fn calc_steps(steps_to_walk: usize, map: &Map, start: Position) -> usize {
    // After study of the input it's revealed that it's a perfect square where start is exactly in the middle
    // It also shows that the large number of steps to walk / map.width == number of squares to cross plus half a square (start point to edge)
    // I.e. 2650136526501365 / 131 = 202300 + 65

    let map_width = map.len();
    let map_half = map_width / 2;
    let parity = steps_to_walk % 2;
    let n_borders = steps_to_walk / map_width;

    let visited = bfs(map, start);
    let parity_0_all = visited.iter().filter(|(_, &steps)| steps % 2 == 0).count();
    let parity_1_all = visited.iter().filter(|(_, &steps)| steps % 2 == 1).count();
    let corner_plots_0 = parity_0_all - visited.iter().filter(|(_, &steps)| steps <= map_half && steps % 2 == 0).count();
    let corner_plots_1 = parity_1_all - visited.iter().filter(|(_, &steps)| steps <= map_half && steps % 2 == 1).count();

    // The given explanation shows - due to the nature of the input - that for n # of map boundaries crossed where
    //  n is even:
    //      B = (N + 1)^2	+ N
    //      G = N^2			- (N + 1)

    //  n is odd:
    //      B = N^2			+ N
    //      G = (N + 1)^2	- (N + 1)

    // Five total squares gives (even parity):
    // 4 * num_even_parity_squares
    // + 1 * num_odd_parity_squares
    // + 1 * num_even_parity_corner_squares
    // - 2 * num_odd_parity_corner_squares

    let (n_maps_odd, n_maps_even) = if parity == 1 {
        // I.e. # of borders to cross are even
        ((n_borders + 1).pow(2), n_borders.pow(2))
    } else {
        (n_borders.pow(2), (n_borders + 1).pow(2))
    };

    // I noticed that corner plots to add and subtract can be based on n_borders. Even simpler than suggested solution.
    n_maps_odd * parity_1_all + n_maps_even * parity_0_all + n_borders * corner_plots_0 - (n_borders + 1) * corner_plots_1

}

fn task_a(lines: &[String]) {
    let (map, start) = parse_input(lines);
    let n_plots = walk_steps(64, &map, start);

    println!("# garden plots: {}", n_plots);
}

fn task_b(lines: &[String]) {
    // No clue how to solve this if it wasn't for @xavdid, https://advent-of-code.xavd.id/writeups/2023/day/21/

    let (map, start) = parse_input(lines);
    let n_plots = calc_steps(26501365, &map, start);

    println!("# garden plots: {}", n_plots);
}

fn parse_input(lines: &[String]) -> (Map, Position) {
    let mut start = (0, 0);
    let mut map = Vec::new();

    for (r, l) in lines.iter().enumerate() {
        let row = l.chars().collect();
        map.push(row);
        if let Some(p) = l.chars().position(|c| c == 'S') {
            start = (r as Index, p as Index);
        }
    }

    (map, start)
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
mod tests23_21 {
    use super::*;

    #[test]
    fn tests23_21_t0() {
        /*
        In exactly 6 steps, he can still reach 16 garden plots.
        In exactly 10 steps, he can reach any of 50 garden plots.
        In exactly 50 steps, he can reach 1594 garden plots.
        In exactly 100 steps, he can reach 6536 garden plots.
        In exactly 500 steps, he can reach 167004 garden plots.
        In exactly 1000 steps, he can reach 668697 garden plots.
        In exactly 5000 steps, he can reach 16733044 garden plots.
         */
        let input = common::get_input_from_file("21_sample-1".to_string());
        let (map, start) = parse_input(&input);
        for ns in [6, 10, 50, 100, 500, 1000, 5000] {
            println!("ns = {}", ns);
            let n_plots = walk_steps(ns, &map, start);
            println!("# garden plots: {}", n_plots);
        }
    }

    #[test]
    fn tests23_21_t1() {
        let input = common::get_input_from_file("21_sample-2".to_string());
        let (map, start) = parse_input(&input);
        let n_plots = walk_steps(4, &map, start);
        println!("# garden plots: {}", n_plots);
    }

    #[test]
    fn tests23_21_t2() {
        let input = common::get_input_from_file("21_sample-2".to_string());
        let (map, start) = parse_input(&input);
        let steps_to_walk = 4;
        let visited = bfs(&map, start);
        let mut mmap = map.clone();
        for v in visited.iter().filter(|(_, &steps)| steps <= steps_to_walk && steps % 2 == steps_to_walk % 2) {
            mmap[v.0.0 as usize][v.0.1 as usize] = 'O';
        }
        for mr in mmap.iter() {
            println!("{}", mr.iter().collect::<String>());
        }
        println!();
        let mut mmap = map.clone();
        for v in visited.iter().filter(|(_, &steps)| steps <= steps_to_walk && steps % 2 != steps_to_walk % 2) {
            mmap[v.0.0 as usize][v.0.1 as usize] = '1';
        }
        for mr in mmap.iter() {
            println!("{}", mr.iter().collect::<String>());
        }
        let n_plots = visited.iter().filter(|(_, &steps)| steps <= steps_to_walk && steps % 2 == steps_to_walk % 2).count();

        println!("# garden plots: {}", n_plots);
    }

    #[test]
    fn tests23_21_t3() {
        let input = common::get_input_from_file("21_sample-2".to_string());
        let (map, start) = parse_input(&input);
        let n_plots = calc_steps(13, &map, start);
        println!("# garden plots: {}", n_plots);
    }
}


type Coordinate = f64;
type Coordinates = [Coordinate; 3];
type Particles = Vec<Particle>;

#[derive(Clone, Copy, Debug)]
struct Particle {
    position:     Coordinates,
    velocity:     Coordinates,
}

impl Particle {
    fn new(pos_str: &str, vel_str: &str, ) -> Self {
        Self { position: Self::get_coordinates(pos_str), velocity: Self::get_coordinates(vel_str) }
    }

    fn get_coordinates(s: &str) -> Coordinates {
        let mut coordinates = s
            .split(", ")
            .map(|ss| ss.trim().parse().unwrap());

        [
            coordinates.next().unwrap(),
            coordinates.next().unwrap(),
            coordinates.next().unwrap(),
        ]
    }
}

#[derive(Clone, Copy, Debug)]
struct IParticle {
    position: [i128; 3],
    velocity: [i128; 3],
}

fn calc_intersection(hstone1: &Particle, hstone2: &Particle) -> (Coordinate, Coordinate) {
    // y = mx + b
    // m1 = (y2 - y1)/(x2 - x1)
    // b1 = y1 - m1*x1
    // x = (b2 - b1) / (m1 - m2)
    let m1 = hstone1.velocity[1] / hstone1.velocity[0];
    let m2 = hstone2.velocity[1] / hstone2.velocity[0];
    let b1 = hstone1.position[1] - m1 * hstone1.position[0];
    let b2 = hstone2.position[1] - m2 * hstone2.position[0];

    let x = (b2 - b1) / (m1 - m2);
    let y = m1 * x + b1;

    (x, y)
}

fn find_intersections(hailstones: &Particles, min: Coordinate, max: Coordinate) -> usize {
    let mut count = 0;

    let check_past = |strt, intp, vel: Coordinate| (intp > strt && vel < 0.0) || (intp < strt && vel > 0.0);
    for (i, hstone1) in hailstones.iter().enumerate().take(hailstones.len() - 1) {
        for hstone2 in hailstones.iter().skip(i + 1) {
            let (x, y) = calc_intersection(hstone1, hstone2);

            if (x + y).is_infinite() ||
               !(min ..= max).contains(&x) ||
               !(min ..= max).contains(&y) ||
               check_past(hstone1.position[0], x, hstone1.velocity[0]) ||
               check_past(hstone2.position[0], x, hstone2.velocity[0]) ||
               check_past(hstone1.position[1], y, hstone1.velocity[1]) ||
               check_past(hstone2.position[1], y, hstone2.velocity[1])
            {
                continue;
            }

            count += 1;
        }
    }

    count
}

fn calc_intersection_3d(hstone: &Particle, rock: &Particle) -> Option<Coordinates> {
    // THANK YOU Chat GPT!!!
    // p1 + t * d1 = p2 + s * d2
    // Lös linjärt ekvationssystem för t och s
    if let Some((t, _s)) = solve_parameters(hstone, rock) {
        // Beräkna skärningspunkten
        let intersection = [
            hstone.position[0] + t * hstone.velocity[0],
            hstone.position[1] + t * hstone.velocity[1],
            hstone.position[2] + t * hstone.velocity[2],
        ];

        Some(intersection)
    } else {
        None
    }
}

fn solve_parameters(hstone: &Particle, rock: &Particle) -> Option<(Coordinate, Coordinate)> {
    // Ställ upp ekvationerna:
    // p1 + t * d1 = p2 + s * d2
    // Komponentvis:
    // (pN = .position, dN = .velocity)
    // 1. p1[0] + t * d1[0] = p2[0] + s * d2[0]
    // 2. p1[1] + t * d1[1] = p2[1] + s * d2[1]
    // 3. p1[2] + t * d1[2] = p2[2] + s * d2[2]

    // Ekvationer i matrisform:
    let a = [
        [hstone.velocity[0], -rock.velocity[0]], // Koefficienter för x-komponenten
        [hstone.velocity[1], -rock.velocity[1]], // Koefficienter för y-komponenten
    ];

    let b = [
        rock.position[0] - hstone.position[0], // Högerled för x-komponenten
        rock.position[1] - hstone.position[1], // Högerled för y-komponenten
    ];

    // Lös systemet Ax = b för x = [t, s]
    if let Some(solution) = solve_linear_system(a, b) {
        let t = solution[0];
        let s = solution[1];

        // Kontrollera med tredje ekvationen
        let z1 = hstone.position[2] + t * hstone.velocity[2];
        let z2 = rock.position[2] + s * rock.velocity[2];
        if (z1 - z2).abs() < 1e-6 {
            return Some((t, s));
        }
    }

    None
}

/// Lös ett 2x2-linjärt ekvationssystem Ax = b
fn solve_linear_system(a: [[Coordinate; 2]; 2], b: [Coordinate; 2]) -> Option<[Coordinate; 2]> {
    let det = a[0][0] * a[1][1] - a[0][1] * a[1][0];
    if det.abs() < 1e-6 {
        return None; // Singular matris
    }

    let inv_det = 1.0 / det;
    let x = [
        (b[0] * a[1][1] - b[1] * a[0][1]) * inv_det,
        (a[0][0] * b[1] - a[1][0] * b[0]) * inv_det,
    ];

    Some(x)
}

fn hit_hailstones_samp(hailstones: &Particles) -> Coordinate {
    let mut ns_1 = 1.0;
    let mut ns_2 = ns_1;

    loop {
        ns_2 += 1.0;
        for (i, hstone1) in hailstones.iter().enumerate() {
            'inner: for (j, hstone2) in hailstones.iter().enumerate() {
                if j == i {
                    continue;
                }
                let coll_pos_1: Coordinates = [
                    hstone1.position[0] + hstone1.velocity[0] * ns_1,
                    hstone1.position[1] + hstone1.velocity[1] * ns_1,
                    hstone1.position[2] + hstone1.velocity[2] * ns_1,
                ];
                let mut rock = Particle {
                    position: [0.0; 3],
                    velocity: [
                        ((hstone2.position[0] + hstone2.velocity[0] * ns_2) - coll_pos_1[0]) / (ns_2 - ns_1),
                        ((hstone2.position[1] + hstone2.velocity[1] * ns_2) - coll_pos_1[1]) / (ns_2 - ns_1),
                        ((hstone2.position[2] + hstone2.velocity[2] * ns_2) - coll_pos_1[2]) / (ns_2 - ns_1),
                    ]
                };
                rock.position = [
                    coll_pos_1[0] - rock.velocity[0] / ns_1,
                    coll_pos_1[1] - rock.velocity[1] / ns_1,
                    coll_pos_1[2] - rock.velocity[2] / ns_1,
                ];

                for ind in 0 .. 3 {
                    if !almost::equal(rock.position[ind], rock.position[ind].round() as Coordinate) {
                        continue 'inner;
                    }
                }

                // We've got a rock that starts at an integer position. Now check where other hailstones collides with the rock.
                // The collision points have to be integer positions.
                for (hi, hs) in hailstones.iter().enumerate() {
                    if hi == i || hi == j {
                        continue;
                    }
                    let Some(pos) = calc_intersection_3d(hs, &rock) else {
                        continue 'inner;
                    };

                    for p in pos.iter() {
                        if !almost::equal(*p, p.round() as Coordinate) {
                            continue 'inner;
                        }
                    }
                }

                return rock.position.iter().sum::<Coordinate>();
            }
        }

        if ns_2 - ns_1 > 10000.0 {
            ns_1 += 1.0;
            ns_2 = ns_1;
        }
    }
}

fn hit_hailstones(hailstones: &Particles) -> i128 {
    if common::sample_input_used() {
        return hit_hailstones_samp(hailstones) as i128;
    }

    // Most brilliant solution here: https://www.reddit.com/r/adventofcode/comments/18pnycy/comment/kicuapd/
    // Can't say that I completely understand this solution ...
    /*
    # If standing on a hail we will see the rock travel in a straight line,
    # pass through us and two other points on two other pieces of hail that zip by
    # there must be two vectors from our hail to the other two collisions (v1 and v2)
    # such that v1 = m * v2 where m is some unknown scalar multiplier.
    # we can make v1 = v2 by dividing one of the x,y or z components by itself to ensure
    # it is equal to 1. Then solve.

    # Select three hail so that the relative, x, y or z is all zero
    */

    // Find three hailstones with equal velocity in one direction
    // Use y since that is used in the code from the solution
    let mut hs = hailstones.iter().map(|e| IParticle {
        position: [e.position[0] as i128, e.position[1] as i128, e.position[2] as i128],
        velocity: [e.velocity[0] as i128, e.velocity[1] as i128, e.velocity[2] as i128]
    })
    .collect::<Vec<_>>();
    hs.sort_unstable_by_key(|k| k.velocity[1]);
    let mut s = 0;
    loop {
        if hs[s + 1].velocity[1] == hs[s].velocity[1] &&
           hs[s + 2].velocity[1] == hs[s].velocity[1] {
            break;
        }

        s += 1;
    }

    let vx = [hs[s].velocity[0], hs[s + 1].velocity[0], hs[s + 2].velocity[0]];
    let vy = [hs[s].velocity[1], hs[s + 1].velocity[1], hs[s + 2].velocity[1]];
    let vz = [hs[s].velocity[2], hs[s + 1].velocity[2], hs[s + 2].velocity[2]];

    let px = [hs[s].position[0], hs[s + 1].position[0], hs[s + 2].position[0]];
    let py = [hs[s].position[1], hs[s + 1].position[1], hs[s + 2].position[1]];
    let pz = [hs[s].position[2], hs[s + 1].position[2], hs[s + 2].position[2]];

    // # calculate relative velocities of hail 1 and 2 to hail 0
    // # the x component is zero due to selection of hail
    let vxr = [ vx[1] - vx[0], vx[2] - vx[0] ];
    let vzr = [ vz[1] - vz[0], vz[2] - vz[0] ];

    // # relative initial position of the hails
    let pxr = [ px[1] - px[0], px[2] - px[0] ];
    let pyr = [ py[1] - py[0], py[2] - py[0] ];
    let pzr = [ pz[1] - pz[0], pz[2] - pz[0] ];

    /*
    # 1st Hail equations
    # x = xr[1]
    # y = yr[1] + vyr[1]*t1
    # z = zr[1] + vzr[1]*t1

    # 2nd Hail equations
    # x = xr[2]
    # y = yr[2] + vyr[2]*t2
    # z = zr[2] + vzr[2]*t2

    #divide all equations by the x component to make x = 1 and ensure both vectors are the same
    # 1st Set results
    # x = 1
    # y = (yr[1]+vyr[1]*t1)/pxr[1]
    # z = (zr[1]+vzr[1]*t1)/pxr[1]

    #2nd Set results
    # x = 1
    # y = (yr[2]+vyr[2]*t2)/pxr[2]
    # z = (zr[2]+vzr[2]*t2)/pxr[2]
    */

    // #Solve set of two linear equations y=y and z=z  [what does this mean???]
    let num = (pyr[1] * pxr[0] * vzr[0]) - (vxr[0] * pyr[1] * pzr[0]) + (pyr[0] * pzr[1] * vxr[0]) - (pyr[0] * pxr[1] * vzr[0]);
    let t2 = num / (pyr[0] * ((vzr[0] * vxr[1]) - (vxr[0] * vzr[1])));

    // #Substitute t2 into a t1 equation
    let num = (pyr[0] * pxr[1]) + (pyr[0] * vxr[1] * t2) - (pyr[1] * pxr[0]);
    let t1 = num / (pyr[1] * vxr[0]);
    // println!("t1 time of first vector @ collision: {}", t1);
    // println!("t2 time of second vector @ collision: {}", t2);

    // #calculate collision position at t1 and t2 of hail 1 and 2 in normal frame of reference
    let cx = [ px[1] + (t1 * vx[1]), px[2] + (t2 * vx[2]) ];
    let cy = [ py[1] + (t1 * vy[1]), py[2] + (t2 * vy[2]) ];
    let cz = [ pz[1] + (t1 * vz[1]), pz[2] + (t2 * vz[2]) ];

    // println!("collision one occurs @ {}, {}, {}", cx[0], cy[0], cz[0]);
    // println!("collision two occurs @ {}, {}, {}", cx[1], cy[1], cz[1]);

    // #calculate the vector the rock travelled between those two collisions
    let xm = (cx[1] - cx[0]) / (t2 - t1);
    let ym = (cy[1] - cy[0]) / (t2 - t1);
    let zm = (cz[1] - cz[0]) / (t2 - t1);
    // println!("rock vector: {} {} {}", xm, ym, zm);

    // #calculate the initial position of the rock based on its vector
    let xc = cx[0] - (xm * t1);
    let yc = cy[0] - (ym * t1);
    let zc = cz[0] - (zm * t1);
    // println!("rock inital position: {} {} {}", xc, yc, zc);

    xc + yc + zc
}

fn get_limits() -> (Coordinate, Coordinate) {
    if !common::sample_input_used() {
        (200000000000000.0, 400000000000000.0)
    } else {
        (7.0, 27.0)
    }
}

fn task_a(lines: &[String]) {
    let hailstones = parse_input(lines);
    let (min, max) = get_limits();
    let n_intersect = find_intersections(&hailstones, min, max);

    println!("# of intersections: {}", n_intersect);
}

fn task_b(lines: &[String]) {
    let hailstones = parse_input(lines);
    let coord_sum  = hit_hailstones(&hailstones);

    println!("Coordinates sum: {}", coord_sum);
}

fn parse_input(lines: &[String]) -> Particles {
    lines
        .iter()
        .map(|line|
            line
            .split(" @ ")
            .collect::<Vec<_>>()
        )
        .map(|coo|
            Particle::new(coo[0], coo[1])
        )
        .collect::<Particles>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[allow(unused_imports)]
#[cfg(test)]
mod tests23_24 {
    use super::*;

    #[test]
    fn tests23_24_t0() {}
}


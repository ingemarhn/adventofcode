use std::collections::HashMap;

type Loss = u16;
type Map = Vec<Vec<Loss>>;
type Position = (usize, usize);
type Neighbor = (Position, usize);
type Neighbors = Vec<Neighbor>;

fn find_least_loss(map: &Map, goal: Position, min_moves: usize, max_moves: usize) -> usize {
    /*
    [Python]
    frontier = PriorityQueue()  (priority is estimated cost to the goal)
    frontier.put(start, 0)      (here the priority isn't interesting since there only is one element in the queue)
    came_from = dict()          (only needed if path shall be returned)
    cost_so_far = dict()
    came_from[start] = None
    cost_so_far[start] = 0

    while not frontier.empty():
    current = frontier.get()

    if current == goal:
        break

    for next in graph.neighbors(current):
        new_cost = cost_so_far[current] + graph.cost(current, next)
        if next not in cost_so_far or new_cost < cost_so_far[next]:
            cost_so_far[next] = new_cost
            priority = new_cost + heuristic(goal, next)
            frontier.put(next, priority)
            came_from[next] = current
    */
    // A*, A-star, a_star
    use priority_queue::PriorityQueue;
    use std::cmp::Reverse;

    #[derive(Clone, Debug, Eq, PartialEq, Hash)]
    struct Node {
        position: Position,
        dir: usize,
        dir_count: usize,
    }
    let heuristic = |p: &Position| (goal.0.abs_diff(p.0) + goal.1.abs_diff(p.1));

    let mut candidates = PriorityQueue::new();
    let mut cost_so_far = HashMap::new();
    let h = heuristic(&(1, 0));
    candidates.push(Node { position: (1, 0), dir: 0, dir_count: 1}, Reverse(map[1][0] as usize + h));
    candidates.push(Node { position: (0, 1), dir: 1, dir_count: 1}, Reverse(map[0][1] as usize + h));
    cost_so_far.insert(((1, 0), 0, 1), map[1][0] as usize);
    cost_so_far.insert(((0, 1), 1, 1), map[0][1] as usize);

    let directions = [(1,  0), (0,  1), (-1,  0), (0, -1)];
    let get_neibr = |(yi, xi): &Position, these_dirs: std::ops::Range<usize>| -> Neighbors {
        let mut nbrs = Vec::new();
        for (dir, (ym, xm)) in directions.iter().enumerate() {
            if !these_dirs.contains(&dir) {
                continue;
            }
            let y = *yi as i32 + ym;
            let x = *xi as i32 + xm;
            if y >= 0 && x >= 0 {
                let y = y as usize;
                let x = x as usize;
                if y <= goal.0 && x <= goal.1 {
                    nbrs.push(((y, x), dir));
                }
            }
        }

        nbrs
    };

    while let Some((current_node, _)) = candidates.pop() {
        if current_node.position == goal {
            if current_node.dir_count >= min_moves {
                return *cost_so_far.get(&(current_node.position, current_node.dir, current_node.dir_count)).unwrap();
            } else {
                continue;
            }
        }

        for (nghbr, nghbr_dir) in get_neibr(&current_node.position, 0 .. 4).iter() {
            if *nghbr_dir == (current_node.dir + 2) % 4 {
                // Don't go back to where you came from
                continue;
            }
            let this_dir_count = if *nghbr_dir == current_node.dir {
                current_node.dir_count + 1
            } else {
                1
            };
            if this_dir_count > max_moves ||
               (current_node.dir_count < min_moves && *nghbr_dir != current_node.dir)
            {
                continue;
            }

            let new_cost = cost_so_far.get(&(current_node.position, current_node.dir, current_node.dir_count)).unwrap() + map[nghbr.0][nghbr.1] as usize;
            let better_cost = if let Some(cost_now) = cost_so_far.get(&(*nghbr, *nghbr_dir, this_dir_count)) {
                new_cost < *cost_now
            } else {
                true
            };
            if better_cost {
                cost_so_far.insert((*nghbr, *nghbr_dir, this_dir_count), new_cost);
                let est_cost = new_cost + heuristic(nghbr);
                candidates.push(Node { position: *nghbr, dir: *nghbr_dir, dir_count: this_dir_count}, Reverse(est_cost));
            }
        }
    }

    0
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let loss = find_least_loss(&map, (map.len() - 1, map[0].len() - 1), 1, 3);

    println!("Loss: {}", loss);
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let loss = find_least_loss(&map, (map.len() - 1, map[0].len() - 1), 4, 10);

    println!("Loss: {}", loss);
}

fn parse_input(lines: &[String]) -> Map {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|chr|
                chr
                .to_digit(10)
                .unwrap() as Loss
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


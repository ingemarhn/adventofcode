use itertools::Itertools;

type RangeDesc = (usize, usize, usize);

fn parse_line(line: &str) -> RangeDesc {
    line
        .split(' ')
        .map(|num| num.parse::<usize>().unwrap())
        .collect_tuple().unwrap()
}

fn get_ranges(lines_it: &mut std::slice::Iter<String>) -> Vec<RangeDesc> {
    lines_it.next();
    let mut ranges = Vec::new();
    #[allow(clippy::while_let_on_iterator)]
    while let Some(line) = lines_it.next() {
        if line.is_empty() { break; }
        ranges.push(parse_line(line));
    }
    ranges.sort_by(|a, b| a.1.cmp(&b.1));

    ranges
}

fn get_lowest_location(lines: &[String]) -> usize {
    let mut lines_it = lines.iter();
    let mut seeds = lines_it
        .next()
        .unwrap()[7 ..]
        .split(' ')
        .map(|num| num.parse::<usize>().unwrap())
        .collect_vec();
    lines_it.next();

    for _ in 1 ..= 7 {
        let ranges = get_ranges(&mut lines_it);
        for seed in seeds.iter_mut() {
            for (dest_start, src_start, length) in ranges.iter() {
                if (*src_start .. *src_start + *length).contains(seed) {
                    *seed = *dest_start + (*seed - *src_start);
                    break;
                }
            }
        }
    }

    seeds.iter().fold(usize::MAX, |min, s| min.min(*s))
}

fn get_lowest_location_b(lines: &[String]) -> usize {
    let mut lines_it = lines.iter();
    let (mut seeds, _) =
        lines_it
        .next()
        .unwrap()[7 ..]
        .split(' ')
        .map(|num| num.parse::<usize>().unwrap())
        .fold((Vec::new(), 0), |(mut ranges, i), e|
            if i == 0 {
                ranges.push(e .. 0);
                (ranges, 1)
            } else {
                let last = ranges.iter_mut().last().unwrap();
                last.end = last.start + e + 1;
                (ranges, 0)
            }
        );
    lines_it.next();

    for _ in 1 ..= 7 {
        let ranges = get_ranges(&mut lines_it);
        let mut new_ranges = Vec::new();
        for seed in seeds.iter_mut() {
            let mut change_ranges = Vec::new();
            for (dest_start, src_start, length) in ranges.iter() {
                if *src_start > seed.end { break; }
                let src_range = *src_start .. *src_start + *length;
                if src_range.end < seed.start { continue; }

                let src_dest_diff = src_start.abs_diff(*dest_start);
                let fn_src_dest = if dest_start < src_start { usize::saturating_sub } else { usize::saturating_add };

                if src_range.contains(&seed.start) && src_range.contains(&seed.end) {
                    change_ranges.push((seed.start, seed.end, src_dest_diff, fn_src_dest));
                    break;
                } else if src_range.contains(&seed.start) {
                    change_ranges.push((seed.start, src_range.end, src_dest_diff, fn_src_dest));
                } else if src_range.contains(&seed.end) {
                    change_ranges.push((src_range.start, seed.end, src_dest_diff, fn_src_dest));
                } else if seed.start < src_range.start && seed.end > src_range.end {
                    change_ranges.push((src_range.start, src_range.end, src_dest_diff, fn_src_dest));
                }
            }

            if change_ranges.is_empty() { continue; }

            if seed.start < change_ranges[0].0 {
                new_ranges.push(seed.start .. change_ranges[0].0);
            }
            let ch_last = change_ranges.last().unwrap();
            if seed.end > ch_last.1 {
                new_ranges.push(ch_last.1 .. seed.end);
            }

            seed.start = change_ranges[0].3(change_ranges[0].0, change_ranges[0].2);
            seed.end = change_ranges[0].3(change_ranges[0].1, change_ranges[0].2);
            for ch_ran in change_ranges.iter().skip(1) {
                new_ranges.push(ch_ran.3(ch_ran.0, ch_ran.2) .. ch_ran.3(ch_ran.1, ch_ran.2));
            }
        }

        seeds.append(&mut new_ranges);
    }

    seeds.iter().fold(usize::MAX, |min, s| min.min(s.start))
}

fn task_a(lines: &[String]) {
    let min = get_lowest_location(lines);

    println!("Lowest locations number: {}", min);
}

fn task_b(lines: &[String]) {
    let min = get_lowest_location_b(lines);

    println!("Lowest locations number: {}", min);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


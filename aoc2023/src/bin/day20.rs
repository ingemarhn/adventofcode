use std::collections::VecDeque;
use num::integer;

mod internal {
    use std::collections::{HashMap, HashSet};

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    enum PulseType {
        Low,
        High,
    }

    #[derive(Clone, Debug)]
    enum ModuleType {
        Broadcast,
        FlipFlop(PulseType),
        Conjunction(Vec<String>, Vec<PulseType>),
    }

    pub type Modules = HashMap<String, Module>;

    #[derive(Clone, Debug)]
    pub struct Module {
        name: String,
        mtype: ModuleType,
        destinations: Vec<String>,
    }

    impl Module {
        fn new(name: &str, tchar: char, dests: &[&str]) -> Self {
            let mod_type = match tchar {
                '%' => ModuleType::FlipFlop(PulseType::Low),
                '&' => ModuleType::Conjunction(Vec::new(), Vec::new()),
                _   => ModuleType::Broadcast,
            };

            Self { name: name.to_string(), mtype: mod_type, destinations: dests.iter().map(|d| d.to_string()).collect() }
        }

        fn receive(&mut self, pulse: PulseType, sender: &String) -> bool {
            match self.mtype.clone() {
                ModuleType::FlipFlop(current_pt) => if pulse == PulseType::Low {
                    self.mtype = ModuleType::FlipFlop(
                        match current_pt {
                            PulseType::Low  => PulseType::High,
                            PulseType::High => PulseType::Low,
                        }
                    );
                    true
                } else {
                    false
                },
                ModuleType::Conjunction(senders, _) => {
                    let ModuleType::Conjunction(_, ref mut sndrs_values) = self.mtype else { panic!() };
                    let ind = senders.iter().position(|s| s == sender).unwrap();
                    sndrs_values[ind] = pulse;
                    true
                },
                _ => panic!()
            }
        }

        pub fn destinations_len(&self) -> usize {
            self.destinations.len()
        }
    }

    #[derive(Debug)]
    pub struct Pulse {
        ptype: PulseType,
        sender: String,
        destinations: Vec<String>,
    }

    impl Pulse {
        pub fn new(modules: &Modules, name: String) -> Self {
            let module = &modules[&name];
            let ptype = match module.mtype.clone() {
                ModuleType::Broadcast => PulseType::Low,
                ModuleType::FlipFlop(pt) => pt,
                ModuleType::Conjunction(_, senders_values) => if senders_values.iter().all(|v| *v == PulseType::High) {
                    PulseType::Low
                } else {
                    PulseType::High
                }
            };
            Self { ptype, sender: name, destinations: module.destinations.clone() }
        }

        pub fn send(&self, modules: &mut Modules) -> Vec<String> {
            let mut receivers = Vec::new();
            for dest in self.destinations.iter() {
                /* dest "final machine" isn't in modules, thus returning an empty vector */
                modules.entry(dest.to_string())
                    .and_modify(|module|
                        if module.receive(self.ptype, &self.sender) {
                            receivers.push(dest.to_string());
                        }
                    );
            }

            receivers
        }

        pub fn is_low(&self) -> bool {
            self.ptype == PulseType::Low
        }
    }

    pub fn get_block_nodes(modules: &Modules) -> (Vec<&String>, Vec<&String>) {
        let mut top_nodes = Vec::new();

        let mut last = "";
        for module in modules.values() {
            if !modules.contains_key(&module.destinations[0]) {
                last = &module.name;
                break;
            }
        }

        let get_parents = |child: &str| {
            let mut parents = Vec::new();
            for module in modules.values() {
                if module.destinations.contains(&child.to_string()) {
                    parents.push(&module.name);
                }
            }

            parents
        };

        let final_nodes = get_parents(last);

        for node in final_nodes.iter() {
            let mut check_parents = HashSet::new();
            let mut current_node = *node;
            loop {
                let parents = get_parents(current_node);
                if parents.contains(&&"broadcaster".to_string()) {
                    top_nodes.push(current_node);
                    break;
                }

                for paren in parents {
                    if !check_parents.contains(paren) {
                        check_parents.insert(paren);
                        current_node = paren;
                        break;
                    }
                }
            }
        }

        (top_nodes, final_nodes)
    }

    pub fn parse_input(lines: &[String]) -> Modules {
        let mut modules:Modules = HashMap::new();

        for line in lines {
            let mut parts = line.split(&[' ', ','][..]);
            let module = parts.next().unwrap();
            let mut mchrs = module.chars().peekable();
            let tchar = *mchrs.peek().unwrap();
            if tchar != 'b' {
                mchrs.next();
            }
            let name = mchrs.collect::<String>();

            parts.next();
            let dests = parts.filter(|p| !p.is_empty()).collect::<Vec<&str>>();
            let m = Module::new(&name, tchar, &dests);
            modules.insert(name, m);
        }

        for module in modules.clone().values() {
            for dest in &module.destinations {
                modules.entry(dest.clone()).and_modify(|m|
                    if let ModuleType::Conjunction(ref mut senders, ref mut senders_values) = m.mtype {
                        senders.push(module.name.clone());
                        senders_values.push(PulseType::Low);
                    }
                );
            }
        }

        modules
    }
}
use internal::*;

fn run_pulse_cycle(modules_in: &Modules, n_times: usize) -> (usize, usize) {
    let mut count = (0, 0);
    let mut modules = modules_in.clone();
    let mut queue = VecDeque::new();

    for _ in 1 ..= n_times {
        queue.push_back("broadcaster".to_string());
        count.0 += 1;
        while let Some(sender) = queue.pop_front() {
            let pulse = Pulse::new(&modules, sender.clone());
            let new_dests = pulse.send(&mut modules);
            *(if pulse.is_low() {
                &mut count.0
            } else {
                &mut count.1
            }) += modules[&sender].destinations_len();
            for d in new_dests {
                queue.push_back(d);
            }
        }
    }

    count
}

fn run_node_block(modules_in: &Modules, top_node: &str, final_node: &String) -> usize {
    let mut button_presses = 0;
    let mut modules = modules_in.clone();

    'l: loop {
        {
            let pulse = Pulse::new(&modules, "broadcaster".to_string());
            pulse.send(&mut modules);
        }

        let mut queue = VecDeque::new();
        queue.push_back(top_node.to_string());
        button_presses += 1;
        while let Some(sender) = queue.pop_front() {
            let pulse = Pulse::new(&modules, sender.to_owned());
            let new_dests = pulse.send(&mut modules);
            if sender == *final_node && !pulse.is_low() {
                break 'l;
            }
            for d in new_dests {
                queue.push_back(d);
            }
        }
    }

    button_presses
}

fn task_a(lines: &[String]) {
    let modules = parse_input(lines);
    let pulses_count = run_pulse_cycle(&modules, 1000);

    println!("Number = {}", pulses_count.0 * pulses_count.1);
}

fn task_b(lines: &[String]) {
    let modules = parse_input(lines);
    // let final_node = _build_tree(&modules);
    // let presses = _run_backwards(&modules, final_node);
    let (top_nodes, final_nodes) = get_block_nodes(&modules);
    let mut presses = 1;
    for i in 0 .. top_nodes.len() {
        let bp = run_node_block(&modules, top_nodes[i], final_nodes[i]);
        presses = integer::lcm(presses, bp);
    }
    println!("# of button presses = {}", presses);
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

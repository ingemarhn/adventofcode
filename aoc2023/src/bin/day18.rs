mod internal {
    use parse_int::parse;

    pub type DigPlan = Vec<DigInstruction>;
    pub type Position = (isize, isize);
    // pub type GridNaive = Vec<Vec<PositionNaive>>;
    pub type Positions = Vec<Position>;

    #[derive(Debug)]
    enum Direction {
        Up,
        Right,
        Down,
        Left,
    }

    // #[derive(Clone, Copy, Debug, PartialEq)]
    // pub enum Terrain {
    //     Untouched,
    //     DugOut,
    // }

    // #[derive(Clone, Debug)]
    // pub struct PositionNaive {
    //     state: Terrain,
    // }

    // impl PositionNaive {
    //     pub fn new() -> Self {
    //         Self { state: Terrain::Untouched }
    //     }

    //     pub fn dig(&mut self) {
    //         self.state = Terrain::DugOut;
    //     }

    //     pub fn _is_dugout(&self) -> bool {
    //         self.state == Terrain::DugOut
    //     }
    // }

    #[derive(Debug)]
    pub struct DigInstruction {
        direction: Direction,
        count: u32,
    }

    impl DigInstruction {
        pub fn new(dir: char, cnt: &str, color_str: &str, use_color_only: bool) -> Self {
            let (direction, count) = if use_color_only {
                let color = parse::<u32>(color_str).unwrap();
                let direction = match color & 0xf {
                    0 => Direction::Right,
                    1 => Direction::Down,
                    2 => Direction::Left,
                    3 => Direction::Up,
                    _ => panic!()
                };

                (direction, color >> 4)
            } else {
                let direction = match dir {
                    'U' => Direction::Up,
                    'R' => Direction::Right,
                    'D' => Direction::Down,
                    'L' => Direction::Left,
                    _ => panic!()
                };

                let count = cnt.parse::<u32>().unwrap();

                (direction, count)
            };
            Self { direction, count }
        }

        // pub fn count(&self) -> u32 {
        //     self.count
        // }
    }

    // fn get_grid_borders(dig_plan: &DigPlan) -> (isize, isize, isize, isize) {
    //     let (maxr, maxc, minr, minc, _, _) = dig_plan
    //     .iter()
    //     .fold((0, 0, 0, 0, 0, 0), |(maxr, maxc, minr, minc, nr, nc), instr| {
    //         let (nrx, ncx) = match instr.direction {
    //             Direction::Up => (nr - instr.count as isize, nc),
    //             Direction::Right => (nr, nc + instr.count as isize),
    //             Direction::Down => (nr + instr.count as isize, nc),
    //             Direction::Left => (nr, nc - instr.count as isize),
    //         };
    //         (maxr.max(nrx), maxc.max(ncx), minr.min(nrx), minc.min(ncx), nrx, ncx)
    //     });

    //     (maxr, maxc, minr, minc)
    // }

    // pub fn _get_grid_naive(dig_plan: &DigPlan) -> (GridNaive, isize, isize) {
    //     let (hr, hc, lr, lc) = get_grid_borders(dig_plan);
    //     let offs_r = if lr < 0 { lr.abs() } else { 0 };
    //     let offs_c = if lc < 0 { lc.abs() } else { 0 };
    //     let mut grid = GridNaive::new();
    //     let grid_row = (lc + offs_c ..= hc + offs_c).map(|_| PositionNaive::new()).collect::<Vec<_>>();
    //     for _ in lr + offs_r ..= hr + offs_r {
    //         grid.push(grid_row.clone());
    //     }

    //     (grid, offs_r, offs_c)
    // }

    pub fn get_grid_corners(dig_plan: &DigPlan) -> Positions {
        dig_plan
            .iter()
            .fold(Positions::from([(0, 0)]), |mut poss, instr| {
                let mv = match instr.direction {
                    Direction::Up => (-1, 0),
                    Direction::Right => (0, 1),
                    Direction::Down => (1, 0),
                    Direction::Left => (0, -1),
                };
                let prev = poss.last().unwrap();
                poss.push((prev.0 + mv.0 * instr.count as isize, prev.1 + mv.1 * instr.count as isize));

                poss
            } )
    }

    // pub fn get_delta(instr: &DigInstruction) -> (isize, isize) {
    //     match instr.direction {
    //         Direction::Up => (-1, 0),
    //         Direction::Right => (0, 1),
    //         Direction::Down => (1, 0),
    //         Direction::Left => (0, -1),
    //     }
    // }
}
// use std::collections::VecDeque;

use internal::*;

// fn _dig_edges_naive(dig_plan: &DigPlan) -> GridNaive {
//     let (mut grid, offs_r, offs_c) = _get_grid_naive(dig_plan);
//     let mut position = (offs_r, offs_c);

//     for instr in dig_plan {
//         let (dr, dc) = get_delta(instr);
//         for _ in 0 .. instr.count() {
//             position = (position.0 + dr, position.1 + dc);
//             grid[position.0 as usize][position.1 as usize].dig()
//         }
//     }
//     grid
// }

// fn _dig_lagoon_naive(grid: &GridNaive) -> usize {
//     let top_left = grid[0].iter().position(|c| c._is_dugout()).unwrap();
//     let mut count_dugout =
//         grid
//         .iter()
//         .fold(0, |tot_count, r|
//             r
//             .iter()
//             .fold(tot_count, |r_count, p|
//                 r_count + if p._is_dugout() { 1 } else { 0 }
//             )
//         );

//     let mut queue = VecDeque::new();

//     let first = (1_isize, top_left as isize + 1);
//     queue.push_back(first);
//     let mut visited = vec![first];

//     while let Some(pos) = queue.pop_front() {
//         count_dugout += 1;

//         for (dr, dc) in [(1, 0), (0, 1), (-1, 0), (0, -1)] {
//             let n_pos = (pos.0 + dr, pos.1 + dc);
//             if !visited.contains(&n_pos) && !grid[n_pos.0 as usize][n_pos.1 as usize]._is_dugout() {
//                 visited.push(n_pos);
//                 queue.push_back(n_pos);
//             }
//         }
//     }

//     count_dugout
// }

fn dig_lagoon(positions: &Positions) -> isize {
    // https://en.m.wikipedia.org/wiki/Shoelace_formula
    let calc_column = |i1: usize, i2: usize| positions[i1].0 * positions[i2].1 - positions[i1].1 * positions[i2].0;
    let segment = |i1: usize, i2: usize|
        if positions[i1].0 != positions[i2].0 {
            (positions[i1].0 - positions[i2].0).abs()
        } else {
            (positions[i1].1 - positions[i2].1).abs()
        };
    let mut a2 = 0;
    let mut perimeter = 0;
    for i in 0 .. positions.len() - 1 {
        a2 += calc_column(i, i + 1);
        perimeter += segment(i, i + 1);
    }
    a2 += calc_column(positions.len() - 1, 0);
    perimeter += segment(positions.len() - 1, 0);
    a2.abs() / 2 + perimeter / 2 + 1
}

fn task_a(lines: &[String]) {
    let dig_plan = parse_input(lines, false);
    let positions = get_grid_corners(&dig_plan);
    let n_cbm = dig_lagoon(&positions);

    println!("cubic meters of lava = {}", n_cbm);
}

fn task_b(lines: &[String]) {
    let dig_plan = parse_input(lines, true);
    let positions = get_grid_corners(&dig_plan);
    let n_cbm = dig_lagoon(&positions);

    println!("cubic meters of lava = {}", n_cbm);
}

fn parse_input(lines: &[String], use_color: bool) -> DigPlan {
    lines
        .iter()
        .map(|line| {
            let mut elems = line.split_ascii_whitespace();
            DigInstruction::new(
                elems.next().unwrap().chars().next().unwrap(),
                elems.next().unwrap(),
                &["0x", &elems.next().unwrap()[2 .. 8]].concat(),
                use_color
            )
        })
        .collect::<Vec<_>>()
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}


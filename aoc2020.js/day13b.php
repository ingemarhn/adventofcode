<?php
//$input = file('temp/s13', FILE_IGNORE_NEW_LINES);
$input = file('/data/Google Drive/adventofcode/2020/input-day-13.txt', FILE_IGNORE_NEW_LINES);
$factors = explode(',', $input[1]);
$timestamp = 0;
$add = 1;
$step = 0;

while(true){
	if($factors[$step] == 'x'){
		$step++;
		continue;
	}
	if(($timestamp + $step) % $factors[$step] == 0){
		$add *= $factors[$step];
		$step++;
	}
	if($step == count($factors)){
		break;
	}
	$timestamp+= $add;
}

echo $timestamp . "\n";
return;
?>

'use strict';
// https://adventofcode.com/2020

const functions = []

// Find two numbers in input that sums up to 2020. Multiply these two numbers and show all involved figures.
functions["d1a"] = (data) => {
    var iData = []
    for (let i in data) {
        iData.push(Number(data[i]))
    }

    for (let i = 0; i < iData.length - 1; i++) {
        for (let j = i + 1; j < iData.length; j++) {
            if (iData[i] + iData[j] === 2020) {
                console.log(iData[i], iData[j], iData[i] * iData[j])
            }
        }
    }
}

// Find three numbers in input that sums up to 2020. Multiply these three numbers and show all involved figures.
functions["d1b"] = (data) => {
    var iData = []
    for (let i in data) {
        iData.push(Number(data[i]))
    }

    for (let i = 0; i < iData.length - 2; i++) {
        for (let j = i + 1; j < iData.length - 1; j++) {
            if (iData[i] + iData[j] < 2020) {
                for (let k = j + 1; k < iData.length; k++) {
                    if (iData[i] + iData[j] + iData[k] === 2020) {
                        console.log(
                            iData[i],
                            iData[j],
                            iData[k],
                            iData[i] * iData[j] * iData[k]
                        )
                    }
                }
            }
        }
    }
}

// Check passwords: n-m c: pwd
// n to m occurrences of c in pwd
functions["d2a"] = (data) => {
    var count = 0
    for (const line of data) {
        const [, min, max, chr, pwd] = line.match(/(\d+)-(\d+)\s+(.):\s+(.*)/)
        const diff = pwd.length - pwd.replaceAll(chr, "").length
        if (min <= diff && diff <= max) {
            count++
        }
    }

    console.log(count)
}

// Check passwords: n-m c: pwd
// Character c in either pos n or pos m. Exactly one occurrance.
functions["d2b"] = (data) => {
    var count = 0
    for (const line of data) {
        const [, p1, p2, chr, pwd] = line.match(/(\d+)-(\d+)\s+(.):\s+(.*)/)
        const [c1, c2] = [pwd[Number(p1) - 1], pwd[Number(p2) - 1]]
        var cnt = 0
        if (c1 === chr) {
            cnt++
        }
        if (c2 === chr) {
            cnt++
        }
        if (cnt === 1) {
            count++
        }
    }

    console.log(count)
}

function hlp3_1(data, steps) {
    const nLines = data.length
    const lineLength = data[0].length
    const reqLineLength = nLines * steps - 2

    // Make the map large enough
    for (let i = 0; i < nLines; i++) {
        data[i] = data[i].repeat((reqLineLength / lineLength + 0.5).toFixed())
    }

    return data
}

function hlp3_2(data, ra, ca) {
    var count = 0
    var [r, c] = [0, 0]
    while (r < data.length) {
        if (data[r][c] === '#') {
            count++
        }
        r += ra
        c += ca
    }

    return count
}

// Find how many trees you will encounter passing through given map. Three right, one down until bottom is reached.
functions["d3a"] = (data) => {
    var data = hlp3_1(data, 3)

    const count = hlp3_2(data, 1, 3)

    console.log(count)
}

// Find how many trees you will encounter passing through given map in five different paths.
functions["d3b"] = (data) => {
    var data = hlp3_1(data, 7)

    const addOns = [
        [1, 1],
        [1, 3],
        [1, 5],
        [1, 7],
        [2, 1],
    ]

    var counts = []
    var countM = 1
    for (const add of addOns) {
        const cnt = hlp3_2(data, add[0], add[1])
        counts.push(cnt)
        countM *= cnt
    }

    console.log(counts)
    console.log(countM)
}

function getHash4() {
    // "cid" may exist, but since it's optional we don't care if it exists or not
    return { "ecl": null, "pid": null, "eyr": null, "hcl": null, "byr": null, "iyr": null, "hgt": null }
}

const chkPPdata = function (ppData_) {
    for (let key in ppData_) {
        if (ppData_[key] === null) {
            return false
        }
    }

    return true
}

// Check text file with passport data for valid passports
functions["d4a"] = (data) => {
    var ppData = getHash4()
    var count = 0
    for (const line of data) {
        if (line.trim() === '') {
            if (chkPPdata(ppData)) {
                count++
            }

            ppData = getHash4()

            continue
        }

        for (const el of line.split(' ')) {
            if (mtch = el.match(/(\w+):(\S+)/)) {
                ppData[mtch[1]] = mtch[2]
            }
        }
    }
    if (chkPPdata(ppData)) {
        count++
    }

    console.log(count)
}

/*
    byr (Birth Year) - four digits at least 1920 and at most 2002.
    iyr (Issue Year) - four digits at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.
*/
const chkPPdataExt = function (ppData_) {
    const byr = Number(ppData_.byr)
    if ((byr < 1920) || (byr > 2002)) {
        return 'byr'
    }
    const iyr = Number(ppData_.iyr)
    if ((iyr < 2010) || (iyr > 2020)) {
        return 'iyr'
    }
    const eyr = Number(ppData_.eyr)
    if ((eyr < 2020) || (eyr > 2030)) {
        return 'eyr'
    }
    var mt = ppData_.hgt.match(/(\d*)(cm|in)(\s|$)/)
    if (mt === null) {
        return 'hgt 1'
    }
    if (mt[2] === 'in') {
        mt[1] = (mt[1] * 2.54).toFixed()
    }
    if ((mt[1] < 150) || (mt[1] > 193)) {
        return 'hgt 2'
    }
    mt = ppData_.hcl.match(/#[0-9a-f]{6}(?:\s|$)/)
    if (mt === null) {
        return 'hcl'
    }
    mt = ppData_.ecl.match(/(amb|blu|brn|gry|grn|hzl|oth)/)
    if (mt === null) {
        return 'ecl'
    }
    mt = ppData_.pid.match(/[0-9]{9}(?:\s|$)/)
    if (mt === null) {
        return 'pid'
    }

    return 'ok'
}

// Check text file with passport data for valid passports, harder requirements
functions["d4b"] = (data) => {
    var ppData = getHash4()
    var count = 0

    const chkValid = function (ppData_) {
        var isValid = chkPPdata(ppData_)
        if (isValid) {
            rv = chkPPdataExt(ppData_)
            isValid = rv === "ok"
        }

        return isValid
    }

    for (const line of data) {
        if (line.trim() === "") {
            if (chkValid(ppData)) {
                count++
            }

            ppData = getHash4()

            continue
        }

        for (const el of line.split(" ")) {
            if ((mtch = el.match(/(\w+):(\S+)/))) {
                ppData[mtch[1]] = mtch[2]
            }
        }
    }
    if (chkValid(ppData)) {
        count++
    }

    console.log(count)
}

function gID(bStr) {
    var row = bStr.substr(0, 7)
    var col = bStr.substr(7)

    row = row.replaceAll('F', '0')
    row = row.replaceAll('B', '1')
    col = col.replaceAll('L', '0')
    col = col.replaceAll('R', '1')
    row = parseInt(row, 2)
    col = parseInt(col, 2)

    return row * 8 + col
}

// Calculate the highest index number from seats in an airplane
functions["d5a"] = (data) => {
    var hIndex = 0

    // Convert all letters to binary
    for (const line of data) {
        const sID = gID(line)
        if (sID > hIndex) {
            hIndex = sID
        }
    }

    console.log(hIndex)
}

// Find the missing index number from seats in an airplane
functions["d5b"] = (data) => {
    var hIndex = 0
    var lIndex = 999999
    var IDs = []

    // Convert all letters to binary
    for (const line of data) {
        const sID = gID(line)
        IDs[sID] = 1
        if (sID > hIndex) {
            hIndex = sID
        }
        if (sID < lIndex) {
            lIndex = sID
        }
    }

    var missInd = 0
    for (ind = lIndex; ind < hIndex; ind++) {
        //console.log(ind + ': ' + IDs[ind])
        if (IDs[ind + 1] === undefined) {
            missInd = Number(ind) + 1
            break
            //console.log(Number(ind) + 1 + ': ' + IDs[ind+1])
        }
    }

    console.log(missInd)
}

function countYes(data) {
    let cnt = 0
    for (const v of data) {
        cnt += v
    }

    return cnt
}

// Calculate how many answers to 26 questions that anyone in a group has answered yes to
functions["d6a"] = (data) => {
    const qData = Array(26).fill(0)
    var count = 0

    for (const line of data) {
        if (line.trim() === '') {
            count += countYes(qData)
            qData.fill(0)

            continue
        }

        for (const ch of line.split('')) {
            qData[ch.charCodeAt(0) - 97] = 1
        }
    }
    count += countYes(qData)

    console.log(count)
}

// Calculate how many answers to 26 questions that everyone in a group has answered yes to
functions["d6b"] = (data) => {
    const qData = Array(26).fill(1)
    var count = 0

    for (const line of data) {
        d(line)
        const q_Data = Array(26).fill(0)
        if (line.trim() === '') {
            count += countYes(qData)
            qData.fill(1)

            continue
        }

        for (const ch of line.split('')) {
            q_Data[ch.charCodeAt(0) - 97] = 1
        }
        for (let i = 0; i < 26; i++) {
            qData[i] *= q_Data[i]
        }
        d(q_Data)
        d(qData)
    }
    count += countYes(qData)

    console.log(count)
}

/*
class Bag {
    #containers
    #content
    constructor() {
        this.#containers = new Set()
        this.#content = {}
    }

    addContainer(color) {
        this.#containers.add(color)
    }

    addContent(color, count) {
        this.#content[color] = count;
    }

    get containerCount() {
        return this.#containers.size
    }

    get contentCount() {
        return this.#content.size
    }

    get containers() {
        return this.#containers
    }

    get content() {
        return this.#content
    }
}
*/
class Bag {
    constructor() {
        this.containers = new Set()
        this.content = {}
    }

    addContainer(color) {
        this.containers.add(color)
    }

    addContent(color, count) {
        this.content[color] = count;
    }

    get containerCount() {
        return this.containers.size
    }

    get contentCount() {
        return Object.keys(this.content).length
    }
}

function getBags(data) {
    const containerRgx = /(\w+ \w+) bags contain/
    const contentRgx = /( (\d+) (\w+ \w+) bags?[,.])|no other bags/g
    var bags = {}

    for (const line of data) {
        const containerMtc = line.match(containerRgx)
        const contentMtc = line.matchAll(contentRgx)

        if (bags[containerMtc[1]] === undefined) {
            bags[containerMtc[1]] = new Bag()
        }
        for (const mtc of contentMtc) {
            if (mtc[2] !== undefined) {
                if (bags[mtc[3]] === undefined) {
                    bags[mtc[3]] = new Bag()
                }
                bags[mtc[3]].addContainer(containerMtc[1])
                bags[containerMtc[1]].addContent(mtc[3], mtc[2])
            }
        }
    }

    return bags
}

// Calculate how many bags that can carry the 'shiny gold' bag
functions["d7a"] = (data) => {
    const bags = getBags(data)

    var containerCount = new Set();
    (function countContainers(color, cases, containers) {
        if (!containers.has(color)) {
            containers.add(color)
        }
        if (cases[color].containerCount === 0) {
            return
        }

        for (cont of cases[color].containers) {
            countContainers(cont, cases, containers)
        }
    })('shiny gold', bags, containerCount)

    console.log(containerCount.size - 1)
    d(containerCount)
}

// Calculate how many bags the 'shiny gold' bag must carry
functions["d7b"] = (data) => {
    const bags = getBags(data)

    var contentCounter =
        (function countContent(color, cases) {
            if (cases[color].contentCount === 0) {
                return 1
            }

            var count = 0
            for (const key of Object.keys(cases[color].content)) {
                const bagCount = cases[color].content[key]
                const ccount = countContent(key, cases)
                count += ccount * bagCount
            }

            count++

            return count
        })('shiny gold', bags)

    console.log(contentCounter - 1)
}

// Find the eternal loop and return value of accumulator
functions["d8a"] = (instructions) => {
    var accumulator = 0
    var operations = {}
    operations['nop'] = (instrPtr, acc, _val) => [++instrPtr, acc]
    operations['acc'] = (instrPtr, acc, val) => {
        acc += val
        return [++instrPtr, acc]
    }
    operations['jmp'] = (instrPtr, acc, val) => [instrPtr + val, acc]
    operations['vis'] = (_instrPtr, acc, _val) => [-1, acc]

    var instructionPtr = 0
    while (instructionPtr >= 0) {
        const iinstructionPtr = instructionPtr;
        const [instr, value] = instructions[instructionPtr].match(/(\w+) ([+-]\d+)/).slice(1, 3);
        [instructionPtr, accumulator] = operations[instr](instructionPtr, accumulator, Number(value));
        instructions[iinstructionPtr] = 'vis +0'
    }

    console.log(accumulator)
}

// Find the eternal loop and return value of accumulator
functions["d8b"] = (instructions) => {
    var operations = {};
    operations['nop'] = (instrPtr, acc, _val) => [++instrPtr, acc];
    operations['acc'] = (instrPtr, acc, val) => {
        acc += val;
        return [++instrPtr, acc];
    }
    operations['jmp'] = (instrPtr, acc, val) => [instrPtr + val, acc];

    var accumulator = 0;
    var instructionPtr = 0;
    var visitedPtrs = new Set();

    var lastModPtr = -1;
    // Save original instructions (using spread operator)
    const instructionsOrig = [...instructions];
    while (instructionPtr < instructions.length) {
        visitedPtrs.add(instructionPtr);
        const [instr, value] = instructions[instructionPtr].match(/(\w+) ([+-]\d+)/).slice(1, 3);
        [instructionPtr, accumulator] = operations[instr](instructionPtr, accumulator, Number(value));
        if (visitedPtrs.has(instructionPtr)) {
            // Restore previously modified instruction
            if (lastModPtr >= 0) {
                instructions[lastModPtr] = instructionsOrig[lastModPtr];
            }

            // Find an operator to modify
            for (let i = lastModPtr + 1; i < instructions.length; i++) {
                let modDone = false;
                switch (instructions[i].substr(0, 3)) {
                    case 'nop':
                        instructions[i] = instructions[i].replaceAll('nop', 'jmp');
                        modDone = true;
                        break;

                    case 'jmp':
                        instructions[i] = instructions[i].replaceAll('jmp', 'nop');
                        modDone = true;
                        break;
                }

                if (modDone) {
                    lastModPtr = i;
                    break
                }
            }

            accumulator = 0;
            instructionPtr = 0;
            visitedPtrs = new Set();
        }
    }

    console.log(accumulator);
}

// Find the first invalid number in the XMAS cypher
functions["d9a"] = (xmasNumbers, preambleLength) => {
    if (preambleLength === undefined) {
        console.log('Usage: 9a dataFile preambleLength');
        return
    }
    var firstInvalidNumber;

    // Loop over all numbers after the initial preamble
    for (let i = preambleLength; i < xmasNumbers.length; i++) {
        numbToTest = Number(xmasNumbers[i]);

        const retval = ((xmasNumbs, preambLen) => {
            // For each number, add two of the numbers in the preamble to see if it sums up to the current number in the loop
            for (let m = i - preambLen; m < (i - 1); m++) {
                const n1 = Number(xmasNumbs[m]);
                for (let n = m + 1; n < i; n++) {
                    const n2 = Number(xmasNumbs[n]);
                    if ((n1 + n2) === numbToTest) {
                        return 0;
                    }
                }
            }

            return numbToTest;
        })(xmasNumbers, preambleLength);

        if (retval > 0) {
            firstInvalidNumber = retval;
            break
        }
    }

    console.log(firstInvalidNumber);
}

// Find a contiguous set of numbers in the XMAS cypher list that adds up to the faulty number from the above test
functions["d9b"] = (xmasNumbers, faultyNumberIn) => {
    const faultyNumber = Number(faultyNumberIn);
    var min = 99999999999;
    var max = 0;

    // Loop over all numbers in the list
    for (let i = 0; i < (xmasNumbers.length - 1); i++) {
        const base = i;
        var contigList;
        var contigSum = Number(xmasNumbers[base]);
        var index = base + 1;

        do {
            contigSum += Number(xmasNumbers[index]);
            index++;
        } while ((contigSum < faultyNumber) && (index < xmasNumbers.length));

        if (contigSum > faultyNumber) {
            continue
        }

        if (contigSum === faultyNumber) {
            for (let j = base; j < index; j++) {
                thisNumb = Number(xmasNumbers[j]);
                if (thisNumb < min) {
                    min = thisNumb
                }
                if (thisNumb > max) {
                    max = thisNumb
                }
            }

            break
        }
    }

    console.log(min + max);
}

// Calculate in how many ways the different adapters can be arranged
functions["d10b"] = (data) => {
    // Sort the numbers as numbers (not strings)
    for (let i = 0; i < data.length; i++) {
        data[i] = Number(data[i]);
    }
    data[data.length] = 0;
    data.sort(((a, b) => {
        return a - b
    }));
    data[data.length] = data[data.length - 1] + 3;

    var i = 0;
    var nVariants = 1;
    while (i < data.length) {
        var s = i;
        while ((data[i + 1] - data[i]) === 1) {
            i++;
        }
        const nBinaries = i - s - 1;
        if (nBinaries > 0) {
            switch (nBinaries) {
                case 1:
                case 2:
                    nVariants *= 1 << nBinaries;
                    break
                case 3:
                    nVariants *= (1 << nBinaries) - 1;
            }
        }
        i++;
    }

    console.log(nVariants)
}

// Create a matrix that includes walls apart from seats and floor
function createAndFillMatrix(seatsData) {
    const rows = seatsData.length + 2;
    const slots = seatsData[0].length + 2;
    const seats = Array(rows).fill().map(() => Array(slots));

    // Fill the seats
    seats[0].fill('W');
    seats[rows - 1].fill('W');
    for (let i = 1; i < rows - 1; i++) {
        seats[i][0] = 'W';
        seats[i][slots - 1] = 'W';
    }
    for (let i = 1; i < rows - 1; i++) {
        for (let j = 1; j < slots - 1; j++) {
            seats[i][j] = seatsData[i - 1][j - 1];
        }
    }

    return [rows, slots, seats];
}

const copyMatrix = (matr) => {
    const newMatrix = [...matr];
    for (let i = 0; i < matr.length; i++) {
        newMatrix[i] = [...matr[i]];
    }

    return newMatrix;
}

const updateSeats = (lseats, nRows, nSlots, charComp, charSwitch, limit, oper, fCountFree) => {
    let newSeats = copyMatrix(lseats);
    for (let i = 1; i < nRows - 1; i++) {
        for (let j = 1; j < nSlots - 1; j++) {
            if (lseats[i][j] === charComp) {
                let nFree = fCountFree(lseats, i, j);
                if (eval(nFree + oper + limit)) {
                    newSeats[i][j] = charSwitch;
                }
            }
        }
    }

    return copyMatrix(newSeats);
}

// Calculate number of free seats in waiting area following two rules
functions["d11a"] = (data) => {
    let [rows, slots, seats] = createAndFillMatrix(data);

    const countFree = (lseats, r, s) => {
        const neighbors = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]];
        let nFree = 0;
        for (inds of neighbors) {
            const [rp, sp] = inds;
            if (['W', 'L', '.'].includes(lseats[r + rp][s + sp])) {
                nFree++;
            }
        }

        return nFree;
    }

    let doLoop = true
    while (doLoop) {
        const newSeats1 = updateSeats(seats, rows, slots, 'L', '#', 8, '===', countFree);
        const newSeats2 = updateSeats(newSeats1, rows, slots, '#', 'L', 4, '<=', countFree);

        if (seats.toString() === newSeats2.toString()) {
            doLoop = false;
        }
        seats = copyMatrix(newSeats2);
    }

    let nOcc = 0;
    for (r of seats) {
        for (s of r) {
            if (s === '#') {
                nOcc++;
            }
        }
    }

    console.log(nOcc)
}

// Calculate number of free seats in waiting area following two rules
functions["d11b"] = (data) => {
    let [rows, slots, seats] = createAndFillMatrix(data);

    const countFree = (lseats, r, s) => {
        const neighbors = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]];
        let nFree = 0;
        for (inds of neighbors) {
            const [rp, sp] = inds;
            let isFree = true;
            for (let rt = r + rp, st = s + sp; ((rt > 0) && (rt < lseats.length - 1)) && ((st > 0) && (st < lseats[0].length - 1)); rt += rp, st += sp) {
                isFree = true;
                if (lseats[rt][st] === 'L') {
                    break
                }
                if (lseats[rt][st] === '#') {
                    isFree = false;
                    break;
                }
            }
            if (isFree) {
                nFree++;
            }
        }

        return nFree;
    }

    let doLoop = true
    while (doLoop) {
        const newSeats1 = updateSeats(seats, rows, slots, 'L', '#', 8, '===', countFree);
        const newSeats2 = updateSeats(newSeats1, rows, slots, '#', 'L', 3, '<=', countFree);

        if (seats.toString() === newSeats2.toString()) {
            doLoop = false;
        }
        seats = copyMatrix(newSeats2);
    }

    let nOcc = 0;
    for (r of seats) {
        for (s of r) {
            if (s === '#') {
                nOcc++;
            }
        }
    }

    console.log(nOcc)
}

// Calculate the Manhattan position after given instructions
functions["d12a"] = (data) => {
    // Current position
    const currentPosition = { 'NS': 0, 'EW': 0 };
    // Current direction
    var currentDirection = 'E';
    // Move in some direction
    const move = (currPos, direction, units) => {
        switch (direction) {
            case 'E': currPos['EW'] += units; break
            case 'W': currPos['EW'] -= units; break
            case 'N': currPos['NS'] += units; break
            case 'S': currPos['NS'] -= units; break
        }
    }

    // Turn left or right
    const turn = (currDir, direction, degrees) => {
        const directions = ['E', 'S', 'W', 'N'];
        let ind = directions.indexOf(currDir);
        const nPrevTurns = degrees / 90;
        const dirAsNum = direction === 'L' ? -1 : 1;
        ind = (ind + nPrevTurns * dirAsNum) % 4;
        if (ind < 0) {
            ind += 4;
        }

        return directions[ind];
    }

    for (instr of data) {
        let [, action, value] = instr.match(/(.)(\d+)/);
        value = Number(value);
        if (['R', 'L'].includes(action)) {
            currentDirection = turn(currentDirection, action, value);
            continue
        }

        const direction = action === 'F' ? currentDirection : action;
        move(currentPosition, direction, value);
    }

    console.log(currentPosition);
    console.log(Math.abs(currentPosition['NS']) + Math.abs(currentPosition['EW']));
}

functions["d12b"] = (data) => {
    // Current position
    const currentPosition = { 'NS': 0, 'EW': 0 };
    const currentWaypoint = { 'NS': 1, 'EW': 10 };

    // Move to the waypoint a number of times
    const moveShip = (currWaypoint, currPosition, times) => {
        currPosition['NS'] += currWaypoint['NS'] * times;
        currPosition['EW'] += currWaypoint['EW'] * times;
    }

    const moveWaypoint = (currWaypoint, direction, units) => {
        switch (direction) {
            case 'E': currWaypoint['EW'] += units; break
            case 'W': currWaypoint['EW'] -= units; break
            case 'N': currWaypoint['NS'] += units; break
            case 'S': currWaypoint['NS'] -= units; break
        }
    }

    // Turn waypoint left or right
    const turnWaypoint = (currWaypoint, direction, degrees) => {
        const locCurrWaypoint = { ...currWaypoint };
        const directions = ['E', 'S', 'W', 'N'];
        const nPrevTurns = degrees / 90;
        const dirAsNum = direction === 'L' ? -1 : 1;
        for (wpInd of ['NS', 'EW']) {
            const currDirection = locCurrWaypoint[wpInd] > 0 ? wpInd[0] : wpInd[1];
            let ind = directions.indexOf(currDirection);
            ind = (ind + nPrevTurns * dirAsNum) % 4;
            if (ind < 0) {
                ind += 4;
            }
            switch (directions[ind]) {
                case 'E': currWaypoint['EW'] = Math.abs(locCurrWaypoint[wpInd]); break
                case 'W': currWaypoint['EW'] = -Math.abs(locCurrWaypoint[wpInd]); break
                case 'N': currWaypoint['NS'] = Math.abs(locCurrWaypoint[wpInd]); break
                case 'S': currWaypoint['NS'] = -Math.abs(locCurrWaypoint[wpInd]); break
            }
        }
    }

    for (instr of data) {
        let [, action, value] = instr.match(/(.)(\d+)/);
        value = Number(value);
        switch (action) {
            case 'R':
            case 'L':
                turnWaypoint(currentWaypoint, action, value);
                break;

            case 'F':
                moveShip(currentWaypoint, currentPosition, value);
                break;

            default:
                moveWaypoint(currentWaypoint, action, value);
        }
    }

    console.log(Math.abs(currentPosition['NS']) + Math.abs(currentPosition['EW']));
}

functions["d13a"] = (data) => {
    const timeStamp = Number(data[0]);
    let departAt = 9999999999;
    let busToRide = 0;
    for (let id of data[1].split(',')) {
        if (id === 'x') {
            continue
        }

        const busID = Number(id);
        const depart = Math.ceil(timeStamp / busID) * busID;
        if (depart < departAt) {
            departAt = depart;
            busToRide = busID;
        }
    }

    console.log((departAt - timeStamp) * busToRide);
}

functions["d13b"] = (data) => {
    const buses = data[1].split(',');

    let startVal = 0;
    let timeStep = parseInt(buses[0]);
    let addMins = 0;
    for (const bus of buses.slice(1)) {
        addMins++;
        if (bus === 'x') {
            continue
        }

        let timeStepNext = parseInt(bus)
        let mult;
        for (mult = 1; (startVal + timeStep * mult + addMins) % timeStepNext !== 0; mult++) { }
        startVal += timeStep * mult;
        timeStep *= timeStepNext;
    }

    console.log(startVal);
}

functions["d14a"] = (data) => {
    // Memory address space
    const memory = [];

    // Current mask
    const getMasks = (maskString) => {
        return {
            or: BigInt('0b' + maskString.replaceAll('X', '0')),
            and: BigInt('0b' + maskString.replaceAll('X', '1'))
        }
    }

    // Apply mask
    const applyMask = (value, mask) => {
        return value & mask['and'] | mask['or'];
    }

    data.forEach((line) => {
        [, , instr, index, value] = line.match(/((\S+?)(?:\[(\d+)\])?) = (.*)/);
        if (instr === 'mask') {
            masks = getMasks(value);
        } else {
            memory[index] = applyMask(BigInt(value), masks);
        }
    });

    let sum = BigInt(0);
    memory.forEach((elem) => {
        sum += elem;
    })
    console.log(sum);
}

functions["d14b--"] = (data) => {
    // Memory address space
    const memory = [];

    // Current mask
    // mask = 000000000000000000000000000000X1001X
    const getMasks = (maskString) => {
        const andMask = BigInt('0b' + maskString.replaceAll('0', '1').replaceAll('X', '0'));
        const baseOrMask = maskString.replaceAll('X', '0');
        const baseOrMaskArr = baseOrMask.split('');

        const xPoss = maskString.split('').reduce((acc, ch, ind) => {
            if (ch === 'X') {
                acc.push(ind)
            }
            return acc
        }, []);

        const orMasks = [];
        for (let i = 0; i < 2 ** xPoss.length; i++) {
            orMasks.push([...baseOrMaskArr]);
        }
        let modder = 1;
        xPoss.forEach((xInd) => {
            let zeroOne = '1';
            for (let i = 0; i < 2 ** xPoss.length; i++) {
                if ((i + 1) % modder === 0) {
                    zeroOne = zeroOne === '0' ? '1' : '0'
                }
                orMasks[i][xInd] = zeroOne;
            }
            modder *= 2;
        });

        for (let i in orMasks) {
            orMasks[i] = BigInt('0b' + orMasks[i].join(''));
        }

        return {
            or: orMasks,
            and: andMask
        };
    }

    // Apply mask
    const applyMask = (value, mask) => {
        const retVals = []
        const base = value & mask['and'];
        mask['or'].forEach((msk) => {
            retVals.push(base | msk);
        });

        return retVals;
    }

    data.forEach((line, ix) => {
        [, , instr, index, value] = line.match(/((\S+?)(?:\[(\d+)\])?) = (.*)/);
        if (instr === 'mask') {
            masks = getMasks(value);
        } else {
            applyMask(BigInt(index), masks).forEach(idx => {
                memory[idx] = value
            });
            //applyMask(BigInt(index), masks).forEach(idx => memory[idx] = value);
        }
    });

    let sum = 0n;
    memory.forEach((elem, ix) => {
        sum += BigInt(elem);
    })
    console.log(sum);
}

functions["d14b"] = (data) => {
    const getMasks = (maskString) => {
        return {
            or: BigInt('0b' + maskString.replaceAll('X', '0')),
            and: BigInt('0b' + maskString.replaceAll('0', '1').replaceAll('X', '0'))
        }
    }

    const getZeroBits = (mask => {
        return [...new Array(36)].reduce((acc, _, i) => {
            if (!(mask & (1n << BigInt(i)))) {
                acc.push(i)
            }
            return acc
        }, []);
    });

    // Apply mask
    const applyMasks = (value, masks, zerBts) => {
        let base = value & masks['and'] | masks['or'];
        retVals = zerBts.reduce((accum, bit) => {
            // Double the length of the accumulator
            let accuLen = accum.length;
            for (let i = 0; i < accuLen; i++) {
                accum.push(accum[i]);
            }

            // Insert 1's in given bit positions in half of the elements
            accuLen = accum.length;
            for (let i = accuLen / 2; i < accuLen; i++) {
                accum[i] |= 1n << BigInt(bit);
            }

            return accum;
        }, [base]);
        return retVals;
    }

    let masks;
    let zeroBits;
    // Memory address space
    const memory = {};
    data.forEach((line) => {
        [, , instr, index, value] = line.match(/((\S+?)(?:\[(\d+)\])?) = (.*)/);
        if (instr === 'mask') {
            masks = getMasks(value);
            zeroBits = getZeroBits(masks['and']);
        } else {
            applyMasks(BigInt(index), masks, zeroBits).forEach(idx => memory[idx] = Number(value));
        }
    });

    let sum = 0;
    //memory.forEach((elem) => {
    Object.keys(memory).forEach(key => sum += memory[key]);
    //memory.forEach((v, i) => console.log(`${i}: ${v}`));
    console.log(sum);
}

functions["d15a"] = (data) => {
    let initLastSpoken;
    const initNumbersAndTurns = data[0].split(',').reduce((numbs, n, i) => {
        let nn = parseInt(n);
        numbs[nn] = i + 1;
        initLastSpoken = nn;

        return numbs;
    }, {});
    delete initNumbersAndTurns[initLastSpoken];

    for (const limit of [2020, 30000000]) {
        let lastSpoken = initLastSpoken;
        const numbersAndTurns = { ...initNumbersAndTurns };
        let nPrevTurns = Object.keys(numbersAndTurns).length + 1;
        while (nPrevTurns < limit) {
            const prevRoundSpoken = numbersAndTurns[lastSpoken];
            numbersAndTurns[lastSpoken] = nPrevTurns;
            if (prevRoundSpoken !== undefined) {
                lastSpoken = nPrevTurns - prevRoundSpoken;
            } else {
                lastSpoken = 0;
            }
            nPrevTurns++;
        }

        console.log(lastSpoken);
    }
}

functions["d16"] = (data) => {
    let ind = -1;
    let validNumbers = [];
    const ticketData = {};
    const getNumbs = ((lb, ub, arr) => {
        for (let i = Number(lb); i <= Number(ub); i++) { arr[i] = 0; }
    });
    while (data[++ind] !== '') {
        [, fldName, lb1, ub1, lb2, ub2] = data[ind].match(/([^:]+):\s+(\d+)-(\d+)\s+or\s+(\d+)-(\d+)/);
        ticketData[fldName] = [];
        getNumbs(lb1, ub1, ticketData[fldName]);
        getNumbs(lb2, ub2, ticketData[fldName]);
        getNumbs(lb1, ub1, validNumbers);
        getNumbs(lb2, ub2, validNumbers);
    }

    ind += 2; // Skip 'your ticket' header
    let myTicketNumbs = data[ind++].split(',').reduce((acc, num) => {
        acc.push(parseInt(num));
        return acc;
    }, []);
    ind += 2;
    let nearbyTickets = [];
    for (const line of data.slice(ind)) {
        let numbs = [];
        line.split(',').forEach(num => numbs.push(Number(num)));
        nearbyTickets.push(numbs);
    }

    let invalidSum = 0;
    let invalidTickets = [];
    nearbyTickets.forEach((ticket, tInd) => {
        ticket.forEach(num => {
            if (validNumbers[num] === undefined) {
                invalidSum += num;
                if (!invalidTickets.includes(tInd)) {
                    invalidTickets.push(tInd);
                }
            }
        });
    });
    console.log(invalidSum);

    // Remove invalid tickets
    invalidTickets.forEach(tInd => delete nearbyTickets[tInd]);
    nearbyTickets = nearbyTickets.reduce((nBy, tick) => {
        if (tick !== undefined) {
            nBy.push(tick);
        }
        return nBy;
    }, []);


    // Loop through nearby tickets, column per column.
    // Identify one single field per column
    // - First number: get a list of all possible fields
    // - For each following: eliminate fields until one field remains
    // - Save position for the field and its valid numbers
    /*
        const fieldsLeft = Object.keys(ticketData).reduce( (acc, key) => {
            acc[key] = 0;
            return acc;
        }, {});
    */


    const nTicketFields = nearbyTickets[0].length;
    const fieldPossibilities = [];
    for (let i = 0; i < nTicketFields; i++) {
        //       fieldPossibilities[i] = {};
    }
    for (let i = 0; i < nTicketFields; i++) {
        fieldPossibilities[i] = {};
        for (const fld of Object.keys(ticketData)) {
            let validField = true
            for (const ticket of nearbyTickets) {
                if (ticketData[fld][ticket[i]] === undefined) {
                    validField = false
                    break
                }
            }
            if (validField) {
                fieldPossibilities[i][fld] = null;
            }
        }
    }

    let fields = {};
    do {
        for (const i in fieldPossibilities) {
            if (Object.keys(fieldPossibilities[i]).length === 1) {
                let key = Object.keys(fieldPossibilities[i])[0];
                fields[key] = i;
                delete fieldPossibilities[i];
                fieldPossibilities.forEach(fld => {
                    if (Object.keys(fld).includes(key)) {
                        delete fld[key];
                    }
                });
            }
        }
    } while (Object.keys(fields).length < nTicketFields)

    // Check your ticket
    // - identify the six 'departure' fields
    // - multiply these numbers
    let multFields = 1;
    Object.keys(fields).forEach(field => {
        if (field.startsWith('departure')) {
            ind = fields[field];
            multFields *= myTicketNumbs[ind];
        }
    });

    console.log(multFields);
}

functions["d17a"] = (data) => {
    // Set grid size of initial grid.
    const gridBorders = {
        xl: 0, xh: 0 + data[0].length - 1,
        yl: 0, yh: 0 + data.length - 1,
        zl: 0, zh: 0
    }

    const knownGrid = {}
    // Set active cubes from current grid
    data.reverse().forEach((line, y) => {
        line.split('').forEach((v, x) => {
            knownGrid[`${x},${y},0`] = v === '#';
        });
    });

    const countActiveNeighbors = ((grid, x, y, z) => {
        let countActive = 0;
        [x - 1, x, x + 1].forEach(xp => {
            [y - 1, y, y + 1].forEach(yp => {
                [z - 1, z, z + 1].forEach(zp => {
                    if (xp !== x || yp !== y || zp !== z) {
                        if (grid[`${xp},${yp},${zp}`]) {
                            countActive++;
                        }
                    }
                });
            });
        });

        return countActive;
    });

    function printGrid(cycles) {
        console.log(`After ${cycles + 1} cycles`);
        const states = { true: '#', false: '.', undefined: '.' }
        for (let z = gridBorders['zl']; z <= gridBorders['zh']; z++) {
            console.log(`z=${z}`);
            for (let y = gridBorders['yh']; y >= gridBorders['yl']; y--) {
                let line = '';
                for (let x = gridBorders['xl']; x <= gridBorders['xh']; x++) {
                    line += states[knownGrid[`${x},${y},${z}`]];
                }
                console.log(line);
            }
            console.log('');
        }
    }

    // Expand the grid six times
    let lCoord = ['xl', 'yl', 'zl'];
    let hCoord = ['xh', 'yh', 'zh'];
    for (let i = 0; i < 6; i++) {
        const flipCoordinates = [];
        // Set new grid size
        lCoord.forEach(ind => gridBorders[ind] -= 1);
        hCoord.forEach(ind => gridBorders[ind] += 1);

        // Loop over all cubes in new grid
        for (let ix = gridBorders['xl']; ix <= gridBorders['xh']; ix++) {
            for (let iy = gridBorders['yl']; iy <= gridBorders['yh']; iy++) {
                for (let iz = gridBorders['zl']; iz <= gridBorders['zh']; iz++) {
                    // Count active neighbors
                    const nActive = countActiveNeighbors(knownGrid, ix, iy, iz);
                    // If rules are fulfilled, add coordinates to array
                    if (
                        (knownGrid[`${ix},${iy},${iz}`] && (nActive < 2 || nActive > 3)) ||
                        (!knownGrid[`${ix},${iy},${iz}`] && nActive === 3)
                    ) {
                        flipCoordinates.push({ x: ix, y: iy, z: iz });
                    }
                }
            }
        }

        // Flip all identified cubes
        flipCoordinates.forEach(flp => {
            [x, y, z] = [flp['x'], flp['y'], flp['z']];
            knownGrid[`${x},${y},${z}`] = !knownGrid[`${x},${y},${z}`];
        });
        //printGrid(i);
    }
    // Count active cubes
    const nActiveCubes = Object.values(knownGrid).reduce((sum, val) => {
        if (val) {
            sum++;
        }

        return sum;
    }, 0);

    console.log(nActiveCubes);
}

functions["d17b"] = (data) => {
    // Set grid size of initial grid.
    const gridBorders = {
        xl: 0, xh: 0 + data[0].length - 1,
        yl: 0, yh: 0 + data.length - 1,
        zl: 0, zh: 0,
        wl: 0, wh: 0
    }

    const knownGrid = {}
    // Set active cubes from current grid
    data.reverse().forEach((line, y) => {
        line.split('').forEach((v, x) => {
            knownGrid[`${x},${y},0,0`] = v === '#';
        });
    });

    const countActiveNeighbors = ((grid, x, y, z, w) => {
        let countActive = 0;
        [x - 1, x, x + 1].forEach(xp => {
            [y - 1, y, y + 1].forEach(yp => {
                [z - 1, z, z + 1].forEach(zp => {
                    [w - 1, w, w + 1].forEach(wp => {
                        if (xp !== x || yp !== y || zp !== z || wp !== w) {
                            if (grid[`${xp},${yp},${zp},${wp}`]) {
                                countActive++;
                            }
                        }
                    });
                });
            });
        });

        return countActive;
    });

    function printGrid(cycles) {
        console.log(`After ${cycles + 1} cycles`);
        const states = { true: '#', false: '.', undefined: '.' }
        for (let z = gridBorders['zl']; z <= gridBorders['zh']; z++) {
            console.log(`z=${z}`);
            for (let y = gridBorders['yh']; y >= gridBorders['yl']; y--) {
                let line = '';
                for (let x = gridBorders['xl']; x <= gridBorders['xh']; x++) {
                    line += states[knownGrid[`${x},${y},${z}`]];
                }
                console.log(line);
            }
            console.log('');
        }
    }

    // Expand the grid six times
    let lCoord = ['xl', 'yl', 'zl', 'wl'];
    let hCoord = ['xh', 'yh', 'zh', 'wh'];
    for (let i = 0; i < 6; i++) {
        const flipCoordinates = [];
        // Set new grid size
        lCoord.forEach(ind => gridBorders[ind] -= 1);
        hCoord.forEach(ind => gridBorders[ind] += 1);

        // Loop over all cubes in new grid
        for (let ix = gridBorders['xl']; ix <= gridBorders['xh']; ix++) {
            for (let iy = gridBorders['yl']; iy <= gridBorders['yh']; iy++) {
                for (let iz = gridBorders['zl']; iz <= gridBorders['zh']; iz++) {
                    for (let iw = gridBorders['wl']; iw <= gridBorders['wh']; iw++) {
                        // Count active neighbors
                        const nActive = countActiveNeighbors(knownGrid, ix, iy, iz, iw);
                        // If rules are fulfilled, add coordinates to array
                        if (
                            (knownGrid[`${ix},${iy},${iz},${iw}`] && (nActive < 2 || nActive > 3)) ||
                            (!knownGrid[`${ix},${iy},${iz},${iw}`] && nActive === 3)
                        ) {
                            flipCoordinates.push({ x: ix, y: iy, z: iz, w: iw });
                        }
                    }
                }
            }
        }

        // Flip all identified cubes
        flipCoordinates.forEach(flp => {
            const [x, y, z, w] = [flp['x'], flp['y'], flp['z'], flp['w']];
            knownGrid[`${x},${y},${z},${w}`] = !knownGrid[`${x},${y},${z},${w}`];
        });
        //printGrid(i);
    }
    // Count active cubes
    const nActiveCubes = Object.values(knownGrid).reduce((sum, val) => {
        if (val) {
            sum++;
        }

        return sum;
    }, 0);

    console.log(nActiveCubes);
}

functions["d18"] = (data) => {
    let totalSum = 0;
    let exprIndex = 0; // Function global value - ugly :-)

    // Function to calculate an expression
    // Input is an array of numbers, operators and parenthesis
    const calcExpression = (elements => {
        const calc2numbs = ((lh, op, rh) => {
            return eval(`${lh}${op}${rh}`)
        });

        let locSum;
        let op;

        // First char is always first number to handle, unless it's a parenthesis
        const chr = elements[exprIndex++];
        if (chr !== '(') {
            locSum = parseInt(chr);
        } else {
            locSum = calcExpression(elements);
        }

        while (exprIndex < elements.length) {
            // As long as no parenthesis are found, calculate each number using operators
            const ch = elements[exprIndex++];
            if (/\d/.test(ch)) {
                locSum = calc2numbs(locSum, op, parseInt(ch));
            } else if (/[\*\/\+-]/.test(ch)) {
                op = ch;
            } else if (ch === '(') {
                // Upon a parenthesis, call the function again
                const num = calcExpression(elements, exprIndex);
                locSum = calc2numbs(locSum, op, num);
            } else {
                // Right parenthesis
                break
            }
        }

        // Return the calculated value
        return locSum;
    });

    // Loop all lines
    for (const line of data) {
        // Split string
        const elements = line.split('').filter(ch => ch !== ' ');

        // Calculate
        exprIndex = 0;
        const result = calcExpression(elements);

        // Sum up result
        totalSum += result;
    }
    console.log(totalSum);

    totalSum = 0;
    // Loop all lines
    for (const line of data) {
        const getElems = ((str) => {
            const elems = [];
            let ind = 0;
            while (ind < str.length) {
                const ch = str[ind++];
                if (!/[\(\)]/.test(ch)) {
                    elems.push(ch);
                } else if (ch === '(') {
                    const [elms, indx] = getElems(str.substr(ind));
                    elems.push(['('].concat(elms));
                    ind += indx;
                } else if (ch === ')') {
                    elems.push(ch);
                    break
                }
            }

            return [elems, ind];
        });

        const [elements, _] = getElems(line.replaceAll(' ', ''));
        const getModLine = (elems => {
            let prevElem = typeof elems[0] === 'object' ? getModLine(elems[0]) : elems[0];
            let retStr = '';
            for (let i = 1; i < elems.length; i++) {
                let elem = typeof elems[i] === 'object' ? getModLine(elems[i]) : elems[i];
                if (elem === '+') {
                    prevElem = '(' + prevElem;
                }
                if (prevElem === '+') {
                    elem += ')';
                }
                retStr += prevElem;
                prevElem = elem;
            }

            return retStr + prevElem;
        });

        let newLine = getModLine(elements);
        totalSum += eval(newLine);
    }
    console.log(totalSum);
}

functions["d19"] = (data) => {
    // Read all rules (until first empty line)
    let rules = [];
    for (const line of data) {
        if (line === '') break

        let [_, ruleNum, rule] = line.match(/(\d+): "?([^"]+)/)
        if (/\|/.test(rule)) {
            rule = `(?:${rule})`;
        }
        rules[ruleNum] = rule;
    }

    // Loop over rule 0 and replace numbers with content of corresponding rule until no numbers remains
    const getMatchRule = (ruless, ruleNum = 0) => {
        let rule = ruless[ruleNum];
        let mtch;
        while ((mtch = rule.match(/(?:^|[\s:\(\?])(\d+)(?:[\s\)\{]|$)/)) !== null) {
            rule = rule.replace(mtch[1], ruless[mtch[1]]);
        }
        // Remove all spaces from rule 0, i.e. make it a valid regex
        rule = '^' + rule.replaceAll(' ', '') + '$';
        return rule;
    };

    const countMatches = (rule, dataLines) => {
        const reg = new RegExp(rule);
        let count = 0;
        for (const line of dataLines) {
            if (reg.test(line)) {
                count++
            }
        }

        return count;
    };

    let matchingRule = getMatchRule(rules);
    //console.log(matchingRule);

    // Run through rest of input lines and match rule 0 against them, counting number of matches
    let count = countMatches(matchingRule, data.slice(rules.length + 1));
    console.log(count);

    // New settings for rules 8 and 11
    // 8: 42 | 42 8
    // 11: 42 31 | 42 11 31
    rules[8] = '42+';
    rules[11] = '(?:42(?:42(?:42(?:42(?:42(?:42(?:42 31)?31)?31)?31)?31)?31)?31)';
    matchingRule = getMatchRule(rules);
    //console.log(matchingRule);
    count = countMatches(matchingRule, data.slice(rules.length + 1));
    console.log(count);
}

functions["d20"] = (data) => {
    const revString = (str => {
        return str.split('').reverse().join('');
    });

    class Tile {
        ident;
        pixels;
        maxIndex;
        // links to adjacent tile: right, below, left, above
        neighbors;
        position;

        constructor(num) {
            this.ident = num;
            this.pixels = [];
            this.neighbors = { right: null, below: null, left: null, above: null }
        }

        addPixels(pxl) {
            this.pixels.push(pxl);
            this.maxIndex = pxl.length;
        }

        getEdges() {
            const top = this.pixels[0];
            const bot = this.pixels[this.maxIndex-1];
            const lef = this.pixels.map(s => s[0]).join('');
            const rig = this.pixels.map(s => s[this.maxIndex-1]).join('');

            return { above: top, right: rig, below: bot, left: lef }
        }

        removeEdges() {
            const pxls = [...this.pixels];
            this.pixels = [];
            const oldLen = this.maxIndex;
            for (let i = 1; i < oldLen - 1; i++) {
                this.addPixels(pxls[i].substr(1, oldLen - 2));
            }
        }

        relocate(orientations) {
            for (const orient of orientations.split('')) {
                let newPixels;
                let nghbr;
                switch (orient) {
                    case 'u':
                        this.pixels.reverse();
                        nghbr = this.neighbors.below;
                        this.neighbors.below = this.neighbors.above;
                        this.neighbors.above = nghbr;
                        break;

                    case 'f':
                        for (let i = 0; i < this.pixels.length; i++) {
                            this.pixels[i] = revString(this.pixels[i]);
                        }
                        nghbr = this.neighbors.left;
                        this.neighbors.left = this.neighbors.right;
                        this.neighbors.right = nghbr;
                        break;

                    case 'l':
                        newPixels = [];
                        for (let c = this.maxIndex-1; c >= 0; c--) {
                            let rowStr = '';
                            for (let r = 0; r <= this.maxIndex-1; r++) {
                                rowStr += this.pixels[r][c];
                            }
                            newPixels.push(rowStr);
                        }
                        this.pixels = newPixels;
                        nghbr = this.neighbors.left;
                        this.neighbors.left = this.neighbors.above;
                        this.neighbors.above = this.neighbors.right;
                        this.neighbors.right = this.neighbors.below;
                        this.neighbors.below = nghbr;

                        break
                }
            }
        }

        getNeighbor(pos) {
            return this.neighbors[pos];
        }

        setNeighbor(pos, tile) {
            this.neighbors[pos] = tile;
        }
    }

    let tilesList = {};

    let thisTile;
    for (const line of data) {
        if (line.startsWith('Tile ')) {
            const tileId = line.match(/Tile (\d+):/)[1];
            thisTile = new Tile(tileId);
            tilesList[tileId] = thisTile;
        } else if (line !== '') {
            thisTile.addPixels(line);
        }
    }

    const checkCandidates = ((tileId, tileEdge, edge, candidates, testEdges) => {
        for (const candidate of Object.values(candidates)) {
            if (candidate.ident === tileId) {
                continue
            }

            let candidateEdgeInd;
            switch (edge) {
                case 'right': candidateEdgeInd = 2; break
                case 'below': candidateEdgeInd = 3; break
                case 'left':  candidateEdgeInd = 0; break
                case 'above': candidateEdgeInd = 1; break
            }
            const candidateEdge = testEdges[candidateEdgeInd];

            for (let i = 0; i < 2; i++) {
                for (let j = 0; j < 4; j++) {
                    const candidateEdges = candidate.getEdges();
                    const testEdge = candidateEdges[candidateEdge];
                    if (tileEdge === testEdge) {
                        return candidate.ident
                    }
                    candidate.relocate('l');
                }
                candidate.relocate('u');
            }
        }

        return null
    });

    let corners = {}
    let edges = {}
    let others = {}
    const edgePositions = ['right', 'below', 'left', 'above'];

    // loop over all tiles
    for (const tileOuter of Object.values(tilesList)) {
        const currentTileEdges = tileOuter.getEdges();

        // loop over all edges
        let foundNeighbors = 0;
        for (const edge of edgePositions) {

            const foundMatch = checkCandidates(tileOuter.ident, currentTileEdges[edge], edge, tilesList, edgePositions);
            if (foundMatch !== null) {
                foundNeighbors++;
                const idMatch = foundMatch;
                const tileMatch = tilesList[idMatch];
                tileOuter.setNeighbor(edge, tileMatch);
            }
        }

        switch (foundNeighbors) {
            case 2: corners[tileOuter.ident] = tileOuter; break
            case 3: edges  [tileOuter.ident] = tileOuter; break
            case 4: others [tileOuter.ident] = tileOuter; break
        }
    }

    // set length of the side of the square
    const squareWidth = Math.sqrt(Object.keys(tilesList).length);

    let startCorner = Object.values(corners)[0];
    while (startCorner.getNeighbor('above') !== null) {
        startCorner.relocate('l');
    }
    if (startCorner.getNeighbor('left') !== null) {
        startCorner.relocate('f');
    }

    const theGrid = Array.from(new Array(squareWidth), () => Array.from(new Array(squareWidth)));
    theGrid[0][0] = startCorner;

    const fillGridPart = ((tileStart, edgeLength, edgeToMatch, grid, inds, adds) => {
        let tileToMatch = tileStart;
        let edge;
        let tileMatched
        for (let index = 0; index < edgeLength; index++) {
            edge = tileToMatch.getEdges()[edgeToMatch];
            let candidates = {}
            candidates[tileToMatch.neighbors[edgeToMatch].ident] = tileToMatch.neighbors[edgeToMatch]
            const foundMatch = checkCandidates(null, edge, edgeToMatch, candidates, edgePositions);
            if (foundMatch === null) {
                throw 'No match'
            }
            tileMatched = candidates[foundMatch];
            inds[0] += adds[0];
            inds[1] += adds[1];
            grid[inds[0]][inds[1]] = tileMatched;
            tileToMatch = tileMatched;
        }

        return tileMatched
    });

    let lastMatchedTile = startCorner
    let pos=[0,0];
    let aind = 0;
    for (const edgePos of edgePositions) {
        const add = [[0,1], [1,0], [0,-1], [-1,0]][aind++];
        lastMatchedTile = fillGridPart(lastMatchedTile, squareWidth - 2, edgePos, theGrid, pos, add);
        lastMatchedTile = fillGridPart(lastMatchedTile, 1, edgePos, theGrid, pos, add);
}

    // insert the other tiles
    for (let c = 1; c < squareWidth - 1; c++) {
        lastMatchedTile = theGrid[0][c];
        pos = [0, c];
        lastMatchedTile = fillGridPart(lastMatchedTile, squareWidth - 2, 'below', theGrid, pos, [1,0]);
    }

    console.log(Object.values(corners).reduce( (product, tile) => {
       product *= tile.ident;
       return product;
    }, 1))

    // === Part two
    const megaTile = new Tile('mega');
    for (let row = 0; row < squareWidth; row++) {
        for (let col = 0; col < squareWidth; col++) {
            theGrid[row][col].removeEdges();
        }
    }
    for (let row = 0; row < squareWidth; row++) {
        const newRows = [];
        for (let i = 0; i < theGrid[0][0].maxIndex; i++) {
            newRows.push('');
        }
        for (let col = 0; col < squareWidth; col++) {
            for (let i = 0; i < theGrid[0][0].maxIndex; i++) {
                newRows[i] += theGrid[row][col].pixels[i];
            }
        }
        for (let i = 0; i < theGrid[0][0].maxIndex; i++) {
            megaTile.addPixels(newRows[i]);
        }
    }

    const offsets = [[1, -18], [1, -13], [1, -12], [1, -7], [1, -6], [1, -1], [1, 0], [1, 1], [2, -17], [2, -14], [2, -11], [2, -8], [2, -5], [2, -2]];
    const findMonster = ((pixels, row, col, offs) => {
        if (pixels[row][col] !== '#') { return false }
        for (const thisOffs of offs) {
            if (pixels[row + thisOffs[0]][col + thisOffs[1]] !== '#') { return false }
        }

        return true
    });
    const markMonster = ( (pixels, row, col, offs) => {
        pixels[row][col] = 'O';
        for (const thisOffs of offs) {
            pixels[row + thisOffs[0]][col + thisOffs[1]] = 'O';
        }
    });
    const splitMegatile = ( () => {
        let matrix = [];
        for (const pixLine of megaTile.pixels) {
            matrix.push(pixLine.split(''));
        }

        return matrix
    });

megaTile.relocate('u');
megaTile.relocate('l');
megaTile.relocate('l');
            const megaMatrix = splitMegatile();
            for (let row = 0; row < megaTile.pixels.length - 2; row++) {
                for (let col = 18; col < megaTile.pixels[0].length - 1; col++) {
                    if (findMonster(megaMatrix, row, col, offsets)) {
                        markMonster(megaMatrix, row, col, offsets);
                    }
                }
            }
            const joinedPixels = megaMatrix.join('');
            console.log(joinedPixels.length - joinedPixels.replaceAll('#', '').length);
}

functions["d21"] = (data) => {
    const foods = data.map( line => {
        const [_, ingr, allerg] = line.match(/(.*?)\s+\(contains\s+(.*)\)/);
        return [ingr.split(' '), allerg.split(', ')];
    });

    const [ingredients, allergens] = foods.reduce( ([accIngr, accAller], [arrIngr, arrAller], foodNum) => {
        const parseFoodElem = ( (arrFoodElem, accFoodElem) => {
            arrFoodElem.forEach( foodElem  => {
                if (!Object.keys(accFoodElem).includes(foodElem)) {
                    accFoodElem[foodElem] = [foodNum]
                } else {
                    accFoodElem[foodElem].push(foodNum);
                }
            });
        });
        parseFoodElem(arrIngr, accIngr);
        parseFoodElem(arrAller, accAller);
        return [accIngr, accAller]
    }, [{}, {}]);

    let allerKeys = Object.keys(allergens);
    let ingrWithAller = allerKeys.reduce( (accAlr, alr) => {
        const allerFoods = allergens[alr];
        let ingrCandidates = Object.keys(ingredients).reduce( (accIngr, ingr) => {
            if (ingredients[ingr].includes(allerFoods[0])) {
                accIngr.push(ingr);
            }
            return accIngr
        }, []);

        for (const alrFoodNum of allerFoods.slice(1)) {
            ingrCandidates = Object.keys(ingredients).reduce( (accIngr, ingr) => {
                if (ingrCandidates.includes(ingr) && ingredients[ingr].includes(alrFoodNum)) {
                    accIngr.push(ingr);
                }
                return accIngr
            }, []);

            if (ingrCandidates.length === 1) {
                break
            }
        }
        accAlr.push(ingrCandidates);

        return accAlr
    }, []);

    let ind = 0;
    do {
        const ingrs = ingrWithAller[ind++];
        if (ingrs.length === 1) {
            const ingr = ingrs[0];
            ingrWithAller = ingrWithAller.reduce( (acc, a) => {
                if (a.length > 1) {
                    let ix = a.indexOf(ingr);
                    if (ix >= 0) {
                        delete a[ix];
                        a = a.reduce( (aa, e) => {
                            aa.push(e);
                            return aa
                        }, []);
                        ind = 0;
                    }
                }
                acc.push(a);
                return acc
            }, []);
        }
    } while (ind < ingrWithAller.length);
    ingrWithAller = ingrWithAller.flat();

    const ingrWithoutAller = Object.keys(ingredients).reduce( (acc, ingr) => {
        if (!ingrWithAller.includes(ingr)) {
            acc[ingr] = ingredients[ingr];
        }
        return acc
    }, {});

    const nTimes = Object.values(ingrWithoutAller).reduce( (sum, foods) => {
        sum += foods.length;
        return sum
    }, 0);

    console.log(nTimes);

    const zip = allerKeys.reduce( (acc, a, i) => {
        acc[a] = ingrWithAller[i];
        return acc
    }, {});

    allerKeys.sort();
    const sIngr = allerKeys.map( a => zip[a] )
    console.log(sIngr.join(','));
}

functions["d22"] = (data) => {
    let [, [deck1, deck2]] = data.reduce( ([n, p], line) => {
        const newPlayer = line.match(/Player (\d):/);
        if (newPlayer) {
            n = newPlayer[1] - 1;
        } else if (line !== '') {
            p[n].push(parseInt(line));
        }
        return [n, p]
    }, [0, [[], []]]);

    const play = ( (d, recursive) => {
        const pack = ( arr => {
            delete arr[0];
            return arr.reduce((a, e) => { a.push(e); return a }, []);
        });

        let winInd;
        let cards = [d[0][0], d[1][0]];0
        let stdRound = true;
        if (recursive) {
            let histInd = decksHistory.length - 1;
            const s = d[0].toString() + '-' + d[1].toString();
            if (decksHistory[histInd].has(s)) {
                return [0, d[0], d[1]]
            }
            decksHistory[histInd].set(s, histInd);
            if (cards[0] < d[0].length && cards[1] < d[1].length) {
                let ds1 = d[0].slice(1, cards[0] + 1);
                let ds2 = d[1].slice(1, cards[1] + 1);
                decksHistory.push(new Map());
                stdRound = false;
                winInd = null;
                while (winInd === null) {
                    [winInd, ds1, ds2] = play([ds1, ds2], true);
                }
                decksHistory.pop();
            }
        }

        if (stdRound) {
            winInd = cards[0] > cards[1] ? 0 : 1;
        }

        d[winInd].push(cards[winInd]);
        d[winInd].push(cards[(winInd + 1) % 2]);
        d[0] = pack(d[0]);
        d[1] = pack(d[1]);
        let winner = null;
        if (!d[0].length) {
            winner = 1;
        } else if (!d[1].length) {
            winner = 0;
        }
        return [winner, d[0], d[1]]
    });

    let d1 = [...deck1];
    let d2 = [...deck2];
    let decksHistory = [new Map()];
    let champ = null;
    while (champ === null) {
        [champ, d1, d2] = play([d1, d2], false);
    }
    champ = champ === 0 ? d1 : d2;

    const calcSum = (champ => {
        return champ.reverse().reduce( (sum, val, i) => sum + val * (i + 1))
    });

    console.log(calcSum(champ));

    decksHistory = [new Map()];
    champ = null;
    while (champ === null) {
        [champ, deck1, deck2] = play([deck1, deck2], true);
    }
    champ = champ === 0 ? deck1 : deck2;
    console.log(calcSum(champ));
}

functions["d23"] = (data, bigCircleC) => {
    let nLoops;
    const bigCircle = bigCircleC === 'y' ? true : false;
    if (bigCircle) {
        nLoops = 10000000;
    } else {
        nLoops = 100;
    }
    // Get cups and set current cup
    const cups = new Map();
    const cupsNum = data[0].split('').map( n => parseInt(n) );
    cups.set('current', cupsNum[0]);
    const lastCupIn = cupsNum[cupsNum.length - 1];

    // Create a circle of cups
    for (let i = 0; i < cupsNum.length - 1; i++) {
        cups.set(cupsNum[i], cupsNum[i + 1]);
    }

    // Determine lowest and highest values of the cups (Needed for wrapping)
    cupsNum.sort( (a, b) => a - b );
    const low = cupsNum[0];
    let high = cupsNum[cupsNum.length - 1];

    if (bigCircle) {
        let nextCup = high + 1;
        cups.set(lastCupIn, nextCup);
        high = 1000000;
        for (let i = nextCup; i < high; i++) {
            cups.set(i, i + 1);
        }
        cups.set(high, cups.get('current'));
    } else {
        cups.set(lastCupIn, cups.get('current'));
    }

    // A Map for picked cups
    const picked = new Map();

    for (let i = 0; i < nLoops; i++) {
        // Pick up the three after current cup
        let cupNum = cups.get(cups.get('current'));
        picked.clear();
        const picked1st = cupNum;
        // Fill the gap in the circle (let current cup point the one after the whole)
        for (let j = 0; j < 3; j++) {
            picked.set(cupNum, cups.get(cupNum));
            cupNum = cups.get(cupNum);
        }
        cups.set(cups.get('current'), cupNum);

        // Select destination
        let destination = cups.get('current') - 1;
        if (destination < low) {
            destination = high;
        }
        while (picked.has(destination)) {
            if (--destination < low) {
                destination = high;
            }
        }

        // Insert the picked up cups after the destination cup
        cups.set(picked.get(picked.get(picked1st)), cups.get(destination));
        cups.set(destination, picked1st);
        // Switch current cup to be the next after current current cup :)
        cups.set('current', cups.get(cups.get('current')));
    }

    if (bigCircle) {
        const key1 = cups.get(1);
        const key2 = cups.get(key1);
        console.log(key1);
        console.log(key2);
        console.log(key1 * key2);
    } else {
        let s = '';
        let key = 1;
        do {
            s += key;
            key = cups.get(key);
        } while (key != 1);
        console.log(s.substr(1));
    }
}

functions["d24"] = (data) => {
    class Tile {
        static nBlack = 0;
        static tiles = new Map();
        isBlack;
        row;
        col;
        neighbors;

        constructor(row, col, direction) {
            this.isBlack = false;
            this.neighbors = new Map();
            const [nRow, nCol] = this.getPosForDir(row, col, direction);
            this.row = nRow;
            this.col = nCol;

            const id = this.getId();
            this.constructor.tiles.set(id, this);
        }

        getPosForDir(row, col, direction) {
            switch (direction) {
                case 'nw': row++; col--;    break
                case 'ne': row++; col++;    break
                case 'w':         col -= 2; break
                case 'e':         col += 2; break
                case 'sw': row--; col--;    break
                case 'se': row--; col++;    break
            }

            return [row,col];
        }

        getId(row = null, col = null) {
            if (row === null) row = this.row;
            if (col === null) col = this.col;
            return `${row},${col}`;
        }

        getNeighbor(direction) {
            if (this.neighbors.has(direction)) {
                return this.neighbors.get(direction);
            }
            const [row, col] = this.getPosForDir(this.row, this.col, direction);
            const id = this.getId(row, col);
            if (this.constructor.tiles.has(id)) {
                this.setNeighbor(direction, this.constructor.tiles.get(id));
                return this.neighbors.get(direction);
            }

            return null
        }

        setNeighbor(direction, tile) {
            this.neighbors.set(direction, tile);
        }

        flipColor() {
            this.isBlack = !this.isBlack;
            this.constructor.nBlack += this.isBlack ? 1 : -1;
        }
    }

    const refTile = new Tile(0, 0, '');

    for (const line of data) {
        const targetTile = line.match(/(ne|se|sw|nw|e|w)/g).reduce( (currTile, dir) => {
            let nbr = currTile.getNeighbor(dir);
            if (nbr === null) {
                nbr = new Tile(currTile.row, currTile.col, dir);
                currTile.setNeighbor(dir, nbr);
            }

            return nbr
        }, refTile);

        targetTile.flipColor();
    }

    console.log(Tile.nBlack);

    const forceNeighbors = () => {
        Tile.tiles.forEach(tile => {
            if (tile.isBlack) {
                ['ne', 'se', 'sw', 'nw', 'e', 'w'].forEach( dir => {
                    let nbr = tile.getNeighbor(dir);
                    if (nbr === null) {
                        nbr = new Tile(tile.row, tile.col, dir);
                        tile.setNeighbor(dir, nbr);
                    }
                });
            }
        });
    }
    forceNeighbors();

    for (let i = 1; i < 101; i++) {
        const tilesToFlip = []
        Tile.tiles.forEach( tile => {
            const nBlackNbrs = ['ne', 'se', 'sw', 'nw', 'e', 'w'].reduce( (sum, dir) => {
                const nbr = tile.getNeighbor(dir);
                if (nbr !== null && nbr.isBlack) {
                    sum++
                }
                return sum
            }, 0);

            if (tile.isBlack && ![1,2].includes(nBlackNbrs) || !tile.isBlack && nBlackNbrs === 2) {
                tilesToFlip.push(tile);
            }
        }, []);

        tilesToFlip.forEach( t => t.flipColor() );
//if ((i<11)         || i%10===0)
//console.log(Tile.nBlack);
        forceNeighbors();
    }

    console.log(Tile.nBlack);
}

functions["d25"] = (data) => {
    // transform a subject number - 7 for both door and card
        // start with 1 (= value)
        // loop size - secret and different for door and card
            // value = value times subject number
            // value = value % 20201227

    const calcValue = (subjNum, val) => {
        val *= subjNum;
        val %= 20201227;
        return val
    }

    const cardPublic = parseInt(data[0]);
    const doorPublic = parseInt(data[1]);

    let value = 1;
    let cardLoopSize;
    let doorLoopSize;
    let n = 0;
    while (cardLoopSize === undefined || doorLoopSize === undefined) {
        n++;
        value = calcValue(7, value);
        if (value === cardPublic) cardLoopSize = n;
        if (value === doorPublic) doorLoopSize = n;
    }

    let cardEncrypt = 1;
    for (let i = 0; i < cardLoopSize; i++) {
        cardEncrypt = calcValue(doorPublic, cardEncrypt)
    }
    let doorEncrypt = 1;
    for (let i = 0; i < doorLoopSize; i++) {
        doorEncrypt = calcValue(cardPublic, doorEncrypt)
    }

    console.log(cardEncrypt);
    console.log(doorEncrypt);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var [input, task, extra1] = process.argv.slice(2, 5);

// If input file name contains the characters "{}", replace them with the current task number
if (input.indexOf('{}') >= 0) {
    const [tNum] = task.match(/\d+/);
    input = input.replace('{}', tNum)
}

require("constants");
const fs = require("fs");
require("worker_threads");

var dataFromFile = fs.readFileSync(input).toString().trim().split("\n")
if (extra1 === undefined) {
    functions['d' + task](dataFromFile)
} else {
    functions['d' + task](dataFromFile, extra1)
}

use std::collections::{HashMap, VecDeque};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut paths = find_vault(&lines[0], (0, 0, String::new()), (3, 3, String::new()), true);
    paths.retain(|&(x, y, _), _| x == 3 && y == 3);
    println!("Path is: {}", paths.values().next().unwrap().path);
}

fn task_b(lines: Vec<String>) {
    let mut paths = find_vault(&lines[0], (0, 0, String::new()), (3, 3, String::new()), false);
    let mut longest = 0;
    paths.retain(|&(x, y, _), _| x == 3 && y == 3);
    for p in paths.values() {
        if p.path.len() > longest {
            longest = p.path.len()
        }
    }
    println!("Longest path is: {}", longest);
}

type Position = (i32, i32, String);
type Positions = Vec<Position>;
type Rooms = HashMap<Position, Room>;

#[derive(Clone, Debug)]
struct Room {
    path: String,
}

impl Room {
    fn new(p: &String) -> Self {
        Room {
            path: p.to_string(),
        }
    }

    fn add_path(&mut self, path_char: &str) {
        self.path.push_str(path_char);
    }
}

/* --- BFS
Put the starting node on a queue and marked it as visited
While the queue is not empty:
	pop off the node at the head of the queue

	If it is the node we are searching for Then exit and return the node

	For all of the unvisited neighbors:
		mark the neighbour as visited
		put the neighbour in the queue

If we get here, the node is not reachable from the starting node, BFS failed
*/
fn find_vault(passcode: &String, start_pos: Position, target: Position, shortest: bool) -> Rooms {
    let mut visited = Rooms::new();
    let mut queue = VecDeque::new();

    queue.push_back(start_pos.clone());
    visited.insert(start_pos, Room::new(&String::new()));

    while !queue.is_empty() {
        let current_pos = queue.pop_front().unwrap();
        let current_room = visited[&current_pos].clone();
        if current_pos.0 == target.0 && current_pos.1 == target.1 {
            if shortest {
                break
            } else {
                continue
            }
        }

        let curr_path = current_room.path.clone();
        let neighbors = get_neighbors(&current_pos, passcode, &curr_path);
        for nbr in neighbors {
            visited.entry(nbr.clone()).or_insert_with(|| {
                queue.push_back(nbr.clone());
                let mut room = Room::new(&curr_path);
                let l = nbr.2.len();
                room.add_path(nbr.2.get(l-1..=l-1).unwrap());

                room
            });
        }
    }

    visited
}

fn get_neighbors(current_pos: &Position, passcode: &String, curr_path: &String) -> Positions {
    const OPEN_CHARS: [char; 5] = ['b', 'c', 'd', 'e', 'f'];
    const DIR_CHARS: [char; 4] = ['U', 'D', 'L', 'R'];
    const RAN: std::ops::Range<usize> = 0..4;
    let mut neighbors = Vec::new();
    let hash = format!("{:x}", md5::compute(format!("{}{}", passcode, curr_path).as_bytes()));

    let door_chars = &hash[RAN];
    for i in RAN {
        let c = door_chars.chars().nth(i).unwrap();
        if OPEN_CHARS.contains(&c) {
            let dir_char = DIR_CHARS[i];
            let (xd, yd) = [(0, -1), (0, 1), (-1, 0), (1, 0)][i];
            let x = current_pos.0 + xd;
            let y = current_pos.1 + yd;
            if x < RAN.start as i32  || y < RAN.start as i32 || x >= RAN.end as i32 || y >= RAN.end as i32 {
                continue
            }
            let mut p = curr_path.clone();
            p.push(dir_char);
            neighbors.push((x, y, p));
        }
    }

    neighbors
}

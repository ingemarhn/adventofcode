use std::collections::{HashMap, VecDeque};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let nodes = parse_input(lines);

    let mut viable = Vec::new();

    let cmp = |n1: &Node, n2: &Node| {
        n1.used != 0 && n1.used <= n2.avail
    };
    for i in 0..nodes.len()-1 {
        for j in i+1..nodes.len() {
            if cmp(&nodes[i], &nodes[j]) {
                viable.push((&nodes[i], &nodes[j]))
            }
            if cmp(&nodes[j], &nodes[i]) {
                viable.push((&nodes[j], &nodes[i]))
            }
        }
    }
    println!("# of viable pairs: {}", viable.len());
}

fn task_b(lines: Vec<String>) {
    let nodes = parse_input(lines);
    let mut hashed_nodes: HashedNodes = HashMap::new();
    let max_x = nodes[nodes.len() - 1].x;
    let max_y = nodes[nodes.len() - 1].y;

    let target = (max_x, 0);
    let mut start_pos = (0, 0);
    for n in &nodes {
        let pos = (n.x, n.y);
        if n.used == 0 {
            start_pos = pos;
        }
        hashed_nodes.insert(pos, *n);
    };
    let mut steps = find_target(&hashed_nodes, max_x, max_y, start_pos, target);
    steps += find_target(&hashed_nodes, max_x, max_y, (target.0 - 1, target.1), (0, 0)) * 5;
    println!("Fewest # of steps: {}", steps);
}

type Nodes<'a> = Vec<Node>;
type NodePos = (u8, u8);
type NodePositions = Vec<NodePos>;
type HashedNodes<'a> = HashMap<NodePos, Node>;

#[derive(Debug, Clone, Copy)]
struct Node {
    x: u8,
    y: u8,
    size: u16,
    used: u16,
    avail: u16,
    cost: u32,
}

impl Node {
    fn new(x: u8, y: u8, size: u16, used: u16, avail: u16) -> Self {
        Self {x, y, size, used, avail, cost: 1}
    }
}

fn parse_input<'a>(lines: Vec<String>) -> Nodes<'a> {
    use regex::Regex;

    let line_reg: Regex = Regex::new(r"/dev/grid/node-x(?P<x>\d+)-y(?P<y>\d+)\s+(?P<size>\d+)T\s+(?P<used>\d+)T\s+(?P<avail>\d+)T\s+(?P<use>\d+)%").unwrap();

    macro_rules! caps_str { ($cap:expr, $k:expr) => { $cap.name($k).unwrap().as_str() }; }
    macro_rules! caps_u8  { ($cap:expr, $k:expr) => { caps_str!($cap, $k).parse::<u8>().unwrap() }; }
    macro_rules! caps_u16 { ($cap:expr, $k:expr) => { caps_str!($cap, $k).parse::<u16>().unwrap() }; }

    let mut nodes = Nodes::new();

    for line in lines {
        let captures_chk = line_reg.captures(&line);
        if captures_chk.is_none() {
            continue;
        }
        let captures = captures_chk.unwrap();

        let x = caps_u8!(captures, "x");
        let y = caps_u8!(captures, "y");
        let s = caps_u16!(captures, "size");
        let d = caps_u16!(captures, "used");
        let a = caps_u16!(captures, "avail");

        let node = Node::new(x, y, s, d, a);
        nodes.push(node);
    }

    nodes
}

// BFS - Dijkstra
fn find_target(hashed_nodes: &HashedNodes, max_x: u8, max_y: u8, start_pos: NodePos, target: NodePos) -> u32 {
    let mut visited = NodePositions::new();
    let mut queue = VecDeque::new();
    let mut shortest_paths = HashMap::<NodePos, u32>::new();

    queue.push_back(start_pos);
    visited.push(start_pos);
    shortest_paths.insert(start_pos, 0);

    while !queue.is_empty() {
        let current_pos = queue.pop_front().unwrap();

        let get_nbr = |nbrs: &mut Vec<NodePos>, pos: &NodePos, mv_x: i8, mv_y: i8| {
            let ox = if (mv_x >= 0 && pos.0 + mv_x as u8 <= max_x) || (mv_x < 0 && pos.0 != 0) { Some((pos.0 as i8 + mv_x) as u8) } else { None };
            let oy = if (mv_y >= 0 && pos.1 + mv_y as u8 <= max_y) || (mv_y < 0 && pos.1 != 0) { Some((pos.1 as i8 + mv_y) as u8) } else { None };
            if let Some(x) = ox {
                if let Some(y) = oy {
                    let new_pos = (x, y);
                    if hashed_nodes[pos].used <= hashed_nodes[&current_pos].size {
                        nbrs.push(new_pos);
                    }
                }
            }
        };

        let mut nbrs = Vec::new();

        get_nbr(&mut nbrs, &current_pos,  1,  0);
        get_nbr(&mut nbrs, &current_pos, -1,  0);
        get_nbr(&mut nbrs, &current_pos,  0,  1);
        get_nbr(&mut nbrs, &current_pos,  0, -1);

        if nbrs.is_empty() {
            continue;
        }

        let current_path_len = shortest_paths[&current_pos];

        let mut curr_shortest = (u32::MAX, (0, 0));
        for pos in &nbrs {
            if !visited.contains(pos) && hashed_nodes[pos].used <= hashed_nodes[&current_pos].size {
                queue.push_back(*pos);
                let pos_path_len = current_path_len + hashed_nodes[pos].cost;
                shortest_paths.entry(*pos).and_modify(|e| { *e = pos_path_len.min(*e) }).or_insert(pos_path_len);
                if pos_path_len < curr_shortest.0 {
                    curr_shortest.0 = pos_path_len;
                    curr_shortest.1 = *pos;
                }
            }
        }

        if curr_shortest.0 != u32::MAX {
            visited.push(curr_shortest.1);
        }
    }

    shortest_paths[&target]
}

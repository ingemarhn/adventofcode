use regex::Regex;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (mut discs, largest_disc) = parse_lines(lines);

    let time = find_time(&mut discs, largest_disc);
    println!("Time to press button is: {}", time);
}

fn task_b(lines: Vec<String>) {
    let (mut discs, largest_disc) = parse_lines(lines);
    let disc = Disc::new(0, 11);
    discs.push(disc);

    let time = find_time(&mut discs, largest_disc);
    println!("Time to press button is: {}", time);
}

type Discs = Vec<Disc>;

#[derive(Clone,Debug)]
struct Disc {
    position: u32,
    size: u32,
}

impl Disc {
    fn new(s_pos: u32, sz: u32) -> Disc {
        Disc { position: s_pos, size: sz }
    }

    fn update_pos(&mut self, steps: u32) -> u32 {
        self.position = (self.position + steps) % self.size;
        self.position
    }
}

fn parse_lines(lines: Vec<String>) -> (Discs, usize) {
    let reg: Regex = Regex::new(r"Disc #(?P<disc>\d+) has (?P<n_pos>\d+) positions; at time=0, it is at position (?P<act_pos>\d+)\.").unwrap();

    let get_match = |cap: &regex::Captures, num| {
        cap.name(num).unwrap().as_str().parse::<u32>().unwrap()
    };
    let mut largest = 0;

    let mut discs = Vec::new();
    for line in &lines {
        let captures = reg.captures(line).unwrap();
        let size = get_match(&captures, "n_pos");
        let disc = Disc::new(get_match(&captures, "act_pos"), size);
        discs.push(disc);
        if size > discs[largest].size {
            largest = discs.len() - 1;
        }
    }

    (discs, largest)
}

fn find_time(discs: &mut Discs, largest_disc: usize) -> u32 {
    let incr_steps = discs[largest_disc].size;
    let mut desired_positions = Vec::new();
    for (i, discs_it) in discs.iter().enumerate() {
        let s = discs_it.size;
        let q = s - i as u32 % s - 1;
        desired_positions.push(q);
    }
    let mut time = desired_positions[largest_disc] - discs[largest_disc].position;
    #[allow(clippy::needless_range_loop)] // Iterator not implemented for Discs
    for i in 0..discs.len() {
        discs[i].update_pos(time);
    }

    loop {
        let mut current_positions = Vec::new();
        #[allow(clippy::needless_range_loop)] // Iterator not implemented for Discs
        for i in 0..discs.len() {
            let pos = discs[i].update_pos(incr_steps);
            current_positions.push(pos);
        }
        time += incr_steps;

        if current_positions == desired_positions {
            break
        }
    }

    time
}

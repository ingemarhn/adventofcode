use std::collections::HashMap;
use std::str::FromStr;
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let grid = parse_input(lines);
    let max_x = grid[0].len() - 1;
    let max_y = grid.len() - 1;
    let high = if max_x > 20 { 7 } else { 4 };
    let paths: Vec<(u8, u8)> =
        (0..high).fold(Vec::new(), |mut dsts, f| {
            (f+1..=high).for_each(|t| dsts.push((f, t)));
            dsts
        });

    let distances = get_distances(&grid, max_x, max_y, paths);
    let perms: Vec<Vec<u8>> = (1..=high).permutations(high as usize).collect();
    let min_len = perms.iter().fold(u32::MAX, |min, perm| {
        let len = get_path_len(&distances, perm);
        if len < min { len } else { min }
    });

    println!("Min length = {}", min_len);
}

fn task_b(lines: Vec<String>) {
    let grid = parse_input(lines);
    let max_x = grid[0].len() - 1;
    let max_y = grid.len() - 1;
    let high = if max_x > 20 { 7 } else { 4 };
    let paths: Vec<(u8, u8)> =
        (0..high).fold(Vec::new(), |mut dsts, f| {
            (f+1..=high).for_each(|t| dsts.push((f, t)));
            dsts
        });

    let distances = get_distances(&grid, max_x, max_y, paths);
    let perms: Vec<Vec<u8>> = (1..=high).permutations(high as usize).collect();
    let min_len = perms.iter().fold(u32::MAX, |min, perm| {
        let len = get_path_len(&distances, &[perm.to_vec(), vec![0]].concat());
        if len < min { len } else { min }
    });

    println!("Min length = {}", min_len);
}

type Nodes = Vec<Vec<Node>>;
type NodePos = (usize, usize);
type NodePositions = Vec<NodePos>;

#[derive(Debug)]
enum Node {
    Wall,
    Passage,
    Number(u8),
}
use Node::*;

impl FromStr for Node {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "#" => Ok(Wall),
            "." => Ok(Passage),
            _ => Ok(Number(s.parse().unwrap())),
        }
    }
}

fn parse_input(lines: Vec<String>) -> Nodes {
    lines.iter().map(|l|
        l.chars().map(|c|
            c.to_string().parse().unwrap()
        ).collect()
    ).collect()
}

fn get_distances(nodes: &Nodes, max_x: usize, max_y: usize, paths: Vec<(u8, u8)>) -> HashMap<(u8, u8), u32> {
    let mut dists = HashMap::new();

    let (posns, _) = &nodes[1..nodes.len()-1].iter().fold((Vec::new(), 1), |(mut psn, ind), v| {
        v.iter()
            .positions(|c| matches!(c, Number(_)))
            .for_each(|p| psn.push((ind, p)));
        (psn, ind + 1)
    });

    let get_pos = |val: u8| {
        posns.iter().fold((0,0), |p, (y, x)| {
            if matches!(nodes[*y][*x], Number(n) if n == val) { (*y, *x) } else { p }
        })
    };

    for p in paths {
        let start_p = get_pos(p.0);
        let targ_p = get_pos(p.1);
        let len = find_target(nodes, max_x, max_y, start_p, targ_p);
        dists.insert(p, len);
    }

    dists
}

// BFS - Dijkstra
fn find_target(nodes: &Nodes, max_x: usize, max_y: usize, start_pos: NodePos, target: NodePos) -> u32 {
    use std::collections::VecDeque;

    let get_nbr = |(yi, xi): &NodePos, rel_pos: Vec<(i32, i32)>| {
        let mut nbrs = Vec::new();
        for (mv_x, mv_y) in rel_pos {
            let ox = if (mv_x >= 0 && *xi + mv_x as usize <= max_x) || (mv_x < 0 && *xi != 0) { Some((*xi as i32 + mv_x) as usize) } else { None };
            let oy = if (mv_y >= 0 && *yi + mv_y as usize <= max_y) || (mv_y < 0 && *yi != 0) { Some((*yi as i32 + mv_y) as usize) } else { None };
            if let Some(x) = ox {
                if let Some(y) = oy {
                    if !matches!(nodes[y][x], Wall) {
                        nbrs.push((y, x));
                    }
                }
            }
        }
        nbrs
    };

    let mut visited = NodePositions::new();
    let mut queue = VecDeque::new();
    let mut shortest_paths = HashMap::<NodePos, u32>::new();

    queue.push_back(start_pos);
    visited.push(start_pos);
    shortest_paths.insert(start_pos, 0);

    while !queue.is_empty() {
        let current_pos = queue.pop_front().unwrap();

        let nbrs = get_nbr(&current_pos,  vec![(1,  0), (-1,  0), (0,  1), (0, -1)]);

        if nbrs.is_empty() {
            continue;
        }

        let current_path_len = shortest_paths[&current_pos];

        let mut curr_shortest = (u32::MAX, (0, 0));
        for pos in &nbrs {
            if !visited.contains(pos) {
                queue.push_back(*pos);
                let pos_path_len = current_path_len + 1;
                if pos_path_len < curr_shortest.0 {
                    curr_shortest.0 = pos_path_len;
                    curr_shortest.1 = *pos;
                }
                if !shortest_paths.contains_key(pos) || pos_path_len < shortest_paths[pos] {
                    shortest_paths.insert(*pos, pos_path_len);
                }
            }
        }

        if curr_shortest.0 != u32::MAX {
            visited.push(curr_shortest.1);
        }
    }

    shortest_paths[&target]
}

fn get_path_len(dists: &HashMap<(u8, u8), u32>, perm: &[u8]) -> u32 {
    let (_, len) = perm.iter().fold((0, 0), |(from, len), to| {
        let key = if from < *to { (from, *to) } else { (*to, from) };
        (*to, len + dists[&key])
    });

    len
}

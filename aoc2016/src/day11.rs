use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::cmp::Ordering;
use std::fmt;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    get_steps(lines, false);
}

fn task_b(lines: Vec<String>) {
    get_steps(lines, true);
}

fn get_steps(lines: Vec<String>, add_extra: bool) {
    let mut building = Building::new();
    let mut elevator = Items::new();
    let mut least_steps = u32::MAX;
    let mut items_count = 0;

    for line in &lines {
        let line_prs = LineItems::parse_line(line);
        if let LineItems::Floor(floor_num) = &line_prs["floor"] {
            if let LineItems::Items(items) = &line_prs["items"] {
                building.add_to_floor(*floor_num, items);
                items_count += items.len();
            }
        }
    }
    if add_extra {
        building.add_to_floor(0, &vec![Item {kind: G, flavor: 'E'}, Item {kind: M, flavor: 'E'}, Item {kind: G, flavor: 'D'}, Item {kind: M, flavor: 'D'}]);
        items_count += 4;
    }

    building.n_items = items_count;

    calc_steps(0, 99, &mut building, &mut elevator, 0, &mut least_steps, Vec::new());

    println!("Least # of steps = {}", least_steps);
}

fn calc_steps(floor_current: usize, floor_from: usize, building: &mut Building, elevator: &mut Items, steps: u32, least_steps: &mut u32, step_chain: Vec<String>) {
    if steps >= *least_steps {
        return;
    }

    building.add_to_floor_retbal(floor_current, elevator);
    if building.is_floor_comb_used_better(floor_current, steps) {
        return;
    }
    building.add_floor_combination(floor_current, steps);
    let items_cfloor = building.get_floor_items(floor_current);

    if floor_current == 3 && items_cfloor.len() == building.n_items {
        if steps < *least_steps {
            *least_steps = steps
        }

        return;
    }

    let mut floor_next = if building.any_items_below(floor_current) {
        floor_current - 1
    } else {
        floor_current + 1
    };

    if floor_next > 3 {
        return;
    }

    let get_combinations = |going_up: bool| {
        let mut combinations = Vec::new();

        let (generators, microchips) = separate_items(&items_cfloor);

        let combine = |components: &Items, combinations: &mut Vec<ItemCombination>| {
            if !components.is_empty() {
                for i in 0..(components.len() - 1) {
                    for j in i + 1.. components.len() {
                        combinations.push(Pair(components[i], components[j]));
                    }
                }
            }
        };
        if going_up {
            combine(&generators, &mut combinations);
            combine(&microchips, &mut combinations);

            for g in &generators {
                let m = Item {kind: M, flavor: g.flavor};
                if microchips.contains(&m) {
                    combinations.push(Pair(*g, m));
                }
            }
        }

        let add_single = |components: &Items, combinations: &mut Vec<ItemCombination>| {
            for components_it in components.iter() {
                combinations.push(Single(*components_it));
            }
        };
        add_single(&generators, &mut combinations);
        add_single(&microchips, &mut combinations);

        combinations
    };

    loop {
        let items_nfloor = building.get_floor_items(floor_next);
        let elevator_clone = elevator.clone();
        let item_combinations = get_combinations(floor_next > floor_current);
        let move_candidates = item_combinations.iter().filter(|cmb| {
            let mut citems_test = items_cfloor.clone();
            let mut nitems_test = items_nfloor.clone();
            let cmb_vec = match cmb {
                Pair(i1, i2) => vec![*i1, *i2],
                Single(i1)   => vec![*i1],
            };

            let ele_vec = sort_items(&elevator_clone);
            if ele_vec == sort_items(&cmb_vec) &&
               floor_next == floor_from {
                return false;
            }

            for c in &cmb_vec {
                citems_test.remove(citems_test.iter().position(|ci| c == ci).unwrap());
            }
            let (g_test, m_test) = separate_items(&citems_test);
            if !g_test.is_empty() {
                for m in &m_test {
                    if !g_test.iter().any(|g| m.flavor == g.flavor) {
                        return false;
                    }
                }
            }

            nitems_test.extend(cmb_vec);
            let (g_test, m_test) = separate_items(&nitems_test);
            if g_test.is_empty() {
                return true;
            }

            for m in &m_test {
                if !g_test.iter().any(|g| m.flavor == g.flavor) {
                    return false;
                }
            }

            true
        }).collect::<Vec<&ItemCombination>>();

        let mut is_pair_tried = false;
        for cand in &move_candidates {
            elevator.clear();
            let mut step_str = format!("{} -> {}: ", floor_current+1, floor_next+1);
            match cand {
                Pair(i1, i2) => {
                    elevator.push(*i1);
                    step_str.push_str(format!(" {}", *i1).as_str());
                    elevator.push(*i2);
                    step_str.push_str(format!(" {}", *i2).as_str());
                    is_pair_tried = true;
                },
                Single(i1)   => {
                    if is_pair_tried {
                        continue;
                    }
                    elevator.push(*i1);
                    step_str.push_str(format!(" {}", *i1).as_str());
                },
            }
            let floors_save = building.floors.clone();
            let step_chain_n = {
                let mut chn = step_chain.clone();
                chn.push(step_str);
                chn
            };
            if building.remove_from_floor_retbal(floor_current, elevator.to_vec()) == Illegal {
                building.floors = floors_save;
                continue;
            }
            calc_steps(floor_next, floor_current, building, elevator, steps + 1, least_steps, step_chain_n);
            building.floors = floors_save;
        }

        if floor_next > floor_current {
            break;
        }

        floor_next = floor_current + 1;
        if floor_next > 3 {
            break;
        }
    }
}

fn separate_items(itms: &Items) -> (Items, Items) {
    let mut generators = Items::new();
    let mut microchips = Items::new();

    for item in itms {
        match item.kind {
            G => generators.push(*item),
            M => microchips.push(*item),
        }
    }

    (generators, microchips)
}

fn sort_items(items: &Items) -> Items {
    let mut items_l = items.clone();

    items_l.sort_by(|a, b| {
        let cmp = a.kind.cmp(&b.kind);
        if cmp != Ordering::Equal {
            cmp
        } else {
            a.flavor.cmp(&b.flavor)
        }
    });

    items_l
}

#[derive(Clone, Copy, Hash, Eq, Ord, PartialEq, PartialOrd)]
enum ItemKind {
    G, M
}
use ItemKind::*;

impl std::str::FromStr for ItemKind {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "generator" => Ok(G),
            "microchip" => Ok(M),
            _ => Err(format!("'{}' is not a valid value for ItemKind", s)),
        }
    }
}

impl fmt::Display for ItemKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self { G => "G", M => "M", } )
    }
}

#[derive(Clone, Copy, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Item {
    flavor: char,
    kind: ItemKind,
}

impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}", self.flavor, self.kind)
    }
}

type Items = Vec<Item>;

#[derive(Clone, Copy, Hash, Eq, PartialEq)]
enum ItemCombination {
    Pair(Item, Item),
    Single(Item),
}
use ItemCombination::*;

impl fmt::Display for ItemCombination {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Pair(i1, i2) => write!(f, "Pair({},{})", i1, i2),
            Single(i)    => write!(f, "Single({})", i),
        }
    }
}

#[derive(Clone, Default, PartialEq)]
struct Floor {
    items: Items,
    balance: FloorBalance,
}
impl Floor {
    fn count(&self) -> usize {
        self.items.len()
    }

    fn add_to_floor_base(&mut self, items_to_add: &Items) {
        for itmr in items_to_add {
            self.items.push(*itmr);
        }

        self.items = sort_items(&self.items);
    }

    fn add_to_floor(&mut self, items_to_add: &Items) {
        self.add_to_floor_base(items_to_add);
        self.calc_balance(true);
    }

    fn add_to_floor_retbal(&mut self, items_to_add: &Items) -> FloorBalance {
        self.add_to_floor_base(items_to_add);
        self.calc_balance(false);

        self.balance.clone()
    }

    fn remove_from_floor_base(&mut self, items_to_remove: Items) {
        for itmr in &items_to_remove {
            let i = self.items.iter().position(|itm| itmr == itm).unwrap();
            self.items.remove(i);
        }
    }

//    fn remove_from_floor(&mut self, items_to_remove: Items) {
//        self.remove_from_floor_base(items_to_remove);
//        self.calc_balance(true);
//    }

    fn remove_from_floor_retbal(&mut self, items_to_remove: Items) -> FloorBalance {
        self.remove_from_floor_base(items_to_remove);
        self.calc_balance(false);

        self.balance.clone()
    }

    fn calc_balance(&mut self, panic_on_illegal: bool) -> FloorBalance {
        let mut n_generators = 0;
        let mut n_microchips = 0;

        for item in &self.items {
            match item.kind {
                G => n_generators += 1,
                M => n_microchips += 1,
            }
        }

        self.balance = match n_generators.cmp(&n_microchips) {
            Ordering::Equal   => Balanced,
            Ordering::Greater => OwgtGener,
            Ordering::Less    => {
                if n_generators != 0 {
                    if panic_on_illegal {
                        panic!("BAAAAAD! Illegal floor combination = {}", self);
                    }

                    Illegal
                } else {
                    ChipsOnly
                }
            },
        };

        self.balance.clone()
    }

    fn get_items(&self) -> Items {
        sort_items(&self.items)
    }

}
impl fmt::Display for Floor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut outstr = "".to_string();
        for i in 0..self.items.len() { outstr.push_str( &format!("{} ", self.items[i]) ) }
        write!(f, "{}", outstr.trim() )
    }
}

#[derive(Clone, Debug, Default ,PartialEq)]
enum FloorBalance {
    #[default]
    Balanced,
    OwgtGener,
    ChipsOnly,
    Illegal,
}
use FloorBalance::*;

#[derive(Clone)]
struct Building {
    floors: [Floor; 4],
    used_floor_comb: Vec<([Vec<ItemKind>; 4], usize, u32)>,
    n_items: usize,
}

impl Building {
    fn new() -> Self {
        let nfs: [Floor; 4] = Default::default();
        Self { floors: nfs, used_floor_comb: Vec::new(), n_items: 0 }
    }

    fn any_items_below(&self, fl_num: usize) -> bool {
        for i in (0..fl_num).rev() {
            if self.floors[i].count() != 0  {
                return true
            }
        }

        false
    }

    fn get_floor_items(&self, fl_num: usize) -> Items {
        self.floors[fl_num].get_items()
    }

    fn add_to_floor(&mut self, fl_num: usize, items_to_add: &Items) {
        self.floors[fl_num].add_to_floor(items_to_add);
    }

    fn add_to_floor_retbal(&mut self, fl_num: usize, items_to_add: &Items) -> FloorBalance {
        self.floors[fl_num].add_to_floor_retbal(items_to_add)
    }

//    fn remove_from_floor(&mut self, fl_num: usize, items_to_remove: Items) {
//        self.floors[fl_num].remove_from_floor(items_to_remove);
//    }

    fn remove_from_floor_retbal(&mut self, fl_num: usize, items_to_remove: Items) -> FloorBalance {
        self.floors[fl_num].remove_from_floor_retbal(items_to_remove)
    }

    fn get_item_kinds(&self) -> [Vec<ItemKind>; 4] {
        let mut item_kinds: [Vec<ItemKind>; 4] = Default::default();
        for (i, iter) in item_kinds.iter_mut().enumerate() {
            *iter = Vec::new();
            let mut ik = Vec::new();
            for itm in &self.floors[i].items { ik.push(itm.kind); }
            *iter = ik;
        }

        item_kinds
    }

    fn add_floor_combination(&mut self, floor: usize, steps: u32) {
        let item_kinds = self.get_item_kinds();
        if let Some(pos) = self.used_floor_comb.iter().position(|(cmb, with_floor, _)| *cmb == item_kinds && *with_floor == floor) {
            self.used_floor_comb[pos] = (item_kinds, floor, steps);
        } else {
            self.used_floor_comb.push((item_kinds, floor, steps));
        }
    }

    fn is_floor_comb_used_better(&self, floor: usize, steps: u32) -> bool {
        let item_kinds = self.get_item_kinds();
        self.used_floor_comb.iter().any(|(cmb, with_floor, with_steps)| *cmb == item_kinds && *with_floor == floor && *with_steps <= steps)
    }
}
impl fmt::Display for Building {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in (1..4).rev() { writeln!(f, "{}: {}", i, self.floors[i]).unwrap(); }
        write!(f, "{}: {}", 0, self.floors[0])
    }
}

enum LineItems {
    Floor(usize),
    Items(Items),
}

impl LineItems {
    fn parse_line(line: &str) -> HashMap<&str, LineItems> {
        lazy_static! {
            static ref RE_LINE: Regex = Regex::new(r"The (?P<floor>\w+) floor contains(?P<items>.*)\.").unwrap();
            static ref RE_ITEM: Regex = Regex::new(r" (?:and )?a (?P<flavor>\w+)(?:-compatible)? (?P<kind>\w+)").unwrap();
        }

        let captures = RE_LINE.captures(line).unwrap();
        let items = captures.name("items").unwrap().as_str();

        let floor_num = match captures.name("floor").unwrap().as_str() {
            "first"  => 0,
            "second" => 1,
            "third"  => 2,
            "fourth" => 3,
            _ => panic!("Gahhh!")
        };

        let mut ret = HashMap::new();
        let mut ret_items = Vec::new();
        ret.insert("floor", LineItems::Floor(floor_num));

        for itm in items.split(',') {
            let captures = RE_ITEM.captures(itm);
            if let Some(caps) = captures {
                let kind = caps.name("kind").unwrap().as_str().parse().unwrap();
                let c = &caps.name("flavor").unwrap().as_str()[..=0];
                ret_items.push(Item { kind, flavor: c.to_uppercase().chars().next().unwrap(), });
            }
        }

        ret.insert("items", LineItems::Items(ret_items));
        ret
    }
}

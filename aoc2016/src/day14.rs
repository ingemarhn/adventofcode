use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let index = find_64_keys(&lines[0], 1);

    println!("Index for 64th key: {}", index);
}

fn task_b(lines: Vec<String>) {
    let index = find_64_keys(&lines[0], 2017);

    println!("Index for 64th key: {}", index);
}

type Candidates = Vec<KeyCandidate>;

#[derive(Debug)]
struct KeyCandidate {
    index: usize,
    chr: char,
}

impl KeyCandidate {
    fn new(i: usize, c: char) -> Self {
        Self { index: i, chr: c }
    }
}

fn find_64_keys(salt: &String, hash_count: usize) -> usize {
    let mut cand_index: usize;
    let mut last_candidate_index = 0;
    let mut candidates: Vec<KeyCandidate> = Vec::new();
    let mut possible5: HashMap<char, Vec<usize>> = HashMap::new();
    let mut n_found_keys = 0;

    'mainloop: loop {
        let n_get = 64 + 100 - candidates.len();
        // Returns candidates in reversed order, faster to pop last element than removing index 0 every time
        get_candidates(last_candidate_index, salt, hash_count, n_get, &mut candidates, &mut possible5);

        last_candidate_index = candidates.first().unwrap().index;

        loop {
            let cand = candidates.pop().unwrap();
            cand_index = cand.index;
            if cand_index > last_candidate_index - 1000 {
                candidates.push(cand);
                last_candidate_index += 1;
                break;
            }

            if possible5.contains_key(&cand.chr) {
                for idx in &possible5[&cand.chr] {
                    if cand_index < *idx && *idx <= cand_index + 1000 {
                        n_found_keys += 1;
                        if n_found_keys == 64 {
                            break 'mainloop;
                        }
                    }
                }
            }
        }
    }

    cand_index
}

fn get_candidates(start_index: usize, salt: &String, hash_count: usize, count: usize, candidates: &mut Candidates, possible5: &mut HashMap<char, Vec<usize>>) {
    let mut found_cand = 0;
    let mut index = start_index;
    candidates.reverse();

    while found_cand < count {
        let mut hash = format!("{}{}", salt, index);
        for _ in 0..hash_count {
            hash = format!("{:x}", md5::compute(hash.as_bytes()));
        }
        if let Some((ch, _)) = find_eq_chars(&hash, 3) {
            found_cand += 1;
            let eq5 = find_eq5_chars(&hash, ch);
            for c5 in &eq5 {
                let mut idx = if possible5.contains_key(c5) {
                    possible5[c5].clone()
                } else {
                    Vec::new()
                };
                idx.push(index);
                possible5.insert(*c5, idx);
            }
            let cand = KeyCandidate::new(index, ch);
            candidates.push(cand);
        }

        index += 1;
    }

    candidates.reverse();
}

fn find_eq_chars(str: &str, len: usize) -> Option<(char, usize)> {
    if str.len() <= len {
        return None
    }

    let chars: Vec<char> = str.chars().collect();
    for i in 0..chars.len() - len + 1 {
        let mut ip = i + 1;
        while chars[i] == chars[ip] {
            if ip - i == len - 1 {
                return Some((chars[i], i))
            }
            ip += 1;
        }
    }

    None
}

fn find_eq5_chars(str: &str, ch: char) -> Vec<char> {
    let mut chrs = Vec::new();

    let str5 = ch.to_string().repeat(5);
    let ind_o = str.find(&str5);

    let mut h_ind = if let Some(ind) = ind_o {
        chrs.push(ch);
        ind + 5
    } else {
        0
    };

    loop {
        let s_str = &str[h_ind..].to_string();
        if let Some((ch, ind)) = find_eq_chars(s_str, 5) {
            chrs.push(ch);
            h_ind = ind + 5;
            if h_ind > s_str.len() - 5 {
                break
            }
        } else {
            break
        }
    }

    chrs
}

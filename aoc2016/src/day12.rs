use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (mut registers, instructions) = parse_input(lines);
    assembunny(&mut registers, instructions);

    println!("register a = {}", registers["a"]);
}

fn task_b(lines: Vec<String>) {
    let (mut registers, instructions) = parse_input(lines);
    registers.insert("c".to_string(), 1);
    assembunny(&mut registers, instructions);

    println!("register a = {}", registers["a"]);
}

type Registers = HashMap<String, i32>;
type Register = String;

#[derive(Debug)]
enum Instructions {
    Cpy(Register, Register),
    Jnz(Register, isize),
    Inc(Register),
    Dec(Register),
}
use Instructions::*;

fn parse_input(lines: Vec<String>) -> (Registers, Vec<Instructions>) {
    let mut regs = Registers::new();
    let mut instr = Vec::<Instructions>::new();

    fn check_insert(regs: &mut Registers, k: &String, v: i32) {
        regs.entry(k.to_string()).or_insert(v);
    }

    for line in lines {
        let parts :Vec<&str> = line.split(' ').collect();
        let reg1 = parts[1].to_string();
        let inst = match parts[0] {
            "cpy" => {
                let reg2 = parts[2].to_string();
                check_insert(&mut regs, &reg2, 0);
                let num = reg1.parse::<i32>().unwrap_or(0);
                check_insert(&mut regs, &reg1, num);
                Cpy(reg1, reg2)
            },
            "inc" => {
                check_insert(&mut regs, &reg1, 0);
                Inc(reg1)
            },
            "dec" => {
                check_insert(&mut regs, &reg1, 0);
                Dec(reg1)
            },
            "jnz" => {
                let num = parts[2]
                            .to_string()
                            .parse::<isize>()
                            .unwrap();
                let reg = parts[1].to_string();
                check_insert(&mut regs, &reg, 0);
                Jnz(reg1, num)
            },
            _ => panic!("Gahhh!")
        };

        instr.push(inst);
    }

    (regs, instr)
}

fn assembunny(registers: &mut Registers, instructions: Vec<Instructions>) {
    let mut prog_index = 0;
    while prog_index < instructions.len() {
        let index_incr = match &instructions[prog_index] {
            Cpy(reg1, reg2) => {
                registers.insert(reg2.to_string(), *registers.get(reg1).unwrap());
                1
            },
            Jnz(reg, steps) => {
                if *registers.get(reg).unwrap() != 0 {
                    *steps
                } else {
                    1
                }
            },
            Inc(reg) => {
                registers.insert(reg.to_string(), *registers.get(reg).unwrap() + 1);
                1
            },
            Dec(reg) => {
                registers.insert(reg.to_string(), *registers.get(reg).unwrap() - 1);
                1
            },
        };

        prog_index = (prog_index as isize + index_incr) as usize;
    }
}

use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let matrix_coord2num = build_matrix_a();

    let code = build_code(lines, &matrix_coord2num, [2, 2]);

    println!("Bathroom code is: {}", code);
}

fn task_b(lines: Vec<String>) {
    let matrix_coord2num = build_matrix_b();

    let code = build_code(lines, &matrix_coord2num, [3, 1]);

    println!("Bathroom code is: {}", code);
}

fn build_matrix_a() -> HashMap<[i32; 2], i32> {
    let mut matr_coord2num = HashMap::new();

    let mut row_num = 0;
    for i in 0..9 {
        let remainder = i % 3;
        if remainder == 0 {
            row_num += 1;
        }
        let index = [row_num, remainder + 1];

        matr_coord2num.insert(index, i + 1);
    }

    matr_coord2num
}

fn build_matrix_b() -> HashMap<[i32; 2], i32> {
    let mut matr_coord2num = HashMap::new();

    matr_coord2num.insert([1, 3], 49);
    matr_coord2num.insert([2, 2], 50);
    matr_coord2num.insert([2, 3], 51);
    matr_coord2num.insert([2, 4], 52);
    matr_coord2num.insert([3, 1], 53);
    matr_coord2num.insert([3, 2], 54);
    matr_coord2num.insert([3, 3], 55);
    matr_coord2num.insert([3, 4], 56);
    matr_coord2num.insert([3, 5], 57);
    matr_coord2num.insert([4, 2], 65);
    matr_coord2num.insert([4, 3], 66);
    matr_coord2num.insert([4, 4], 67);
    matr_coord2num.insert([5, 3], 68);

    matr_coord2num
}

fn build_code(lines: Vec<String>, matrix_coord2num: &HashMap<[i32; 2], i32>, curr_pos: [i32; 2]) -> String {
    let mut code = "".to_string();
    let mut current_pos = curr_pos;
    for line in lines {
        for ch in line.chars() {
            current_pos = match ch {
                'U' => check_position([current_pos[0] - 1, current_pos[1]], matrix_coord2num, current_pos),
                'D' => check_position([current_pos[0] + 1, current_pos[1]], matrix_coord2num, current_pos),
                'L' => check_position([current_pos[0], current_pos[1] - 1], matrix_coord2num, current_pos),
                'R' => check_position([current_pos[0], current_pos[1] + 1], matrix_coord2num, current_pos),
                _ => panic!("duhhh..."),
            }
        }
        let digit = matrix_coord2num[&current_pos];
        if digit < 10 {
            code.push_str(&digit.to_string());
        } else {
            code.push(std::char::from_u32(digit as u32).unwrap());
        }
    }

    code
}

fn check_position(new_pos: [i32; 2], matr_coord2num: &HashMap<[i32; 2], i32>, curr_pos: [i32; 2]) -> [i32; 2] {
    if matr_coord2num.contains_key(&new_pos) {
        new_pos
    } else {
        curr_pos
    }
}

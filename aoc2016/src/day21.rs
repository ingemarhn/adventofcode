pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let instructions = parse_input(lines);
    let scrambled = scramble("abcdefgh", instructions);

    println!("The scrambled string is: {}", scrambled);
}

fn task_b(lines: Vec<String>) {
    let instructions = parse_input(lines);
//    let scrambled = descramble("decab", instructions);
//    let scrambled = descramble("hcdefbag", instructions);
    let scrambled = descramble("fbgdceah", instructions);

    println!("The descrambled string is: {}", scrambled);
}

type Chars = Vec<char>;

#[derive(Debug)]
enum Instruction {
    SwapIndexes(usize, usize), // swap position X with position Y
    SwapLetters(char, char), // swap letter X with letter Y
    RotateSteps(char, usize), // rotate left/right X steps
    RotateOnPosition(char), // rotate based on position of letter X
    Reverse(usize, usize), // reverse positions X through Y
    MovePosition(usize, usize), // move position X to position Y
    Nope,
}
use Instruction::*;


#[derive(Debug)]
enum Pwde {
    Pwd(String),
    Pwdb(Chars),
}
use Pwde::*;

impl Pwde {
    fn swap_indexes(&mut self, x: usize, y: usize) {
        if let Pwdb(chars) = self {
            chars.swap(x, y);
        }
    }

    fn swap_letters(&mut self, a: char, b: char) {
        if let Pwdb(chars) = self {
            let ai = chars.iter().position(|c| *c == a).unwrap();
            let bi = chars.iter().position(|c| *c == b).unwrap();
            self.swap_indexes(ai, bi);
        }
    }

    fn rotate_steps(&mut self, dir: char, x: usize) {
        if let Pwdb(chars) = self {
            if dir == 'l' {
                chars.rotate_left(x);
            } else {
                chars.rotate_right(x);
            }
        }
    }

    fn rotate_on_position(&mut self, cx: char) {
        if let Pwdb(chars) = self {
            let ci = chars.iter().position(|c| *c == cx).unwrap();
            let wr = (1 + ci + if ci >= 4 { 1 } else { 0 }) % chars.len();
            chars.rotate_right(wr);
        }
    }

    fn rotate_left_on_position(&mut self, cx: char) {
        if let Pwdb(chars) = self {
            let cp = chars.iter().position(|c| *c == cx).unwrap();
            let ci = if cp == 0 { 8 } else { cp };
            let cn = (if ci % 2 == 0 { (ci - 2 + 8) / 2 } else { ci - (ci / 2 + 1) }) % chars.len();
            chars.rotate_right((8 + cn - cp) % 8);
        }
    }

    fn reverse(&mut self, x: usize, y: usize) {
        if let Pwdb(chars) = self {
            let mut sl = chars[x..=y].to_vec();
            sl.reverse();
            chars[x ..= y].copy_from_slice(&sl[..((y - x) + 1)]);
        }
    }

    fn move_position(&mut self, x: usize, y: usize) {
        if let Pwdb(chars) = self {
            let c = chars[x];
            for i in x+1..chars.len() {
                chars[i - 1] = chars[i];
            }
            for i in (y+1..chars.len()).rev() {
                chars[i] = chars[i - 1];
            }
            chars[y] = c;
        }
    }

    fn convert(&mut self) {
        let s = if let Pwdb(chars) = self {
            chars.iter().collect()
        } else {
            String::new()
        };
        *self = Pwd(s);
    }
}

fn parse_input(lines: Vec<String>) -> Vec<Instruction> {
    let mut instr = Vec::<Instruction>::new();

    use regex::Regex;
    let line_reg: Regex = Regex::new(concat!(
        r"(?P<swap_p>swap position) (?P<spx>\d+) with position (?P<spy>\d+)|",
        r"(?P<swap_l>swap letter) (?P<lx>.) with letter (?P<ly>.)|",
        r"(?P<rot_s>rotate) (?P<dir>left|right) (?P<n>\d+) steps?|",
        r"(?P<rot_b>rotate based) on position of letter (?P<l>.)|",
        r"(?P<rev>reverse) positions (?P<rpx>\d+) through (?P<rpy>\d+)|",
        r"(?P<mov>move) position (?P<mpx>\d+) to position (?P<mpy>\d+)")).unwrap();

    macro_rules! caps_get {
        ($cap:expr, $k:expr) => {
            $cap.name($k).unwrap().as_str()
        };
    }
    macro_rules! caps_uz {
        ($cap:expr, $k:expr) => {
            caps_get!($cap, $k).parse::<usize>().unwrap()
        };
    }
    macro_rules! caps_ch {
        ($cap:expr, $k:expr) => {
            caps_get!($cap, $k).chars().nth(0).unwrap()
        };
    }

    for line in lines {
        let captures = line_reg.captures(&line).unwrap();
        let inst = ["swap_p", "swap_l", "rot_s", "rot_b", "rev", "mov"].iter().fold(Nope, |instr, name| {
            if captures.name(name).is_some() {
                match *name {
                    "swap_p" => {
                        let x = caps_uz!(captures, "spx");
                        let y = caps_uz!(captures, "spy");
                        SwapIndexes(x, y)
                    },
                    "swap_l" => {
                        let x = caps_ch!(captures, "lx");
                        let y = caps_ch!(captures, "ly");
                        SwapLetters(x, y)
                    },
                    "rot_s" => {
                        let dir = captures.name("dir").unwrap().as_str().chars().next().unwrap();
                        let x = caps_uz!(captures, "n");
                        RotateSteps(dir, x)
                    },
                    "rot_b" => {
                        let l = captures.name("l").unwrap().as_str().chars().next().unwrap();
                        RotateOnPosition(l)
                    },
                    "rev" => {
                        let x = caps_uz!(captures, "rpx");
                        let y = caps_uz!(captures, "rpy");
                        Reverse(x, y)
                    },
                    "mov" => {
                        let x = caps_uz!(captures, "mpx");
                        let y = caps_uz!(captures, "mpy");
                        MovePosition(x, y)
                    },
                    _ => instr,
                }
            } else {
                instr
            }
        });

        instr.push(inst);
    }

    instr
}

fn scramble(start: &str, instrs: Vec<Instruction>) -> String {
    let mut scrambled = Pwdb(start.chars().collect());

    for instr in instrs {
        match instr {
            SwapIndexes(x, y) => scrambled.swap_indexes(x, y),
            SwapLetters(a, b) => scrambled.swap_letters(a, b),
            RotateSteps(d, x) => scrambled.rotate_steps(d, x),
            RotateOnPosition(c) => scrambled.rotate_on_position(c),
            Reverse(x, y) => scrambled.reverse(x, y),
            MovePosition(x, y) => scrambled.move_position(x, y),
            Nope => panic!("Gaaaah!")
        }
    }

    scrambled.convert();

    if let Pwd(pwd) = scrambled {
        pwd
    } else {
        "Password not converted to string".to_string()
    }
}

fn descramble(start: &str, instrs: Vec<Instruction>) -> String {
    let mut scrambled = Pwdb(start.chars().collect());
    let mut instrs_rev = instrs;
    instrs_rev.reverse();

    for instr in instrs_rev {
        match instr {
            SwapIndexes(x, y) => scrambled.swap_indexes(y, x),
            SwapLetters(a, b) => scrambled.swap_letters(b, a),
            RotateSteps(d, x) => scrambled.rotate_steps(if d == 'l' {'r'} else {'l'}, x),
            RotateOnPosition(c) => scrambled.rotate_left_on_position(c),
            Reverse(x, y) => scrambled.reverse(x, y),
            MovePosition(x, y) => scrambled.move_position(y, x),
            Nope => panic!("Gaaaah!")
        }
    }

    scrambled.convert();

    if let Pwd(pwd) = scrambled {
        pwd
    } else {
        "Password not converted to string".to_string()
    }
}

use std::cmp::Ordering;
use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Debug, Clone)]
struct Room {
    room_encrypted: String,
    num_of_letters: Vec<(i32, char)>,
    id: i32,
    checksum: Vec<char>,
}

impl Room {
    fn parse(line: String) -> Room {
        let parts: Vec<&str> = line.split('-').collect();
        let n_parts_m1 = parts.len() - 1;

        let mut char_counter = HashMap::new();
        for parts_i in parts.iter().take(n_parts_m1) {
            for c in parts_i.chars() {
                let counter = char_counter.entry(c).or_insert(0);
                *counter += 1;
            }
        }
        let mut counter_char = Vec::new();
        for (chr, count) in char_counter.iter() {
            counter_char.push((*count, *chr));
        }
        counter_char.sort_by(|a, b| {
            let cmp = b.0.cmp(&a.0);
            if cmp != Ordering::Equal {
                cmp
            } else {
                a.1.cmp(&b.1)
            }
        });

        let id_chks: Vec<&str> = parts[n_parts_m1].split(['[', ']']).collect();

        Room {
            room_encrypted: line.split('[').collect::<Vec<&str>>()[0].to_string(),
            num_of_letters: counter_char,
            id: id_chks[0].parse::<i32>().unwrap(),
            checksum: id_chks[1].chars().collect(),
        }
    }

    fn check_room(&self) -> bool {
        for i in 0..5 {
            let (_n_letters, letter) = self.num_of_letters[i];
            if letter != self.checksum[i] {
                return false;
            }
        }

        true
    }
}

fn task_a(lines: Vec<String>) {
    let mut rooms: Vec<Room> = Vec::new();
    for line in lines {
        let room = Room::parse(line);
        let is_valid = room.check_room();
        if is_valid {
            rooms.push(room);
        }
    }

    let mut sector_sum = 0;
    for room in rooms {
        sector_sum += room.id;
    }

    println!("Sum of the sector IDs: {}", sector_sum);
}

fn task_b(lines: Vec<String>) {
    const N_ASCII: u8 = 26;
    const A_BASE: u8 = 96;
    let mut rooms: Vec<Room> = Vec::new();
    for line in lines {
        let room = Room::parse(line);
        let is_valid = room.check_room();
        if is_valid {
            rooms.push(room);
        }
    }

    for room in rooms {
        let mut room_decrypted: Vec<char> = room.room_encrypted[..room.room_encrypted.rfind('-').unwrap()].chars().collect();
        let c_add: u8 = (room.id % N_ASCII as i32) as u8;

        for room_decrypted_i in &mut room_decrypted {
            *room_decrypted_i = if *room_decrypted_i != '-' {
                let cv = (*room_decrypted_i as u8 - A_BASE + c_add) % N_ASCII;
                char::from_u32((cv + A_BASE).into()).unwrap()
            } else {
                ' '
            }

        }

        let room_decrypted_str = room_decrypted.into_iter().collect::<String>();
        if room_decrypted_str == *"northpole object storage" {
            println!("The sector ID of the room North Pole objects: {}", room.id);
        }
    }
}

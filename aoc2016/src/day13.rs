use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (favorite, x, y) = parse_input(lines);
    let target = (x, y);
    let nodes = find_target(favorite, (1, 1), target);
    println!("# steps to target = {}", nodes.get(&target).unwrap().distance);
}

static STEPS_LIMIT: i32 = 50;
fn task_b(lines: Vec<String>) {
    let (favorite, _x, _y) = parse_input(lines);
    let n_locations = find_locations(favorite, (1, 1));
    println!("# locations = {}", n_locations);
}

fn parse_input(lines: Vec<String>) -> (i32, i32, i32) {
    let fav = lines[0].parse::<i32>().unwrap();
    let xy :Vec<&str> = lines[1].split(',').collect();
    let x = xy[0].parse::<i32>().unwrap();
    let y = xy[1].parse::<i32>().unwrap();

    (fav, x, y)
}

type Position = (i32, i32);
type Positions = Vec<Position>;
type Nodes = HashMap<Position, Node>;

#[derive(Clone)]
struct Node {
    distance: i32,
    predecessor: Option<Position>,
}

impl Node {
    fn new() -> Self {
        Self {
            distance: 0,
            predecessor: None,
        }
    }

    fn set(&mut self, distance: i32, predecessor: Position) {
        self.distance = distance;
        self.predecessor = Some(predecessor);
    }
}

// BFS
fn find_target(fav: i32, start_pos: Position, target: Position) -> Nodes {
    let mut visited = Nodes::new();
    let mut queue = Vec::new();

    queue.push(start_pos);
    visited.insert(start_pos, Node::new());

    while !queue.is_empty() {
        let current_pos = queue.remove(0);
        if current_pos == target {
            break
        }

        let current_node = visited[&current_pos].clone();
        let neighbors = get_neighbors(current_pos, fav);
        for nbr in neighbors {
            visited.entry(nbr).or_insert_with(|| {
                queue.push(nbr);
                let mut node = Node::new();
                node.set(current_node.distance + 1, current_pos);
                node
            });
        }
    }

    visited
}

fn find_locations(fav: i32, start_pos: Position) -> i32 {
    let mut visited = Nodes::new();
    let mut queue = Vec::new();

    queue.push(start_pos);
    visited.insert(start_pos, Node::new());

    while !queue.is_empty() {
        let current_pos = queue.remove(0);
        let current_node = visited[&current_pos].clone();
        let neighbors = get_neighbors(current_pos, fav);
        for nbr in neighbors {
            visited.entry(nbr).or_insert_with(|| {
                let mut node = Node::new();
                node.set(current_node.distance + 1, current_pos);
                if node.distance != STEPS_LIMIT {
                    queue.push(nbr);
                }

                node
            });
        }
    }

    visited.len() as i32
}

fn get_neighbors(current_pos: Position, fav: i32) -> Positions {
    let mut neighbors = Vec::new();
    for (xd, yd) in [(-1, 0), (0, -1), (1, 0), (0, 1)] {
        let pos = (current_pos.0 + xd, current_pos.1 + yd);
        let (x, y) = pos;
        if x < 0  || y < 0 { continue }
        let sum = x*x + 3*x + 2*x*y + y + y*y + fav;
        if sum.count_ones() % 2 == 0 {
            neighbors.push(pos);
        }
    }

    neighbors
}

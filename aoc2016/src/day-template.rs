pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    println!("{:?}", lines);
}

fn task_b(lines: Vec<String>) {
    println!("{:?}", lines);
}

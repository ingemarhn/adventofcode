pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let n_elves = lines[0].parse::<usize>().unwrap();
    let mut nodes = Vec::new();
    let mut current = n_elves / 2;

    nodes.push( Node {id: 1, next: 1, prev: current } );
    for i in (3..n_elves).step_by(2) {
        let n = nodes.len();
        nodes.push( Node {id: i, next: n + 1, prev: n - 1 } );
    }
    nodes.push( Node {id: n_elves, next: 0, prev: current - 1 } );

    loop {
        let next = nodes[current].next;
        let next_next = nodes[next].next;
        if current == next_next {
            break
        }
        nodes[current].next = next_next;
        nodes[next_next].prev = current;
        current = next_next;
    }

    println!("Last elve standing: {}", nodes[current].id);
}

fn task_b(lines: Vec<String>) {
    let n_elves = lines[0].parse::<i32>().unwrap();
    let mut mult3t = 1;
    while mult3t <= n_elves {
        mult3t *= 3;
    }
    let mult3b = mult3t / 3;

    let winner = if mult3b == n_elves {
        n_elves
    } else {
        let mult2 = mult3b * 2;
        if mult2 >= n_elves {
            n_elves - mult3b
        } else {
            mult3t - (mult3t - n_elves) * 2
        }
    };

    println!("The winner is {}", winner);
}

fn _task_a(lines: Vec<String>) {
    let n_elves = lines[0].parse::<u32>().unwrap();
    common::dbg_on(1);
    common::dbg(format!("n_elves = {}", n_elves), 1);
        let mut round = 0;
        let mut first_in_game = 0;
        let mut last_shall_take_from_first = true;
        let last_elve = loop {
            let offset = 2u32.pow(round);
    common::dbg(format!("offset = {}", offset), 1);
            let span = n_elves / (2 * offset) * 2 * offset;
    common::dbg(format!("span = {}", span), 1);
            let last_to_lose =
                if last_shall_take_from_first {
                    first_in_game += offset;
                    if span == 0 {
    common::dbg("break 1".to_string(), 1);
                        break first_in_game;
                    }
    common::dbg(format!("first_in_game = {}", first_in_game), 1);
                    offset - 1 + span
                } else {
                    let mut last = first_in_game + offset + span;
                    if last > n_elves {
                        last -= offset * 2;
                    }
                    if last + offset > n_elves {
    common::dbg("break 2".to_string(), 1);
                        //break first_in_game;
                    }
                    last
                };
    common::dbg(format!("last_to_lose = {}", last_to_lose), 1);

            last_shall_take_from_first = last_to_lose + offset <= n_elves;
    common::dbg(format!("last_shall_take_from_first = {}", last_shall_take_from_first), 1);

            if last_to_lose < offset {
    common::dbg("break 3".to_string(), 1);
            break last_to_lose;
        }

        round += 1;
    };

    println!("Last elve standing: {}", last_elve);
}

/*
use std::thread;
const STACK_SIZE: usize = 4096 * 1024 * 1024;

fn josephus_variant(n: u32, k: u32) -> u32 {
common::dbg(format!("n = {}, k = {}", n, k), 12);
    if n == 1 {
        1
    } else {
        /* The position returned by josephus(n - 1, k) is adjusted because the recursive call josephus(n - 1, k) considers the original position k%n + 1 as position 1 */
        let n_1 = n - 1;
        let r = josephus_variant(n_1, n_1/2);
common::dbg(format!("r({}) = {}, n = {}, k = {}", n_1, r, n, k), 2);
        let rr = r + k;
        let r = rr % n + 1; //if rr <= n { 1 } else  { 0 };
common::dbg(format!("ret = {}", r), 12);
        //(josephus_variant(n_1, n_1 / 2) + k) % n + 1
        r
    }
}
/*
1		1
2		1
3		3
4		1
5		2	7	12
6		3	9	15
7		5	12	19
8		7	15	22
*/

fn __task_b(lines: Vec<String>) {
common::dbg_on(3);
    let n_elves  = lines[0].parse::<u32>().unwrap();
    let _w = 9;
common::dbg(format!("n_elves = {}", n_elves), 3);
    let child = thread::Builder::new()
        .stack_size(STACK_SIZE)
        .spawn(move || { println!("The winning elf is: {}", josephus_variant(n_elves, n_elves/2)); })
        .unwrap();

    // Wait for thread to join
    child.join().unwrap();
}
*/

fn _task_b(lines: Vec<String>) {
    let mut n_elves = lines[0].parse::<usize>().unwrap();
    let mut nodes = Vec::new();

    nodes.push( Node {id: 1, next: 1, prev: n_elves - 1 } );
    for i in 2..n_elves {
        let n = nodes.len();
        nodes.push( Node {id: i, next: n + 1, prev: n - 1 } );
    }
    nodes.push( Node {id: n_elves, next: 0, prev: n_elves - 2 } );

    let mut starter = 0;
    let mut current;
    while n_elves > 1 {
        current = starter;
        for _i in 0..n_elves/2 {
            current = nodes[current].next;
        }
        let prev = nodes[current].prev;
        let next = nodes[current].next;
        nodes[prev].next = next;
        nodes[next].prev = prev;
        n_elves -= 1;
        starter = nodes[starter].next;
    }
    println!("Winner is {}", nodes[starter].id);
}

struct Node {
    id: usize,
    next: usize,
    prev: usize,
}

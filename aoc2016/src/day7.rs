pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Debug)]
struct Abba {
    hypernet: bool,
    index: i32,
    value: [char; 4],
    contains_abba: bool,
}

impl Abba {
    fn new() -> Abba {
        Abba {
            hypernet: false,
            index: -1,
            value: ['-'; 4],
            contains_abba: false,
        }
    }

    fn reset(&mut self, hypernet_value: bool) {
        self.hypernet = hypernet_value;
        self.index = -1;
    }

    fn add_ch(&mut self, ch: char) {
        self.index += 1;
        self.value[self.index as usize] = ch;
    }

    fn find_abba(&mut self, ch: char) -> bool {
        match self.index {
            -1 => {
                self.add_ch(ch);
            },
            0 => {
                if ch != self.value[0] {
                    self.add_ch(ch);
                } else {
                    self.reset(self.hypernet);
                    self.add_ch(ch);
                }
            },
            1 => {
                if ch == self.value[1] {
                    self.add_ch(ch);
                } else {
                    self.reset(self.hypernet);
                    self.add_ch(self.value[1]);
                    self.add_ch(ch);
                }
            },
            2 => {
                if ch == self.value[0] {
                    self.add_ch(ch);
                    self.contains_abba = true;
                    self.reset(self.hypernet);
                    return true;
                } else {
                    self.reset(self.hypernet);
                    self.add_ch(self.value[2]);
                    self.add_ch(ch);
                }
            },
            _ => println!("duhhh..."),
        }

        false
    }
}

#[derive(Debug)]
struct Aba {
    chars: Vec<char>,
    abas_found: Vec<String>,
}

impl Aba {
    fn new(line: &str) -> Aba {
        Aba {
            chars: line.chars().collect(),
            abas_found: Vec::new(),
        }
    }

    fn find(&mut self) -> bool {
        self.abas_found = Vec::new();
        let mut s: String;
        for i in 0..self.chars.len()-2 {
            if (self.chars[i] != self.chars[i+1]) && (self.chars[i] == self.chars[i+2]) {
                s = self.chars[i..=i+2].iter().collect::<String>();
                self.abas_found.push(s);
//println!("{:?}",self.abas_found[self.abas_found.len()-1]);
            }
        }

        !self.abas_found.is_empty()
    }

    fn is_match(&self, h_nets: &Vec<&str>) -> bool {
        for h_net in h_nets {
            for aba in &self.abas_found {
                let mut aba_it = aba.chars();
                let c1 = aba_it.next().unwrap();
                let c2 = aba_it.next().unwrap();
                let bab: &str = &[c2, c1, c2].iter().collect::<String>();
                if let Some(_index) = h_net.find(bab) {
                    return true;
                }
            }
        }

        false
    }
}

fn task_a(lines: Vec<String>) {
    let mut abba_count = 0;

    for line in lines {
        let mut abba = Abba::new();
        for ch in line.chars() {
            match ch {
                '[' => abba.reset(true),
                ']' => abba.reset(false),
                _   => {
                    if abba.find_abba(ch) && abba.hypernet {
                        break;
                    }
                },
            }
        }

        if abba.contains_abba && !abba.hypernet {
            abba_count += 1;
        }
    }

    println!("IP count: {}", abba_count);
}

fn task_b(lines: Vec<String>) {
    let mut ssl_count = 0;

    for line in lines {
        let mut ind = 0;
        let (supernet, hypernet): (Vec<&str>, Vec<&str>) = line
            .split(['[', ']'])
            .partition(|_| {
                let even_odd = ind % 2 == 0;
                ind += 1;
                even_odd
            });

        for s_net in supernet {
            let mut aba = Aba::new(s_net);
            if aba.find() && aba.is_match(&hypernet) {
                ssl_count += 1;
                break;
            }
        }
    }

    println!("SSL count: {}", ssl_count);
}

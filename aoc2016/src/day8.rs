use lazy_static::lazy_static;
use std::collections::HashMap;
use regex::Regex;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Debug)]
struct Screen {
    columns: i32,
    rows: i32,
    matrix: HashMap<(i32, i32), bool>,
}

impl Screen {
    fn new(lines: &[String]) -> Screen {

        let (columns, rows) = if lines.len() < 10 {
            (7, 3)
        } else {
            (50, 6)
        };

        let mut matrix = HashMap::new();
        for r in 0..rows {
            for c in 0..columns {
                matrix.insert((r, c), false);
            }
        }

        Screen {
            columns,
            rows,
            matrix,
        }
    }

    fn lights_on(&mut self, n_cols: i32, n_rows: i32) {
        for r in 0..n_rows {
            for c in 0..n_cols {
                self.matrix.insert((r, c), true);
            }
        }
    }

    fn count_lit(&self) -> usize {
        self.matrix
            .iter()
            .filter(|&(_, v)| *v)
            .count()
    }

    fn rotate_row(&mut self, row: i32, n_cols: i32) {
        let mut part_row = HashMap::new();
        for col_src in (self.columns - n_cols)..self.columns {
            part_row.insert((row, col_src), self.matrix[&(row, col_src)]);
        }
        for col_src in (0..self.columns - n_cols).rev() {
            let col_trg = col_src + n_cols;
            self.matrix.insert((row, col_trg), self.matrix[&(row, col_src)]);
        }
        for col_src in (self.columns - n_cols)..self.columns {
            let col_trg = col_src - (self.columns - n_cols);
            self.matrix.insert((row, col_trg), part_row[&(row, col_src)]);
        }
    }

    fn rotate_column(&mut self, col: i32, n_rows: i32) {
        let mut part_col = HashMap::new();
        for row_src in (self.rows - n_rows)..self.rows {
            part_col.insert((row_src, col), self.matrix[&(row_src, col)]);
        }
        for row_src in (0..self.rows - n_rows).rev() {
            let row_trg = row_src + n_rows;
            self.matrix.insert((row_trg, col), self.matrix[&(row_src, col)]);
        }
        for row_src in (self.rows - n_rows)..self.rows {
            let row_trg = row_src - (self.rows - n_rows);
            self.matrix.insert((row_trg, col), part_col[&(row_src, col)]);
        }
    }
}

fn task_a(lines: Vec<String>) {
    let mut screen = Screen::new(&lines);

    for line in lines {
        let (action, num1, num2) = parse_line(line);

        match action.as_str() {
            "column" => screen.rotate_column(num1, num2),
            "row"    => screen.rotate_row(num1, num2),
            "rect"   => screen.lights_on(num1, num2),
            _ => (),
        }
    }

    println!("Pixels turned on: {:?}", &screen.count_lit());

    println!("\nThe displayed code is:");
    for r in 0..screen.rows {
        for c in 0..screen.columns {
            print!("{}", if screen.matrix[&(r, c)] { '*' } else { ' ' });
        }
        println!();
    }
}

fn task_b(_lines: Vec<String>) {
    println!("Task B is solved in A!");
}

fn parse_line(line: String) -> (String, i32, i32) {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?P<rect>rect) (?P<rect_cols>\d+)x(?P<rect_rows>\d+)|(?P<row>rotate row y)=(?P<row_num>\d+) by (?P<row_by>\d+)|(?P<column>rotate column x)=(?P<col_num>\d+) by (?P<col_by>\d+)").unwrap();
    }

    let captures = RE.captures(&line).unwrap();

    let mut names = ["column", "row", "rect"].iter();
    let (action, (num1, num2)) = loop {
        let name = names.next().unwrap();
        if captures.name(name).is_some() {
            let subnames = match *name {
                "column" => ("col_num", "col_by"),
                "row"    => ("row_num", "row_by"),
                "rect"   => ("rect_cols", "rect_rows"),
                _ => ("", ""),
            };
            break (name.to_string(), subnames);
        }
    };

    let get_match = |num| {
        captures.name(num).unwrap().as_str().parse::<i32>().unwrap()
    };

    (action, get_match(num1), get_match(num2))

}

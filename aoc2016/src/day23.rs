use std::collections::HashMap;
use std::str::FromStr;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (mut registers, mut instructions) = parse_input(lines);
    registers.insert(Reg::A, 7);
    assembunny(&mut registers, &mut instructions);

    println!("register a = {}", registers[&Reg::A]);
}

fn task_b(lines: Vec<String>) {
    let (mut registers, mut instructions) = parse_input(lines);
    registers.insert(Reg::A, 9);
    assembunny(&mut registers, &mut instructions);

    println!("register a = {}", registers[&Reg::A]);
}

#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug)]
enum Reg {
    A, B, C, D,
    I(i32),
}

impl FromStr for Reg {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "a" => Ok(Reg::A),
            "b" => Ok(Reg::B),
            "c" => Ok(Reg::C),
            "d" => Ok(Reg::D),
            _ => Ok(Reg::I(s.parse::<i32>().unwrap())),
        }
    }
}

type Registers = HashMap<Reg, i32>;

#[derive(Debug)]
enum Instructions {
    Cpy(Reg, Reg),
    Jnz(Reg, Reg),
    Inc(Reg),
    Dec(Reg),
    Tgl(Reg),
}
use Instructions::*;

fn parse_input(lines: Vec<String>) -> (Registers, Vec<Instructions>) {
    let mut regs = Registers::new();
    let mut instr = Vec::<Instructions>::new();

    fn check_insert(regs: &mut Registers, k: &Reg, v: i32) {
        regs.entry(*k).or_insert(v);
    }

    for line in lines {
        let parts :Vec<&str> = line.split(' ').collect();
        let reg1 = parts[1].parse().unwrap();
        let inst = match parts[0] {
            "cpy" => {
                let reg2 = parts[2].parse().unwrap();
                check_insert(&mut regs, &reg2, 0);
                let num = if let Reg::I(num) = reg1 { num } else { 0 };
                check_insert(&mut regs, &reg1, num);
                Cpy(reg1, reg2)
            },
            "inc" => {
                check_insert(&mut regs, &reg1, 0);
                Inc(reg1)
            },
            "dec" => {
                check_insert(&mut regs, &reg1, 0);
                Dec(reg1)
            },
            "jnz" => {
                let reg2 = parts[2].parse().unwrap();
                let num = if let Reg::I(num) = reg1 { num } else { 0 };
                check_insert(&mut regs, &reg1, num);
                let num = if let Reg::I(num) = reg2 { num } else { 0 };
                check_insert(&mut regs, &reg2, num);
                Jnz(reg1, reg2)
            },
            "tgl" => {
                check_insert(&mut regs, &reg1, 0);
                Tgl(reg1)
            },
            _ => panic!("Gahhh!")
        };

        instr.push(inst);
    }

    (regs, instr)
}

fn assembunny(registers: &mut Registers, instructions: &mut [Instructions]) {
    let mut prog_index = 0;
    let ilen = instructions.len();
    while prog_index < ilen {
        let index_incr = match &instructions[prog_index] {
            Cpy(reg1, reg2) => {
                if let Reg::I(_) = reg2 {
                    // Invalid instruction
                    continue;
                }
                registers.insert(*reg2, *registers.get(reg1).unwrap());
                1
            },
            Jnz(reg1, reg2) => {
                if *registers.get(reg1).unwrap() != 0 {
                    *registers.get(reg2).unwrap()
                } else {
                    1
                }
            },
            Inc(reg) => {
                registers.insert(*reg, *registers.get(reg).unwrap() + 1);
                1
            },
            Dec(reg) => {
                registers.insert(*reg, *registers.get(reg).unwrap() - 1);
                1
            },
            Tgl(reg) => {
                let regv = *registers.get(reg).unwrap();
                let prg_idx = prog_index as i32 + regv;
                if prg_idx >= 0 &&
                   (prg_idx as usize) < instructions.len() {
                    let instr = match &instructions[prg_idx as usize] {
                        Cpy(reg1, reg2) => {
                            Jnz(*reg1, *reg2)
                        },
                        Jnz(reg, steps) => {
                            Cpy(*reg, *steps)
                        },
                        Inc(reg) => {
                            Dec(*reg)
                        },
                        Dec(reg) |
                        Tgl(reg) => {
                            Inc(*reg)
                        }
                    };

                    instructions[prg_idx as usize] = instr;
                }

                1
            },
        };

        prog_index = (prog_index as i32 + index_incr) as usize;
    }
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let pattern = get_pattern(&lines[0], &lines[1]);
    let mut n_safe = 0;
    for mut r in pattern {
        r.retain(|c| c != '^');
        n_safe += r.len();
    }

    println!("No of safe tiles = {}", n_safe);
}

fn task_b(lines: Vec<String>) {
    let pattern = get_pattern(&lines[0], "400000");
    let mut n_safe = 0;
    for mut r in pattern {
        r.retain(|c| c != '^');
        n_safe += r.len();
    }

    println!("No of safe tiles = {}", n_safe);
}

fn get_pattern(start_line: &str, n_rows_s: &str) -> Vec<String> {
    let n_rows = n_rows_s.parse::<u32>().unwrap();
    let mut work_line = start_line.to_string();
    let mut result = Vec::new();
    result.push(work_line.clone());
    for _ in 0..n_rows-1 {
        let line_n_wall = format!(".{}.", work_line);//.chars().collect::<Vec<char>>();
        work_line = String::new();

        for i in 0..line_n_wall.len()-2 {
            let test = &line_n_wall[i..i+3];
            let is_trap = matches!(test,
                "^^." |
                ".^^" |
                "..^" |
                "^.."
            );

            work_line.push(
                if is_trap { '^' } else { '.' }
            );
        }

        result.push(work_line.clone());
    }

    result
}
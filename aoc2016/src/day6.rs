use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut messages: Vec<Vec<char>> = vec![];
    for line in lines {
        messages.push(line.chars().collect());
    }

    let mut message = String::new();

    // Loop every column of the messages
    for col in 0..messages[0].len() {
        let mut char_counter = HashMap::new();
        let mut current_largest = ('-', 0);
        // Loop every message
        for message_it in messages.iter() {
            let ch = message_it[col];
            let count = char_counter.entry(ch).or_insert(0);
            *count += 1;
            if *count > current_largest.1 {
                current_largest = (ch, *count);
            }
        }

        message.push(current_largest.0);
    }

    println!("Error corrected message is: {}", message);
}

fn task_b(lines: Vec<String>) {
    let messages = split_messages(lines);

    let mut message = String::new();

    // Loop every column of the messages
    for col in 0..messages[0].len() {
        let mut char_counter = HashMap::new();
        // Loop every message
        for message_it in messages.iter() {
            let ch = message_it[col];
            char_counter.entry(ch).or_insert(0);
            let count = char_counter.get(&ch).unwrap() + 1;
            char_counter.insert(ch, count);
        }
        let smallest = char_counter.iter().reduce(|a, b| {
            if a.1 < b.1 { a } else { b }
        });

        message.push(*smallest.unwrap().0);
    }

    println!("Error corrected message is: {}", message);
}

fn split_messages(lines: Vec<String>) -> Vec<Vec<char>> {
    let mut messages: Vec<Vec<char>> = vec![];
    for line in lines {
        messages.push(line.chars().collect());
    }

    messages
}

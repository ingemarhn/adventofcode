pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let triangles = get_valid_triangles_from_lines(lines);

    println!("Number of valid triangles: {}", triangles.len());
}

fn task_b(lines: Vec<String>) {
    let n_triangles = get_valid_triangles_from_columns(lines);

    println!("Number of valid triangles: {}", n_triangles);
}

fn get_valid_triangles_from_lines(lines: Vec<String>) -> Vec<Vec<i32>> {
    let mut valid_triangles: Vec<Vec<i32>> = Vec::new();
    for line in lines {
        let lengths: Vec<i32> = line.split_whitespace()
                                    .map(|leng| leng.parse::<i32>().unwrap())
                                    .collect();
        let mut is_valid = true;
        for i1 in 0..=2 {
            let i2 = (i1 + 1) % 3;
            let i3 = (i1 + 2) % 3;
            if lengths[i1] + lengths[i2] <= lengths[i3] {
                is_valid = false;
                break;
            }
        }

        if is_valid {
            valid_triangles.push(lengths)
        }
    }

    valid_triangles
}

fn get_valid_triangles_from_columns(lines: Vec<String>) -> i32 {
    // Init Vector with lines*3 length.
    let n_lines = lines.len();
    let mut column_data = vec![1; n_lines * 3];

    // Loop all lines. Insert rectangle lengths from each column into the column_data vector
    for line_num in 0..n_lines {
        let lengths: Vec<i32> = lines[line_num].split_whitespace()
                                               .map(|leng| leng.parse::<i32>().unwrap())
                                               .collect();
        for i in 0..=2 {
            column_data[line_num + i * n_lines] = lengths[i];
        }
    }

    // All data is sorted in one long column. Find valid triangles.
    let mut n_valid_triangles = 0;
    let mut i = 0;
    while i < (column_data.len()) {
        let mut is_valid = true;
        for i1 in 0..=2 {
            let i2 = (i1 + 1) % 3;
            let i3 = (i1 + 2) % 3;
            if column_data[i+i1] + column_data[i+i2] <= column_data[i+i3] {
                is_valid = false;
                break;
            }
        }

        if is_valid {
            n_valid_triangles += 1;
        }

        i += 3;
    }

    n_valid_triangles
}

use std::collections::{HashMap, hash_map};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Debug, Clone, Copy)]
struct Position {
    current_direction: char,
    current_n_s_pos: i32,
    current_e_w_pos: i32,
}

impl Position {
    //fn next(&mut self, left_right: char, count: i32) -> Position {
    fn next(&mut self, direction: &str) -> Position {
        let direction_split = direction.split_at(1);
        let (left_right, count) = (direction_split.0.chars().next().unwrap(), direction_split.1.parse::<i32>().unwrap());
        match (self.current_direction, left_right) {
            ('N', 'L') => { self.current_direction = 'W'; self.current_e_w_pos += -count; },
            ('N', 'R') => { self.current_direction = 'E'; self.current_e_w_pos += count; },
            ('E', 'L') => { self.current_direction = 'N'; self.current_n_s_pos += count; },
            ('E', 'R') => { self.current_direction = 'S'; self.current_n_s_pos += -count; },
            ('S', 'L') => { self.current_direction = 'E'; self.current_e_w_pos += count; },
            ('S', 'R') => { self.current_direction = 'W'; self.current_e_w_pos += -count; },
            ('W', 'L') => { self.current_direction = 'S'; self.current_n_s_pos += -count; },
            ('W', 'R') => { self.current_direction = 'N'; self.current_n_s_pos += count; },
            _ => {println!("duh!");},
        }

        *self
    }
}

fn task_a(lines: Vec<String>) {
    let directions :Vec<&str> = lines[0].split(", ").collect();
    let mut current_pos = Position {current_direction: 'N', current_n_s_pos: 0, current_e_w_pos: 0};

    for direction in directions.iter() {
        current_pos = current_pos.next(direction);
    }
    println!("Easter Bunny HQ is {} blocks away", current_pos.current_n_s_pos.abs() + current_pos.current_e_w_pos.abs())
}

fn task_b(lines: Vec<String>) {
    let directions :Vec<&str> = lines[0].split(", ").collect();
    let mut current_pos = Position {current_direction: 'N', current_n_s_pos: 0, current_e_w_pos: 0};
    let mut visited = HashMap::new();
    visited.insert([0,0], 1);

    let mut check_and_fill_visited = |current_dir, ind: i32, v1, v2, static_val| -> Option<[i32; 2]> {
        let mut indices: [i32; 2] = [0, 0];
        let ind2: usize = (ind - 1).unsigned_abs() as usize;
        indices[ind2] = static_val;
        let mod_val = match current_dir {
            'N' | 'E' => 1,
            _         => -1,
        };
        let mut a = [v1 + mod_val, v2];
        a.sort();
        for x in a[0]..=a[1] {
            indices[ind as usize] = x;

            if let hash_map::Entry::Vacant(e) = visited.entry(indices) {
                e.insert(1);
            } else {
                return Some(indices);
            }
        }

        None
    };

    for direction in directions.iter() {
        let prev_pos = current_pos;
        current_pos = current_pos.next(direction);
        let has_been_there =
            if prev_pos.current_n_s_pos != current_pos.current_n_s_pos {
                check_and_fill_visited(current_pos.current_direction, 0, prev_pos.current_n_s_pos, current_pos.current_n_s_pos, current_pos.current_e_w_pos)
            } else {
                check_and_fill_visited(current_pos.current_direction, 1, prev_pos.current_e_w_pos, current_pos.current_e_w_pos, current_pos.current_n_s_pos)
        };

        if let Some(visited_pos) = has_been_there {
            println!("Easter Bunny HQ is {} blocks away", visited_pos[0].abs() + visited_pos[1].abs());
            break;
        }
    }
}

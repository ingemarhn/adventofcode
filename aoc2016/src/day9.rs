use regex::Regex;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut decoder = Decoder::new(lines[0].as_str());
    decoder.decompress();
    println!("Decrompressed length: {}", decoder.decoded.len());
}

fn task_b(lines: Vec<String>) {
    let mut decoder = Decoder::new(lines[0].as_str());
    decoder.decompress2();
    println!("Decrompressed length: {}", decoder.decoded_len);
}

#[derive(Debug)]
struct Decoder<'a> {
    encoded_left: &'a str,
    decoded: String,
    decoded_len: i64,
    indicator_reg: Regex,
}

impl<'a> Decoder<'a> {
    fn new(s: &'a str) -> Decoder<'a> {
        Decoder {
            encoded_left: s,
            decoded: String::new(),
            decoded_len: 0,
            indicator_reg: Regex::new(r"(\w*)\((\d+)x(\d+)\)").unwrap(),
        }
    }

    fn decompress(&mut self) {
        while let Some(matches) = self.indicator_reg.captures(self.encoded_left) {
            let get_end = |num| {
                matches.get(num).unwrap().range().end
            };
            let get_match = |num| {
                matches.get(num).unwrap().as_str()
            };
            let parse_match = |num| {
                get_match(num).parse::<usize>().unwrap()
            };

            self.encoded_left = &self.encoded_left[get_end(0)..];
            self.decoded.push_str(get_match(1));
            let repeat_str = &self.encoded_left[..parse_match(2)];
            self.encoded_left = &self.encoded_left[parse_match(2)..];
            for _i in 0..parse_match(3) {
                self.decoded.push_str(repeat_str);
            }
        }

        self.decoded.push_str(self.encoded_left);
        self.encoded_left = "";
    }

    fn decompress2(&mut self) {
        while let Some(matches) = self.indicator_reg.captures(self.encoded_left) {
            let get_end = |num| {
                matches.get(num).unwrap().range().end
            };
            let get_match = |num| {
                matches.get(num).unwrap().as_str()
            };
            let parse_match = |num| {
                get_match(num).parse::<usize>().unwrap()
            };

            self.encoded_left = &self.encoded_left[get_end(0)..];
            self.decoded_len += get_match(1).len() as i64;
            let repeat_str_len = parse_match(2);
            let mut repeat_decoder = Decoder::new(&self.encoded_left[..repeat_str_len]);
            repeat_decoder.decompress2();
            self.encoded_left = &self.encoded_left[repeat_str_len..];
            self.decoded_len += repeat_decoder.decoded_len * parse_match(3) as i64;
        }

        self.decoded_len += self.encoded_left.len() as i64;
        self.encoded_left = "";
    }
}
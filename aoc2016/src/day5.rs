pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let door_id = &lines[0];
    let mut pwd = String::new();

    let mut counter = 0;
    let mut pwd_char_count = 0;
    while pwd_char_count < 8 {
        let hash = get_hash(door_id, counter);

        if &hash[..5] == "00000" {
            pwd.push_str(&hash[5..6]);
            pwd_char_count += 1;
        }

        counter += 1;
    }

    println!("Password is: {}", pwd);
}

fn task_b(lines: Vec<String>) {
    let door_id = &lines[0];
    let mut pwd = vec!['-'; 8];

    let mut counter = 0;
    let mut pwd_char_count = 0;
    while pwd_char_count < 8 {
        let hash = get_hash(door_id, counter);

        if &hash[..5] == "00000" {
            let test_char: char = hash[5..6].chars().next().unwrap();
            if ('0' ..= '7').contains(&test_char) {
                let index = test_char.to_digit(10).unwrap() as usize;
                if pwd[index] == '-' {
                    pwd[index] = hash[6..7].chars().next().unwrap();
                    pwd_char_count += 1;
                }
            }
        }

        counter += 1;
    }

    println!("Password is: {}", &pwd.into_iter().collect::<String>());
}

fn get_hash(door_id: &str, counter: i32) -> String {
    let mut candidate = String::new();
    candidate.push_str(door_id);
    candidate.push_str(&counter.to_string());

    format!("{:x}", md5::compute(candidate.as_bytes()))
}

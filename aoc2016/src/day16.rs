pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let disk = get_disk(&lines[0], lines[1].parse::<u32>().unwrap());
    let chksum = calc_checksum(disk);

    println!("chksum = {}", chksum);
}

fn task_b(lines: Vec<String>) {
    let disk = get_disk(&lines[0], 35651584);
    let chksum = calc_checksum(disk);

    println!("chksum = {}", chksum);
}

fn get_disk(start: &String, size: u32) -> String {
    let mut sb: Vec<u8> = start.as_bytes().to_vec();

    while sb.len() < size as usize {
        let sbv = sb.clone().into_iter().rev().collect::<Vec<u8>>();
        let sbv = sbv.iter().map(|b| if *b == 48 { 49 } else { 48 }).collect::<Vec<u8>>();
        sb.push(48);
        sb.extend(&sbv);
    };

    String::from_utf8(sb[0..size as usize].to_vec()).unwrap()
}

fn calc_checksum(disk: String) -> String {
    let mut c_sum = disk.as_bytes();

    let mut csc;
    while c_sum.len() % 2 == 0 {
        let mut cs = Vec::new();
        let mut i = 0;
        while i < c_sum.len() {
            let c = if c_sum[i] == c_sum[i+1] { 49 } else { 48 };
            cs.push(c);
            i += 2;
        }

        csc = cs.clone();
        c_sum = &csc[..];
    }

    String::from_utf8(c_sum.to_vec()).unwrap()
}

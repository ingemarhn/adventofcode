use regex::Regex;
use std::ops;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let all_ranges = parse_input(&lines);
    let merged_ranges = merge_ranges(all_ranges);
    let lowest = find_lowest(merged_ranges);
    println!("Lowest-valued IP: {}", lowest);
}

fn task_b(lines: Vec<String>) {
    let all_ranges = parse_input(&lines);
    let merged_ranges = merge_ranges(all_ranges);
    let n_addr = find_n_ips(merged_ranges);
    println!("# of free IPs: {}", n_addr);
}

type Ranges = Vec<ops::RangeInclusive<u32>>;

fn parse_input(lines: &[String]) -> Ranges {
    let line_reg: Regex = Regex::new(r"(\d+)-(\d+)").unwrap();
    let mut ranges = Ranges::new();

    for line in lines {
        let captures = line_reg.captures(line).unwrap();
        ranges.push(captures.get(1).unwrap().as_str().parse().unwrap() ..= captures.get(2).unwrap().as_str().parse().unwrap())
    }

    ranges.sort_by(|a, b| { a.start().cmp(b.start())});
    ranges
}

fn merge_ranges(ranges_in: Ranges) -> Ranges {
    let mut ranges_out = Ranges::new();

    for range in ranges_in {
        let (lower, upper) = range.clone().into_inner();
        let mut range_ref = (0, usize::MAX);
        for (oi, ran_it) in ranges_out.iter().enumerate() {
            if ran_it.contains(&lower) {
                range_ref = (1, oi);
                break;
            } else if ran_it.contains(&upper) {
                range_ref = (2, oi);
                break;
            }
        }

        match range_ref.0 {
            0 => ranges_out.push(range),
            1 => if upper > *ranges_out[range_ref.1].end() { ranges_out[range_ref.1] = *ranges_out[range_ref.1].start() ..= upper },
            _ => ranges_out[range_ref.1] = lower ..= *ranges_out[range_ref.1].end(),
        }
    }

    ranges_out.sort_by(|a, b| { a.start().cmp(b.start())});
    ranges_out
}

fn find_lowest(ranges: Ranges) -> u32 {
    let mut lowest = 0;

    for i in 0..ranges.len() {
        let end = *ranges[i].end();
        if end + 1 < *ranges[i + 1].start() {
            lowest = end + 1;
            break;
        }
    }

    lowest
}

fn find_n_ips(ranges: Ranges) -> u32 {
    let mut n_ips = 0;

    let r_up = ranges.len() - 1;
    for i in 0..r_up {
        n_ips += *ranges[i + 1].start() - *ranges[i].end() - 1;
    }
    n_ips += u32::MAX - *ranges[r_up].end();

    n_ips
}

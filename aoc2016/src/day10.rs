use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::str::FromStr;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (low_comp, high_comp) = if lines.len() > 10 { (17, 61) } else { (2, 5) };
    let mut bots: BotArray = HashMap::new();
    let mut outputs: OutputArray = HashMap::new();

    // Sort the lines to make all hand out instructions come first to make sure that all instructions are set before any bot reaches to chips
    let mut lines_m = lines;
    lines_m.sort();

    for line in &lines_m {
        let action_elem = LineKind::from_str(line.as_str()).unwrap();
        match action_elem {
            LineKind::Bot(bot_id, low_trg, low_trg_id, high_trg, high_trg_id) => {
                let mut bot = bots.entry(bot_id).or_insert_with(Bot::new).clone();
                bot.receiver_low = (low_trg, low_trg_id);
                bot.receiver_high = (high_trg, high_trg_id);
                bots.insert(bot_id, bot);
            },
            LineKind::Value(value, bot_id) => {
                let chip_count = Bot::add_value_chip(&mut bots, bot_id, value);

                if chip_count == 2 {
                    bot_handout(bot_id, &mut bots, &mut outputs);
                }
            },
        }
    }

    let chk_receiver = |receiver: &Receiver, &value| -> i32 {
        match receiver {
            (Output, id) => {
                if [0, 1, 2].contains(id) {
                    value
                } else {
                    0
                }
            },
            _ => 0,
        }
    };

    let mut resp_bot = -1;
    let mut output_product = 1;
    for (id, bot) in &bots {
        if bot.low_value == low_comp && bot.high_value == high_comp {
            resp_bot = *id;
        }

        let output_val = chk_receiver(&bot.receiver_low, &bot.low_value);
        if output_val > 0 {
            output_product *= output_val;
        }
        let output_val = chk_receiver(&bot.receiver_high, &bot.high_value);
        if output_val > 0 {
            output_product *= output_val;
        }
    }
    println!("Bot {} is responsible for comparing values {} and {}", resp_bot, low_comp, high_comp);
    println!("Multiplied output values is {}", output_product);
}

fn task_b(_lines: Vec<String>) {
    println!("Implemented in task_a");
}

type BotArray = HashMap<i32, Bot>;
type OutputArray = HashMap<i32, Output>;
type Receiver = (Target, i32);

#[derive(Debug)]
enum LineKind {
    Value(i32, i32),
    Bot(i32, Target, i32, Target, i32),
}

impl std::str::FromStr for LineKind {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE_V: Regex = Regex::new(r"value (\d+) goes to bot (\d+)").unwrap();
            static ref RE_B: Regex = Regex::new(r"bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)").unwrap();
        }

        let mtch =   |capts: &regex::Captures, num| -> String { capts.get(num).unwrap().as_str().to_string() };
        let mtch_i = |capts: &regex::Captures, num|           { mtch(capts, num).parse::<i32>().unwrap() };
        let mtch_t = |capts: &regex::Captures, num|           { mtch(capts, num).parse::<Target>().unwrap() };

        if let Some(cu) = RE_B.captures(s) {
            Ok(LineKind::Bot(mtch_i(&cu, 1), mtch_t(&cu, 2), mtch_i(&cu, 3), mtch_t(&cu, 4), mtch_i(&cu, 5)))
        } else if let Some(cu) = RE_V.captures(s) {
            Ok(LineKind::Value(mtch_i(&cu, 1), mtch_i(&cu, 2)))
        } else {
            Err( format!("Error matching '{}' as LineKind!", s) )
        }
    }
}

#[derive(Debug, Clone)]
enum Target {
    Nothing,
    Bot,
    Output,
}
use Target::*;

impl std::str::FromStr for Target {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "nothing" => Ok(Nothing),
            "bot"     => Ok(Bot),
            "output"  => Ok(Output),
            _ => Err(format!("'{}' is not a valid value for Target", s)),
        }
    }
}

#[derive(Debug, Clone)]
struct Bot {
    chip_count: i32,
    low_value: i32,
    high_value: i32,
    receiver_low: Receiver,
    receiver_high: Receiver,
}

impl Bot {
    fn new() -> Bot {
        Bot {
            chip_count: 0,
            low_value: 0,
            high_value: 0,
            receiver_low: (Nothing, 0),
            receiver_high: (Nothing, 0),
        }
    }

    fn add_value_chip(bots: &mut BotArray, id: i32, value: i32) -> i32 {
        let mut b = bots.entry(id).or_insert_with(Self::new).clone();

        if b.low_value == 0 {
            b.low_value = value;
        } else if b.low_value < value {
            b.high_value = value;
        } else {
            b.high_value = b.low_value;
            b.low_value = value;
        }
        b.chip_count += 1;
        let chip_count = b.chip_count;
        bots.insert(id, b);

        chip_count
    }
}

#[derive(Debug)]
struct Output {
}

impl Output {
    fn new() -> Output {
        Output {
        }
    }
}

fn bot_handout(bot_id: i32, bots: &mut BotArray, outputs: &mut OutputArray) {
    let bot = bots[&bot_id].clone();

    let mut upd_receiver = |receiver, &value| {
        match receiver {
            (Bot, id) => {
                let chip_count = Bot::add_value_chip(bots, id, value);
                if chip_count == 2 {
                    bot_handout(id, bots, outputs);
                }
            },
            (Output, id) => {
                outputs.insert(id, Output::new());
            },
            (Nothing, _) => panic!("Receiver for bot id {} not set!", bot_id),
        }
    };

    upd_receiver(bot.receiver_low, &bot.low_value);
    upd_receiver(bot.receiver_high, &bot.high_value);
}

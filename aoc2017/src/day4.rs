use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn count_valid_words(all_words: Vec<Vec<&str>>, sort_word: bool) -> usize {
    all_words
        .iter()
        .fold(0, |n_val, words| {
            let new_words = words
                .iter()
                .fold(HashMap::<String, usize>::new(), |mut nw_wds, word| {
                    let c_word = if sort_word {
                        let mut wword = word.chars().map(|c| c.to_string()).collect::<Vec<String>>();
                        wword.sort();
                        wword.concat()
                    } else {
                        word.to_string()
                    };
                    let ns_wd = c_word;
                    nw_wds.insert(ns_wd, 0);
                    nw_wds
                });

            if words.len() == new_words.len() {
                n_val + 1
            } else {
                n_val
            }
        })
}

fn task_a(lines: Vec<String>) {
    let all_words = parse_input(&lines);
    let n_valid = count_valid_words(all_words, false);

    println!("# of valid passphrases: {n_valid}");
}

fn task_b(lines: Vec<String>) {
    let all_words = parse_input(&lines);
    let n_valid = count_valid_words(all_words, true);

    println!("# of valid passphrases: {n_valid}");
}

fn parse_input(lines: &[String]) -> Vec<Vec<&str>> {
    lines.iter().map(|line| line.split(' ').collect()).collect()
}

#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::HashMap;

    pub type State = char;
    pub type Value = u8;
    pub type Direction = i8;
    pub type StateInstruct = HashMap<State, StateData>;

    #[derive(Debug)]
    pub struct StateData {
        new_value: [Value; 2],
        move_dir: [Direction; 2],
        next_state: [State; 2],
    }

    impl StateData {
        pub fn new(new_value: [Value; 2], move_dir: [Direction; 2], next_state: [char; 2]) -> Self {
            Self { new_value, move_dir, next_state }
        }

        pub fn get_data(&self, curr_value: Value) -> (Value, Direction, State) {
            let i = curr_value as usize;
            (self.new_value[i], self.move_dir[i], self.next_state[i])
        }
    }
}
use std::collections::HashMap;

use internal::*;

fn task_a(lines: Vec<String>) {
    let (mut state, n_steps, state_instructs) = parse_input(&lines);

    let mut tape = HashMap::<i32, Value>::new();
    let mut cursor = 0;

    tape.insert(0, 0);

    for i in 0 .. n_steps {
        let instruct = &state_instructs[&state];
        let val = tape.get_mut(&cursor).unwrap();
        let (n_val, dir, n_state) = instruct.get_data(*val);
        *val = n_val;
        cursor += dir as i32;
        tape.entry(cursor).or_insert(0);
// println!("(state, n_val, dir, n_state, cursor) = {:?}", (state, n_val, dir, n_state, cursor));
// println!("tape = {:?}", tape);
// if i % 100000 == 0 {println!("{}", i);}
        state = n_state;
    }

    let count =
        tape
        .values()
        .fold(0, |sum, k| sum + *k as u32);

    println!("Diagnostic checksum = {count}");
}

fn task_b(lines: Vec<String>) {
    let _ = parse_input(&lines);
}

fn parse_input(lines: &[String]) -> (State, u32, StateInstruct) {
    let mut state_instruct = StateInstruct::new();
    let init_state =
        lines[0]
            .split(' ')
            .nth(3)
            .unwrap()
            .chars()
            .next()
            .unwrap();

    let n_steps =
        lines[1]
            .split(' ')
            .nth(5)
            .unwrap()
            .parse::<u32>()
            .unwrap();

    let mut state = ' ';
    let mut curr_val = 0;
    let mut vals = [0; 2];
    let mut dirs = [0; 2];
    let mut n_states = [' '; 2];

    let get_ch = |line: &str, ind| {
        line.chars().nth(ind).unwrap()
    };
    let get_dig = |line, ind| {
        get_ch(line, ind).to_digit(10).unwrap()
    };

    for line in lines[3..].iter() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        match &line[0..8] {
            "In state" => state = get_ch(line, 9),
            "If the c" => curr_val = get_dig(line, 24) as usize,
            "- Write " => {
                              let val =  get_dig(line, 18) as Value;
                              vals[curr_val] = val
                          },
            "- Move o" => dirs[curr_val] =
                              match line.split_ascii_whitespace().nth(6).unwrap() {
                                  "right." => 1,
                                  _ => -1,
                              },
            "- Contin" => {
                              n_states[curr_val] = get_ch(line, 22);
                              if curr_val == 1 {
                                  let state_data = StateData::new(vals, dirs, n_states);
                                  state_instruct.insert(state, state_data);
                              }
                          },
            _ => panic!("Crap!!! What's this line: '{line}'?"),
        }
    }

    (init_state, n_steps, state_instruct)
}

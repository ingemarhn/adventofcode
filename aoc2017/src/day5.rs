pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn calc_steps(offs: Vec<i32>, is_at_3: bool) -> i32 {
    let mut offsets = offs;
    let offsets_len = offsets.len();

    let mut steps = 0;
    let mut offset_ptr = 0;
    for i in 1.. {
        let jmp = offsets[offset_ptr];
        if is_at_3 && jmp >= 3 {
            offsets[offset_ptr] -= 1;
        } else {
            offsets[offset_ptr] += 1;
        }

        offset_ptr = offset_ptr.saturating_add_signed(jmp as isize);
        if offset_ptr >= offsets_len {
            steps = i;
            break;
        }
    }

    steps
}

fn task_a(lines: Vec<String>) {
    let offsets = parse_input(&lines);
    let steps = calc_steps(offsets, false);

    println!("# of steps = {steps}");
}

fn task_b(lines: Vec<String>) {
    let offsets = parse_input(&lines);
    let steps = calc_steps(offsets, true);

    println!("# of steps = {steps}");
}

fn parse_input(lines: &[String]) -> Vec<i32> {
    lines
        .iter()
        .map(|l| l.parse().unwrap())
        .collect()
}

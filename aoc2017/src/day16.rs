pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type DanceMoves = Vec<DanceMove>;
type Position = u8;
type ProgName = char;
type Program = [ProgName; 16];

#[derive(Debug)]
enum DanceMove {
    Spin(Position),
    Exchange(Position, Position),
    Partner(ProgName, ProgName),
}

fn init_program() -> Program {
    core::array::from_fn(|i| char::from_u32(i as u32 + 97).unwrap())
}

fn spin(prog: &Program, len: &Position) -> Program {
    let mut ret_prog: Program = *prog;
    let ulen = *len as usize;
    for i in 0..ulen {
        ret_prog[i] = prog[prog.len() - ulen + i];
    }
    ret_prog[ulen..prog.len()].copy_from_slice(&prog[..(prog.len() - ulen)]);

    ret_prog
}

fn xchange(prog: &Program, x: &Position, y: &Position) -> Program {
    let mut ret_prog: Program = *prog;
    ret_prog[*x as usize] = prog[*y as usize];
    ret_prog[*y as usize] = prog[*x as usize];

    ret_prog
}

fn do_dance(init_program: Program, moves: &DanceMoves) -> Program {
    moves.iter().fold(init_program, |program, mv| {
        let prog = match mv {
            DanceMove::Spin(len) => spin(&program, len),
            DanceMove::Exchange(x, y) => xchange(&program, x, y),
            DanceMove::Partner(n1, n2) => {
                let x = program.iter().position(|&c| c == *n1).unwrap();
                let y = program.iter().position(|&c| c == *n2).unwrap();
                xchange(&program, &(x as Position), &(y as Position))
            },
        };

        prog
    })
}

fn task_a(lines: Vec<String>) {
    let moves = parse_input(&lines);
    let init_prog = init_program();

    let end_prog = do_dance(init_prog, &moves);

    println!("Final program order = {}", end_prog.into_iter().fold(String::new(), |mut s, c| { s.push(c); s }));
}

fn task_b(lines: Vec<String>) {
    let moves = parse_input(&lines);
    let init_prog = init_program();

    // By testing I found that the program positions loop (60 permutations in my case)
    // Run a loop until a program standing equal to the start standing is found
    let mut prg = init_prog;
    let mut count = 0;
    loop {
        prg = do_dance(prg, &moves);
        count += 1;
        if prg == init_prog {
            break
        }
    }

    let end_prog = (0..(1000000000 % count)).fold(init_prog, |prg, _| {
        do_dance(prg, &moves)
    });

    println!("Final program order = {}", end_prog.into_iter().fold(String::new(), |mut s, c| { s.push(c); s }));
}

fn parse_input(lines: &[String]) -> DanceMoves {
    lines[0].split(',').fold(Vec::new(), |mut moves, mv| {
        let mv_kind = mv.chars().next().unwrap();
        let this_mv = match mv_kind {
            's' => DanceMove::Spin(mv[1..].parse().unwrap()),
            'x' => {
                let pos_s = mv[1..].split('/').map(|e| e.parse().unwrap()).collect::<Vec<Position>>();
                DanceMove::Exchange(pos_s[0], pos_s[1])
            },
            'p' => {
                let names = mv[1..].split('/').map(|e| e.parse().unwrap()).collect::<Vec<ProgName>>();
                DanceMove::Partner(names[0], names[1])
            },
            _ => panic!("Shejt! Bad mvKind: {mv_kind}")
        };
        moves.push(this_mv);

        moves
    })
}

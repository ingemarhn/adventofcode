use std::collections::HashMap;

use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn transform(inp: &[String]) -> Vec<Vec<String>> {
    let inp = inp.to_vec();
    let mut ret_vec = Vec::new();

    // Save unconverted first
    ret_vec.push(inp.to_vec());

    // Flip V
    let flip_v = |inp: &[String]| {
        let mut ret = Vec::new();
        for i in (0..inp.len()).rev() {
            ret.push(inp[i].clone());
        }

        ret
    };
    ret_vec.push(flip_v(&inp));

    // Flip H
    let flip_h = |inp: &[String]| {
        let mut ret = Vec::new();
        for ip in inp {
            let mut s = String::new();
            ret.push(String::new());
            for i in (0..ip.len()).rev() {
                let c = ip.chars().nth(i).unwrap();
                s.push(c);
                let j = ret.len() - 1;
                ret[j].push(c);
            }
        }

        ret
    };
    ret_vec.push(flip_h(&inp));

    // Rotate three times
    let mut next: Vec<String> =
        inp
        .iter()
        .cloned()
        .collect_vec();
    let box_len = next.len();

    for _ in 0..3 {
        let curr =
            next
            .iter()
            .map(|s|
                s
                .chars()
                .collect::<Vec<_>>()
            )
            .collect::<Vec<_>>();

        ret_vec.push(Vec::new());
        for i in 0..box_len {
            let rvi = ret_vec.len() - 1;
            ret_vec[rvi].push(String::new());
            for j in (0..box_len).rev() {
                let rvj = ret_vec[rvi].len() - 1;
                ret_vec[rvi][rvj].push(curr[j][i]);
            }
        }

        next.clone_from(&ret_vec[ret_vec.len() - 1]);
        ret_vec.push(flip_v(&next));
        ret_vec.push(flip_h(&next));
    }

    ret_vec
}

fn count_pixels(lines: &[String], n_loops: usize) -> usize {
    let input_output_s = parse_input(lines);

    let input_output =
        input_output_s
        .iter()
        .fold(HashMap::new(), |mut hmap, in_ou| {
            let tr = transform(&in_ou[0]);
            for itr in tr {
                hmap.insert(itr.join("/"), in_ou[1].join("/"));
            }

            hmap
        });

    let mut current_map: String = input_output[&String::from(".#./..#/###")].clone();

    for _ in 1 .. n_loops {
        let box_lines: Vec<&str> = current_map.split('/').collect();
        let box_size = box_lines.len();
        let divisor = match box_size % 2 {
            0 => 2,
            _ => 3,
        };

        let buf_elem_size = box_size / divisor;
        let mut buf = vec![String::new(); buf_elem_size * buf_elem_size];
        for (line_ind, line) in box_lines.iter().enumerate() {
            let offset = line_ind / divisor;
            for buf1_ind in 0 .. box_size / divisor {
                let slc_start = buf1_ind * divisor;
                let slc = &line[slc_start .. slc_start + divisor];
                let buf_index = offset * buf_elem_size + buf1_ind;
                buf[buf_index].push_str(slc);
                buf[buf_index].push('/');
            }
        }
        for buf_it in &mut buf {
            buf_it.pop();
        }

        let mut new_box = vec![String::new(); (divisor + 1) * (box_size / divisor)];
        for (i, grid_part) in buf.iter().enumerate() {
            for (j, new_part) in input_output[grid_part].split('/').enumerate() {
                // let nl = new_part.len();
                // let nbi = (i / (nl-1)) * nl + j;
                // let nbi = (i / buf_elem_size) * (buf_elem_size + 1) + j;
                let nbi = (i / buf_elem_size) * (buf_elem_size + 1 - (buf_elem_size - divisor)) + j;
                new_box[nbi].push_str(new_part);
            }
        }

        current_map = new_box.join("/");
    }

    current_map
    .chars()
    .fold(0, |count, v| {
        if v == '#' {
            count + 1
        } else {
            count
        }
    })
}

fn task_a(lines: Vec<String>) {
    let count = count_pixels(&lines, 5);
    println!("# of pixels on = {count}");
}

fn task_b(lines: Vec<String>) {
    let count = count_pixels(&lines, 18);
    println!("# of pixels on = {count}");
}

/*
		flip h	flip v	rot 1	rot 2	rot 3

abc		cba		ghi		gda		ihg		cfi               ghi  adg
def		fed		def		heb		fed		beh               def  beh
ghi		ihg		abc		ifc		cba		adg               abc  cfi

ab		ba		cd		ca		dc		bd
cd		dc		ab		db		ba		ac

3x3:
    - Middle cell is always the same
    - create a hashtable with the three middle cells as key, containing a vector of standard/flipped/rotated squares
        - consider to leave out duplicates

2x2:
    - create a vector of vector of standard/flipped/rotated squares
        - consider to leave out duplicates
*/
fn parse_input(lines: &[String]) -> Vec<Vec<Vec<String>>> {
    lines
        .iter()
        .map(|line|
            line
            .split(" => ")
            .map(|in_out|
                in_out
                .split('/')
                .map(|s| s.to_string())
                .collect::<Vec<_>>()
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

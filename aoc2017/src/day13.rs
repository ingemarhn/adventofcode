#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Layers = Vec<Layer>;

trait Move {
    fn move_scanners(&mut self);
}

impl Move for Layers {
    fn move_scanners(&mut self) {
        for self_i in self {
            self_i.move_me();
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum MoveDirs {
    Down = 1,
    Up = -1,
}
use MoveDirs::*;

#[derive(Clone, Debug)]
struct Layer {
    number: i8,
    depth: i8,
    scanner_pos: i8,
    move_dir: MoveDirs,
}

impl Layer {
    fn new(number: i8, depth: i8) -> Self {
        Self { number, depth, scanner_pos: 0, move_dir: Down }
    }

    fn move_me(&mut self) {
        if self.scanner_pos == 0 {
            self.move_dir = Down
        } else if self.scanner_pos == self.depth - 1 {
            self.move_dir = Up
        }

        self.scanner_pos += self.move_dir as i8;
    }
}

fn task_a(lines: Vec<String>) {
    let mut layers = parse_input(&lines);
    let mut severity = 0;

    let mut lay_ind = 0;
    for pico_s in 0 ..= layers.last().unwrap().clone().number as usize {
        let layer = layers[lay_ind].clone();

        if layer.number != pico_s as i8 {
            layers.move_scanners();
            continue;
        }

        if layer.scanner_pos == 0 {
            severity += layer.number as usize * layer.depth as usize;
        }
        layers.move_scanners();
        lay_ind += 1;
    }

    println!("Severity = {severity}");
}

fn task_b(lines: Vec<String>) {
    let mut layers_base = parse_input(&lines);
    let mut layers;
    let h_layer = layers_base.last().unwrap().clone().number as usize;

    let mut s_delay = 0;
    loop {
        layers = layers_base.clone();
        let mut lay_ind = 0;

        s_delay += 1;
        layers.move_scanners();
        layers_base.clone_from(&layers);
        let mut is_caught = false;
        for pico_s in 0 ..= h_layer {
            let layer = layers[lay_ind].clone();

            if layer.number != pico_s as i8 {
                layers.move_scanners();
                continue;
            }

            if layer.scanner_pos == 0 {
                is_caught = true;
                break;
            }

            layers.move_scanners();
            lay_ind += 1;
        }

        if !is_caught {
            break
        }
    }

    println!("Picoseconds delay = {s_delay}");
}

fn parse_input(lines: &[String]) -> Layers {
    lines.iter().fold(Vec::new(), |mut layers, line| {
        let mut elems = line
            .split(": ")
            .map(|e| e.parse().unwrap() );

        layers.push( Layer::new(elems.next().unwrap(), elems.next().unwrap()) );

        layers
    })
}

use std::collections::HashMap;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time::Duration;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Reg = char;
type Val = i64;
type Assembly = Vec<Instruction>;
type Registers = HashMap<Reg, Val>;

#[derive(Clone, Copy, Debug)]
enum Source {
    Reg(Reg),
    Val(Val),
    None,
}

impl Source {
    fn get_source(src: &str, rf: fn(&str) -> Reg, arg: &str) -> Self {
        if let Ok(s) = src.parse::<Val>() {
            Source::Val(s)
        } else {
            Source::Reg(rf(arg))
        }
    }

    fn get_val(&self, regs: &Registers) -> Val {
        match *self {
            Self::Reg(reg) => regs[&reg],
            Self::Val(val) => val,
            Self::None => panic!("Source is None!")
        }
    }
}
#[derive(Clone, Copy, Debug)]
enum Instruction {
    Add(Reg, Source),
    Jgz(Source, Source),
    Mod(Reg, Source),
    Mul(Reg, Source),
    Rcv(Reg),
    Set(Reg, Source),
    Snd(Reg),
}

impl Instruction {
    fn new(instr_parts: Vec<&str>) -> Self {
        let src = if instr_parts.len() == 3 {
            Source::get_source(instr_parts[2], Self::get_reg_name, instr_parts[2])
        } else {
            Source::None
        };
        let r = Self::get_reg_name(instr_parts[1]);

        match instr_parts[0] {
            "add" => Self::Add(r, src),
            "jgz" => {
                let r = Source::get_source(instr_parts[1], Self::get_reg_name, instr_parts[1]);
                Self::Jgz(r, src)
            },
            "mod" => Self::Mod(r, src),
            "mul" => Self::Mul(r, src),
            "rcv" => Self::Rcv(r),
            "set" => Self::Set(r, src),
            "snd" => Self::Snd(r),
            _ => panic!("Invalid instr")
        }
    }

    fn get_reg_name(instr: &str) -> Reg {
        instr.chars().next().unwrap()
    }
}

#[derive(PartialEq)]
enum ThreadState {
    Recv,
    Other,
}

/*
snd X plays a sound with a frequency equal to the value of X.
set X Y sets register X to the value of Y.
add X Y increases register X by the value of Y.
mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
mod X Y sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
rcv X recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
jgz X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction,
    an offset of -1 jumps to the previous instruction, and so on.)
*/

fn run_prog_to_first_rcv(assembly: Assembly, regs: Registers) -> Val {
    let mut prog_pointer = 0;
    let mut registers = regs;
    let mut last_sent_sound = 0;

    loop {
        let instr = assembly[prog_pointer];
        match instr {
            Instruction::Add(reg, src) => *registers.get_mut(&reg).unwrap() += src.get_val(&registers),
            Instruction::Jgz(src_r, src_s) => if src_r.get_val(&registers) > 0 {
                let t_ptr = prog_pointer as Val + src_s.get_val(&registers);
                if t_ptr < 0 || t_ptr >= assembly.len() as Val {
                    break 0
                }
                prog_pointer = t_ptr as usize
            } else {
                prog_pointer += 1
            },
            Instruction::Mod(reg, src) => {
                let sv = src.get_val(&registers);
                if sv != 0 {
                    let rv = *registers.get(&reg).unwrap();
                    *registers.get_mut(&reg).unwrap() = rv % sv
                }
            },
            Instruction::Mul(reg, src) => *registers.get_mut(&reg).unwrap() *= src.get_val(&registers),
            Instruction::Rcv(reg) => if registers[&reg] > 0 && last_sent_sound != 0 {
                break last_sent_sound
            },
            Instruction::Set(reg, src) => *registers.get_mut(&reg).unwrap() = src.get_val(&registers),
            Instruction::Snd(reg) => last_sent_sound = registers[&reg],
        }

        if !matches!(instr, Instruction::Jgz(_, _)) {
            prog_pointer += 1;
        }
    }
}

fn run_prog_to_both_rcv(assembly: Assembly, regs: Registers, sndv: Sender<Val>, rcvv: Receiver<Val>, snds: Sender<ThreadState>, rcvs: Receiver<ThreadState>) -> Val {
    let mut prog_pointer = 0;
    let mut registers = regs;
    let mut count_sends = 0;
    let dur = Duration::from_millis(5);

    let last_state = |rcv: &Receiver<ThreadState>| {
        let mut lst = ThreadState::Other;
        loop {
            let st = rcv.recv_timeout(dur);
            match st {
                Ok(rst) => lst = rst,
                Err(_) => break lst,
            };
        }
    };

    loop {
        let instr = assembly[prog_pointer];
        if let Instruction::Rcv(_) = instr {
            snds.send(ThreadState::Recv).unwrap();
        } else {
            snds.send(ThreadState::Other).unwrap();
        }
        match instr {
            Instruction::Add(reg, src) => *registers.get_mut(&reg).unwrap() += src.get_val(&registers),
            Instruction::Jgz(src_r, src_s) => if src_r.get_val(&registers) > 0 {
                let t_ptr = prog_pointer as Val + src_s.get_val(&registers);
                if t_ptr < 0 || t_ptr >= assembly.len() as Val {
                    break 0
                }
                prog_pointer = t_ptr as usize
            } else {
                prog_pointer += 1
            },
            Instruction::Mod(reg, src) => {
                let sv = src.get_val(&registers);
                if sv != 0 {
                    let rv = *registers.get(&reg).unwrap();
                    *registers.get_mut(&reg).unwrap() = rv % sv
                }
            },
            Instruction::Mul(reg, src) => *registers.get_mut(&reg).unwrap() *= src.get_val(&registers),
            Instruction::Rcv(reg) => {
                if let Ok(val) = rcvv.recv_timeout(dur) {
                    *registers.get_mut(&reg).unwrap() = val;
                } else {
                    let s_state = last_state(&rcvs);
                    if s_state == ThreadState::Recv {
                        break count_sends
                    }
                }
            },
            Instruction::Set(reg, src) => *registers.get_mut(&reg).unwrap() = src.get_val(&registers),
            Instruction::Snd(reg) => {
                sndv.send(registers[&reg]).unwrap();
                count_sends += 1
            },
        }

        if !matches!(instr, Instruction::Jgz(_, _)) {
            prog_pointer += 1;
        }
    }
}

fn task_a(lines: Vec<String>) {
    let (assembly, registers) = parse_input(&lines);
    let last_sound = run_prog_to_first_rcv(assembly, registers);

    println!("Recovered frequency = {last_sound}");
}

fn task_b(lines: Vec<String>) {
    let (assembly_0, registers_0) = parse_input(&lines);
    let assembly_1 = assembly_0.clone();
    let registers_1: Registers = registers_0
        .keys()
        .map(|r| (*r, 1))
        .collect();

    let (instr_0_send, instr_0_recv) = channel::<Val>();
    let (instr_1_send, instr_1_recv) = channel::<Val>();
    let (state_0_send, state_0_recv) = channel::<ThreadState>();
    let (state_1_send, state_1_recv) = channel::<ThreadState>();

    let builder = thread::Builder::new()
        .name("ettan".into());

    let handle_1 = builder.spawn(move || {
        run_prog_to_both_rcv(assembly_1, registers_0, instr_1_send, instr_0_recv, state_1_send, state_0_recv);
    }).unwrap();

    let one_send_count = run_prog_to_both_rcv(assembly_0, registers_1, instr_0_send, instr_1_recv, state_0_send, state_1_recv);
    // wait for the thread to join so we ensure the sender is dropped
    handle_1.join().unwrap();

    println!("Program 1 sends {one_send_count} values");

}

fn parse_input(lines: &[String]) -> (Assembly, Registers) {
    let mut registers = Registers::new();
    let assembly = lines
        .iter()
        .map(|l| {
            let instr_parts: Vec<&str> = l.split(' ').collect();
            let r = Instruction::get_reg_name(instr_parts[1]);
            if (r as u8) >= 97 && !registers.contains_key(&r) {
                registers.insert(r, 0);
            }
            Instruction::new(instr_parts)
        })
        .collect();

    (assembly, registers)
}

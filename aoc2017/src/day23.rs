use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Reg = char;
type Val = i64;
type Assembly = Vec<Instruction>;
type Registers = HashMap<Reg, Val>;

#[derive(Clone, Copy, Debug)]
enum Source {
    Reg(Reg),
    Val(Val),
    None,
}

impl Source {
    fn get_source(src: &str, rf: fn(&str) -> Reg, arg: &str) -> Self {
        if let Ok(s) = src.parse::<Val>() {
            Source::Val(s)
        } else {
            Source::Reg(rf(arg))
        }
    }

    fn get_val(&self, regs: &Registers) -> Val {
        match *self {
            Self::Reg(reg) => regs[&reg],
            Self::Val(val) => val,
            Self::None => panic!("Source is None!")
        }
    }
}
#[derive(Clone, Copy, Debug)]
enum Instruction {
    Jnz(Source, Source),
    Mul(Reg, Source),
    Set(Reg, Source),
    Sub(Reg, Source),
}
/*
set X Y sets register X to the value of Y.
sub X Y decreases register X by the value of Y.
mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
jnz X Y jumps with an offset of the value of Y, but only if the value of X is not zero. (An offset of 2
        skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
*/
impl Instruction {
    fn new(instr_parts: Vec<&str>) -> Self {
        let src = if instr_parts.len() == 3 {
            Source::get_source(instr_parts[2], Self::get_reg_name, instr_parts[2])
        } else {
            Source::None
        };
        let r = Self::get_reg_name(instr_parts[1]);

        match instr_parts[0] {
            "set" => Self::Set(r, src),
            "sub" => Self::Sub(r, src),
            "jnz" => {
                let r = Source::get_source(instr_parts[1], Self::get_reg_name, instr_parts[1]);
                Self::Jnz(r, src)
            },
            "mul" => Self::Mul(r, src),
            _ => panic!("Invalid instr")
        }
    }

    fn get_reg_name(instr: &str) -> Reg {
        instr.chars().next().unwrap()
    }
}

fn run_prog(assembly: Assembly, regs: Registers, ret_n_mul: bool) -> Val {
    let mut prog_pointer = 0;
    let mut registers = regs;
    let mut n_mul = 0;

    loop {
        let instr = assembly[prog_pointer];
        match instr {
            Instruction::Jnz(src_r, src_s) =>
                if src_r.get_val(&registers) != 0 {
                    let t_ptr = prog_pointer as Val + src_s.get_val(&registers);
                    if t_ptr < 0 || t_ptr >= assembly.len() as Val {
                        break
                    }
                    // Remove 1 from prog_pointer since it will be increased after the match
                    prog_pointer = (t_ptr - 1) as usize;
                },
            Instruction::Mul(reg, src) => {
                n_mul += 1;
                *registers.get_mut(&reg).unwrap() *= src.get_val(&registers)
            },
            Instruction::Set(reg, src) => *registers.get_mut(&reg).unwrap() = src.get_val(&registers),
            Instruction::Sub(reg, src) => *registers.get_mut(&reg).unwrap() -= src.get_val(&registers),
        }

        prog_pointer += 1;
    }

    match ret_n_mul {
        true  => n_mul,
        false => *registers.get(&'h').unwrap()
    }
}

fn task_a(lines: Vec<String>) {
    let (assembly, registers) = parse_input(&lines);

    let n_mul = run_prog(assembly, registers, true);

    println!("The mul instruction is invoked {n_mul} times");
}

fn task_b(_lines: Vec<String>) {
    // let (assembly, mut registers) = parse_input(&lines);

    // The below program will "never" be ready
    // *registers.get_mut(&'a').unwrap() = 1;
    // let h_val = run_prog(assembly, registers, false);
    // println!("h = {h_val}");

    // Analyzing the assembly show the following
        // The main loops
        // The outer loop goes from b = 108400 to 125400
        // The inner loops tries any possible combination of multipying the values in the range 2..=108400
        // For h to be incremented, the above multiplication has to be equal to b at some of the combinations
        // Since all possible combinations are tried that will happen for all values of b except when b is a prime
        // That means that it is enough to count the number of primes and subtract that from number values for b

    let b: u32 = 108400;
    let c = 125400;

    let mut count = 0;
    for bi in (b ..= c).step_by(17) {
        if common::is_prime(bi) {
            count += 1;
        }
    }

    // There are 1001 possible values of b, since the lower and upper bounds are inclusive
    println!("h = {}", 1001 - count);

    // The below loop is not slow, but it's 2000 times slower than the above solution
    // loop {
    //     for d in 2 ..= b / 2 {
    //         if b % d != 0 {
    //             continue;
    //         }

    //         count += 1;
    //         break;
    //     }

    //     if b == c {
    //         break
    //     }
    //     b += 17;
    // }
    // println!("h = {}", count);

}

fn parse_input(lines: &[String]) -> (Assembly, Registers) {
    let mut registers = Registers::new();
    let assembly = lines
        .iter()
        .map(|l| {
            let instr_parts: Vec<&str> = l.split(' ').collect();
            let r = Instruction::get_reg_name(instr_parts[1]);
            if (r as u8) >= 97 && !registers.contains_key(&r) {
                registers.insert(r, 0);
            }
            Instruction::new(instr_parts)
        })
        .collect();

    (assembly, registers)
}

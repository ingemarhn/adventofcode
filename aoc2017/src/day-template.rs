#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let _ = parse_input(&lines);
}

fn task_b(lines: Vec<String>) {
    let _ = parse_input(&lines);
}

fn parse_input(lines: &[String]) -> usize {
    // lines
    //     .iter()
    //     .map(|line|
    //         line
    //         .split(", ")
    //         .map(|p_elem|
    //             p_elem
    //             .trim_start_matches(|pc| ['a','p','v','=','<'].contains(&pc))
    //             .trim_end_matches('>')
    //         )
    //         .collect::<Vec<_>>()
    //     )
    //     .map(|coo|
    //         Particle::new(coo[0], coo[1], coo[2])
    //     )
    //     .enumerate()
    //     .collect::<HashMap<_, _>>()

    // lines[0]
    //     .split(',') | .chars()
    //     .map(|l| l.parse().unwrap())
    //     .collect()

    // lines[0].chars()

    // let path_chars: Vec<Vec<char>> = lines
    // -or: let path_chars: Vec<Vec<u8>> = lines
    //     .iter()
    //     .map(|l| l
    //         .chars()
    // -possibly: .map(|c| c as u8)
    //         .collect()
    //     )
    //     .collect();

    // lines.iter().fold(Vec::new(), |mut splits, line| {
    //     splits.push(line.split('\t'));
    //     splits
    // })

    // for line in lines {
    // }

    0
}

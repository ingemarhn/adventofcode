#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Position = (usize, usize);
type Route = Vec<Vec<PathPoint>>;

#[derive(Clone, Debug, PartialEq)]
enum PathPoint {
    Vertical(Position, Option<char>),
    Horizontal(Position, Option<char>),
    Crossing(Position),
    Turn(Position),
    End(Position, Option<char>),
    Wilderness,
}

impl PathPoint {
    fn unwrap(&self) -> (Position, Option<char>) {
        match self {
            Self::Vertical(pos, o_ch) => (*pos, *o_ch),
            Self::Horizontal(pos, o_ch) => (*pos, *o_ch),
            Self::Crossing(pos) => (*pos, None),
            Self::Turn(pos) => (*pos, None),
            Self::End(pos, o_ch) => (*pos, *o_ch),
            Self::Wilderness => panic!("Cannot unwrap Wilderness"),
        }
    }
}

#[derive(Debug, PartialEq)]
enum Direction {
    Down,
    Up,
    Left,
    Right,
}
use Direction::*;

// fn to check if two enums have equal variant
fn variant_eq<T>(a: &T, b: &T) -> bool {
    std::mem::discriminant(a) == std::mem::discriminant(b)
}

/*
    - Follow direction one step
        - Crossing => continue
        - Same pathpoint kind =>
            - any letter? => pick it up
            - continue
        - Turn =>
            - Check directions that crosses current direction
            - Turn
        - End => return found characters
    - Recursive call
*/

fn follow_path(current_position: Position, route: &Route, current_direction: Direction, found_chars: Vec<char>, n_steps: usize) -> (Vec<char>, usize) {
    let (current_row, current_col) = current_position;
    let (next_row, next_col) = match current_direction {
        Down  => (current_row + 1, current_col),
        Up    => (current_row - 1, current_col),
        Left  => (current_row, current_col - 1),
        Right => (current_row, current_col + 1),
    };
    let next_path_point = &route[next_row][next_col];

    let extract_char = |o_ch, chars| {
        if let Some(ch) = o_ch {
            let mut n_chrs: Vec<char> = chars;
            n_chrs.push(ch);
            n_chrs
        } else {
            chars
        }
    };

    let (next_direction, this_found_chars) =
        if variant_eq(next_path_point, &route[current_row][current_col]) ||
           variant_eq(&route[current_row][current_col], &PathPoint::Crossing((0, 0))) ||
           variant_eq(&route[current_row][current_col], &PathPoint::Turn((0, 0))) {
            let o_ch =
                match next_path_point {
                    PathPoint::Horizontal(_, o_ch) |
                    PathPoint::Vertical(_, o_ch) => *o_ch,
                    _ => None
                };

            let n_chars = extract_char(o_ch, found_chars);

            (current_direction, n_chars)
        } else if let PathPoint::Crossing(_) = next_path_point {
            (current_direction, found_chars)
        } else if let PathPoint::Turn(_) = next_path_point {
            let next_dir =
                if [Down, Up].contains(&current_direction) {
                    if route[next_row][next_col - 1] != PathPoint::Wilderness {
                        Left
                    } else {
                        Right
                    }
                } else if route[next_row - 1][next_col] != PathPoint::Wilderness {
                    Up
                } else {
                    Down
                };

            (next_dir, found_chars)
        } else {
            // It shouldn't be possible to get out in the wilderness, hence we've come to the end
            if let PathPoint::End(_, o_ch) = next_path_point {
                let n_chars = extract_char(*o_ch, found_chars);

                return (n_chars, n_steps + 1)
            } else {
                panic!("Not an End PathPoint: {:?}", next_path_point)
            }
        };

    follow_path((next_row, next_col), route, next_direction, this_found_chars, n_steps + 1)
}

fn task_a(lines: Vec<String>) {
    let route = parse_input(&lines);

    let (current_position, _) = route[0].clone().into_iter().find(|pp| *pp != PathPoint::Wilderness).unwrap().unwrap();

    let (found_chars, _) = follow_path(current_position, &route, Down, Vec::new(), 1);

    println!("Found characters = {}", found_chars.into_iter().fold(String::new(), |mut str, c| {str.push(c); str}));
}

fn task_b(lines: Vec<String>) {
    let route = parse_input(&lines);
    let (current_position, _) = route[0].clone().into_iter().find(|pp| *pp != PathPoint::Wilderness).unwrap().unwrap();

    let (_, n_steps) = follow_path(current_position, &route, Down, Vec::new(), 1);

    println!("Number of steps = {n_steps}");
}

/*
    - Split each line in into chars
        - Create a local char_map: Vec<Vec<char>>
    - Map every non-blank position in char_map into the return matrix
        - '-' if pos-above is blank => Horizontal
        - '-' if pos-above is '|'   => Crossing
        - '|' if pos-left  is blank => Vertical
        - '|' if pos-left  is '-'   => Crossing
        - '+'                       => Turn
        - Ch  if pos-left|right  is not blank => Horizontal
        - Ch  if pos-above|below is not blank => Vertical
        - Ch  if only one connection          => End
*/

fn parse_input(lines: &[String]) -> Route {
    let path_chars: Vec<Vec<char>> = lines
        .iter()
        .map(|l| l
            .chars()
            .collect()
        )
        .collect();

    path_chars
        .iter()
        .enumerate()
        .map(|(row_num, chars)| chars
            .iter()
            .enumerate()
            .map(|(col_num, &c)| match c {
                '-' if path_chars[row_num - 1][col_num] == ' ' => PathPoint::Horizontal((row_num, col_num), None),
                '-' if path_chars[row_num - 1][col_num] == '|'
                ||      path_chars[row_num + 1][col_num] == '|' => PathPoint::Crossing((row_num, col_num)),
                '|' if path_chars[row_num][col_num - 1] == ' ' => PathPoint::Vertical((row_num, col_num), None),
                '|' if path_chars[row_num][col_num - 1] == '-'
                ||      path_chars[row_num][col_num + 1] == '-' => PathPoint::Crossing((row_num, col_num)),
                '+'                                            => PathPoint::Turn((row_num, col_num)),
                ' '                                            => PathPoint::Wilderness,
                ch                                       => {
                    if path_chars[row_num][col_num - 1] != ' ' && path_chars[row_num][col_num + 1] != ' ' {
                        PathPoint::Horizontal((row_num, col_num), Some(ch))
                    } else if path_chars[row_num - 1][col_num] != ' ' && path_chars[row_num + 1][col_num] != ' ' {
                        PathPoint::Vertical((row_num, col_num), Some(ch))
                    } else {
                        PathPoint::End((row_num, col_num), Some(ch))
                    }
                }
            })
            .collect()
        )
        .collect()
}

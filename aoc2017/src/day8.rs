use std::collections::HashMap;
use std::cmp::PartialEq;
use std::cmp::PartialOrd;
use std::ops::{Add, Sub};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Register = String;
type Registers = HashMap<Register, i32>;
type ModVal = fn(i32, i32) -> i32;
type Comp = for<'r, 's> fn(&'r i32, &'s i32) -> bool;
type Instructions = Vec<FullInstruction>;

struct Condition {
    reg: Register,
    val: i32,
    oper: Comp,
}

impl Condition {
    fn new(reg: &str, val: &str, oper: &str) -> Self {
        let v = val.parse().unwrap();
        let op = match oper {
            "==" => PartialEq::eq,
            "!=" => PartialEq::ne,
            "<"  => PartialOrd::lt,
            ">"  => PartialOrd::gt,
            "<=" => PartialOrd::le,
            ">=" => PartialOrd::ge,
            _    => panic!("Invalid operator: {oper}")
        };
        Self {reg: reg.to_string(), val: v, oper: op}
    }

    fn compare(&self, regs: &Registers) -> bool {
        let cmp: Comp = self.oper;
        cmp(&regs[&self.reg], &self.val)
    }
}

struct Instruction {
    reg: Register,
    val: i32,
    oper: ModVal,
}

impl Instruction {
    fn new(reg: &str, val: &str, oper: &str) -> Self {
        let op = match oper {
            "inc" => Add::add,
            "dec" => Sub::sub,
            _     => panic!("Invalid operator: {oper}")
        };
        Self { reg: reg.to_string(), val: val.parse().unwrap(), oper: op }
    }

    fn run(&self, registers: &Registers) -> i32 {
        // g dec 722
        let chg = self.oper;
        chg(registers[&self.reg], self.val)
    }
}

struct FullInstruction {
    cond: Condition,
    inst: Instruction,
}

impl FullInstruction {
    fn new(cond: Condition, inst: Instruction) -> Self {
        Self { cond, inst }
    }
}

fn task_a(lines: Vec<String>) {
    let (instructions, mut registers) = parse_input(&lines);

    for inst in instructions {
        if inst.cond.compare(&registers) {
            let r = inst.inst.run(&registers);
            if let Some(v) = registers.get_mut(&inst.inst.reg) {
                *v = r;
            }
        }
    }

    let r = registers.iter().fold(0, |accum, item| {
        if accum >= *item.1 { accum } else { *item.1 }
    });

    println!("Largest register value = {r}");
}

fn task_b(lines: Vec<String>) {
    let (instructions, mut registers) = parse_input(&lines);
    let mut largest = 0;

    for inst in instructions {
        if inst.cond.compare(&registers) {
            let r = inst.inst.run(&registers);
            if let Some(v) = registers.get_mut(&inst.inst.reg) {
                *v = r;
            }

            let h = registers.iter().fold(0, |accum, item| {
                if accum >= *item.1 { accum } else { *item.1 }
            });

            if h > largest {
                largest = h;
            }
        }
    }

    println!("Largest register value ever = {largest}");
}

fn parse_input(lines: &[String]) -> (Instructions, Registers) {
    use regex::Regex;

    let line_reg: Regex = Regex::new(r"(?P<reg>\w+)\s(?P<chgop>\w+)\s(?P<rval>-?\d+)\sif\s(?P<creg>\w+)\s(?P<cmpop>..?)\s(?P<cval>-?\d+)").unwrap();
    let mut instrs = Vec::new();
    let mut regs = HashMap::new();

    for line in lines {
        let captures = line_reg.captures(line).unwrap();
        let reg   = captures.name("reg")  .unwrap().as_str();
        let chgop = captures.name("chgop").unwrap().as_str();
        let rval  = captures.name("rval") .unwrap().as_str();
        let creg  = captures.name("creg") .unwrap().as_str();
        let cmpop = captures.name("cmpop").unwrap().as_str();
        let cval  = captures.name("cval") .unwrap().as_str();

        let cond  = Condition::new(creg, cval, cmpop);
        let inst  = Instruction::new(reg, rval, chgop);
        let finst = FullInstruction::new(cond, inst);

        instrs.push(finst);
        regs.entry(reg .to_string()).or_insert(0);
        regs.entry(creg.to_string()).or_insert(0);
    }

    (instrs, regs)
}

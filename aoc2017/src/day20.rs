use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Coordinate = i64;
type Coordinates = [Coordinate; 3];
type Particles = HashMap<usize, Particle>;
// type ParticleGroups = HashMap<String, Particles>;

#[derive(Clone, Copy, Debug)]
struct Particle {
    position:     Coordinates,
    velocity:     Coordinates,
    acceleration: Coordinates,
}

impl Particle {
    fn new(pos_str: &str, vel_str: &str, acc_str: &str, ) -> Self {

        Self { position: Self::get_coordinates(pos_str), acceleration: Self::get_coordinates(acc_str), velocity: Self::get_coordinates(vel_str) }
    }

    fn get_coordinates(s: &str) -> Coordinates {
        let mut coordinates = s
            .split(',')
            .map(|ss| ss.parse().unwrap());

        [
            coordinates.next().unwrap(),
            coordinates.next().unwrap(),
            coordinates.next().unwrap(),
        ]
    }
}

fn calc_closest_to_origo(particles: &Particles) -> usize {
    let (index, _) =
        particles
        .iter()
        // .enumerate()
        .fold((0, Coordinate::pow(2, 32)), |(ind, acc_sum), ptl| {
            let (n_ind, pt) = ptl;
            let n_sum = pt.acceleration[0].abs() + pt.acceleration[1].abs() + pt.acceleration[2].abs();
            if n_sum < acc_sum {
                (*n_ind, n_sum)
            } else {
                (ind, acc_sum)
            }
        });

    index
}

fn task_a(lines: Vec<String>) {
    let particles = parse_input(&lines);

    let closest = calc_closest_to_origo(&particles);

    println!("Particle closest to origo = {closest}");
}

/*
    +++
    ++-
    +-+
    -++
    +--
    -+-
    --+
    ---

    - divide particles into one of above groups
        - store particle in a hashmapped vector with above sign combinations as key
    - find the one that will be closest to origo in the long run
        - walk through each group
            - find closest to origo
            - store in hashmap
    - start loop with movings
        - look for collisions
            - create a hashmap with position as key with vectors of particles
            - when all particles are mapped, look for vectors with more than one member (got to wait destroying because there may be more than two)
            - destroy colliding particles
                - remove from all particles vec and the groups vector
            - any of the destroyed that was supposed to closest to origo?
                - find new closest particle
        - break loop when all "closest" are closest to origo.
    - report # of particles left
*/

/*
fn divide_particles(prtcls: &Particles) -> ParticleGroups {
    let mut prtc_grps =
        ["+++", "++-", "+-+", "-++", "+--", "-+-", "--+", "---",]
        .iter()
        .fold(HashMap::new(), |mut map, kind| {
            map.insert(kind.to_string(), Particles::new());
            map
        });

    for (ind, prtc) in prtcls {
        let grp_key =
            "..."
            .chars()
            .enumerate()
            .fold(String::new(), |mut s, (i, c)| {
                let nc = if prtc.acceleration[i] < 0 {
                    '-'
                } else {
                    '+'
                };
                s.push(nc);

                s
            });

        let mut v = prtc_grps.get_mut(&grp_key).unwrap();
        v.insert(*ind,*prtc);
    }

    prtc_grps
}

fn grp_calc_closest_to_origo(particle_groups: &ParticleGroups) -> Vec<(String, usize)> {
    particle_groups
    .iter()
    .map(|(grp_key, partls)| {
        (grp_key.clone(), calc_closest_to_origo(partls))
    })
    .collect()
}
*/
/*
    - start loop with movings
        - look for collisions
            - create a hashmap with position as key with vectors of particles
            - when all particles are mapped, look for vectors with more than one member (got to wait destroying because there may be more than two)
            - destroy colliding particles
                - remove from all particles vec and the groups vector
            - any of the destroyed that was supposed to closest to origo?
                - find new closest particle
        - break loop when all "closest" are closest to origo.
*/

fn take_a_step(particles: &Particles) -> Particles {
    particles
        .iter()
        .map(|(ind, prtcl)| {
            let mut n_prtcl = *prtcl;
            for i in 0..3 {
                n_prtcl.velocity[i] += n_prtcl.acceleration[i];
                n_prtcl.position[i] += n_prtcl.velocity[i];
            }
            (*ind, n_prtcl)
        })
        .collect()
}

fn handle_collisions(particles: &Particles) -> (Particles, usize) {
    let mut collides = HashMap::new();
    for (i, p) in particles {
        collides
            .entry(p.position)
            .and_modify(|e: &mut Vec<usize>| { e.push(*i);  })
            .or_insert(vec![*i]);
    }

    let mut r_particles = particles.clone();
    for (_, ci) in collides {
        if ci.len() == 1 {
            continue;
        }

        for i in ci {
            r_particles.remove(&i).unwrap();
        }
    }

    let diff = particles.len() - r_particles.len();
    (r_particles, diff)
}

fn task_b(lines: Vec<String>) {
    let mut particles = parse_input(&lines);

    // let prtcl_grps = divide_particles(&particles);
    // let group_closest = grp_calc_closest_to_origo(&prtcl_grps);
    let mut n_none_removed = 0;
    loop {
        particles = take_a_step(&particles);
        let n_removed;
        (particles, n_removed) = handle_collisions(&particles);
        n_none_removed = if n_removed == 0 {
            n_none_removed + 1
        } else {
            0
        };
        if n_none_removed > 10 {
            break
        }
    }

    println!("# of particles left = {}", particles.len());
}

/*
    p=<-1978,3315,-371>, v=<32,0,17>, a=<4,-11,0>
    - parse Position, Velocity and Acceleration into a struct
*/
fn parse_input(lines: &[String]) -> Particles {
    lines
        .iter()
        .map(|line|
            line
            .split(", ")
            .map(|p_elem|
                p_elem
                .trim_start_matches(|pc| ['a','p','v','=','<'].contains(&pc))
                .trim_end_matches('>')
            )
            .collect::<Vec<_>>()
        )
        .map(|coo|
            Particle::new(coo[0], coo[1], coo[2])
        )
        .enumerate()
        .collect::<HashMap<_, _>>()
}

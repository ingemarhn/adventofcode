#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
use std::collections::HashMap;

type Value = u64;
static A_FACTOR: Value = 16807;
static B_FACTOR: Value = 48271;
static DIV: Value = 0x7fffffff;
static MASK: Value = 0xffff;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let gens_start = parse_input(&lines);

    let (count, _, _) = (0 .. 40000000).fold((0, gens_start["A"], gens_start["B"]), |(cnt, prev_a, prev_b), _| {
        let new_a = prev_a * A_FACTOR % DIV;
        let new_b = prev_b * B_FACTOR % DIV;
        let new_cnt = if (new_a & MASK) == (new_b & MASK) {
            cnt + 1
        } else {
            cnt
        };

        (new_cnt, new_a, new_b)
    });

    println!("The judge's final count = {count}");
}

fn task_b(lines: Vec<String>) {
    let gens_start = parse_input(&lines);

    let meet_criteria = |prev_value_in, factor, multiple| -> Value {
        let mut prev_value = prev_value_in;
        loop {
            let new_value = prev_value * factor % DIV;
            if new_value % multiple == 0 {
                break new_value
            }
            prev_value = new_value;
        }
    };

    let (count, _, _) = (0 .. 5000000).fold((0, gens_start["A"], gens_start["B"]), |(cnt, prev_a, prev_b), _| {
        let new_a = meet_criteria(prev_a, A_FACTOR, 4);
        let new_b = meet_criteria(prev_b, B_FACTOR, 8);
        let new_cnt = if (new_a & MASK) == (new_b & MASK) {
            cnt + 1
        } else {
            cnt
        };

        (new_cnt, new_a, new_b)
    });

    println!("The judge's final count = {count}");
}

fn parse_input(lines: &[String]) -> HashMap<&str, Value> {
    lines.iter().fold(HashMap::new(), |mut gener, line| {
        let mut l_parts = line.split_ascii_whitespace();
        l_parts.next();
        let ind = l_parts.next().unwrap();
        l_parts.next();
        l_parts.next();
        let val = l_parts.next().unwrap().parse().unwrap();
        gener.insert(ind, val);
        gener
    })
}

#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Stream = Vec<char>;
type Things = Vec<Thing>;

#[derive(Clone, Debug)]
struct Group {
    level: i8,
    elements: Things,
}

#[derive(Clone, Debug)]
enum Thing {
    AChar(char),
    AGroup(Option<Group>),
}

fn count_score(stream: Stream) -> usize {
    let (score, _) = stream.iter().fold((0, 0), |(mut scr, mut lev), &c| {
        match c {
            '{' => {
                lev += 1;
                scr += lev;
            },
            '}' => lev -= 1,
            _ => {},
        }

        (scr, lev)
    });

    score
}

fn task_a(lines: Vec<String>) {
    let (stream, _) = parse_input(&lines);
    let score = count_score(stream);

    println!("Score = {score}");
}
fn task_b(lines: Vec<String>) {
    let (_, count) = parse_input(&lines);

    println!("# of non-cancelled chars = {count}");
}

fn parse_input(lines: &[String]) -> (Stream, u32) {
    let (_, _, mut chrs, count) = lines[0].chars().fold((false, false, Vec::new(), 0), |(mut is_garbage, mut ignore_next_c, mut chrs, mut cnt), c| {
        if ignore_next_c {
            ignore_next_c = false;
        } else if !is_garbage {
            match c {
                '<' => is_garbage = true,
                _   => chrs.push(c),
            }
        } else {
            match c {
                '>' => is_garbage = false,
                '!' => ignore_next_c = true,
                _   =>  cnt += 1,
            }
        }

        (is_garbage, ignore_next_c, chrs, cnt)
    });

    (chrs, count)
}

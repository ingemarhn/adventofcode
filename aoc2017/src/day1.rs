pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut l_iter = parse_input(&lines);
    let first = l_iter.next().unwrap();
    let (sum, last) = l_iter.fold((0, first), |(mut sum, prev), curr_dig| {
        if curr_dig == prev {
            sum += curr_dig.to_digit(10).unwrap();
        }
        (sum, curr_dig)
    });

    let sum = if last == first {
        sum + last.to_digit(10).unwrap()
    } else {
        sum
    };

    println!("Sum is: {}", sum);
}

fn task_b(lines: Vec<String>) {
    let digits: Vec<u32> = parse_input(&lines).fold(Vec::new(), |mut digs, dig| {
        digs.push(dig.to_digit(10).unwrap());
        digs
    });

    let len_full = digits.len();
    let len_half = len_full / 2;

    let mut sum = 0;
    for i in 0..len_full {
        let ic =
            if i < len_half {
                i + len_half
            } else {
                i - len_half
            };

        if digits[i] == digits[ic] {
            sum += digits[i];
        }
    }

    println!("Sum is: {}", sum);
}

fn parse_input(lines: &[String]) -> std::str::Chars {
    lines[0].chars()
}

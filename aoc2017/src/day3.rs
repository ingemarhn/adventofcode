use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Copy, Clone, Debug)]
struct Frame {
    len: usize,
    max_val: usize,
}

impl Frame {
    fn new(len: usize, max_val: usize) -> Self {
        Self { len, max_val }
    }

    fn add_len(&mut self, add_v: usize) {
        self.len += add_v;
    }

    fn add_max(&mut self, add_v: usize) {
        self.max_val += add_v;
    }
}

fn get_frames(max_val: usize) -> Vec<Frame> {
    let mut res: Vec<Frame> = vec![Frame::new(1, 1), Frame::new(8, 9)];
    let mut largest = res.last().unwrap().len;
    while largest < max_val-1 {
        let mut frame = *res.last().unwrap();
        frame.add_len(8);
        frame.add_max(frame.len);
        res.push(frame);
        largest += frame.len;
    }

    res
}

fn get_n_steps(frames: Vec<Frame>, max_val: usize) -> usize {
    let f_len = frames.len();
    let sec_last_frame = frames[f_len - 2];
    let last_frame = frames[f_len - 1];

    let diff = max_val - sec_last_frame.max_val;
    let frames_from_1 = f_len - 1;
    let side_len = last_frame.len / 4;
    let last_side_pos = diff % side_len;
    let steps_from_1 = last_side_pos - side_len / 2;

    frames_from_1 + steps_from_1
}

fn task_a(lines: Vec<String>) {
    let sqr_num = parse_input(&lines);

    let frames = get_frames(sqr_num);
    let steps = get_n_steps(frames, sqr_num);

    println!("# steps = {}", steps);
}

fn task_b(lines: Vec<String>) {
    let sqr_num = parse_input(&lines);

    let frames = get_frames(sqr_num);
    let mut spiral = HashMap::<(i16, i16), usize>::new();
    let mut position = (0, 0);
    spiral.insert(position, 1);
    let mut current_frame = 0;

    let l_val = 'o_loop: loop {
        current_frame += 1;
        let side_len = frames[current_frame].len / 4;

        position = (position.0 + 1, position.1 - 1);
        for (xc, yc) in [(0, 1), (-1, 0), (0, -1), (1, 0)] {
            for _ in 1..=side_len {
                position = (position.0 + xc, position.1 + yc);
                let this_val = [(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1), (1, -1)].iter().fold(0, |current_val, (xpc, ypc)| {
                    let val_pos = (position.0 + xpc, position.1 + ypc);
                    if spiral.contains_key(&val_pos) {
                        current_val + spiral[&val_pos]
                    } else {
                        current_val
                    }
                });

                spiral.insert(position, this_val);
                if this_val > sqr_num {
                    break 'o_loop this_val
                }
            }
        }
    };

    println!("Larger value = {}", l_val);
}

fn parse_input(lines: &[String]) -> usize {
    lines[0].parse().unwrap()
}

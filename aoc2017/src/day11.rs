pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Directions = Vec<(i16, i16)>;

fn task_a(lines: Vec<String>) {
    let directions = parse_input(&lines);

    let (ens, eew) = directions.into_iter().fold((0, 0), |(ns, ew), (dns, dew)| {
        (ns + dns, ew + dew)
    });
    let (ens, eew) = (ens.abs(), eew.abs());

    let nsteps = if eew >= ens {
        eew
    } else {
        (ens - eew) / 2 + eew
    };

    println!("# of steps = {nsteps}");
}

fn task_b(lines: Vec<String>) {
    let directions = parse_input(&lines);

    let (_, _, nsteps) = directions.into_iter().fold((0, 0, 0), |(ns, ew, steps), (dns, dew)| {
        let (rns, rew) = (ns + dns, ew + dew);
        let (ens, eew) = (rns.abs(), rew.abs());
        let tsteps = if eew >= ens {
            eew
        } else {
            (ens - eew) / 2 + eew
        };

        (rns, rew, steps.max(tsteps))
    });

    println!("# of steps = {nsteps}");
}

fn parse_input(lines: &[String]) -> Directions {
    lines[0]
        .split(',')
        .map(|dir| match dir {
            "n"  => (2, 0),
            "s"  => (-2, 0),
            "nw" => (1, -1),
            "ne" => (1, 1),
            "se" => (-1, 1),
            "sw" => (-1, -1),
            _    => panic!("Oh shiiit!")
        })
        .collect()
}

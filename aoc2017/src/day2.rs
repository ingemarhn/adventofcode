pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let numb_list = parse_input(&lines);

    let sum = numb_list.iter().fold(0, |sum, nl| {
        let (small, large) = nl.clone().fold((999999999, 0), |(small, large), el| {
            let num: u32 = el.parse().unwrap();
            if num > large {
                (small, num)
            } else if num < small {
                (num, large)
            } else {
                (small, large)
            }
        });

        sum + large - small
    });

    println!("Sum is: {}", sum);
}

fn task_b(lines: Vec<String>) {
    let numb_list = parse_input(&lines);
    let mut sum = 0;
    for nl in numb_list {
        let nv: Vec<u32> = nl
                           .map(|x| x.parse::<u32>().unwrap() * 2)
                           .collect();
        'out:
        for i in 0 .. nv.len()-1 {
            for j in i+1 .. nv.len() {
                let mut num: u32 = nv[i] / nv[j];
                if num * nv[j] == nv[i] {
                    sum += num;
                    break 'out;
                }
                num = nv[j] / nv[i];
                if num * nv[i] == nv[j] {
                    sum += num;
                    break 'out;
                }
            }
        }
    }

    println!("Sum is: {}", sum);
}

fn parse_input(lines: &[String]) -> Vec<std::str::Split<char>> {
    lines.iter().fold(Vec::new(), |mut splits, line| {
        splits.push(line.split('\t'));
        splits
    })
}

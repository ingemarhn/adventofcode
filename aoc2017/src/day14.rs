use super::day10;
use std::collections::HashSet;
use std::collections::VecDeque;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn get_lengths(len_base: &day10::Lengths, r: usize) -> day10::Lengths {
    [
        len_base.clone(),
        format!("{r}")
            .as_bytes()
            .iter()
            .chain(&[17, 31, 73, 47, 23])
            .map(|&c| c as usize)
            .collect()
    ].concat()
}

fn get_matrix (lengths_base: &day10::Lengths) -> Vec<Vec<char>> {
    let mut matrix = Vec::new();

    for r in 0..=127 {
        let lengths = get_lengths(lengths_base, r);

        if let day10::KnotHash::Bytes(knot_hash) = day10::calc_knot_hash(lengths, false) {
            let bits = knot_hash
                .iter()
                .fold(Vec::new(), |mut v_strs: Vec<Vec<char>>, bv| {
                    let bit_str = format!("{:08b}", bv);
                    v_strs.push(bit_str.chars().collect());
                    v_strs
                })
                .concat();

            matrix.push(bits);
        }
    }

    matrix
}

fn task_a(lines: Vec<String>) {
    let lengths_base = [day10::parse_input_chars(&lines), vec!['-' as usize]].concat();
    let mut n_used = 0;

    for r in 0..=127 {
        let lengths = get_lengths(&lengths_base, r);

        if let day10::KnotHash::Bytes(knot_hash) = day10::calc_knot_hash(lengths, false) {
            n_used += knot_hash
                .iter()
                .fold(0, |mut n_use, &c| {
                    let mut bv = c;
                    while bv != 0 {
                        if bv & 1 == 1 {
                            n_use += 1;
                        }
                        bv >>= 1;
                    }
                    n_use
                });
        }
    }

    println!("Used squares = {n_used}");
}

fn task_b(lines: Vec<String>) {
    let lengths_base = [day10::parse_input_chars(&lines), vec!['-' as usize]].concat();
    let matrix = get_matrix (&lengths_base);

    let mut not_taken = HashSet::new();
    for i in 0..128 {
        for j in 0..128 {
            not_taken.insert((i, j));
        }
    }
    let mut groups = Vec::new();
    while let Some((xp, yp)) = not_taken.iter().next() {
        let (x, y) = (*xp, *yp);

        let group = locate_group((x, y), &matrix);
        not_taken.remove(&(x, y));
        if !group.is_empty() {
            for pos in &group {
                not_taken.remove(pos);
            }
            groups.push(group);
        }
    }

    println!("# of groups = {}", groups.len());
}

type Position = (usize, usize);
type Positions = Vec<Position>;
type Squares = HashSet<Position>;

fn get_neighbors(current_pos: Position) -> Positions {
    let mut neighbors = Vec::new();
    for (xd, yd) in [(-1i16, 0i16), (0, -1), (1, 0), (0, 1)] {
        let (nx, ny) = (current_pos.0 as i16 + xd, current_pos.1 as i16 + yd);
        if !(0..=127).contains(&nx) || !(0..=127).contains(&ny) { continue }
        neighbors.push((nx as usize, ny as usize));
    }

    neighbors
}

// BFS
fn locate_group(start_pos: Position, matrix: &[Vec<char>]) -> Squares {
    let mut visited = Squares::new();
    let mut group = Squares::new();
    let mut queue = VecDeque::new();
    if matrix[start_pos.1][start_pos.0] == '0' {
        // Only consider squares/groups containing '1'
        return group;
    }

    queue.push_back(start_pos);
    visited.insert(start_pos);

    while !queue.is_empty() {
        let current_pos = queue.pop_front().unwrap();
        if matrix[current_pos.1][current_pos.0] == '0' {
            // Not part of a group. Continue to next in the queue
            continue
        }
        group.insert(current_pos);

        for pos in get_neighbors(current_pos) {
            if !visited.contains(&pos) {
                queue.push_back(pos);
                visited.insert(pos);
            }
        }
    }

    group
}

use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type PipeNum = i16;
type Pipes = HashMap<PipeNum, Pipe>;
type Connections = Vec<PipeNum>;

#[derive(Clone, Debug)]
struct Pipe {
    connected_to: Connections,
    connects_to_root_pipenum: bool,
}

impl Pipe {
    fn new(s_connected_to: &str) -> Self {
        let connected_to = s_connected_to.split(", ").map(|p| p.parse().unwrap()).collect();

        Self {connected_to, connects_to_root_pipenum: false}
    }
}

fn connect_pipes(z_pipes: &Connections, pipes_src: &Pipes) -> Pipes {
    let mut pipes_source = pipes_src.clone();

    let mut p_conn = Vec::new();
    for &p_num in z_pipes {
        let p = pipes_source.get_mut(&p_num).unwrap();
        if p.connects_to_root_pipenum {
            continue
        }
        p.connects_to_root_pipenum = true;
        for &ct in &p.connected_to {
            p_conn.push(ct);
        }
    }

    if !p_conn.is_empty() {
        connect_pipes(&p_conn, &pipes_source)
    } else {
        pipes_source
    }
}

fn task_a(lines: Vec<String>) {
    let pipes = parse_input(&lines);

    let m_pipes = connect_pipes(&vec![0], &pipes);
    let c_len = m_pipes.into_iter().fold(0, |cnt, (_, p)| {
        if p.connects_to_root_pipenum {
            cnt + 1
        } else {
            cnt
        }
    });
    println!("# of programs connected to 0 = {}", c_len);
}

fn task_b(lines: Vec<String>) {
    let mut pipes = parse_input(&lines);
    let mut count = 0;
    while !pipes.is_empty() {
        let &p_num = pipes.keys().take(1).next().unwrap();
        pipes = connect_pipes(&vec![p_num], &pipes);
        pipes.retain(|_, p| !p.connects_to_root_pipenum);

        count += 1;
    }

    println!("# of groups = {}", count);
}

fn parse_input(lines: &[String]) -> Pipes {

    let mut pipes = Pipes::new();
    for line in lines {
        let mut s_pipes = line.split(" <-> ");
        let pipe_num = s_pipes.next().unwrap().parse().unwrap();
        pipes.insert(
            pipe_num,
            Pipe::new(s_pipes.next().unwrap())
        );
    }

    pipes
}

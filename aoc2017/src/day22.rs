use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type MapIndices = i32;
type Coordinate = (MapIndices, MapIndices);
type MoveDirection = Coordinate;
type Cells = HashMap<Coordinate, bool>;
type CellsB = HashMap<Coordinate, NodeState>;

#[derive(PartialEq)]
enum NodeState {
    Clean,
    Weakened,
    Infected,
    Flagged,
}
struct CellsMap {
    map: Cells,
    map_b: CellsB,
    curr_pos: Coordinate,
    move_directions: [MoveDirection; 4],
    curr_direction_ind: MapIndices,
}

impl CellsMap {
    fn new(curr_pos: Coordinate) -> Self {
        Self {
            map: Cells::new(),
            map_b: CellsB::new(),
            curr_pos,
            move_directions: [(-1,0), (0,1), (1,0), (0,-1)],
            curr_direction_ind: 0,
         }
    }
    fn add(&mut self, coord: Coordinate, ch_val: char) {
        self.map.entry(coord).or_insert(ch_val == '#');
        self.map_b.entry(coord).or_insert(
            match ch_val{
                '#' => NodeState::Infected,
                _   => NodeState::Clean,
        });
    }

    fn mov(&mut self) -> bool {
        // Decide direction to go
        let turn_val = match self.map[&self.curr_pos] {
            true  => 1,
            false => -1,
        };
        self.curr_direction_ind = (self.curr_direction_ind + turn_val + 4) % 4;

        // Infect or clear current position
        self.map.entry(self.curr_pos).and_modify(|e| *e = !*e);

        // Save result of this burst of activity as a return value
        let ret_stat = *self.map.get(&self.curr_pos).unwrap();

        // Move to next cell
        let move_adder = self.move_directions[self.curr_direction_ind as usize];
        self.curr_pos = (self.curr_pos.0 + move_adder.0, self.curr_pos.1 + move_adder.1);
        self.map.entry(self.curr_pos).or_insert(false);

        ret_stat
    }

    fn mov_b(&mut self) -> bool {
        // Decide direction to go
        let turn_val = match self.map_b[&self.curr_pos] {
            NodeState::Clean    => -1,
            NodeState::Weakened => 0,
            NodeState::Infected => 1,
            NodeState::Flagged  => 2,
        };
        self.curr_direction_ind = (self.curr_direction_ind + turn_val + 4) % 4;

        // set new state for current position
        self.map_b.entry(self.curr_pos).and_modify(|e|
            *e = match *e {
                NodeState::Clean    => NodeState::Weakened,
                NodeState::Weakened => NodeState::Infected,
                NodeState::Infected => NodeState::Flagged,
                NodeState::Flagged  => NodeState::Clean,
            }
        );

        // Save result of this burst of activity as a return value
        let ret_stat = *self.map_b.get(&self.curr_pos).unwrap() == NodeState::Infected;

        // Move to next cell
        let move_adder = self.move_directions[self.curr_direction_ind as usize];
        self.curr_pos = (self.curr_pos.0 + move_adder.0, self.curr_pos.1 + move_adder.1);
        self.map_b.entry(self.curr_pos).or_insert(NodeState::Clean);

        ret_stat
    }
}

fn task_a(lines: Vec<String>) {
    let mut map = parse_input(&lines);

    let count = (0..10000).fold(0, |cnt, _| {
        match map.mov() {
            true  => cnt + 1,
            false => cnt,
        }
    });

    println!("# of bursts that infected a node = {count}");
}

fn task_b(lines: Vec<String>) {
    let mut map = parse_input(&lines);

    let count = (0..10000000).fold(0, |cnt, _| {
        match map.mov_b() {
            true  => cnt + 1,
            false => cnt,
        }
    });

    println!("# of bursts that infected a node = {count}");
}

fn parse_input(lines: &[String]) -> CellsMap {
    // The initial map is a square
    let init_map_size = lines.len() as MapIndices;
    let middle = init_map_size / 2;
    let center = (middle, middle);
    let mut result = CellsMap::new(center);

    for (row, line) in lines.iter().enumerate() {
        for (col, cell) in line.chars().enumerate() {
            let pos = (row as MapIndices, col as MapIndices);
            result.add(pos, cell);
        }
    }

    result
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

static DATA_LEN: usize = 256;

type Byte = u8;
type Bytes = Vec<Byte>;
pub type Lengths = Vec<usize>;
type Numbers = Vec<usize>;
struct Data {
    numbers: Numbers,
    position: usize,
    skip_size: usize,
}

pub enum KnotHash {
    String(String),
    Bytes(Bytes),
}

fn reverse_slices(lengths: Lengths, numbs: &Numbers, start_position: usize, start_skip_size: usize) -> Data {
    let mut numbers = numbs.clone();
    let mut position = start_position;
    let mut skip_size = start_skip_size;

    for len in lengths {
        let mut inds: Vec<usize> = (position..DATA_LEN).take(len).collect();
        let inds_len = inds.len();
        if inds_len != len {
            for i in 0..len - inds_len {
                inds.push(i);
            }
        }
        for ip in 0 .. len / 2 {
            numbers.swap(inds[ip], inds[len - 1 - ip]);
        }
        position = (position + len + skip_size) % DATA_LEN;
        skip_size += 1;
    }

    Data { numbers, position, skip_size }
}

fn get_bytes(values: Data) -> Bytes {
    values
        .numbers
        .iter()
        .map(|&n| n as Byte)
        .collect()
}

fn get_knot_hash(bytes: Bytes, as_string: bool) -> KnotHash {
    let mut res_iter = bytes.into_iter();
    let mut knot_hash_b = Bytes::new();
    let mut knot_hash_s = Vec::<String>::new();

    for _ in 0..16 {
        let slice = res_iter.by_ref().take(16);
        let num = slice.reduce(|accum, item| {
            accum ^ item
        }).unwrap();

        if as_string {
            knot_hash_s.push(format!("{:02x}", num));
        } else {
            knot_hash_b.push(num);
        }
    }

    if as_string {
        KnotHash::String(knot_hash_s.concat())
    } else {
        KnotHash::Bytes(knot_hash_b)
    }
}

pub fn calc_knot_hash(lengths: Lengths, as_string: bool) -> KnotHash {
    let values = (0..64).fold(Data { numbers: (0..).take(DATA_LEN).collect(), position: 0, skip_size: 0 }, |round_vals, _| {
        reverse_slices(lengths.clone(), &round_vals.numbers, round_vals.position, round_vals.skip_size)
    });

    let bytes = get_bytes(values);
    get_knot_hash(bytes, as_string)
}

fn task_a(lines: Vec<String>) {
    let lengths = parse_input_numbers(&lines);
    let result = reverse_slices(lengths, &(0..).take(DATA_LEN).collect(), 0, 0);

    println!("Product = {}", result.numbers[0] * result.numbers[1]);
}

fn task_b(lines: Vec<String>) {
    let lengths: Lengths = parse_input_chars(&lines)
        .iter()
        .chain(&[17, 31, 73, 47, 23])
        .cloned()
        .collect();

    if let KnotHash::String(knot_hash) = calc_knot_hash(lengths, true) {
        println!("knot_hash = {}", knot_hash);
    }
}

fn parse_input_numbers(lines: &[String]) -> Lengths {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .collect()
}

pub fn parse_input_chars(lines: &[String]) -> Lengths {
    lines[0]
        .chars()
        .map(|c| c as usize)
        .collect()
}

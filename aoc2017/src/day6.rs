use std::collections::HashSet;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Banks = Vec<u32>;

fn get_large_bank(banks: &Banks) -> usize {
    let mut max = 0;
    let mut max_i = 0;
    for (i, banks_i) in banks.iter().enumerate() {
        if *banks_i > max {
            max = *banks_i;
            max_i = i;
        }
    }

    max_i
}

fn distribute(banks: &Banks) -> Banks {
    let mut r_banks = banks.clone();
    let max_i = get_large_bank(banks);
    let mut b_val = r_banks[max_i];
    r_banks[max_i] = 0;

    let mut i = max_i + 1;
    while b_val != 0 {
        if i == r_banks.len() {
            i = 0;
        }

        r_banks[i] += 1;
        b_val -= 1;
        i += 1;
    }

    r_banks
}

fn calc_redistribution_cycles(banks: &Banks, find_again: bool) -> usize {
    let mut used_banks_set = HashSet::<Banks>::new();
    used_banks_set.insert(banks.clone());

    let mut n_cycles = 1;
    let mut n_banks = distribute(banks);
    loop {
        n_cycles += 1;
        n_banks = distribute(&n_banks);
        if !used_banks_set.insert(n_banks.clone()) {
            break
        }
    }

    if find_again {
        let fa_bank = n_banks.clone();
        n_cycles = 0;
        loop {
            n_cycles += 1;
            n_banks = distribute(&n_banks);
            if n_banks == fa_bank {
                break
            }
        }
    }

    n_cycles
}

fn task_a(lines: Vec<String>) {
    let banks = parse_input(&lines);

    println!("# of cycles = {}", calc_redistribution_cycles(&banks, false));
}

fn task_b(lines: Vec<String>) {
    let banks = parse_input(&lines);

    println!("# of cycles = {}", calc_redistribution_cycles(&banks, true));
}

fn parse_input(lines: &[String]) -> Banks {
    lines[0].split_ascii_whitespace().map(|n| n.parse().unwrap()).collect()
}

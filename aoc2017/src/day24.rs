// #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

use std::collections::HashSet;
use std::str::FromStr;
use std::fmt::Debug;

use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ComponentPort = u16;
type ComponentVal = Vec<ComponentPort>;
type ComponentVals = Vec<ComponentVal>;

/*
    Work recursively
        Move extraction of zero components to build_strongest_bridge
        for each zero component
            Send zero component (to be received as a bridge) and rest components to build_possible_bridges
            loop
                Go through the rest of the rest components and try to find a possible match to the last component in the bridge
                if found
                    remove that match from rest components and send the match recursively
            when no more matches are found, return the current bridge

*/
fn build_possible_bridges(port: ComponentPort, comp_vals_rest: &ComponentVals, all_bridges: &[ComponentVals]) -> Vec<ComponentVals> {
    let start_stones =
        comp_vals_rest
        .iter()
        .enumerate()
        .filter(|(_, s)| s.iter().contains(&port))
        .filter(|(_, s)| port == 0 || !s.iter().contains(&0));

    if start_stones.clone().count() == 0 {
        return all_bridges.to_vec();
    }

    let mut ret_bridges = Vec::new();

    for (i, start_stone) in start_stones {
        let mut comp_vals_candidates = comp_vals_rest.clone();
        comp_vals_candidates.remove(i);

        let mut next_bridge_port = port;
        let mut next_bridges = all_bridges.to_vec();

        let mut next_bridge =
            match next_bridges.last() {
                Some(bridge) => bridge.clone(),
                None    => ComponentVals::new(),
            };

        let mut stone = start_stone.clone();
        'bl: loop {
            let next_bridge_comp =
                match stone[0] == next_bridge_port {
                    true  => stone.clone(),
                    false => vec![stone[1], stone[0]],
                };
            next_bridge_port = next_bridge_comp[1];
            next_bridge.push(next_bridge_comp);
            if !next_bridges.iter().contains(&next_bridge) {
                next_bridges.push(next_bridge.clone());
                next_bridges = build_possible_bridges(next_bridge_port, &comp_vals_candidates, &next_bridges);
            }

            loop {
                let o_fi = comp_vals_candidates.iter().position(|c| c.contains(&next_bridge_port));
                if o_fi.is_none() {
                    break 'bl
                }
                let fi = o_fi.unwrap();
                stone = comp_vals_candidates.remove(fi);
                if !stone.iter().contains(&0) {
                    break;
                }
            }
        }

        ret_bridges.extend_from_slice(&next_bridges[..]);
    }

    ret_bridges
}

fn build_strongest_bridge(comp_vals: &ComponentVals) -> i32 {
    let mut strength = 0;

    let bridges = build_possible_bridges(0, comp_vals, &[]);
    let mut br_chk = HashSet::new();
    for b in bridges {
        if br_chk.contains(&b) {
            continue;
        }
        br_chk.insert(b.clone());

        let strng =
            b
            .iter()
            .fold(0, |sum, e| sum + e.iter().sum::<ComponentPort>() as i32);

        strength = std::cmp::max(strng, strength);
    }

    strength
}

fn build_longest_bridge(comp_vals: &ComponentVals) -> i32 {
    let mut strength = 0;
    let mut longest = 0;

    let bridges = build_possible_bridges(0, comp_vals, &[]);
    let mut br_chk = HashSet::new();
    for b in bridges {
        if br_chk.contains(&b) {
            continue;
        }
        br_chk.insert(b.clone());

        let c_strng = |br: ComponentVals| {
            br
                .iter()
                .fold(0, |sum, e| sum + e.iter().sum::<ComponentPort>() as i32)
        };

        let len = b.len();
        if len >= longest {
            if len > longest {
                strength = c_strng(b);
            } else {
                strength = std::cmp::max(strength, c_strng(b));
            }

            longest = len;
        }
    }

    strength
}

fn task_a(lines: Vec<String>) {
    let component_vals: ComponentVals = parse_input(&lines);
    let strength = build_strongest_bridge(&component_vals);

    println!("Strongest brigde has strength {}", strength);
}

fn task_b(lines: Vec<String>) {
    let component_vals: ComponentVals = parse_input(&lines);
    let strength = build_longest_bridge(&component_vals);

    println!("Strength of longest brigde = {}", strength);
}

fn parse_input<T>(lines: &[String]) -> Vec<Vec<T>>
    where T: FromStr + Ord,
          <T as FromStr>::Err: Debug
{
    let mut compnts =
        lines
        .iter()
        .map(|line| {
            let mut line_vec =
                line
                .split('/')
                .map(|p_elem|
                    p_elem
                    .parse::<T>()
                    .unwrap()
                )
                .collect::<Vec<_>>();
            line_vec.sort();
            line_vec
        })
        .collect::<Vec<_>>();

    compnts.sort();
    compnts
}

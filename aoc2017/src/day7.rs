use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ProgNames = Vec<String>;
type Disks = Option<ProgNames>;

#[derive(Clone, Debug)]
struct Program {
    weight: u32,
    holds_programs: Disks,
    total_weight: u32,
}

#[derive(Clone, Debug)]
struct Programs {
    map: HashMap<String, Program>
}

impl Program {
    fn new(weight: &str, holds_programs: ProgNames) -> Self {
        let holds_progs = if !holds_programs.is_empty() {
            Some(holds_programs)
        } else {
            None
        };
        Self {
            weight: weight.parse().unwrap(),
            holds_programs: holds_progs,
            total_weight: 0
        }
    }
}

impl Programs {
    fn new() -> Self {
        Self { map: HashMap::new() }
    }

    fn calc_totals(&mut self, prog_n: String) {
        let prog = self.map.get(&prog_n).unwrap();
        let wght =
            {
                if let Some(hold_progs) = prog.holds_programs.clone() {
                    let wgt = hold_progs.iter().fold(prog.weight, |mut sum, prg| {
                        self.calc_totals(prg.clone());
                        sum += self.map[prg].total_weight;
                        sum
                    });
                    wgt
                } else {
                    prog.weight
                }
            };
        let prog = self.map.get_mut(&prog_n).unwrap();
        prog.total_weight = wght;
    }

    fn find_unbalanced(&self, prog_n: String) -> u32 {
        let mut ret = 0;
        let prog = self.map.get(&prog_n).unwrap();
        if let Some(hold_progs) = &prog.holds_programs {
            let mut wgt_map = HashMap::<i32, u8>::new();
            for pg in hold_progs {
                ret = self.find_unbalanced(pg.clone());
                if ret != 0 {
                    break;
                }
                *wgt_map.entry(self.map.get(pg).unwrap().total_weight as i32).or_insert(0) += 1;
            }

            if wgt_map.len() > 1 {
                let keys: Vec<&i32> = wgt_map.keys().collect();
                let inds = if wgt_map[keys[0]] == 1 {
                    (0, 1)
                } else {
                    (1, 0)
                };

                let pgw: Vec<&String> = hold_progs.iter().filter(|p| *keys[inds.0] as u32 == self.map.get(*p).unwrap().total_weight).collect();
                ret = self.map.get(pgw[0]).unwrap().weight - (*keys[inds.0] - *keys[inds.1]) as u32;
            }
        }

        ret
    }
}

fn task_a(lines: Vec<String>) {
    let (root, _) = parse_input(&lines);

    println!("Name of bottom program = {}", root);
}

fn task_b(lines: Vec<String>) {
    let (root, mut programs) = parse_input(&lines);

    programs.calc_totals(root.clone());
    let n_weight = programs.find_unbalanced(root);

    println!("New weight = {n_weight}");
}

fn parse_input(lines: &[String]) -> (String, Programs) {
    use regex::Regex;

    let line_reg: Regex = Regex::new(r"(?P<name>\w+)\s\((?P<weight>\d+)\)(?:\s->\s(?P<progs>.*))?").unwrap();
    let mut all_programs = Programs::new();
    let mut holds_programs = HashMap::new();

    for line in lines {
        let captures = line_reg.captures(line).unwrap();
        let name = captures.name("name").unwrap().as_str();
        let weight = captures.name("weight").unwrap().as_str();
        let progs = captures.name("progs");
        if let Some(prog) = progs {
            holds_programs.insert(name, (weight, prog.as_str().split(", ").collect::<Vec<&str>>()));
        } else {
            let prog = Program::new(
                weight,
                ProgNames::new()
            );
            all_programs.map.insert(name.to_string(), prog);
        }
    }

    let mut root = String::new();
    while !holds_programs.is_empty() {
        'holds: for (name, prg) in holds_programs.clone() {
            let mut progs = ProgNames::new();
            for p in prg.1 {
                let ps = p.to_string();
                if !all_programs.map.contains_key(&ps) {
                    continue 'holds;
                }

                progs.push(ps);
            }

            root = name.to_string();
            holds_programs.remove(root.as_str());

            let prog = Program::new(
                prg.0,
                progs
            );
            all_programs.map.insert(root.clone(), prog);
        }
    }

    (root, all_programs)
}

use std::collections::VecDeque;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type RingBuf = VecDeque<usize>;

#[derive(PartialEq)]
enum RepPos {
    PosAfterCurrent,
    PosAfterZero,
}

fn calc_ring_buf(n_steps: usize, n_insertions: usize, report_pos: RepPos) -> (RingBuf, usize) {
    let mut ring_buffer = RingBuf::new();
    ring_buffer.push_back(0);
    if report_pos == RepPos::PosAfterZero {
        ring_buffer.push_back(0);
    }
    let mut curr_pos = 0;

    for n in 1..=n_insertions {
        let moves = n_steps % n;
        curr_pos = (curr_pos + moves) % n;
        if report_pos == RepPos::PosAfterCurrent {
            if curr_pos == n - 1 {
                ring_buffer.push_back(n);
            } else {
                ring_buffer.insert(curr_pos + 1, n);
            }
        } else if curr_pos == 0 {
            ring_buffer[1] = n;
        }
        curr_pos += 1;
    }

    let ret_pos = match report_pos {
        RepPos::PosAfterCurrent => (curr_pos + 1) % ring_buffer.len(),
        RepPos::PosAfterZero => 1,
    };

    (ring_buffer, ret_pos)
}

fn task_a(lines: Vec<String>) {
    let n_steps = parse_input(&lines);

    let (ring_buf, rep_pos) = calc_ring_buf(n_steps, 2017, RepPos::PosAfterCurrent);

    println!("Value = {}", ring_buf[rep_pos]);
}

fn task_b(lines: Vec<String>) {
    let n_steps = parse_input(&lines);

    let (ring_buf, rep_pos) = calc_ring_buf(n_steps, 50000000, RepPos::PosAfterZero);

    println!("Value after 0 = {}", ring_buf[rep_pos]);
}

fn parse_input(lines: &[String]) -> usize {
    lines[0]
        .parse()
        .unwrap()
}

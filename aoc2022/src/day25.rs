#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {

}
use std::collections::VecDeque;

use internal::*;

fn task_a(lines: &[String]) {
    let figures = parse_input(lines);
// println!("figures = {:?}", figures);
    let mut left_to_convert: isize =
        figures
        .iter()
        .map(|vals|
            vals
            .iter()
            .rev()
            .enumerate()
            // .inspect(|(i_pos, val)| println!("i_pos = {}, val = {}", i_pos, val))
            .map(|(i_pos, val)| *val * 5_isize.pow(i_pos as u32) )
            .sum::<isize>()
        )
        .sum();

    let mut snafu_number = String::new();
    while left_to_convert != 0 {
// println!("left_to_convert = {}", left_to_convert);
        let fig_num = left_to_convert % 5;
        left_to_convert /= 5;
// println!("fig_num = {}", fig_num);

        let c = match fig_num {
            0  => '0',
            1  => '1',
            2  => '2',
            3  => { left_to_convert += 1; '=' },
            4  => { left_to_convert += 1; '-' },
            _ => panic!()
        };
// println!("c = {}", c);

        snafu_number.insert(0, c);
    }

    println!("SNAFU number is: {}", snafu_number);
}

fn task_b(lines: &[String]) {
    let _ = parse_input(lines);
}

fn parse_input(lines: &[String]) -> Vec<Vec<isize>> {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|c|
                match c {
                    '0' | '1' | '2' => c.to_digit(10).unwrap() as isize,
                    '-' => -1,
                    '=' => -2,
                    _ => panic!()
                }
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t() {}
}

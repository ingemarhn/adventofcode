pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::VecDeque;

    pub type IntType = u64;
    pub type Monkeys = Vec<Monkey>;

    #[derive(Clone, Debug)]
    enum Oper {
        Plus(IntType),
        Mul(IntType),
        Sqr,
        Nul,
    }

    #[derive(Clone, Debug)]
    pub struct Monkey {
        items: VecDeque<IntType>,
        operation: Oper,
        divisor: IntType,
        send_to: (usize, usize),
        n_insp_items: u32,
    }

    impl Monkey {
        pub fn new() -> Self {
            Self { items: VecDeque::new(), operation: Oper::Nul, divisor: 0, send_to: (usize::MAX, usize::MAX), n_insp_items: 0 }
        }

        pub fn add_item(&mut self, item: IntType) {
            self.items.push_back(item);
        }

        pub fn get_first_item(&mut self) -> Option<IntType> {
            self.items.pop_front()
        }

        pub fn add_oper(&mut self, value: IntType, oper: &str) {
            self.operation = match oper {
                "+"  => Oper::Plus(value),
                "*"  => Oper::Mul(value),
                "**" => Oper::Sqr,
                _ => panic!("Shitty operator!")
            };
        }

        pub fn add_divisor(&mut self, div: IntType) {
            self.divisor = div;
        }

        pub fn get_divisor(&self) -> IntType {
            self.divisor
        }

        pub fn add_send_to(&mut self, test_val: bool, monkey: usize) {
            match test_val {
                true  => self.send_to = (monkey, self.send_to.1),
                false => self.send_to = (self.send_to.0, monkey),
            };
        }

        pub fn get_send_to(&self, item: IntType) -> usize {
            match self.run_test(item) {
                true  => self.send_to.0,
                false => self.send_to.1,
            }
        }

        pub fn run_oper(&mut self, item: IntType) -> IntType {
            self.n_insp_items += 1;

            match self.operation {
                Oper::Plus(val) => item + val,
                Oper::Mul(val)  => item * val,
                Oper::Sqr  => item * item,
                Oper::Nul  => panic!("Ohhh, Nul oper :("),
            }
        }

        pub fn get_insp_items(&self) -> u32 {
            self.n_insp_items
        }

        fn run_test(&self, item: IntType) -> bool {
            item % self.divisor == 0
        }
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let mut monkeys = parse_input(lines);
    let n_monkeys = monkeys.len();

    for _ in 0 .. 20 {
        for i in 0 .. n_monkeys {
            let mut next_monkeys = Vec::new();
            {
                let monkey = monkeys.get_mut(i).unwrap();
                while let Some(item) = monkey.get_first_item() {
                    let new_item = monkey.run_oper(item) / 3;
                    let next_monkey_ind = monkey.get_send_to(new_item);
                    next_monkeys.push((next_monkey_ind, new_item));
                }
            }
            for (next_m_ind, item) in next_monkeys {
                let monkey = monkeys.get_mut(next_m_ind).unwrap();
                monkey.add_item(item);
            }
        }
    }

    let mut two_most_active = vec![0, 0];
    for monkey in monkeys {
        let n_insp = monkey.get_insp_items();
        if n_insp > two_most_active[1] {
            two_most_active.pop();
            two_most_active.push(n_insp);
            two_most_active.sort_by(|a, b| b.cmp(a));
        }
    }

    println!("Monkey business level = {}", two_most_active[0] * two_most_active[1]);
}

fn task_b(lines: &[String]) {
    let mut monkeys = parse_input(lines);
    let n_monkeys = monkeys.len();
    // "to keep your worry levels manageable".
    // To do so, you must find a common multiple of all monkeys' divisors and perform a modulo to each item.
    // In this way, you find the right worry level for dividing by the divisor of each monkey.
    let mod_val: IntType =
        monkeys
        .iter()
        .map(|m| m.get_divisor())
        .product();

    for _ in 0 .. 10_000 {
        for i in 0 .. n_monkeys {
            let mut next_monkeys = Vec::new();
            {
                let monkey = monkeys.get_mut(i).unwrap();
                while let Some(item) = monkey.get_first_item() {
                    let new_item = monkey.run_oper(item) % mod_val;
                    let next_monkey_ind = monkey.get_send_to(new_item);
                    next_monkeys.push((next_monkey_ind, new_item));
                }
            }
            for (next_m_ind, item) in next_monkeys {
                let monkey = monkeys.get_mut(next_m_ind).unwrap();
                monkey.add_item(item);
            }
        }
    }

   let mut two_most_active = Vec::new();
    for monkey in monkeys {
        let n_insp = monkey.get_insp_items() as IntType;
        two_most_active.push(n_insp);
    }
    two_most_active.sort_unstable_by(|a, b| b.cmp(a));

    println!("Monkey business level = {}", two_most_active[0] * two_most_active[1]);
}

fn parse_input(lines: &[String]) -> Monkeys {
    let mut monkeys = Monkeys::new();

    let mut monkey = Monkey::new();
    for line in lines {
        if line.is_empty() {
            continue;
        }

        let parts: Vec<&str> =
            line
            .trim()
            .split_ascii_whitespace()
            .collect();
        match &parts[..] {
            ["Monkey", _n] => monkey = Monkey::new(),
            ["Starting", "items:", items @ ..] => {
                for item in items {
                    monkey.add_item(item.trim_end_matches(',').parse().unwrap());
                }
            },
            ["Operation:", "new", "=", "old", op, val] => {
                if val != &"old" {
                    monkey.add_oper(val.parse().unwrap(), op);
                } else {
                    monkey.add_oper(0, "**");
                }
            },
            ["Test:", "divisible", "by", div] => monkey.add_divisor(div.parse().unwrap()),
            [_, "true:",  _, _, _, monk] => monkey.add_send_to(true, monk.parse().unwrap()),
            [_, "false:", _, _, _, monk] => {
                monkey.add_send_to(false, monk.parse().unwrap());
                monkeys.push(monkey.clone());
            },
            pnc => panic!("Bad parsing! {pnc:?}"),
        }
    }

    monkeys
}

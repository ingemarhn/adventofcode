use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::fmt;
    use std::collections::HashMap;

    type ParentLink = String;
    type ChildLink = String;
    type Children = Vec<ChildLink>;

    pub type Size = u64;
    pub type Directories = HashMap<ChildLink, Dir>;

    #[derive(Clone, Debug)]
    #[allow(dead_code)]
    enum SemiTree {
        Empty,
        BinTree((ChildLink, ChildLink)),
        MultiTree(Children),
    }

    #[derive(Clone)]
    pub struct DirControl {
        paths: Directories,
        root: String,
        current_path: ChildLink,
    }

    impl DirControl {
        pub fn new(root_folder: &str) -> Self {
            let child = Dir::new(root_folder);
            let root_folder_s = root_folder.to_string();

            let mut paths = HashMap::new();

            paths.insert(root_folder.to_string(), child);
            Self { paths, root: root_folder_s.clone(), current_path: root_folder_s }
        }

        pub fn set_dir_multi(&mut self, dir_name: &str) {
            self.current_path = dir_name.to_string();
        }

        pub fn change_dir_multi(&mut self, dir_name: &str) {
            if dir_name == ".." {
                self.change_dir_up();
            } else {
                self.current_path = self.push_path(dir_name);
            }
        }

        fn change_dir_up(&mut self) {
            self.current_path = self.pop_path();
        }

        pub fn add_child_multi(&mut self, name_s: &str) {
            let name = name_s.to_string();
            // Create new child and add parent
            let mut child = Dir::new(&name);
            child.add_parent(&self.current_path);

            let child_path = self.push_path(name_s);
            self.paths.insert(child_path.clone(), child);

            // Update children of the current_path Dir with the new child
            let curr_dir = self.paths.get_mut(&self.current_path).unwrap();
            match curr_dir.children {
                SemiTree::Empty => {
                    curr_dir.children = SemiTree::MultiTree(vec![child_path]);
                },
                SemiTree::MultiTree(ref mut child_vec) => {
                    child_vec.push(child_path);
                },
                SemiTree::BinTree(_) => panic!("SemiTree::BinTree not allowed in add_child_multi"),
            }
        }

        fn push_path(&self, name: &str) -> String {
            let mut path = self.current_path.clone();
            if path != "/" {
                path.push('/');
            }
            path.push_str(name);

            path
        }

        fn pop_path(&self) -> String {
            let mut path = self.current_path.clone();
            let mut p_ind = path.rfind('/').unwrap();
            if p_ind == 0 {
                p_ind = 1;
            }
            path.drain(p_ind ..);

            path
        }

        pub fn add_file(&mut self, filename: &str, size: Size) {
            let curr_dir = self.paths.get_mut(&self.current_path).unwrap();
            curr_dir.add_file(filename, size);
        }

        pub fn get_children(&self) -> Children {
            let curr_dir = self.paths.get(&self.current_path).unwrap();
            match curr_dir.children {
                SemiTree::Empty => {
                    Children::new()
                },
                SemiTree::MultiTree(ref child_vec) => {
                    child_vec.clone()
                },
                SemiTree::BinTree(_) => panic!("SemiTree::BinTree not allowed"),
            }
        }

        pub fn get_current_path(&self) -> String {
            self.current_path.clone()
        }

        #[allow(dead_code)]
        pub fn get_paths(&self) -> Children {
            let mut keys: Vec<String> = self.paths.keys().cloned().collect();
            keys.sort();

            keys
        }

        pub fn get_files(&self) -> Vec<File> {
            let curr_dir = self.paths.get(&self.current_path).unwrap();
            curr_dir.files.clone()
        }
    }

    impl fmt::Debug for DirControl {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let mut keys: Vec<&String> = self.paths.keys().collect();
            writeln!(f, "DirControl {{").unwrap();
            writeln!(f, "    paths: {{").unwrap();
            keys.sort();
            for k in keys {
                write!(f, "        {:#?}: ", k).unwrap();
                let d = format!("{:#?}", self.paths[k]);
                writeln!(f, "{},", d.replace('\n', "\n        ")).unwrap();
            }
            writeln!(f, "    root: {}", self.root).unwrap();
            writeln!(f, "    current_path: {}", self.current_path)
        }
    }

    #[derive(Clone, Debug)]
    #[allow(dead_code)]
    pub struct File {
        name: String,
        size: Size,
    }

    impl File {
        pub fn get_size(&self) -> Size {
            self.size
        }
    }

    #[derive(Clone, Debug)]
    #[allow(dead_code)]
    pub struct Dir {
        name: String,
        parent: Option<ParentLink>,
        children: SemiTree,
        files: Vec<File>,
    }

    impl Dir {
        fn new(name: &str) -> Self{
            Self { name: name.to_string(), parent: None, children: SemiTree::Empty, files: Vec::new() }
        }

        fn add_parent(&mut self, parent: &str) {
            self.parent = Some(parent.to_string());
        }

        fn add_file(&mut self, filename: &str, size: Size) {
            let file = File { name: filename.to_string(), size };
            self.files.push(file);
        }
    }
}
use internal::*;

fn calc_folder_sizes(dir_control: &mut DirControl, folder_sizes_in: HashMap<String, Size>) -> HashMap<String, Size> {
    let curr_path = dir_control.get_current_path();
    let mut folder_sizes = folder_sizes_in;

    let mut size_sum = 0;

    for child in dir_control.get_children() {
        dir_control.set_dir_multi(&child);
        folder_sizes = calc_folder_sizes(dir_control, folder_sizes);
        size_sum += folder_sizes[&child];
    }
    dir_control.set_dir_multi(&curr_path);
    for file in dir_control.get_files() {
        size_sum += file.get_size();
    }
    folder_sizes.insert(curr_path, size_sum);

    folder_sizes
}

fn task_a(lines: &[String]) {
    let mut dir_ctrl = parse_input(lines);
    dir_ctrl.set_dir_multi("/");

    let folder_sizes = calc_folder_sizes(&mut dir_ctrl, HashMap::new());
    let keys: Vec<&String> = folder_sizes.keys().collect();

    let limit = 100000;
    let mut sum = 0;
    for key in keys {
        let f_size = folder_sizes[key];
        if f_size <= limit {
            sum += f_size;
        }
    }

    println!("Sum size of folders <= 100000 = {}", sum);
}

fn task_b(lines: &[String]) {
    let mut dir_ctrl = parse_input(lines);
    dir_ctrl.set_dir_multi("/");

    let folder_sizes = calc_folder_sizes(&mut dir_ctrl, HashMap::new());
    let keys: Vec<&String> = folder_sizes.keys().collect();
    let total_space = 70000000;
    let needed_space = 30000000;
    let avail_space = total_space - folder_sizes["/"];
    let needs_more = needed_space - avail_space;
    let mut small_to_delete = total_space;

    for key in keys {
        let f_size = folder_sizes[key];
        if f_size <= small_to_delete && f_size >= needs_more {
            small_to_delete = f_size;
        }
    }

    println!("Size of smallest folder to delete = {}", small_to_delete);
}

fn parse_input(lines: &[String]) -> DirControl {
    // Expect that first line is root (i.e. "$ cd /")
    let mut dir_control = DirControl::new("/");

    // Handle rest of the lines
    for line in &lines[1..] {
        match line.split_ascii_whitespace().collect::<Vec<&str>>()[..] {
            ["$", "cd", location] => dir_control.change_dir_multi(location),
            ["$", "ls"] => (),
            ["dir", dir_name] => dir_control.add_child_multi(dir_name),
            [size, file_name] => dir_control.add_file(file_name, size.parse().unwrap()),
            _ => panic!(),
        }
    }

    dir_control
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dirctrl() {
        let dctrl = DirControl::new("/");
        println!("0 dctrl = {:#?}", dctrl);
    }

    #[test]
    fn add_child_multi() {
        let mut dctrl = DirControl::new("/");
        dctrl.add_child_multi("Nisse");
        println!("1 dctrl = {:#?}", dctrl);
    }

    #[test]
    fn add_child_multi2() {
        let mut dctrl = DirControl::new("/");
        dctrl.add_child_multi("Nisse");
        dctrl.add_child_multi("Hult");
        println!("2 dctrl = {:#?}", dctrl);
    }

    #[test]
    fn add_child_multi3() {
        let mut dctrl = DirControl::new("/");
        dctrl.add_child_multi("Nisse");
        dctrl.add_child_multi("Hult");
        dctrl.add_file("nissepisse", 123);
        dctrl.change_dir_multi("Hult");
        dctrl.add_child_multi("Goat");
        dctrl.add_file("päronpung", 6);
        dctrl.change_dir_multi("..");
        dctrl.add_child_multi("Linda");
        println!("3 dctrl = {:#?}", dctrl);
    }
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::fmt;

    pub type Num = i64;

    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
    #[derive(Debug)]
    pub struct Number (Num);

    #[derive(Clone, Copy, Debug)]
    pub struct NumberNode {
        value: Number,
        left_index: usize,
        right_index: usize,
    }

    #[derive(Debug)]
    pub struct Numbers {
        number_nodes: Vec<NumberNode>,
    }

    type GetIndex = fn(&Numbers, usize) -> usize;

    impl Numbers {
        pub fn new() -> Self {
            Numbers { number_nodes: Vec::new() }
        }

        pub fn push(&mut self, num: Num) {
            let new_last_index = self.number_nodes.len();

            let Some(mut work_node) = self.number_nodes.first_mut() else {
                let node = NumberNode { value: Number(num), left_index: 0, right_index: 0 };
                self.number_nodes.push(node);

                return;
            };

            let left_index = work_node.left_index;
            work_node.left_index = new_last_index; // This index will be created further down

            work_node = self.number_nodes.last_mut().unwrap();
            work_node.right_index = new_last_index;

            let node = NumberNode { value: Number(num), left_index, right_index: 0 };
            self.number_nodes.push(node);
        }

        fn get_l_index(&self, index: usize)  -> usize {
            self.number_nodes[index].left_index
        }

        fn get_r_index(&self, index: usize)  -> usize {
            self.number_nodes[index].right_index
        }

        fn set_l_index(&mut self, index: usize, value: usize) {
            self.number_nodes[index].left_index = value;
        }

        fn set_r_index(&mut self, index: usize, value: usize) {
            self.number_nodes[index].right_index = value;
        }

        pub fn move_node(&mut self, index: usize) {
            let (node_value, node_left_index, node_right_index) = {
                let node = self.number_nodes[index];
                (node.value.0, node.left_index, node.right_index)
            };

            if node_value == 0 {
                return;
            }

            let get_l_index = Self::get_l_index as GetIndex;
            let set_l_index  = Self::set_l_index;
            let get_r_index = Self::get_r_index as GetIndex;
            let set_r_index  = Self::set_r_index;

            // Pull out this node from the node ring
            set_r_index(self, node_left_index, node_right_index);
            set_l_index(self, node_right_index, node_left_index);

            let numbers_len_after_pull_out = self.number_nodes.len() as Num - 1;
            let mut ind = index;
            let (range, get_index) = {
                let n_steps = {
                    let stps = node_value % numbers_len_after_pull_out;
                    if stps.abs() > numbers_len_after_pull_out / 2 {
                        -(numbers_len_after_pull_out - stps)
                    } else {
                        stps
                    }
                };

                if n_steps > 0 {
                    (0 .. n_steps, get_r_index)
                } else {
                    (n_steps - 1 .. 0, get_l_index)
                }
            };

            for _ in range {
                ind = get_index(self, ind);
            }

            let ind_right_index = get_r_index(self, ind);
            set_l_index(self, index, ind);
            set_r_index(self, index, ind_right_index);
            set_r_index(self, ind, index);
            set_l_index(self, ind_right_index, index);
        }

        pub fn len(&self) -> usize {
            self.number_nodes.len()
        }
    }

    impl fmt::Display for NumberNode {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{}", self.value.0)
        }
    }

    impl fmt::Display for Numbers {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Numbers {{ ")?;
            if !self.number_nodes.is_empty() {
                write!(f, "{}", self.number_nodes[0])?;
                let mut next = self.number_nodes[0].right_index;
                while next != 0 {
                    write!(f, ", {}", self.number_nodes[next])?;
                    next = self.number_nodes[next].right_index;
                }
            }
            write!(f, " }}")
        }
    }

    pub fn calc_grove_coordinates_sum(input_numbers: &[Num], groove_counters: &[usize], n_decryptions: u8) -> Num {
        let mut numbers = Numbers::new();

        for n in input_numbers {
            numbers.push(*n);
        }

        for _ in 0 .. n_decryptions {
            for i in 0 .. numbers.len() {
                numbers.move_node(i);
            }
        }

        let mut node_ind =
            numbers
                .number_nodes
                .iter()
                .position(|n| n.value.0 == 0).unwrap();

        let mut gc_sum = 0;
        let mut gc_start = 1;
        for gc in groove_counters {
            for _ in gc_start ..= *gc {
                node_ind = numbers.number_nodes[node_ind].right_index;
            }

            gc_sum += numbers.number_nodes[node_ind].value.0;
            gc_start = *gc + 1;
        }

        gc_sum
    }
}
use internal::*;
use itertools::Itertools;

fn task_a(lines: &[String]) {
    let input_numbers = parse_input(lines);
    let sum_grove_coordinates = calc_grove_coordinates_sum(&input_numbers, &[1000, 2000, 3000], 1);

    println!("Sum of grove coordinates = {sum_grove_coordinates}");
}

fn task_b(lines: &[String]) {
    let input_numbers =
        parse_input(lines)
        .iter()
        .map(|n| *n * 811589153)
        .collect_vec();
    let sum_grove_coordinates = calc_grove_coordinates_sum(&input_numbers, &[1000, 2000, 3000], 10);

    println!("Sum of grove coordinates = {sum_grove_coordinates}");
}

fn parse_input(lines: &[String]) -> Vec<Num> {
    lines
        .iter()
        .map(|line|
            line
            .parse()
            .unwrap()
        )
        .collect()
}

#[cfg(test)]
mod tests20 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use core::num;

    use itertools::Itertools;

    use super::*;

    #[test]
    fn t01() {
        let input = "1
2
-3
3
-2
0
4";
        let input_numbers = parse_input(&input.split_ascii_whitespace().map(|e| e.to_string()).collect_vec());
        let mut numbers = Numbers::new();
        for n in input_numbers {
            numbers.push(n);
        }
        println!("{}", numbers);

        for i in 0 .. numbers.len() {
            numbers.move_node(i);
            println!("{i}");
            println!("{}", numbers);
        }
    }
}

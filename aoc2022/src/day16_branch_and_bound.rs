// See https://github.com/orlp/aoc2022/blob/master/src/bin/day16.rs

use std::cmp::Reverse;
use std::collections::BinaryHeap;

use anyhow::{Ok, Result};
// use aoc2022::{OptionSomeExt, Priority, RegexExtract};
use hashbrown::{HashMap, HashSet};
use itertools::Itertools;
use regex::Regex;

// The part below, marked with hashes, is taken from https://github.com/orlp/aoc2022/blob/master/src/lib.rs
// #######################################################################
// aoc2022::{OptionSomeExt, Priority, RegexExtract};
use core::panic::Location;
use std::cmp::Ordering;
use std::fmt;

use regex::{CaptureMatches, Captures};

#[derive(Debug)]
pub struct NoneError {
    location: &'static Location<'static>,
}

impl std::error::Error for NoneError {}

impl fmt::Display for NoneError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "some() was called on None value at {}", self.location)
    }
}

pub trait OptionSomeExt {
    type Item;

    fn some(self) -> Result<Self::Item, NoneError>;
}

impl<T> OptionSomeExt for Option<T> {
    type Item = T;

    #[track_caller]
    fn some(self) -> Result<Self::Item, NoneError> {
        match self {
            Some(val) => std::result::Result::Ok(val),
            None => Err(NoneError {
                location: Location::caller(),
            }),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Priority<P, T>(pub P, pub T);

impl<P: Ord + Eq, T> Ord for Priority<P, T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<P: Ord + Eq, T> PartialOrd for Priority<P, T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<P: Eq, T> PartialEq for Priority<P, T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl<P: Eq, T> Eq for Priority<P, T> {}

pub struct RegexExtractIter<'r, 't, const N: usize> {
    captures: CaptureMatches<'r, 't>,
}

impl<'r, 't, const N: usize> Iterator for RegexExtractIter<'r, 't, N> {
    type Item = (&'t str, [&'t str; N]);

    fn next(&mut self) -> Option<Self::Item> {
        self.captures.next().map(extract_from_capture)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.captures.size_hint()
    }
}

pub trait RegexExtract {
    /// Finds the leftmost-first match in `text` and returns a tuple containing the whole match
    /// and its N participating capture groups as strings. If no match is found, `None` is returned.
    ///
    /// # Panics
    ///
    /// Panics if the number of participating captures is not equal to N.
    fn extract<'t, const N: usize>(&self, text: &'t str) -> Option<(&'t str, [&'t str; N])>;

    fn _extract_iter<'r, 't, const N: usize>(&'r self, text: &'t str)
        -> RegexExtractIter<'r, 't, N>;
}

impl RegexExtract for Regex {
    fn extract<'t, const N: usize>(&self, text: &'t str) -> Option<(&'t str, [&'t str; N])> {
        self.captures(text).map(extract_from_capture)
    }

    fn _extract_iter<'r, 't, const N: usize>(
        &'r self,
        text: &'t str,
    ) -> RegexExtractIter<'r, 't, N> {
        RegexExtractIter {
            captures: self.captures_iter(text),
        }
    }
}

#[allow(clippy::needless_lifetimes)]
fn extract_from_capture<'t, const N: usize>(caps: Captures<'t>) -> (&'t str, [&'t str; N]) {
    let mut participating = caps.iter().flatten();
    let whole_match = participating.next().unwrap().as_str();
    let captured = [0; N].map(|_| participating.next().unwrap().as_str());
    assert!(
        participating.next().is_none(),
        "too many participating capture groups"
    );
    (whole_match, captured)
}

// #######################################################################

struct Valve<'s> {
    flow: u32,
    neighbors: Vec<&'s str>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
struct State {
    pressure: u32,
    opened: u64,
    pos: [u16; 2],
    time: [u32; 2],
}

impl State {
    fn upper_bound(&self, best_valves: &[Vec<(usize, u32, u32)>]) -> u32 {
        let [mut max_t, mut min_t] = self.time;
        let mut opened = self.opened;
        let mut bound = self.pressure;
        'next_valve: loop {
            for (i, min_dist, f) in &best_valves[max_t as usize] {
                if opened & (1 << i) == 0 {
                    max_t -= min_dist;
                    bound += f * max_t;
                    if max_t < min_t {
                        (min_t, max_t) = (max_t, min_t);
                    }
                    opened |= 1 << i;
                    continue 'next_valve;
                }
            }
            return bound;
        }
    }
}

fn max_pressure_release(
    start: u16,
    edges: &[Vec<(u16, u32)>],
    flows: &[u32],
    best_valves: &[Vec<(usize, u32, u32)>],
    time: [u32; 2],
) -> u32 {
    let init = State {
        pressure: 0,
        opened: 1 << start,
        pos: [start, start],
        time,
    };

    let mut seen = HashSet::with_capacity(1024);
    let mut best = 0;
    let mut paths = BinaryHeap::with_capacity(1024);
    paths.push(Priority(u32::MAX, init));
    while let Some(Priority(upper, cur)) = paths.pop() {
        if upper <= best {
            return best;
        }

        if !seen.insert(State { pressure: 0, ..cur }) {
            continue;
        }

        for (next, edge_len) in &edges[cur.pos[0] as usize] {
            if cur.time[0] > *edge_len && cur.opened & (1 << next) == 0 {
                let new_time = cur.time[0] - edge_len;
                let mut next_state = State {
                    pressure: cur.pressure + flows[*next as usize] * new_time,
                    opened: cur.opened | (1 << *next),
                    pos: [*next, cur.pos[1]],
                    time: [new_time, cur.time[1]],
                };
                if next_state.time[0] < next_state.time[1] {
                    next_state.pos.swap(0, 1);
                    next_state.time.swap(0, 1);
                }
                best = best.max(next_state.pressure);
                let upper = next_state.upper_bound(best_valves);
                if upper > best {
                    paths.push(Priority(upper, next_state));
                }
            }
        }
    }
    best
}

fn floyd_warshall(dists: &mut [u32], n: usize) {
    for k in 0..n {
        for i in 0..n {
            for j in 0..n {
                dists[i + j * n] =
                    dists[i + j * n].min(dists[i + k * n].saturating_add(dists[k + j * n]));
            }
        }
    }
}

fn day16_branch_and_bound_main(lines: &[String], part: u8) -> Result<u32> {
    let start = std::time::Instant::now();

    let mut valves = Vec::new();
    let mut ids = HashMap::new();
    let re = Regex::new(
        r"Valve ([A-Z]{2}) has flow rate=(\d+); tunnels? leads? to valves? ([A-Z]{2}(?:, [A-Z]{2})*)",
    )?;
    for line in lines {
        let [name, flow, neighbors] = re.extract(line).some()?.1;
        ids.insert(name, valves.len());
        valves.push(Valve {
            flow: flow.parse()?,
            neighbors: neighbors.split(", ").collect_vec(),
        });
    }
    let n = valves.len();
    assert!(n <= 64);

    let mut dists = vec![u32::MAX; n * n];
    for (i, v) in valves.iter().enumerate() {
        dists[i + i * n] = 0;
        for neighbor in &v.neighbors {
            dists[i + ids[neighbor] * n] = 1;
        }
    }
    floyd_warshall(&mut dists, n);

    let direct_connections = (0..n).map(|from| {
        let nonzero_flow = (0..n).flat_map(|to| {
            let dist = dists[from + to * n];
            let valid = (from == ids["AA"] || valves[from].flow > 0 && valves[to].flow > 0)
                && dist < u32::MAX;
            valid.then_some((to as u16, dist + 1))
        });
        nonzero_flow
            .sorted_by_key(|(to, _)| Reverse(valves[*to as usize].flow))
            .collect_vec()
    });

    let edges = direct_connections.collect_vec();
    let flows = valves.iter().map(|v| v.flow).collect_vec();
    let best_valves: Vec<Vec<_>> = (0..=30)
        .map(|t| {
            // Order valves by payoff given our remaining time t.
            let iflows = flows.iter().copied().enumerate();
            iflows
                .flat_map(|(i, f)| {
                    let nonzero_neighbors = (0..n).filter(|j| i != *j && valves[*j].flow > 0);
                    let nonzero_dists = nonzero_neighbors.map(|j| dists[i + j * n] + 1);
                    let min_dist = nonzero_dists.min().unwrap();
                    (t > min_dist).then_some((i, min_dist, f))
                })
                .sorted_by_key(|(_i, d, f)| Reverse(f * (t - d)))
                .collect()
        })
        .collect();
    let result =
        if part == 1 {
            max_pressure_release(ids["AA"] as u16, &edges, &flows, &best_valves, [30, 0])
        } else {
            max_pressure_release(ids["AA"] as u16, &edges, &flows, &best_valves, [26, 26])
        };

    println!("time: {:?}", start.elapsed());
    Ok(result)
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: &[String]) {
    let pressure = day16_branch_and_bound_main(lines, 1).unwrap();
    println!("Most pressure released = {}", pressure);
}

fn task_b(lines: &[String]) {
    let pressure = day16_branch_and_bound_main(lines, 2).unwrap();
    println!("Most pressure released = {}", pressure);
}

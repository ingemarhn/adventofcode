use std::collections::{HashMap, VecDeque, HashSet, BTreeMap, BTreeSet};
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type TimeNpressure = u16;
type Valves<'a> = HashMap<String, Valve>;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Valve {
    name: String,
    flow_rate: u8,
    valves_following: Vec<String>,
}

/*
    This problem involves using the Floyd-Warshall algorithm
        https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
        https://www.khanacademy.org/computing/computer-science/algorithms/graph-representation/a/describing-graphs
        https://www.youtube.com/results?search_query=Floyd-Warshall

    Nodes = one vertex, many vertices
    Path between vertices = edge
    Edges
        may be "weighted"
        may be undirected, the edge can be followed both ways
        may be directed, the edge can be followed one way only
            the edge 'leaves' one vertex and 'enters' another
    Undirected graph
        the number of edges connected to a vertex gives the vertex its 'degree'
    Directed graph
        the number of edges leaving a vertex is its 'out-degree'
        the number of edges entering is the 'in-degree'

    Pseudo code for Floyd-Warshall
        for each edge (u, v) do
            dist[u][v] ← w(u, v)  // The weight of the edge (u, v)
        for each vertex v do
            dist[v][v] ← 0
        for k from 1 to |V|
            for i from 1 to |V|
                for j from 1 to |V|
                    if dist[i][j] > dist[i][k] + dist[k][j]
                        dist[i][j] ← dist[i][k] + dist[k][j]
                    end if
*/

fn shortest_paths(valves: &Valves, keys: &[&String]) -> Vec<Vec<TimeNpressure>> {
    const INFINITY: TimeNpressure = TimeNpressure::MAX / 2;
    let keys_len = keys.len();
    // |V| × |V| array of minimum distances initialized to ∞ (infinity)
    let mut times_to_valves: Vec<Vec<TimeNpressure>> = vec![ vec![INFINITY; keys_len]; keys_len ];

    // Find edges and init the distance matrix for them (value 1)
    for (i, key) in keys.iter().enumerate() {
        for tv in valves[*key].valves_following.iter() {
            let j = keys.iter().position(|&s| s == tv).unwrap();
            times_to_valves[i][j] = 1;
        }
    }

    // Distance between one valve is of course zero
    for (i, ttv_iter) in times_to_valves.iter_mut().enumerate().take(keys_len) {
        ttv_iter[i] = 0;
    }

    // Find the shortest distances between all valves.
    for k in 0 .. keys_len {
        for i in 0 .. keys_len {
            for j in 0 .. keys_len {
                let dist = times_to_valves[i][k] + times_to_valves[k][j];
                if dist < times_to_valves[i][j] {
                    times_to_valves[i][j] = dist;
                }
            }
        }
    }

    times_to_valves
}

fn valves_with_flows<'a>(valves: &'a Valves) -> Vec<&'a Valve> {
    let mut flow_valves = Vec::new();

    for v in valves.values() {
        if v.flow_rate != 0 {
            flow_valves.push(v);
        }
    }
    flow_valves.sort_by(|a, b| a.name.partial_cmp(&b.name).unwrap());

    flow_valves
}

fn get_time_to_valve(from: &String, to: &String, times_to_valves: &[Vec<TimeNpressure>], keys: &[&String]) -> TimeNpressure {
    let i = keys.iter().position(|k| *k == from).unwrap();
    let j = keys.iter().position(|k| *k == to).unwrap();
    times_to_valves[i][j]
}

fn find_high_pressure(start_valve: &Valve, flow_valves: &Vec<&Valve>, times_to_valves: &Vec<Vec<TimeNpressure>>, keys: &Vec<&String>) -> TimeNpressure {

    fn next_pressure(from_valve: &Valve, fv_rem: &VecDeque<&&Valve>, times_to_valves: &Vec<Vec<TimeNpressure>>, mins_left: u8, keys: &Vec<&String>) -> TimeNpressure {
        let mut h_press = 0;

        for fv in fv_rem.iter() {
            let times_to_valve = get_time_to_valve(&from_valve.name, &fv.name, times_to_valves, keys);
            if let Some(minutes_left) = mins_left.checked_add_signed(-(times_to_valve as i8) - 1) { // -1 for opening the valve
                let mut fv_rem_nxt = fv_rem.clone();
                fv_rem_nxt.retain(|v| v != fv);
                let this_press = minutes_left as TimeNpressure * fv.flow_rate as TimeNpressure;
                let follow_press = next_pressure(fv, &fv_rem_nxt, times_to_valves, minutes_left, keys);
                let tot_press = this_press + follow_press;
                if tot_press > h_press {
                    h_press = tot_press;
                }
            } else {
                break;
            }
        }

        h_press
    }

    let mut high_press = 0;
    let mut flow_valves = VecDeque::from_iter(flow_valves);
    for _ in 0 .. flow_valves.len() {
        let hp = next_pressure(start_valve, &flow_valves, times_to_valves, 30, keys);
        if hp > high_press {
            high_press = hp;
        }

        flow_valves.rotate_left(1);
    }

    high_press
}

fn find_high_pressure_b(start_valve: &Valve, flow_valves: &Vec<&Valve>, times_to_valves: &Vec<Vec<TimeNpressure>>, keys: &Vec<&String>) -> TimeNpressure {
    type Stack = Vec<(String, TimeNpressure, u8)>;

    fn next_stack(from_valve: &Valve, fv_rem: &VecDeque<&&Valve>, times_to_valves: &Vec<Vec<TimeNpressure>>, used_time: u8, keys: &Vec<&String>, stack: &Stack, stack_map: &mut HashSet<Stack>) {
        for fv in fv_rem.iter() {
            let times_to_valve = get_time_to_valve(&from_valve.name, &fv.name, times_to_valves, keys);
            let used_time_nxt = used_time + times_to_valve as u8 + 1;
            if used_time_nxt >= 26 {
                continue;
            } else {
                let mut stack_nxt = stack.clone();
                stack_nxt.push((fv.name.clone(), times_to_valve, fv.flow_rate));
                // if stack_nxt.len() > 2 {
                    stack_map.insert(stack_nxt.clone());
                // }
                let mut fv_rem_nxt = fv_rem.clone();
                fv_rem_nxt.retain(|v| v != fv);
                next_stack(fv, &fv_rem_nxt, times_to_valves, used_time_nxt, keys, &stack_nxt, stack_map);
            }
        }
    }

    let mut stacks_map = HashSet::new();
    let mut flow_valves_vd = VecDeque::from_iter(flow_valves);
    for _ in 0 .. flow_valves_vd.len() {
        next_stack(start_valve, &flow_valves_vd, times_to_valves, 0, keys, &Vec::new(), &mut stacks_map);
        flow_valves_vd.rotate_left(1);
    }

    fn calc_press(stack_map: &Stack) -> TimeNpressure {
        let mut prev_flow = 0;
        let mut tot_time = 0;

        let press = stack_map.iter().fold(0, |p, (_, mv_time, flow)| {
            let use_tm = *mv_time + 1;
            tot_time += use_tm;
            let press = prev_flow * use_tm;
            prev_flow += *flow as TimeNpressure;
            p + press
        });

        press + prev_flow * (26 - tot_time)
    }

    let stacks_press_iter =
        stacks_map
            .iter()
            .map(|v| {
                let valves: BTreeSet<String> = v.iter().map(|s| s.0.clone() ).collect();
                (valves, calc_press(v))
            });

    let mut stacks_press = BTreeMap::new();
    for (sp, prs) in stacks_press_iter {
        stacks_press.entry(sp).and_modify(|p| *p = prs.max(*p)).or_insert(prs);
    }

    stacks_press
        .iter()
        .tuple_combinations()
        .filter(|(human, elephant)| human.0.is_disjoint(elephant.0))
        .map(|(human, elephant)| human.1 + elephant.1)
        .max()
        .unwrap()

}

fn task_a(lines: &[String]) {
    let valves = parse_input(lines);
    let mut keys: Vec<&String> = valves.keys().collect();
    keys.sort();

    let times_to_valves = shortest_paths(&valves, &keys);
    let flow_valves = valves_with_flows(&valves);

    let pressure = find_high_pressure(&valves[&"AA".to_string()], &flow_valves, &times_to_valves, &keys);

    println!("Most pressure released = {}", pressure);
}

fn task_b(lines: &[String]) {
    let valves = parse_input(lines);
    let mut keys: Vec<&String> = valves.keys().collect();
    keys.sort();

    let times_to_valves = shortest_paths(&valves, &keys);
    let flow_valves = valves_with_flows(&valves);

    let pressure = find_high_pressure_b(&valves[&"AA".to_string()], &flow_valves, &times_to_valves, &keys);
    println!("Most pressure released = {}", pressure);
}

fn parse_input(lines: &[String]) -> Valves {
    lines.iter().fold(Valves::new(), |mut valves, line| {
        let lspl: Vec<&str> = line.split(&[' ', '=', ';', ','][..]).collect();

        valves.insert(
            lspl[1].to_string(),
            Valve {
                name: lspl[1].to_string(),
                flow_rate: lspl[5].parse().unwrap(),
                valves_following: lspl[11..].iter().filter(|&&s| !s.is_empty()).map(|e| e.to_string()).collect()
            }
        );
        valves
    })
}

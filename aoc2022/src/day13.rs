// #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    type Integ = u8;
    pub type List<T> = Vec<T>;
    pub type Packets = List<Value>;

    #[derive(Clone, Debug, PartialEq)]
    pub enum Value {
        Int(Integ),
        List(List<Self>),
    }

    impl Value {
        pub fn new_int(v: Integ) -> Self {
            Self::Int(v)
        }

        pub fn new_list() -> Self {
            Self::List(List::new())
        }

        pub fn _new_list_w_val(v: &Self) -> Self {
            let mut l = Self::List(List::new());
            l.push(v.clone());
            l
        }

        pub fn push(&mut self, el: Value) {
            match self {
                Self::List(l) => l.push(el),
                Self::Int(_) => panic!("Bad list!"),
            }
        }

        pub fn get_list_values(&self) -> List<Self> {
            match self {
                Self::List(l) => l.clone(),
                Self::Int(_) => panic!("Bad list: {:?}!", self),
            }
        }

        pub fn _get_int_value(&self) -> u8 {
            match self {
                Self::Int(l) => *l,
                Self::List(_) => panic!("Bad value!"),
            }
        }

        pub fn _is_int_value(&self) -> bool {
            match self {
                Self::Int(_)  => true,
                Self::List(_) => false,
            }
        }
    }
}
use std::cmp::Ordering;

use internal::*;

#[derive(Debug, PartialEq)]
enum Order {
    Ok,
    NotOk,
    Draw,
}

fn compare(left: &Value, right: &Value) -> Order {
    let l_vals = left .get_list_values();
    let r_vals = right.get_list_values();

    let mut l_it = l_vals.iter();
    let mut r_it = r_vals.iter();

    loop {
        let l_nx = l_it.next();
        let r_nx = r_it.next();
        match (l_nx.is_none(), r_nx.is_none()) {
            (true, false) => return Order::Ok,
            (false, true) => return Order::NotOk,
            (true,  true) => return Order::Draw,
            _ => (),
        }

        let lf = l_nx.unwrap();
        let rt = r_nx.unwrap();

        match (lf, rt) {
            (Value::Int(l), Value::Int(r)) =>
                match l.cmp(r) {
                    Ordering::Less => return Order::Ok,
                    Ordering::Greater => return Order::NotOk,
                    Ordering::Equal => {},
                }
            (Value::List(_), Value::List(_)) => {
                let ret = compare(lf, rt);
                if ret != Order::Draw {
                    return ret
                }
            },
            _ => {
                let mut i_lst  = Value::new_list();
                let (lv, rv) =
                    match (lf, rt) {
                        (Value::Int(l), Value::List(_)) => {
                            i_lst.push(Value::new_int(*l));
                            (&i_lst, rt)
                        },
                        (Value::List(_), Value::Int(r)) => {
                            i_lst.push(Value::new_int(*r));
                            (lf, &i_lst)
                        },
                        _ => panic!("Bad (lf, rt): {:?}", (lf, rt))
                    };

                let ret = compare(lv, rv);
                if ret != Order::Draw {
                    return ret
                }
            },
        }
    }
}

fn task_a(lines: &[String]) {
    let packets = parse_input(lines);

    let mut pair_counter = 1;
    let mut sum_ok = 0;
    for q in packets.chunks(2) {
        if compare(&q[0], &q[1]) == Order::Ok {
            sum_ok += pair_counter;
        }

        pair_counter += 1;
    }

    println!("Sum indicies for correctly ordered packets = {}", sum_ok);
}

fn task_b(lines: &[String]) {
    let mut packets = parse_input(lines);
    let divider_packets = parse_input(&["[[2]]".to_string(), "[[6]]".to_string()]);
    packets.push(divider_packets[0].clone());
    packets.push(divider_packets[1].clone());

    packets.sort_unstable_by(|a, b|
        match compare(a, b) {
            Order::Ok    => Ordering::Less,
            Order::NotOk => Ordering::Greater,
            Order::Draw  => Ordering::Equal,
        }
    );

    let dp1 = packets.iter().position(|p| p == &divider_packets[0]).unwrap() + 1;
    let dp2 = packets.iter().position(|p| p == &divider_packets[1]).unwrap() + 1;

    println!("Decoder key = {}", dp1 * dp2);
}

fn parse_input(lines: &[String]) -> Packets {
    let mut packets = Packets::new();

    fn interpret_chars(ch_iter: &mut std::str::Chars<'_>) -> Value {
        let mut list = Value::new_list();
        let mut buf = String::new();

        while let Some(c) = ch_iter.next() {
            match c {
                '[' => {
                    let r_list = interpret_chars(ch_iter);
                    list.push(r_list);
                },
                ']' | ',' => {
                    if !buf.is_empty() {
                        let v = buf.parse().unwrap();
                        list.push(Value::new_int(v))
                    }

                    if c == ']' {
                        return list
                    }

                    buf = String::new();
                },
                num => buf.push(num),
            }
        }

        if !buf.is_empty() {
            let v = buf.parse().unwrap();
            list.push(Value::new_int(v))
        }

        list
    }

    for line in lines {
        if line.is_empty() {
            continue;
        }

        // Remove first and last bracket since they represent the packet itself
        let mut line_iter = line[1 .. line.len() - 1].chars();
        // let mut line_iter = line[0 .. line.len()].chars();
        let packet = interpret_chars(&mut line_iter);

        packets.push(packet);
    }

    packets
}

#[cfg(test)]
mod tests13 {
    use super::*;

    #[test]
    fn t_comp1() {
        let lines = &["[1,1,3,1,1]".to_string(), "[1,1,5,1,1]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::Ok);
    }

    #[test]
    fn t_comp2() {
        let lines = &["[[1],[2,3,4]]".to_string(), "[[1],4]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::Ok);
    }

    #[test]
    fn t_comp3() {
        let lines = &["[9]".to_string(), "[[8,7,6]]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::NotOk);
    }

    #[test]
    fn t_comp4() {
        let lines = &["[[4,4],4,4]".to_string(), "[[4,4],4,4,4]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::Ok);
    }

    #[test]
    fn t_comp5() {
        let lines = &["[7,7,7,7]".to_string(), "[7,7,7]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::NotOk);
    }

    #[test]
    fn t_comp6() {
        let lines = &["[]".to_string(), "[3]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::Ok);
    }

    #[test]
    fn t_comp7() {
        let lines = &["[[[]]]".to_string(), "[[]]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::NotOk);
    }

    #[test]
    fn t_comp8() {
        let lines = &["[1,[2,[3,[4,[5,6,7]]]],8,9]".to_string(), "[1,[2,[3,[4,[5,6,0]]]],8,9]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::NotOk);
    }

    #[test]
    fn t_comp_d() {
        let lines = &["[7,7,7]".to_string(), "[7,7,7]".to_string()];
        let pk = parse_input(lines);
        let ret = compare(&pk[0], &pk[1]);
        assert_eq!(ret, Order::Draw);
    }
}

use std::collections::HashMap;
use std::fmt;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Num = u64;
type Oper = fn(Num, Num) -> Num;

#[derive(Clone, Debug)]
enum MonkeyJob {
    Number(Num),
    Math(
        String,
        String,
        char,
        Oper,
        Oper,
    ),
}

struct Monkeys (HashMap<String, (MonkeyJob, bool)>);
impl fmt::Display for Monkeys {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fn fmt_one(mnks: &Monkeys, f: &mut fmt::Formatter, lev: usize, name: &String) -> fmt::Result {
            write!(f, "{}", " ".repeat(lev * 2))?;
            let monkey_job = &mnks.0[name];
            if let MonkeyJob::Number(num) = monkey_job.0 {
                writeln!(f, "{}: {}", name, num)
            } else {
                let MonkeyJob::Math(name_1, name_2, oper, ..) = monkey_job.clone().0 else { panic!() };
                let ret = writeln!(f, "{}: {} {} {}", name, name_1, oper, name_2);

                fmt_one(mnks, f, lev + 1, &name_1)?;
                fmt_one(mnks, f, lev + 1, &name_2)?;

                ret
            }
        }

        fmt_one(self, f, 0, &"root".to_string())
    }
}

fn get_number(monkeys: &Monkeys, name: &String) -> Num {
    let monkey_job = &monkeys.0[name];
    if let MonkeyJob::Number(num) = monkey_job.0 {
        num
    } else {
        let MonkeyJob::Math(name_1, name_2, _, oper, _) = monkey_job.clone().0 else { panic!() };
        let num_1 = get_number(monkeys, &name_1);
        let num_2 = get_number(monkeys, &name_2);

        oper(num_1, num_2)
    }
}

fn get_human_num(monkeys: &Monkeys, name: &String, branch_value: Num) -> Num {
    let monkey_job = &monkeys.0[name];
    if let MonkeyJob::Math(name_1, name_2, oper_c, oper, counter_oper) = monkey_job.clone().0 {
        let (human_branch, human_branch_value) =
        {
            if monkeys.0[&name_1].1 {
                let static_value = get_number(monkeys, &name_2);
                let humn_br_val = counter_oper(branch_value, static_value);
                (&name_1, humn_br_val)
            } else {
                let static_value = get_number(monkeys, &name_1);
                let humn_br_val =
                    match oper_c {
                        '+' | '*' => counter_oper(branch_value, static_value),
                        '-' | '/' => oper(static_value, branch_value),
                        _ => panic!()
                    };
                (&name_2, humn_br_val)
            }
        };

        get_human_num(monkeys, human_branch, human_branch_value)
    } else {
        // We've found humn
        let MonkeyJob::Number(_) = monkey_job.0 else { panic!() };

        branch_value
    }
}

fn find_humn(monkeys: &mut Monkeys, name: &String) {
    let is_humn =
        if let MonkeyJob::Number(_) = monkeys.0[name].0 {
            name == "humn"
        } else {
            let monkey_job = monkeys.0[name].clone();
            let MonkeyJob::Math(name_1, name_2, ..) = monkey_job.clone().0 else { panic!() };
            find_humn(monkeys, &name_1);
            find_humn(monkeys, &name_2);

            let MonkeyJob::Math(name_1, name_2, ..) = monkey_job.0 else { panic!() };

            let humn = |name| { monkeys.0.get(name).unwrap().1 };
            humn(&name_1) || humn(&name_2)
        };

    if is_humn {
        monkeys.0.get_mut(name).unwrap().1 = true;
    }
}

fn task_a(lines: &[String]) {
    let monkeys = parse_input(lines);

    let root_number = get_number(&monkeys, &"root".to_string());

    println!("Root will yell {}", root_number);
}

fn task_b(lines: &[String]) {
    let mut monkeys = parse_input(lines);
    let MonkeyJob::Math(name_1, name_2, ..) = monkeys.0[&"root".to_string()].clone().0 else { panic!() };

    find_humn(&mut monkeys, &name_1);
    let (human_branch, cmp_value) = {
        if monkeys.0[&name_1].1 {
            let cmp_value = get_number(&monkeys, &name_2);
            (&name_1, cmp_value)
        } else {
            let cmp_value = get_number(&monkeys, &name_1);
            (&name_2, cmp_value)
        }
    };

    let yell_number = if monkeys.0.len() > 22222222 {
        // Keep this hack since it was the first working solution :)
        let human = "humn".to_string();
        let mut low = get_number(&monkeys, &human);
        let mut high = 6626671253683;
        loop {
            let mean = low + (high - low) / 2;
            monkeys.0.entry(human.clone()).and_modify(|e| {
                let MonkeyJob::Number(num) = &mut e.0 else { panic!() };
                *num = mean;
            });
            let test_val = get_number(&monkeys, human_branch);

            if test_val == cmp_value {
                break mean
            }

            if test_val > cmp_value {
                low = mean;
            } else {
                high = mean
            }
        }
    } else {
        get_human_num(&monkeys, human_branch, cmp_value)
    };

    println!("You will yell {}", yell_number);
}


fn parse_input(lines: &[String]) -> Monkeys {
    use std::ops::{Add, Sub, Mul, Div};

    let monkeys = lines
        .iter()
        .map(|line| {
            let mut parts = line.split_ascii_whitespace();
            let name = parts.next().unwrap().trim_end_matches(':').to_string();
            let part_2 = parts.next().unwrap();
            let job = if let Ok(num) = part_2.parse() {
                (MonkeyJob::Number(num), false)
            } else {
                let oper_str = parts.next().unwrap();
                let moneky_2 = parts.next().unwrap();
                let (oper, counter_oper) =
                    match oper_str {
                        "+" => (Add::add as Oper, Sub::sub as Oper),
                        "-" => (Sub::sub as Oper, Add::add as Oper),
                        "*" => (Mul::mul as Oper, Div::div as Oper),
                        "/" => (Div::div as Oper, Mul::mul as Oper),
                        _ => panic!()
                    };
                (MonkeyJob::Math(part_2.to_string(), moneky_2.to_string(), oper_str.chars().next().unwrap(), oper, counter_oper), false)
            };

            (name, job)
        })
        .collect::<HashMap<_, _>>();

    Monkeys(monkeys)
}

#[cfg(test)]
mod tests21 {
    use super::*;
    use std::io::BufRead;
    use std::io::BufReader;

    fn get_input_from_file(day_num: &str) -> Vec<String> {
        let filename = format!("/home/ingemar/adventofcode/aoc2022/input/day-{}.txt", day_num);
        let file = std::fs::File::open(&filename).unwrap_or_else(|err| {
            eprintln!("Cannot open file '{}': {}", filename, err);
            std::process::exit(1);
        });

        let reader = BufReader::new(&file);
        let lines: Vec<String> = reader.lines().collect::<Result<_, _>>().unwrap();

        // The contents of 'lines' is moved out of this function, hence it will continue to live.
        lines
    }

    #[test]
    fn t01() {
        let lines = &get_input_from_file("21_sample-1");
        let monkeys = parse_input(lines);

        let sjmn = get_number(&monkeys, &"sjmn".to_string());
        println!("\nsjmn = {}", sjmn);
        let pppw = get_number(&monkeys, &"pppw".to_string());
        println!("pppw = {}", pppw);
    }

    #[test]
    fn t01_2() {
        let lines = &get_input_from_file("21_sample-2");
        let monkeys = parse_input(lines);

        let sjmn = get_number(&monkeys, &"sjmn".to_string());
        println!("\nsjmn = {}", sjmn);
        let pppw = get_number(&monkeys, &"pppw".to_string());
        println!("pppw = {}", pppw);
    }

    #[test]
    fn t02() {
        let lines = &get_input_from_file("21");
        let monkeys = parse_input(lines);

        let nfct = get_number(&monkeys, &"nfct".to_string());
        println!("\nnfct = {}", nfct);
        let rjmz = get_number(&monkeys, &"rjmz".to_string());
        println!("rjmz = {}", rjmz);
    }
}

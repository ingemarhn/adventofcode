use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Position = (usize, usize);
type Rock = Vec<Position>;
type Cave = HashMap<Position, PositionStates>;

#[derive(Debug, PartialEq)]
enum PositionStates {
    Air,
    Rock,
    Sand,
}

fn place_rocks_in_cave(rocks: &Vec<Rock>) -> (Cave, [Position; 2]) {
    let mut cave = Cave::new();

    let get_range = |lim1: usize, lim2, margin: usize| {
        if lim1 <= lim2 {
            lim1.max(margin) - margin ..= lim2 + margin
        } else {
            lim2.max(margin) - margin ..= lim1 + margin
        }
    };

    let mut limits = [(99999, 99999), (0, 0)];
    for rock in rocks {
        let mut rock_it = rock.windows(2);
        loop {
            let p = rock_it.next();
            if p.is_none() {
                break;
            }

            let pos = p.unwrap();
            if pos[0].0 < limits[0].0 { limits[0].0 = pos[0].0; }
            if pos[1].0 < limits[0].0 { limits[0].0 = pos[1].0; }
            if pos[0].1 < limits[0].1 { limits[0].1 = pos[0].1; }
            if pos[1].1 < limits[0].1 { limits[0].1 = pos[1].1; }
            if pos[0].0 > limits[1].0 { limits[1].0 = pos[0].0; }
            if pos[1].0 > limits[1].0 { limits[1].0 = pos[1].0; }
            if pos[0].1 > limits[1].1 { limits[1].1 = pos[0].1; }
            if pos[1].1 > limits[1].1 { limits[1].1 = pos[1].1; }

            for x in get_range(pos[0].0, pos[1].0, 0) {
                for y in get_range(pos[0].1, pos[1].1, 0) {
                    cave.entry((x, y)).or_insert(PositionStates::Rock);
                }
            }
        }
    }

    // Add floor
    for x in get_range(limits[0].0, limits[1].0, 1000) {
        cave.entry((x, limits[1].1 + 2)).or_insert(PositionStates::Rock);
    }

    for x in get_range(limits[0].0, limits[1].0, 1000) {
        for y in get_range(2, limits[1].1, 2) {
            cave.entry((x, y)).or_insert(PositionStates::Air);
        }
    }

    (cave, limits)
}

fn add_sand_unit(cave: &mut Cave, max_y: usize, end_when_abyss: bool) -> bool {
    let mut current_pos = (500, 0);

    loop {
        let (cp0, cp1) = current_pos;
        let mut next_pos = None;
        for pos in [(cp0, cp1 + 1), (cp0 - 1, cp1 + 1), (cp0 + 1, cp1 + 1)] {
            if cave[&pos] == PositionStates::Air {
                next_pos = Some(pos);
                break;
            }
        }

        if next_pos.is_none() {
            *cave.get_mut(&current_pos).unwrap() = PositionStates::Sand;
            return end_when_abyss || current_pos != (500, 0);
        }

        if next_pos.unwrap().1 > max_y {
            return  false;
        }

        current_pos = next_pos.unwrap();
    };
}

fn task_a(lines: &[String]) {
    let rocks = parse_input(lines);
    let (mut cave, limits) = place_rocks_in_cave(&rocks);

    let mut sand_count = 0;
    while add_sand_unit(&mut cave, limits[1].1, true) {
        sand_count += 1;
    }

    println!("# of sand units = {}", sand_count);
}

fn task_b(lines: &[String]) {
    let rocks = parse_input(lines);
    let (mut cave, limits) = place_rocks_in_cave(&rocks);

    let mut sand_count = 0;
    while add_sand_unit(&mut cave, limits[1].1 + 999, false) {
        sand_count += 1;
    }

    println!("# of sand units = {}", sand_count + 1);
}

fn parse_input(lines: &[String]) -> Vec<Rock> {
    lines
        .iter()
        .map(|line|
            line
            .split(" -> ")
            .map(|pos| {
                let xy: Vec<&str> = pos.split(',').collect();
                let x = xy[0].parse().unwrap();
                let y = xy[1].parse().unwrap();
                (x, y)
            })
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Position = (isize, isize);
type Sensors = Vec<Sensor>;
type Cave = HashMap<Position, char>;

#[derive(Debug)]
struct Sensor {
    pos: Position,
    beacon_pos: Position,
    distance: isize,
}

fn define_cave(sensors: &Sensors, y_row: isize) -> Cave {
    let mut cave = Cave::new();

    for sen in sensors {
        cave.entry(sen.pos).or_insert('S');
        cave.entry(sen.beacon_pos).or_insert('B');

        let dist_from_y_row = (y_row - sen.pos.1).abs();
        if dist_from_y_row > sen.distance {
            continue;
        }

        let dist_from_x_col = sen.distance - dist_from_y_row;
        for x in sen.pos.0 - dist_from_x_col ..= sen.pos.0 + dist_from_x_col {
            cave.entry((x, y_row)).or_insert('#');
        }
    }

    cave
}

fn find_tuning_freq(sensors: &Sensors) -> isize {
    const MAX_X_Y: isize = 4_000_000;

    for y in 0 ..= MAX_X_Y {
        let mut ranges = Vec::new();

        for sen in sensors {
            let dist_from_y_row = (y - sen.pos.1).abs();
            if dist_from_y_row > sen.distance {
                continue;
            }

            let dist_from_x_col = sen.distance - dist_from_y_row;
            let mut from_x = sen.pos.0 - dist_from_x_col;
            let mut to_x = sen.pos.0 + dist_from_x_col;
            if from_x > MAX_X_Y || to_x < 0 {
                continue;
            }

            if from_x < 0 {
                from_x = 0;
            }
            if to_x > MAX_X_Y {
                to_x = MAX_X_Y;
            }
            ranges.push((from_x, to_x));
        }

        ranges.sort_by(|a, b| a.0.cmp(&b.0));
        let mut mod_i = 0;
        loop {
            let src_i = mod_i + 1;
            if  src_i >= ranges.len() {
                break;
            }
            let (sr0, sr1) = ranges[src_i];
            let r = ranges.get_mut(mod_i).unwrap();
            if sr0 <= r.1 {
                if sr1 > r.1 {
                    r.1 = sr1;
                }
                ranges.remove(src_i);
            } else {
                mod_i += 1;
            }
        }

        if ranges.len() != 1 {
            return (ranges[1].0 - 1) * MAX_X_Y + y;
        }
    }

    panic!("Not expected...")
}

fn task_a(lines: &[String]) {
    const Y_ROW: isize = 2_000_000;

    let sensors = parse_input(lines);

    let cave = define_cave(&sensors, Y_ROW);
    let keys = cave.keys().filter(|k| k.1 == Y_ROW && cave[k] == '#');

    println!("{} positions cannot contain a beacon", keys.count());
}

fn task_b(lines: &[String]) {
    let sensors = parse_input(lines);

    let freq = find_tuning_freq(&sensors);

    println!("Tuning frequency = {}", freq);
}

fn parse_input(lines: &[String]) -> Sensors {
    let mut sensors = Sensors::new();
    let pars = |num: &str| {
        num.parse().unwrap()
    };

    for line in lines {
        let line_parts: Vec<&str> =
            line
            .split(|c| "=,:".contains(c))
            .collect();
        let mut sensor = Sensor { pos: (pars(line_parts[1]), pars(line_parts[3])), beacon_pos: (pars(line_parts[5]), pars(line_parts[7])), distance: 0 };
        sensor.distance = (sensor.pos.0 - sensor.beacon_pos.0).abs() + (sensor.pos.1 - sensor.beacon_pos.1).abs();

        sensors.push(sensor);
    }

    sensors
}

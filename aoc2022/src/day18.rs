pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::BTreeMap;

    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
    pub struct Cube([i8; 3]);

    impl Cube {
        pub fn new(x: i8, y: i8, z: i8) -> Self {
            Self([x, y, z])
        }

        pub fn get_x(&self) -> i8 {
            self.0[0]
        }
        pub fn get_y(&self) -> i8 {
            self.0[1]
        }
        pub fn get_z(&self) -> i8 {
            self.0[2]
        }
    }

    impl FromIterator<i8> for Cube {
        fn from_iter<I: IntoIterator<Item=i8>>(iter: I) -> Self {
            let mut cube = Cube([0; 3]);

            for (i, ci) in iter.into_iter().enumerate() {
                cube.0[i] = ci;
            }

            cube
        }
    }

    #[derive(Debug, Clone)]
    pub struct Cubes(BTreeMap<Cube, u8>);

    impl Cubes {
        pub fn new() -> Self {
            Self(BTreeMap::new())
        }
        pub fn len(&self) -> usize {
            self.0.len()
        }

        pub fn insert(&mut self, key: &Cube, v: u8) {
            self.0.insert(*key, v);
        }

        pub fn iter(&self) -> std::collections::btree_map::Iter<'_, Cube, u8> {
            self.0.iter()
        }

        pub fn keys(&self) -> std::collections::btree_map::Keys<'_, Cube, u8> {
            self.0.keys()
        }

        pub fn find_neighbor(&mut self, key: &Cube) {
            let Cube([x, y, z]) = *key;
            let mut nbr = |nx, ny, nz| {
                let t_cube = Cube([nx, ny, nz]);
                if self.0.contains_key(&t_cube) {
                    self.0.entry(*key).and_modify(|e| { *e += 1 });
                }
            };

            nbr(x - 1, y, z);
            nbr(x + 1, y, z);
            nbr(x, y - 1, z);
            nbr(x, y + 1, z);
            nbr(x, y, z - 1);
            nbr(x, y, z + 1);
        }

        pub fn sum_connections(&self) -> usize {
            self
                .iter()
                .fold(0, |sum, (_, e)| {
                    sum + *e as usize
                })
        }

        pub fn sum_air_connections(&self, air_cubes: &Cubes) -> usize {
            let is_air_cube = |x, y, z| {
                if air_cubes.0.contains_key(&Cube([x, y, z])) { 1 } else { 0 }
            };
            self
                .iter()
                .fold(0, |sum, (&c, _)| {
                    let Cube([x, y, z]) = c;
                    sum +
                    (
                        is_air_cube(x + 1, y, z) +
                        is_air_cube(x - 1, y, z) +
                        is_air_cube(x, y + 1, z) +
                        is_air_cube(x, y - 1, z) +
                        is_air_cube(x, y, z + 1) +
                        is_air_cube(x, y, z - 1)
                    ) as usize
                })
        }

        pub fn fill_air_cubes(&self, low_hgh: &[(i8, i8); 3]) -> Cubes {
            let mut air_cubes = Cubes::new();

            for x in low_hgh[0].0 + 1 .. low_hgh[0].1 {
                for y in low_hgh[1].0 + 1 .. low_hgh[1].1 {
                    for z in low_hgh[2].0 + 1 .. low_hgh[2].1 {
                        let t_cube = Cube::new(x, y, z);
                        if !self.0.contains_key(&t_cube) {
                            air_cubes.0.insert(t_cube, 1);
                        }
                    }
                }
            }

            air_cubes
        }

        pub fn remove_unconnected(&mut self, lava_cubes: &Cubes) {
            let mut prev_len = self.0.len();

            loop {
                let self_clone = self.0.clone();
                self.0.retain(|c, _| {
                    let Cube([x, y, z]) = *c;

                    let cubes =
                        (0 .. 3)
                            .fold(Cubes::new(), |mut cubs, ind| {
                                for i in [-1, 1] {
                                    let mut cb = [x, y, z];
                                    cb[ind] += i;
                                    let [xm, ym, zm] = cb;
                                    cubs.insert(&Cube::new(xm, ym, zm), 0)
                                }
                                cubs
                            });

                    cubes.0
                        .into_iter()
                        .fold(true, |keep, (cub, _)| {
                            keep && (self_clone.contains_key(&cub) || lava_cubes.0.contains_key(&cub))
                        })
                });

                if self.len() == prev_len {
                    break;
                }

                prev_len = self.len();
            }
        }
    }

    impl FromIterator<Cube> for Cubes {
        fn from_iter<I>(iter: I) -> Self
        where I: IntoIterator<Item=Cube>
        {
            let mut cubes = Cubes(BTreeMap::new());

            for ci in iter {
                cubes.0
                    .entry(ci)
                    .and_modify(|e| { *e += 1 })
                    .or_insert(0);
            }

            cubes
        }
    }
}

use internal::*;

fn task_a(lines: &[String]) {
    let mut cubes = parse_input(lines);

    for cube in cubes.clone().keys() {
        cubes.find_neighbor(cube);
    }

    let n_tot_sides = cubes.len() * 6;
    let n_connections = cubes.sum_connections();

    println!("Lava droplet surface area = {}", n_tot_sides - n_connections);
}

fn task_b(lines: &[String]) {
    let mut lava_cubes = parse_input(lines);

    for cube in lava_cubes.clone().keys() {
        lava_cubes.find_neighbor(cube);
    }

    let low_high =
        lava_cubes
            .iter()
            .fold([(99,0), (99,0), (99,0)], |xtrms, (c, _)| {
                let x = c.get_x();
                let y = c.get_y();
                let z = c.get_z();

                [
                    (xtrms[0].0.min(x), xtrms[0].1.max(x)),
                    (xtrms[1].0.min(y), xtrms[1].1.max(y)),
                    (xtrms[2].0.min(z), xtrms[2].1.max(z)),
                ]
            });

    let mut air_cubes = lava_cubes.fill_air_cubes(&low_high);
    air_cubes.remove_unconnected(&lava_cubes);

    let n_tot_sides = lava_cubes.len() * 6;
    let n_connections = lava_cubes.sum_connections();
    let n_air_connections = lava_cubes.sum_air_connections(&air_cubes);

    println!("Lava droplet surface area = {}", n_tot_sides - n_connections - n_air_connections);
}

fn parse_input(lines: &[String]) -> Cubes {
    lines
        .iter()
        .map(|line|
            line
            .split(',')
            .map(|p_elem|
                p_elem
                .parse()
                .unwrap()
            )
            .collect::<Cube>()
        )
        .collect::<Cubes>()
}


#[cfg(test)]
mod tests18 {
    use super::*;

    #[test]
    fn t1() {
        let cubs =
            (0 .. 3)
                .fold(Cubes::new(), |mut cubes, ind| {
                    for i in [-1, 1] {
                        let mut cb = [0; 3];
                        cb[ind] = i;
                        let [x, y, z] = cb;
                        cubes.insert(&Cube::new(x, y, z), 0)
                    }
                    cubes
                });
        println!("cubs = {:?}", cubs);
    }
}

use std::collections::{VecDeque, HashSet};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    type Neighbors = [(Position, u8); 4];
    type Matrix<T> = Vec<Vec<T>>;

    #[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
    pub struct Position(usize, usize);

    #[derive(Clone, Debug)]
    pub struct HeightMap {
        map: Matrix<u8>,
        start: Position,
        end: Position,
        distances: Matrix<u16>,
    }

    impl HeightMap {
        pub fn new(lines: &[String]) -> Self {
            let mut start = Position(0, 0);
            let mut end = Position(0, 0);
            let map: Matrix<u8> = lines
                .iter()
                .enumerate()
                .map(|(r, line)|
                    line
                    .chars()
                    .enumerate()
                    .map(|(c, ch)| {
                        if ch == 'S' {
                            start = Position(r, c);
                            b'a'
                        } else if ch == 'E' {
                            end = Position(r, c);
                            b'z'
                        } else {
                            ch as u8
                        }
                    })
                    .collect::<Vec<_>>()
                )
                .collect();
            let row_distances = vec![0; map[0].len()];
            let distances = vec![row_distances; map.len()];

            Self { map, start, end, distances }
        }

        pub fn get_start_position(&self) -> Position {
            self.start
        }

        pub fn get_end_position(&self) -> Position {
            self.end
        }

        pub fn get_neighbors(&self, Position(r, c): &Position) -> Neighbors {
            let unreachable = Position(usize::MAX, usize::MAX);
            let mut neighbrs = [(unreachable, u8::MAX); 4];
            if *r > 0 {
                neighbrs[0] = (Position(*r - 1, *c), self.get_level(&Position(*r - 1, *c)));
            }
            if *r < self.map.len() - 1 {
                neighbrs[2] = (Position(*r + 1, *c), self.get_level(&Position(*r + 1, *c)));
            }
            if *c > 0 {
                neighbrs[1] = (Position(*r, *c - 1), self.get_level(&Position(*r, *c - 1)));
            }
            if *c < self.map[0].len() - 1 {
                neighbrs[3] = (Position(*r, *c + 1), self.get_level(&Position(*r, *c + 1)));
            }

            neighbrs
        }

        pub fn get_level(&self, Position(r, c): &Position) -> u8 {
            self.map[*r][*c]
        }

        pub fn set_distance(&mut self, base: Position, this: Position) {
            self.distances[this.0][this.1] = self.distances[base.0][base.1] + 1;
        }

        pub fn get_distance(&self, this: Position) -> u16 {
            self.distances[this.0][this.1]
        }

        pub fn get_all_pos_for_char(&self, ch: char) -> Vec<Position> {
            let mut positions = Vec::new();
            let chv = ch as u8;

            for (r, cols) in self.map.iter().enumerate() {
                for (c, cv) in cols.iter().enumerate() {
                    if chv == *cv {
                        positions.push(Position(r, c));
                    }
                }
            }

            positions
        }
    }
}
use internal::*;

// fn find_end(height_map: &HeightMap) -> HashSet<Position> {
fn find_end(height_map: &mut HeightMap, start: Position) -> u16 {
    let mut visited = HashSet::new();
    let mut queue = VecDeque::new();

    let end = height_map.get_end_position();

    queue.push_back(start);
    visited.insert(start);

    while !queue.is_empty() {
        let current_pos = queue.pop_front().unwrap();
        if current_pos == end {
            return height_map.get_distance(current_pos);
        }

        let neighbors = height_map.get_neighbors(&current_pos);
        for (nbr, lev) in neighbors {
            if !visited.contains(&nbr) && height_map.get_level(&current_pos) + 1 >= lev {
                queue.push_back(nbr);
                visited.insert(nbr);
                height_map.set_distance(current_pos, nbr);
            }
        }
    }

    u16::MAX
}


fn task_a(lines: &[String]) {
    let mut height_map = parse_input(lines);

    let start = height_map.get_start_position();
    let n_steps = find_end(&mut height_map, start);

    println!("Fewest steps from current location to target = {}", n_steps);
}

fn task_b(lines: &[String]) {
    let height_map = parse_input(lines);

    let mut min_steps = u16::MAX;
    let a_pos = height_map.get_all_pos_for_char('a');
    for pos in a_pos {
        let mut h_map = height_map.clone();
        let n_steps = find_end(&mut h_map, pos);
        if n_steps < min_steps {
            min_steps = n_steps;
        }
    }

    println!("Fewest steps from any location to target = {}", min_steps);
}

fn parse_input(lines: &[String]) -> HeightMap {
    HeightMap::new(lines)
}

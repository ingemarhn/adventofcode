use std::str::FromStr;
use regex::{Regex, Captures};

use common::math::triangular_number;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::HashMap;
    use std::fmt;

    #[repr(u8)]
    #[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, PartialOrd, Ord)]
    pub enum Material {
        Ore,
        Clay,
        Obsidian,
        Geode,
    }

    type Ore = u16;
    type Clay = u16;
    type Obsidian = u16;

    #[derive(Clone, Copy, Debug)]
    pub struct Blueprint {
        id: u8,
        ore_robot_cost: Ore,
        clay_robot_cost: Ore,
        obsidian_robot_cost: (Ore, Clay),
        geode_robot_cost: (Ore, Obsidian),
        max_ore_cost: Ore,
        max_clay_cost: Ore,
        max_obsidian_cost: Ore,
    }

    impl Blueprint {
        pub fn new(id: u8, ore_robot_cost: Ore, clay_robot_cost: Ore, obsidian_robot_cost_1: Ore, obsidian_robot_cost_2: Clay, geode_robot_cost_1: Ore, geode_robot_cost_2: Obsidian) -> Self {
            Self {
                id,
                ore_robot_cost,
                clay_robot_cost,
                obsidian_robot_cost: (obsidian_robot_cost_1, obsidian_robot_cost_2),
                geode_robot_cost: (geode_robot_cost_1, geode_robot_cost_2),
                max_ore_cost: ore_robot_cost.max(clay_robot_cost).max(obsidian_robot_cost_1).max(geode_robot_cost_1),
                max_clay_cost: obsidian_robot_cost_2,
                max_obsidian_cost: geode_robot_cost_2,
            }
        }

        pub fn id(&self) -> u8 {
            self.id
        }

        #[cfg(test)]
        pub fn robot_cost(&self, mtrl: &Material) -> (Ore, Ore) {
            match *mtrl {
                Material::Ore => (self.ore_robot_cost, 0),
                Material::Clay => (self.clay_robot_cost, 0),
                Material::Obsidian => self.obsidian_robot_cost,
                Material::Geode => self.geode_robot_cost,
            }
        }
    }

    #[derive(Debug)]
    pub struct Blueprints(Vec<Blueprint>);

    impl Blueprints {
        pub fn get(&self, ind: usize) -> &Blueprint {
            &self.0[ind]
        }

        pub fn len(&self) -> usize {
            self.0.len()
        }

        pub fn iter(&mut self) -> std::slice::Iter<'_, Blueprint> {
            self.0.iter()
        }
    }

    impl FromIterator<Blueprint> for Blueprints {
        fn from_iter<I>(iter: I) -> Self
        where I: IntoIterator<Item=Blueprint>
        {
            let mut blueprints = Blueprints(Vec::new());

            for ci in iter {
                blueprints.0.push(ci);
            }

            blueprints
        }
    }

    pub type RobotCount = u16;
    pub type MtrlCount = u16;
    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Possessions {
        robots: HashMap<Material, RobotCount>,
        count: HashMap<Material, MtrlCount>,
        factory_working: HashMap<Material, bool>,
    }

    impl Possessions {
        pub fn new() -> Self {
            Self {
                robots:HashMap::from([(Material::Ore, 1)]),
                count: HashMap::new(),
                factory_working: HashMap::new(),
            }
        }

        pub fn _new_values(bots: &[(Material, RobotCount)], counts: &[(Material, MtrlCount)]) -> Self {
            let mut possesion = Self {
                robots: HashMap::new(),
                count:  HashMap::new(),
                factory_working: HashMap::new(),
            };

            for b in bots {
                possesion.robots.insert(b.0, b.1);
            }
            for c in counts {
                possesion.count.insert(c.0, c.1);
            }

            possesion
        }

        fn add_count(&mut self, mtrl: Material, count: MtrlCount) {
            self.mod_count(mtrl, std::primitive::u16::checked_add, count);
        }

        fn sub_count(&mut self, mtrl: Material, count: MtrlCount) {
            self.mod_count(mtrl, std::primitive::u16::checked_sub, count);
        }

        fn mod_count(&mut self, mtrl: Material, mod_func: fn(MtrlCount, MtrlCount) -> Option<MtrlCount>, count: MtrlCount) {
            self
                .count
                .entry(mtrl)
                .and_modify(|e| { *e = mod_func(*e, count).unwrap() })
                .or_insert(count);
        }

        pub fn collect_material(&mut self) {
            for (rob, count) in self.clone().robots {
                self.add_count(rob, count);
            }
        }

        pub fn init_create_robot(&mut self, mtrl: &Material, blueprt: &Blueprint) {
            if self.is_factory_active(mtrl) {
                panic!("Fuuck! Factory for {:?} is already working", mtrl);
            }

            self.mod_factory_status(mtrl, true);

            match *mtrl {
                Material::Ore => {
                    self.sub_count(Material::Ore, blueprt.ore_robot_cost);
                },
                Material::Clay => {
                    self.sub_count(Material::Ore, blueprt.clay_robot_cost);
                },
                Material::Obsidian => {
                    self.sub_count(Material::Ore, blueprt.obsidian_robot_cost.0);
                    self.sub_count(Material::Clay, blueprt.obsidian_robot_cost.1);
                },
                Material::Geode => {
                    self.sub_count(Material::Ore, blueprt.geode_robot_cost.0);
                    self.sub_count(Material::Obsidian, blueprt.geode_robot_cost.1);
                },
            }
        }

        pub fn finish_create_robot(&mut self, mtrl: &Material) {
            if !self.is_factory_active(mtrl) {
                panic!("Fuuck! Factory for {:?} isn't working", mtrl);
            }

            self.mod_factory_status(mtrl, false);

            self
                .robots
                .entry(*mtrl)
                .and_modify(|e| { *e += 1 })
                .or_insert(1);
        }

        fn is_factory_active(&mut self, mtrl: &Material) -> bool {
            match self.factory_working.get(mtrl) {
                Some(is_active) => *is_active,
                None => false,
            }
        }

        fn mod_factory_status(&mut self, mtrl: &Material, status: bool) {
            self
                .factory_working
                .entry(*mtrl)
                .and_modify(|e| { *e = status })
                .or_insert(status);

        }

        pub fn can_and_need_build_robot(&self, rob: &Material, blueprt: &Blueprint) -> bool {
            match *rob {
                Material::Ore =>        self.material_count(&Material::Ore) >= blueprt.ore_robot_cost &&
                                        self.robots_count(rob) < blueprt.max_ore_cost,
                Material::Clay =>       self.material_count(&Material::Ore) >= blueprt.clay_robot_cost &&
                                        self.robots_count(rob) < blueprt.max_clay_cost,
                Material::Obsidian =>   self.material_count(&Material::Ore) >= blueprt.obsidian_robot_cost.0 &&
                                        self.material_count(&Material::Clay) >= blueprt.obsidian_robot_cost.1 &&
                                        self.robots_count(rob) < blueprt.max_obsidian_cost,
                Material::Geode =>      self.material_count(&Material::Ore) >= blueprt.geode_robot_cost.0 &&
                                        self.material_count(&Material::Obsidian) >= blueprt.geode_robot_cost.1,
            }
        }

        pub fn robots_count(&self, rob: &Material) -> RobotCount {
            match self.robots.get(rob) {
                Some(cnt) => *cnt,
                None => 0
            }
        }

        pub fn material_count(&self, mtrl: &Material) -> MtrlCount {
            match self.count.get(mtrl) {
                Some(cnt) => *cnt,
                None => 0
            }
        }
    }

    impl fmt::Display for Possessions {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            writeln!(f, "Ore Clay Obs. Geo.")?;
            let bot_str = format!("{:3}{:5}{:5}{:5}",
                self.robots_count(&Material::Ore),
                self.robots_count(&Material::Clay),
                self.robots_count(&Material::Obsidian),
                self.robots_count(&Material::Geode),
            );
            let pos_str = format!("{:3}{:5}{:5}{:5}",
                self.material_count(&Material::Ore),
                self.material_count(&Material::Clay),
                self.material_count(&Material::Obsidian),
                self.material_count(&Material::Geode),
            );
            write!(f, "{bot_str}\n{pos_str}")
        }
    }

    #[derive(Clone, Debug, Eq)]
    pub struct State {
        minutes: u8,
        possessions: Possessions,
    }

    impl State {
        pub fn new( minutes: u8, possessions: Possessions,) -> Self {
            Self { minutes, possessions }
        }

        pub fn get(&self) -> (u8, Possessions) {
            (self.minutes, self.possessions.clone())
        }
    }

    use std::hash::{Hash, Hasher};

    impl Hash for Possessions {
        fn hash<H: Hasher>(&self, state: &mut H) {
            let mut k: Vec<_> = self.robots.keys().collect();
            k.sort();
            for r in k.iter() {
                r.hash(state);
            }
            k = self.count.keys().collect();
            k.sort();
            for c in k.iter() {
                c.hash(state);
            }
            k = self.factory_working.keys().collect();
            k.sort();
            for f in k.iter() {
                f.hash(state);
            }
        }
    }

    impl Hash for State {
        fn hash<H: Hasher>(&self, state: &mut H) {
            self.minutes.hash(state);
            self.possessions.hash(state);
        }
    }

    impl PartialEq for State {
        fn eq(&self, other: &Self) -> bool {
            self.minutes            == other.minutes &&
            self.possessions.robots == other.possessions.robots &&
            self.possessions.count  == other.possessions.count
        }
    }
}
use internal::*;

fn run_blueprint(bp: &Blueprint, max_minutes: u8) -> u16 {
    let mut max_geodes = 0;

    let mut stack = Vec::new();
    stack.push(State::new(max_minutes, Possessions::new()));

    while let Some(state) = stack.pop() {
        let (minutes_left, mut possessions) = state.get();

        {
            let theroretical_geodes = possessions.material_count(&Material::Geode) +
                                    possessions.robots_count(&Material::Geode) * minutes_left as u16 +
                                    triangular_number(minutes_left as u16 - 1);
            if theroretical_geodes <= max_geodes {
                continue;
            }
        }

        if minutes_left > 1 {
            let mut bots_to_build = Vec::new();

            if possessions.can_and_need_build_robot(&Material::Geode, bp) {
                bots_to_build.push(Material::Geode);
            } else {
                if minutes_left > 2 && possessions.can_and_need_build_robot(&Material::Obsidian, bp) {
                    bots_to_build.push(Material::Obsidian);
                }
                if minutes_left > 3 && possessions.can_and_need_build_robot(&Material::Clay, bp) {
                    bots_to_build.push(Material::Clay);
                }
                if minutes_left > 2 && possessions.can_and_need_build_robot(&Material::Ore, bp) {
                    bots_to_build.push(Material::Ore);
                }
            }

            {
                let mut possessions_nxt = possessions.clone();
                possessions_nxt.collect_material();
                stack.push(State::new(minutes_left - 1, possessions_nxt));
            }

            for bot in &bots_to_build {
                let mut possessions_nxt = possessions.clone();

                possessions_nxt.init_create_robot(bot, bp);
                possessions_nxt.collect_material();
                possessions_nxt.finish_create_robot(bot);

                stack.push(State::new(minutes_left - 1, possessions_nxt));
            }
        } else {
            possessions.collect_material();
            max_geodes = max_geodes.max(possessions.material_count(&Material::Geode));
        }
    }

    max_geodes
}

fn task_a(lines: &[String]) {
    let blueprints = parse_input(lines);

    let mut quality_level = 0;
    for i in 0 .. blueprints.len() {
        let bp = blueprints.get(i);
        let n_geodes = run_blueprint(bp, 24);
        quality_level += bp.id() as u64 * n_geodes as u64;
    }

    println!("quality_level = {}", quality_level);
}

fn task_b(lines: &[String]) {
    let mut blueprints = parse_input(lines);
    let geodes_prod: u32 = blueprints
        .iter()
        .take(3)
        .map(|bp| run_blueprint(bp, 32) as u32)
        .product()
        ;

    println!("Product of #geodes = {:?}", geodes_prod);
}

// Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
fn parse_input(lines: &[String]) -> Blueprints {
    use std::fmt::Debug;
    let re = Regex::new(
        r"Blueprint (?P<bpi>\d+): Each ore robot costs (?P<or_c>\d+) ore. Each clay robot costs (?P<cl_c>\d+) ore. Each obsidian robot costs (?P<ob_c_o>\d+) ore and (?P<ob_c_c>\d+) clay. Each geode robot costs (?P<ge_c_o>\d+) ore and (?P<ge_c_ob>\d+) obsidian."
    )
    .unwrap();

    fn get_re_match<T>(caps: &Captures, match_name: &str) -> T
    where T: FromStr + Ord,
          <T as FromStr>::Err: Debug
    {
        caps.name(match_name).unwrap().as_str().parse().unwrap()
    }

    lines
        .iter()
        .map(|line| {
            let caps = re.captures(line).unwrap();

            Blueprint::new(
                get_re_match(&caps, "bpi"),
                get_re_match(&caps, "or_c"),
                get_re_match(&caps, "cl_c"),
                get_re_match(&caps, "ob_c_o"),
                get_re_match(&caps, "ob_c_c"),
                get_re_match(&caps, "ge_c_o"),
                get_re_match(&caps, "ge_c_ob"),
            )
        })
        .collect()
}

#[cfg(test)]
mod tests19 {
    use super::*;
    use Material::*;

    const BLPRT: &str = "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.";

    #[test]
    fn t01() {
        let blprts = parse_input(&[BLPRT.to_owned()]);
        let blprt0 = *blprts.get(0);

        let mut poss = Possessions::new(); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Ore, &blprt0) = {}", poss.can_and_need_build_robot(&Ore, &blprt0));
        poss.collect_material(); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
        poss.collect_material(); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
        println!("create Clay robot");
        poss.init_create_robot(&Clay, &blprt0); println!("poss = {:?}", poss);
        poss.finish_create_robot(&Clay); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
        poss.collect_material(); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
        poss.collect_material(); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
        println!("create Clay robot");
        poss.init_create_robot(&Clay, &blprt0); println!("poss = {:?}", poss);
        poss.finish_create_robot(&Clay); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
        poss.collect_material(); println!("poss = {:?}", poss);
        println!("poss.can_buy_robot(&Clay, &blprt0) = {}", poss.can_and_need_build_robot(&Clay, &blprt0));
    }
}

#[cfg(test)]
mod tests19b {
    use super::*;
    use common::*;
    use itertools::Itertools;

    const BLPRT: &str = "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.";

    #[test]
    fn t01() {
            use Material::*;
        let blprts = parse_input(&[BLPRT.to_owned()]);
        let bp = *blprts.get(0);
        let mtrls = {
            let or = vec![Ore; bp.robot_cost(&Ore).0 as usize];
            let cl = vec![Clay; bp.robot_cost(&Clay).0 as usize];
            let ob = vec![Obsidian; bp.robot_cost(&Obsidian).1 as usize];
            // let mtrls = vec![or, cl, ob ].iter().flatten();
            [or, cl, ob ]
                .iter()
                .flatten()
                .copied()
                .collect_vec()
        };
// let mtrls = vec![1,2,3,3,5,1,2,3,3,5,1,2,3,3,5,1,2,3,3,5,];
        let k = mtrls.len();
        let mtrl_permutations = mtrls.iter().permutations_unique(k);
        println!("mtrls.len() = {}", mtrls.len());
        println!("mtrl_permutations.len() = {}", mtrl_permutations.clone().count());
        let t = start_timer();
        for p in mtrl_permutations {
            println!("{:?}", p);
        }
        report_timer(t);
    }
}

#[cfg(test)]
mod tests19c {
    // use super::*;
    use common::*;

    #[test]
    fn t01() {
        for i in 1..=17 {
            let tup = math::quadratic_equation_pq(1, -i * 2);
            println!("{i:2} {:?} - {}", tup, tup.0.ceil());
        }
    }
}

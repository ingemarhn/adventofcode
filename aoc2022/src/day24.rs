pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    pub type Pos = (usize, usize);
    pub type Map = Vec<Vec<Tile>>;

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub enum Blizzard {
        Up,
        Down,
        Left,
        Right,
    }

    #[derive(Clone, Debug, PartialEq)]
    pub enum Tile {
        Wall,
        FreeGround,
        Blizzards(Vec<Blizzard>),
    }

    #[derive(Clone, Debug, PartialEq)]
    pub struct FieldMap {
        map: Map,
        e_position: Pos,
        exit: Pos,
    }

    impl FieldMap {
        pub fn new() -> Self {
            Self { map: Vec::new(), e_position: (0, 0), exit: (0,  0) }
        }

        fn new_from(&self) -> Self {
            let mut me = Self { map: Vec::new(), .. *self };
            me.map.push(self.map[0].clone());
            let mut row = Vec::new();
            row.push(Tile::Wall);
            for _ in 0 .. self.map[0].len() - 2 {
                row.push(Tile::FreeGround);
            }
            row.push(Tile::Wall);
            for _ in 0 .. self.map.len() - 2 {
                me.map.push(row.clone());
            }
            me.map.push(self.map[self.map.len() - 1].clone());

            me
        }

        pub fn set_map(&mut self, map: Map) {
            self.map = map;
        }

        pub fn move_all_blizzards(&self) -> Self {
            let mut new_map = self.new_from();
            let n_wrap_row = (self.map.len() - 3) as isize;
            let n_wrap_col = (self.map[0].len() - 3) as isize;

            for (n_row, tiles_row) in self.map.iter().enumerate().take(self.map.len() - 1).skip(1) {
                for (n_col, tile) in tiles_row.iter().enumerate().take(self.map[0].len() - 1).skip(1) {
                    // Any blizzards occupying this tile?
                    if let Tile::Blizzards(blizzards) = tile {
                        let new_pos = |ra, ca, ra_alt, ca_alt| {
                            let n_r: usize = n_row.saturating_add_signed(ra);
                            let n_c: usize = n_col.saturating_add_signed(ca);
                            if self.map[n_r][n_c] != Tile::Wall {
                                (n_r, n_c)
                            } else {
                                (n_row.saturating_add_signed(ra_alt), n_col.saturating_add_signed(ca_alt))
                            }
                        };

                        for blizzard in blizzards {
                            let (nxt_r, nxt_c) = match blizzard {
                                Blizzard::Up => new_pos(-1, 0, n_wrap_row, 0),
                                Blizzard::Down => new_pos(1, 0, -n_wrap_row, 0),
                                Blizzard::Left => new_pos(0, -1, 0, n_wrap_col),
                                Blizzard::Right => new_pos(0, 1, 0, -n_wrap_col),
                            };

                            // Get a mutable entry to next blizzard position
                            let Some(n_tile) = new_map.map[nxt_r].get_mut(nxt_c) else { panic!() };
                            // Are there any blizzards that have moved to this new location already?
                            let mut new_blizzards = if let Tile::Blizzards(blzs) = (*n_tile).clone() {
                                blzs
                            } else {
                                Vec::new()
                            };

                            // Push current blizzard to the vector for the next location
                            new_blizzards.push(*blizzard);
                            // Insert the updated blizzards the new position
                            *n_tile = Tile::Blizzards(new_blizzards);
                        }
                    }
                }
            }

            new_map
        }

        pub fn e_alternatives(&self, e_pos: Pos, start_pos: Pos) -> Vec<Pos> {
            let mut positions = Vec::new();

            let (e_row, e_col) = e_pos;
            let mut is_free = |ra, ca| {
                let nr = e_row.saturating_add_signed(ra);
                if nr == self.map.len() {
                    return;
                }
                let nc = e_col.saturating_add_signed(ca);

                if (ra, ca) != (0, 0) && (nr, nc) == start_pos {
                    return;
                }

                if self.map[nr][nc] == Tile::FreeGround {
                    positions.push((nr, nc));
                }
            };

            is_free(1, 0);
            is_free(0, 1);
            is_free(0, 0); // Is it possible to stay where we are?
            is_free(0, -1);
            is_free(-1, 0);

            positions
        }
    }
}
use internal::*;

#[derive(Clone, Debug)]
struct State {
    step_count: usize,
    field_map_index: usize,
    e_position: Pos,
}

fn get_all_field_variants(start_field: &FieldMap) -> Vec<FieldMap> {
    let mut fields = vec![start_field.clone()];
    let mut field = start_field.clone();
    loop {
        field = field.move_all_blizzards();
        if &field == start_field {
            break;
        }

        fields.push(field.clone());
    }

    fields
}

fn cross_field(possible_fields: &[FieldMap], field_map_index: &mut usize, e_pos_in: Pos, exit: Pos, min: usize) -> usize {
    let possible_fields_count = possible_fields.len();
    let mut min_steps = min;

    let mut stack = Vec::new();
    let mut e_visited: Vec<(Pos, usize, usize)> = vec![(e_pos_in, *field_map_index, 0)];
    stack.push(State { step_count: 0, field_map_index: *field_map_index, e_position: e_pos_in });

    let mut exit_found = false;
    while let Some(state) = stack.pop() {
        let fld_map_index = (state.field_map_index + 1) % possible_fields_count;
        let e_pos_variants = possible_fields[fld_map_index].e_alternatives(state.e_position, e_pos_in);

        for e_pos in e_pos_variants.iter() {
            let mut a_state = state.clone();
            a_state.step_count += 1;

            if *e_pos == exit {
                exit_found = true;
                if a_state.step_count < min_steps {
                    min_steps = a_state.step_count;
                    *field_map_index = fld_map_index;
                }
                break;
            }
            if e_visited.contains(&(*e_pos, fld_map_index, a_state.step_count)) {
                continue;
            }

            let min_steps_left = e_pos.0.abs_diff(exit.0) + e_pos.1.abs_diff(exit.1);
            if a_state.step_count + min_steps_left >= min_steps {
                continue;
            }

            e_visited.push((*e_pos, fld_map_index, a_state.step_count));
            stack.push(State { field_map_index: fld_map_index, e_position: *e_pos, .. a_state });
        }
    }

    if !exit_found {
        panic!("Sheit!")
    }

    min_steps
}

fn task_a(lines: &[String]) {
    let (start_field, e_pos, exit) = parse_input(lines);

    let possible_fields = get_all_field_variants(&start_field);
    let mut field_map_index = 0;
    let min_steps = cross_field(&possible_fields, &mut field_map_index, e_pos, exit, (lines.len() + lines[0].len()) * 2);

    println!("Fewest number of minutes: {}", min_steps);
}

fn task_b(lines: &[String]) {
    let (start_field, e_pos, exit) = parse_input(lines);

    let possible_fields = get_all_field_variants(&start_field);
    let mut field_map_index = 0;
    let mut min_steps = cross_field(&possible_fields, &mut field_map_index, e_pos, exit, (lines.len() + lines[0].len()) * 2);
    min_steps += cross_field(&possible_fields, &mut field_map_index, exit, e_pos, (lines.len() + lines[0].len()) * 2);
    min_steps += cross_field(&possible_fields, &mut field_map_index, e_pos, exit, (lines.len() + lines[0].len()) * 2);

    println!("Fewest number of minutes: {}", min_steps);
}

fn parse_input(lines: &[String]) -> (FieldMap, Pos, Pos) {
    let mut field = FieldMap::new();

    field.set_map(
        lines
            .iter()
            .map(|line| {
                line
                .chars()
                .map(|ch|
                    match ch {
                        '.' => Tile::FreeGround,
                        '#' => Tile::Wall,
                        '^' => Tile::Blizzards(vec![Blizzard::Up]),
                        'v' => Tile::Blizzards(vec![Blizzard::Down]),
                        '<' => Tile::Blizzards(vec![Blizzard::Left]),
                        '>' => Tile::Blizzards(vec![Blizzard::Right]),
                        _ => panic!()
                    }
                )
                .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>()
    );

    let p_entry = lines[0].chars().position(|c| c == '.').unwrap();
    let last_line = lines.len() - 1;
    let p_exit = lines[last_line].chars().position(|c| c == '.').unwrap();

    (field, (0, p_entry), (last_line, p_exit))
}

#[cfg(test)]
mod tests {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;

    #[test]
    fn t() {}
}

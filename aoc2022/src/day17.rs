use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    #[derive(Clone, Copy, Debug, PartialEq)]
    pub enum Variant {
        Dash,
        Plus,
        Ell,
        Bar,
        Square,
        Undefined,
    }

    impl Variant {
        fn next(&self) -> Self {
            match *self {
                Self::Dash      => Self::Plus,
                Self::Plus      => Self::Ell,
                Self::Ell       => Self::Bar,
                Self::Bar       => Self::Square,
                Self::Square    => Self::Dash,
                Self::Undefined => Self::Dash,
            }
        }
    }

    #[derive(Clone, Debug)]
    pub struct Chamber {
        rocks_count: u64,
        current_variant: Variant,
        rocks: Vec<Rock>,
        units: Vec<Vec<bool>>,
        this_rock_low_left: LowLeft,
        valid_rock: bool,
    }

    #[derive(Clone, Debug)]
    struct LowLeft {
        row: u32,
        col: u32,
    }

    impl Chamber {
        pub fn new() -> Self {
            let mut rocks = Vec::new();
            for i in 0 .. 5 {
                rocks.push(Rock::new(i));
            }

            Self {
                rocks_count: 0,
                current_variant: Variant::Undefined,
                rocks,
                units: Vec::new(),
                this_rock_low_left: LowLeft { row: (3), col: (2) },
                valid_rock: false,
            }
        }

        #[allow(dead_code)]
        pub fn display(&self) {
            self.display_some(0);
        }

        pub fn display_some(&self, rows: usize) {
            // println!("rocks_count = {}", self.rocks_count);
            // println!("current_variant = {:?}\n", self.current_variant);

            let uni_len = self.units.len();
            let this_rock = &self.rocks[self.current_variant as usize];
            let rock_hgt = this_rock.height as usize;
            let ll_r = self.this_rock_low_left.row as usize;
            let ll_c = self.this_rock_low_left.col as usize;

            let high_row = uni_len.max(ll_r + rock_hgt);
            let low_row = if rows == 0 {
                0
            } else {
                high_row - rows.min(high_row)
            };
            for r in (low_row .. high_row).rev() {
                print!("{:4} | ", r);
                let in_uni = r < uni_len;
                if (ll_r .. ll_r + rock_hgt).contains(&r) && self.valid_rock {
                    for c in 0 .. ll_c {
                        let ch = if !in_uni || !self.units[r][c] { '.' } else { '#' };
                        print!("{}", ch);
                    }
                    for c in 0 .. this_rock.shape[0].len() {
                        let ch =
                            if this_rock.shape[r - ll_r][c] {
                                '@'
                            } else if in_uni && self.units[r][c + ll_c] {
                                '#'
                            } else {
                                '_'
                            };
                        print!("{}", ch);
                    }
                    for c in ll_c + this_rock.shape[0].len() .. 7 {
                        let ch = if !in_uni || !self.units[r][c] { '.' } else { '#' };
                        print!("{}", ch);
                    }
                } else {
                    for c in 0 .. 7 {
                        let ch = if !in_uni || !self.units[r][c] { '.' } else { '#' };
                        print!("{}", ch);
                    }

                }

                println!();
            }
            println!();
        }

        #[allow(dead_code)]
        pub fn display_top(&self) {
            print!("{:5} | ", self.units.len());
            for c in 0 .. 7 {
                let ch = if !self.units[self.units.len() - 1][c] { '.' } else { '#' };
                print!("{}", ch);
            }

            println!();
        }

        pub fn drop(&mut self) -> bool {
            if !self.valid_rock {
                self.rock_appear();
            } else {
                let this_rock = &self.rocks[self.current_variant as usize];
                let is_free = {
                    // Is the next row in the chamber free to fall into?
                    if self.this_rock_low_left.row == 0 {
                        false
                    } else {
                        let rock_next_low_row = self.this_rock_low_left.row as usize - 1;

                        // Any resting rocks in the way?
                        let n_layers_to_check = if self.current_variant != Variant::Plus { 1 } else { 2 };
                        (0 .. n_layers_to_check).fold(true, |free_r, row| {
                            let u_ind = rock_next_low_row + row;
                            if free_r && u_ind < self.units.len() {
                                let cmp_level = &self.units[u_ind];
                                (0 .. this_rock.width).fold(true, |free, col| {
                                    if free && this_rock.shape[row][col as usize] {
                                        // this_rock's bottom row occupies this column. Is it free in the chamber?
                                        !cmp_level[(self.this_rock_low_left.col + col) as usize]
                                    } else {
                                        free
                                    }
                                })
                            } else {
                                // The rock row is above the resting rocks
                                free_r
                            }
                        })
                    }
                };

                if !is_free {
                    // Can't fall further down. Rock is coming to rest
                    self.rocks_count += 1;
                    let mut row_for_shape = self.this_rock_low_left.row as usize;
                    for rock_row in this_rock.shape.iter() {
                        if row_for_shape == self.units.len() {
                            self.units.push( vec![false; 7] );
                        }
                        for (i, rr_iter) in rock_row.iter().enumerate() {
                            self.units[row_for_shape][self.this_rock_low_left.col as usize + i] |= *rr_iter;
                        }
                        row_for_shape += 1;
                    }
                    self.valid_rock = false;
                } else {
                    self.this_rock_low_left.row -= 1;
                }

            }

            !self.valid_rock
        }

        pub fn jet_push(&mut self, jet: i8) {
            let this_rock = &self.rocks[self.current_variant as usize];
            let next_left_col = self.this_rock_low_left.col as i8 + jet;
            if next_left_col < 0 || next_left_col + this_rock.width as i8 > 7 {
                return;
            }

            // Would the next position for the rock collide with any resting rocks?
            let is_free = (0 .. this_rock.height).fold(true, |free_rows, rock_row| {
                let u_ind = (self.this_rock_low_left.row + rock_row) as usize;
                if !free_rows || u_ind >= self.units.len() {
                    free_rows
                } else {
                    let cmp_level = &self.units[u_ind];

                    (0 .. this_rock.width).fold(true, |free_cols, rock_col| {
                        if !free_cols || !this_rock.shape[rock_row as usize][rock_col as usize] {
                            free_cols
                        } else {
                            // this_rock wants to occupy this cell. Is it free?
                            !cmp_level[(next_left_col as u32 + rock_col) as usize]
                        }
                    })
                }
            });

            if is_free {
                self.this_rock_low_left.col = next_left_col as u32;
            }
        }

        pub fn height(&self) -> u64 {
            self.units.len() as u64
        }

        pub fn rocks_count(&self) -> u64 {
            self.rocks_count
        }

        fn rock_appear(&mut self) {
            self.current_variant = self.current_variant.next();
            self.this_rock_low_left = LowLeft { row: self.units.len() as u32 + 3, col: 2 };
            self.valid_rock = true;
        }
    }

    #[derive(Clone, Debug)]
    struct Rock {
        height: u32,
        width: u32,
        shape: Vec<Vec<bool>>,
    }

    // Rock shapes:
    // dash   plus  ell  bar  square
    // ####    #      #   #   ##
    //        ###     #   #   ##
    //         #    ###   #
    //                    #
    impl Rock {
        fn new(variant: u8) -> Self {
            match variant {
                0 => Self { height: 1, width: 4, shape: vec![vec![true; 4]] },
                1 => Self { height: 3, width: 3, shape: vec![vec![false, true, false], vec![true, true, true], vec![false, true, false]] },
                2 => Self { height: 3, width: 3, shape: vec![vec![true, true, true], vec![false, false, true], vec![false, false, true]] },
                3 => Self { height: 4, width: 1, shape: vec![vec![true]; 4] },
                4 => Self { height: 2, width: 2, shape: vec![vec![true, true]; 2] },
                _ => panic!("Sheit!!!")
            }
        }
    }
}
use internal::*;

fn run(jets: &[i8], n_rocks_lim: u64) -> u64 {
    enum Limit {
        Rocks(u64),
        Jets(u64),
    }

    fn calc_units(chamber: &mut Chamber, limit: Limit, jets: &[i8], offset: u64) -> u64 {
        let mut jet_i = 0;
        let mut jets_streams_count = 0;

        loop {
            let at_rest = chamber.drop();
            let next_jet = jets[jet_i];

            if at_rest {
                if let Limit::Rocks(n_rocks_lim) = limit {
                    if chamber.rocks_count() == n_rocks_lim {
                        break;
                    }
                }
                chamber.drop();
            }
            chamber.jet_push(next_jet);

            jet_i += 1;
            if jet_i == jets.len() {
                jet_i = 0;
                jets_streams_count += 1;
            }

            if let Limit::Jets(n_jets_lim) = limit {
                if jets_streams_count == n_jets_lim {
                    break;
                }
            }
        }

        chamber.height() - offset
    }

    let mut chamber = Chamber::new();

    if n_rocks_lim == 2022 {
        calc_units(&mut chamber, Limit::Rocks(n_rocks_lim), jets, 0)
    } else {
        let loop_n_rocks;
        let loop_n_units;
        let jets_limit;
        if jets.len() < 100 {
            loop_n_rocks = 35;
            loop_n_units = 53;
            jets_limit = 3;
        } else {
            loop_n_rocks = 1720;
            loop_n_units = 2729;
            jets_limit = 5;
        }
        // Build up to first standard - base - level
        let n_base_units = calc_units(&mut chamber, Limit::Jets(jets_limit), jets, 0);

        let n_base_rocks = chamber.rocks_count();
        let n_base_loops = (n_rocks_lim - n_base_rocks) / loop_n_rocks;
        let n_rocks_left = (n_rocks_lim - n_base_rocks) % loop_n_rocks;

        // Build last part above standard levels
        let last_units = if n_rocks_left != 0 {
            calc_units(&mut chamber, Limit::Rocks(n_base_rocks + n_rocks_left), jets, n_base_units)
        } else {
            0
        };
        n_base_units + n_base_loops * loop_n_units + last_units
    }
}

fn task_a(lines: &[String]) {
    let jets = parse_input(lines);

    let height = run(&jets, 2022);

    println!("Chamber is {} units tall", height);
}

fn task_b(lines: &[String]) {
    let jets = parse_input(lines);

    let height = run(&jets, 1_000_000_000_000);

    println!("Chamber is {} units tall", height);
}

fn parse_input(lines: &[String]) -> Vec<i8> {
    lines[0]
        .chars()
        .map(|c| if c == '<' { -1 } else { 1 })
        .collect_vec()
}

#[cfg(test)]
mod tests17 {
    use super::*;

    #[test]
    fn t1() {
        let mut cham = Chamber::new();
        let drop = |c: &mut Chamber, jet| {
            println!("jet = {}", jet);
            if c.drop() {
                print!("D 0: ");
                c.display();
                c.drop();
            }
            print!("D 1: ");
            c.display();
            c.jet_push(jet);
            print!("D 2: ");
            c.display();
        };
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, -1);
        drop(&mut cham, 1);
        drop(&mut cham, 1);
        cham.display();
        drop(&mut cham, -1);
        cham.display();
        drop(&mut cham, 1);
        cham.display();
        drop(&mut cham, -1);
        cham.display();
        drop(&mut cham, -1);
        cham.display();
        drop(&mut cham, 1);
        cham.display();
        drop(&mut cham, 1);
        cham.display();
        drop(&mut cham, 1);
        cham.display();
        drop(&mut cham, 1);
        cham.display();
    }
}

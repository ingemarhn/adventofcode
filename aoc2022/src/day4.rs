pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    pub type Lim = u8;
    type SecPair = (Section, Section);
    pub type SecPairs = Vec<SecPair>;

    #[derive(Clone, Copy, Debug)]
    pub struct Section {
        ll: Lim,
        ul: Lim,
    }

    impl Section {
        pub fn new(ll: Lim, ul: Lim) -> Self {
            Self { ll, ul }
        }

        pub fn any_contains(&self, other: Self) -> bool {
            (self.ll <= other.ll && self.ul >= other.ul) ||
            (other.ll <= self.ll && other.ul >= self.ul)
        }

        pub fn any_overlaps(&self, other: Self) -> bool {
            (self.ll >= other.ll) && (self.ll <= other.ul) ||
            (self.ul >= other.ll) && (self.ul <= other.ul) ||
            (other.ll >= self.ll) && (other.ll <= self.ul)
        }
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let sec_pairs = parse_input(lines);

    let n_contained =
        sec_pairs
        .iter()
        .fold(0, |n, pairs| {
            if pairs.0.any_contains(pairs.1) {
                n + 1
            } else {
                n
            }
        });

    println!("No of pairs fully contained by another = {}", n_contained);
}

fn task_b(lines: &[String]) {
    let sec_pairs = parse_input(lines);

    let n_overlaps =
        sec_pairs
        .iter()
        .fold(0, |n, pairs| {
            if pairs.0.any_overlaps(pairs.1) {
                n + 1
            } else {
                n
            }
        });

    println!("No of pairs with overlapping assignments = {}", n_overlaps);
}

fn parse_input(lines: &[String]) -> SecPairs {
    lines
        .iter()
        .map(|line|
            line
            .split(|c| c == ',' || c == '-')
            .map(|dgs| dgs.parse::<Lim>().unwrap())
            .collect::<Vec<_>>()
        )
        .map(|vl| (Section::new(vl[0], vl[1]), Section::new(vl[2], vl[3])))
        .collect::<Vec<_>>()
}

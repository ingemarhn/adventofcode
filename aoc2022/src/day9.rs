pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    type Position = (i16, i16);
    type Move = (i8, i8);
    type Instruction = (Move, u8);
    pub type Instructions = Vec<Instruction>;

    pub struct Rope {
        n_knots: u8,
        knots: Vec<Position>,
    }

    impl Rope {
        pub fn new(n_knots: u8) -> Self {
            let knots = vec![(0, 0); n_knots as usize];
            Self { n_knots, knots }
        }

        pub fn mv(&mut self, mov: Move) {
            self.knots[0].0 += mov.0 as i16;
            self.knots[0].1 += mov.1 as i16;

            for i in 0 .. (self.n_knots - 1) as usize{
                if self.is_in_touch(i, i + 1) {
                    break;
                }

                let dy = self.knots[i].0 - self.knots[i + 1].0;
                let dx = self.knots[i].1 - self.knots[i + 1].1;

                let dmy = if dy.abs() == 2 { dy / 2 } else { dy };
                let dmx = if dx.abs() == 2 { dx / 2 } else { dx };

                self.knots[i + 1].0 += dmy;
                self.knots[i + 1].1 += dmx;
            }
        }

        fn is_in_touch(&self, i1: usize, i2: usize) -> bool {
            (self.knots[i1].0 - self.knots[i2].0).abs() != 2 && (self.knots[i1].1 - self.knots[i2].1).abs() != 2
        }

        pub fn get_tail_pos(&self) -> Position {
            self.knots[(self.n_knots - 1) as usize]
        }
    }
}
use std::collections::HashMap;

use internal::*;

fn task_a(lines: &[String]) {
    let instructions = parse_input(lines);

    let mut rope = Rope::new(2);
    let mut visited = HashMap::new();
    for inst in instructions {
        for _ in 0 .. inst.1 {
            rope.mv(inst.0);
            let pos = rope.get_tail_pos();
            visited.entry(pos).and_modify(|cnt| *cnt += 1).or_insert(1);
        }
    }

    println!("# of visited positions: {}", visited.len());
}


fn task_b(lines: &[String]) {
    let instructions = parse_input(lines);

    let mut rope = Rope::new(10);
    let mut visited = HashMap::new();
    for inst in instructions {
        for _ in 0 .. inst.1 {
            rope.mv(inst.0);
            let pos = rope.get_tail_pos();
            visited.entry(pos).and_modify(|cnt| *cnt += 1).or_insert(1);
        }
    }

    println!("# of visited positions: {}", visited.len());
}

fn parse_input(lines: &[String]) -> Instructions {
    lines
        .iter()
        .map(|line| {
            let mut parts =
                line
                .split(' ');
            let mv = match parts.next().unwrap() {
                "U" => (1, 0),
                "L" => (0, -1),
                "D" => (-1, 0),
                "R" => (0, 1),
                p => panic!("Noooo... {p}"),
            };

            (mv, parts.next().unwrap().parse().unwrap())
        })
        .collect::<Vec<_>>()
}

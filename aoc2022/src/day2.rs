pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use core::panic;

    pub type Rounds = Vec<Round>;

    #[derive(Copy, Clone, Debug)]
    enum Shape {
        Rock     = 1,
        Paper,
        Scissors,
    }

    enum Outcome {
        Lose = 0,
        Draw = 3,
        Win  = 6,
    }

    impl Shape {
        fn fight_outcome(&self) -> Outcome {
            match *self {
                Self::Rock     => Outcome::Lose,
                Self::Paper    => Outcome::Draw,
                Self::Scissors => Outcome::Win,
            }
        }
    }

    #[derive(Copy, Clone, Debug)]
    pub struct Round {
        opp_shape: Shape,
        my_shape: Shape,
    }

    impl Round {
        pub fn new(o_shp: char, m_shp: char) -> Self {
            Self { opp_shape: Self::get_shape(o_shp), my_shape: Self::get_shape(m_shp) }
        }

        pub fn fight_strategy_1(&self) -> u32 {
            let mut my_points = self.my_shape as u32;

            my_points +=
                match (self.opp_shape, self.my_shape) {
                    (Shape::Rock, Shape::Paper)       |
                    (Shape::Paper, Shape::Scissors)   |
                    (Shape::Scissors, Shape::Rock)      => 6,
                    (Shape::Rock, Shape::Rock)        |
                    (Shape::Paper, Shape::Paper)      |
                    (Shape::Scissors, Shape::Scissors)  => 3,
                    _                                   => 0,
                };

            my_points
        }

        pub fn fight_strategy_2(&self) -> u32 {
            let outcome = self.my_shape.fight_outcome();

            let new_weapon =
                match outcome {
                    Outcome::Win => match self.opp_shape {
                        Shape::Paper        => Shape::Scissors,
                        Shape::Scissors     => Shape::Rock,
                        Shape::Rock         => Shape::Paper,
                    },
                    Outcome::Draw => self.opp_shape,
                    Outcome::Lose => match self.opp_shape {
                        Shape::Paper        => Shape::Rock,
                        Shape::Scissors     => Shape::Paper,
                        Shape::Rock         => Shape::Scissors,
                    }
                };

            new_weapon as u32 + outcome as u32
        }

        fn get_shape(shp: char) -> Shape {
            match shp {
                'A' | 'X' => Shape::Rock,
                'B' | 'Y' => Shape::Paper,
                'C' | 'Z' => Shape::Scissors,
                _ => panic!("Gaaaaaaaah!"),
            }
        }
    }
}
use internal::*;

fn fight(rounds: &Rounds, fn_fight: fn(&Round) -> u32) -> u32 {
    rounds
        .iter()
        .fold(0, |sum, r| {
            sum + fn_fight(r)
        })
}

fn task_a(lines: &[String]) {
    let rounds: Rounds = parse_input(lines);

    let points = fight(&rounds, Round::fight_strategy_1);

    println!("Total score = {}", points);
}

fn task_b(lines: &[String]) {
    let rounds: Rounds = parse_input(lines);

    let points = fight(&rounds, Round::fight_strategy_2);

    println!("Total score = {}", points);
}

fn parse_input(lines: &[String]) -> Rounds {
    lines
        .iter()
        .map(|l| {
            let mut chrs = l.chars();
            let c_opp = chrs.next().unwrap();
            let c_me  = chrs.nth(1).unwrap();
            Round::new(c_opp, c_me)
        })
        .collect()
}

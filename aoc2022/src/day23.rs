use std::ops::Add;
use std::collections::{HashSet, HashMap};
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum CompassBearing {
    N,
    Ne,
    E,
    Se,
    S,
    Sw,
    W,
    Nw,
}
use CompassBearing::*;

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
struct Pos(isize, isize);

impl Add for Pos {
    type Output = Pos;
    fn add(self, rhs: Self) -> Self {
        Pos(self.0 + rhs.0, self.1 + rhs.1)
    }
}

#[derive(Debug)]
struct Ground {
    relative_positions: HashMap<CompassBearing, Pos>,
    compass_bearings: [CompassBearing; 4],
    tiles: HashSet<Pos>,
}

impl Ground {
    fn new() -> Self {
        Self {
            relative_positions: HashMap::from([(N, Pos(-1, 0)), (Ne, Pos(-1, 1)), (E, Pos(0, 1)), (Se, Pos(1, 1)), (S, Pos(1, 0)), (Sw, Pos(1, -1)), (W, Pos(0, -1)), (Nw, Pos(-1, -1)), ]),
            compass_bearings: [N, S, W, E],
            tiles: HashSet::new(),
        }
    }

    fn propose_move(&self, pos: &Pos, start_bearing: usize) -> Option<Pos> {
        let mut bearing_ind = start_bearing;

        let mut ret_bear = None;
        let mut all_is_empty = true;
        for _ in 0 .. 4 {
            let test_bearings = match self.compass_bearings[bearing_ind] {
                N => [N, Nw, Ne],
                S => [S, Sw, Se],
                W => [W, Nw, Sw],
                E => [E, Ne, Se],
                _ => panic!()
            };

            let mut test_positions = Vec::new();
            for b in test_bearings.iter() {
                test_positions.push(*pos + self.relative_positions[b]);
            }
            let is_empty = !test_positions.iter().any(|p| self.tiles.iter().contains(p));
            if is_empty && ret_bear.is_none() {
                ret_bear = Some(test_positions[0]);
            }
            all_is_empty &= is_empty;

            bearing_ind = (bearing_ind + 1) % 4;
        }

        if all_is_empty {
            ret_bear = None;
        }

        ret_bear
    }

    fn move_pos(&mut self, pos: &Pos, pos_new: &Pos) {
        self.tiles.remove(pos);
        self.tiles.insert(*pos_new);
    }

    fn get_borders(&self) -> Vec<isize> {
        let mut borders = vec![0, 0, 0, 0];

        for elf in self.tiles.iter() {
            let Pos(r, c) = *elf;
            borders[0] = borders[0].min(r);
            borders[1] = borders[1].min(c);
            borders[2] = borders[2].max(r);
            borders[3] = borders[3].max(c);
        }

        borders
    }
}

fn run_round(ground: &mut Ground, start_bearing: usize) -> bool {
    let mut proposed_pos = HashMap::new();
    let mut elf_props_inds = Vec::new();

    for elf in ground.tiles.iter() {
        let pos = ground.propose_move(elf, start_bearing);
        elf_props_inds.push((*elf, pos));
        if let Some(pos) = pos {
            proposed_pos.entry(pos).and_modify(|counter| *counter += 1).or_insert(1);
        }
    }

    let mut has_moved = false;
    for pos in elf_props_inds.iter() {
        let Some(pos_new) = pos.1 else {
            continue;
        };
        if proposed_pos[&pos_new] == 1 {
            ground.move_pos(&pos.0, &pos_new);
            has_moved = true;
        }
    }

    has_moved
}

fn task_a(lines: &[String]) {
    let mut ground = parse_input(lines);

    let mut bearing_ind = 0;
    for _ in 0 .. 10 {
        run_round(&mut ground, bearing_ind);
        bearing_ind = (bearing_ind + 1) % 4;
    }

    let borders = ground.get_borders();
    let n_rows = borders[2] - borders[0] + 1;
    let n_cols = borders[3] - borders[1] + 1;

    println!("# of empty ground tiles: {}", n_rows * n_cols - ground.tiles.len() as isize);
}

fn task_b(lines: &[String]) {
    let mut ground = parse_input(lines);

    let mut bearing_ind = 0;
    let mut round_count = 0;
    loop {
        round_count += 1;
        let has_moved = run_round(&mut ground, bearing_ind);
        if !has_moved {
            break;
        }
        bearing_ind = (bearing_ind + 1) % 4;
    }

    println!("First round without moves is {}", round_count);
}

fn parse_input(lines: &[String]) -> Ground {
    let mut ground = Ground::new();
    lines
        .iter()
        .enumerate()
        .map(|(row, line)| {
            line
            .char_indices()
            .filter_map(move |(col, tile)| if tile == '#' { Some(Pos(row as isize, col as isize)) } else { None })
        })
        .for_each(|p|
            for pp in p {
                ground.tiles.insert(pp);
            }
        );

    ground
}

#[cfg(test)]
mod tests {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;

    #[test]
    fn t() {}
}

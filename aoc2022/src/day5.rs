pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    pub type Stack = Vec<char>;
    pub type Stacks = Vec<Stack>;
    pub type Int = u8;
    pub type Instructions = Vec<Instruction>;

    #[derive(Debug)]
    pub struct Instruction {
        n_moves: Int,
        from_stack: Int,
        to_stack: Int,
    }

    impl Instruction {
        pub fn new(n_moves: Int, from_stack: Int, to_stack: Int) -> Self {
            Self { n_moves, from_stack, to_stack }
        }

        pub fn get_n(&self) -> Int {
            self.n_moves
        }

        pub fn get_f(&self) -> usize {
            self.from_stack as usize
        }

        pub fn get_t(&self) -> usize {
            self.to_stack as usize
        }
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let (mut stacks, instructions) = parse_input(lines);

    for instr in instructions {
        for _ in 0 .. instr.get_n() {
            let c = stacks[instr.get_f() - 1].pop().unwrap();
            stacks[instr.get_t() - 1].push(c);
        }
    }

    let mut tc = String::new();
    for mut s in stacks {
        tc.push(s.pop().unwrap());
    }

    println!("Stack top row = {}", tc);
}

fn task_b(lines: &[String]) {
    let (mut stacks, instructions) = parse_input(lines);

    for instr in instructions {
        let f = instr.get_f() - 1;
        let ind = stacks[f].len() - instr.get_n() as usize;
        let mut pop = stacks[f].drain(ind..).collect();
        stacks[instr.get_t() - 1].append(&mut pop);
    }

    let mut tc = String::new();
    for mut s in stacks {
        tc.push(s.pop().unwrap());
    }

    println!("Stack top row = {}", tc);
}

fn parse_input(lines: &[String]) -> (Stacks, Instructions) {
    let mut i = 0;

    // Find line separating stacks and instructions
    loop {
        if lines[i].is_empty() {
            break;
        }

        i += 1;
    }

    // Create a stack for each number found in last line of stacks description
    let stack_nbrs = lines[i - 1].clone();
    let mut stacks = Stacks::new();
    for _ in stack_nbrs.trim().split_ascii_whitespace() {
        stacks.push(Stack::new());
    }

    // Create initial stacks
    for li in (0 .. i - 1).rev() {
        let mut chrs = lines[li].chars();
        let mut ci = 1;
        let mut si = 0;
        while let Some(c) = chrs.nth(ci) {
            if c != ' ' {
                stacks[si].push(c);
            }

            si += 1;
            ci = 3;
        }
    }

    let p = |s: &str| {
        s.parse().unwrap()
    };

    let mut instructions = Instructions::new();
    for line in &lines[i + 1 ..] {
        let mut parts = line.split(' ');
        let n = p(parts.by_ref().nth(1).unwrap());
        let f = p(parts.by_ref().nth(1).unwrap());
        let t = p(parts.by_ref().nth(1).unwrap());

        let instr = Instruction::new(n, f, t);
        instructions.push(instr);
    }

    (stacks, instructions)
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    type Tree = u8;
    pub type Wood = Vec<Vec<(Tree, bool)>>;
}
use internal::*;

fn task_a(lines: &[String]) {
    let mut wood = parse_input(lines);

    let side_len = wood.len();
    let side_len_m1 = side_len - 1;
    for i in 0 .. side_len {
        wood[0][i].1 = true;
        wood[side_len_m1][i].1 = true;
        wood[i][0].1 = true;
        wood[i][side_len_m1].1 = true;
    }

    for wood_y in wood.iter_mut().take(side_len_m1).skip(1) {
        let mut high = wood_y[0].0;
        for wood_y_x in wood_y.iter_mut().take(side_len_m1).skip(1) {
            if wood_y_x.0 > high {
                high = wood_y_x.0;
                wood_y_x.1 = true;
            }
        }

        let mut high = wood_y[side_len_m1].0;
        for x in (1 .. side_len_m1).rev() {
            if wood_y[x].0 > high {
                high = wood_y[x].0;
                wood_y[x].1 = true;
            }
        }
    }

    for x in 1 .. side_len_m1 {
        let mut high = wood[0][x].0;
        for wood_y in wood.iter_mut().take(side_len_m1).skip(1) {
            if wood_y[x].0 > high {
                high = wood_y[x].0;
                wood_y[x].1 = true;
            }
        }

        let mut high = wood[side_len_m1][x].0;
        for y in (1 .. side_len_m1).rev() {
            if wood[y][x].0 > high {
                high = wood[y][x].0;
                wood[y][x].1 = true;
            }
        }
    }

    let count =
        wood
        .iter()
        .fold(0, |cnt, trees| {
            cnt + trees
                    .iter()
                    .fold(0, |cntp, (_, vis)| {
                        if *vis {
                            cntp + 1
                        } else {
                            cntp
                        }
                    })
        });

    println!("# of visible trees = {}", count);
}

fn task_b(lines: &[String]) {
    let wood = parse_input(lines);

    let side_len = wood.len();
    let side_len_m1 = side_len - 1;

    let mut scenic_score = 0;

    for y in 1 .. side_len_m1 {
        for x in 1 .. side_len_m1 {
            let my_height = wood[y][x].0;

            let mut view_dists = [0; 4];
            for yy in (0 .. y).rev() {
                view_dists[0] += 1;
                if wood[yy][x].0 >= my_height { break; }
            }

            for xx in x + 1 .. side_len {
                view_dists[1] += 1;
                if wood[y][xx].0 >= my_height { break; }
            }

            for wood_y in wood.iter().take(side_len).skip(y + 1) {
                view_dists[2] += 1;
                if wood_y[x].0 >= my_height { break; }
            }

            for xx in (0 .. x).rev() {
                view_dists[3] += 1;
                if wood[y][xx].0 >= my_height { break; }
            }

            scenic_score = scenic_score.max(view_dists.iter().product());
        }
    }

    println!("Highset scenic score = {}", scenic_score);
}

fn parse_input(lines: &[String]) -> Wood {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|c|
                (c.to_digit(10).unwrap() as u8, false)
            )
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
}

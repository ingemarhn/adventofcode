pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: &[String]) {
    let calories = parse_input(lines);

    let (sum, _) =
        calories
        .iter()
        .fold((0, 0), |(largest, elfsum), n| {
            if let Some(cal) = n {
                (largest, elfsum + cal)
            } else {
                (std::cmp::max(largest, elfsum), 0)
            }
        });

    println!("Largest total calories = {sum}");
}

fn task_b(lines: &[String]) {
    let calories = parse_input(lines);

    let (top_three, _) =
        calories
        .iter()
        .fold(([0; 3], 0), |(largest, elfsum), n| {
            if let Some(cal) = n {
                (largest, elfsum + cal)
            } else {
                let mut lg = largest.to_vec();
                lg.push(elfsum);
                lg.sort();
                lg.reverse();
                let lga = [lg[0], lg[1], lg[2]];
                (lga, 0)
            }
        });

    let sum: i32 = top_three.iter().sum();

    println!("Total top three calories = {sum}");
}

fn parse_input(lines: &[String]) -> Vec<Option<i32>> {
    lines
        .iter()
        .map(|line|
            match line.as_str() {
                "" => None,
                _ => Some(line.parse().unwrap()),
            }
        )
        .collect::<Vec<_>>()
}

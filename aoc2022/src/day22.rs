use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Index = usize;
type Instructions = Vec<Instruction>;
type Locations = Vec<Vec<Location>>;

#[derive(Clone, Copy, Debug, PartialEq)]
enum LocationType {
        Tile,
        Wall,
        Void,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
struct Position {
    row: Index,
    col: Index,
}

impl Position {
    fn new(row: Index, col: Index) -> Self {
        Self { row, col }
    }

    fn get_side_indices(&self, sqr_size: usize, dir: usize, rev: bool) -> ((usize, usize), (isize, isize)) {
        let Position { row, col } = *self;
        let sqr_size_m1 = sqr_size - 1;
        let (start, adders) = match dir {
            0 => ((row, col + sqr_size_m1), (1, 0)),
            1 => ((row + sqr_size_m1, col + sqr_size_m1), (0, -1)),
            2 => ((row + sqr_size_m1, col), (-1, 0)),
            3 => ((row, col), (0, 1)),
            _ => panic!()
        };

        if !rev {
            (start, adders)
        } else {
            (
                (
                    (start.0 as isize + sqr_size_m1 as isize * adders.0) as usize,
                    (start.1 as isize + sqr_size_m1 as isize * adders.1) as usize,
                ),
                (
                    -adders.0,
                    -adders.1,
                )
            )
        }
    }
}

#[derive(Debug, PartialEq)]
struct Location {
    loc_type: LocationType,
    my_location: Position,
    next_location: Option<[Position; 4]>,
    change_dir_aft_mv: [Option<usize>; 4],
}

impl Location {
    fn new(typ: LocationType, row: Index, col: Index) -> Self {
        Self {
            loc_type: typ,
            my_location: Position::new(row, col),
            next_location: match typ {
                LocationType::Tile => {
                    // These are preliminary settings. The border positions have to adjusted when all locations are created.
                    Some([
                        Position::new(row, col + 1),
                        Position::new(row + 1, col),
                        Position::new(row, col - 1),
                        Position::new(row - 1, col),
                    ])
                },
                LocationType::Wall => None,
                LocationType::Void => None,
            },
            change_dir_aft_mv: [None; 4],
        }

    }
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum EdgeType {
    TopRight,
    TopFront,
    TopLeft,
    TopBack,
    RightBack,
    RightBottom,
    RightFront,
    RightTop,
    FrontRight,
    FrontBottom,
    FrontLeft,
    FrontTop,
    LeftFront,
    LeftBottom,
    LeftBack,
    LeftTop,
    BackLeft,
    BackBottom,
    BackRight,
    BackTop,
    BottomRight,
    BottomBack,
    BottomLeft,
    BottomFront,
}
use EdgeType::*;

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum SideWithEdge {
    Top(EdgeType),
    Right(EdgeType),
    Front(EdgeType),
    Left(EdgeType),
    Back(EdgeType),
    Bottom(EdgeType),
}
use SideWithEdge::*;

impl SideWithEdge {
    fn next_edge_clock_wise(&mut self) {
        *self = match *self {
            Top(e_type) => Top( match e_type { TopRight => TopFront, TopFront => TopLeft, TopLeft => TopBack, TopBack => TopRight, _ => panic!()} ),
            Right(e_type) => Right( match e_type { RightTop => RightBack, RightBack => RightBottom, RightBottom => RightFront, RightFront => RightTop, _ => panic!()} ),
            Front(e_type) => Front( match e_type { FrontTop => FrontRight, FrontRight => FrontBottom, FrontBottom => FrontLeft, FrontLeft => FrontTop, _ => panic!()} ),
            Left(e_type) => Left( match e_type { LeftTop => LeftFront, LeftFront => LeftBottom, LeftBottom => LeftBack, LeftBack => LeftTop, _ => panic!()} ),
            Back(e_type) => Back( match e_type { BackTop => BackLeft, BackLeft => BackBottom, BackBottom => BackRight, BackRight => BackTop, _ => panic!()} ),
            Bottom(e_type) => Bottom( match e_type { BottomFront => BottomRight, BottomRight => BottomBack, BottomBack => BottomLeft, BottomLeft => BottomFront, _ => panic!()} ),
        }
    }

    fn connected_side(&self) -> Self {
        match *self {
            Top(e_type) => match e_type { TopRight => Right(RightTop), TopFront => Front(FrontTop), TopLeft => Left(LeftTop), TopBack => Back(BackTop), _ => panic!()},
            Right(e_type) => match e_type { RightTop => Top(TopRight), RightBack => Back(BackRight), RightBottom => Bottom(BottomRight), RightFront => Front(FrontRight), _ => panic!()},
            Front(e_type) => match e_type { FrontTop => Top(TopFront), FrontRight => Right(RightFront), FrontBottom => Bottom(BottomFront), FrontLeft => Left(LeftFront), _ => panic!()},
            Left(e_type) => match e_type { LeftTop => Top(TopLeft), LeftFront => Front(FrontLeft), LeftBottom => Bottom(BottomLeft), LeftBack => Back(BackLeft), _ => panic!()},
            Back(e_type) => match e_type { BackTop => Top(TopBack), BackLeft => Left(LeftBack), BackBottom => Bottom(BottomBack), BackRight => Right(RightBack), _ => panic!()},
            Bottom(e_type) => match e_type { BottomFront => Front(FrontBottom), BottomRight => Right(RightBottom), BottomBack => Back(BackBottom), BottomLeft => Left(LeftBottom), _ => panic!()},
        }
    }

    fn get_top_or_front_edge(&self) -> Self {
        match self {
            Top(_) => Top(TopFront),
            Right(_) => Right(RightTop),
            Front(_) => Front(FrontTop),
            Left(_) => Left(LeftTop),
            Back(_) => Back(BackTop),
            Bottom(_) => Bottom(BottomFront),
        }
    }

    fn get_edge_type(&self) -> EdgeType {
        match self {
            Top(e_type)   |
            Right(e_type) |
            Front(e_type) |
            Left(e_type)  |
            Back(e_type)  |
            Bottom(e_type) => *e_type,
        }
    }

    fn match_corner(&self, other: Self) -> bool {
        let self_type = self.get_edge_type();
        let other_type = other.get_edge_type();

        let self_match = match self_type {
            TopRight => RightTop,
            TopFront => FrontTop,
            TopLeft => LeftTop,
            TopBack => BackTop,
            RightBack => BackRight,
            RightBottom => BottomRight,
            RightFront => FrontRight,
            RightTop => TopRight,
            FrontRight => RightFront,
            FrontBottom => BottomFront,
            FrontLeft => LeftFront,
            FrontTop => TopFront,
            LeftFront => FrontLeft,
            LeftBottom => BottomLeft,
            LeftBack => BackLeft,
            LeftTop => TopLeft,
            BackLeft => LeftBack,
            BackBottom => BottomBack,
            BackRight => RightBack,
            BackTop => TopBack,
            BottomRight => RightBottom,
            BottomBack => BackBottom,
            BottomLeft => LeftBottom,
            BottomFront => FrontBottom,
        };

        self_match == other_type
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Square {
    square_map_pos: (usize, usize),
    sqr_type: SideWithEdge,
    top_or_front_direction: usize,
    is_connected: [bool; 4],
}

impl Square {
    fn new(r: usize, c: usize, side: SideWithEdge, dir: usize) -> Self {
        Self {
            square_map_pos: (r, c),
            sqr_type: side,
            top_or_front_direction: dir,
            is_connected: [false; 4],
        }
    }

    fn set_connected(&mut self, dir_ind: usize) {
        self.is_connected[dir_ind] = true;
    }

    fn eq_side(&self, other: SideWithEdge) -> bool {
        self.sqr_type.get_top_or_front_edge() == other.get_top_or_front_edge()
    }
}

#[derive(Debug)]
enum Instruction {
    NumberSteps(Index),
    Direction(char),
}

fn run_instructions(locations: &Locations, instructions: &Instructions) -> usize {
    let mut col = 1;

    // Find left topmost tile
    while locations[1][col].loc_type != LocationType::Tile {
        col += 1;
    }

    let char_to_dir = |curr_dir, dir_ch| match dir_ch {
        'L' => (curr_dir + 4 - 1) % 4,
        'R' => (curr_dir + 1) % 4,
        _ => panic!(),
    };

    let (row, col, direction) =
        instructions
        .iter()
        .fold((1, col, 0), |(mut r, mut c, mut d), instr| {
            match instr {
                Instruction::NumberSteps(steps) => {
                    for _ in 0 .. *steps {
                        let Position {row, col} = locations[r][c].next_location.unwrap()[d];

                        if locations[row][col].loc_type != LocationType::Tile {
                            break;
                        }
                        if let Some(new_dir) = locations[r][c].change_dir_aft_mv[d] {
                            d = new_dir;
                        }

                        r = row;
                        c = col;
                    }

                    (r, c, d)
                },
                Instruction::Direction(dir) => {
                    let nx_d = char_to_dir(d, *dir);

                    (r, c, nx_d)
                },
            }
        });

    1000 * row + 4 * col + direction
}

fn task_a(lines: &[String]) {
    let (locations, instructions) = parse_input(lines, false);

    let pwd = run_instructions(&locations, &instructions);

    println!("Password is {}", pwd);
}

fn task_b(lines: &[String]) {
#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    let (locations, instructions) = parse_input(lines, true);

    let pwd = run_instructions(&locations, &instructions);

    println!("Password is {}", pwd);
}

fn parse_input(lines: &[String], fold_box: bool) -> (Locations, Instructions) {
    let mut locations = Vec::new();
    // Add an empty row on top since first row is index 1
    locations.push(Vec::new());

    let mut max_line_len = 0;
    let mut line_iter = lines.iter().enumerate();
    loop {
        let (n_row, line) = line_iter.next().unwrap();
        if line.is_empty() {
            break;
        }

        max_line_len = max_line_len.max(line.len());

        let mut line_locations = Vec::new();
        // Add a void location first since the first column is index 1
        line_locations.push(Location::new(LocationType::Void, locations.len(), 0));

        for (n_col, ch) in line.chars().enumerate() {
            let location_type = match ch {
                '.' => LocationType::Tile,
                '#' => LocationType::Wall,
                _   => LocationType::Void,
            };

            let location = Location::new(location_type, n_row + 1, n_col + 1);
            line_locations.push(location);
        }

        locations.push(line_locations);
    }

    // Get a row's first and last non-void location
    let first_last = |row: usize| {
        let mut left = 1;
        for i in 1 .. locations[row].len() {
            if locations[row][i].loc_type != LocationType::Void {
                left = i;
                break;
            }
        }
        // The first row is 0 length. Avoid crash using saturating_add_signed instead of '-'
        let right = locations[row].len().saturating_add_signed(-1);

        (left, right)
    };

    let all_left_right =
        locations
        .iter()
        .enumerate()
        .map(|(row, _)| first_last(row))
        .collect_vec();

    // Shall we fold a box?
    let squares_positions_map = if fold_box {
        let mut squares_positions = Vec::new();
        let box_size = if max_line_len == 16 { 4 } else { 50 };
        for r in (1 .. locations.len()).step_by(box_size) {
            let mut r_squares = Vec::new();
            for c in (1 ..= max_line_len).step_by(box_size) {
                if c > all_left_right[r].1 || locations[r][c].loc_type == LocationType::Void {
                    r_squares.push(None);
                } else {
                    r_squares.push( Some(Position::new(r, c)) );
                }
            }

            squares_positions.push(r_squares);
        }

        Some((squares_positions, box_size))
    } else {
        None
    };

    if let Some((squares_positions, square_size)) = squares_positions_map {
        let n_sqr_rows = squares_positions.len();
        let n_sqr_cols = squares_positions[0].len();

        // Get start row and column for top square
        let (tsq_row, tsq_col) = if n_sqr_rows == 3 {
            (1, if squares_positions[1][1].is_some() {1} else {2})
        } else {
            (if squares_positions[1][1].is_some() {1} else {2}, 1)
        };

        // *** Fold the box
        let mut squares = Vec::new();
        let mut visited = Vec::new();
        visited.push((tsq_row, tsq_col));

        let top = Square::new(tsq_row, tsq_col, Top(TopFront), 1);

        let mut queue = Vec::new();
        queue.push(top);

        while let Some(mut square) = queue.pop() {
            let (row, col) = square.square_map_pos;
            let mut dir = square.top_or_front_direction;
            let mut side_with_edge = square.sqr_type;
            for _ in 0 ..= 3 {
                dir = (dir + 1) % 4;
                side_with_edge.next_edge_clock_wise();
                let (row_nbr, col_nbr) = match dir {
                    0 => (row, col + 1),
                    1 => (row + 1, col),
                    2 => {
                        if col == 0 { continue; }
                        (row, col - 1)
                    },
                    3 => {
                        if row == 0 { continue; }
                        (row - 1, col)
                    },
                    _ => panic!()
                };

                if row_nbr == n_sqr_rows || col_nbr == n_sqr_cols {
                    continue;
                }

                if squares_positions[row_nbr][col_nbr].is_some() {
                    square.set_connected(dir);
                    let mut side_nbr = side_with_edge.connected_side();
                    let mut dir_nbr = (dir + 2) % 4;
                    let top_or_front_edge = side_nbr.get_top_or_front_edge();

                    while side_nbr != top_or_front_edge {
                        side_nbr.next_edge_clock_wise();
                        dir_nbr = (dir_nbr + 1) % 4;
                    }

                    let square_nbr = Square::new(row_nbr, col_nbr, side_nbr, dir_nbr);
                    if visited.contains(&(row_nbr, col_nbr)) {
                        continue;
                    }
                    queue.push(square_nbr);
                }
            }

            squares.push(square);
            visited.push((row, col));
        }

        // *** Connect unconnected edges in the box
        let set_nxt_loc = |locs: &mut Locations, pos_from: (usize, usize), pos_to: (usize, usize), dir_from, dir_to| {
            let loc_from = &mut locs.get_mut(pos_from.0).unwrap()[pos_from.1];
            if loc_from.loc_type == LocationType::Tile {
                let Some(nxt_loc) = &mut loc_from.next_location else { panic!() };
                nxt_loc[dir_from] = Position::new(pos_to.0, pos_to.1);
                loc_from.change_dir_aft_mv[dir_from] = Some((dir_to + 2) % 4);
            }

            let loc_to = &mut locs.get_mut(pos_to.0).unwrap()[pos_to.1];
            if loc_to.loc_type == LocationType::Tile {
                let Some(nxt_loc) = &mut loc_to.next_location else { panic!() };
                nxt_loc[dir_to] = Position::new(pos_from.0, pos_from.1);
                loc_to.change_dir_aft_mv[dir_to] = Some((dir_from + 2) % 4);
            }
        };

        for si in 0 .. squares.len() {
            let mut square_edge = squares[si].sqr_type;
            let mut dir_from = squares[si].top_or_front_direction;
            for _ in 0 ..= 3 {
                if !squares[si].is_connected[dir_from] {
                    let square_to_connect_edge = square_edge.connected_side();
                    let sqr_conn_ind = squares.iter().position(|sqr| sqr.eq_side(square_to_connect_edge)).unwrap();
                    let mut test_edge = squares[sqr_conn_ind].sqr_type;
                    let mut dir_to = squares[sqr_conn_ind].top_or_front_direction;
                    while !square_edge.match_corner(test_edge) {
                        test_edge.next_edge_clock_wise();
                        dir_to = (dir_to + 1) % 4;
                    }
                    let (row, col) = squares[si].square_map_pos;
                    let (mut pos_from, adders_from) = squares_positions[row][col].unwrap().get_side_indices(square_size, dir_from, false);
                    let (row, col) = squares[sqr_conn_ind].square_map_pos;
                    let (mut pos_to, adders_to) = squares_positions[row][col].unwrap().get_side_indices(square_size, dir_to, true);
                    for _ in 0 .. square_size {
                        set_nxt_loc(&mut locations, pos_from, pos_to, dir_from, dir_to);
                        pos_from.0 = pos_from.0.saturating_add_signed(adders_from.0);
                        pos_from.1 = pos_from.1.saturating_add_signed(adders_from.1);
                        pos_to.0 = pos_to.0.saturating_add_signed(adders_to.0);
                        pos_to.1 = pos_to.1.saturating_add_signed(adders_to.1);
                    }

                    squares[si].set_connected(dir_from);
                    squares[sqr_conn_ind].set_connected(dir_to);
                }

                dir_from = (dir_from + 1) % 4;
                square_edge.next_edge_clock_wise();
            }
        }
    } else {
        // Set "next location" for all tiles that are at the border of a line
        for (row, (lef, rig)) in all_left_right.iter().enumerate().skip(1) {
            for (edge, nxt_ind, other_edge) in [(*lef, 2, *rig), (*rig, 0, *lef)] {
                let loc_e = locations.get_mut(row).unwrap();
                if loc_e[edge].loc_type == LocationType::Tile {
                    if let Some(nxt_loc) = &mut loc_e[edge].next_location {
                        nxt_loc[nxt_ind].col = other_edge;
                    }
                }
            }
        }

        // Set "next location" for all tiles that are at the border of a column
        for col in 1 ..= max_line_len {
            let mut this_col_edges = (0, locations.len() - 1);

            for (row, location_row) in locations.iter().enumerate().skip(1) {
                if location_row[col].loc_type != LocationType::Void {
                    this_col_edges.0 = row;
                    break;
                }
            }
            for (row, (_, row_len)) in all_left_right.iter().enumerate().skip(this_col_edges.0) {
                if col > *row_len {
                    this_col_edges.1 = row - 1;
                    break;
                }
            }

            for (row, nxt_ind, other_row) in [(this_col_edges.0, 3, this_col_edges.1), (this_col_edges.1, 1, this_col_edges.0)] {
                if col > all_left_right[row].1 {
                    continue;
                }
                if locations[row][col].loc_type == LocationType::Tile {
                    if let Some(nxt_loc) = &mut locations[row][col].next_location {
                        nxt_loc[nxt_ind].row = other_row;
                    }
                }
            }
        }
    }

    // Parse the instructions line
    let (_, line) = line_iter.next().unwrap();
    let (instructions, _) =
        line
        .chars()
        .fold((Vec::new(), 0), |(mut instrs, steps), ch| {
            let nxt_steps = match ch {
                'L' | 'R' => {
                    instrs.push(Instruction::Direction(ch));
                    0
                },
                digit if steps == 0 => {
                    let st = digit.to_digit(10).unwrap() as Index;
                    instrs.push(Instruction::NumberSteps(st));
                    st
                },
                digit => {
                    let l = instrs.len();
                    if let Instruction::NumberSteps(instr) = instrs.get_mut(l - 1).unwrap() {
                        *instr = steps * 10 + digit.to_digit(10).unwrap() as Index;
                    }
                    0
                },
            };

            (instrs, nxt_steps)
        });

    (locations, instructions)
}

#[cfg(test)]
mod tests {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;

    #[test]
    fn t() {}
}

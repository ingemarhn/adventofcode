use std::collections::HashSet;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn calc_n_chars(lines: &[String], win_sz: usize) -> usize {
    let chars = parse_input(lines);
    let mut nch = win_sz - 1;
    let mut witer = chars.windows(win_sz);
    'l: loop {
        nch += 1;
        let win = witer.next();
        if win.is_none() {
            break;
        }

        let win = win.unwrap();
        let mut chk = HashSet::new();
        for c in win {
            if !chk.insert(*c) {
                continue 'l;
            }
        }

        break 'l;
    }

    nch
}

fn task_a(lines: &[String]) {
    let nch = calc_n_chars(lines, 4);

    println!("# of processed chars = {}", nch);
}

fn task_b(lines: &[String]) {
    let nch = calc_n_chars(lines, 14);

    println!("# of processed chars = {}", nch);
}

fn parse_input(lines: &[String]) -> Vec<char> {
    lines[0]
        .chars()
        .collect()
}

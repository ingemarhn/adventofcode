use std::collections::{HashMap, HashSet};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn get_prio(c: char) -> u32 {
    (match c {
        'A' ..= 'Z' => c as u8 - 64 + 26,
        _           => c as u8 - 96,
    }) as u32
}

fn task_a(lines: &[String]) {
    let prio_sum =
        lines
        .iter()
        .fold(0, |sum, line| {
            let l_parts = line.split_at(line.len() / 2);
            let mut prio = 0;
            for c in l_parts.0.chars() {
                if l_parts.1.contains(c) {
                    prio = get_prio(c);

                    break;
                }
            }

            sum + prio
        });

    println!("Sum of priorities = {}", prio_sum);
}

fn task_b(lines: &[String]) {
    let prio_sum =
        lines
        .chunks(3)
        .fold(0, |sum, chunks| {
            let mut char_elf = HashMap::new();
            let mut cnt = 0;

            for s in chunks {
                cnt += 1;
                for c in s.chars() {
                    let e = char_elf.entry(c).or_insert(HashSet::new());
                    e.insert(cnt);
                }
            }

            let mut ce_iter = char_elf.iter();
            sum +
                loop {
                    if let Some((c, hs)) = ce_iter.next() {
                        if hs.len() == 3 {
                            break get_prio(*c)
                        }
                    }
                }
        });

    println!("Sum of priorities = {}", prio_sum);
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    pub type Instructions = Vec<Instruction>;

    #[derive(Debug)]
    pub enum Instruction {
        Noop,
        Addx(i8),
    }

    impl Instruction {
        pub fn new(inst: &str, val: Option<i8>) -> Self {
            match inst {
                "noop" => Self::Noop,
                "addx" => Self::Addx(val.unwrap()),
                x => panic!("No, no, no... {x}"),
            }
        }

        pub fn n_cycles_req(&self) -> u16 {
            match *self {
                Self::Noop => 1,
                Self::Addx(_) => 2,
            }
        }

        pub fn add_to_x(&self) -> i8 {
            match *self {
                Self::Noop => 0,
                Self::Addx(v) => v,
            }
        }
    }
}
use internal::*;

fn task_a(lines: &[String]) {
    let instructions = parse_input(lines);

    let mut cycle_check = 20;
    let mut sum_sig_stren = 0_i32;
    let mut reg_x = 1;
    let mut instr_i = 0;
    let mut last_cyc = 0;

    for cyc in 1 .. {
        if cyc == cycle_check {
            sum_sig_stren += reg_x as i32 * cyc as i32;
            cycle_check += 40;
            if cycle_check > 220 {
                break;
            }
        }

        if last_cyc + instructions[instr_i].n_cycles_req() == cyc {
            reg_x += instructions[instr_i].add_to_x();
            last_cyc = cyc;
            instr_i += 1;
        }

        if instr_i == instructions.len() {
            break;
        }
    }

    println!("Sum signal strengths = {}", sum_sig_stren);
}

fn task_b(lines: &[String]) {
    let instructions = parse_input(lines);

    let mut reg_x = 1;
    let mut instr_i = 0;
    let mut cyc_count = instructions[instr_i].n_cycles_req();
    let mut screen = Vec::new();

    for _ in 0 .. 6 {
        let mut row = String::new();

        for pos_x in 0 .. 40 {
            let px = if (reg_x - 1 ..= reg_x + 1).contains(&(pos_x as i8)) { '#' } else { '.' };
            row.push(px);
            cyc_count -= 1;
            if cyc_count == 0 {
                reg_x += instructions[instr_i].add_to_x();
                instr_i += 1;
                if instructions.get(instr_i).is_none() {
                    break;
                }
                cyc_count = instructions[instr_i].n_cycles_req();
            }
        }

        screen.push(row);
    }

    for row in screen {
        println!("{}", row);
    }
}

fn parse_input(lines: &[String]) -> Instructions {
    lines
        .iter()
        .map(|line| {
            let mut parts =
                line
                .split(' ');

            match parts.next().unwrap() {
                p @ "noop" => Instruction::new(p, None),
                p @ "addx" => Instruction::new(p, Some(parts.next().unwrap().parse().unwrap())),
                p => panic!("Noooo... {p}"),
            }
        })
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod tests10 {
    use super::*;

    #[test]
    fn t_enum() {
        Instruction::new("noop", None);
        Instruction::new("addx", Some(8));
    }
}

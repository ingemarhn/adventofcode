#!/snap/bin/pwsh -NoProfile

param (
    [parameter(Mandatory = $true, HelpMessage = "AOC year")]
    [String]$year
)

$ErrorActionPreference = 'Stop'

$template = @"
#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]

mod internal {

}
use internal::*;

fn task_a(lines: &[String]) {
    let _ = parse_input(lines);
}

#task_b#

fn parse_input(lines: &[String]) -> usize {
    // lines
    //     .iter()
    //     .map(|line|
    //         line
    //         .split(", ")
    //     )
    //     .collect::<Vec<_>>()

    // lines[0]
    //     .split(',') | .chars()
    //     .map(|l| l.parse().unwrap())
    //     .collect()

    // lines[0].chars()

    // let path_chars = lines
    //     .iter()
    //     .map(|l| l
    //         .chars()
    // -possibly: .map(|c| c as u8)
    //         .collect::Vec<_>>()
    //     )
    //     .collect::Vec<_>>();

    // lines.iter().fold(Vec::new(), |mut splits, line| {
    //     splits.push(line.split('\t'));
    //     splits
    // })

    // for line in lines {
    // }

    0
}

fn main() {
    let (task, sample) = common::parse_command_line(std::env::args().collect());
    common::run_task(file!(), sample.as_ref(), &task, task_a, task_b);
}

#[cfg(test)]
#[allow(unused_imports)]
mod tests {
    use super::*;

    #[test]
    fn tests_t0() {}
}

"@
$task_b_std = @"
fn task_b(lines: &[String]) {
    let _ = parse_input(lines);
}
"@
$task_b_25 = @"
fn task_b(_lines: &[String]) {
    println!("No real task B on day 25");
}
"@

$aoc_folder = "aoc$year"

$cargo = @"
[package]
name = "$aoc_folder"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
# common = { path = "../common" }   # Use for uncommitted changes
# common = { git="file:/home/ingemar/adventofcode/", branch = "main" }    # Use for changes committed to main
common = { git="file:/home/ingemar/adventofcode/", rev="common_0.8.0" }    # Use for finnished AOC year to avoid compiler errors if incompatible changes are introduced

###itertools = "0.10"
###lazy_static = "1.4"
###regex = "1"
###either = "1.6"

[profile.dev]
opt-level = 0               # No optimizations.
debug = 2
"@

Push-Location $PSScriptRoot
try {
    cargo init $aoc_folder
    Remove-Item $aoc_folder/src/main.rs
    New-Item -Type Directory $aoc_folder/input | Out-Null

    $src = "$aoc_folder/src/bin"
    New-Item -ItemType Directory $src | Out-Null

    $cargo | Out-File -Encoding utf8 $aoc_folder/Cargo.toml

    1..24 | ForEach-Object {
        $d = '{0:d2}' -f $_
        ($template -replace '#day#', $d) -replace '#task_b#', $task_b_std | Out-File -Encoding utf8 $src/day$d.rs
    }
    ($template -replace '#day#', 25) -replace '#task_b#', $task_b_25 | Out-File -Encoding utf8 $src/day25.rs

    Get-ChildItem -Recurse $aoc_folder -Exclude .git
}
finally {
    Pop-Location
}

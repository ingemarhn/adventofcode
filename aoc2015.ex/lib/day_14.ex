defmodule Aoc2015.Day14 do
  def getInput(_d, _t), do: ["Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.",
                             "Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds."]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defstruct speeds: nil, isFlying: true, fliedDistance: 0, secondsInMode: 0, secondsLimits: nil, leaderPoints: 0

  defp parseInput lines do
    Enum.reduce(lines, %{}, fn line, parsed ->
      [reinDeer, speed, flySeconds, restSeconds] = Regex.run(~r|(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds|, line, capture: :all_but_first)
      flySeconds = String.to_integer(flySeconds)
      restSeconds = String.to_integer(restSeconds)
      Map.put(parsed, reinDeer, %{speed: String.to_integer(speed), flySeconds: flySeconds, restSeconds: restSeconds, cycleSeconds: flySeconds + restSeconds, leaderPoints: 0})
    end)
  end

  defp parseInputb lines do
    Enum.reduce(lines, %{}, fn line, parsed ->
      [reinDeer, speed, flySeconds, restSeconds] = Regex.run(~r|(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds|, line, capture: :all_but_first)
      Map.put(parsed, reinDeer, %__MODULE__{speeds: %{true: String.to_integer(speed), false: 0}, secondsLimits: %{true: String.to_integer(flySeconds), false: String.to_integer(restSeconds)} })
    end)
  end

  def a(d) do
    {lines, secondsLimit} =
      if false do
        {getInput(d, 1), 1000}
      else
        {getInput(d), 2503}
      end

      parseInput(lines)
        |> Enum.reduce(0, fn {_deerName, attributes}, winnerDistance ->
            nLoops = floor(secondsLimit / attributes[:cycleSeconds])
            remainFlySeconds = rem(secondsLimit, attributes[:cycleSeconds]) |> min(attributes[:flySeconds])
            max(winnerDistance, nLoops * attributes[:speed] * attributes[:flySeconds] + remainFlySeconds * attributes[:speed])
          end)
      |> IO.inspect
  end

  defp flyCalc reinDeers, 0 do
    Enum.reduce(reinDeers, 0, fn {_n, attributes}, winnerPoints ->
      max(winnerPoints, attributes.leaderPoints)
    end)
  end
  defp flyCalc reinDeers, secondsLeft do
    updReinDeers = Enum.reduce(reinDeers, %{}, fn {deerName, attributes}, newDeers ->
      attributes = %{attributes | fliedDistance: attributes.fliedDistance + attributes.speeds[attributes.isFlying]}
      attributes = %{attributes | secondsInMode: attributes.secondsInMode + 1}
      Map.put(newDeers, deerName,
        if attributes.secondsInMode == attributes.secondsLimits[attributes.isFlying] do
          tmp = %{attributes | secondsInMode: 0}
          %{tmp | isFlying: !attributes.isFlying}
        else
          attributes
        end
      )
    end)

    {_, leaders} = Enum.reduce(updReinDeers, {0, []}, fn {deerName, attributes}, {leadDistance, leaders} ->
      cond do
        attributes.fliedDistance > leadDistance -> {attributes.fliedDistance, [deerName]}
        attributes.fliedDistance == leadDistance -> {attributes.fliedDistance, [deerName | leaders]}
        true -> {leadDistance, leaders}
      end
    end)

    Enum.reduce(leaders, updReinDeers, fn leader, updReinDeers ->
      leaderAttrib = Map.fetch!(updReinDeers, leader)
      leaderAttrib = %{leaderAttrib | leaderPoints: leaderAttrib.leaderPoints + 1}
      Map.put(updReinDeers, leader, leaderAttrib)
    end)
    |> flyCalc(secondsLeft - 1)
  end

  def b(d) do
    t1 = :timer.tc(fn x -> Enum.each(1..x, fn _ -> b1(d) end) end, [100])
    t2 = :timer.tc(fn x -> Enum.each(1..x, fn _ -> b2(d) end) end, [100])
    {t1,t2}  |> IO.inspect
  end

  def b1(d) do
    {lines, secondsLimit} =
      if false do
        {getInput(d, 1), 1000}
      else
        {getInput(d), 2503}
      end

    parseInputb(lines)
    |> flyCalc(secondsLimit)
    |> IO.inspect
  end

  # b2 is roughly twice as fast as b1
  def b2(d) do
    {lines, secondsLimit} = if false do {getInput(d, 1), 1000} else {getInput(d), 2503} end

    getDistance = fn seconds, flySecs, restSecs, speed ->
      cycleSeconds = flySecs + restSecs
      nLoops = floor(seconds / cycleSeconds)
      remainFlySeconds = rem(seconds, cycleSeconds) |> min(flySecs)
      (nLoops * flySecs + remainFlySeconds) * speed
    end
    parsedLines = parseInput(lines)
    Enum.reduce(1..secondsLimit, parsedLines, fn secsFlied, deerData ->
      {roundWinnerNames, _} = Enum.reduce(deerData, {[], 0}, fn {deerName, attributes}, {leaders, leaderDistance} ->
        currDistance = getDistance.(secsFlied, attributes[:flySeconds], attributes[:restSeconds], attributes[:speed])
        cond do
          currDistance >  leaderDistance -> {[deerName], currDistance}
          currDistance == leaderDistance -> {[deerName | leaders], currDistance}
          true                           -> {leaders, leaderDistance}
        end
      end)

      Enum.reduce(roundWinnerNames, deerData, fn roundWinnerName, deerData ->
        roundWinnerData = deerData[roundWinnerName]
        points = Map.fetch!(roundWinnerData, :leaderPoints)
        roundWinnerData = Map.put(roundWinnerData, :leaderPoints, points + 1)
        Map.put(deerData, roundWinnerName, roundWinnerData)
      end)
    end)
    |> Enum.reduce(0, fn {_, attributes}, winnerPoints ->
      max(winnerPoints, attributes[:leaderPoints])
    end)
    |> IO.inspect
  end
end

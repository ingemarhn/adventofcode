defmodule Aoc2015.Day8 do
  def getInput(_d, _t), do: ["\"\"", "\"abc\"", "\"aaa\\\"aaa\"", "\"\\x27\""]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  def a(d) do
    lines = getInput(d)

    sumLiterals = Enum.reduce(lines, 0, fn line, sum ->
      sum + String.length(line)
    end)

    sumMem = Enum.reduce(lines, 0, fn line, sum ->
      sum + (line |> Code.eval_string |> elem(0) |> String.length)
    end)

    IO.puts sumLiterals - sumMem
  end

  def b(d) do
    lines = getInput(d)

    sumLiterals = Enum.reduce(lines, 0, fn line, sum ->
      sum + String.length(line)
    end)

    sumEncoded = Enum.reduce(lines, 0, fn line, sum ->
      sum + ((Regex.replace(~r/([\"\\])/, line, "\\\\\\1")) |> String.length()) + 2
    end)

    IO.puts sumEncoded - sumLiterals
  end
end

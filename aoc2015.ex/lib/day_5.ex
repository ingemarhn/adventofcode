defmodule Aoc2015.Day5 do
  def getInput(_d, _t), do: ["ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb"]
  def getInput(_d, _t, _tt), do: ["qjhvhtzxzqqjkmpb", "xxyxx", "uurcxstgmygtbstg", "ieodomkazucvgmuy"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp checkVowels?(line) do
    lineVless = Regex.replace(~r/a|e|i|o|u/, line, "", global: true)
    (String.length(line) - String.length(lineVless)) >= 3
  end

  def a(d) do
    lines = getInput(d)
    nice = Enum.reduce(lines, [], fn line, niceLines ->
      if !Regex.match?(~r/ab|cd|pq|xy/, line) && Regex.match?(~r/(.)\1/, line) && checkVowels?(line) do
        [line | niceLines]
      else
        niceLines
      end
    end)

    IO.puts length(nice)
  end

  def b(d) do
    lines = getInput(d)
    nice = Enum.reduce(lines, [], fn line, niceLines ->
      if Regex.match?(~r/(..).*\1/, line) && Regex.match?(~r/(.).\1/, line) do
        [line | niceLines]
      else
        niceLines
      end
    end)

    IO.puts length(nice)
  end
end

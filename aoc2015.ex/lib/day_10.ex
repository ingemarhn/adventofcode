defmodule Aoc2015.Day10 do
  def getInput(_d, _t), do: "1"
  def getInput(_d), do: "1113122113"

  def getNext figures, currentFig, figCount, outString do
    getOutStr = fn str, count, fig ->
      str <> Integer.to_string(count) <> fig
    end

    if !Enum.empty?(figures) do
      thisFig = hd figures
      if thisFig == currentFig do
        getNext(tl(figures), currentFig, figCount + 1, outString)
      else
        getNext(tl(figures), thisFig, 1, getOutStr.(outString, figCount, currentFig))
      end
    else
      getOutStr.(outString, figCount, currentFig)
    end
  end

  def a(d) do
    text = getInput(d)
    figures = Enum.reduce(1..40, text, fn _n, figText ->
      figures = String.split(figText, "", trim: true)
      restFigs = tl(figures)
      getNext(restFigs, hd(figures), 1, "")
    end)

    IO.puts String.length figures
  end

  def b(d) do
    text = getInput(d)
    figures = Enum.reduce(1..50, text, fn _n, figText ->
      figures = String.split(figText, "", trim: true)
      restFigs = tl(figures)
      getNext(restFigs, hd(figures), 1, "")
    end)

    IO.puts String.length figures
  end
end

defmodule Aoc2015.Day12 do
  def getInput(_d, _t), do: "[1,{\"c\":\"red\",\"b\":2},3]"
  def getInput(_d, _t,_tt), do: "{\"d\":\"red\",\"e\":[1,2,3,4],\"f\":5}"
  def getInput(d), do: Aoc2015.InputFile.getText(d)

  defp extractRed json do
    cond do
      is_list(json) -> Enum.reduce(json, [], fn el, jsonList -> [extractRed(el) | jsonList] end) #|> Enum.reverse
      is_map(json)  -> if "red" in Map.values(json), do: 0, else: Enum.reduce(Map.keys(json), %{}, fn el, jsonMap -> Map.put(jsonMap, el, extractRed(json[el])) end)
      true          -> json
    end
  end

  defp countInts(json) do
    cond do
      is_list(json)    -> Enum.reduce(json, 0, fn el, sum -> sum + countInts(el) end)
      is_map(json)     -> Enum.reduce(json, 0, fn el, sum -> sum + countInts(el|>IO.inspect) end)
      is_integer(json) -> json
      true             -> 0
    end
  end

  def a(d) do
    text = getInput(d)
    {_, json} = JSON.decode(text)

    countInts(json)
    |> IO.inspect
  end

  def b(d) do
    text = getInput(d)
    {_, json} = JSON.decode(text)
    nJson = extractRed(json)

    countInts(nJson)
    |> IO.inspect
  end
end

defmodule Aoc2015.Day6 do
  def getInput(_d, _t), do: ["turn on 0,0 through 999,999", "toggle 0,0 through 999,0", "turn off 499,499 through 500,500"]
  def getInput(_d, _t, _tt), do: ["turn on 0,0 through 0,0", "toggle 0,0 through 999,999"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp getGrid do
    r = Enum.reduce(0..999, %{}, &(Map.put(&2, &1, 0)))
    Enum.reduce(0..999, %{}, &(Map.put(&2, &1, r)))
  end

  defp getUpdFuncsA do
    %{
      "turn on"  => fn key, leds -> Map.replace(leds, key, 1) end,
      "turn off" => fn key, leds -> Map.replace(leds, key, 0) end,
      "toggle"   => fn key, leds -> Map.replace(leds, key, (if leds[key] == 1, do: 0, else: 1)) end
    }
  end

  defp getUpdFuncsB do
    %{
      "turn on"  => fn key, leds -> Map.replace(leds, key, leds[key] + 1) end,
      "turn off" => fn key, leds -> Map.replace(leds, key, (if leds[key] - 1 >= 0, do: leds[key] - 1, else: 0)) end,
      "toggle"   => fn key, leds -> Map.replace(leds, key, leds[key] + 2) end
    }
  end

  defp countLight grid do
    Enum.reduce(0..999, 0, fn row, count ->
      rowLeds = grid[row]
      Enum.reduce(0..999, count, fn col, counter ->
        counter + rowLeds[col]
      end)
    end)
  end

  defp updateGrid lines, updFunctions do
    Enum.reduce(lines, getGrid(), fn line, mainGrid ->
      [func, colStart, rowStart, colEnd, rowEnd] = Regex.run(~r/(.+)(?= \d) (\d+),(\d+) through (\d+),(\d+)/, line, capture: :all_but_first)
      [colStart, rowStart, colEnd, rowEnd] = Enum.map([colStart, rowStart, colEnd, rowEnd], &(String.to_integer(&1)))
      Enum.reduce(rowStart..rowEnd, mainGrid, fn row, nGrid ->
        nRow = Enum.reduce(colStart..colEnd, nGrid[row], fn col, gridLine ->
          updFunctions[func].(col, gridLine)
        end)
        Map.replace(nGrid, row, nRow)
      end)
    end)
  end

  def a(d) do
    grid = updateGrid getInput(d), getUpdFuncsA()
    IO.puts countLight grid
  end

  def b(d) do
    grid = updateGrid getInput(d), getUpdFuncsB()
    IO.puts countLight grid
  end
end

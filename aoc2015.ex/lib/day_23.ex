defmodule Aoc2015.Day23 do
  defp getInput(_d, "1"), do: ""
  defp getInput(d, "0"), do: Aoc2015.InputFile.getLines(d)
  defp parseInput(lines) do
    Enum.reduce(lines, %{}, fn line, prog ->
      parts = String.split(line, [" ", ","])
      possibleRegister = String.to_atom(Enum.at(parts, 1))
      {instruction, params} = case Enum.at(parts, 0) do
        "hlf" ->
          {&hlf/2, {possibleRegister}}
        "tpl" ->
          {&tpl/2, {possibleRegister}}
        "inc" ->
          {&inc/2, {possibleRegister}}
        "jmp" ->
          {&jmp/2, {String.to_integer(Enum.at(parts, 1))}}
        "jie" ->
          {&jie/2, {possibleRegister, String.to_integer(Enum.at(parts, 3))}}
        "jio" ->
          {&jio/2, {possibleRegister, String.to_integer(Enum.at(parts, 3))}}
      end
      Map.put(prog, map_size(prog) + 1, {instruction, params})
    end)
  end

  defp handleRegister(value) do
    receive do
      {:calc, func, modValue} ->
        Kernel.round(func.(value, modValue))
      {:set, value} ->
        value
      {:get, caller} ->
        send(caller, value)
        value
    end
    |> handleRegister
  end

  defp getRegisterValue(register) do
    send(register, {:get, self()})
    receive do
      value -> value
    end
  end

  defp hlf({register}, pointer) do
    send(register, {:calc, &Kernel.//2, 2})
    pointer + 1
  end
  defp tpl({register}, pointer) do
    send(register, {:calc, &Kernel.*/2, 3})
    pointer + 1
  end
  defp inc({register}, pointer) do
    send(register, {:calc, &Kernel.+/2, 1})
    pointer + 1
  end
  defp jmp({move}, pointer) do
    pointer + move
  end
  defp jmpIf(doJump, move, pointer) do
    if doJump do
      jmp({move}, pointer)
    else
      pointer + 1
    end
  end
  defp jie({register, move}, pointer) do
    jmpIf(rem(getRegisterValue(register), 2) == 0, move, pointer)
  end
  defp jio({register, move}, pointer) do
    jmpIf(getRegisterValue(register) == 1, move, pointer)
  end

  defp runProgram(program, programPointer) do
    if programPointer <= map_size(program) do
      {instruction, params} = program[programPointer]
      progPointer = instruction.(params, programPointer)

      runProgram(program, progPointer)
    else
      getRegisterValue(:b)
    end
  end

  def a(d, t) do
    lines = getInput(d, t)
    {_, pida} = Task.start_link(fn -> handleRegister(0) end)
    Process.register(pida, :a)
    {_, pidb} = Task.start_link(fn -> handleRegister(0) end)
    Process.register(pidb, :b)

    parseInput(lines)
    |> runProgram(1)
    |> IO.inspect
  end

  def b(d, t) do
    lines = getInput(d, t)
    send(:a, {:set, 1})
    send(:b, {:set, 0})

    parseInput(lines)
    |> runProgram(1)
    |> IO.inspect
  end
end

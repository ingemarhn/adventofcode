# Start at house 0,0
# Loop through all instructions and deliver presents
# Count number of times each house is visited
#   A tuple represents the current house
#   A map handles the visits count

defmodule Aoc2015.Day3 do
  def getInput(_d, _t), do: "^>v<"
  def getInput(_d, _t, _tt), do: "^v^v^v^v^v"
  def getInput(d), do: Aoc2015.InputFile.getText(d)

  defp nextPosition(dir, x, y) do
    case dir do
        "^" -> {x, y + 1}
        ">" -> {x + 1, y}
        "v" -> {x, y - 1}
        "<" -> {x - 1, y}
      end
  end

  defp calcVisits(directions) do
    # Santa starts at the house where he is and delivers one package
    Enum.reduce(directions, {{0,0}, %{{0,0} => 1}}, fn direction, acc ->
      {{x,y}, houses} = acc
      position = nextPosition(direction, x, y)
      houses = if !Map.has_key?(houses, position), do: Map.put(houses, position, 1), else:  %{houses | position => houses[position] + 1}
      {position, houses}
    end)
  end

  def a(d) do
    directions = String.split(getInput(d), "", trim: true)
    {_, houseVisits} = calcVisits(directions)
    IO.puts length(Map.keys(houseVisits))
  end

  def b(d) do
    directions = Regex.scan(~r/../, getInput(d))
    {sDirections, rDirections} = Enum.reduce(directions, {[], []}, fn [directionPair], {sDirs, rDirs} ->
      [sDirection, rDirection] = String.split(directionPair, "", trim: true)
      {[sDirection | sDirs], [rDirection | rDirs]}
    end)
    {_, sHouseVisits} = calcVisits(Enum.reverse(sDirections))
    {_, rHouseVisits} = calcVisits(Enum.reverse(rDirections))
    IO.puts length(Map.keys(Map.merge(sHouseVisits, rHouseVisits)))
  end
end

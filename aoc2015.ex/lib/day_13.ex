defmodule Aoc2015.Day13 do
  def getInput(_d, _t), do: String.split( String.trim(File.read!(__DIR__ <> "/../input/day-13-test.txt")), "\n" )
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp parseInput(lines) do
    Enum.map(lines, fn line ->
      Regex.run(~r/(\w+) would (\w+) (\d+) .*?(\w+)\./, line, capture: :all_but_first)
    end)
  end

  defp getGuestNames lines do
    Enum.reduce(lines, [], fn line, guests ->
      guestName = hd(line)
      unless Enum.member?(guests, guestName), do: [guestName | guests], else: guests
    end)
  end

  defp getHappinessList lines do
    Enum.reduce(lines, %{}, fn line, happy ->
      [guest, unaryStr, happiness, potentialNeighbor] = line
      happinessVal = if unaryStr == "gain", do: String.to_integer(happiness), else: -String.to_integer(happiness)

      Map.put(happy, {guest, potentialNeighbor}, happinessVal)
    end)
  end

  defp permutations([]), do: [[]]
  defp permutations(list), do: for elem <- list, rest <- permutations(list--[elem]), do: [elem|rest]

  defp findUltimateHappiness lines do
    newHappy = fn happinessList, happy, guest1, guest2 ->
      happy +
        Map.fetch!(happinessList, {guest1, guest2}) +
        Map.fetch!(happinessList, {guest2, guest1})
    end

    happinessList = getHappinessList(lines)
    getGuestNames(lines)
      |> permutations
      |> Enum.reduce(0, fn guestListTry, currentBestHappy ->
          firstGuest = hd(guestListTry)
          {lastGuest, happiness} = Enum.reduce(guestListTry -- [firstGuest], {firstGuest, 0}, fn guest, {prevGuest, happy} ->
            {guest, newHappy.(happinessList, happy, prevGuest, guest)}
          end)
          max(newHappy.(happinessList, happiness, firstGuest, lastGuest), currentBestHappy)
        end)
  end

  def a(d) do
    lines =
      getInput(d)
      |> parseInput

    findUltimateHappiness(lines)
    |> IO.inspect
  end

  def b(d) do
    linesInp = getInput(d)
    parseInput(linesInp)
      |> getGuestNames
      |>  Enum.reduce(linesInp, fn guest, addLines ->
            [ "Me would gain 0 happiness units by sitting next to #{guest}." |
              [ "#{guest} would gain 0 happiness units by sitting next to Me." | addLines ]
            ]
          end)
      |> parseInput
      |> findUltimateHappiness
      |> IO.inspect
  end
end

defmodule Aoc2015.Day19 do
  def getInput(_d, _t), do: ["H => HO", "H => OH", "O => HH", "e => H", "e => O", "", "HOHOHO"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp parseLines(lines) do
    handleResult = fn
      nil, ret -> ret
      (matches, {swap, _}) when hd(matches) == "" -> {swap, List.last(matches)}
      (matches, {swap, str}) ->
        [key, value] = matches
        valueList = if swap[key], do: [value | swap[key]], else: [value]
        {Map.put(swap, key, valueList), str}
    end

    Enum.reduce(lines, {%{}, ""}, fn line, ret ->
      Regex.run(~r/^(\w+) => (\w+)|(.+)/, line, capture: :all_but_first)
      |> handleResult.(ret)
    end)
  end

  defp replaceInMolecule(_, "", _, variants), do: variants
  defp replaceInMolecule(preStr, restStr, swaps, variants) do
    [atom, restAtoms] = Regex.run(~r/^([[:upper:]][[:lower:]]?)(.*)/, restStr, capture: :all_but_first)
    updVariants = if swaps[atom] != nil do
      Enum.reduce(swaps[atom], variants, fn replAtom, variantz ->
        Map.put(variantz, preStr <> replAtom <> restAtoms, 1)
      end)
    else
      variants
    end
    replaceInMolecule(preStr <> atom, restAtoms, swaps, updVariants)
  end

  def a(d) do
    lines = getInput(d)
    {swaps, molecule} = parseLines(lines)
    replaceInMolecule("", molecule, swaps, %{})
    |> Kernel.map_size
    |> IO.inspect
  end

  defp tryReplaceMoleculePart(molecule, _swaps, _allSwaps, replaceCount) when molecule == "e" do
    replaceCount
  end
  defp tryReplaceMoleculePart(molecule, swapsToTest, allSwaps, replaceCount) do
    [{swapCandidate, swapReplacement} | restSwaps] = swapsToTest
    moleculeRest = String.replace(molecule, swapCandidate, swapReplacement)
    if moleculeRest != molecule do
      lenOld = String.length(molecule)
      lenNew = String.length(moleculeRest)
      count = if lenOld != lenNew do
        (lenOld - lenNew) / (String.length(swapCandidate) - String.length(swapReplacement))
      else
        moleculeTest = String.replace(molecule, swapCandidate, "")
        (lenOld - String.length(moleculeTest)) / String.length(swapCandidate)
      end
        tryReplaceMoleculePart(moleculeRest, allSwaps, allSwaps, replaceCount + count)
    else
      tryReplaceMoleculePart(molecule, restSwaps, allSwaps, replaceCount)
    end
  end

  def b(d) do
    lines = getInput(d)
    {swapsStr, molecule} = parseLines(lines)

    swaps = Enum.reduce(swapsStr, [], fn {ind, replAtoms}, swapList ->
      countedList = Enum.map(replAtoms, fn atoms ->
        count = Regex.replace(~r/[[:lower:]]/, atoms, "") |> String.length
        {count, atoms, ind}
      end)
      [countedList | swapList]
    end)
    |> Enum.concat
    |> Enum.sort(fn {lenA, _x, ind}, {lenB, _z, _w} -> (lenA >= lenB) && (ind != "e") end)
    |> Enum.map(fn {_, ind, repl} -> {ind, repl} end)
    tryReplaceMoleculePart(molecule, swaps, swaps, 0)
    |> IO.inspect
  end
end

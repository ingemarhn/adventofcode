defmodule Aoc2015.Day17 do
  def getInput(_d, _t), do: ["20", "15", "10", "5", "5"]
  #def getInput(_d, _t), do: ["1", "2", "3", "4", "5"]
  #def getInput(_d, _t), do: ["1", "2", "3", "4"]
  #def getInput(_d, _t), do: ["1", "2", "3"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

# Replace by: for x <- elements, y <- elements, x < y, do: [x, y]
#  defp findCombinations [] do
#    []
#  end
#  defp findCombinations [firstContainer | restContainers] do
#    pairs = Enum.map(restContainers, fn container ->
#      [firstContainer, container]
#    end)
#    pairs ++ findCombinations(restContainers)
#  end
#  defp findCombinations(combinators, _) when Kernel.length(combinators) == 1 do
#    []
#  end
#  defp findCombinations combinators, [firstContainer | restContainers] do
#    restCombinators = Enum.drop_while(combinators, fn el -> hd(el) <= firstContainer end)
#    above = findCombinations(restCombinators, restContainers)
#    Enum.map(restCombinators, fn el -> [firstContainer | el] end) ++ above
#  end
#  defp findAllCombinations list, containers, maxLen do
#    thisList = findCombinations(list, containers)
#    if Kernel.length(hd(thisList)) < maxLen do
#      thisList ++ findAllCombinations thisList, containers, maxLen
#    else
#      thisList
#    end
#  end
  defp findAllCombinations(list, _containers, maxLen) when Kernel.length(hd(list)) == maxLen, do: list
  defp findAllCombinations list, containers, maxLen do
    # Replaced by below: thisList = findCombinations(list, containers)
    thisList = for container <- containers, combination <- list, container < hd(combination), do: [container | combination]
    thisList ++ findAllCombinations thisList, containers, maxLen
  end

  def a(d) do
    {eggnogVolume, containerSizesStr} = {150, getInput(d)}
    #{eggnogVolume, containerSizesStr} = {25, getInput(d,1)}

    containerSizes = Enum.map(containerSizesStr, & String.to_integer(&1))
    {_, containers} = Enum.reduce(containerSizes, {0, %{}}, fn size, {n, acc} -> n = n + 1; {n, Map.put(acc,n, size)}; end)
    containerKeys = Map.keys(containers)

    # Replaced by below line: pairs = findCombinations(containerKeys)
    pairs = for x <- containerKeys, y <- containerKeys, x < y, do: [x, y]
    {nCombinations, minCombinations, listOfPossibleMin} = pairs ++ findAllCombinations(pairs, containerKeys, Kernel.length(containerKeys))
    |> Enum.reduce({0, 99999, []}, fn combination, {matches, minMatches, minList} ->
      volume = Enum.reduce_while(combination, 0, fn key, sum ->
        rSum = sum + containers[key]
        if rSum <= eggnogVolume, do: {:cont, rSum}, else: {:halt, rSum}
      end)
      if volume == eggnogVolume do
        {matches + 1, Kernel.min(minMatches, Kernel.length(combination)), [combination | minList]}
      else
        {matches, minMatches, minList}
      end
    end)
    IO.puts nCombinations

    Enum.reduce(listOfPossibleMin, 0, fn possibleMin, nMins ->
      if Kernel.length(possibleMin) == minCombinations do
        nMins + 1
      else
        nMins
      end
    end)
    |> IO.inspect
  end

  def b(_d) do
  end
end

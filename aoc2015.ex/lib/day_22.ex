defmodule Aoc2015.Day22 do
  def getInput(_d, "1"), do: [boss: %{hitPoints: 13, damage: 8}, player: %{hitPoints: 10, mana: 250, armor: 0}]
  def getInput(_d, "2"), do: [boss: %{hitPoints: 14, damage: 8}, player: %{hitPoints: 10, mana: 250, armor: 0}]
  def getInput(_d, "0"), do: [boss: %{hitPoints: 71, damage: 10}, player: %{hitPoints: 50, mana: 500, armor: 0}]

  @spellCosts %{magicmissile: 53, drain: 73, shield: 113, poison: 173, recharge: 229}

#  instant:
#    Magic Missile costs 53 mana. It instantly does 4 damage.
#    Drain costs 73 mana. It instantly does 2 damage and heals you for 2 hit points.
#  effects:
#    Shield costs 113 mana. It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
#    Poison costs 173 mana. It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
#    Recharge costs 229 mana. It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.
# You start with 50 hit points and 500 mana points.
# The boss starts with 71 hit points and 10 damage
# What is the least amount of mana you can spend and still win the fight?

  defp executeEffect(effectsStates, effect, fighter, valueKey, value) do
    if effectsStates[effect] > 0, do: %{fighter | valueKey => fighter[valueKey] + value}, else: fighter
  end

  defp decreaseTimer(effectsStates, effect) do
    if effectsStates[effect] > 0, do: %{effectsStates | effect => effectsStates[effect] - 1}, else: effectsStates
  end

  defp calcEffects(playerProps, bossProps, effectsStates) do
    # Boss hit points
    bossProps = executeEffect(effectsStates, :poison, bossProps, :hitPoints, -3)
    # Player mana
    playerProps = executeEffect(effectsStates, :recharge, playerProps, :mana, 101)
    # Poison / Shield / Recharge timers
    effectsStates = decreaseTimer(effectsStates, :poison)
    |> decreaseTimer(:shield)
    |> decreaseTimer(:recharge)
    # Player armor
    playerProps = if effectsStates.shield == 0, do: %{playerProps | armor: 0}, else: %{playerProps | armor: 7}

    {playerProps, bossProps, effectsStates}
  end

  defp checkGameEndOrContinue(continueFunction, hardMode, playerProps, bossProps, effectsStates, minSpentMana) do
    cond do
      bossProps.hitPoints <= 0 ->
        {_, _, castList} = getCastList()
        usedManaThisTime = Enum.reduce(castList, 0, fn spell, sum ->
          sum + @spellCosts[spell]
        end)
        min(minSpentMana, usedManaThisTime)

      playerProps.hitPoints <= 0 || playerProps.mana < 0 ->
        minSpentMana

      true ->
        continueFunction.(hardMode, playerProps, bossProps, effectsStates, minSpentMana)
    end
  end

  defp playerCast(hardMode, playerProps, bossProps, effectsStates, minSpentMana) do
    playerProps = if hardMode, do: %{playerProps | :hitPoints => playerProps[:hitPoints] - 1}, else: playerProps
    {playerProps, bossProps, effectsStates} = calcEffects(playerProps, bossProps, effectsStates)

    isSpellAllowed? = fn spell, effectsStates ->
      spell in [:magicmissile, :drain] or
        effectsStates[spell] == 0
    end

    cast = fn spell, playerProps, bossProps, effectsStates ->
      if spell in [:shield, :poison, :recharge] do
        {costMana, lastsRounds} = %{shield: {@spellCosts.shield, 6}, poison: {@spellCosts.poison, 6}, recharge: {@spellCosts.recharge, 5}}[spell]
        {
          %{playerProps | mana: playerProps.mana - costMana},
          bossProps,
          %{effectsStates | spell => lastsRounds}
        }
      else
        {costMana, damage, heal} = %{magicmissile: {@spellCosts.magicmissile, 4, 0}, drain: {@spellCosts.drain, 2, 2}}[spell]
        playerProps = %{playerProps | mana: playerProps.mana - costMana}
        {
          %{playerProps | hitPoints: playerProps.hitPoints + heal},
          %{bossProps | hitPoints: bossProps.hitPoints - damage},
          effectsStates
        }
      end
    end

    Enum.reduce([:shield, :recharge, :drain, :poison, :magicmissile], minSpentMana, fn spell, minSpentMana ->
      if isSpellAllowed?.(spell, effectsStates) do
        {playerProps, bossProps, effectsStates} = cast.(spell, playerProps, bossProps, effectsStates)
        spellPush(spell)

        minSpentMana = checkGameEndOrContinue(&bossAttack/5, hardMode, playerProps, bossProps, effectsStates, minSpentMana)
        spellPop()
        minSpentMana
      else
        minSpentMana
      end
    end)
  end

  defp bossAttack(hardMode, playerProps, bossProps, effectsStates, minSpentMana) do
    {playerProps, bossProps, effectsStates} = calcEffects(playerProps, bossProps, effectsStates)
    # Player hit points
    playerProps = %{playerProps | hitPoints: playerProps.hitPoints - (bossProps.damage - playerProps.armor)}
    checkGameEndOrContinue(&playerCast/5, hardMode, playerProps, bossProps, effectsStates, minSpentMana)
  end

  defp trackCasts({list, count, level}) do
    receive do
      {:push, spell} ->
        # Add a spell to the list, increase total count and current level
        {[spell | list], count + 1, level + 1}
      :pop ->
        # Reduce list with one, keep total count and decrease current level
        {tl(list), count, level - 1}
      {:get, caller} ->
        # Send current state to caller
        send(caller, {count, level, Enum.reverse(list)})
        # Keep current state in this process
        {list, count, level}
    end
    |> trackCasts
  end
  defp spellPush(spell) do
    send(:casts, {:push, spell})
  end
  defp spellPop() do
    send(:casts, :pop)
  end
  defp getCastList() do
    send(:casts, {:get, self()})
    receive do
      list -> list
    end
  end

  def a(d, t) do
    [boss: bossProps, player: playerProps] = getInput(d, t)
    {_, pid} = Task.start_link(fn -> trackCasts({[], 0, 0}) end)
    Process.register(pid, :casts)

    playerCast(false, playerProps, bossProps, %{poison: 0, shield: 0, recharge: 0}, 999999)
    |> IO.inspect(label: "a")

    playerCast(true, playerProps, bossProps, %{poison: 0, shield: 0, recharge: 0}, 999999)
    |> IO.inspect(label: "b")
  end

  def b(_d, _t) do
  end
end

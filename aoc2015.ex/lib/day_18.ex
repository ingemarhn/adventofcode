defmodule Aoc2015.Day18 do
  def getInput(_d, _t), do: [".#.#.#", "...##.", "#....#", "..#...", "#.#..#", "####.."]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp startState nghbrs do
    {:ok, pid} = Agent.start_link(fn -> %{light: :off, neighbors: nghbrs} end)
    pid
  end

  defp neighborsAsPids matrix do
    Enum.each(matrix, fn {_c, pid} ->
      Agent.cast(pid, fn state ->
        neighborsPids = Enum.reduce(state[:neighbors], [], fn nbor, pids ->
          [matrix[nbor] | pids]
        end)
        Map.put(state, :neighbors, neighborsPids)
      end)
    end)
  end

  defp setLightState pid, onOff do
    Agent.cast(pid, fn state ->
      if !state[:isStatic] do
        Map.put(state, :light, onOff)
      else
        state
      end
    end)
  end

  defp turnOn pid do
    setLightState(pid, :on)
  end

  defp turnOff pid do
    setLightState(pid, :off)
  end

  defp isTurnedOn? pid do
    Agent.get(pid, fn state -> Map.get(state, :light) == :on end)
  end

  defp countLitNeighbors matrix do
    Enum.each(matrix, fn {_, pid} ->
      Agent.update(pid, fn state ->
        nLit = Enum.reduce(state[:neighbors], 0, fn nborPid, count ->
          count + %{true: 1, false: 0}[isTurnedOn?(nborPid)]
        end)
        Map.put(state, :nLitNeighbors, nLit)
      end)
    end)
  end

  defp getNlitNeighbors pid do
    Agent.get(pid, fn state -> state[:nLitNeighbors] end)
  end

  defp setNeighBors r, c, nLines, lineLen do
    [{r-1, c-1}, {r-1, c}, {r-1, c+1}, {r, c+1}, {r+1, c+1}, {r+1, c}, {r+1, c-1}, {r, c-1}]
    |> Enum.filter(fn {row, col} ->
      row > 0 and row <= nLines and col > 0 and col <= lineLen
    end)
  end

  defp getMatrixStates nLines, lineLen do
    for r <- 1..nLines, c <- 1..lineLen, into: %{} do
      pid = setNeighBors(r, c, nLines, lineLen)
      |> startState
      {{r, c}, pid}
    end
  end

  defp initLights lines, matrix do
    for {lNum, line} <- Enum.zip(1..111, lines), {cNum, char} <- Enum.zip(1..111, String.split(line, "", trim: true)) do
      if char == "#" do
        Agent.cast(matrix[{lNum, cNum}], fn state -> Map.put(state, :light, :on) end)
      end
    end
  end

  defp setStaticLights(matrix, nLines, lineLen) do
    for r <- [1,nLines], c <- [1,lineLen], do: Agent.update(matrix[{r,c}], fn state -> Map.put(state, :isStatic, true) |> Map.put(:light, :on) end)
  end

  defp updateLights matrix, nLoops, nLines, lineLen do
    for _n <- 1..nLoops do
      countLitNeighbors(matrix)
      for r <- 1..nLines, c <- 1..lineLen do
        cell = matrix[{r, c}]
        nLit = getNlitNeighbors(cell)
        if isTurnedOn?(cell) and (nLit not in [2, 3]) do
          turnOff(cell)
        end
        if !isTurnedOn?(cell) and (nLit == 3) do
          turnOn(cell)
        end
      end
    end
  end

  defp countLit matrix do
    Enum.reduce(matrix, 0, fn {_c, pid}, count ->
      count + (%{true: 1, false: 0}[isTurnedOn?(pid)])
    end)
  end

  def a(d) do
    {lines, nLoops} = {getInput(d), 100}
    #{lines, nLoops} = {getInput(d, 1), 4}
    nLines = Kernel.length(lines)
    lineLen = String.length(hd(lines))
    matrixStates = getMatrixStates(nLines, lineLen)
    initLights(lines, matrixStates)
    neighborsAsPids(matrixStates)

    updateLights(matrixStates, nLoops, nLines, lineLen)
    countLit(matrixStates)
    |> IO.inspect
    Enum.each(matrixStates, fn {_c, pid} -> Agent.stop(pid) end)
  end

  def b(d) do
    {lines, nLoops} = {getInput(d), 100}
    #{lines, nLoops} = {getInput(d, 1), 5}
    nLines = Kernel.length(lines)
    lineLen = String.length(hd(lines))
    matrixStates = getMatrixStates(nLines, lineLen)
    initLights(lines, matrixStates)
    setStaticLights(matrixStates, nLines, lineLen)
    neighborsAsPids(matrixStates)

    updateLights(matrixStates, nLoops, nLines, lineLen)
    countLit(matrixStates)
    |> IO.inspect
  end
end

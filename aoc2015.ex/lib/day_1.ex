defmodule Aoc2015.Day1 do
  def a(d) do
    parens = Aoc2015.InputFile.getText(d)
    levVals = %{"(" => 1, ")" => -1}
    lev = Enum.reduce(String.split(parens, "", trim: true), 0, fn(p, level) ->
      level + levVals[p]
    end)

    IO.puts lev
  end

  def b(d) do
    parens = Aoc2015.InputFile.getText(d)
    levVals = %{"(" => 1, ")" => -1}
    %{count: position} = Enum.reduce_while(String.split(parens, "", trim: true), %{count: 0, level: 0}, fn(p, acc) ->
      lev = acc[:level] + levVals[p]
      atm = if lev >= 0, do: :cont, else: :halt

      {atm, %{count: acc[:count] + 1, level: lev}}
    end)

    IO.puts position
  end
end

defmodule Mix.Tasks.Main do
  use Mix.Task

  defp getUsedTime(timerVal, func) do
    timerVal |> elem(0) |> Kernel./(1_000_000) |> IO.inspect(label: "Used time: " <> func)
  end

  def run(args) do
    [d, t] = Enum.take(parse_args(args) ++ ["0"], 2)

    module = String.to_atom("Elixir.Aoc2015.Day#{d}")
    #apply(module, String.to_atom("a"), [d])
    #apply(module, String.to_atom("b"), [d])
    :timer.tc(module, String.to_atom("a"), [d, t]) |> getUsedTime("a")
    :timer.tc(module, String.to_atom("b"), [d, t]) |> getUsedTime("b")
  end

  defp parse_args(args) do
    if length(args) == 0 do
      raise "Dag nr saknas (aoc2015 <day #>)"
    end

    {_, arguments, _} = OptionParser.parse(args, switches: [] )
    arguments
  end
end

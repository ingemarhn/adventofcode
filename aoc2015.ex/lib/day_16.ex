defmodule Aoc2015.Day16 do
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp parseInput(lines) do
    Enum.reduce(lines, %{}, fn line, parsed ->
      [sue, attribs] = Regex.run(~r/(Sue \d+): (.*)/, line, capture: :all_but_first)
      {attribs, _} = Code.eval_string("%{#{attribs}}")
      Map.put(parsed, sue, attribs)
    end)
  end

  def getTickerTape do
    %{
      children: 3,
      cats: 7,
      samoyeds: 2,
      pomeranians: 3,
      akitas: 0,
      vizslas: 0,
      goldfish: 5,
      trees: 3,
      cars: 2,
      perfumes: 1
    }
  end

  def a(d) do
    allSues = getInput(d)
    |> parseInput

    tickerTape = getTickerTape()
    Enum.each(allSues, fn {sue, attributes} ->
      cmp = Map.keys(attributes)
      |> Enum.reduce(%{}, fn key, nMap ->
        Map.put(nMap, key, Map.get(tickerTape, key))
      end)
      if cmp == attributes, do: IO.puts sue
    end)
  end

  def b(d) do
    allSues = getInput(d)
    |> parseInput

    tickerTape = getTickerTape()

    cmpFuncs = Enum.reduce(tickerTape, %{}, fn {attrib, _}, attribCmp ->
      cmp = cond do
        attrib == :cats        or attrib == :trees    -> &Kernel.>/2
        attrib == :pomeranians or attrib == :goldfish -> &Kernel.</2
        true                                          -> &Kernel.==/2
      end
      Map.put(attribCmp, attrib, cmp)
    end)

    Enum.each(allSues, fn {sue, attributes} ->
      sueFound = Map.keys(attributes)
      |> Enum.reduce_while(true, fn key, _found ->
        cmp = Map.get(cmpFuncs, key)
        v1 = Map.get(tickerTape, key)
        v2 = Map.get(attributes, key)
        if cmp.(v2, v1) do
          {:cont, true}
        else
          {:halt, false}
        end
      end)
      if sueFound, do: IO.puts sue
    end)
  end
end

defmodule Aoc2015.Day9 do
  def getInput(_d, _t), do: ["London to Dublin = 464", "London to Belfast = 518", "Dublin to Belfast = 141"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  def parseInput(lines) do
    Enum.map(lines, fn line ->
      Regex.run(~r/(\w+) to (\w+) = (\d+)/, line, capture: :all_but_first)
    end)
  end

  def getCities lines do
    firstCity = hd (hd lines)
    Enum.reduce_while(lines, [firstCity], fn line, cities ->
      if hd(line) == firstCity do
        city = Enum.fetch!(line, 1)
        {:cont, (if !Enum.member?(cities, city), do: [city | cities], else: cities)}
      else
        {:halt, cities}
      end
    end)
  end

  def getDistanceList lines do
    Enum.reduce(lines, %{}, fn line, dists ->
      [city1, city2, dist] = line
      dist = String.to_integer(dist)
      Map.put(dists, {city1, city2}, dist)
      |> Map.put({city2, city1}, dist)
      |> Map.put({nil, city1}, 0)
      |> Map.put({nil, city2}, 0)
    end)
  end

  defp findWantedRoute(cities, fromCity, cmpDistance, currentDistance, distanceList, cmp) do
    Enum.reduce(cities, cmpDistance, fn city, minDist ->
      newCurrentDistance = fn ->
        currentDistance + Map.fetch!(distanceList, {fromCity, city})
      end

      if Enum.count(cities) > 1 do
        findWantedRoute(cities -- [city], city, minDist, newCurrentDistance.(), distanceList, cmp)
      else
        cmp.(newCurrentDistance.(), cmpDistance)
      end
    end)
  end

  def a(d) do
    lines = getInput(d)
    |> parseInput

    cities = getCities(lines)
    distanceList = getDistanceList(lines)

    # Part 1
    IO.puts findWantedRoute(cities, nil, 999999999, 0, distanceList, &min/2)
    # Part 2
    IO.puts findWantedRoute(cities, nil, 0,         0, distanceList, &max/2)
  end

  def b(_d) do
  end
end

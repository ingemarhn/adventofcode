defmodule Aoc2015.Day20 do
  require Integer
  require Math

  defp calcHouses(c, e, p, houses) do
    if c > e do
      houses
    else
      calcHouses(c + p, e, p, Map.update(houses, c, p*10, fn house -> house + p*10 end))
    end
  end

  defp calcHousesb(c, e, p, houses) do
    if c > e do
      houses
    else
      calcHousesb(c + p, e, p, Map.update(houses, c, p*11, fn house -> house + p*11 end))
    end
  end

  def a(_d) do
    minLimit = 29000000
    n = div(minLimit, 2 * 10)
    #houses = Enum.reduce(Stream.take_every(2..n, 2), %{}, fn i, acc -> (calcHouses(i, n, i, acc)) end)
    houses = Enum.reduce(Stream.map(1..div(n, 2), & &1 * 2), %{}, fn i, acc -> (calcHouses(i, n, i, acc)) end)
    #reduce = fn currHouse, lastHouse, houses, fun, me ->
    #  if currHouse > lastHouse do
    #    houses
    #  else
    #    me.(currHouse + 2, lastHouse, fun.(currHouse, houses), fun, me)
    #  end
    #end
    #houses = reduce.(2, n, %{}, (fn i, acc -> (calcHouses(i, n, i, acc)) end), reduce)

    Enum.reduce_while(Stream.take_every(2..n, 2), 0, fn j, _ ->
      if houses[j] > minLimit do
        {:halt, j}
      else
        {:cont, 0}
      end
    end)
    |> IO.inspect
  end

  def b(_d) do
    minLimit = 29000000
    n = div(minLimit, 2 * 11)
    houses = Enum.reduce(Stream.map(1..div(n, 2), & &1 * 2), %{}, fn i, acc -> (calcHousesb(i, min(i * 50, n), i, acc)) end)
    Enum.reduce_while(Stream.take_every(2..n, 2), 0, fn j, _ ->
      if houses[j] > minLimit do
        {:halt, j}
      else
        {:cont, 0}
      end
    end)
    |> IO.inspect
  end

  def part_one(presents) do
    find_house(presents, 10)
  end


  def part_two(presents) do
    find_house(presents, 11, &( div(&1, &2) <= 50 ))
  end


  def find_house(presents, value, filter \\ fn _house, _elf -> true end) do
    Stream.iterate( 1, &(&1 + 1) )
    |> Enum.find( fn house ->
      house
      |> divisors
      |> Enum.filter(&filter.(house, &1))
      |> Enum.sum
      |> Kernel.*(value)
      |> Kernel.>=(presents)
    end)
  end


  def divisors(n) do
    e = n |> :math.sqrt |> trunc

    (1..e)
    |> Enum.flat_map(fn
      x when rem(n, x) != 0 -> []
      x when x != div(n, x) -> [x, div(n, x)]
      x -> [x]
    end)
  end

  def xa(_) do
    DateTime.now!("Etc/UTC")|>IO.puts
    part_one(29000000)
    |> IO.inspect
  end

  def xb(_) do
    DateTime.now!("Etc/UTC")|>IO.puts
    part_two(29000000)
    |> IO.inspect
    DateTime.now!("Etc/UTC")|>IO.puts
  end

end

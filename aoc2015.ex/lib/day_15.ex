defmodule Aoc2015.Day15 do
  def getInput(_d, _t), do: ["Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8", "Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp parseInput(lines) do
    ti = &String.to_integer/1
    Enum.reduce(lines, %{}, fn line, parsed ->
      [ingedient, capacity, durability, flavor, texture, calories] = Regex.run(~r/(\w+):.*?([\d-]+).*?([\d-]+).*?([\d-]+).*?([\d-]+).*?([\d-]+)/, line, capture: :all_but_first)
      Map.put(parsed, ingedient, %{capacity: ti.(capacity), durability: ti.(durability), flavor: ti.(flavor), texture: ti.(texture), calories: ti.(calories)})
    end)
  end

  def listSumEqual 0, _, highest, nums do
    [highest - Enum.sum(nums) | nums]
    |> Enum.reverse
  end
  def listSumEqual count, max, highest, nums do
    for n <- 1..max do
      listSumEqual count-1, highest-n, highest, [n | nums]
    end
    |> Enum.concat
  end
  def getList count, highest do
    listSumEqual(count-1, highest, highest, [])
    |> Enum.chunk_every(count)
  end

  def getScore [], [] do
    []
  end
  def getScore [val1 | tail1], [val2 | tail2] do
    [val1 * val2 | getScore(tail1, tail2)]
  end

  def getAttribValues ingredients do
    Enum.reduce(ingredients, [], fn {_, attributes}, attribValues ->
      values = Map.delete(attributes, :calories)
      |> Map.values
      [values | attribValues]
    end)
    |> Enum.zip
    |> Enum.map(& Tuple.to_list(&1))
  end

  def a(d) do
    ingredients = getInput(d)
    |> parseInput

    attributeValuesList = getAttribValues(ingredients)

    map_size(ingredients)
    |> getList(100)
    |> Enum.reduce(0, fn teaSpoons, highScore ->
      Enum.reduce(attributeValuesList, [], fn attributeValues, scores ->
        score = getScore(teaSpoons, attributeValues)
        |> Enum.sum
        |> max(0)

        [score | scores]
      end)
      |> Enum.reduce(1, &(&1 * &2))
      |> max(highScore)
    end)
    |>IO.inspect
  end

  def b(d) do
    ingredients = getInput(d)
    |> parseInput

    [calories | attributeValuesList] = Enum.reduce(ingredients, [], fn {_, attributes}, attribValues ->
      values = Map.delete(attributes, :calories)
      |> Map.values
      values = [Map.get(attributes, :calories) | values]
      [values | attribValues]
    end)
    |> Enum.zip
    |> Enum.map(& Tuple.to_list(&1))

    map_size(ingredients)
    |> getList(100)
    |> Enum.reduce(0, fn teaSpoons, highScore ->
      Enum.reduce(attributeValuesList, [], fn attributeValues, scores ->
        calScore = getScore(teaSpoons, calories)
        |> Enum.sum
        if calScore == 500 do
          score = getScore(teaSpoons, attributeValues)
          |> Enum.sum
          |> max(0)
          [score | scores]
        else
          [0]
        end
      end)
      |> Enum.reduce(1, &(&1 * &2))
      |> max(highScore)
    end)
    |>IO.inspect
  end
end

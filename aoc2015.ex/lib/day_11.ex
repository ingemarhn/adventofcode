defmodule Aoc2015.Day11 do
  def getInput(_d, _t), do: "abcdefgh"
  def getInput(_d, _t, _tt), do: "ghjaabby" #"ghjaaaaa"
  def getInput(_d), do: "hxbxwxba"

  defp getCharMaps do
    chrs = (?a..?z |> Enum.to_list |> List.to_string |> String.split("", trim: true)) -- ["i", "l", "o"]
    {
      Enum.zip(1..23, chrs) |> Enum.into(%{}),
      Enum.zip(chrs, 1..23) |> Enum.into(%{})
    }
  end

  defp incrPwd pwInts, ind \\ 8 do
    v = pwInts[ind]
    if v < 23 do
      Map.replace!(pwInts, ind, v + 1)
    else
      incrPwd(pwInts, ind - 1)
      |> Map.replace!(ind, 1)
    end
  end

  defp incrNchkPwd pwInts do
    pwInts = incrPwd pwInts

    chkDbl = fn pwdIn ->
      [_, count] = Enum.reduce(Map.values(pwdIn), [0 , 0], fn fig, [cmp, cnt] ->
        if fig == cmp do
          [0, cnt + 1]
        else
          [fig, cnt]
        end
      end)
      count >= 2
    end

    chkConseq = fn pwdIn ->
      %{1 => first} = Map.take(pwdIn, [1])
      [nSeq, _] = Map.drop(pwdIn, [1]) |> Map.values()
      |> Enum.reduce_while([1, %{1 => first}], fn fig, [ind, seq] ->
        if fig == seq[ind] + 1 and fig not in [?i-96, ?l-1-96, ?o-2-96] do
          nInd = ind + 1
          seq = Map.put(seq, nInd, fig)
          op = if nInd == 3, do: :halt, else: :cont
          {op, [nInd, seq]}
        else
          {:cont, [1, %{1 => fig}]}
        end
      end)

      nSeq == 3
    end

    if chkDbl.(pwInts) && chkConseq.(pwInts) do
      pwInts
    else
      incrNchkPwd pwInts
    end
  end

  def a(d) do
    text = getInput(d)
    {numToChar, charToNum} = getCharMaps()
    [_, pwdInts] = String.split(text, "", trim: true)
    |> Enum.reduce([1, %{}], fn chr, [i, m] -> [i + 1, Map.put(m, i, charToNum[chr])] end)

    pwdIntegs = incrNchkPwd(pwdInts)
    newPwd = Map.values(pwdIntegs) |> Enum.map(&(numToChar[&1])) |> Enum.join()
  |> IO.inspect( label: "1st pwd")

    [_, pwdInts] = String.split(newPwd, "", trim: true)
    |> Enum.reduce([1, %{}], fn chr, [i, m] -> [i + 1, Map.put(m, i, charToNum[chr])] end)

    pwdIntegs = incrNchkPwd(pwdInts)
    Map.values(pwdIntegs) |> Enum.map(&(numToChar[&1])) |> Enum.join()
  |> IO.inspect( label: "2nd pwd")

  end

  def b(_d) do
  #  text = getInput(d, 1)
  #  lines = getInput(d, 1)

  #  IO.puts d
  end
end

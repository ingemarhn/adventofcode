defmodule Aoc2015.Day24 do
  defp getInput(_d, "1"), do: ~w(1 2 3 4 5 7 8 9 10 11)
  defp getInput(d, "0"), do: Aoc2015.InputFile.getLines(d)

  defp getQE(list) do
    Enum.reduce(list, 1, &(&1 * &2))
  end

  defp findSomeCombinations(_, [], _, allWeights) do
    allWeights
  end
  defp findSomeCombinations(targetWeight, candidates, currentWeights, allWeights) do
    testWeights = [hd(candidates) | currentWeights]
    weightSum = Enum.sum(testWeights)
    cond do
      weightSum < targetWeight ->
        findAllCombinations(targetWeight, tl(candidates), testWeights, allWeights)
      weightSum == targetWeight ->
        nextWeights = if testWeights in allWeights, do: allWeights, else: [testWeights | allWeights]
        findAllCombinations(targetWeight, tl(candidates), [], nextWeights)# |>IO.inspect(label: "allWeights"))
      true ->
        findSomeCombinations(targetWeight, tl(candidates), currentWeights, allWeights)
    end
  end

  defp findAllCombinations(_, [], _, allWeights) do
    allWeights
  end
  defp findAllCombinations(targetWeight, candidates, currentWeights, allWeights) do
    allWeights = findSomeCombinations(targetWeight, candidates, currentWeights, allWeights)
    findAllCombinations(targetWeight, tl(candidates), currentWeights, allWeights)
  end

  defp sortWeightCombinations(weightCombinations) do
length(weightCombinations) |> IO.inspect(label: "length")
    Enum.map(weightCombinations, fn oneCombination -> {oneCombination, length(oneCombination), getQE(oneCombination)} end)
    |> Enum.sort(fn {_, lengthA, qe_A}, {_, lengthB, qe_B} ->
      cond do
        lengthA < lengthB -> true
        lengthA > lengthB -> false
        true -> qe_A < qe_B
      end
    end)
  end

  defp testShortestCombinationValidity?([], _, nPartsLeft) do
    (nPartsLeft == 0)
  end
  defp testShortestCombinationValidity?(_, _, 0) do
    true
  end
  defp testShortestCombinationValidity?(combinations, weights, nPartsLeft) do
    {firstCombination, _, _} = hd(combinations)
    weightsN = weights -- firstCombination
    if length(weightsN) == length(weights) - length(firstCombination) do
      testShortestCombinationValidity?(tl(combinations), weightsN, nPartsLeft - 1)
    else
      testShortestCombinationValidity?(tl(combinations), weights, nPartsLeft)
    end
  end

  defp findShortestBalancedCombination(combinations, weights, nParts) do
    Enum.reduce_while(combinations, tl(combinations), fn {combination, _, _}, restCombinations ->
      if testShortestCombinationValidity?(restCombinations, weights -- combination, nParts - 1) do
        {:halt, combination}
      else
        {:cont, tl(restCombinations)}
      end
    end)
  end

  defp findCombination(d, t, nParts) do
    lines = getInput(d, t)
    weights = Enum.map(lines, &(String.to_integer(&1)))

    weightPart = round(Enum.sum(weights) / nParts)
    findAllCombinations(weightPart, weights, [], [])
    |> sortWeightCombinations
    |> findShortestBalancedCombination(weights, nParts)
    |> getQE
    |> IO.inspect
  end

  def findCombination1(d, t, nParts) do
    lines = getInput(d, t)
    weights = Enum.map(lines, &(String.to_integer(&1)))
    weightPart = round(Enum.sum(weights) / nParts)
    {minCombLen, _} =
      Enum.reverse(weights)
      |> Enum.reduce_while({0, 0}, fn wgt, {count, sum} ->
        sumN = sum + wgt
        ret = {count + 1, sumN}
        if sumN < weightPart do
          {:cont, ret}
        else
          {:halt, ret}
        end
      end)

    maxCombLen = length(weights) - (nParts - 1) * minCombLen

    for n <- minCombLen..maxCombLen do
      nCombinations = Comb.combinations(weights, n)
      #|>IO.inspect(label: "combinations, #{n}")
      for c <- nCombinations, Enum.sum(c) == weightPart, do: c
      #|>IO.inspect(label: "combinations, #{n}'")
    end
    |> Enum.concat
    |> sortWeightCombinations
    |> findShortestBalancedCombination(weights, 3)
    |> getQE
    |> IO.inspect
  end

  def a(d, t) do
    findCombination(d, t, 3)
  end

  def b(d, t) do
    findCombination(d, t, 4)
  end
end

defmodule Aoc2015.InputFile do
  @spec getText(String.t) :: String.t
  def getText(fileNum) do
    String.trim(File.read!(__DIR__ <> "/../input/day-#{fileNum}.txt"))
  end

  @spec getLines(String.t) :: [String.t]
  def getLines(fileNum) do
    String.split(getText(fileNum), "\n")
  end
end

defmodule Aoc2015.Day7 do
  import Bitwise

  def getInput(_d, _t), do: ["123 -> x", "456 -> y", "x AND y -> d", "x OR y -> e", "x LSHIFT 2 -> f", "y RSHIFT 2 -> g", "NOT x -> h", "NOT y -> i", "x -> a"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  defp newWires wiresMap_, wire_ do
    if !is_integer(wire_) && wire_ != "" do
      wiresMap_ = modWireValues(wiresMap_, wire_)
      %{value: value} = Map.get(wiresMap_, wire_)

      {wiresMap_, value}
    else
      {wiresMap_, wire_}
    end
  end

  defp modWireValues(wiresMap, wire) do
    wireMap = Map.get(wiresMap, wire)
    %{left: left, op: oper, right: right, value: value} = wireMap
    if value == nil do
      {wiresMap, left } = newWires(wiresMap, left)
      {wiresMap, right} = newWires(wiresMap, right)

      val = case oper do
        "" -> right
        "NOT" -> ~~~right
        "AND" -> left &&& right
        "OR" -> left ||| right
        "LSHIFT" -> left <<< right
        "RSHIFT" -> left >>> right
      end

      wireMap = Map.replace!(wireMap, :value, val)
      Map.replace!(wiresMap, wire, wireMap)
    else
      wiresMap
    end
  end

  defp getWires(lines) do
    Enum.reduce(lines, %{}, fn line, wres ->
      [left, op, right, wire] = Regex.run(~r/(?:([a-z]+|\d+) )?(NOT|AND|LSHIFT|OR|RSHIFT)?(?: ?([a-z]+|\d+))? -> ([a-z]+)/, line, capture: :all_but_first)
      checkForInt = &(if (v = Integer.parse(&1)) != :error, do: ({vv,_} = v; vv), else: &1)
      Map.put(wres, wire, %{op: op, left: checkForInt.(left), right: checkForInt.(right), value: nil})
    end)
  end

  def a(d) do
    IO.puts getInput(d)
    |> getWires()
    |> modWireValues("a")
    |> Map.get("a")
    |> Map.get(:value)
  end

  def b(d) do
    wires = getInput(d)
    |> getWires()
    wireMap = Map.get(wires, "b")
    |> Map.replace!(:right, 3176)

    IO.puts Map.replace!(wires, "b", wireMap)
    |> modWireValues("a")
    |> Map.get("a")
    |> Map.get(:value)
  end
end

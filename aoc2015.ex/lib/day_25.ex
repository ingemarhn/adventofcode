defmodule Aoc2015.Day25 do
  defp getInput(_d, "1"), do: ~w(6 2)
  defp getInput(_d, "2"), do: ~w(6 5)
  defp getInput(_d, "3"), do: ~w(6 6)
  defp getInput(_d, "4"), do: ~w(100 1000)
  defp getInput(d, "0") do
    ln = hd Aoc2015.InputFile.getLines(d)
    Regex.run(~r/row (\d+), column (\d+)/, ln)
    |> Enum.drop(1)
  end

  defp getCodeAt(targetRow, targetCol, currentRow, currentCol, currentCode) when targetRow == currentRow and targetCol == currentCol do
    currentCode
  end
  defp getCodeAt(targetRow, targetCol, 0, currentCol, currentCode) do
    getCodeAt(targetRow, targetCol, currentCol, 1, currentCode)
  end
  defp getCodeAt(targetRow, targetCol, currentRow, currentCol, currentCode) do
    nextCode = rem(currentCode * 252533, 33554393)
    getCodeAt(targetRow, targetCol, currentRow - 1, currentCol + 1, nextCode)
  end

  def a(d, t) do
    [row, col] = getInput(d, t)
    |> Enum.map(& String.to_integer(&1))

    getCodeAt(row, col, 1, 1, 20151125)
    |> IO.inspect
  end

  def b(_d, _t) do
  end
end

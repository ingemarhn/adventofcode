defmodule Aoc2015.Day4 do
  def getInput(_d, _t), do: "abcdef"
  def getInput(d), do: Aoc2015.InputFile.getText(d)

  defp findMD5zero5(v, key, ub, cmp) do
    md5 = :crypto.hash(:md5 , key <> Integer.to_string(v)) |> Base.encode16()
    if String.slice(md5, 0..ub) == cmp do
      v
    else
      findMD5zero5 v + 1, key, ub, cmp
    end
  end

  def a(d) do
    text = getInput(d)
    low = findMD5zero5 0, text, 4, "00000"

    IO.puts low
  end

  def b(d) do
    text = getInput(d)
    low = findMD5zero5 0, text, 5, "000000"

    IO.puts low
  end
end

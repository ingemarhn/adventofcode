# Length x Width x Height
# a:
#   Get surface sizes: a1=l*w, a2=h*w, a3=h*l
#   Pick smallest size as slack
#   Area == 2*a1 + 2*a2 + 2*a3 + small
#   Sum up all areas
# b:
#   Get smallest perimeter
#   Get volume
#   Add perimeter and volume

defmodule Aoc2015.Day2 do
  def getInput(_d, test) when test, do: ["2x3x4", "1x1x10"]
  def getInput(d), do: Aoc2015.InputFile.getLines(d)

  def a(d) do
    presents = getInput(d)
    area = Enum.reduce(presents, 0, fn(present, totArea) ->
      dimensions = String.split(present, "x", trim: true)
      [l, w, h] = Enum.map(dimensions, fn d -> String.to_integer(d) end)
      a1 = l * w
      a2 = h * w
      a3 = h * l
      smallest = min(a1, min(a2, a3))
      totArea + 2 * a1 + 2 * a2 + 2 * a3 + smallest
    end)

    IO.puts area
  end

  def b(d) do
    presents = getInput(d)

    length = Enum.reduce(presents, 0, fn(present, totLength) ->
      dimensions = String.split(present, "x", trim: true)
      |> Enum.map(fn d -> String.to_integer(d) end)
      |> Enum.sort()
      [l1, l2] = Enum.slice(dimensions , 0, 2)
      [l, w, h] = dimensions
      totLength + 2 * (l1 + l2) + l * w * h
    end)

    IO.puts length
  end
end

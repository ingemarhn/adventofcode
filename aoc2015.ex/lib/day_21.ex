defmodule Aoc2015.Day21 do
  def getInput(_d), do: {8, 2}

  defp getShopItems do
    %{
      weapons: [ {4, 8}, {5, 10}, {6, 25}, {7, 40}, {8, 74} ],
      armor: [ {1, 13}, {2, 31}, {3, 53}, {4, 75}, {5, 102} ],
      ringsDamage: [ {1, 25}, {2, 50}, {3, 100} ],
      ringsArmor: [ {1, 20}, {2, 40}, {3, 80} ]
    }
  end

  defp combineTwoRings(items1, ring1, ring2) do
    for {i1s, i1c} <- items1, {i2s, i2c} <- ring1, {i3s, i3c} <- ring2 do {i1s+i2s+i3s, i1c+i2c+i3c} end
  end

  defp combineOneRingDbl(items1, ring) do
    for {i1s, i1c} <- items1, {i2s, i2c} <- ring, {i3s, i3c} <- ring -- [{i2s, i2c}] do {i1s+i2s+i3s, i1c+i2c+i3c} end
  end

  defp combine(items1, items2) do
    for {i1s, i1c} <- items1, {i2s, i2c} <- items2 do {i1s+i2s, i1c+i2c} end
  end

  defp getMinCost(battleItems, myMinStrength, minCost) do
    Enum.reduce(battleItems, minCost, fn {strength, cost}, currMinCost ->
      if strength >= myMinStrength && cost < currMinCost, do: cost, else: currMinCost
    end)
  end

  defp getMaxCost(battleItems, myMaxStrength, maxCost) do
    Enum.reduce(battleItems, maxCost, fn {strength, cost}, currMaxCost ->
      if strength <= myMaxStrength && cost > currMaxCost, do: cost, else: currMaxCost
    end)
  end

  def a(d) do
    {bossDam, bossArm} = getInput(d)
    bossStrength = bossDam + bossArm
    myMinStrength = bossStrength + 1
    %{weapons: weapons, armor: armor, ringsDamage: ringsDamage, ringsArmor: ringsArmor} = getShopItems()
    weaponsArmor = combine(weapons, armor)
    weaponsRingD1 = combine(weapons, ringsDamage)
    weaponsRingA1 = combine(weapons, ringsArmor)
    weaponsArmorRingD1 = combine(weaponsArmor, ringsDamage)
    weaponsArmorRingA1 = combine(weaponsArmor, ringsArmor)
    weaponsRingDA = combineTwoRings(weapons, ringsDamage, ringsArmor)
    weaponsRingDD = combineOneRingDbl(weapons, ringsDamage)
    weaponsRingAA = combineOneRingDbl(weapons, ringsArmor)
    weaponsArmorRingDA = combineTwoRings(weaponsArmor, ringsDamage, ringsArmor)
    weaponsArmorRingDD = combineOneRingDbl(weaponsArmor, ringsDamage)
    weaponsArmorRingAA = combineOneRingDbl(weaponsArmor, ringsArmor)
    minCostW = getMinCost(weapons, myMinStrength, 9999)
    minCostWA = getMinCost(weaponsArmor, myMinStrength, minCostW)
    minCostWrD1 = getMinCost(weaponsRingD1, myMinStrength, minCostWA)
    minCostWrA1 = getMinCost(weaponsRingA1, myMinStrength, minCostWrD1)
    minCostWArD1 = getMinCost(weaponsArmorRingD1, myMinStrength, minCostWrA1)
    minCostWArA1 = getMinCost(weaponsArmorRingA1, myMinStrength, minCostWArD1)
    minCostWrDA = getMinCost(weaponsRingDA, myMinStrength, minCostWArA1)
    minCostWrDD = getMinCost(weaponsRingDD, myMinStrength, minCostWrDA)
    minCostWrAA = getMinCost(weaponsRingAA, myMinStrength, minCostWrDD)
    minCostWArDA = getMinCost(weaponsArmorRingDA, myMinStrength, minCostWrAA)
    minCostWArDD = getMinCost(weaponsArmorRingDD, myMinStrength, minCostWArDA)
    _minCostWArAA = getMinCost(weaponsArmorRingAA, myMinStrength, minCostWArDD)
    |> IO.inspect
    myMaxStrength = bossStrength
    maxCostW = getMaxCost(weapons, myMaxStrength, 0)
    maxCostWA = getMaxCost(weaponsArmor, myMaxStrength, maxCostW)
    maxCostWrD1 = getMaxCost(weaponsRingD1, myMaxStrength, maxCostWA)
    maxCostWrA1 = getMaxCost(weaponsRingA1, myMaxStrength, maxCostWrD1)
    maxCostWArD1 = getMaxCost(weaponsArmorRingD1, myMaxStrength, maxCostWrA1)
    maxCostWArA1 = getMaxCost(weaponsArmorRingA1, myMaxStrength, maxCostWArD1)
    maxCostWrDA = getMaxCost(weaponsRingDA, myMaxStrength, maxCostWArA1)
    maxCostWrDD = getMaxCost(weaponsRingDD, myMaxStrength, maxCostWrDA)
    maxCostWrAA = getMaxCost(weaponsRingAA, myMaxStrength, maxCostWrDD)
    maxCostWArDA = getMaxCost(weaponsArmorRingDA, myMaxStrength, maxCostWrAA)
    maxCostWArDD = getMaxCost(weaponsArmorRingDD, myMaxStrength, maxCostWArDA)
    _maxCostWArAA = getMaxCost(weaponsArmorRingAA, myMaxStrength, maxCostWArDD)
    |> IO.inspect
  end

  def b(_d) do
  end
end
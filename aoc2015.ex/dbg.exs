defmodule D do
  def b(m, l) do
    :int.break(m, l)
  end
end

mod = Aoc2015.Day16
File.cd('_build/dev/lib/aoc2015/ebin')
IO.puts "-> Working directory: " <> File.cwd!()
:debugger.start()
:int.ni(mod)

D.b(mod, 61)
mod.b(16)

# Starta en agent som håller modulnamnet. Då kan man skapa generella funktioner som jobbar mot en viss modul.


pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: &[String]) {
    let masses = parse_input(lines);
    let mass_sum =
        masses
        .iter()
        .fold(0, |sum, m| sum + (m / 3) - 2);

    println!("Fuel requirements: {}", mass_sum);
}

fn task_b(lines: &[String]) {
    let masses = parse_input(lines);
    let mass_sum =
        masses
        .iter()
        .fold(0, |sum, m| {
            let mut s = sum;
            let mut f = (m / 3) - 2;
            while f != 0 {
                s += f;
                f = (f / 3).saturating_sub(2);
            }

            s
        });

    println!("Fuel requirements: {}", mass_sum);
}

fn parse_input(lines: &[String]) -> Vec<u32> {
    lines
        .iter()
        .map(|line|
            line
            .parse()
            .unwrap()
        )
        .collect::<Vec<_>>()
}

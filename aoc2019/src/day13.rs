use std::collections::HashMap;
use std::convert::TryFrom;
use num_enum::TryFromPrimitive;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ProgValue = isize;
type Program = HashMap<usize, ProgValue>;
type Position = (isize, isize);

/*
    0 is an empty tile. No game object appears in this tile.
    1 is a wall tile. Walls are indestructible barriers.
    2 is a block tile. Blocks can be broken by the ball.
    3 is a horizontal paddle tile. The paddle is indestructible.
    4 is a ball tile. The ball moves diagonally and bounces off objects.
*/
#[repr(u8)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, TryFromPrimitive)]
enum Tile {
    Empty,
    Wall,
    Block,
    Paddle,
    Ball,
}

impl Tile {
    fn to_enum(val: u8) -> Self {
        Self::try_from(val).unwrap()
    }
}

#[derive(Debug)]
struct Arcade {
    tiles: HashMap<Position, Tile>,
    ball_x: isize,
    paddle_x: isize,
    display: Vec<Vec<Tile>>,
    is_init: bool,
}

impl Arcade {
    fn new() -> Self {
        Self {
            tiles: HashMap::new(),
            ball_x: 0,
            paddle_x: 0,
            display: Vec::new(),
            is_init: false,
        }
    }

    fn insert(&mut self, x: isize, y: isize, tile: Tile) {
        self.tiles.insert((x, y), tile);
        if self.is_init {
            self.display[y as usize][x as usize] = tile;
        }
        match tile {
            Tile::Ball   => self.ball_x = x,
            Tile::Paddle => self.paddle_x = x,
            _ => {}
        }
    }

    fn get_joystick_pos(&self) -> isize {
        match self.ball_x - self.paddle_x {
            ..= -1 => -1,
            0     =>  0,
            1 ..  =>  1,
        }
    }

    fn var_init(&mut self) {
        let (max_x, max_y) = self
            .tiles
            .iter()
            .fold((0,0), |(mx, my), (&(x, y), _)| {
                (mx.max(x), my.max(y))
            });

        self.display = vec![vec![Tile::Empty; max_x as usize + 1]; max_y as usize + 1];

        for (&(x, y), tile) in self.tiles.iter() {
            self.display[y as usize][x as usize] = *tile;
        }

        self.is_init = true;
    }

    #[allow(dead_code)]
    fn print(&self) {
        for r in self.display.iter() {
            for t in r.iter() {
                let c = match *t {
                    Tile::Empty  => ' ',
                    Tile::Ball   => '\u{25CF}',
                    Tile::Wall   => '\u{2588}',
                    Tile::Paddle => '\u{25AC}',
                    Tile::Block  => '\u{25A2}',
                };

                print!("{}", c);
            }
            println!();
        }
    }
}

fn run_program(program: &Program, run_to_exit: bool, instr_ptr: &mut usize, relative_base: &mut ProgValue, arcade: &Arcade) -> (bool, ProgValue, Program) {
    let mut program = program.clone();
    let mut prog_val = ProgValue::MIN;

    let get_value = |instr_ptr: &mut usize, program: &Program, mode, relative_base: &mut ProgValue| {
        *instr_ptr += 1;
        let value_ptr =
            match mode {
                0 => program[instr_ptr] as usize,
                1 => *instr_ptr,
                2 => (program.get(instr_ptr).unwrap() + *relative_base) as usize,
                m => panic!("Illegal mode: {m}"),
        };

        match program.get(&value_ptr) {
            Some(value) => *value,
            None => 0,
        }
    };

    let get_three_params = |instr_ptr: &mut usize, program: &Program, elems: (ProgValue, ProgValue, ProgValue, ProgValue), relative_base: &mut ProgValue| {
        (
            get_value(instr_ptr, program, elems.1, relative_base),
            get_value(instr_ptr, program, elems.2, relative_base),
            {
                let mut third = get_value(instr_ptr, program, 1, relative_base);
                if elems.3 == 2 {
                    third += *relative_base;
                }
                third as usize
            },
        )
    };

    let mut did_exit = true;
    while program[instr_ptr] != 99 {
        let elems = match program.get(instr_ptr).unwrap() {
            op @ 1 ..= 98 => {
                (*op, 0, 0, 0)
            },
            param => {
                let oper = *param % 100;
                let rest = *param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;
                let mode3 = if *param < 10000 { 1 } else { rest / 10 };

                (oper, mode1, mode2, mode3)
            }
        };

        let mut mod_prog_val = None;
        match elems.0 {
            1 => {  // Add
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                mod_prog_val = Some((reg, v1 + v2));
            },
            2 => {  // Multiply
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                mod_prog_val = Some((reg, v1 * v2));
            },
            3 => {  // Get input
                let mut reg = get_value(instr_ptr, &program, 1, relative_base) as usize;
                if elems.1 == 2 {
                    reg += *relative_base as usize;
                }
                mod_prog_val = Some((reg, arcade.get_joystick_pos()));
            },
            4 => {  // Output
                prog_val = get_value(instr_ptr, &program, elems.1, relative_base);
                if !run_to_exit {
                    did_exit = false;
                    *instr_ptr += 1;
                    break;
                }
            },
            op @ (5 | 6) => {   // Jump if true | false
                let v1 = get_value(instr_ptr, &program, elems.1, relative_base);
                let v2 = get_value(instr_ptr, &program, elems.2, relative_base);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    *instr_ptr = v2 as usize;
                    continue;
                }
            },
            op @ (7 | 8) => {   // If is less than | equal
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) { 1 } else { 0 };
                mod_prog_val = Some((reg, v));
            },
            9 => {
                *relative_base += get_value(instr_ptr, &program, elems.1, relative_base);
            },

            u => panic!("Unsupported oper: {}", u)
        }

        if let Some((reg, v)) = mod_prog_val {
            program.entry(reg).and_modify(|e| *e = v).or_insert(v);
        }
        *instr_ptr += 1;
    }

    (did_exit, prog_val, program)
}

fn get_arcade(program_start: &Program) -> Arcade {
    let mut program = program_start.clone();
    let mut instr_ptr = 0;
    let mut arcade: Arcade = Arcade::new();
    let mut relative_base = 0;

    loop {
        let (_, x, mod_program) = run_program(&program, false, &mut instr_ptr, &mut relative_base, &arcade);
        let (_, y, mod_program) = run_program(&mod_program, false, &mut instr_ptr, &mut relative_base, &arcade);
        let (prog_exit, tile, mod_program) = run_program(&mod_program, false, &mut instr_ptr, &mut relative_base, &arcade);

        if prog_exit {
            break;
        }
        arcade.insert(x, y, Tile::to_enum(tile as u8));

        program = mod_program;
    }

    arcade
}

fn play_arcade_game(program_start: &Program) -> isize {
    let mut program = program_start.clone();
    let mut instr_ptr = 0;
    let mut relative_base = 0;
    let mut points = 0;

    let mut arcade = get_arcade(&program);
    program.insert(0, 2);
    arcade.var_init();

    loop {
        let (_, x, mod_program) = run_program(&program, false, &mut instr_ptr, &mut relative_base, &arcade);
        let (_, y, mod_program) = run_program(&mod_program, false, &mut instr_ptr, &mut relative_base, &arcade);
        let (prog_exit, tile, mod_program) = run_program(&mod_program, false, &mut instr_ptr, &mut relative_base, &arcade);

        if prog_exit {
            break;
        }

        if x == -1 && y == 0 {
            points = tile;
        } else {
            arcade.insert(x, y, Tile::to_enum(tile as u8));
        }

        program = mod_program;
    }

    points
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let arcade = get_arcade(&program);

    println!("# of block tiles = {}", arcade.tiles.iter().filter(|t| *t.1 == Tile::Block ).count());
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let score = play_arcade_game(&program);

    println!("Score: {}", score);
}

fn parse_input(lines: &[String]) -> Program {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect()
}

#[cfg(test)]
mod tests19_13 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;

    #[test]
    fn tests19_13_t0() {}
}


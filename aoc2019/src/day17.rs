pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Line = Vec<char>;
type Image = Vec<Line>;
type ProgValue = isize;
type Program = Vec<ProgValue>;
type Position = (usize, usize);

fn set_prog_val(program: &mut Program, index: usize, value: ProgValue) {
    if program.len() <= index {
        // Lengthen the vector with at least 10 %
        let length = (program.len() / 10).max(index - (program.len() - 1));
        program.extend_from_slice(&vec![0; length]);
    }

    program[index] = value;
}

fn run_program<F>( program: &Program, get_input: &mut F, run_to_exit: bool, instr_ptr: &mut usize, relative_base: &mut ProgValue, ) -> (bool, ProgValue, Program)
where
    F: FnMut() -> ProgValue,
{
    let mut program = program.clone();
    let mut prog_val = ProgValue::MIN;

    let get_value = |instr_ptr: &mut usize, program: &Program, mode, relative_base: isize| {
        *instr_ptr += 1;
        let value_ptr = match mode {
            0 => program[*instr_ptr] as usize,
            1 => *instr_ptr,
            2 => (program[*instr_ptr] + relative_base) as usize,
            m => panic!("Illegal mode: {m}"),
        };
        if program.len() > value_ptr {
            program[value_ptr]
        } else {
            0
        }
    };

    let get_register = |instr_ptr: &mut usize, program: &Program, mode, relative_base| {
        let mut reg = get_value(instr_ptr, program, 1, relative_base);
        if mode == 2 {
            reg += relative_base;
        }
        reg as usize
    };

    let get_three_params = |instr_ptr: &mut usize, program: &Program, elems: &(ProgValue, ProgValue, ProgValue, ProgValue), relative_base| {
        (
            get_value(instr_ptr, program, elems.1, relative_base),
            get_value(instr_ptr, program, elems.2, relative_base),
            get_register(instr_ptr, program, elems.3, relative_base),
        )
    };


    /*
        Program[843] should increment with 1 but doesn't
        Must be that the multiplication at [838] fails, probably based on a previous failure
     */

    let mut did_exit = true;
    while program[*instr_ptr] != 99 {

        let elems = match program.get(*instr_ptr).unwrap() {
            op @ 1..=98 => (*op, 0, 0, 0),
            param => {
                let oper = *param % 100;
                let rest = *param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;
                let mode3 = rest / 10;

                (oper, mode1, mode2, mode3)
            }
        };

        match elems.0 {
            1 => {
                // Add
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, &elems, *relative_base);
                set_prog_val(&mut program, reg, v1 + v2);
            }
            2 => {
                // Multiply
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, &elems, *relative_base);
                set_prog_val(&mut program, reg, v1 * v2);
            }
            3 => {
                // Get input
                let reg = get_register(instr_ptr, &program, elems.1, *relative_base);
                set_prog_val(&mut program, reg, get_input());
            }
            4 => {
                // Output
                prog_val = get_value(instr_ptr, &program, elems.1, *relative_base);
                if !run_to_exit {
                    did_exit = false;
                    *instr_ptr += 1;
                    break;
                }
            }
            op @ (5 | 6) => {
                // Jump if true | false
                let v1 = get_value(instr_ptr, &program, elems.1, *relative_base);
                let v2 = get_value(instr_ptr, &program, elems.2, *relative_base);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    *instr_ptr = v2 as usize - 1; // instr_ptr will be incremented after the match statement
                }
            }
            op @ (7 | 8) => {
                // If is less than | equal
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, &elems, *relative_base);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) {
                    1
                } else {
                    0
                };
                set_prog_val(&mut program, reg, v);
            }
            9 => {
                *relative_base += get_value(instr_ptr, &program, elems.1, *relative_base);
            }

            u => panic!("Unsupported oper: {}", u),
        }

        *instr_ptr += 1;
    }

    (did_exit, prog_val, program)
}

fn get_image(program_start: &Program) -> Image {
    let mut program = program_start.clone();
    let mut instr_ptr = 0;
    let mut relative_base = 0;

    let mut image = Vec::new();
    let mut line = Line::new();

    let mut get_input = || 0; // Dummy for now
    loop {
        let (did_exit, pixel_out, program_out) = run_program(&program, &mut get_input, false, &mut instr_ptr, &mut relative_base);
        if did_exit {
            break;
        }
        let pixel = pixel_out as u8 as char;
        if !['\n', '#', '.', '^', 'v', '<', '>'].contains(&pixel) {
            panic!("Bad program return: {}", pixel)
        }
        program = program_out;

        if pixel != '\n' {
            line.push(pixel);
        } else {
            if !line.is_empty() {
                image.push(line);
            }
            line = Line::new();
        }
    }

    image
}

fn get_instersections(image: &[Line]) -> Vec<Position> {
    let mut intersections = Vec::new();

    for l in 1 .. image.len() - 1 {
        for c in 1 .. image[l].len() - 1 {
            if image[l][c] == '#' {
                let scaffold_count = [(l-1, c), (l, c+1), (l+1, c), (l, c-1)].iter().fold(0, |sum, (lp, cp)|
                    if image[*lp][*cp] == '#' {
                        sum + 1
                    } else {
                        sum
                    }
                );
                if scaffold_count == 4 {
                    intersections.push((l, c));
                }
            }
        }
    }

    intersections
}

fn get_sum(intersections: &[Position]) -> usize {
    intersections.iter().fold(0, |sum, (l, c)| {
        sum + *l * *c
    })
}

fn get_pixel(image: &[Line], l: isize, c: isize) -> char {
    if l < 0 || c < 0 || image.len() as isize <= l || image[l as usize].len() as isize <= c {
        return 0u8 as char;
    }

    image[l as usize][c as usize]
}

fn get_path(image: &[Line]) -> String {
    let mut robot_pos = (0, 0);
    for (ln, l) in image.iter().enumerate() {
        if let Some(pos) = l.iter().position(|c| ['^', 'v', '<', '>'].contains(c)) {
            robot_pos = (ln, pos);
            break;
        }
    }
    let mut robot_dir = match image[robot_pos.0][robot_pos.1] {
        '^' => 'U',
        '>' => 'R',
        'v' => 'D',
        '<' => 'L',
        _ => panic!()
    };

    let mut robot_pos = (robot_pos.0 as isize, robot_pos.1 as isize);
    let (l, c) = (robot_pos.0, robot_pos.1);
    let (scaffold_pos, mut _goto_dir) =
        [(l-1, c, 'U'), (l, c+1, 'R'), (l+1, c, 'D'), (l, c-1, 'L')]
        .iter()
        .fold(((0, 0), ' '), |pos, (lt, ct, d)| {
            if get_pixel(image, *lt, *ct) == '#' {
                ((*lt, *ct), *d)
            } else {
                pos
            }
        });

    let turn_left = match robot_dir {
        'U' => scaffold_pos.1 < robot_pos.1,
        'R' => scaffold_pos.0 < robot_pos.0,
        'D' => scaffold_pos.1 > robot_pos.1,
        _   => scaffold_pos.0 > robot_pos.0,
    };
    let mut turn_dir = if turn_left { 'L' } else { 'R' };

    let mut path = String::new();
    loop {
        path.push(turn_dir);
        path.push(',');
        robot_dir = match robot_dir {
            'U' => if turn_dir == 'L' { 'L' } else { 'R' },
            'R' => if turn_dir == 'L' { 'U' } else { 'D' },
            'D' => if turn_dir == 'L' { 'R' } else { 'L' },
            _   => if turn_dir == 'L' { 'D' } else { 'U' },
        };

        // Positions for forward, left and right moves
        let (nl_f, nc_f, nl_l, nc_l, nl_r, nc_r) = match robot_dir {
            'U' => (-1,  0,  0, -1,  0,  1),
            'R' => ( 0,  1, -1,  0,  1,  0),
            'D' => ( 1,  0,  0,  1,  0, -1),
            _   => ( 0, -1,  1,  0, -1,  0),
        };
        // How many steps in this direction?
        let mut mv_count = 0;
        while get_pixel(image, robot_pos.0 + nl_f, robot_pos.1 + nc_f) == '#' {
            mv_count += 1;
            robot_pos = (robot_pos.0 + nl_f, robot_pos.1 + nc_f);
        }
        path.push_str(format!("{},", mv_count).as_str());

        // Turn left, right or is this the end?
        if get_pixel(image, robot_pos.0 + nl_l, robot_pos.1 + nc_l) == '#' {
            turn_dir = 'L';
        } else if get_pixel(image, robot_pos.0 + nl_r, robot_pos.1 + nc_r) == '#' {
            turn_dir = 'R';
        } else {
            break
        }
    }
    path.pop();

    path
}

fn get_dust(program_start: &Program, movements: &str) -> isize {
    let mut program = program_start.clone();
    program[0] = 2;
    let mut instr_ptr = 0;
    let mut relative_base = 0;

    let mut input_iter = movements.chars();
    let mut get_input = || {
        input_iter.next().unwrap() as isize
    };

    let mut dust = 0;
    loop {
        let (did_exit, dust_part, program_out) = run_program(&program, &mut get_input, false, &mut instr_ptr, &mut relative_base);
        if did_exit {
            break;
        }
        program = program_out;
        dust = dust_part;
    }

    dust
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let image = get_image(&program);
    let intersections = get_instersections(&image);
    let sum = get_sum(&intersections);

    println!("Sum of the alignment parameters: {}", sum);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let image = get_image(&program);
    let path = get_path(&image);
    common::dbg(format!("path = {}", path), 1);
    // Jag orkar inte skriva en generisk rutin för att generera 'main routine' och 'movement functions'
    // let movements = get_movements(&path);
    let movements = "A,B,B,C,C,A,B,B,C,A\nR,4,R,12,R,10,L,12\nL,12,R,4,R,12\nL,12,L,8,R,10\nn\n";
    let dust = get_dust(&program, movements);

    println!("Collected dust: {}", dust);
}

fn parse_input(lines: &[String]) -> Program {
    use std::collections::HashMap;

    let prog: HashMap<usize, ProgValue> =
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect();

    let mut program = vec![0; prog.len()];
    for e in prog.iter() {
        set_prog_val(&mut program, *e.0, *e.1);
    }

    program
}
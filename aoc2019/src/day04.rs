pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Figures = [u8; 6];
type Range = [Figures; 2];

fn is_valid(pwd: &Figures, strict: bool) -> bool {
    let mut prev = 0;
    let mut is_incr = true;
    let mut eq_found = false;
    let mut eq_count = 1;

    fn check_count(eq_count: &mut i32, eq_found: &mut bool, strict: bool) {
        if *eq_count == 2 && strict || *eq_count >= 2 && !strict {
            *eq_found = true;
        }
    }

    for d in pwd.iter() {
        if *d < prev {
            is_incr = false;
            break;
        }
        if *d == prev {
            eq_count += 1;
        } else {
            check_count(&mut eq_count, &mut eq_found, strict);
            eq_count = 1;
        }

        prev = *d;
    }
    check_count(&mut eq_count, &mut eq_found, strict);

    is_incr && eq_found
}

fn next_pwd(pwd: &mut Figures, strict: bool) {
    let mut prev = 0;
    for i in 0 .. 6 {
        if prev > pwd[i] {
            for e in pwd.iter_mut().skip(i) {
                *e = prev;
            }

            pwd[5] -= 1;
            break;
        }

        prev = pwd[i];
    }

    if pwd[5] < 9 {
        pwd[5] += 1;
    } else {
        let mut i = 4;
        while pwd[i] == 9 {
            i -= 1;
        }
        let v = pwd[i] + 1;
        for e in pwd.iter_mut().skip(i) {
            *e = v;
        }
    }

    while !is_valid(pwd, strict) {
        next_pwd(pwd, strict);
    }
}

fn calc_n_pwds(range: Range, strict: bool) -> u32 {
    let mut current_pwd = range[0];
    next_pwd(&mut current_pwd, strict);
    let mut highest_pwd = range[1];
    let mut prev = 0;
    for i in 0 .. 6 {
        if prev > highest_pwd[i] {
            highest_pwd[i - 1] -= 1;
            for e in highest_pwd.iter_mut().skip(i) {
                *e = 9;
            }

            break;
        }

        prev = highest_pwd[i];
    }

    let mut n_pwds = 1;
    while current_pwd < highest_pwd {
        next_pwd(&mut current_pwd, strict);
        n_pwds += 1;
    }

    n_pwds
}

fn task_a(lines: &[String]) {
    let range = parse_input(lines);
    let n_pwds = calc_n_pwds(range, false);

    println!("Number of passwords = {}", n_pwds);
}

fn task_b(lines: &[String]) {
    let range = parse_input(lines);
    let n_pwds = calc_n_pwds(range, true);

    println!("Number of passwords = {}", n_pwds);
}

fn parse_input(lines: &[String]) -> Range {
    lines[0]
        .split('-')
        .map(|n|
            n
            .chars()
            .map(|d| d.to_digit(10).unwrap() as u8)
            .collect::<Vec<_>>()
            .try_into()
            .unwrap()
        )
        .collect::<Vec<_>>()
        .try_into()
        .unwrap()
}

#[cfg(test)]
mod tests19_04 {
    use super::*;

    #[test]
    fn tests19_04_t0() {
        let r = parse_input(&[String::from("111111-122345")]);
        assert!(is_valid(&r[0], false));
        assert!(is_valid(&r[1], false));
        let r = parse_input(&[String::from("223450-123589")]);
        assert!(!is_valid(&r[0], false));
        assert!(!is_valid(&r[1], false));
    }

    #[test]
    fn tests19_04_t1() {
        let mut r = parse_input(&[String::from("111111-122345")]);
        next_pwd(&mut r[0], false);
        assert_eq!(r[0], [1,1,1,1,1,2]);
        next_pwd(&mut r[1], false);
        assert_eq!(r[1], [1,2,2,3,4,6]);
        let mut r = parse_input(&[String::from("223420-123789")]);
        next_pwd(&mut r[0], false);
        assert_eq!(r[0], [2,2,3,4,4,4]);
        next_pwd(&mut r[1], false);
        assert_eq!(r[1], [1,2,3,7,9,9]);
        let mut r = parse_input(&[String::from("223456-123799")]);
        next_pwd(&mut r[0], false);
        assert_eq!(r[0], [2,2,3,4,5,7]);
        next_pwd(&mut r[1], false);
        assert_eq!(r[1], [1,2,3,8,8,8]);
        let mut r = parse_input(&[String::from("123456-123799")]);
        next_pwd(&mut r[0], false);
        assert_eq!(r[0], [1,2,3,4,6,6]);
    }

    #[test]
    fn tests19_04_t2() {
        let mut highest_pwd = [6,7,3,2,5,1];
        let mut prev = 0;
        for i in 0 .. 6 {
            if prev > highest_pwd[i] {
                highest_pwd[i - 1] -= 1;
                for e in highest_pwd.iter_mut().skip(i) {
                    *e = 9;
                }

                break;
            }

            prev = highest_pwd[i];
        }
        assert_eq!(highest_pwd, [6,6,9,9,9,9]);
    }

    #[test]
    fn tests19_04_t3() {
        let r = parse_input(&[String::from("111111-122345")]);
        assert!(!is_valid(&r[0], true));
        assert!(is_valid(&r[1], true));
        let r = parse_input(&[String::from("223450-123589")]);
        assert!(!is_valid(&r[0], true));
        assert!(!is_valid(&r[1], true));
    }
}


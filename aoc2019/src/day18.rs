use std::collections::{HashMap, HashSet, VecDeque};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum MapElement {
    Wall,
    Open,
    Key(char),
    Door(char),
}

type Position = (usize, usize);
type GraphElem = (Position, char, usize, Vec<(Position, char)>);

#[derive(Clone, Debug)]
struct Map {
    map: Vec<Vec<MapElement>>,
    n_keys: usize,
}

impl Map {
    fn keys_or_doors(&self, get_keys: bool) -> Vec<(char, Position)> {
        let mut kys = Vec::new();
        for (row, elems) in self.map.iter().enumerate() {
            for (col, elem) in elems.iter().enumerate() {
                match *elem {
                    MapElement::Key(c)  if  get_keys => kys.push((c, (row, col))),
                    MapElement::Door(c) if !get_keys => kys.push((c, (row, col))),
                    _ => {}
                }
            }
        }

        kys
    }

    fn get_ch(&self, pos: &Position) -> char {
        let MapElement::Key(key) = self.map[pos.0][pos.1] else {
            return '@';
        };

        key
    }

    fn _prt(&self, entrances: &[Position]) {
        let mut map = Vec::new();
        for r in self.map.iter() {
            let mut row = String::new();
            for c in r.iter() {
                match *c {
                    MapElement::Door(d) => row.push(d),
                    MapElement::Key(k) => row.push(k),
                    MapElement::Open => row.push('.'),
                    MapElement::Wall => row.push('#'),
                }
            }
            map.push(row);
        }
        for ent in entrances {
            map[ent.0].replace_range(ent.1..=ent.1, "@");
        }
        for r in map.iter() {
            println!("{}", *r);
        }
    }
}

#[derive(Clone, Debug)]
struct Graph {
    start_pos: Position,
    graph: HashMap<Position, Vec<GraphElem>>,
    door_pointers: HashMap<char, Vec<Position>>,
}

impl Graph {
    fn new(pos: &Position) -> Self {
        Self { start_pos: *pos, graph: HashMap::new(), door_pointers: HashMap::new() }
    }

    fn insert(&mut self, pos: &Position, keys: &GraphElem) {
        self.graph
            .entry(*pos)
            .and_modify(|v| {
                if !v.contains(keys) {
                    v.push(keys.clone());
                }
            })
            .or_insert(vec![keys.clone()]);
    }

    fn def_door_pointers(&mut self) {
        for key in self.graph.keys() {
            for chld in self.graph.get(key).unwrap() {
                for door in chld.3.iter() {
                    let key_ch = door.1.to_ascii_lowercase();
                    self
                    .door_pointers
                    .entry(key_ch)
                    .and_modify(|poss| poss.push(*key))
                    .or_insert(vec![*key])
                    ;
                }
            }
        }
    }
}

fn get_neighbors((yi, xi): &Position, map: &Map, visited: &[Position], include_doors: bool) -> Vec<(Position, MapElement)> {
    let mut neighbors = Vec::new();
    for (xm, ym) in &[(2_usize,  1_usize), (0,  1), (1,  2), (1, 0)] {
        let y = *yi + ym - 1;
        let x = *xi + xm - 1;
        if visited.contains(&(y, x)) {
            continue;
        }
        let do_push = match map.map[y][x] {
            MapElement::Wall => false,
            MapElement::Open => true,
            MapElement::Key(_) => true,
            MapElement::Door(_) => include_doors,
        };
        if do_push {
            neighbors.push(((y, x), map.map[y][x]));
        }
    }

    neighbors
}

fn get_reachable_keys(map: &Map, start_pos: &Position, include_doors: bool) -> Vec<GraphElem> {
    let mut key_pos = Vec::new();
    let mut queue = VecDeque::new();
    queue.push_back((*start_pos, 0, Vec::new()));
    let mut visited = vec![*start_pos];

    while let Some((m_pos, steps, doors)) = queue.pop_front() {
        let neighbors = get_neighbors(&m_pos, map, &visited, include_doors);
        for (n_pos, elem) in &neighbors {
            if let MapElement::Key(key) = elem {
                key_pos.push((*n_pos, *key, steps + 1, doors.clone()));
            } else {
                let mut doors = doors.clone();
                if let MapElement::Door(door) = elem {
                    doors.push((*n_pos, *door));
                }
                queue.push_back((*n_pos, steps + 1, doors.clone()));
                visited.push(*n_pos);
            }
        }
    }

    key_pos
}

fn get_steps(map: &Map, graph_in: &Graph, pos: &Position, steps: usize, min_steps: &mut usize) {
    let mut queue = vec![(*pos, map.get_ch(pos), steps, graph_in.to_owned())];
    while let Some((parent_pos, parent_key, steps, grph)) = queue.pop() {
        if steps >= *min_steps {
            continue;
        }

        let mut graph = grph;
        let children = graph.graph.get(&parent_pos).unwrap().clone();
        graph.graph.remove(&parent_pos);
        if children.is_empty() && steps < *min_steps {
            *min_steps = steps;
        }
        for (child_pos, key, n_steps, doors) in children.iter() {
            // Replace current key in the child, with current key's children
            graph.graph.entry(*child_pos).and_modify(|grand_children| {
                let pos = grand_children.iter().position(|gr_child| gr_child.1 == parent_key).unwrap();
                grand_children.remove(pos);
                for new_child in children.iter() {
                    if new_child.1 != *key && !grand_children.iter().any(|c| c.1 == new_child.1) {
                        let mut new_doors = new_child.3.clone();
                        new_doors.extend(doors.iter());
                        grand_children.push((new_child.0, new_child.1, new_child.2 + n_steps, new_doors));
                    }
                }
            });
        }

        'child:
        for (child_pos, key, n_steps, doors) in children.iter() {
            for (_, d) in doors {
                if graph.graph.iter().any(|e| map.get_ch(e.0) == d.to_ascii_lowercase()) {
                    continue 'child;
                }
            }

            queue.push((*child_pos, *key, steps + n_steps, graph.clone()));
        }
    }
}

fn get_steps_b(map: &Map, graphs_in: &[Graph], min_steps: &mut usize) {
    let mut g_min_steps = vec![*min_steps; graphs_in.len()];
    let mut queue = Vec::new();
    for (ind, gr) in graphs_in.iter().enumerate() {
        queue.push((gr.start_pos, map.get_ch(&gr.start_pos), 0, graphs_in.to_owned(), ind))
    }
    while let Some((parent_pos, parent_key, steps, graphs, ind)) = queue.pop() {
        if steps >= g_min_steps[ind] {
            continue;
        }

        let mut graph = graphs[ind].clone();
        let children =
            if let Some(chldrn) = graph.graph.get(&parent_pos) {
                chldrn.to_owned()
            } else {
                Vec::new()
            }
        ;

        graph.graph.remove(&parent_pos);
        if children.is_empty() && steps < g_min_steps[ind] {
            g_min_steps[ind] = steps;
        }
        for (child_pos, child_key, n_steps, doors) in children.iter() {
            // Replace current key in the child, with current key's children
            graph.graph.entry(*child_pos).and_modify(|grand_children| {
                let pos_o = grand_children.iter().position(|gr_child| {
                    gr_child.1 == parent_key
                });
                if let Some(pos) = pos_o {
                    // The above is always true unless start_pos is @ ???
                    grand_children.remove(pos);
                    for new_child in children.iter() {
                        if new_child.1 != *child_key && !grand_children.iter().any(|c| c.1 == new_child.1) {
                            let mut new_doors = new_child.3.clone();
                            new_doors.extend(doors.iter());
                            for new_door in new_doors.iter() {
                                graph.door_pointers
                                .entry(new_door.1.to_ascii_lowercase())
                                .and_modify(|dp| {
                                    dp.retain(|&pos| pos != graph.start_pos);
                                    if !dp.contains(child_pos) {
                                        dp.push(*child_pos);
                                    }
                                });
                            }
                            grand_children.push((new_child.0, new_child.1, new_child.2 + n_steps, new_doors));
                        }
                    }
                }
            });
        }

        'child:
        for (child_pos, key, n_steps, doors) in children.iter() {
            for (_, d) in doors {
                if graph.graph.iter().any(|e| map.get_ch(e.0) == d.to_ascii_lowercase()) {
                    continue 'child;
                }
            }

            let mut graph_t = graph.clone();
            graph_t.graph.remove(&graph_t.start_pos);
            graph_t.start_pos = *child_pos;
            let mut graphs_t = graphs.clone();
            graphs_t[ind] = graph_t;

            queue.push((*child_pos, *key, steps + n_steps, graphs_t, ind));
        }
    }

    *min_steps = g_min_steps.iter().sum();
}

fn get_no_of_steps_a(map: &Map, start_pos: &Position) -> usize {
    let mut least_n_steps = usize::MAX;

    let reachable_keys = get_reachable_keys(map, start_pos, false);
    let mut graph = Graph::new(start_pos);
    for key in reachable_keys.iter() {
        graph.insert(start_pos, key);
    }
    let mut reachables_queue = VecDeque::new();
    let mut visited = Vec::new();
    reachables_queue.push_back(reachable_keys.clone());
    while let Some(reach_keys) = reachables_queue.pop_front() {
        for pos_from in reach_keys.iter() {
            if visited.contains(&pos_from.0) {
                continue;
            }
            let next_reachable_keys = get_reachable_keys(map, &pos_from.0, true);
            visited.push(pos_from.0);
            for (next_reachable, key, steps, doors) in next_reachable_keys.iter() {
                let (from, to) = (pos_from.0, *next_reachable);
                let to_n_pos = (to, *key, *steps, doors.clone());
                graph.insert(&from, &to_n_pos);
            }
            reachables_queue.push_back(next_reachable_keys);
        }
    }

    for (pos, _, n_steps, _) in reachable_keys {
        get_steps(map, &graph, &pos, n_steps, &mut least_n_steps);
    }

    least_n_steps
}

fn _prtgrph(map: &Map, graph: &HashMap<Position, Vec<GraphElem>>, entrance: &Position) {
    let mut prtd = HashSet::new();
    println!("@");

    fn prt(map: &Map, grph: &HashMap<Position, Vec<GraphElem>>, pos: &Position, prtd: &mut HashSet<char>, lev: usize) {
        let k = map.get_ch(pos);
        if prtd.contains(&k) {
            return;
        }
        println!("{}{}", "  ".repeat(lev), k);
        prtd.insert(k);
        if let Some(chldr) = grph.get(pos) {
            for chld in chldr {
                prt(map, grph, &chld.0, prtd, lev + 1);
            }
        }
    }
    for chl in graph.get(entrance).unwrap() {
        prtd.insert(chl.1);
    }
    for chl in graph.get(entrance).unwrap() {
        prtd.remove(&chl.1);
        prt(map, graph, &chl.0, &mut prtd, 1);
    }
}

fn get_no_of_steps_b(map: &Map, start_positions: &[Position]) -> usize {
    let mut least_n_steps = usize::MAX;
    let mut graphs = Vec::new();

    for start_pos in start_positions {
        let mut reachable_keys = get_reachable_keys(map, start_pos, false);
        if reachable_keys.is_empty() {
            reachable_keys = get_reachable_keys(map, start_pos, true);
        }
        let mut graph = Graph::new(start_pos);
        for key in reachable_keys.iter() {
            graph.insert(start_pos, key);
        }
        let mut reachables_queue = VecDeque::new();
        let mut visited = Vec::new();
        reachables_queue.push_back(reachable_keys.clone());
        while let Some(reach_keys) = reachables_queue.pop_front() {
            for pos_from in reach_keys.iter() {
                if visited.contains(&pos_from.0) {
                    continue;
                }
                let next_reachable_keys = get_reachable_keys(map, &pos_from.0, true);
                visited.push(pos_from.0);
                for (next_reachable, key, steps, doors) in next_reachable_keys.iter() {
                    let (from, to) = (pos_from.0, *next_reachable);
                    let to_n_pos = (to, *key, *steps, doors.clone());
                    graph.insert(&from, &to_n_pos);
                }
                reachables_queue.push_back(next_reachable_keys);
            }
        }

        graph.def_door_pointers();
        graphs.push(graph);
    }

    get_steps_b(map, &graphs, &mut least_n_steps);

    least_n_steps
}

fn task_a(lines: &[String]) {
    let (map, entrance) = parse_input(lines);
    let no_of_steps = get_no_of_steps_a(&map, &entrance);

    println!("# of steps {}", no_of_steps);
}

fn task_b(lines: &[String]) {
    let (mut map, entrance) = parse_input(lines);
    for dw in [(1, 1), (1, 0), (1, 2), (0, 1), (2, 1)] {
        map.map[entrance.0 + dw.0 - 1][entrance.1 + dw.1 - 1] = MapElement::Wall;
    }
    let entrances = [
                                            (entrance.0 - 1, entrance.1 - 1),
                                            (entrance.0 - 1, entrance.1 + 1),
                                            (entrance.0 + 1, entrance.1 - 1),
                                            (entrance.0 + 1, entrance.1 + 1)
                                         ];
    let no_of_steps = get_no_of_steps_b(&map, &entrances);

    println!("# of steps {}", no_of_steps);
}

fn parse_input(lines: &[String]) -> (Map, Position) {
    let mut position = (0, 0);
    let map0 = lines
        .iter()
        .enumerate()
        .map(|(row, line)|
            line
            .chars()
            .enumerate()
            .map(|(col, elem)|
                match elem {
                    '#' => MapElement::Wall,
                    '.' => MapElement::Open,
                    c @ 'a' ..= 'z' => MapElement::Key(c),
                    c @ 'A' ..= 'Z' => MapElement::Door(c),
                    '@' => {
                        position = (row, col);
                        MapElement::Open
                    }
                    _   => panic!("Bad input: {}", elem),
                }
            )
            .collect::<Vec<_>>()
        )
    .collect::<Vec<_>>();

    let mut map = Map {map: map0, n_keys: 0};
    map.n_keys = map.keys_or_doors(true).len();

    (map, position)
}

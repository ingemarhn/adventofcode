pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ImageData = Vec<Vec<u8>>;

fn validate(image: &ImageData) -> usize {
    let count_fig = |ind: usize, fig| {
        image[ind].iter().filter(|e| **e == fig).count()
    };
    let mut fewest_0_data = (image[0].len(), 0);
    for i in 0 .. image.len() {
        let count_0 = count_fig(i, 0);
        if count_0 < fewest_0_data.0 {
            fewest_0_data = (count_0, i);
        }
    }

    count_fig(fewest_0_data.1, 1) * count_fig(fewest_0_data.1, 2)
}

fn build_image(image: &ImageData) -> [[u8; 25]; 6] {
    let mut visible_image = [[2; 25]; 6];

    for layer in image.iter() {
        for (row_n, row_data) in layer.chunks(25).enumerate() {
            for (col_n, col_data) in row_data.iter().enumerate() {
                if visible_image[row_n][col_n] == 2 {
                    visible_image[row_n][col_n] = *col_data;
                }
            }
        }
    }

    visible_image
}

fn task_a(lines: &[String]) {
    let image = parse_input(lines);
    let one_times_two = validate(&image);

    println!("Validation product = {}", one_times_two);
}

fn task_b(lines: &[String]) {
    let image = parse_input(lines);
    let visible_image = build_image(&image);

    for row in visible_image.iter() {
        for col in row.iter() {
            print!("{}", if *col == 1 { '#' } else { ' ' });
        }
        println!();
    }
}

fn parse_input(lines: &[String]) -> ImageData {
    lines[0]
        .chars()
        .map(|c| c as u8 - 48)
        .collect::<Vec<u8>>()
        .chunks(25 * 6)
        .map(|ch| ch.to_vec())
        .collect::<ImageData>()
}

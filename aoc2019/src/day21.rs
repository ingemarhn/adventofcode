use crate::intcode::*;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn run_script(script: String, int_prog: &Program) -> ProgValue {
    let mut script_iter = script.chars();
    let mut get_input = || {
        script_iter.next().unwrap() as isize
    };

    let mut int_code = int_prog.clone();
    let mut damage = 0;
    loop {
        let (did_exit, out) = int_code.run(&mut get_input, false);
        if did_exit {
            break;
        }
        damage = out;
        if damage < 256 {
            print!("{}", damage as u8 as char)
        }
    }

    damage
}

fn task_a(lines: &[String]) {
    let int_program = parse_input(lines);
    let spring_script = Vec::from([
        "NOT B J",
        "NOT C T",
        "AND T J",
        "AND D J",
        "NOT A T",
        "OR T J",
        "NOT C T",
        "AND D T",
        "OR T J",
        "WALK\n"
    ]);

    let damage = run_script(spring_script.join("\n"), &int_program);

    println!("Hull damage = {}", damage);
}

fn task_b(lines: &[String]) {
    let int_program = parse_input(lines);
    let spring_script = Vec::from([
        "NOT B J",
        "AND D J",
        "NOT A T",
        "OR T J",
        "NOT C T",
        "AND D T",
        "AND H T",
        "OR T J",
        "RUN\n"
    ]);

    let damage = run_script(spring_script.join("\n"), &int_program);

    if damage > 256 {
        println!("Hull damage = {}", damage);
    }
}

fn parse_input(lines: &[String]) -> Program {
    Program::parse_intcode(lines)
}

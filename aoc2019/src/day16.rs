pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Digit = i32;
type DigiList = Vec<Digit>;

fn phase(input: &DigiList, offset: usize) -> DigiList {
    let mut output = vec![0; offset];
    let base_pattern = [0, 1, 0, -1];

    let mut digits_sum = 0;
    let mut rev_values = Vec::new();
    for pattern_sub_len in offset + 1 ..= input.len() {
        if offset == 0 {
            digits_sum = 0;
            // First digit in base_pattern is 0. Therefore we can just skip the first input figures that should have been multiplied with 0
            // By that, we can also just skip the first digit in base_pattern
            let mut base_patt_it = base_pattern.iter().cycle().skip(1);

            let input_it = input[pattern_sub_len - 1 .. ].chunks(pattern_sub_len);
            for input_digs in input_it {
                let patt_dig = *base_patt_it.next().unwrap();
                match patt_dig {
                    -1 => digits_sum -= input_digs.iter().sum::<Digit>(),
                    0 => {},
                    1 => digits_sum += input_digs.iter().sum::<Digit>(),
                    _  => panic!()
                }
            }

            output.push(digits_sum.abs() % 10);
        } else {
            /*
            At offset N into the string the pattern has N-1 0 then N 1, then N -1 then N 0. For part 2 the string length is 6 500 000 and the offset
            is (if you look) more than half that. So the output digit at N is just the sum of all the input digits between N and the end. The next
            output digit is the same sum but without the input digit at N, and so on.

            This is where the formula you've quoted comes from. If you do this from the end of the string back towards the offset you are given,
            each new output digit is the sum of the previous output digit in the new phase (before the mod 10 I think) and the output digit from
            the previous phase.

            So, to turn …….abcdef into …….ghijkl

            a+b+c+d+e+f = g = a+h
            b+c+d+e+f = h = b+i
                c+d+e+f = i = c+j
                d+e+f = j = d+k
                    e+f = k = e+l
                    f = l
            where ……. is the contents of the string prior to the offset.
            */

            let ind = offset + input.len() - pattern_sub_len;
            digits_sum += input[ind];

            rev_values.push(digits_sum);
        }
    }
    for v in rev_values.iter().rev() {
        output.push(v % 10)
    }

    output
}

fn get_eight_from_offset(input_start: &DigiList, n_duplicates: usize, offset: usize) -> String {
    let mut input = Vec::new();
    for _ in 0 .. n_duplicates {
        input.extend(input_start.iter());
    }

    let mut count = 0;
    let output = loop {
        let phase_output = phase(&input, offset);
        count += 1;
        if count == 100 {
            break phase_output;
        }

        input = phase_output;
    };

    let mut ret = String::new();
    ret.extend(output[offset .. offset + 8].iter().map(|&dig| (dig as u8 + b'0') as char));

    ret
}

fn task_a(lines: &[String]) {
    let input_list = parse_input(lines);
    let eight_dig = get_eight_from_offset(&input_list, 1, 0);

    println!("Eight first: {}", eight_dig);
}

fn task_b(lines: &[String]) {
    let input_list = parse_input(lines);
    let offset = input_list[0 .. 7].iter().fold(0, |offs, &dig| {
        offs * 10 + dig as usize
    });
    let eight_dig = get_eight_from_offset(&input_list, 10_000, offset);
    println!("Message: {}", eight_dig);
}

fn parse_input(lines: &[String]) -> DigiList {
    lines[0]
        .chars()
        .map(|line|
            line
            .to_digit(10)
            .unwrap() as Digit
        )
        .collect::<Vec<_>>()
}

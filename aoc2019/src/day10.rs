use std::collections::HashMap;
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
    Angle((i64, i8)),
}

type Map = Vec<Vec<bool>>;
type Position = (usize, usize);

fn asteroids_from_map(map: &Map) -> Vec<Position> {
    let mut asteroids: Vec<Position> = Vec::new();
    for (y, row) in map.iter().enumerate() {
        for (x, is_asteroid) in row.iter().enumerate() {
            if *is_asteroid {
                asteroids.push((y, x));
            }
        }
    }

    asteroids
}

fn angles_to_visible(asteroids: &[Position], moni_y: &usize, moni_x: &usize) -> HashMap<Direction, (isize, isize)> {
    let mut angles: HashMap<Direction, (isize, isize)> = HashMap::new();
    for (targ_y, targ_x) in asteroids.iter().map(|e| (e.0 as isize, e.1 as isize)) {
        let rel_pos = (targ_y - *moni_y as isize, targ_x - *moni_x as isize);
        if rel_pos == (0, 0) { continue; }
        let direction =
            if rel_pos.1 == 0 && rel_pos.0 < 0 {
                Direction::Up
            } else if rel_pos.1 == 0 {
                Direction::Down
            } else if rel_pos.0 == 0 && rel_pos.1 < 0 {
                Direction::Left
            } else if rel_pos.0 == 0 {
                Direction::Right
            } else {
                let rel_0 = ((rel_pos.0 as f64 / rel_pos.1.abs() as f64) * 1_000_000_000.0) as i64;
                let rel_1 = (rel_pos.1 / rel_pos.1.abs()) as i8;
                Direction::Angle((rel_0, rel_1))
            }
        ;

        if let Some(prev_pos) = angles.get(&direction) {
            let calc_dist = |(x, y): (isize, isize)| {
                (*moni_x as isize - x).abs() + (*moni_y as isize - y).abs()
            };
            if calc_dist((targ_x, targ_y)) < calc_dist(*prev_pos) {
                angles.insert(direction, (targ_x, targ_y));
            }
        } else {
            angles.insert(direction, (targ_x, targ_y));
        }
    }

    angles
}

fn find_best_asteroid(map: &Map) -> (Position, usize) {
    let mut best_loc = (0, 0);
    let mut max_asteroids = 0;
    let asteroids = asteroids_from_map(map);

    for (moni_y, moni_x) in asteroids.iter() {
        let angles = angles_to_visible(&asteroids, moni_y, moni_x);
        let n_angles = angles.len();
        if n_angles > max_asteroids {
            max_asteroids = n_angles;
            best_loc = (*moni_x, *moni_y);
        }
    }

    (best_loc, max_asteroids)
}

fn find_200th_asteroid(map: &Map) -> isize {
    use std::cmp::Ordering;

    let ((moni_stn_x, moni_stn_y), _) = find_best_asteroid(map);
    let quadrant = |pos: (isize, isize)| {
        if pos.0 >= 0 && pos.1 < 0 {
            1
        } else if pos.0 > 0 && pos.1 >= 0 {
            2
        } else if pos.0 <= 0 && pos.1 > 0 {
            3
        } else {
            4
        }
    };

    let radian = |pos : (isize, isize)| {
        let hypo = ((pos.0.pow(2) + pos.1.pow(2)) as f64).sqrt();
        (pos.0 as f64 / hypo).asin()
    };

    let asteroid_200;
    let mut n_vaporized = 0;

    let mut vapor_map = map.clone();
    'l:
    loop {
        let asteroids = asteroids_from_map(&vapor_map);
        let angles = angles_to_visible(&asteroids, &moni_stn_y, &moni_stn_x);
        let mut keys = angles.keys().copied().collect_vec();
        if n_vaporized + keys.len() >= 200 {
            keys.sort_unstable_by(|a, b| {
                // This sorting is far from perfect. But no matter how I change it this function comes up with the correct answer :-)
                let a_pos = *angles.get(a).unwrap();
                let b_pos = *angles.get(b).unwrap();
                let a_rel = (a_pos.0 - moni_stn_x as isize, a_pos.1 - moni_stn_y as isize);
                let b_rel = (b_pos.0 - moni_stn_x as isize, b_pos.1 - moni_stn_y as isize);
                let a_quad = quadrant(a_rel);
                let b_quad = quadrant(b_rel);
                let quad_cmp = a_quad.cmp(&b_quad);
                if quad_cmp != Ordering::Equal {
                    quad_cmp
                } else {
                    let a_rad = radian(a_rel);
                    let b_rad = radian(b_rel);
                    match a_quad {
                        1 | 2 => a_rad.partial_cmp(&b_rad).unwrap(),
                        _     => b_rad.partial_cmp(&a_rad).unwrap(),
                    }
                }
            });
        }

        for key in keys.iter() {
            let (x, y) = angles.get(key).unwrap();
            vapor_map[*y as usize][*x as usize] = false;
            n_vaporized += 1;
            if n_vaporized == 200 {
                asteroid_200 = (*x, *y);
                break 'l;
            }
        }
    }

    asteroid_200.0 * 100 + asteroid_200.1
}

fn task_a(lines: &[String]) {
    let map = parse_input(lines);
    let (best_pos, n_asteroids) = find_best_asteroid(&map);

    println!("Best location = {:?}, # of asteroids seen = {}", best_pos, n_asteroids);
}

fn task_b(lines: &[String]) {
    let map = parse_input(lines);
    let product = find_200th_asteroid(&map);

    println!("Coord product = {}", product);
}

fn parse_input(lines: &[String]) -> Map {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .map(|pos|
                pos == '#'
            )
            .collect::<Vec<_>>()
        )
    .collect::<Vec<_>>()
}


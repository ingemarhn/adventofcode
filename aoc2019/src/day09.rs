use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ProgValue = i64;
type Program = HashMap<usize, ProgValue>;

fn run_program(program: &Program, inp: &[ProgValue], run_to_exit: bool, instr_ptr: &mut usize) -> (bool, ProgValue, Program) {
    let mut program = program.clone();
    let mut prog_val = ProgValue::MIN;
    let mut relative_base = 0;
    let mut inp_iter = inp.iter();

    let get_value = |instr_ptr: &mut usize, program: &Program, mode, relative_base| {
        *instr_ptr += 1;
        let value_ptr =
            match mode {
                0 => program[instr_ptr] as usize,
                1 => *instr_ptr,
                2 => (program[instr_ptr] + relative_base) as usize,
                m => panic!("Illegal mode: {m}"),
        };
        if program.contains_key(&value_ptr) {
            program[&value_ptr]
        } else {
            0
        }
    };

    let get_three_params = |instr_ptr: &mut usize, program: &Program, elems: (ProgValue, ProgValue, ProgValue, ProgValue), relative_base| {
        (
            get_value(instr_ptr, program, elems.1, relative_base),
            get_value(instr_ptr, program, elems.2, relative_base),
            {
                let mut third = get_value(instr_ptr, program, 1, relative_base);
                if elems.3 == 2 {
                    third += relative_base;
                }
                third as usize
            },
        )
    };

    let mut did_exit = true;
    while program[instr_ptr] != 99 {
        let elems = match program.get(instr_ptr).unwrap() {
            op @ 1 ..= 98 => {
                (*op, 0, 0, 0)
            },
            param => {
                let oper = *param % 100;
                let rest = *param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;
                let mode3 = if *param < 10000 { 1 } else { rest / 10 };

                (oper, mode1, mode2, mode3)
            }
        };

        let mut mod_prog_val = None;
        match elems.0 {
            1 => {  // Add
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                mod_prog_val = Some((reg, v1 + v2));
            },
            2 => {  // Multiply
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                mod_prog_val = Some((reg, v1 * v2));
            },
            3 => {  // Get input
                let mut reg = get_value(instr_ptr, &program, 1, relative_base) as usize;
                if elems.1 == 2 {
                    reg += relative_base as usize;
                }
                mod_prog_val = Some((reg, *inp_iter.next().unwrap()));
            },
            4 => {  // Output
                prog_val = get_value(instr_ptr, &program, elems.1, relative_base);
                println!(">> {}", prog_val);
                if !run_to_exit {
                    did_exit = false;
                    *instr_ptr += 1;
                    break;
                }
            },
            op @ (5 | 6) => {   // Jump if true | false
                let v1 = get_value(instr_ptr, &program, elems.1, relative_base);
                let v2 = get_value(instr_ptr, &program, elems.2, relative_base);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    *instr_ptr = v2 as usize;
                    continue;
                }
            },
            op @ (7 | 8) => {   // If is less than | equal
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) { 1 } else { 0 };
                mod_prog_val = Some((reg, v));
            },
            9 => {
                relative_base += get_value(instr_ptr, &program, elems.1, relative_base);
            },

            u => panic!("Unsupported oper: {}", u)
        }

        if let Some((reg, v)) = mod_prog_val {
            program.entry(reg).and_modify(|e| *e = v).or_insert(v);
        }
        *instr_ptr += 1;
    }

    (did_exit, prog_val, program)
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let boost_keycode = run_program(&program, &[1], true, &mut 0);

    println!("BOOST key code = {}", boost_keycode.1);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let distress_signal = run_program(&program, &[2], true, &mut 0);

    println!("Coordinates = {}", distress_signal.1);
}

fn parse_input(lines: &[String]) -> Program {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect()
}

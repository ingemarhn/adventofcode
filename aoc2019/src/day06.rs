use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Obj<'a> = &'a str;
type ObjOrbs<'a> = Vec<(Obj<'a>, Obj<'a>)>;

fn build_orbits_map(obj_orbs: ObjOrbs) -> HashMap<Obj, Vec<Obj>> {
    let mut orbits_map = HashMap::new();

    for (obj, orb) in obj_orbs.iter() {
        orbits_map.entry(*obj).and_modify(|e: &mut Vec<Obj>| e.push(*orb)).or_insert(vec![*orb]);
    }

    orbits_map
}

fn calc_n_orbits(map: HashMap<Obj, Vec<Obj>>) -> u32 {
    let mut orbit_counter = HashMap::new();
    let mut keys = vec![("COM", 0)];
    while let Some((key, orb_cnt)) = keys.pop() {
        orbit_counter.insert(key, orb_cnt);
        if let Some(orbs) = map.get(key) {
            for orb in orbs.iter() {
                keys.push((*orb, orb_cnt + 1));
            }
        }
    }

    orbit_counter.iter().map(|(_, &e)| e).sum()
}

fn calc_n_transfers(map: HashMap<Obj, Vec<Obj>>) -> usize {
    let mut paths = HashMap::new();
    let mut keys = vec![("COM", Vec::new())];
    while let Some((key, mut path)) = keys.pop() {
        path.push(key);
        if let Some(orbs) = map.get(key) {
            for orb in orbs.iter() {
                keys.push((*orb, path.clone()));
                if ["YOU", "SAN"].contains(orb) {
                    paths.insert(*orb, path.clone());
                }
            }
        }
    }

    let mut count = 0;
    let n_trans = loop {
        let mut tail = "";
        paths.entry("YOU").and_modify(|p| tail = p.pop().unwrap());
        if let Some(ind) = paths["SAN"].iter().rposition(|e| *e == tail) {
            break count - 1 + paths[&"SAN"].len() - ind;
        }
        count += 1;
    };

    n_trans
}

fn task_a(lines: &[String]) {
    let obj_orbs = parse_input(lines);
    let map = build_orbits_map(obj_orbs);
    let n_orbits = calc_n_orbits(map);

    println!("# of orbits = {}", n_orbits);
}

fn task_b(lines: &[String]) {
    let obj_orbs = parse_input(lines);
    let map = build_orbits_map(obj_orbs);
    let n_transfers = calc_n_transfers(map);

    println!("# of transfers = {}", n_transfers);
}

fn parse_input(lines: &[String]) -> ObjOrbs {
    lines
        .iter()
        .map(|line|
            line
            .split_once(')')
            .unwrap()
        )
        .collect::<Vec<_>>()
}

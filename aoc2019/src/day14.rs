use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Chemical = String;
type Counter = u64;
type Reactions = HashMap<Chemical, Reaction>;

#[derive(Clone, Debug)]
struct Reaction {
    output_count: Counter,
    input: Vec<(Counter, Chemical)>,
}

fn get_input_chemicals(reactions_start: Reactions, chemicals_start: Vec<(Counter, Chemical)>) -> Counter {
    let mut left_overs = HashMap::new();
    let mut reactions = reactions_start.clone();
    if chemicals_start[0].0 > 1 {
        reactions.entry(chemicals_start[0].1.clone()).and_modify(|r| {
            r.output_count = chemicals_start[0].0;
            for inp in r.input.iter_mut() {
                inp.0 *= chemicals_start[0].0;
            }
        });
    }

    let mut chemicals = chemicals_start.clone();
    loop {
        let mut repl_done = false;
        let mut collected_chemicals = HashMap::new();
        for chemical in chemicals.iter() {
            let replace_chemicals = reactions.get(&chemical.1).unwrap();
            if replace_chemicals.input[0].1 == "ORE" {
                collected_chemicals.entry(&chemical.1).and_modify(|e| *e += chemical.0).or_insert(chemical.0);
                continue;
            }

            let n_leftover = if let Some(l) = left_overs.get(&chemical.1) { *l } else { 0 };
            let n_chemicals_needed = if chemical.0 >= n_leftover {
                left_overs.remove(&chemical.1);
                chemical.0 - n_leftover
            } else {
                left_overs.entry(chemical.1.clone()).and_modify(|e| *e -= chemical.0);
                0
            };
            let add = if n_chemicals_needed % replace_chemicals.output_count > 0 { 1 } else { 0 };
            let factor = n_chemicals_needed / replace_chemicals.output_count + add;
            let n_left = factor * replace_chemicals.output_count - n_chemicals_needed;
            if n_left != 0 {
                left_overs.entry(chemical.1.clone()).and_modify(|e| *e += n_left).or_insert(n_left);
            }

            for chem in replace_chemicals.input.iter() {
                collected_chemicals.entry(&chem.1).and_modify(|e| *e += chem.0 * factor).or_insert(chem.0 * factor);
            }

            repl_done = true;
        }

        chemicals = Vec::from_iter(collected_chemicals.iter().map(|(&s, &v)| (v, s.clone())));

        if !repl_done {
            break;
        }
    }

    let mut ore_subtract = 0;
    for lo in left_overs.iter() {
        let replace_chemicals = reactions.get(lo.0).unwrap();
        if replace_chemicals.input[0].1 != "ORE" {
            continue;
        }
        if *lo.1 >= replace_chemicals.output_count {
            let factor = *lo.1 / replace_chemicals.output_count;
            ore_subtract += replace_chemicals.input[0].0 * factor;
        }
    }

    chemicals
        .iter()
        .fold(0, |ore_count, (numb_collected, chem_name )|
    {
        let reaction = reactions.get(chem_name).unwrap();
        let add = if *numb_collected % reaction.output_count > 0 { 1 } else { 0 };
        let factor = *numb_collected / reaction.output_count + add;
        ore_count + reaction.input[0].0 * factor
    }) - ore_subtract
}


fn task_a(lines: &[String]) {
    let reactions = parse_input(lines);
    let n_ore = get_input_chemicals(reactions, vec![(1, "FUEL".to_string())]);

    println!("# ORE: {}", n_ore);
}

fn task_b(lines: &[String]) {
    let cargo_hold = 1_000_000_000_000;
    let reactions = parse_input(lines);
    let mut n_ore = get_input_chemicals(reactions.clone(), vec![(1, "FUEL".to_string())]);
    let mut n_fuel = cargo_hold / n_ore;

    loop {
        let n_ore_prev = n_ore;
        n_ore = get_input_chemicals(reactions.clone(), vec![(n_fuel, "FUEL".to_string())]);
        if n_ore == n_ore_prev {
            break
        }
        n_fuel += (n_fuel as f64 * ((cargo_hold as f64 - n_ore as f64) / n_ore as f64)) as Counter;
    }

    println!("Max # FUEL: {}", n_fuel);
}

fn parse_input(lines: &[String]) -> Reactions {
    let mut reactions = HashMap::new();

    for line in lines {
        let input_output: Vec<&str> = line.split(" => ").collect();
        let output: Vec<&str> = input_output[1].split(' ').collect();
        let mut input = Vec::new();
        for inp_str in input_output[0].split(", ") {
            let one_chem: Vec<&str> = inp_str.split(' ').collect();
            input.push((
                one_chem[0].parse().unwrap(),
                one_chem[1].to_string()
            ))
        }

        let reaction = Reaction { output_count: output[0].parse().unwrap(), input };
        reactions.insert(output[1].to_string(), reaction);
    }

    reactions
}

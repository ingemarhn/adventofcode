use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ProgValue = i32;
type Program = HashMap<usize, ProgValue>;

fn run_program(program: &Program, inp: &[ProgValue], run_to_exit: bool, instr_ptr: &mut usize) -> (bool, ProgValue, Program) {
    let mut program = program.clone();
    let mut prog_val = ProgValue::MIN;
    let mut inp_iter = inp.iter();

    let get_value = |instr_ptr: &mut usize, program: &Program, mode| {
        *instr_ptr += 1;
        match mode {
            0 => program[ &(program[instr_ptr] as usize) ],
            1 => program[instr_ptr],
            m => panic!("Illegal mode: {m}"),
        }
    };

    let get_three_params = |instr_ptr: &mut usize, program: &Program, m1, m2| {
        (
            get_value(instr_ptr, program, m1),
            get_value(instr_ptr, program, m2),
            get_value(instr_ptr, program, 1) as usize,
        )
    };

    let mut did_exit = true;
    while program[instr_ptr] != 99 {
        let elems = match program[instr_ptr] {
            op @ 1 ..= 98 => {
                (op, 0, 0)
            },
            param => {
                let oper = param % 100;
                let rest = param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;

                (oper, mode1, mode2)
            }
        };

        match elems.0 {
            1 => {  // Add
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems.1, elems.2);
                program.entry(reg).and_modify(|e| *e = v1 + v2);
            },
            2 => {  // Multiply
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems.1, elems.2);
                program.entry(reg).and_modify(|e| *e = v1 * v2);
            },
            3 => {  // Get input
                let reg = get_value(instr_ptr, &program, 1) as usize;
                program.entry(reg).and_modify(|e| *e = *inp_iter.next().unwrap());
            },
            4 => {  // Output
                prog_val = get_value(instr_ptr, &program, elems.1);
                if !run_to_exit {
                    did_exit = false;
                    *instr_ptr += 1;
                    break;
                }
            },
            op @ (5 | 6) => {   // Jump if true | false
                let v1 = get_value(instr_ptr, &program, elems.1);
                let v2 = get_value(instr_ptr, &program, elems.2);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    *instr_ptr = v2 as usize;
                    continue;
                }
            },
            op @ (7 | 8) => {   // If is less than | equal
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems.1, elems.2);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) { 1 } else { 0 };
                program.entry(reg).and_modify(|e| *e = v);
            },
            u => panic!("Unsupported oper: {}", u)
        }

        *instr_ptr += 1;
    }

    (did_exit, prog_val, program)
}

fn calc_highest_signal_a(program: &Program) -> ProgValue {
    let mut highest_sig = ProgValue::MIN;

    fn run_one_amp(program: &Program, inp_sig: ProgValue, used_phases_in: Vec<ProgValue>, highest_sig: &mut ProgValue) {
        for phase in 0 .. 5 {
            let mut used_phases = used_phases_in.clone();
            if used_phases.contains(&phase) { continue; }
            used_phases.push(phase);
            let inp = [phase, inp_sig];
            let (_, out_v, _) = run_program(program, &inp, true, &mut 0);
            let out = out_v;
            if used_phases.len() < 5 {
                run_one_amp(program, out, used_phases, highest_sig);
            } else {
                let mut ov = out;
                *highest_sig = *highest_sig.max(&mut ov);
            }
        }
    }

    run_one_amp(program, 0, Vec::new(), &mut highest_sig);

    highest_sig
}

fn calc_highest_signal_b(program_orig: &Program) -> ProgValue {
    let mut highest_sig = ProgValue::MIN;
    let mut phase_combinations = Vec::new();

    fn build_phases(phase_combinations: &mut Vec<Vec<ProgValue>>, used_phases_in: Vec<ProgValue>) {
        for phase in 5 .. 10 {
            let mut used_phases = used_phases_in.clone();
            if used_phases.contains(&phase) { continue; }
            used_phases.push(phase);
            if used_phases.len() < 5 {
                build_phases(phase_combinations, used_phases);
            } else {
                phase_combinations.push(used_phases);
            }
        }
    }

    build_phases(&mut phase_combinations, Vec::new());

    for phase_combination in phase_combinations.iter() {
        let mut programs = vec![program_orig.clone(); 5];
        let mut instr_pointers = [0_usize; 5];
        let mut inputs = phase_combination.iter().map(|p| vec![*p]).collect::<Vec<_>>();
        let mut amp_out = 0;
        let mut exit_on_99 = false;

        loop {
            let mut inp_iter = inputs.iter_mut();
            let mut instr_ptr_iter = instr_pointers.iter_mut();
            for program in programs.iter_mut() {
                let instr_ptr = instr_ptr_iter.next().unwrap();
                let inp = inp_iter.next().unwrap();
                inp.push(amp_out);

                let out = run_program(program, inp, false, instr_ptr);
                amp_out = out.1;
                *program = out.2;
                exit_on_99 = out.0;
            }

            highest_sig = highest_sig.max(amp_out);
            if exit_on_99 {
                break;
            }
            inputs = vec![Vec::new(); 5];
        }
    }

    highest_sig
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let sig = calc_highest_signal_a(&program);

    println!("Highest signal = {}", sig);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let sig = calc_highest_signal_b(&program);

    println!("Highest signal = {}", sig);
}

fn parse_input(lines: &[String]) -> Program {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect()
}

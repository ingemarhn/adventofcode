use crate::intcode::*;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::VecDeque;
    use super::{ProgValue, Program};

    pub struct Computer {
        program: Program,
        queue: VecDeque<ProgValue>,
    }

    impl Computer {
        pub fn new(prog: &Program, address: isize) -> Self {
            let mut comp = Self {
                program: prog.clone(),
                queue: VecDeque::new(),
            };
            comp.queue.push_back(address);

            comp
        }

        pub fn add_to_queue(&mut self, val: ProgValue) {
            self.queue.push_back(val);
        }

/*         pub fn add_to_nat_queue(&mut self, val: ProgValue) {
            self.queue.push_back(val);
            if self.queue.len() > 2 {
                self.queue.pop_front();
            }
        }

        // Needed for NAT
        pub fn pop_queue(&mut self) -> ProgValue {
            self.queue.pop_front().unwrap()
        } */

        pub fn run(&mut self) -> (bool, ProgValue) {
            let mut get_input = || self.queue.pop_front().unwrap_or(-1);
            self.program.run_check_input(&mut get_input, false, -1)
        }
    }
}
use internal::*;

fn run_nic(prog: &Program, n_comp: ProgValue, run_with_nat: bool) -> ProgValue {
    let mut computers: Vec<Computer> =
        (0 .. n_comp)
        .map(|n| Computer::new(prog, n))
        .collect();
    let mut nat_packet = (0, 0);
    let mut idle_count = 0;
    let mut last_y_to_0 = ProgValue::MIN;

    let mut comp_ind = 0;
    loop {
        let mut state = 0;
        let mut send_to = computers.len();
        loop {
            let (_, retv) = computers[comp_ind].run();
            if retv == ProgValue::MIN {
                comp_ind = (comp_ind + 1) % computers.len();
                idle_count += 1;
                break
            }

            idle_count = 0;
            match state {
                0 => {
                    if retv != 255 {
                        send_to = retv as usize;
                        state += 1;
                    } else {
                      state = 11;
                    }
                },
                1 => {
                    computers[send_to].add_to_queue(retv);
                    state += 1;
                },
                2 => {
                    computers[send_to].add_to_queue(retv);
                    state = 0;
                },
                11 => {
                    if run_with_nat {
                        nat_packet.0 = retv;
                    }
                    state += 1;
                },
                12 => {
                    if run_with_nat {
                        nat_packet.1 = retv;
                        state = 0;
                    } else {
                        return retv
                    }
                },
                _ => panic!(),
            }
        }

        if idle_count >= computers.len() * 2 {
            computers[0].add_to_queue(nat_packet.0);
            computers[0].add_to_queue(nat_packet.1);
            idle_count = 0;
            if nat_packet.1 == last_y_to_0 {
                return last_y_to_0;
            }
            last_y_to_0 = nat_packet.1;
        }
    }
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let y = run_nic(&program, 50, false);

    println!("Y is {}", y);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let y = run_nic(&program, 50, true);

    println!("Y is {}", y);
}

fn parse_input(lines: &[String]) -> Program {
    Program::parse_intcode(lines)
}

// #[cfg(test)]
// mod tests19_23 {
//     use super::*;

//     #[test]
//     fn tests19_23_t0() {}
// }


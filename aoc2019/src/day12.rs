use std::cmp::Ordering;
use itertools::Itertools;
use prime_factorization::Factorization;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ThreeDimensions = [isize; 3];
type DimensionValues = [ThreeDimensions; 4];

fn calc_velocity(moons: &DimensionValues, velocity: &mut DimensionValues) {
    for i in 0 .. moons.len() - 1 {
        for j in i + 1 .. moons.len() {
            for d in 0 .. moons[i].len() {
                let velo = match moons[i][d].cmp(&moons[j][d]) {
                    Ordering::Less => (1, -1),
                    Ordering::Greater => (-1, 1),
                    Ordering::Equal => (0, 0),
                };
                velocity[i][d] += velo.0;
                velocity[j][d] += velo.1;
            }
        }
    }
}

fn move_moons(moons: &mut DimensionValues, velocity: &DimensionValues) {
    for i in 0 .. moons.len() {
        for d in 0 .. moons[i].len() {
            moons[i][d] += velocity[i][d];
        }
    }
}

fn calc_energy(moons: &DimensionValues, n_steps: usize) -> isize {
    let mut moons = *moons;
    let mut velocity = [[0; 3]; 4];
    for _ in 0 .. n_steps {
        calc_velocity(&moons, &mut velocity);
        move_moons(&mut moons, &velocity);
    }

    let mut energy = 0;
    for i in 0 .. moons.len() {
        let pot: isize = moons[i].iter().map(|e| e.abs()).sum();
        let kin: isize = velocity[i].iter().map(|e| e.abs()).sum();
        energy += pot * kin;
    }

    energy
}

fn detect_pattern(data: &[isize]) -> Option<(usize, usize)> {
    let data_length = data.len();
    let end_index = data_length / 2;
    // Find all all values in rest of data range that matches the value for test_ind
    let inds = (1 .. end_index)
        .filter(|&i| data[i] == data[0])
        .collect_vec();

    'f: for &segment_len in inds.iter() {
        if segment_len <= 1 {
            // At least two equal numbers in row found, next index
            continue;
        }

        let range = 0 .. segment_len;
        for i in range {
            if data[i] != data[i + segment_len] {
                // The segment doesn't repeat, test next index
                continue 'f;
            }
        }

        return Some((0, segment_len));
    }
    None
}

fn detect_patterns(pos_data: &[Vec<isize>], pattern_inds: &mut Vec<(usize, usize)>) {
    for (i, p_data) in pos_data.iter().enumerate() {
        if pattern_inds.len() > i {
            continue;
        }
        let Some(inds) = detect_pattern(p_data) else {
            break;
        };
        pattern_inds.push(inds);
    }
}

fn find_equal_state(moons_start: &DimensionValues) -> u64 {
    /* This solution isn't entirely correct but good enough :)
       Some more calculations/checks should probably be done on "aggregate" at the end, but I've ran out of energy...
       For the assigned task all aggregate values are equal, but for test 2 they're not. The bigger are divisible by the smaller with result == 3

       An easier way to solve this is to scan through each moon's positions to find where the first position is repeated. Then calculate lcm for these indices.
    */
    let mut moons = *moons_start;
    let velocity_start = [[0; 3]; 4];
    let mut velocity = velocity_start;
    let mut data_collected = Vec::new();
    for _ in 0 .. moons.len() * moons[0].len() {
        data_collected.push(Vec::new());
    }

    let mut count = 0;
    let mut patterns = Vec::new();
    loop {
        calc_velocity(&moons, &mut velocity);
        move_moons(&mut moons, &velocity);
        let mut ind = 0;
        for i in 0 .. moons.len() {
            for j in 0 .. moons[0].len() {
                data_collected[ind].push(moons[i][j]);
                ind += 1;
            }
        }

        if data_collected[0].len() % 100000 == 0 {
            detect_patterns(&data_collected, &mut patterns);
            if patterns.len() == 12 {
                break;
            }
        }
    };

    for i in 0 .. 4 {
        let mut aggregat = 1;
        for j in 0 .. 3 {
            let Factorization {num: _, is_prime: _, factors: mut factors_aggr } = Factorization::run(aggregat);
            let Factorization {num: _, is_prime: _, factors: mut factors_patt } = Factorization::run(patterns[i*3 + j].1 as u64);
            let mut fi = 0;
            while fi < factors_patt.len() {
                let Some(ind) = factors_aggr.iter().position(|e| *e == factors_patt[fi]) else {
                    fi += 1;
                    continue;
                };
                factors_aggr.remove(ind);
                factors_patt.remove(fi);
            }
            if !factors_patt.is_empty() {
                aggregat *= factors_patt.iter().product::<u64>();
            }
        }

        count = count.max(aggregat);
    }

    count
}

fn task_a(lines: &[String]) {
    let moons = parse_input(lines);
    let energy = calc_energy(&moons, 1000);

    println!("Total energy = {}", energy);
}

fn task_b(lines: &[String]) {
    let moons = parse_input(lines);
    let n_steps = find_equal_state(&moons);

    println!("# of steps = {}", n_steps);
}

fn parse_input(lines: &[String]) -> DimensionValues {
    let mut dim_vals: DimensionValues = [[0; 3]; 4];

    for (unit, line) in lines.iter().enumerate() {
        let splitted: Vec<&str> = line.split(&['=', ',', '>']).collect();
        for (dim, spl_ind) in [1, 3, 5].iter().enumerate() {
            dim_vals[unit][dim] = splitted[*spl_ind].to_string().parse().unwrap();
        }
    }

    dim_vals
}

#[cfg(test)]
mod tests19_12 {
    use super::*;

    fn get_test_input_1() -> Vec<String> {
        "<x=-1, y=0, z=2>|<x=2, y=-10, z=-7>|<x=4, y=-8, z=8>|<x=3, y=5, z=-1>".split('|').map(|s| s.to_string()).collect::<Vec<String>>()
    }

    fn get_test_input_2() -> Vec<String> {
        "<x=-8, y=-10, z=0>|<x=5, y=5, z=10>|<x=2, y=-7, z=3>|<x=9, y=-8, z=-3>".split('|').map(|s| s.to_string()).collect::<Vec<String>>()
    }

    #[test]
    fn tests19_12_t0() {
        let lines = get_test_input_1();
        let moons = parse_input(&lines);
        let energy = calc_energy(&moons, 10);

        println!("Total energy = {}", energy);
    }

    #[test]
    fn tests19_12_t1() {
        let lines = get_test_input_1();
        let moons = parse_input(&lines);
        let n_steps = find_equal_state(&moons);

        println!("# of steps = {}", n_steps);
    }

    #[test]
    fn tests19_12_t2() {
        let lines = get_test_input_2();
        let moons = parse_input(&lines);
        let n_steps = find_equal_state(&moons);

        println!("# of steps = {}", n_steps);
    }
}


mod day01; mod day02; mod day03; mod day04; mod day05; mod day06; mod day07; mod day08; mod day09; mod day10; mod day11; mod day12; mod day13; mod day14; mod day15; mod day16; mod day17; mod day18; mod day19; mod day20; mod day21; mod day22; mod day23; mod day24; mod day25;
// mod dayNN_external_module_located_in_repo;
mod intcode;

fn main() {
    let (day, task, sample) = common::parse_command_line(std::env::args().collect());
    let (task, sample) = (&task, sample.as_ref());

    match String::as_str(&day) {
         "1" => day01::run_task(task, sample),
//         "1xx" =>  dayNN_external_module_located_in_repo::run_task(task, sample),
         "2" => day02::run_task(task, sample),
         "3" => day03::run_task(task, sample),
         "4" => day04::run_task(task, sample),
         "5" => day05::run_task(task, sample),
         "6" => day06::run_task(task, sample),
         "7" => day07::run_task(task, sample),
         "8" => day08::run_task(task, sample),
         "9" => day09::run_task(task, sample),
        "10" => day10::run_task(task, sample),
        "11" => day11::run_task(task, sample),
        "12" => day12::run_task(task, sample),
        "13" => day13::run_task(task, sample),
        "14" => day14::run_task(task, sample),
        "15" => day15::run_task(task, sample),
        "16" => day16::run_task(task, sample),
        "17" => day17::run_task(task, sample),
        "18" => day18::run_task(task, sample),
        "19" => day19::run_task(task, sample),
        "20" => day20::run_task(task, sample),
        "21" => day21::run_task(task, sample),
        "22" => day22::run_task(task, sample),
        "23" => day23::run_task(task, sample),
        "24" => day24::run_task(task, sample),
        "25" => day25::run_task(task, sample),
        _  => panic!("Day {} not implemented", day),
    }
}

use std::collections::HashMap;
use std::ops::{Add, Mul};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn run_program(program: &HashMap<usize, u32>) -> u32 {
    let mut program = program.clone();
    let mut instr_ptr = 0;

    while program[&instr_ptr] != 99 {
        let oper = match program[&instr_ptr] {
            1 => Add::add,
            2 => Mul::mul,
            e => panic!("Unknown instruction: {e}"),
        };

        let r1 = program[&(instr_ptr + 1)] as usize;
        let r2 = program[&(instr_ptr + 2)] as usize;
        let result = oper(program[&r1], program[&r2]);
        let store = program[&(instr_ptr + 3)] as usize;
        program.entry(store).and_modify(|e| *e = result);

        instr_ptr += 4;
    }

    *program.get(&0).unwrap()
}

fn task_a(lines: &[String]) {
    let mut program = parse_input(lines);
    program.entry(1).and_modify(|e| *e = 12);
    program.entry(2).and_modify(|e| *e = 2);

    let reg_0 = run_program(&program);

    println!("Register 0 is: {}", reg_0);
}

fn task_b(lines: &[String]) {
    let mut program = parse_input(lines);

    'o:
    for noun in 0 ..= 99 {
        for verb in 0 ..= 99 {
            program.entry(1).and_modify(|e| *e = noun);
            program.entry(2).and_modify(|e| *e = verb);

            let reg_0 = run_program(&program);
            if reg_0 == 19690720 {
                println!("noun: {}, verb: {}", noun, verb);
                println!("result: {}", 100 * noun + verb);
                break 'o;
            }
        }
    }
}

fn parse_input(lines: &[String]) -> HashMap<usize, u32> {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect()
}

use std::collections::{HashMap, HashSet};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Grid = Vec<Vec<char>>;

fn find_matching_layouts(grid: &Grid) -> usize {
    let mut grids = HashSet::new();
    let mut grid = grid.to_owned();
    grids.insert(grid.clone());
    loop {
        grid =
            grid
            .iter()
            .enumerate()
            .map(|(r, row)|
                row
                .iter()
                .enumerate()
                .map(|(c, e)| {
                    let mut adjacents = Vec::new();
                    if r > 0 { adjacents.push((r - 1, c)); }
                    if c > 0 { adjacents.push((r, c - 1)); }
                    if c < grid[0].len() - 1 { adjacents.push((r, c + 1)); }
                    if r < grid.len() - 1 { adjacents.push((r + 1, c)); }
                    let mut count_adjacent = 0;
                    for (rr, cc) in adjacents.iter() {
                        if grid[*rr][*cc] == '#' {
                            count_adjacent += 1;
                            if count_adjacent > 2 {
                                break;
                            }
                        }
                    }
                    if *e == '.' && (count_adjacent == 1 || count_adjacent == 2) {
                        '#'
                    } else if *e == '#' && count_adjacent != 1 {
                        '.'
                    } else {
                        *e
                    }
                })
                .collect::<Vec<_>>()
            )
            .collect::<Vec<_>>();

        if grids.contains(&grid) {
            return
                grid
                .iter()
                .enumerate()
                .fold(0, |biodiv_rating, (r, row)| {
                    row
                    .iter()
                    .enumerate()
                    .fold(biodiv_rating, |biodiv_rating, (c, chr)|
                        if *chr == '#' {
                            biodiv_rating + 2_usize.pow(r as u32 * 5 + c as u32)
                        } else {
                            biodiv_rating
                        }
                    )
                })
        }
        grids.insert(grid.clone());
    }
}

fn run_multi_level(grid: &Grid) -> usize {
    let empty_grid = (0 .. 5)
        .map(|_| vec!['.'; 5])
        .collect::<Vec<_>>();
    let mut grids = HashMap::new();
    grids.insert(0, grid.to_owned());
    let mut min_max = (0, 0);
    let mut new_min_max = min_max;

    for _i in 0 .. 200 {
        let mut new_grids= grids.clone();
        for lev in min_max.0 ..= min_max.1 {
            let grid_above = if let Some(g) = grids.get(&(lev - 1)) { g.clone() } else { empty_grid.clone() };
            let grid_below = if let Some(g) = grids.get(&(lev + 1)) { g.clone() } else { empty_grid.clone() };
            new_grids.insert(lev,
                grids[&lev]
                .iter()
                .enumerate()
                .map(|(r, row)|
                    row
                    .iter()
                    .enumerate()
                    .map(|(c, e)| {
                        let mut count_adjacent = 0;
                        if !(r == 2 && c == 2) {
                            // up
                            if r == 0 { if grid_above[1][2] == '#' { count_adjacent += 1; } }
                            else if !(r == 3 && c == 2) { if grids[&lev][r - 1][c] == '#' { count_adjacent += 1; } }
                            else { count_adjacent += grid_below[4].iter().filter(|ch| **ch == '#').count(); }
                            // right
                            if c == 4 { if grid_above[2][3] == '#' { count_adjacent += 1; } }
                            else if !(r == 2 && c == 1) {if grids[&lev][r][c + 1] == '#' { count_adjacent += 1; } }
                            else { count_adjacent += grid_below.iter().filter(|rr| rr[0] == '#').count(); }
                            // down
                            if r == 4 { if grid_above[3][2] == '#' { count_adjacent += 1; } }
                            else if !(r == 1 && c == 2) { if grids[&lev][r + 1][c] == '#' { count_adjacent += 1; } }
                            else { count_adjacent += grid_below[0].iter().filter(|ch| **ch == '#').count(); }
                            // left
                            if c == 0 { if grid_above[2][1] == '#' { count_adjacent += 1; } }
                            else if !(r == 2 && c == 3) { if grids[&lev][r][c - 1] == '#' { count_adjacent += 1; } }
                            else { count_adjacent += grid_below.iter().filter(|rr| rr[4] == '#').count(); }
                        }
                        match *e {
                            '.' if (count_adjacent == 1 || count_adjacent == 2) => '#',
                            '#' if count_adjacent != 1 => '.',
                            _ => *e,
                        }
                    })
                    .collect::<Vec<_>>()
                )
                .collect::<Vec<_>>()
            );
        }

        let mut grid_above = empty_grid.clone();
        let mut is_infested = false;
        let mut check_adjacent = |count_adjacent: usize, r: usize, c:usize| {
            if count_adjacent == 1 || count_adjacent == 2 {
                grid_above[r][c] = '#';
                is_infested = true;
            }
        };
        // up
        check_adjacent(grids[&min_max.0][4].iter().filter(|ch| **ch == '#').count(), 3, 2);
        // right
        check_adjacent(grids[&min_max.0].iter().filter(|rr| rr[0] == '#').count(), 2, 1);
        // down
        check_adjacent(grids[&min_max.0][0].iter().filter(|ch| **ch == '#').count(), 1, 2);
        // left
        check_adjacent(grids[&min_max.0].iter().filter(|rr| rr[4] == '#').count(), 2, 3);
        if is_infested {
            new_min_max.0 -= 1;
            new_grids.insert(new_min_max.0, grid_above);
        }

        let mut grid_below = empty_grid.clone();
        is_infested = false;
        // up
        if grids[&min_max.1][1][2] == '#' {
            for ch in grid_below[0].iter_mut() {
                *ch = '#';
            }
            is_infested = true;
        }
        // right
        if grids[&min_max.1][2][3] == '#' {
            for rr in grid_below.iter_mut() {
                rr[4] = '#';
            }
            is_infested = true;
        }
        // down
        if grids[&min_max.1][3][2] == '#' {
            for ch in grid_below[4].iter_mut() {
                *ch = '#';
            }
            is_infested = true;
        }
        // left
        if grids[&min_max.1][2][1] == '#' {
            for rr in grid_below.iter_mut() {
                rr[0] = '#';
            }
            is_infested = true;
        }
        if is_infested {
            new_min_max.1 += 1;
            new_grids.insert(new_min_max.1, grid_below);
        }

        grids = new_grids;
        min_max = new_min_max;
    }

    grids
        .iter()
        .fold(0, |tot, (_, grid)|
            grid
                .iter()
                .fold(tot, |gtot, row|
                    row
                        .iter()
                        .fold(gtot, |rtot, ch|
                            rtot + if *ch == '#' {
                                1
                            } else {
                                0
                            }
                        )
                )
        )
}

fn task_a(lines: &[String]) {
    let grid = parse_input(lines);
    let bio_div = find_matching_layouts(&grid);

    println!("Biodiversity rating is {}", bio_div);
}

fn task_b(lines: &[String]) {
    let grid = parse_input(lines);
    let n_bugs = run_multi_level(&grid);

    println!("# of bugs: {}", n_bugs);
}

fn parse_input(lines: &[String]) -> Grid {
    lines
        .iter()
        .map(|line|
            line
            .chars()
            .collect::<Vec<_>>()
        )
    .collect::<Vec<_>>()
}

/* #[cfg(test)]
mod tests19_24 {
    use super::*;

    #[test]
    fn tests19_24_t0() {}
}

 */
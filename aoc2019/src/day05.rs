use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ProgValue = i32;

fn run_program(program: &HashMap<usize, ProgValue>, inp_itr: &mut std::slice::Iter<'_, i32>) {
    let mut program = program.clone();
    let mut instr_ptr = 0;

    let get_value = |instr_ptr: &mut usize, program: &HashMap<usize, ProgValue>, mode| {
        *instr_ptr += 1;
        match mode {
            0 => program[ &(program[instr_ptr] as usize) ],
            1 => program[instr_ptr],
            m => panic!("Illegal mode: {m}"),
        }
    };

    let get_three_params = |instr_ptr: &mut usize, program: &HashMap<usize, ProgValue>, m1, m2| {
        (
            get_value(instr_ptr, program, m1),
            get_value(instr_ptr, program, m2),
            get_value(instr_ptr, program, 1) as usize,
        )
    };

    while program[&instr_ptr] != 99 {
        let elems = match program[&instr_ptr] {
            op @ 1 ..= 98 => {
                (op, 0, 0)
            },
            param => {
                let oper = param % 100;
                let rest = param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;
                // let rest = rest / 10;
                // let mode3 = rest % 10;

                (oper, mode1, mode2)
            }
        };

        match elems.0 {
            1 => {
                let (v1, v2, reg) = get_three_params(&mut instr_ptr, &program, elems.1, elems.2);
                program.entry(reg).and_modify(|e| *e = v1 + v2);
            },
            2 => {
                let (v1, v2, reg) = get_three_params(&mut instr_ptr, &program, elems.1, elems.2);
                program.entry(reg).and_modify(|e| *e = v1 * v2);
            },
            3 => {
                let reg = get_value(&mut instr_ptr, &program, 1) as usize;
                program.entry(reg).and_modify(|e| *e = *inp_itr.next().unwrap());
            },
            4 => {
                println!("{}", get_value(&mut instr_ptr, &program, elems.1));
            },
            op @ (5 | 6) => {
                let v1 = get_value(&mut instr_ptr, &program, elems.1);
                let v2 = get_value(&mut instr_ptr, &program, elems.2);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    instr_ptr = v2 as usize;
                    continue;
                }
            },
            op @ (7 | 8) => {
                let (v1, v2, reg) = get_three_params(&mut instr_ptr, &program, elems.1, elems.2);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) { 1 } else { 0 };
                program.entry(reg).and_modify(|e| *e = v);
            },
            u => panic!("Unsupported oper: {}", u)
        }

        instr_ptr += 1;
    }
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);

    let mut inp_itr = [1].iter();
    run_program(&program, &mut inp_itr);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);

    let mut inp_itr = [5].iter();
    run_program(&program, &mut inp_itr);
}

fn parse_input(lines: &[String]) -> HashMap<usize, ProgValue> {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect()
}

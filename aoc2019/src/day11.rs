use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type ProgValue = i64;
type Program = HashMap<usize, ProgValue>;
type Position = (isize, isize);
type Grid = HashMap<Position, (Color, u8)>;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Color {
    Black,
    White,
}

#[derive(Debug, PartialEq, Eq)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    fn turn_left(&mut self) {
        *self = match *self {
            Self::Up    => Self::Left,
            Self::Right => Self::Up,
            Self::Down  => Self::Right,
            Self::Left  => Self::Down,
        }
    }

    fn turn_right(&mut self) {
        *self = match *self {
            Self::Up    => Self::Right,
            Self::Right => Self::Down,
            Self::Down  => Self::Left,
            Self::Left  => Self::Up,
        }
    }
}

#[derive(Debug)]
struct Robot {
    direction: Direction,
    position: Position,
}

impl Robot {
    fn mv(&mut self) {
        match self.direction {
            Direction::Up    => self.position.1 -= 1,
            Direction::Right => self.position.0 += 1,
            Direction::Down  => self.position.1 += 1,
            Direction::Left  => self.position.0 -= 1,
        }
    }
}

fn run_program(program: &Program, inp: &[ProgValue], run_to_exit: bool, instr_ptr: &mut usize, relative_base: &mut i64) -> (bool, ProgValue, Program) {
    let mut program = program.clone();
    let mut prog_val = ProgValue::MIN;
    let mut inp_iter = inp.iter();

    let get_value = |instr_ptr: &mut usize, program: &Program, mode, relative_base: &mut i64| {
        *instr_ptr += 1;
        let value_ptr =
            match mode {
                0 => program[instr_ptr] as usize,
                1 => *instr_ptr,
                2 => (program.get(instr_ptr).unwrap() + *relative_base) as usize,
                m => panic!("Illegal mode: {m}"),
        };

        match program.get(&value_ptr) {
            Some(value) => *value,
            None => 0,
        }
    };

    let get_three_params = |instr_ptr: &mut usize, program: &Program, elems: (ProgValue, ProgValue, ProgValue, ProgValue), relative_base: &mut i64| {
        (
            get_value(instr_ptr, program, elems.1, relative_base),
            get_value(instr_ptr, program, elems.2, relative_base),
            {
                let mut third = get_value(instr_ptr, program, 1, relative_base);
                if elems.3 == 2 {
                    third += *relative_base;
                }
                third as usize
            },
        )
    };

    let mut did_exit = true;
    while program[instr_ptr] != 99 {
        let elems = match program.get(instr_ptr).unwrap() {
            op @ 1 ..= 98 => {
                (*op, 0, 0, 0)
            },
            param => {
                let oper = *param % 100;
                let rest = *param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;
                let mode3 = if *param < 10000 { 1 } else { rest / 10 };

                (oper, mode1, mode2, mode3)
            }
        };

        let mut mod_prog_val = None;
        match elems.0 {
            1 => {  // Add
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                mod_prog_val = Some((reg, v1 + v2));
            },
            2 => {  // Multiply
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                mod_prog_val = Some((reg, v1 * v2));
            },
            3 => {  // Get input
                let mut reg = get_value(instr_ptr, &program, 1, relative_base) as usize;
                if elems.1 == 2 {
                    reg += *relative_base as usize;
                }
                mod_prog_val = Some((reg, *inp_iter.next().unwrap()));
            },
            4 => {  // Output
                prog_val = get_value(instr_ptr, &program, elems.1, relative_base);
                if !run_to_exit {
                    did_exit = false;
                    *instr_ptr += 1;
                    break;
                }
            },
            op @ (5 | 6) => {   // Jump if true | false
                let v1 = get_value(instr_ptr, &program, elems.1, relative_base);
                let v2 = get_value(instr_ptr, &program, elems.2, relative_base);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    *instr_ptr = v2 as usize;
                    continue;
                }
            },
            op @ (7 | 8) => {   // If is less than | equal
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, relative_base);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) { 1 } else { 0 };
                mod_prog_val = Some((reg, v));
            },
            9 => {
                *relative_base += get_value(instr_ptr, &program, elems.1, relative_base);
            },

            u => panic!("Unsupported oper: {}", u)
        }

        if let Some((reg, v)) = mod_prog_val {
            program.entry(reg).and_modify(|e| *e = v).or_insert(v);
        }
        *instr_ptr += 1;
    }

    (did_exit, prog_val, program)
}

fn paint_ship(program: &Program, start_color: Color) -> Grid {
    let mut program = program.clone();
    let mut instr_ptr = 0;
    let mut grid: Grid = HashMap::from([((0_isize, 0_isize), (start_color, 0))]);
    let mut robot = Robot { direction: Direction::Up, position: (0, 0) };
    let mut relative_base = 0;

    loop {
        let curr_color_val =
            if let Some(panel) = grid.get(&robot.position) {
                if panel.0 == Color::Black { 0 } else { 1 }
            } else {
                0
            }
        ;
        let ret_values = run_program(&program, &[curr_color_val], false, &mut instr_ptr, &mut relative_base);
        if ret_values.0 {
            break;
        }
        let color = match ret_values.1 {
            0 => Color::Black,
            1 => Color::White,
            _ => panic!("Bad color: {}", ret_values.1)
        };
        let ret_values = run_program(&ret_values.2, &[ret_values.1], false, &mut instr_ptr, &mut relative_base);
        match ret_values.1 {
            0 => robot.direction.turn_left(),
            1 => robot.direction.turn_right(),
            _ => panic!("Bad direction: {}", ret_values.1)
        }

        program = ret_values.2;

        grid.entry(robot.position).and_modify(|(col, count)| {
            *col = color;
            *count += 1;
        }).or_insert((color, 1));
        robot.mv();
    }

    grid
}

fn paint_ship_from_black(program: &Program) -> usize {
    let grid = paint_ship(program, Color::Black);

    grid.len()
}

fn paint_ship_from_white(program: &Program) -> Grid {
    paint_ship(program, Color::White)
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let n_painted_panels = paint_ship_from_black(&program);

    println!("# of painted panels = {}", n_painted_panels);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let grid = paint_ship_from_white(&program);

    let mut x_range = (0, 0);
    let mut y_range = (0, 0);

    for (x, y) in grid.keys() {
        x_range = (x_range.0.min(*x), x_range.1.max(*x));
        y_range = (y_range.0.min(*y), y_range.1.max(*y));
    }

    for y in y_range.0 ..= y_range.1 {
        for x in x_range.0 ..= x_range.1 {
            print!("{}",
                if let Some((color, _)) = grid.get(&(x, y)) {
                    if *color == Color::White { '#' } else { ' ' }
                } else {
                    ' '
                }
            );
        }
        println!();
    }
}

fn parse_input(lines: &[String]) -> Program {
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect()
}

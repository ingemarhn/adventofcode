use std::collections::{HashMap, HashSet, VecDeque};
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::HashMap;

    use itertools::Itertools;
    pub type Position = (usize, usize);

    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    enum Tile {
        NonWalkable,
        Open,
        Portal(Position, i16), // Position shall contain warp position of the other portal
    }

    #[derive(Debug)]
    pub struct Maze {
        maze: Vec<Vec<Tile>>,
        pub start_position: Position, // First position after AA
        pub end_portal_position: Position,
    }

    impl Maze {
        pub fn new(rows: usize, columns: usize, open: &Vec<Position>, portals: &HashMap<u16, Vec<Position>>, start: Position, end: Position) -> Self {
            let row = vec![Tile::NonWalkable; columns];

            let mut maze = Self {
                maze: vec![row.clone(); rows],
                start_position: start,
                end_portal_position: end,
            };

            for o in open {
                maze.maze[o.0][o.1] = Tile::Open;
            }

            let inner_x = 2 .. columns - 3;
            let inner_y = 2 .. rows - 3;

            for portals_pos in portals.values() {
                let warp_pos = [portals_pos[0], portals_pos[1]].iter().map(|&p| {
                    let mut wrppos = p;
                    for (r, c) in [(p.0, p.1 + 1), (p.0 + 1, p.1), (p.0, p.1 - 1), (p.0 - 1, p.1)].iter() {
                        if maze.maze[*r][*c] == Tile::Open {
                            wrppos = (*r, *c);
                            break;
                        }
                    }
                    wrppos
                })
                .collect_vec();

                let leveller = if inner_y.contains(&portals_pos[0].0) && inner_x.contains(&portals_pos[0].1) {
                    1
                } else {
                    -1
                };

                maze.maze[portals_pos[0].0][portals_pos[0].1] = Tile::Portal(warp_pos[1], leveller);
                maze.maze[portals_pos[1].0][portals_pos[1].1] = Tile::Portal(warp_pos[0], -leveller);
            }
            maze
        }

        pub fn possible_neighbors(&self, pos: Position) -> Vec<(Position, i16)> {
            let mut ret = Vec::new();

            for (r, c) in [(pos.0, pos.1 + 1), (pos.0 + 1, pos.1), (pos.0, pos.1 - 1), (pos.0 - 1, pos.1)].iter() {
                match self.maze[*r][*c] {
                    Tile::Open => ret.push(((*r, *c), 0)),
                    Tile::Portal(warp_pos, leveller) => ret.push((warp_pos, leveller)),
                    Tile::NonWalkable => if (*r, *c) == self.end_portal_position {
                                                            ret.push(((*r, *c), 0))
                                                        },
                }
            }

            ret
        }
    }
}
use internal::*;

// BFS
fn find_shortest_path(maze: &Maze, use_levels:bool) -> usize {
    let mut min_len = usize::MAX;
    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();
    visited.insert((maze.start_position, 0));
    queue.push_back((maze.start_position, 0, visited, 0));

    let check_steps: Box<dyn Fn(_, _) -> bool> = if use_levels {
        Box::new(|pos, level| pos == maze.end_portal_position && level == 0)
    } else {
        Box::new(|pos, _| pos == maze.end_portal_position)
    };
    let mod_level: Box<dyn Fn(_) -> i16> = if use_levels {
        Box::new(|lvlr| lvlr)
    } else {
        Box::new(|_| 0)
    };

    while let Some((curr_pos, steps, mut visited, level)) = queue.pop_front() {
        if level < 0 {
            continue;
        }

        for (pos, leveller) in maze.possible_neighbors(curr_pos) {
            let level = level + mod_level(leveller);
            if visited.contains(&(pos, level)) {
                continue;
            }
            if steps >= min_len {
                break;
            }

            if check_steps(pos, level) {
                if steps < min_len {
                    min_len = steps;
                }
                continue;
            }
            visited.insert((pos, level));
            queue.push_back((pos, steps + 1, visited.clone(), level));
        }
    }

    min_len
}

fn task_a(lines: &[String]) {
    let maze = parse_input(lines);

    let n_steps = find_shortest_path(&maze, false);

    println!("Shortest path: {}", n_steps);
}

fn task_b(lines: &[String]) {
    let maze = parse_input(lines);

    let n_steps = find_shortest_path(&maze, true);

    println!("Shortest path: {}", n_steps);
}

fn parse_input(lines: &[String]) -> Maze {
    let mut portals = HashMap::new();
    let mut walkable = Vec::new();

    let get_portal_id = |ch1, ch2| ch1 as u16 * 100 + ch2 as u16;

    let maze_input = lines
        .iter()
        .map(|line| line.chars().collect_vec())
        .collect_vec();

    for row in 0 .. maze_input.len() {
        for col in 0 .. maze_input[0].len() {
            let ch = maze_input[row][col];
            if ch == '.' {
                walkable.push((row, col));
                for d in ['r', 'd', 'l', 'u'].iter() {
                    let coord = match *d {
                        'r' => (row, col + 1, row, col + 2),
                        'd' => (row + 1, col, row + 2, col),
                        'l' => (row, col.saturating_sub(1), row, col.saturating_sub(2)),
                        _   => (row.saturating_sub(1), col, row.saturating_sub(2), col),
                    };
                    let c1 = maze_input[coord.0][coord.1];
                    if !c1.is_ascii_uppercase() {
                        continue;
                    }
                    let c2 = maze_input[coord.2][coord.3];
                    let (ch1, ch2, ) = if *d == 'r' || *d == 'd' {
                        (c1, c2)
                    } else {
                        (c2, c1)
                    };
                    let portal_pos = (coord.0, coord.1);
                    let id = get_portal_id(ch1, ch2);
                    portals.entry(id).and_modify(|v: &mut Vec<Position>| v.push(portal_pos)).or_insert(vec![portal_pos]);

                    break;
                }
            }
        }
    }

    let aa = get_portal_id('A', 'A');
    let zz = get_portal_id('Z', 'Z');
    let mut start_pos = (0,0);
    let mut end_pos = (0,0);
    for portal in portals.iter() {
        let pos = portal.1[0];
        if *portal.0 == aa {
            start_pos = if walkable.contains(&(pos.0 + 1, pos.1)) {
                (pos.0 + 1, pos.1)
            } else {
                (pos.0 - 1, pos.1)
            };
        } else if *portal.0 == zz {
            end_pos = pos;
        }
    }
    portals.remove(&aa);
    portals.remove(&zz);

    Maze::new(lines.len(), lines[0].len(), &walkable, &portals, start_pos, end_pos)
}

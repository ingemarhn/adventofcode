pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::HashMap;

    pub type ProgValue = isize;
    pub type Program = Vec<ProgValue>;
    type Position = (isize, isize);

    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    enum Location {
        Wall,
        Visited,
    }

    pub struct Area {
        droid: Position,
        next_pos: Position,
        area: HashMap<Position, Location>,
    }

    impl Area {
        pub fn new() -> Self {
            Self {
                droid: (0, 0),
                next_pos: (0, 0),
                area: HashMap::from([((0, 0), Location::Visited)]),
            }
        }

        pub fn get_droid_loc(&self) -> Position {
            self.droid
        }

        pub fn set_droid_loc(&mut self, pos: Position) {
            self.droid = pos;
        }

        pub fn set_location(&mut self, droid_status: ProgValue) {
            let loc = match droid_status {
                0     => Location::Wall,
                1 | 2 => Location::Visited,
                _ => panic!("{:?}", droid_status),
            };

            if loc == Location::Visited {
                self.droid = self.next_pos;
            }
            self.area.entry(self.next_pos).or_insert(loc);
        }

        pub fn set_next_pos(&mut self, position: &Position) {
            self.next_pos = *position;
        }

        pub fn possible_neighbors(&self) -> Vec<(Position, isize)> {
            let mut ret = Vec::new();
            let (x, y) = self.droid;
            for (mvc, &dir) in [(x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y)]
                .iter()
                .enumerate()
            {
                if !self.area.contains_key(&dir) {
                    ret.push((dir, (mvc + 1) as isize));
                }
            }

            ret
        }

        pub fn get_visited_neighbors(&self, pos: Position) -> Vec<Position> {
            let mut ret = Vec::new();
            let (x, y) = pos;
            for &dir in [(x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y)].iter() {
                if let Some(loc) = self.area.get(&dir) {
                    if *loc != Location::Wall {
                        ret.push(dir);
                    }
                }
            }

            ret
        }

        #[allow(dead_code)]
        pub fn print(&self) {
            let (min_x, min_y, max_x, max_y) =
                self.area
                    .iter()
                    .fold((0, 0, 0, 0), |(mi_x, mi_y, ma_x, ma_y), (&(x, y), _)| {
                        (mi_x.min(x), mi_y.min(y), ma_x.max(x), ma_y.max(y))
                    });

            let mut display =
                vec![vec![' '; (max_x - min_x) as usize + 1]; (max_y - min_y) as usize + 1];

            for (&(x, y), loc) in self.area.iter() {
                display[(y - min_y) as usize][(x - min_x) as usize] = match *loc {
                    Location::Visited => '.',
                    Location::Wall => '\u{2588}',
                };
            }

            display[(0 - min_y) as usize][(0 - min_x) as usize] = '*';
            display[(self.droid.1 - min_y) as usize][(self.droid.0 - min_x) as usize] = 'D';

            print!("   "); for i in min_x ..= max_x { print!("{}", if i < 0 { '-' } else { ' ' }); }; println!();
            print!("   "); for i in min_x ..= max_x { print!("{}", i.abs() / 10); }; println!();
            print!("   "); for i in min_x ..= max_x { print!("{}", i.abs() % 10); }; println!();

            let mut l = min_y;
            for r in display.iter() {
                print!("{:3}", l); l += 1;
                for c in r.iter() {
                    print!("{}", c);
                }
                println!();
            }
        }
    }
}
use std::collections::HashSet;

use internal::*;

fn set_prog_val(program: &mut Program, index: usize, value: ProgValue) {
    if !program.len() - 1 < index {
        program.extend_from_slice(&[0, (index - program.len() + 1) as isize]);
    }

    program[index] = value;
}

fn run_program<F>( program: &Program, get_input: &mut F, run_to_exit: bool, instr_ptr: &mut usize, relative_base: &mut ProgValue, ) -> (bool, ProgValue, Program)
where
    F: FnMut() -> ProgValue,
{
    let mut program = program.clone();
    let mut prog_val = ProgValue::MIN;

    let get_value = |instr_ptr: &mut usize, program: &Program, mode, relative_base: isize| {
        *instr_ptr += 1;
        let value_ptr = match mode {
            0 => program[*instr_ptr] as usize,
            1 => *instr_ptr,
            2 => (program[*instr_ptr] + relative_base) as usize,
            m => panic!("Illegal mode: {m}"),
        };
        if program.len() > value_ptr {
            program[value_ptr]
        } else {
            0
        }
    };

    let get_three_params = |instr_ptr: &mut usize, program: &Program, elems: (ProgValue, ProgValue, ProgValue, ProgValue), relative_base| {
        (
            get_value(instr_ptr, program, elems.1, relative_base),
            get_value(instr_ptr, program, elems.2, relative_base),
            {
                let mut third = get_value(instr_ptr, program, 1, relative_base);
                if elems.3 == 2 {
                    third += relative_base;
                }
                third as usize
            },
        )
    };

    let mut did_exit = true;
    while program[*instr_ptr] != 99 {
        let elems = match program.get(*instr_ptr).unwrap() {
            op @ 1..=98 => (*op, 0, 0, 0),
            param => {
                let oper = *param % 100;
                let rest = *param / 100;
                let mode1 = rest % 10;
                let rest = rest / 10;
                let mode2 = rest % 10;
                let mode3 = if *param < 10000 { 1 } else { rest / 10 };

                (oper, mode1, mode2, mode3)
            }
        };

        let mut mod_prog_val = None;
        match elems.0 {
            1 => {
                // Add
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, *relative_base);
                mod_prog_val = Some((reg, v1 + v2));
            }
            2 => {
                // Multiply
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, *relative_base);
                mod_prog_val = Some((reg, v1 * v2));
            }
            3 => {
                // Get input
                let mut reg = get_value(instr_ptr, &program, 1, *relative_base) as usize;
                if elems.1 == 2 {
                    reg += *relative_base as usize;
                }
                mod_prog_val = Some((reg, get_input()));
            }
            4 => {
                // Output
                prog_val = get_value(instr_ptr, &program, elems.1, *relative_base);
                if !run_to_exit {
                    did_exit = false;
                    *instr_ptr += 1;
                    break;
                }
            }
            op @ (5 | 6) => {
                // Jump if true | false
                let v1 = get_value(instr_ptr, &program, elems.1, *relative_base);
                let v2 = get_value(instr_ptr, &program, elems.2, *relative_base);
                if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                    *instr_ptr = v2 as usize;
                    continue;
                }
            }
            op @ (7 | 8) => {
                // If is less than | equal
                let (v1, v2, reg) = get_three_params(instr_ptr, &program, elems, *relative_base);
                let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) {
                    1
                } else {
                    0
                };
                mod_prog_val = Some((reg, v));
            }
            9 => {
                *relative_base += get_value(instr_ptr, &program, elems.1, *relative_base);
            }

            u => panic!("Unsupported oper: {}", u),
        }

        if let Some((reg, v)) = mod_prog_val {
            set_prog_val(&mut program, reg, v);
        }
        *instr_ptr += 1;
    }

    (did_exit, prog_val, program)
}

fn fill_area(program_start: &Program) -> Area {
    use std::collections::VecDeque;

    let mut instr_ptr = 0;
    let mut relative_base = 0;

    let mut area = Area::new();
    let mut queue = VecDeque::new();
    queue.push_back(((0, 0), program_start.clone())); // This is cheating :)
    area.set_location(1); // Sets start droid position to Visited
    let mut target_pos = (0, 0);

    while let Some((current_pos, program)) = queue.pop_front() {
        area.set_droid_loc(current_pos);
        let neighbors = area.possible_neighbors();
        for pos in &neighbors {
            let mut get_mv_cmd = || pos.1;
            let (_, status, mod_program) = run_program( &program, &mut get_mv_cmd, false, &mut instr_ptr, &mut relative_base, );
            area.set_next_pos(&pos.0);
            area.set_location(status);
            if status == 1 {
                queue.push_back((pos.0, mod_program));
            } else if status == 2 {
                target_pos = area.get_droid_loc();
            }
        }
    }

    area.set_droid_loc(target_pos);
    area
}

fn find_oxygene_system(program: &Program) -> ProgValue {
    let area = fill_area(program);

    let mut queue = Vec::new();
    let mut visited = HashSet::new();
    queue.push(((0, 0), 0));
    visited.insert((0, 0));
    let target_pos = area.get_droid_loc();

    while let Some((pos, path_len)) = queue.pop() {
        if pos == target_pos {
            return path_len;
        }

        let neighbors = area.get_visited_neighbors(pos);
        for p in &neighbors {
            if !visited.contains(p) {
                queue.push((*p, path_len + 1));
                visited.insert(*p);
            }
        }
    }

    0
}

fn fill_oxygene(program: &Program) -> ProgValue {
    let area = fill_area(program);

    let mut queue = Vec::new();
    let mut visited = HashSet::new();
    let mut minutes_count = 0;

    queue.push((area.get_droid_loc(), 0));
    visited.insert(area.get_droid_loc());

    while let Some((pos, count)) = queue.pop() {
        let neighbors = area.get_visited_neighbors(pos);
        let mut dead_end = true;
        for p in &neighbors {
            if !visited.contains(p) {
                queue.push((*p, count + 1));
                visited.insert(*p);
                dead_end = false;
            }
            if dead_end {
                minutes_count = minutes_count.max(count);
            }
        }
    }

    minutes_count
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    let n_moves = find_oxygene_system(&program);

    println!("Fewest # of moves: {}", n_moves);
}

fn task_b(lines: &[String]) {
    let program = parse_input(lines);
    let n_minutes = fill_oxygene(&program);

    println!("# of minutes: {}", n_minutes);
}

fn parse_input(lines: &[String]) -> Program {
    use std::collections::HashMap;

    let prog: HashMap<usize, ProgValue> =
    lines[0]
        .split(',')
        .map(|l| l.parse().unwrap())
        .enumerate()
        .collect();

    let mut program = vec![0; prog.len()];
    for e in prog.iter() {
        set_prog_val(&mut program, *e.0, *e.1);
    }

    program
}

use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Debug)]
enum Direction {
    Up(u16),
    Down(u16),
    Left(u16),
    Right(u16),
}

type Directions = Vec<Direction>;
type AllDirections = [Directions; 2];
type Map = HashMap<(isize, isize), (u8, u32)>;

fn build_paths(all_directions: AllDirections, map: &mut Map) {
    for (i, dir) in all_directions.iter().enumerate() {
        let mut pos = (0, 0);
        let mut n_steps = 0;

        for dir in dir.iter() {
            let (add, n) = match *dir {
                Direction::Up(n) => ((0, -1), n),
                Direction::Down(n) => ((0, 1), n),
                Direction::Left(n) => ((-1, 0), n),
                Direction::Right(n) => ((1, 0), n),
            };

            for _ in 0 .. n {
                pos = (pos.0 + add.0, pos.1 + add.1);
                n_steps += 1;
                let iu = i as u8 + 1;
                map.entry(pos).and_modify(|(e, n)| {*e += iu; *n += n_steps}).or_insert((iu, n_steps));
            }
        }
    }
}

fn get_closest_distance(map: &Map) -> isize {
    let mut closest = isize::MAX;
    for (pos, _) in map.iter().filter(|(_, (e, _))| *e == 3) {
        closest = closest.min(pos.0.abs() + pos.1.abs());
    }

    closest
}

fn get_fewest_steps(map: &Map) -> u32 {
    let mut fewest = u32::MAX;
    for (_, (_, n_steps)) in map.iter().filter(|(_, (e, _))| *e == 3) {
        fewest = fewest.min(*n_steps);
    }

    fewest
}

fn task_a(lines: &[String]) {
    let all_directions = parse_input(lines);
    let mut map = Map::new();
    build_paths(all_directions, &mut map);
    let close = get_closest_distance(&map);

    println!("Distance to closest: {}", close);
}

fn task_b(lines: &[String]) {
    let all_directions = parse_input(lines);
    let mut map = Map::new();
    build_paths(all_directions, &mut map);
    let fewest = get_fewest_steps(&map);

    println!("Fewest steps: {}", fewest);
}

fn parse_input(lines: &[String]) -> AllDirections {
    let parse_line = |line: &String| {
        line
        .split(',')
        .map(|dir| {
            let parts = dir.split_at(1);
            let num: u16 = parts.1.parse().unwrap();
            match parts.0 {
                "U" => Direction::Up(num),
                "D" => Direction::Down(num),
                "L" => Direction::Left(num),
                "R" => Direction::Right(num),
                c => panic!("Bad direction: {c}")
            }
        })
        .collect()
    };

    [
        parse_line(&lines[0]),
        parse_line(&lines[1]),
    ]
}

pub type ProgValue = isize;

#[derive(Clone)]
pub struct Program {
    instr_n_data: Vec<ProgValue>,
    pub instr_ptr: usize,
    relative_base: ProgValue,
}

impl Program {
    fn new(len: usize) -> Self {
        Self {
            instr_n_data: vec![0; len],
            instr_ptr: 0,
            relative_base: 0,
        }
    }

    pub fn parse_intcode(lines: &[String]) -> Program {
        use std::collections::HashMap;

        let prog: HashMap<usize, crate::intcode::ProgValue> =
        lines[0]
            .split(',')
            .map(|l| l.parse().unwrap())
            .enumerate()
            .collect();

        let mut program = crate::intcode::Program::new(prog.len());
        for e in prog.iter() {
            program.set_value(*e.0, *e.1);
        }

        program
    }

    fn set_value(&mut self, index: usize, value: ProgValue) {
        if self.instr_n_data.len() <= index {
            // Lengthen the vector with at least 10 %
            let length = (self.instr_n_data.len() / 10).max(index - (self.instr_n_data.len() - 1));
            self.instr_n_data.extend_from_slice(&vec![0; length]);
        }

        self.instr_n_data[index] = value;
    }

    fn get_value (&mut self, mode: ProgValue) -> ProgValue {
        self.instr_ptr += 1;
        let value_ptr = match mode {
            0 => self.instr_n_data[self.instr_ptr] as usize,
            1 => self.instr_ptr,
            2 => (self.instr_n_data[self.instr_ptr] + self.relative_base) as usize,
            m => panic!("Illegal mode: {m}"),
        };
        if self.instr_n_data.len() > value_ptr {
            self.instr_n_data[value_ptr]
        } else {
            0
        }
    }

    fn get_register(&mut self, mode: ProgValue) -> usize {
        let mut reg = self.get_value(1);
        if mode == 2 {
            reg += self.relative_base;
        }
        reg as usize
    }

    fn get_three_params(&mut self, elems: &(ProgValue, ProgValue, ProgValue, ProgValue)) -> (ProgValue, ProgValue, usize) {
        (
            self.get_value(elems.1),
            self.get_value(elems.2),
            self.get_register(elems.3),
        )
    }

    pub fn run<Func>(&mut self, get_input: &mut Func, run_to_exit: bool) -> (bool, ProgValue)
    where
        Func: FnMut() -> ProgValue,
    {
        self.runrun(get_input, run_to_exit, false, 0)
    }

    pub fn run_check_input<Func>(&mut self, get_input: &mut Func, run_to_exit: bool, no_input_indicator: ProgValue) -> (bool, ProgValue)
    where
        Func: FnMut() -> ProgValue,
    {
        self.runrun(get_input, run_to_exit, true, no_input_indicator)
    }

    fn runrun<Func>(&mut self, get_input: &mut Func, run_to_exit: bool, return_on_no_input: bool, no_input_indicator: ProgValue) -> (bool, ProgValue)
    where
        Func: FnMut() -> ProgValue,
    {
        let mut prog_val = ProgValue::MIN;

        let mut did_exit = true;
        loop {
            if self.instr_n_data[self.instr_ptr] == 99 {
                break
            }

            let elems = match self.instr_n_data.get(self.instr_ptr).unwrap() {
                op @ 1..=98 => (*op, 0, 0, 0),
                param => {
                    let oper = *param % 100;
                    let rest = *param / 100;
                    let mode1 = rest % 10;
                    let rest = rest / 10;
                    let mode2 = rest % 10;
                    let mode3 = rest / 10;

                    (oper, mode1, mode2, mode3)
                }
            };

            match elems.0 {
                1 => {
                    // Add
                    let (v1, v2, reg) = self.get_three_params(&elems);
                    self.set_value(reg, v1 + v2);
                }
                2 => {
                    // Multiply
                    let (v1, v2, reg) = self.get_three_params(&elems);
                    self.set_value(reg, v1 * v2);
                }
                3 => {
                    // Get input
                    let reg = self.get_register(elems.1);
                    let input = get_input();
                    self.set_value(reg, input);
                    if return_on_no_input && input == no_input_indicator {
                        did_exit = false;
                        self.instr_ptr += 1;
                        break;
                    }
                }
                4 => {
                    // Output
                    prog_val = self.get_value(elems.1);
                    if !run_to_exit {
                        did_exit = false;
                        self.instr_ptr += 1;
                        break;
                    }
                }
                op @ (5 | 6) => {
                    // Jump if true | false
                    let v1 = self.get_value(elems.1);
                    let v2 = self.get_value(elems.2);
                    if (op == 5 && v1 != 0) || (op == 6 && v1 == 0) {
                        self.instr_ptr = v2 as usize - 1; // instr_ptr will be incremented after the match statement
                    }
                }
                op @ (7 | 8) => {
                    // If is less than | equal
                    let (v1, v2, reg) = self.get_three_params(&elems);
                    let v = if (op == 7 && v1 < v2) || (op == 8 && v1 == v2) {
                        1
                    } else {
                        0
                    };
                    self.set_value(reg, v);
                }
                9 => {
                    self.relative_base += self.get_value(elems.1);
                }

                u => panic!("Unsupported oper: {}", u),
            }

            self.instr_ptr += 1;
        }

        (did_exit, prog_val)
    }
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: &[String]) {
    let program_start = parse_input(lines);
    const GRID_SIZE: usize = 50;

    let mut input_iter = (0 .. GRID_SIZE)
        .flat_map(|y| (0 .. GRID_SIZE).map(move |x| [x, y]) )
        .flatten()
    ;

    let mut get_input = || {
        input_iter.next().unwrap() as isize
    };

    let mut output_counter = 0;
    let mut n_affected = 0;
    loop {
        let mut program = program_start.clone();
        let (_, point_value) = program.run(&mut get_input, true);
        n_affected += point_value;
        output_counter += 1;
        if output_counter == GRID_SIZE.pow(2) {
            break;
        }
    }

    println!("# of points: {}", n_affected);
}

fn task_b(lines: &[String]) {
    let program_start = parse_input(lines);

    let mut y = 500;
    let mut x_min_max;
    let mut x_start = 0;
    loop {
        let mut input_iter = (x_start .. 1000).flat_map(move |x| [x, y]);

        let mut get_input = || {
            input_iter.next().unwrap()
        };

        x_min_max = (999999, 0);
        let mut x = x_start;
        loop {
            let mut program = program_start.clone();
            let (_, point_value) = program.run(&mut get_input, true);
        // println!("point_value = {}", point_value);
            if point_value == 1 {
                x_min_max = (x_min_max.0.min(x), x_min_max.1.max(x));
            }
            if x_min_max.0 != 999999 && point_value == 0 {
                break;
            }
            x += 1;
        }

        if x_min_max.1 - x_min_max.0 > 110 {
            break;
        }

        x_start = x_min_max.0;
        y += 50;
    }

    let mut program;
    let mut x = x_min_max.1 - 99;
    let prod = loop {
        let input = [x, y + 99,
                                 x + 100, y + 1,
                                 x + 101, y + 1
                                ];
        let mut ind = 0;
        let mut get_input = || {
            ind += 1;
            input[ind -1]
        };

        // Check if position at the bottom left point is 1
        program = program_start.clone();
        let (_, point_value) = program.run(&mut get_input, true);
        if point_value == 1 {
            break x * 10000 + y
        }

        // We need to move a line downwards. Find the rightmost point(1) in next row
        for _ in 0 .. 2 {
            program = program_start.clone();
            let (_, point_value) = program.run(&mut get_input, true);
            if point_value == 0 {
                break;
            }
            x += 1;
        }

        y += 1;
    };

    println!("X Y value: {}", prod);
}

fn parse_input(lines: &[String]) -> crate::intcode::Program {
    crate::intcode::Program::parse_intcode(lines)
}

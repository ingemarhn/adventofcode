use crate::intcode::*;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::VecDeque;
    use super::{ProgValue, Program};

    pub struct Computer {
        program: Program,
        queue: VecDeque<ProgValue>,
    }

    impl Computer {
        pub fn new(prog: &Program) -> Self {
            Self {
                program: prog.clone(),
                queue: VecDeque::new(),
            }
        }

        pub fn add_to_queue(&mut self, val: ProgValue) {
            self.queue.push_back(val);
        }

        pub fn run(&mut self) -> (bool, ProgValue) {
            let mut get_input = || {
                let Some(q_val) = self.queue.pop_front() else {
                    panic!("No input in queue!")
                };
                q_val
            };
            self.program.run_check_input(&mut get_input, false, -1)
        }
    }
}
use internal::*;

fn find_password(prog: &Program) {
    let commands = ["south", "west", "north", "take weather machine",
        "west", "south", "east", "take candy cane", "west", "north", "east",
        "south", "south", "take shell", "north", "east", "east", "south", "take hypercube", "south", "south",
        "east"];
    let mut computer = Computer::new(prog);

    for cmd in commands {
        for c in cmd.chars() {
            computer.add_to_queue(c as ProgValue);
        }
        computer.add_to_queue(10);
    }

    loop {
        let (did_exit, retv) = computer.run();
        if did_exit {
            break
        }
        print!("{}", retv as u8 as char);
    }
}

fn task_a(lines: &[String]) {
    let program = parse_input(lines);
    find_password(&program);
}

fn task_b(_lines: &[String]) {
    println!("No real task B on day 25");
}

fn parse_input(lines: &[String]) -> Program {
    Program::parse_intcode(lines)
}

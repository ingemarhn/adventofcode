use std::collections::VecDeque;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn full_reaction(units: &VecDeque<u8>) -> VecDeque<u8> {
    let mut units = units.clone();

    let mut ind = 0;
    loop {
        let (c0_uc, c1_uc) =
            match units[ind] {
                c @ 65 ..= 90 => (c | 32, units[ind + 1]),
                c             => (c, units[ind + 1] | 32),
            };

        if c0_uc == c1_uc && units[ind] != units[ind + 1] {
            units.remove(ind + 1);
            units.remove(ind);
            ind = ind.saturating_sub(1);
        } else {
            ind += 1
        }

        if ind + 2 > units.len() {
            break;
        }
    }

    units
}

fn task_a(lines: Vec<String>) {
    let units = parse_input(&lines);
    let units = full_reaction(&units);

    println!("Remaining units = {}", units.len());
}

fn task_b(lines: Vec<String>) {
    let units = parse_input(&lines);
    let mut shortest = units.len();

    for c in 97 ..= 122 {
        let mut r_units = units.clone();

        let ru_len = r_units.len();
        for i in (0 .. ru_len).rev() {
            if (r_units[i] | 32) == c {
                r_units.remove(i);
            }
        }

        let r_units_len = full_reaction(&r_units).len();
        if r_units_len < shortest {
            shortest = r_units_len;
        }
    }

    println!("Length of shortest polymer = {}", shortest);
}

fn parse_input(lines: &[String]) -> VecDeque<u8> {
    lines[0]
        .chars()
        .map(|l| l as u8)
        .collect()
}

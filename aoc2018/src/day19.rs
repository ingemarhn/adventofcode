use std::str::FromStr;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Reg = u32;

#[allow(non_camel_case_types)]
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum OpCode {
    addr,  // (add register) stores into register C the result of adding register A and register B.
    addi,  // (add immediate) stores into register C the result of adding register A and value B.
    mulr,  // (multiply register) stores into register C the result of multiplying register A and register B.
    muli,  // (multiply immediate) stores into register C the result of multiplying register A and value B.
    banr,  // (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
    bani,  // (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
    borr,  // (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
    bori,  // (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
    setr,  // (set register) copies the contents of register A into register C. (Input B is ignored.)
    seti,  // (set immediate) stores value A into register C. (Input B is ignored.)
    gtir,  // (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    gtri,  // (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    gtrr,  // (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    eqir,  // (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
    eqri,  // (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    eqrr,  // (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
}

impl OpCode {
    fn exec(&self, instr: &Instruction, regs: &Registers) -> Reg {
        let ins = |i| instr.instr_regs[i] as usize;
        match *self {
            Self::addr => regs.0[ins(0)] + regs.0[ins(1)],
            Self::addi => regs.0[ins(0)] + instr.instr_regs[1],
            Self::mulr => regs.0[ins(0)] * regs.0[ins(1)],
            Self::muli => regs.0[ins(0)] * instr.instr_regs[1],
            Self::banr => regs.0[ins(0)] & regs.0[ins(1)],
            Self::bani => regs.0[ins(0)] & instr.instr_regs[1],
            Self::borr => regs.0[ins(0)] | regs.0[ins(1)],
            Self::bori => regs.0[ins(0)] | instr.instr_regs[1],
            Self::setr => regs.0[ins(0)],
            Self::seti => instr.instr_regs[0],
            Self::gtir => if instr.instr_regs[0] > regs.0[ins(1)] { 1 } else { 0 },
            Self::gtri => if regs.0[ins(0)] > instr.instr_regs[1] { 1 } else { 0 },
            Self::gtrr => if regs.0[ins(0)] > regs.0[ins(1)] { 1 } else { 0 },
            Self::eqir => if instr.instr_regs[1] == regs.0[ins(1)] { 1 } else { 0 },
            Self::eqri => if regs.0[ins(0)] == instr.instr_regs[1] { 1 } else { 0 },
            Self::eqrr => if regs.0[ins(0)] == regs.0[ins(1)] { 1 } else { 0 },
        }
    }
}

impl FromStr for OpCode {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let oc = match s {
            "addr" => Self::addr,
            "addi" => Self::addi,
            "mulr" => Self::mulr,
            "muli" => Self::muli,
            "banr" => Self::banr,
            "bani" => Self::bani,
            "borr" => Self::borr,
            "bori" => Self::bori,
            "setr" => Self::setr,
            "seti" => Self::seti,
            "gtir" => Self::gtir,
            "gtri" => Self::gtri,
            "gtrr" => Self::gtrr,
            "eqir" => Self::eqir,
            "eqri" => Self::eqri,
            "eqrr" => Self::eqrr,
            _ => panic!()
        };
        Ok( oc )
    }
}


#[derive(Clone, Copy, Debug)]
struct Registers([Reg; 6]);
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Instruction {
    instr: OpCode,
    instr_regs: [Reg; 3],
}

/*
    program starts with by running instructions 0,17-26,1,2 or 0,17-35,1,2 before entering an instruction cycle
    r4 will get a value here that never changes during program execution
    r2 is set to 1 (instr 1)
    r5 is set to 1 (instr 2)

    program mainly loops instructions 3, 4, 5, 6, 8, 9, 10, 11
    r0
        modified when r1 == r4
            at this point instr 7 is run instead of 6
                instr 8 is always run after 6 or 7
            r0 is updated by adding r2
        r1 is either 0, 1 or r2 * r5
            0 and 1 is set by instructions 4 or 9
            r2 * r5 is set by instr 3
            instr 4 checks if r1 == r4
                that only happens when r4 / r2 is even and r5 is the quotient of that division (e.g. r2=463, r4=926, r5=2)
                task a: r2 and r5 is one of the pairs (1, 926), (2, 463)
                task b: r2 and r5 is one of the pairs (1, 10551326), (2, 5275663)
        r2 always increments 1
            when r5 > r4
                at this point, the instruction cycle is slightly modified for one cycle
                    instead of 11 (in the main loop) instructions 12, 13, 14, 15, 2 are run (then back to 3)
                r2 is incremented by instr 12
                    by this r5 is reset to 1 (instr 2)
        r3 is instr pointer
        r4 never changes
        r5 increments 1 once per main loop (instr 8)
    eventually r2 > r4
        this will change the program instruction exception, 15 will become 16
            by that, r3 will be multiplied by itself and most likely point outside available instructions

    all in all:
        this ends up in adding the factorials of r4
*/

fn run_program(ip_reg: usize, instructions: &[Instruction], reg_0_val: Reg) -> Reg {
    let mut registers = Registers([0; 6]);
    registers.0[0] = reg_0_val;
    while registers.0[ip_reg] != 3 {
        let instr = instructions[registers.0[ip_reg] as usize];
        let c = instr.instr.exec(&instr, &registers);
        registers.0[instr.instr_regs[2] as usize] = c;
        registers.0[ip_reg] += 1;
    }

    // Use factor function ...
    let factors = common::factor(registers.0[4]);

    factors.iter().sum()
}

#[cfg(test)]
fn run_program_naive(ip_reg: usize, instructions: &[Instruction], reg_0_val: Reg) -> Reg {
    let mut registers = Registers([0; 6]);
    registers.0[0] = reg_0_val;
    while (registers.0[ip_reg] as usize) < instructions.len() {
        let instr = instructions[registers.0[ip_reg] as usize];
        let c = instr.instr.exec(&instr, &registers);
        registers.0[instr.instr_regs[2] as usize] = c;
        registers.0[ip_reg] += 1;
        println!("{:?}	{}	{}	{}	->	{c}	=>	{}	{}	{}	{}	{}	{}", instr.instr, instr.instr_regs[0], instr.instr_regs[1], instr.instr_regs[2],
        registers.0[0], registers.0[1], registers.0[2], registers.0[3], registers.0[4], registers.0[5], );
    }

    registers.0[0]
}

fn task_a(lines: Vec<String>) {
    let (ip, instructions) = parse_input(&lines);

    let reg_0 = run_program(ip, &instructions, 0);
    println!("Register 0 = {}", reg_0);
}

fn task_b(lines: Vec<String>) {
    let (ip, instructions) = parse_input(&lines);

    let reg_0 = run_program(ip, &instructions, 1);
    println!("Register 0 = {}", reg_0);
}

fn parse_input(lines: &[String]) -> (usize, Vec<Instruction>) {
    let mut lines_it = lines.iter();
    let ip = lines_it.next().unwrap();
    let ip_reg = ip[4..].parse().unwrap();

    let mut instructions = Vec::new();
    let parse = |s: &str| s.parse::<Reg>().unwrap();

    for line in lines_it {
        let l_parts: Vec<&str> = line.split(' ').collect();
        let instruction = Instruction { instr: l_parts[0].parse::<OpCode>().unwrap(), instr_regs: [parse(l_parts[1]), parse(l_parts[2]), parse(l_parts[3])] };
        instructions.push(instruction);
    }

    (ip_reg, instructions)
}

#[cfg(test)]
mod tests18_19 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;
    use std::env;
    use std::io::BufRead;
    use std::io::BufReader;

    fn get_input_and_parse() -> (usize, Vec<Instruction>) {
        env::set_current_dir("src").unwrap();
        let lines = common::get_input("19".to_string());
        parse_input(&lines)
    }

    #[test]
    fn tests18_19_t01() {
        let (ip, instructions) = get_input_and_parse();
        let mut regs = Registers([0, 0, 0, 0, 0, 0]);

        let mut exec = |i: usize| {
            print!("{:?} {} {} {} -> ", instructions[i].instr, instructions[i].instr_regs[0], instructions[i].instr_regs[1], instructions[i].instr_regs[2]);
            let r = instructions[i].instr.exec(&instructions[i], &regs);
            println!("{r}");
            regs.0[instructions[i].instr_regs[2] as usize] = r;
            println!("regs = {:?}", regs);
        };

        exec(0);
        exec(1);
    }

    #[test]
    fn tests18_19_t02() {
        let (ip, instructions) = get_input_and_parse();

        let reg_0 = run_program_naive(ip, &instructions, 0);
        println!("Register 0 = {}", reg_0);
    }

    #[test]
    fn tests18_19_t03() {
        let (ip, instructions) = get_input_and_parse();

        let reg_0 = run_program_naive(ip, &instructions, 1);
        println!("Register 0 = {}", reg_0);
    }
}

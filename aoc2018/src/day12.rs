use std::collections::{VecDeque, HashSet};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type State = VecDeque<char>;
type Rule = (Vec<char>, char);
type Rules = Vec<Rule>;

fn fmt_st(st: &State, prt_all: bool) -> String {
    let mut r = String::new();
    let (s, e) = match prt_all {
        true => (0, st.len() - 1),
        false => {
            let s = st.iter().position(|&c| c == '#').unwrap();
            let e = st.iter().rposition(|&c| c == '#').unwrap();
            (s, e)
        },
    };
    for c in st.iter().take(e + 1).skip(s) {
        r.push(*c);
    }
    if !prt_all {
        r = r.trim_end_matches('.').to_string()
    }
    r
}

fn run_n_generations(state: State, plant_rules: Rules, n_generations: usize) -> i64 {
    let mut current_state = state;
    let mut prev_state = VecDeque::new();
    for _ in 0 .. 4 {
        current_state.push_front('.');
        current_state.push_back('.');
    }
    let mut pot_0_index = 4;
    let mut right_most = current_state.len() - 3;

    let (pots_with_plants_rules, pots_without_plants_rules): (Vec<_>, Vec<_>) = plant_rules.iter().partition(|(rule, _)| rule[2] == '#');
    let mut states = HashSet::new();
    let mut gen_first_eq = 0;

    for gen in 0 .. n_generations {
        let mut next_state = current_state.clone();

        for st_i in 2 ..= right_most {
            let mut rules_it = match current_state[st_i] {
                '#' => pots_with_plants_rules.iter(),
                _   => pots_without_plants_rules.iter(),
            };

            let rule = loop {
                let Some(&(rul, _)) = rules_it.next() else {
                    break None;
                };

                if [0, 1, 3, 4].iter().all(|&i| rul[i] == current_state[st_i - 2 + i]) {
                    break Some(rul);
                }
            };

            if rule.is_some() {
                next_state[st_i] = '#';
            } else {
                next_state[st_i] = '.';
            }
        }

        current_state = next_state;
        if !states.insert(fmt_st(&current_state, false)) {
            gen_first_eq = gen;
            break;
        }

        let plant_margin = 4_usize.saturating_sub(current_state.iter().position(|pot| *pot == '#').unwrap());
        for _ in 0 .. plant_margin {
            current_state.push_front('.');
            pot_0_index += 1;
            right_most += 1;
        }
        let plant_margin = 4_usize.saturating_sub(current_state.len() - 1 - current_state.iter().rposition(|pot| *pot == '#').unwrap());
        for _ in 0 .. plant_margin {
            current_state.push_back('.');
            right_most += 1;
        }

        prev_state.clone_from(&current_state);
    }

    let pot_sum = |calc_state: &State| {
        calc_state.iter().enumerate().fold(0, |sum, (s_i, pot)|
            if *pot == '#' {
                sum + (s_i - pot_0_index) as i64
            } else {
                sum
            }
        )
    };

    let curr_sum = pot_sum(&current_state);
    if gen_first_eq == 0 {
        curr_sum
    } else {
        let prev_sum = pot_sum(&prev_state);
        let sum_diff = curr_sum - prev_sum;
        let n_gen_diff = n_generations - gen_first_eq - 1;

        curr_sum + sum_diff * n_gen_diff as i64
    }
}

fn task_a(lines: Vec<String>) {
    let (state, rules) = parse_input(&lines);
    let plant_rules = rules
        .into_iter()
        .filter(|(_, result)| *result == '#')
        .collect::<Vec<_>>()
    ;

    let pot_sum = run_n_generations(state, plant_rules, 20);

    println!("Pots sum = {}", pot_sum);
}

fn task_b(lines: Vec<String>) {
    let (state, rules) = parse_input(&lines);
    let plant_rules = rules
        .into_iter()
        .filter(|(_, result)| *result == '#')
        .collect::<Vec<_>>()
    ;

    let pot_sum = run_n_generations(state, plant_rules, 50_000_000_000);

    println!("Pots sum = {}", pot_sum);
}

fn parse_input(lines: &[String]) -> (State, Rules) {
    let mut lines_it = lines.iter();
    let init = lines_it
        .next()
        .unwrap()
        .chars()
        .skip(15)
        .collect::<VecDeque<_>>()
    ;

    let rules = lines_it
        .skip(1)
        .map(|line| {
            let chrs = line.chars();
            let rule = chrs.clone().take(5).collect::<Vec<_>>();
            let result = chrs.last().unwrap();
            (rule, result)
        })
        .collect::<Vec<_>>()
    ;

    (init, rules)
}

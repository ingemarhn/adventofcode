pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let serial: i32 = lines[0].parse().unwrap();

    let calc_cell_value = |x, y| {
        let rack_id = x + 10;
        (((rack_id * y + serial) * rack_id) / 100) % 10 - 5
    };

    let mut fuel_cells = [[0; 300]; 300];

    for (y, fuel_cells_row) in fuel_cells.iter_mut().enumerate().take(2) {
        for (x, fuel_cell) in fuel_cells_row.iter_mut().enumerate() {
            *fuel_cell = calc_cell_value(x as i32 + 1, y as i32 + 1);
        }
    }
    for (y, fuel_cells_row) in fuel_cells.iter_mut().enumerate().skip(2) {
        for (x, fuel_cell) in fuel_cells_row.iter_mut().enumerate().take(2) {
            *fuel_cell = calc_cell_value(x as i32 + 1, y as i32 + 1);
        }
    }

    let mut max_power = ((0, 0), 0);
    for y in 0 .. 298 {
        for x in 0 .. 298 {
            fuel_cells[y + 2][x + 2] = calc_cell_value(x as i32 + 3, y as i32 + 3);
            let fuel_quare_power =
                (0 ..= 2)
                .fold(0, |sum, yp| sum + fuel_cells[y + yp][x ..= x + 2].iter().sum::<i32>())
            ;
            if fuel_quare_power > max_power.1 {
                max_power = ((x + 1, y + 1), fuel_quare_power);
            }
        }
    }

    println!("Most powerful square coordinates: {:?}", max_power);
}

fn task_b(lines: Vec<String>) {
    let serial: i32 = lines[0].parse().unwrap();

    let calc_cell_value = |x, y| {
        let rack_id = x + 10;
        (((rack_id * y + serial) * rack_id) / 100) % 10 - 5
    };

    let mut fuel_cells = [[0; 300]; 300];

    for (y, fuel_cells_row) in fuel_cells.iter_mut().enumerate() {
        for (x, fuel_cell) in fuel_cells_row.iter_mut().enumerate() {
            *fuel_cell = calc_cell_value(x as i32 + 1, y as i32 + 1);
        }
    }

    let mut max_power = ((0, 0, 0), 0);
    for y in 0 .. 298 {
        for x in 0 .. 298 {
            let mut fuel_quare_power =
                (0 ..= 1)
                .fold(0, |sum, xyp| sum + fuel_cells[y + xyp][x ..= x + 1].iter().sum::<i32>())
            ;
            for sq_sz in 3 ..= 298 - y.max(x) {
                fuel_quare_power +=
                    (0 .. sq_sz - 1)
                    .fold(0, |sum, yp| sum + fuel_cells[y + yp][x + sq_sz - 1])
                ;
                fuel_quare_power +=
                    (0 .. sq_sz)
                    .fold(0, |sum, xp| sum + fuel_cells[y + sq_sz - 1][x + xp])
                ;
                if fuel_quare_power > max_power.1 {
                    max_power = ((x + 1, y + 1, sq_sz), fuel_quare_power);
                }
            }
        }
    }

    println!("Most powerful square coordinates: {:?}", max_power);
}

#[cfg(test)]
mod tests11 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;

    #[test]
    fn t() {
        /*

        Fuel cell at  122,79, grid serial number 57: power level -5.
        Fuel cell at 217,196, grid serial number 39: power level  0.
        Fuel cell at 101,153, grid serial number 71: power level  4.

         */
        fn ccv(serial:i32,x:i32,y:i32) {
            let calc_cell_value = |x, y| {
                let rack_id = x + 10;
                (((rack_id * y + serial) * rack_id) / 100) % 10 - 5
            };

            println!("({x},{y}), {serial} -> {}", calc_cell_value(x,y));
        }

        ccv(8,3,5);
        ccv(57,122,79);
        ccv(39,217,196);
        ccv(71,101,153);
        ccv(1723,3,3);
    }
}

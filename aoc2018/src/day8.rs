use std::str::SplitAsciiWhitespace;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn sum_metadata(numbers_it: &mut SplitAsciiWhitespace<'_>) -> u16 {
    let mut n_children: u8 = numbers_it.next().unwrap().parse().unwrap();
    let n_meta = numbers_it.next().unwrap().parse().unwrap();
    let mut meta_sum = 0;
    while n_children > 0 {
        meta_sum += sum_metadata(numbers_it);
        n_children -= 1;
    }

    meta_sum + numbers_it
        .take(n_meta)
        .map(|ms| ms.parse::<u16>() .unwrap() )
        .sum::<u16>()
}

fn get_root_node_val(numbers_it: &mut SplitAsciiWhitespace<'_>) -> u16 {
    let mut n_children: u8 = numbers_it.next().unwrap().parse().unwrap();
    let n_meta = numbers_it.next().unwrap().parse().unwrap();

    let has_children = n_children > 0;
    let mut child_vals = Vec::new();
    while n_children > 0 {
        let childs_val = get_root_node_val(numbers_it);
        child_vals.push(childs_val);
        n_children -= 1;
    }

    let meta_vals =
        numbers_it
        .take(n_meta)
        .map(|ms| ms.parse::<u16>() .unwrap() );

    if has_children {
        meta_vals
            .map(|i| (i - 1) as usize)
            .filter(|m| child_vals.get(*m).is_some() )
            .map(|i| *child_vals.get(i).as_ref().unwrap())
            .sum::<u16>()
    } else {
        meta_vals
            .sum::<u16>()
    }
}

fn task_a(lines: Vec<String>) {
    let mut numbers_it = lines[0].split_ascii_whitespace();
    let sum = sum_metadata(&mut numbers_it);

    println!("Metadata sum = {}", sum);
}

fn task_b(lines: Vec<String>) {
    let mut numbers_it = lines[0].split_ascii_whitespace();
    let sum = get_root_node_val(&mut numbers_it);

    println!("Root node value = {}", sum);
}

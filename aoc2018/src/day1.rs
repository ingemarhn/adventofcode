pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

use std::collections::HashSet;

fn task_a(lines: Vec<String>) {
    let numbers = parse_input(&lines);
    let sum: i32 =
        numbers
        .iter()
        .sum();

    println!("Resulting frequency = {sum}");
}

fn task_b(lines: Vec<String>) {
    let numbers = parse_input(&lines);

    let mut used_freqs = HashSet::<i32>::new();
    let mut num_iter = numbers.iter().cycle();
    let mut sum = 0;
    loop {
        if let Some(num) = num_iter.next() {
            sum += *num;
            if !used_freqs.insert(sum) {
                break
            }
        }
    };

    println!("First twice frequency = {sum}");
}

fn parse_input(lines: &[String]) -> Vec<i32> {
    lines
        .iter()
        .map(|line| line.parse().unwrap())
        .collect::<Vec<_>>()
}

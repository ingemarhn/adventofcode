#![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
use std::str::FromStr;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Reg = u64;

#[allow(non_camel_case_types)]
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum OpCode {
    addr,  // (add register) stores into register C the result of adding register A and register B.
    addi,  // (add immediate) stores into register C the result of adding register A and value B.
    mulr,  // (multiply register) stores into register C the result of multiplying register A and register B.
    muli,  // (multiply immediate) stores into register C the result of multiplying register A and value B.
    banr,  // (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
    bani,  // (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
    borr,  // (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
    bori,  // (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
    setr,  // (set register) copies the contents of register A into register C. (Input B is ignored.)
    seti,  // (set immediate) stores value A into register C. (Input B is ignored.)
    gtir,  // (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    gtri,  // (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    gtrr,  // (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    eqir,  // (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
    eqri,  // (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    eqrr,  // (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
}

impl OpCode {
    fn exec(&self, instr: &Instruction, regs: &Registers) -> Reg {
        let ins = |i| instr.instr_regs[i] as usize;
        match *self {
            Self::addr => regs.0[ins(0)] + regs.0[ins(1)],
            Self::addi => regs.0[ins(0)] + instr.instr_regs[1],
            Self::mulr => regs.0[ins(0)] * regs.0[ins(1)],
            Self::muli => regs.0[ins(0)] * instr.instr_regs[1],
            Self::banr => regs.0[ins(0)] & regs.0[ins(1)],
            Self::bani => regs.0[ins(0)] & instr.instr_regs[1],
            Self::borr => regs.0[ins(0)] | regs.0[ins(1)],
            Self::bori => regs.0[ins(0)] | instr.instr_regs[1],
            Self::setr => regs.0[ins(0)],
            Self::seti => instr.instr_regs[0],
            Self::gtir => if instr.instr_regs[0] > regs.0[ins(1)] { 1 } else { 0 },
            Self::gtri => if regs.0[ins(0)] > instr.instr_regs[1] { 1 } else { 0 },
            Self::gtrr => if regs.0[ins(0)] > regs.0[ins(1)] { 1 } else { 0 },
            Self::eqir => if instr.instr_regs[1] == regs.0[ins(1)] { 1 } else { 0 },
            Self::eqri => if regs.0[ins(0)] == instr.instr_regs[1] { 1 } else { 0 },
            Self::eqrr => if regs.0[ins(0)] == regs.0[ins(1)] { 1 } else { 0 },
        }
    }
}

impl FromStr for OpCode {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let oc = match s {
            "addr" => Self::addr,
            "addi" => Self::addi,
            "mulr" => Self::mulr,
            "muli" => Self::muli,
            "banr" => Self::banr,
            "bani" => Self::bani,
            "borr" => Self::borr,
            "bori" => Self::bori,
            "setr" => Self::setr,
            "seti" => Self::seti,
            "gtir" => Self::gtir,
            "gtri" => Self::gtri,
            "gtrr" => Self::gtrr,
            "eqir" => Self::eqir,
            "eqri" => Self::eqri,
            "eqrr" => Self::eqrr,
            _ => panic!()
        };
        Ok( oc )
    }
}

#[derive(Clone, Copy, Debug)]
struct Registers([Reg; 6]);
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Instruction {
    instr: OpCode,
    instr_regs: [Reg; 3],
}

fn detect_pattern(values: &[Reg]) -> Option<(usize, usize)> {
    let test_value = values.last().unwrap();
    let ind_o = values.iter().take(values.len()-1).rposition(|e| e == test_value);
    let mut ind = ind_o?;

    let segment_len = values.len() - ind - 1;
    if segment_len > ind ||
        &values[ind - segment_len] != test_value
    {
        return None;
    }

    ind -= 1;
    while values[ind] == values[ind + segment_len] {
        ind -= 1;
    }

    Some((ind + 1, segment_len))
}

fn run_program(ip_reg: usize, instructions: &[Instruction], find_0_most_instr: bool) -> Reg {
    let mut registers = Registers([0; 6]);
    let mut instr_eqrr_values = Vec::new();

    let get_reg0_comp_val = |registers: &Registers| registers.0[instructions[registers.0[ip_reg] as usize].instr_regs[0] as usize];
    loop {
        let instr = instructions[registers.0[ip_reg] as usize];
        if instr.instr == OpCode::eqrr {
            if !find_0_most_instr {
                break;
            }

            let v = get_reg0_comp_val(&registers);
            instr_eqrr_values.push(v);
            if instr_eqrr_values.len() % 1000 == 0 {
                let pattern_data = detect_pattern(&instr_eqrr_values);
                if let Some((patt_start, patt_len)) = pattern_data {
                    return instr_eqrr_values[patt_start + patt_len - 1];
                }
            }
        }
        let c = instr.instr.exec(&instr, &registers);
        registers.0[instr.instr_regs[2] as usize] = c;
        registers.0[ip_reg] += 1;
    }

    get_reg0_comp_val(&registers)
}

fn task_a(lines: Vec<String>) {
    let (ip, instructions) = parse_input(&lines);

    let reg_eq_to_0 = run_program(ip, &instructions, false);
    println!("Register 0 = {}", reg_eq_to_0);
}

fn task_b(lines: Vec<String>) {
    let (ip, instructions) = parse_input(&lines);

    let reg_eq_to_0 = run_program(ip, &instructions, true);
    println!("Register 0 = {}", reg_eq_to_0);
}


fn parse_input(lines: &[String]) -> (usize, Vec<Instruction>) {
    let mut lines_it = lines.iter();
    let ip = lines_it.next().unwrap();
    let ip_reg = ip[4..].parse().unwrap();

    let mut instructions = Vec::new();
    let parse = |s: &str| s.parse::<Reg>().unwrap();

    for line in lines_it {
        let l_parts: Vec<&str> = line.split(' ').collect();
        let instruction = Instruction { instr: l_parts[0].parse::<OpCode>().unwrap(), instr_regs: [parse(l_parts[1]), parse(l_parts[2]), parse(l_parts[3])] };
        instructions.push(instruction);
    }

    (ip_reg, instructions)
}

use std::cmp::Ordering;
use std::collections::{HashSet, HashMap};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Clone, Debug, PartialEq)]
enum AttackType {
    Radiation,
    Slashing,
    Bludgeoning,
    Fire,
    Cold,
}

impl AttackType {
    fn parse(att_str: &str) -> Self {
        match att_str {
            "radiation"   => Self::Radiation,
            "slashing"    => Self::Slashing,
            "bludgeoning" => Self::Bludgeoning,
            "fire"        => Self::Fire,
            "cold"        => Self::Cold,
            p       => panic!("Unknown attack type: {p}")
        }
    }
}

#[derive(Clone, Debug)]
struct Group {
    units_count: u16,
    hit_points: u16,
    attack_damage: u16,
    attack_type: AttackType,
    initiative: u16,
    effective_power: u32,
    weaknesses: Option<Vec<AttackType>>,
    immunities: Option<Vec<AttackType>>,
}

impl Group {
    fn is_weak(&self, att: &AttackType) -> bool {
        self.contains_attack_type(&self.weaknesses, att)
    }

    fn is_immune(&self, att: &AttackType) -> bool {
        self.contains_attack_type(&self.immunities, att)
    }

    fn contains_attack_type(&self, o_attacks_arr: &Option<Vec<AttackType>>, att: &AttackType) -> bool {
        let Some(attacks_arr) = o_attacks_arr else {
            return false
        };
        attacks_arr.contains(att)
    }
}

fn select_targets(armies: &[Vec<Group>]) -> HashMap<(usize, usize), (usize, usize)> {
    let mut order = Vec::new();
    for (ia, army) in armies.iter().enumerate() {
        for (ig, group) in army.iter().enumerate() {
            order.push((ia, ig, group.effective_power, group.initiative));
        }
    }
    order.sort_by(|a, b|
        match b.2.cmp(&a.2) {
            Ordering::Equal => b.3.cmp(&a.3),
            ord => ord,
        }
    );

    let mut taken = HashSet::new();
    let mut attacker_targets  = HashMap::new();
    for ord in &order {
        let i_atta = ord.0;
        let i_grp_a = ord.1;
        let i_target = (i_atta + 1) % 2;
        let att_group = &armies[i_atta][i_grp_a];
        let mut max_damage = (0, (0, 0), 0);
        for (i_grp_t, trg_group) in armies[i_target].iter().enumerate() {
            if taken.contains(&(i_target, i_grp_t)) {
                continue;
            }

            let mult = if trg_group.is_weak(&att_group.attack_type) {
                2
            } else if trg_group.is_immune(&att_group.attack_type) {
                0
            } else {
                1
            };
            let damage = ord.2 * mult;
            if damage > max_damage.0 ||
               (damage == max_damage.0 && trg_group.effective_power > max_damage.2)
            {
                max_damage = (damage, (i_target, i_grp_t), trg_group.effective_power)
            }
        }

        if max_damage.0 == 0 {
            continue;
        }

        attacker_targets.insert((ord.0, ord.1), max_damage.1);
        taken.insert(max_damage.1);
    }

    attacker_targets
}

fn attack(armies: &mut [Vec<Group>], attacker_targets: HashMap<(usize, usize), (usize, usize)>) {
    let mut order = Vec::new();
    for (ia, army) in armies.iter().enumerate() {
        for (ig, group) in army.iter().enumerate() {
            order.push((ia, ig, group.initiative));
        }
    }
    order.sort_by(|a, b| b.2.cmp(&a.2) );

    for ord in &order {
        let i_atta = ord.0;
        let i_grp_a = ord.1;
        let att_group = &armies[i_atta][i_grp_a];
        if att_group.units_count == 0 {
            continue;
        }
        if !attacker_targets.contains_key(&(i_atta, i_grp_a)) {
            continue;
        }
        let (i_trga, i_grp_t) = attacker_targets.get(&(i_atta, i_grp_a)).unwrap();
        let trg_group = &armies[*i_trga][*i_grp_t];
        let mult = if trg_group.is_weak(&att_group.attack_type) {
            2
        } else {
            1
        };
        let damage = att_group.effective_power * mult;
        let n_kills = (damage / trg_group.hit_points as u32) as u16;
        armies[*i_trga][*i_grp_t].units_count = armies[*i_trga][*i_grp_t].units_count.saturating_sub(n_kills);
        armies[*i_trga][*i_grp_t].effective_power = armies[*i_trga][*i_grp_t].units_count as u32 * armies[*i_trga][*i_grp_t].attack_damage as u32;
    }

    for army in armies.iter_mut() {
        for ig in (0 .. army.len()).rev() {
            if army[ig].units_count == 0 {
                army.remove(ig);
            }
        }
    }
}

fn run_fights(armies: &mut [Vec<Group>]) -> (u16, usize) {
    while !armies[0].is_empty() && !armies[1].is_empty() {
        let attacker_targets = select_targets(armies);
        attack(armies, attacker_targets);
    }

    let ind = if !armies[0].is_empty() { 0 } else { 1 };

    let units_sum = armies[ind].iter().fold(0, |sum, e| {
        sum + e.units_count
    });

    (units_sum, ind)
}

fn run_fights_with_boost(armies: &mut [Vec<Group>]) -> u16 {
    let mut boosts = (0, 50_000, u16::MAX);
    let mut immune_sys_won;

    loop {
        let mut new_armies = armies.to_vec();
        for group in new_armies[0].iter_mut() {
            group.attack_damage += boosts.1;
            group.effective_power = group.units_count as u32 * group.attack_damage as u32
        }

        let (n_units, army_ind) = run_fights(&mut new_armies);
        immune_sys_won = army_ind == 0;

        boosts =
            if immune_sys_won {
                (boosts.0, boosts.1 - (boosts.1 - boosts.0) / 2, boosts.1)
            } else {
                (boosts.1, boosts.1 + (boosts.2 - boosts.1) / 2, boosts.2)
            }
        ;

        if boosts.2 - boosts.1 <= 1 {
            if immune_sys_won {
                break n_units
            } else {
                boosts.1 += 1;
            }
        }
    }

}

fn task_a(lines: Vec<String>) {
    let mut armies = parse_input(&lines);
    let (winning_units, _) = run_fights(&mut armies);

    println!("Winning army has {} units left", winning_units);
}

fn task_b(lines: Vec<String>) {
    let mut armies = parse_input(&lines);
    let winning_units = run_fights_with_boost(&mut armies);

    println!("Immune system has {} units left", winning_units);
}

fn parse_input(lines: &[String]) -> Vec<Vec<Group>> {
    use regex::Regex;

    let mut armies = Vec::new();

    let line_reg: Regex = Regex::new(concat!(
        r"(?P<units>\d+) units each with (?P<hitp>\d+) hit points (?:\((?P<opts>.*)\))? ?",
        r"with an attack that does (?P<n_dmg>\d+) (?P<t_dmg>(\w+)) damage at initiative (?P<ini>\d+)",
    )).unwrap();

    let mut units = Vec::new();
    for line in lines {
        match line.as_str() {
            "Immune System:" | "" => (),
            "Infection:" => {
                armies.push(units);
                units = Vec::new();
            },
            details => {
                let line_captures = line_reg.captures(details).unwrap();

                let count =         line_captures.name("units").unwrap().as_str().parse().unwrap();
                let hit_points =    line_captures.name("hitp").unwrap().as_str().parse().unwrap();
                let attack_damage = line_captures.name("n_dmg").unwrap().as_str().parse().unwrap();
                let initiative =    line_captures.name("ini").unwrap().as_str().parse().unwrap();

                let attack_type = AttackType::parse(line_captures.name("t_dmg").unwrap().as_str());
                let (weaknesses, immunities) =
                    if let Some(opts) = line_captures.name("opts") {
                        let mut weaknesses = Vec::new();
                        let mut immunities = Vec::new();
                        let o_parts = opts.as_str().split(&[';', ' ', ',']);
                        let mut o_attack = &mut weaknesses;
                        for part in o_parts {
                            match part {
                                "weak" => o_attack = &mut weaknesses,
                                "immune" => o_attack = &mut immunities,
                                "to" | "" => (),
                                _ => o_attack.push(AttackType::parse(part)),
                            }
                        }

                        (
                            if !weaknesses.is_empty() { Some(weaknesses) } else { None },
                            if !immunities.is_empty() { Some(immunities) } else { None },
                        )
                    } else {
                        (None, None)
                    };

                units.push(
                    Group {
                        units_count: count,
                        hit_points,
                        attack_damage,
                        attack_type,
                        initiative,
                        effective_power: count as u32 * attack_damage as u32,
                        weaknesses,
                        immunities,
                    }
                );
            }
        }
    }
    armies.push(units);

    armies
}


#[cfg(test)]
mod tests18_24 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;
    use std::io::BufRead;
    use std::io::BufReader;

    fn get_input_from_file(day_num: &str) -> Vec<String> {
        let filename = format!("/home/ingemar/adventofcode/aoc2018/input/day-{}.txt", day_num);
        let file = std::fs::File::open(&filename).unwrap_or_else(|err| {
            eprintln!("Cannot open file '{}': {}", filename, err);
            std::process::exit(1);
        });

        let reader = BufReader::new(&file);
        let lines: Vec<String> = reader.lines().collect::<Result<_, _>>().unwrap();

        lines
    }

    #[test]
    fn tests18_24_t01() {
        use regex::Regex;

        let parse_att_type = |att_str| {
            match att_str {
                "radiation" =>   AttackType::Radiation,
                "slashing" =>    AttackType::Slashing,
                "bludgeoning" => AttackType::Bludgeoning,
                "fire" =>        AttackType::Fire,
                "cold" =>        AttackType::Cold,
                p => panic!("Unknown attack type: {p}")
            }
        };

        let line_reg: Regex = Regex::new(concat!(
            r"(?P<units>\d+) units each with (?P<hitp>\d+) hit points (?:\((?P<opts>.*)\))? ?",
            r"with an attack that does (?P<n_dmg>\d+) (?P<t_dmg>(\w+)) damage at initiative (?P<ini>\d+)",
        )).unwrap();
        for line in [
"956 units each with 7120 hit points (weak to bludgeoning, slashing) with an attack that does 71 radiation damage at initiative 7",
"1155 units each with 5643 hit points (weak to bludgeoning; immune to cold) with an attack that does 42 slashing damage at initiative 15",
"1658 units each with 5507 hit points (weak to cold; immune to bludgeoning, slashing) with an attack that does 32 radiation damage at initiative 3",
"5009 units each with 8078 hit points with an attack that does 14 slashing damage at initiative 8",
"115 units each with 9090 hit points (immune to slashing; weak to bludgeoning) with an attack that does 672 slashing damage at initiative 17",
"5021 units each with 5720 hit points (immune to fire, radiation) with an attack that does 10 fire damage at initiative 4",
        ] {
            println!("----------------------------------------------\n{line}");

            let line_captures = line_reg.captures(line).unwrap();
            if let Some(opts) = line_captures.name("opts") {
                let mut weaknesses = Vec::new();
                let mut immunities = Vec::new();
                println!("§{}", opts.as_str());
                let o_parts = opts.as_str().split(&[';', ' ', ',']);
                let mut o_attack = &mut weaknesses;
                for part in o_parts {
println!("part = {:?}", part);
                    match part {
                        "weak" => o_attack = &mut weaknesses,
                        "immune" => o_attack = &mut immunities,
                        "to" | "" => (),
                        _ => o_attack.push(parse_att_type(part)),
                    }
                }
println!("weaknesses = {:?}", weaknesses);
println!("immunities = {:?}", immunities);
            } else {
                println!("None");
            };
        }
    }
}

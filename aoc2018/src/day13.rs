use std::{cmp::Ordering::*, collections::HashSet};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Tracks = Vec<Vec<Path>>;
type Index = (usize, usize);

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
enum Path {
    UpDown,
    LeftRight,
    CornTopLeft,
    CornTopRight,
    CornBottomLeft,
    CornBottomRight,
    Intersect,
    Void,
}

#[derive(Clone, Debug)]
enum Turn {
    Left,
    Straight,
    Right,
}

impl Turn {
    fn next_turn(&mut self) {
        *self = match *self {
            Self::Left => Self::Straight,
            Self::Straight => Self::Right,
            Self::Right => Self::Left,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, Debug)]
struct Cart {
    x: usize,
    y: usize,
    direction: Direction,
    last_turn: Turn,
}

impl Cart {
    fn mv(&mut self, tracks: &Tracks) {
        match self.direction {
            Direction::Up => self.y -= 1,
            Direction::Down => self.y += 1,
            Direction::Left => self.x -= 1,
            Direction::Right => self.x += 1,
        }

        let path = &tracks[self.y][self.x];
        self.direction = match *path {
            Path::CornTopLeft     if self.direction == Direction::Up    => Direction::Right,
            Path::CornTopLeft     if self.direction == Direction::Left  => Direction::Down,
            Path::CornTopRight    if self.direction == Direction::Up    => Direction::Left,
            Path::CornTopRight    if self.direction == Direction::Right => Direction::Down,
            Path::CornBottomLeft  if self.direction == Direction::Left  => Direction::Up,
            Path::CornBottomLeft  if self.direction == Direction::Down  => Direction::Right,
            Path::CornBottomRight if self.direction == Direction::Right => Direction::Up,
            Path::CornBottomRight if self.direction == Direction::Down  => Direction::Left,
            Path::Intersect => {
                self.last_turn.next_turn();
                match self.last_turn {
                    Turn::Left => match self.direction {
                        Direction::Up    => Direction::Left,
                        Direction::Down  => Direction::Right,
                        Direction::Left  => Direction::Down,
                        Direction::Right => Direction::Up,
                    },
                    Turn::Right => match self.direction {
                        Direction::Up    => Direction::Right,
                        Direction::Down  => Direction::Left,
                        Direction::Left  => Direction::Up,
                        Direction::Right => Direction::Down,
                    },
                    Turn::Straight => self.direction.clone()
                }
            },
            Path::Void => panic!(),
            _ => self.direction.clone(),
        }
    }
}

#[derive(Clone, Debug)]
struct Carts(Vec<Cart>);

impl Carts {
    fn sort_carts(&mut self) {
        self.0.sort_by(|a, b| {
            let comp = a.y.cmp(&b.y);
            match comp {
                Greater | Less => comp,
                Equal => a.x.cmp(&b.x),
            }
        });
    }

    fn add(&mut self, cart: Cart) {
        self.0.push(cart);
    }
}

fn run_to_crash(tracks: &Tracks, carts: &mut Carts, break_at_first: bool) -> Vec<Index> {
    let mut positions = HashSet::new();
    let mut crashes = Vec::new();

    for cart in carts.0.iter() {
        positions.insert((cart.x, cart.y));
    }

    'main: loop {
        carts.sort_carts();

        for cart in carts.0.iter_mut() {
            if crashes.contains(&(cart.x, cart.y)) {
                continue;
            }
            positions.remove(&(cart.x, cart.y));
            cart.mv(tracks);

            if positions.contains(&(cart.x, cart.y)) {
                // Crash detected
                crashes.push((cart.x, cart.y));
                if break_at_first {
                    break 'main
                }
            }

            positions.insert((cart.x, cart.y));
        }

        if !crashes.is_empty() {
            break;
        }
    }

    crashes
}

fn task_a(lines: Vec<String>) {
    let (tracks, mut carts) = parse_input(&lines);

    let index = run_to_crash(&tracks, &mut carts, true)[0];

    println!("Location of first crash: {:?}", index);
}

fn task_b(lines: Vec<String>) {
    let (tracks, mut carts) = parse_input(&lines);

    let index = loop {
        let crash_indices = run_to_crash(&tracks, &mut carts, false);

        for index in crash_indices {
            carts.0.retain(|cart| (cart.x, cart.y) != index);
        }

        if carts.0.len() == 1 {
            break (carts.0[0].x, carts.0[0].y)
        }
    };

    println!("Location of the last cart: {:?}", index);
}

fn parse_input(lines: &[String]) -> (Tracks, Carts) {
    let mut carts = Carts(Vec::new());
    let mut prev_path = Path::Void;
    let tracks = lines
        .iter()
        .enumerate()
        .map(|(y, line)|
            line
            .chars()
            .enumerate()
            .map(|(x, c)| {
                let path = match c {
                    '|' => Path::UpDown,
                    '-' => Path::LeftRight,
                    '/' => if [Path::LeftRight, Path::Intersect].contains(&prev_path) { Path::CornBottomRight } else { Path::CornTopLeft },
                    '\\' => if [Path::LeftRight, Path::Intersect].contains(&prev_path) { Path::CornTopRight } else { Path::CornBottomLeft },
                    '+' => Path::Intersect,
                    '^' => {
                        carts.add(Cart {x, y, direction: Direction::Up, last_turn: Turn::Right});
                        Path::UpDown
                    },
                    'v' => {
                        carts.add(Cart {x, y, direction: Direction::Down, last_turn: Turn::Right});
                        Path::UpDown
                    },
                    '<' => {
                        carts.add(Cart {x, y, direction: Direction::Left, last_turn: Turn::Right});
                        Path::LeftRight
                    },
                    '>' => {
                        carts.add(Cart {x, y, direction: Direction::Right, last_turn: Turn::Right});
                        Path::LeftRight
                    },
                    ' ' => Path::Void,
                    _ => panic!(),
                };
                prev_path = path;

                path
            })
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>();

    (tracks, carts)
}

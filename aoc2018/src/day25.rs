use std::collections::{HashSet, HashMap};
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Points = Vec<Point>;

#[derive(Debug)]
struct Point ([i8; 4]);

impl Point {
    fn calc_distance(&self, other: &Point) -> u16 {
        let mut distance = 0;
        for (i, dim_val) in self.0.iter().enumerate() {
            distance += (*dim_val - other.0[i]).unsigned_abs() as u16;
        }

        distance
    }
}

fn count_constellations(points: &Points) -> usize {
    let mut point_numbers = (1 ..= points.len()).collect::<HashSet<usize>>();
    let mut distances = HashMap::new();

    for i in 0 .. points.len() - 1 {
        for j in i + 1 .. points.len() {
            let dist = points[i].calc_distance(&points[j]);
            distances.insert((i + 1, j + 1), dist);
        }
    }

    let mut constellations = Vec::new();
    while !point_numbers.is_empty() {
        let numb = *point_numbers.iter().next().unwrap();
        let mut constellation = vec![numb];
        point_numbers.remove(&numb);
        let mut candidates = constellation.clone();
        while let Some(number) = candidates.pop() {
            let numbers = point_numbers.iter().copied().collect_vec();
            for other_num in numbers {
                let key = if other_num < number { (other_num, number) } else { (number, other_num) };
                if *distances.get(&key).unwrap() <= 3 {
                    constellation.push(other_num);
                    point_numbers.remove(&other_num);
                    candidates.push(other_num);
                }
            }
        }
        constellation.sort();
        constellations.push(constellation);
    }

    constellations.len()
}

fn task_a(lines: Vec<String>) {
    let points = parse_input(&lines);
    let n_constellations = count_constellations(&points);

    println!("# constellations = {}", n_constellations);
}

fn task_b(_lines: Vec<String>) {
    println!("No real task B on day 25");
}

fn parse_input(lines: &[String]) -> Points {
    lines
        .iter()
        .map(|line| {
            line
                .split(',')
                .filter_map(|s| s.parse().ok())
                .enumerate()
                .fold(Point([99; 4]), |mut p, (i, d)| {
                    p.0[i] = d;
                    p
                })
        })
        .collect()
}

use std::{collections::{BinaryHeap, HashMap}, cmp::Reverse};

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    pub type Pos = (usize, usize);

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    pub enum Region {
        Rocky,
        Narrow,
        Wet,
        Unknown,
    }

    #[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
    pub enum Tool {
        Torch,
        ClimbingGear,
        Nothing,
    }

    #[derive(Debug)]
    pub struct Cave {
        depth: usize,
        target: Pos,
        regions: Vec<Vec<(usize, usize, Region)>>,
    }

    impl Cave {
        pub fn new(depth: usize, target: Pos) -> Self {
            Self { depth, target, regions: Vec::new() }
        }

        pub fn determine_regions(&mut self, add: usize) {
            let target = self.target;
            for y in 0 ..= target.0 + add {
                self.regions.push(Vec::new());
                for x in 0 ..= target.1 + add {
                    self.calc_geo_index((y, x));
                    self.calc_erosion_level((y, x));
                    self.calc_region_type((y, x));
                }
            }
        }

        /*
            The region at 0,0 (the mouth of the cave) has a geologic index of 0.
            The region at the coordinates of the target has a geologic index of 0.
            If the region's Y coordinate is 0, the geologic index is its X coordinate times 16807.
            If the region's X coordinate is 0, the geologic index is its Y coordinate times 48271.
            Otherwise, the region's geologic index is the result of multiplying the erosion levels of the regions at X-1,Y and X,Y-1.
        */
        fn calc_geo_index(&mut self, region: Pos) {
            let (y, x) = region;
            self.regions[y].push((0, 0, Region::Unknown));

            self.regions[y][x].0 =
                if region == (0, 0) || region == self.target {
                    0
                } else if y == 0 {
                    x * 16807
                } else if x == 0 {
                    y * 48271
                } else {
                    self.regions[y - 1][x].1 * self.regions[y][x - 1].1
                }
            ;
        }

        /*
            A region's erosion level is its geologic index plus the cave system's depth, all modulo 20183
        */
        fn calc_erosion_level(&mut self, (y, x): Pos) {
            self.regions[y][x].1 = (self.regions[y][x].0 + self.depth) % 20183;
        }

        /*
            If the erosion level modulo 3 is 0, the region's type is rocky.
            If the erosion level modulo 3 is 1, the region's type is wet.
            If the erosion level modulo 3 is 2, the region's type is narrow.
        */
        fn calc_region_type(&mut self, (y, x): Pos) {
            self.regions[y][x].2 = match self.regions[y][x].1 % 3 {
                0 => Region::Rocky,
                1 => Region::Wet,
                2 => Region::Narrow,
                _ => panic!()
            };

        }

        pub fn calc_risk_level(&self) -> usize {
            self
                .regions
                .iter()
                .map(|rr|
                    rr
                    .iter()
                    .fold(0, |s, r| s + match r.2 {
                        Region::Rocky => 0,
                        Region::Wet => 1,
                        Region::Narrow => 2,
                        _ => panic!()
                    })
                )
                .sum()
        }

        pub fn possible_actions(&mut self, (y, x): Pos, curr_tool: Tool) -> Vec<((usize, usize), usize, Option<Tool>)> {
            /*
                possibilities
                    rocky
                        torch
                            move to rocky or narrow
                            switch to climbing gear
                        climbing gear
                            move to rocky or wet
                            switch to torch
                    wet
                        climbing gear
                            move to rocky or wet
                            switch to nothing
                        nothing
                            move to wet or narrow
                            switch to climbing gear
                    narrow
                        torch
                            move to rocky or narrow
                            switch to nothing
                        nothing
                            move to wet or narrow
                            switch to torch

            */
            let mut actions = Vec::new();
            for nbr in self.get_neighbors((y, x)) {
                match curr_tool {
                    Tool::Torch =>          if [Region::Rocky, Region::Narrow].contains(&nbr.1) { actions.push((nbr.0, 1, None)); },
                    Tool::ClimbingGear =>   if [Region::Rocky, Region::Wet].contains(&nbr.1) { actions.push((nbr.0, 1, None)); },
                    Tool::Nothing =>        if [Region::Wet, Region::Narrow].contains(&nbr.1) { actions.push((nbr.0, 1, None)); },
                }
            }
            let tool =
                match self.regions[y][x].2 {
                    Region::Rocky =>    if curr_tool == Tool::Torch { Tool::ClimbingGear } else { Tool::Torch },
                    Region::Wet =>      if curr_tool == Tool::ClimbingGear { Tool::Nothing } else { Tool::ClimbingGear },
                    Region::Narrow =>   if curr_tool == Tool::Torch { Tool::Nothing } else { Tool::Torch },
                    _ => panic!()
                }
            ;
            actions.push(((y, x), 7, Some(tool)));

            actions
        }

        fn get_neighbors(&mut self, (y, x): Pos) -> Vec<(Pos, Region)> {
            let mut nbrs = Vec::new();
            if y > 0 { nbrs.push(((y - 1, x), self.regions[y - 1][x].2)); }
            if x > 0 { nbrs.push(((y, x - 1), self.regions[y][x - 1].2)); }
            nbrs.push(((y + 1, x), self.regions[y + 1][x].2));
            nbrs.push(((y, x + 1), self.regions[y][x + 1].2));

            nbrs
        }
    }

    #[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
    pub struct State {
        pub cost: usize,
        pub position: Pos,
        pub tool: Tool,
    }
}

use internal::*;

fn shortest_path(cave: &mut Cave, target: Pos) -> Option<usize> {
    let mut distances = HashMap::new();
    let mut queue = BinaryHeap::new();

    // We're at `start`, with a zero cost
    queue.push(Reverse(State { cost: 0, position: (0, 0), tool: Tool::Torch }));

    // Examine the frontier with lower cost nodes first (min-heap)
    while let Some(Reverse(State { cost, position, tool: current_tool })) = queue.pop() {
        // Alternatively we could have continued to find all shortest paths
        if position == target {
            let r_cost =
                if current_tool == Tool::Torch {
                    cost
                } else {
                    cost + 7
                }
            ;
            return Some(r_cost);
        }

        // Important as we may have already found a better way
        if let Some(dist) = distances.get(&(position.0, position.1, current_tool)) {
            if cost > *dist {
                continue;
            }
        }

        // For each region we can reach, see if we can find a way with a lower cost going through this region
        for region in cave.possible_actions(position, current_tool) {
            let next_tool = if region.2.is_some() { region.2.unwrap() } else { current_tool };
            let next = State { cost: cost + region.1, position: region.0, tool: next_tool };
            let (y, x) = next.position;
            let dist_key = (y, x, next.tool);

            // If so, add it to the frontier and continue
            let dist_o = distances.get(&dist_key);
            let is_better = dist_o.is_none() || next.cost < *dist_o.unwrap();
            if is_better  {
                queue.push(Reverse(next));
                // Relaxation, we have now found a better way
                distances.insert((y, x, next.tool), next.cost);
            }
        }
    }

    // Goal not reachable
    None
}

fn task_a(lines: Vec<String>) {
    let (depth, target) = parse_input(&lines);
    let mut cave = Cave::new(depth, target);
    cave.determine_regions(0);
    let risk_level = cave.calc_risk_level();

    println!("Total risk level = {}", risk_level);
}

fn task_b(lines: Vec<String>) {
    let (depth, target) = parse_input(&lines);
    let mut cave = Cave::new(depth, target);
    cave.determine_regions(1000);
    let time = shortest_path(&mut cave, target).unwrap();

    println!("Shortest time to target = {}", time);
}

fn parse_input(lines: &[String]) -> (usize, Pos) {
    let depth: usize = lines[0].split(' ').nth(1).unwrap().parse().unwrap();
    let mut t_parts = lines[1].split([' ', ',']).skip(1);
    let x: usize = t_parts.next().unwrap().parse().unwrap();
    let y: usize = t_parts.next().unwrap().parse().unwrap();

    (depth, (y, x))
}

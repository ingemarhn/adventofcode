use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::collections::HashMap;

    type Step = char;
    type Steps = Vec<Step>;
    pub type BuildSteps = HashMap<Step, BuildStep>;

    #[derive(Clone, Debug, PartialEq)]
    pub struct BuildStep {
        step: Step,
        following_steps: Steps,
        must_be_done_steps: Steps,
        is_done: bool,
    }

    impl BuildStep {
        pub fn new(step: Step,) -> Self {
            Self { step, following_steps: Steps::new(), must_be_done_steps: Steps::new(), is_done: false }
        }

        pub fn add_must_step(&mut self, step: Step) {
            self.must_be_done_steps.push(step);
        }

        pub fn add_follow_step(&mut self, step: Step) {
            self.following_steps.push(step);
        }

        pub fn get_must_steps(&self) -> Steps {
            self.must_be_done_steps.clone()
        }

        pub fn get_follow_steps(&self) -> Steps {
            self.following_steps.clone()
        }

        pub fn set_done(&mut self) {
            self.is_done = true;
        }

        pub fn is_done(&self) -> bool {
            self.is_done
        }

        pub fn completion_time(&self) -> u8 {
            self.step as u8 - 64 + 60
        }
    }

}

use internal::*;

fn task_a(lines: Vec<String>) {
    let mut build_steps = parse_input(&lines);
    let mut exec_order = String::new();

    // Find available start build steps
    let mut available_steps = Vec::new();
    for step in build_steps.keys() {
        if build_steps[step].get_must_steps().is_empty() {
            available_steps.push(*step);
        }
    }

    while !available_steps.is_empty() {
        // Reverse sort
        available_steps.sort_unstable_by(|a, b| b.cmp(a));

        let  step = available_steps.pop().unwrap();
        build_steps.entry(step).and_modify(|e| e.set_done());
        exec_order.push(step);

        for follow in build_steps[&step].get_follow_steps() {
            let mut all_done = true;
            for must in build_steps[&follow].get_must_steps() {
                if !build_steps[&must].is_done() {
                    all_done = false;
                    break;
                }
            }

            if all_done {
                available_steps.push(follow);
            }
        }
    }

    println!("Steps completed in order = {}", exec_order);
}

fn task_b(lines: Vec<String>) {
    let mut build_steps = parse_input(&lines);
    let mut completion_time = 0_u32;
    let mut n_available_workers = 5;

    // Find available start build steps
    let mut available_steps = Vec::new();
    for step in build_steps.keys() {
        if build_steps[step].get_must_steps().is_empty() {
            available_steps.push(*step);
        }
    }

    let mut working_list = HashMap::new();
    while !available_steps.is_empty() || !working_list.is_empty() {
        // Reverse sort
        available_steps.sort_unstable_by(|a, b| b.cmp(a));

        let n_steps = available_steps.len().min(n_available_workers);
        for _ in 0 .. n_steps {
            let  step = available_steps.pop().unwrap();
            let compl_time = build_steps[&step].completion_time();
            working_list.insert(step, compl_time);
        }
        n_available_workers -= n_steps;
        let min_compl_time =
            working_list
            .iter()
            .fold(u8::MAX, |min, (_, &w_t)| min.min(w_t));

        let compl_list =
            working_list
            .iter()
            .fold(Vec::new(), |mut c_l, (&w_c, &w_t)| {
                if w_t == min_compl_time {
                    c_l.push(w_c);
                }

                c_l
            });

        for step in &compl_list {
            build_steps.entry(*step).and_modify(|e| e.set_done());
            working_list.remove(step);
        }

        for (_, comp_t) in working_list.iter_mut() {
            *comp_t -= min_compl_time;
        }

        completion_time += min_compl_time as u32;

        for step in &compl_list {
            for follow in build_steps[step].get_follow_steps() {
                let mut all_done = true;
                for must in build_steps[&follow].get_must_steps() {
                    if !build_steps[&must].is_done() {
                        all_done = false;
                        break;
                    }
                }

                if all_done {
                    available_steps.push(follow);
                }
            }
        }

        n_available_workers += compl_list.len();
    }

    println!("Completion time = {}", completion_time);
}

fn parse_input(lines: &[String]) -> BuildSteps {
    let mut steps = HashMap::new();

    for line in lines {
        let mut chrs = line.chars();
        let must_step = chrs.nth(5).unwrap();
        let dep_step = chrs.nth(30).unwrap();
        {
            let e_must = steps.entry(must_step).or_insert(BuildStep::new(must_step));
            e_must.add_follow_step(dep_step);
        }
        {
            let e_dep  = steps.entry(dep_step).or_insert(BuildStep::new(dep_step));
            e_dep.add_must_step(must_step);
        }
    }

    steps
}

use std::collections::HashMap;
use regex::Regex;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Reg = u16;

#[allow(non_camel_case_types)]
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
enum OpCode {
    addr,  // (add register) stores into register C the result of adding register A and register B.
    addi,  // (add immediate) stores into register C the result of adding register A and value B.
    mulr,  // (multiply register) stores into register C the result of multiplying register A and register B.
    muli,  // (multiply immediate) stores into register C the result of multiplying register A and value B.
    banr,  // (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
    bani,  // (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
    borr,  // (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
    bori,  // (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
    setr,  // (set register) copies the contents of register A into register C. (Input B is ignored.)
    seti,  // (set immediate) stores value A into register C. (Input B is ignored.)
    gtir,  // (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    gtri,  // (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    gtrr,  // (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    eqir,  // (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
    eqri,  // (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    eqrr,  // (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
}

impl OpCode {
    fn exec(&self, instr: Instruction, regs: Registers) -> Reg {
        let ins = |i| instr.0[i] as usize;
        match *self {
            Self::addr => regs.0[ins(1)] + regs.0[ins(2)],
            Self::addi => regs.0[ins(1)] + instr.0[2],
            Self::mulr => regs.0[ins(1)] * regs.0[ins(2)],
            Self::muli => regs.0[ins(1)] * instr.0[2],
            Self::banr => regs.0[ins(1)] & regs.0[ins(2)],
            Self::bani => regs.0[ins(1)] & instr.0[2],
            Self::borr => regs.0[ins(1)] | regs.0[ins(2)],
            Self::bori => regs.0[ins(1)] | instr.0[2],
            Self::setr => regs.0[ins(1)],
            Self::seti => instr.0[1],
            Self::gtir => if instr.0[1] > regs.0[ins(2)] { 1 } else { 0 },
            Self::gtri => if regs.0[ins(1)] > instr.0[2] { 1 } else { 0 },
            Self::gtrr => if regs.0[ins(1)] > regs.0[ins(2)] { 1 } else { 0 },
            Self::eqir => if instr.0[1] == regs.0[ins(2)] { 1 } else { 0 },
            Self::eqri => if regs.0[ins(1)] == instr.0[2] { 1 } else { 0 },
            Self::eqrr => if regs.0[ins(1)] == regs.0[ins(2)] { 1 } else { 0 },
        }
    }

    fn iterator() -> std::slice::Iter<'static, OpCode> {
        use OpCode::*;

        [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr].iter()
    }
}

#[derive(Clone, Copy, Debug)]
struct Registers([Reg; 4]);
#[derive(Clone, Copy, Debug)]
struct Instruction([Reg; 4]);

#[derive(Debug)]
struct Sample {
    before: Registers,
    after: Registers,
    instruction: Instruction,
}

fn test_samples(samples: &[Sample]) -> u16 {
    let mut samples_behave_count = 0;

    for sample in samples {
        let mut candidate_count = 0;

        for op in OpCode::iterator() {
            let c = op.exec(sample.instruction, sample.before);
            if c == sample.after.0[sample.instruction.0[3] as usize] {
                candidate_count += 1;
                if candidate_count == 3 {
                    samples_behave_count += 1;
                    break;
                }
            }
        }
    }

    samples_behave_count
}

fn run_test_program(samples: &[Sample], instructions: &[Instruction]) -> Reg {
    let mut possible_ops = HashMap::new();

    for op in OpCode::iterator() {
        possible_ops.insert(op, Vec::new());
        for sample in samples {
            let c = op.exec(sample.instruction, sample.before);
            if c == sample.after.0[sample.instruction.0[3] as usize] {
                possible_ops.entry(op).and_modify(|e| if !e.contains(&sample.instruction.0[0]) {
                    e.push(sample.instruction.0[0])
                });
            } else {
                possible_ops.entry(op).and_modify(|e| {
                    if let Some(ind) = e.iter().position(|o| *o == sample.instruction.0[0]) {
                        e.remove(ind);
                    }
                });
            }
        }
    }

    let mut opcodes = HashMap::new();
    while !possible_ops.is_empty() {
        let Some((&&op, v)) = possible_ops.iter().find(|(_, v)| v.len() == 1) else { panic!() };
        let opc = op;
        let vc = v.clone();
        opcodes.insert(vc[0], op);
        possible_ops.remove(&opc);
        for (_, vv) in possible_ops.iter_mut() {
            vv.retain(|&o| o != vc[0]);
        }
    }

    let mut registers = Registers([0; 4]);
    for instr in instructions.iter() {
        let c = opcodes.get(&instr.0[0]).unwrap().exec(*instr, registers);
        registers.0[instr.0[3] as usize] = c;
    }

    registers.0[0]
}

fn task_a(lines: Vec<String>) {
    let (samples, _) = parse_input(&lines);

    let n_behave = test_samples(&samples);
    println!("# samples: {}", n_behave);
}

fn task_b(lines: Vec<String>) {
    let (samples, instructions) = parse_input(&lines);

    let reg_0 = run_test_program(&samples, &instructions);
    println!("Register 0 = {}", reg_0);
}

fn parse_input(lines: &[String]) -> (Vec<Sample>, Vec<Instruction>) {
    let mut lines_it = lines.iter();
    let mut prev_line = &String::new();
    let line_reg: Regex = Regex::new(concat!(
        r"(?:(?P<state>(?:Before|After)):\s+\[(?P<r0>\d), (?P<r1>\d), (?P<r2>\d), (?P<r3>\d)\]|",
        r"(?P<opcode>\d+) (?P<a>\d) (?P<b>\d) (?P<c>\d))"
    )).unwrap();
    let mut samples = Vec::new();
    let mut before = Registers([0; 4]);
    let mut instruction = Instruction([0; 4]);

    let parse_line = |line| {
        let captures = line_reg.captures(line).unwrap();
        let state = captures.name("state");
        let parse = move |name: &str| -> Reg {
            captures.name(name).unwrap().as_str().parse().unwrap()
        };

        (parse, state)
    };

    loop {
        let line = lines_it.next().unwrap();
        if line.is_empty() {
            if !prev_line.is_empty() {
                prev_line = line;
                continue;
            }

            lines_it.next();
            break;
        }

        let (parse, state) = parse_line(line);

        match state {
            Some(s) if s.as_str() == "Before" => {
                before = Registers([parse("r0"), parse("r1"), parse("r2"), parse("r3")]);
            },
            Some(_s) => {
                let sample = Sample { before, instruction, after: Registers([parse("r0"), parse("r1"), parse("r2"), parse("r3")]) };
                samples.push(sample);
            },
            None => {
                instruction = Instruction([parse("opcode"), parse("a"), parse("b"), parse("c")])
            },
        };

        prev_line = line;
    }

    let mut instructions = Vec::new();

    for line in lines_it {
        let (parse, _) = parse_line(line);
        instruction = Instruction([parse("opcode"), parse("a"), parse("b"), parse("c")]);
        instructions.push(instruction);
    }

    (samples, instructions)
}

#[cfg(test)]
mod tests18_16 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;
    use std::io::BufRead;
    use std::io::BufReader;

    fn get_input_from_file(day_num: &str) -> Vec<String> {
        let filename = format!("/home/ingemar/adventofcode/aoc2018/input/day-{}.txt", day_num);
        let file = std::fs::File::open(&filename).unwrap_or_else(|err| {
            eprintln!("Cannot open file '{}': {}", filename, err);
            std::process::exit(1);
        });

        let reader = BufReader::new(&file);
        let lines: Vec<String> = reader.lines().collect::<Result<_, _>>().unwrap();

        lines
    }

    #[test]
    fn tests18_16_t01() {
        let lines = &get_input_from_file("16");
        let samples = parse_input(lines);
        println!("samples = {:?}", samples);
    }

    #[test]
    fn tests18_16_t02() {
        let lines = &get_input_from_file("16");
        let (samples, _) = parse_input(lines);

        println!("addr = {}", OpCode::addr.exec(samples[49].instruction, samples[49].before));
        println!("addi = {}", OpCode::addi.exec(samples[49].instruction, samples[49].before));
        println!("mulr = {}", OpCode::mulr.exec(samples[49].instruction, samples[49].before));
        println!("muli = {}", OpCode::muli.exec(samples[49].instruction, samples[49].before));
        println!("gtir = {}", OpCode::gtir.exec(samples[49].instruction, samples[49].before));
        println!("eqir = {}", OpCode::eqir.exec(samples[49].instruction, samples[49].before));
        println!("eqrr = {}", OpCode::eqrr.exec(samples[49].instruction, samples[49].before));
    }
}

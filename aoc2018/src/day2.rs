use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut dbl_tri_counter = HashMap::new();
    dbl_tri_counter.insert(2, 0);
    dbl_tri_counter.insert(3, 0);

    for line in lines {
        let mut char_counter = HashMap::new();

        for ch in line.chars() {
            char_counter.entry(ch).and_modify(|counter| *counter += 1).or_insert(1);
        }

        let mut found_dbl = false;
        let mut found_tri = false;
        for (_, &v) in char_counter.iter() {
            match v {
                2 => found_dbl = true,
                3 => found_tri = true,
                _ => (),
            }
        }

        if found_dbl {
            dbl_tri_counter.entry(2).and_modify(|e| { *e += 1 });
        }
        if found_tri {
            dbl_tri_counter.entry(3).and_modify(|e| { *e += 1 });
        }
    }

    let chksum = dbl_tri_counter.get(&2).unwrap() * dbl_tri_counter.get(&3).unwrap();

    println!("Checksum = {chksum}");
}

fn task_b(lines: Vec<String>) {
    let lines_len = lines.len();
    let mut eq_string = String::new();

    'o: for i in 0 .. lines_len - 1 {
        'i: for j in i + 1 .. lines_len {
            let lines_i = lines[i].chars().enumerate();
            let mut lines_j = lines[j].chars();
            let mut d_ind = 100;
            let mut diff_count = 0;

            for (dfi, i_ch) in lines_i {
                if i_ch != lines_j.next().unwrap() {
                    diff_count += 1;
                    d_ind = dfi;
                }

                if diff_count > 1 {
                    continue 'i;
                }
            }

            if diff_count == 1 {
                eq_string = lines[i][0 .. d_ind].to_string();
                eq_string.push_str(&lines[i][d_ind + 1 ..]);
                break 'o;
            }
        }
    }

    println!("Common letters = {}", eq_string);
}

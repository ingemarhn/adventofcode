use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn get_guard_map(lines: &[String]) -> HashMap<i32, HashMap<i32, i32>> {
    let lines = parse_input(lines);

    let get_min = |tm: &&str| {
        tm[3..5].parse().unwrap()
    };

    let mut guards = HashMap::new();
    let mut guard_no = 0;
    let mut start_sleep = 0;
    for line in lines {
        match &line.split(' ').collect::<Vec<&str>>()[..] {
            [.., g_num, "begins", "shift"] => {
                guard_no = g_num[1..].parse().unwrap();
            },
            [.., sleeps, "falls", "asleep"] => {
                start_sleep = get_min(sleeps);
            },
            [.., wakens, "wakes", "up"] => {
                let wakes_up = get_min(wakens);
                let em = guards.entry(guard_no).or_insert(HashMap::new());
                for min in start_sleep .. wakes_up {
                    let es = em.entry(min).or_insert(0);
                    *es += 1;
                }
            },
            _ => panic!("Oh noooooooooo! {line}"),
        }
    }

    guards
}

fn task_a(lines: Vec<String>) {
    let guards = get_guard_map(&lines);

    let mut max = (0, 0);
    for (g, min_sleep) in &guards {
        let tot_min = min_sleep.iter().fold(0, |s, (_, v)| s + v);
        if tot_min > max.1 {
            max = (*g, tot_min);
        }
    }

    let mut most_min = (0, 0);
    for (m, n) in &guards[&max.0] {
        if *n > most_min.1 {
            most_min = (*m, *n);
        }
    }

    println!("Guard {} sleeps most in minute {}. Answer = {}", max.0, most_min.0, max.0 * most_min.0);
}

fn task_b(lines: Vec<String>) {
    let guards = get_guard_map(&lines);

    let mut g_most_min = (0, 0, 0);
    for (g, min_sleep) in &guards {
        for (m, n) in min_sleep {
            if *n > g_most_min.2 {
                g_most_min = (*g, *m, *n);
            }
        }
    }

    println!("Guard {} sleeps most in minute {}. Answer = {}", g_most_min.0, g_most_min.1, g_most_min.0 * g_most_min.1);
}

fn parse_input(lines: &[String]) -> Vec<String> {
    let mut lines = lines.to_vec();
    lines.sort();
    lines
}

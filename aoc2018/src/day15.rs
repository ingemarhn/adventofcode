use std::collections::{HashMap, HashSet};
use std::cmp::Ordering;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Units = Vec<Unit>;
type Map = Vec<Vec<CaveUnit>>;
type Position = (usize, usize);

#[derive(Clone, Copy, Debug, PartialEq)]
enum CaveUnit {
    Wall,
    Open,
    Elf,
    Goblin,
}


#[derive(Clone, Debug)]
struct Cave {
    map: Map,
    units: Units,
    current_unit: Option<usize>,
}

impl Cave {
    fn new(map: Map, units: Units) -> Self {
        Self { map, units, current_unit: None }
    }

    fn next_unit(&mut self) -> Option<Unit> {
        if let Some(ind) = self.current_unit {
            if ind + 1 < self.units.len() {
                self.current_unit = Some(ind + 1);
            } else {
                self.current_unit = None;
            }
        } else {
            self.current_unit = Some(0);
            self.units.retain(|u| u.hit_points != 0);
            // Sort the units based on their new positions
            self.units.sort_by(|a, b| {
                let mut ord = a.position.1.cmp(&b.position.1);
                if ord == Ordering::Equal {
                    ord = a.position.0.cmp(&b.position.0);
                }

                ord
            });
        }

        if let Some(ind) = self.current_unit {
            Some(self.units[ind])
        } else {
            None
        }
    }

    fn neighbor_open_positions(&self, pos: &Position) -> Vec<Position> {
        let mut nghbrs = Vec::new();
        let mut add_nghbr = |p: Position| if self.map[p.1][p.0] == CaveUnit::Open { nghbrs.push(p); };

        for nbr_pos in 1 ..= 4 {
            match nbr_pos {
                1 => add_nghbr((pos.0, pos.1 - 1)),
                2 => add_nghbr((pos.0 - 1, pos.1)),
                3 => add_nghbr((pos.0 + 1, pos.1)),
                4 => add_nghbr((pos.0, pos.1 + 1)),
                _ => panic!()
            }
        }

        nghbrs
    }

    fn identify_enemies(&self, unit: &Unit) -> Units {
        self.units.iter().filter(|u| u.kind != unit.kind && u.hit_points != 0).copied().collect()
    }

    fn get_enemies_open_squares(&self, enemies: &Units) -> Vec<Position> {
        let mut open: Vec<Position> = enemies
            .iter()
            .flat_map(|e| self.neighbor_open_positions(&e.position))
            .collect();

        let mut uniq_keys = HashSet::new();
        open.retain(|k| {
            uniq_keys.insert(*k)
        });

        open
    }

    /*
   make an openlist containing only the starting node
   make an empty closed list
   while (the destination node has not been reached):
       consider the node with the lowest f score in the open list
       if (this node is our destination node) :
           we are finished
       if not:
           put the current node in the closed list and look at all of its neighbors
           for (each neighbor of the current node):
               if (neighbor has lower g value than current and is in the closed list) :
                   replace the neighbor with the new, lower, g value
                   current node is now the neighbor's parent
               else if (current g value is lower and this neighbor is in the open list ) :
                   replace the neighbor with the new, lower, g value
                   change the neighbor's parent to our current node

               else if this neighbor is not in both lists:
                   add it to the open list and set its g

1.  Initialize the open list. Put the starting node on the list (you can leave its f at zero)
2.  Initialize the closed list

3.  while the open list is not empty
        a) find the node with the least f on the open list, call it "q"

        b) pop q off the open list

        c) generate q's successors and set their parents to q

        d) for each successor
            i) if successor is the goal, stop search

            ii) else, compute both g and h for successor
            successor.g = q.g + distance between q and successor
            successor.h = distance from goal to successor (heuristic)
            successor.f = successor.g + successor.h

            iii) if a node with the same position as successor is in the OPEN list and the f of the successor is larger, skip this successor

            iV) else if a node with the same position as successor is in the CLOSED list the f of the successor is larger, skip this successor
                else, add  the node to the OPEN list
        end (for loop)

        e) push q on the CLOSED list
    end (while loop)    */
    // A*
    fn a_star(&self, start: Position, goal: Position, max_steps: usize) -> Option<Vec<Position>> {
        #[derive(Debug)]
        struct Node {
            parent: Option<Position>,
            g: usize,
            f: usize,
        }

        let mut ret_path = Vec::new();
        let mut min_path_len = max_steps;
        let mut first_neighbors = self.neighbor_open_positions(&start);
        first_neighbors.sort_by(|p1, p2| {
            match p1.1.cmp(&p2.1) {
                cmp @ (Ordering::Less | Ordering::Greater) => cmp,
                Ordering::Equal => p1.0.cmp(&p2.0),
            }
        });

        for first_nghbr in first_neighbors.iter() {
            if *first_nghbr == goal {
                ret_path = vec![goal];
                break;
            }

            let mut open = HashMap::from([(*first_nghbr, Node { parent: None, g: 0, f: 0 })]);
            let mut closed = HashMap::new();

            let h = |p: &Position| goal.0.abs_diff(p.0) + goal.1.abs_diff(p.1);

            'l: while !open.is_empty() {
                // Find the node with smallest f
                let (unit_pos, _f) =
                    open
                    .iter()
                    .fold(((0, 0), usize::MAX), |(pos, f_low), (&key, Node { parent: _, g: _, f: f_cur })| {
                        match f_cur.cmp(&f_low) {
                            Ordering::Less => (key, *f_cur),
                            Ordering::Greater => (pos, f_low),
                            Ordering::Equal =>
                                if first_in_reading_order_positions(key, pos) == key {
                                    (key, *f_cur)
                                } else {
                                    (pos, f_low)
                                },
                        }
                    })
                ;

                let Node { parent: unit_parent, g: g_unit, f: f_unit } = open.remove(&unit_pos).unwrap();

                let neighbors = self.neighbor_open_positions(&unit_pos);
                if neighbors.is_empty() {
                    continue;
                }

                closed.insert(unit_pos, (unit_parent, f_unit));

                for nghbr in neighbors.iter() {
                    // compute both g and h for neighbor
                    // neighbor.g = unit_pos.g + distance between unit_pos and neighbor
                    // neighbor.h = distance from neighbor to goal  (heuristic)
                    // neighbor.f = neighbor.g + neighbor.h
                    let g_nghbr = g_unit + 1;
                    if g_nghbr > min_path_len {
                        continue 'l;
                    }
                    let h_nghbr = h(nghbr);
                    let f_nghbr = g_nghbr + h_nghbr;

                    if *nghbr == goal {
                        let mut path = vec![*nghbr];
                        let mut parent = unit_pos;
                        path.push(parent);

                        loop {
                            let (o_parent, _) = closed.get(&parent).unwrap();
                            if let Some(p) = o_parent {
                                parent = *p;
                                path.push(parent);
                            } else {
                                break;
                            }
                        }

                        if path.len() < min_path_len {
                            path.reverse();
                            min_path_len = path.len();
                            ret_path = path;
                        }

                        break 'l;
                    }

                    // if a unit with the same position as neighbor is in the OPEN list and the f of the neighbor is equal or larger, skip this neighbor
                    if let Some(node) = open.get(nghbr) {
                        if f_nghbr >= node.f {
                            continue;
                        }
                    }

                    // if a unit with the same position as neighbor is in the CLOSED list and the f of the neighbor is equal or larger, skip this neighbor
                    // else, add  the node to the OPEN list
                    if let Some(clsd) = closed.get(nghbr) {
                        if f_nghbr >= clsd.1 {
                            continue;
                        }
                    }

                    open.insert(*nghbr, Node { parent: Some(unit_pos), g: g_nghbr, f: f_nghbr });
                }
            }
        }

        if !ret_path.is_empty() {
            Some(ret_path)
        } else {
            None
        }
    }

    #[allow(unused_assignments, dead_code)]
    fn _a_star(&self, start: Position, goal: Position, max_steps: usize) -> Option<Vec<Position>> {
        #[derive(Debug)]
        struct Node {
            parent: Option<Position>,
            g: usize,
            f: usize,
        }

        let mut min_path_len = max_steps;
        let mut open = HashMap::from([(start, Node { parent: None, g: 0, f: 0 })]);
        let mut closed = HashMap::new();
        let mut ret_path = Vec::new();

        let h = |p: &Position| goal.0.abs_diff(p.0) + goal.1.abs_diff(p.1);

        'l: while !open.is_empty() {
            // Find the node with smallest f
            let (unit_pos, _f) =
                open
                .iter()
                .fold(((0, 0), usize::MAX), |(pos, f_low), (&key, Node { parent: _, g: _, f: f_cur })| {
                    match f_cur.cmp(&f_low) {
                        Ordering::Less => (key, *f_cur),
                        Ordering::Greater => (pos, f_low),
                        Ordering::Equal =>
                            if first_in_reading_order_positions(key, pos) == key {
                                (key, *f_cur)
                            } else {
                                (pos, f_low)
                            },
                    }
                })
            ;

            let Node { parent: unit_parent, g: g_unit, f: f_unit } = open.remove(&unit_pos).unwrap();

            let mut neighbors = self.neighbor_open_positions(&unit_pos);
            if neighbors.is_empty() {
                return None;
            }
            // Sorting is actually only needed for the first neighbors after start
            neighbors.sort_by(|p1, p2| {
                match p1.1.cmp(&p2.1) {
                    cmp @ (Ordering::Less | Ordering::Greater) => cmp,
                    Ordering::Equal => p1.0.cmp(&p2.0),
                }
            });

            closed.insert(unit_pos, (unit_parent, f_unit));

            for nghbr in neighbors.iter() {
                // compute both g and h for neighbor
                // neighbor.g = unit_pos.g + distance between unit_pos and neighbor
                // neighbor.h = distance from neighbor to goal  (heuristic)
                // neighbor.f = neighbor.g + neighbor.h
                let g_nghbr = g_unit + 1;
                if g_nghbr > min_path_len {
                    continue 'l;
                }
                let h_nghbr = h(nghbr);
                let f_nghbr = g_nghbr + h_nghbr;

                if *nghbr == goal {
                    let mut path = vec![*nghbr];
                    let mut parent = unit_pos;
                    path.push(parent);

                    loop {
                        let (o_parent, _) = closed.get(&parent).unwrap();
                        if let Some(p) = o_parent {
                            parent = *p;
                            path.push(parent);
                        } else {
                            // Skip the last position (which is the first) since the path shall start a position away from the start
                            path.pop();
                            break;
                        }
                    }

                    if path.len() < min_path_len {
                        path.reverse();
                        min_path_len = path.len();
                        ret_path = path;
                    }

                    break 'l;
                }

                // if a unit with the same position as neighbor is in the OPEN list and the f of the neighbor is equal or larger, skip this neighbor
                if let Some(node) = open.get(nghbr) {
                    if f_nghbr >= node.f {
                        continue;
                    }
                }

                // if a unit with the same position as neighbor is in the CLOSED list and the f of the neighbor is equal or  larger, skip this neighbor
                // else, add  the node to the OPEN list
                if let Some(clsd) = closed.get(nghbr) {
                    if f_nghbr >= clsd.1 {
                        continue;
                    }
                }

                open.insert(*nghbr, Node { parent: Some(unit_pos), g: g_nghbr, f: f_nghbr });
            }
        }

        if !ret_path.is_empty() {
            Some(ret_path)
        } else {
            None
        }
    }

    fn calc_less_steps_to_squares(&self, unit: Unit, squares: &[Position]) -> (usize, Position) {
        let mut min = usize::MAX;
        let mut start_pos = (0, 0);
        let mut goal_pos = (0, 0);

        for p in squares {
            let o_steps = self.a_star(unit.position, *p, min);
            if o_steps.is_none() {
                continue;
            }

            let steps = o_steps.unwrap();
            let n_steps = steps.len();
            if n_steps > min {
                continue;
            }

            if n_steps < min {
                min = n_steps;
                start_pos = steps[0];
                goal_pos = *p;
            } else {
                // equal # of steps
                let g = first_in_reading_order_positions(*p, goal_pos);
                if g == *p {
                    start_pos = steps[0];
                    goal_pos = *p;
                }
            }
        }

        (min, start_pos)
    }

    fn move_unit(&mut self, to: Position) {
        let unit = self.units.get_mut(self.current_unit.unwrap()).unwrap();
        self.map[to.1][to.0] = self.map[unit.position.1][unit.position.0];
        self.map[unit.position.1][unit.position.0] = CaveUnit::Open;
        unit.position = to;
    }

    fn attack(&mut self, target: Unit) -> bool {
        let attacker = self.units[self.current_unit.unwrap()];
        let ind = self.units.iter().position(|u| u.position == target.position).unwrap();
        let targ = self.units.get_mut(ind).unwrap();
        targ.hit_points = targ.hit_points.saturating_sub(attacker.attack_power);
        let is_killed = targ.hit_points == 0;
        if is_killed {
            self.map[targ.position.1][targ.position.0] = CaveUnit::Open;
        }

        is_killed
    }

    #[allow(dead_code)]
    fn prt(&self) {
        for (ir, r) in self.map.iter().enumerate() {
            let mut hp = String::new();
            for (ic, c) in r.iter().enumerate() {
                match c {
                    CaveUnit::Wall => common::dbgn("#".to_string(), 3),
                    CaveUnit::Open => common::dbgn(".".to_string(), 3),
                    CaveUnit::Elf => {
                        let ind = self.units.iter().position(|u| u.kind == *c && u.position == (ic, ir) && u.hit_points != 0).unwrap();
                        hp.push_str(format!(" E({})", self.units[ind].hit_points).as_str());
                        common::dbgn("E".to_string(), 3)
                    },
                    CaveUnit::Goblin => {
                        let ind = self.units.iter().position(|u| u.kind == *c && u.position == (ic, ir) && u.hit_points != 0).unwrap();
                        hp.push_str(format!(" G({})", self.units[ind].hit_points).as_str());
                        common::dbgn("G".to_string(), 3)
                    },
                }
            }
            common::dbg(hp, 3);
        }
        common::dbg("".to_string(), 3);
    }

    fn set_elf_power(&mut self, pwr: u8) {
        for u in  self.units.iter_mut() {
            u.set_elf_power(pwr);
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
struct Unit {
    kind: CaveUnit,
    hit_points: u8,
    attack_power: u8,
    position: Position,
}

impl Unit {
    fn new(kind: CaveUnit, row: usize, col: usize) -> Self {
        Self { kind, hit_points: 200, attack_power: 3, position: (col, row) }
    }

    fn set_elf_power(&mut self, pwr: u8) {
        if self.kind == CaveUnit::Elf {
            self.attack_power = pwr;
        }
    }
}

fn first_in_reading_order_positions(pos_a: Position, pos_b: Position) -> Position {
    match pos_a.1.cmp(&pos_b.1) {
        Ordering::Less => pos_a,
        Ordering::Greater => pos_b,
        Ordering::Equal => match pos_a.0 < pos_b.0 {
                              true => pos_a,
                              false => pos_b,
                           },
    }
}

fn first_in_reading_order_units(units: &Units) -> Unit {
    units
        .iter()
        .skip(1)
        .fold(units[0], |small, &other| {
            match small.position.1.cmp(&other.position.1) {
                Ordering::Less => small,
                Ordering::Greater => other,
                Ordering::Equal => if small.position.0 < other.position.0 {
                    small
                } else {
                    other
                },
            }
        })
}

fn run_turns(cave: &Cave, allow_kill_elf: bool) -> Option<usize> {
    let mut cave = cave.clone();
    let mut n_turns = 0;

    loop {
        let unit_o = cave.next_unit();
        if unit_o.is_none() {
            n_turns += 1;
            continue;
        }

        let unit = unit_o.unwrap();
        if unit.hit_points == 0 {
            continue;
        }
        let enemies = cave.identify_enemies(&unit);
        if enemies.is_empty() {
            break Some(n_turns * cave.units.iter().map(|u| u.hit_points as usize).sum::<usize>())
        }

        let get_possible_targets = |unit: &Unit| {
            enemies
            .iter()
            .filter(|e| e.position.0 == unit.position.0 && e.position.1.abs_diff(unit.position.1) == 1 ||
                                e.position.1 == unit.position.1 && e.position.0.abs_diff(unit.position.0) == 1)
            .copied()
            .collect()
        };

        // any enemy within attack distance?
        let mut possible_targets: Units = get_possible_targets(&unit);
        if possible_targets.is_empty() {
            // try to move closer to an enemy
            let open = cave.get_enemies_open_squares(&enemies);
            // choose the square reachable in fewest steps
            let (n_steps, pos) = cave.calc_less_steps_to_squares(unit, &open);
            if n_steps == usize::MAX {
                continue;
            }
            cave.move_unit(pos);
            if n_steps == 1 {
                possible_targets = get_possible_targets(&cave.units[cave.current_unit.unwrap()]);
            }
        }

        if !possible_targets.is_empty() {
            // Find the enemy/ies with lowest hit points
            let (targets, _) =
                possible_targets
                .iter()
                .fold((Vec::new(), u8::MAX), |(mut trg, min_pts), u| {
                    match u.hit_points.cmp(&min_pts) {
                        Ordering::Less => (vec![*u], u.hit_points),
                        Ordering::Equal => {
                            trg.push(*u);
                            (trg, min_pts)
                        },
                        Ordering::Greater => (trg, min_pts),
                    }
                })
            ;

            // If more than one target have the same hit points, select the one first in reading order
            let target = if targets.len() > 1 {
                first_in_reading_order_units(&targets)
            } else {
                targets[0]
            };
            let is_killed = cave.attack(target);
            if !allow_kill_elf && target.kind == CaveUnit::Elf && is_killed {
                return None;
            }
        }
    }
}

fn task_a(lines: Vec<String>) {
    let cave = parse_input(&lines);
    let outcome = run_turns(&cave, true);

    println!("Outcome is: {}", outcome.unwrap());
}

fn task_b(lines: Vec<String>) {
    let mut cave = parse_input(&lines);
    let mut power = (200, 100, 0);

    let mut outcome = None;
    let mut prev_outcome;
    loop {
        cave.set_elf_power(power.1);
        prev_outcome = outcome;
        outcome = run_turns(&cave, false);
        if outcome.is_none() {
            if power.0 - power.1 == 1 {
                outcome = prev_outcome;
                break;
            }
            power.2 = power.1;
            power.1 = power.1 + (power.0 - power.1) / 2;
            if power.1 == power.2 {
                power.1 += 1;
            }
        } else {
            power.0 = power.1;
            power.1 = power.1 - (power.1 - power.2) / 2;
            if power.1 == 0 || power.1 == power.0 {
                break;
            }
        };
    }

    println!("Outcome is: {}", outcome.unwrap());
}

// fn parse_input(lines: &[String]) -> (Cave, Units, Units) {
fn parse_input(lines: &[String]) -> Cave {
    let mut units = Vec::new();

    let map = lines
        .iter()
        .enumerate()
        .map(|(row, line)|
            line
            .char_indices()
            .map(|(col, c)|
                match c {
                    '#' => CaveUnit::Wall,
                    '.' => CaveUnit::Open,
                    'E' => {
                        units.push(Unit::new(CaveUnit::Elf, row, col));
                        CaveUnit::Elf
                    },
                    'G' => {
                        units.push(Unit::new(CaveUnit::Goblin, row, col));
                        CaveUnit::Goblin
                    },
                    _ => panic!(),
            })
            .collect::<Vec<_>>()
        )
        .collect::<Vec<_>>()
    ;

    Cave::new(map, units)
}


#[cfg(test)]
mod tests18_15 {
    #![allow(dead_code, unused_variables, unused_mut, unused_imports, unused_labels, unused_assignments)]
    use super::*;
    use std::io::BufRead;
    use std::io::BufReader;

    fn get_input_from_file(day_num: &str) -> Vec<String> {
        let filename = format!("/home/ingemar/adventofcode/aoc2018/input/day-{}.txt", day_num);
        let file = std::fs::File::open(&filename).unwrap_or_else(|err| {
            eprintln!("Cannot open file '{}': {}", filename, err);
            std::process::exit(1);
        });

        let reader = BufReader::new(&file);
        let lines: Vec<String> = reader.lines().collect::<Result<_, _>>().unwrap();

        lines
    }

    #[test]
    fn tests18_15_t01() {
        let lines = &get_input_from_file("15");
        let mut cave = parse_input(lines);

        assert_eq!(cave.current_unit, None);
        cave.next_unit();
        assert_eq!(cave.current_unit, Some(0));
        cave.current_unit = Some(cave.units.len()-2);
        cave.next_unit();
        assert_eq!(cave.current_unit, Some(cave.units.len()-1));
        cave.next_unit();
        assert_eq!(cave.current_unit, None);
        cave.next_unit();
        assert_eq!(cave.current_unit, Some(0));
    }

    #[test]
    fn tests18_15_t02() {
        let lines = &get_input_from_file("15");
        let cave = parse_input(lines);

        let nbrs = cave.neighbor_open_positions(&(7, 3));
        assert_eq!(nbrs, vec![(7, 2), (6, 3), (8, 3), (7, 4)]);
        let nbrs = cave.neighbor_open_positions(&(13, 12));
        assert_eq!(nbrs, vec![(13, 11), ]);
    }

    #[test]
    fn tests18_15_t03() {
        use CaveUnit::*;
        let lines = &get_input_from_file("15");
        let mut cave = parse_input(lines);

        let enemies = cave.identify_enemies(&cave.units[0]);
        assert_eq!(enemies, vec![Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (21, 10) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (9, 11) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (20, 12) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (24, 13) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (3, 20) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (26, 21) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (23, 22) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (30, 22) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (10, 23) }, Unit { kind: Elf, hit_points: 200, attack_power: 3, position: (7, 25) }]);
        let enemies = cave.identify_enemies(&cave.units[10]);
        assert_eq!(enemies, vec![Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (21, 1) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (7, 3) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (9, 3) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (5, 4) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (13, 5) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (23, 5) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (9, 6) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (17, 8) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (9, 9) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (15, 10) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (8, 11) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (12, 12) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (12, 13) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (9, 14) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (7, 16) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (22, 18) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (8, 19) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (12, 21) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (2, 26) }, Unit { kind: Goblin, hit_points: 200, attack_power: 3, position: (4, 26) }]);
    }

    #[test]
    fn tests18_15_t04() {
        let lines = &get_input_from_file("15");
        let cave = parse_input(lines);

        let enemies = cave.identify_enemies(&cave.units[0]);
        let open_sq = cave.get_enemies_open_squares(&enemies);
        assert_eq!(open_sq, vec![(21, 9), (20, 10), (22, 10), (21, 11), (9, 10), (10, 11), (9, 12), (20, 11), (19, 12), (21, 12), (20, 13), (24, 12), (23, 13), (25, 13), (24, 14), (3, 19), (2, 20), (4, 20), (3, 21), (26, 20), (25, 21), (27, 21), (26, 22), (23, 21), (22, 22), (24, 22), (23, 23), (30, 21), (29, 22), (30, 23), (10, 22), (9, 23), (7, 24), (6, 25), (8, 25), (7, 26)]);
    }

    #[test]
    fn tests18_15_t05() {
        for (p1, p2, res) in [((5,5), (5,4), (5,4)), ((5,5), (5,6), (5,5)), ((5,5), (4,5), (4,5)), ((5,5), (6,5), (5,5)), ] {
            let f = first_in_reading_order_positions(p1, p2);
            assert_eq!(f, res);
        }
    }

    #[test]
    fn tests18_15_t06() {
        let lines = &get_input_from_file("15");
        let cave = parse_input(lines);

        for (start, goal) in [((21, 10), (18, 8)), ((4, 26), (6, 25)), ((12, 12), (19, 12)), ((22, 18), (29, 22))] {
            let path = cave.a_star(start, goal, 999);
            println!("{:?} -> {:?}: path = {:?}", start, goal, path);
        }
    }
}

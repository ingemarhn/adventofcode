use std::convert::TryInto;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Clone, Copy, Debug)]
struct Nanobot {
    position: [isize; 3],
    radius: usize,
}

fn calc_bots_in_range(bots: &[Nanobot]) -> usize {
    let strong_bot =
        bots
        .iter()
        .fold(Nanobot {position: [0; 3], radius: 0}, |acc, v| {
            if v.radius > acc.radius {
                *v
            } else {
                acc
            }
        });

    bots
        .iter()
        .filter(|b|
            (strong_bot.position[0] - b.position[0]).abs() +
            (strong_bot.position[1] - b.position[1]).abs() +
            (strong_bot.position[2] - b.position[2]).abs()
            <=
            strong_bot.radius as isize
        )
        .count()
}

fn get_min_max(bots: &[Nanobot]) -> ([isize; 3], [isize; 3]) {
    let mut min = [isize::MAX; 3];
    let mut max = [isize::MIN; 3];

    for bot in bots {
        for i in 0 .. 3 {
            min[i] = min[i].min(bot.position[i] - bot.radius as isize);
            max[i] = max[i].max(bot.position[i] + bot.radius as isize);
        }
    }

    (min, max)
}

fn find_best_pos_distance(bots: &[Nanobot], (min, max): ([isize; 3], [isize; 3])) -> isize {
    let get_longest_side = |min: [isize; 3], max: [isize; 3]| {
        let mut longest_side = (0, 0);
        for i in 0 .. 3 {
            let length = max[i] - min[i] + 1;
            if length > longest_side.0 {
                longest_side = (length, i);
            }
        }
        longest_side
    };

    let mut boxes = vec![(min, max)];

    let distance = loop {
        let mut new_boxes = Vec::new();
        for (mut box_min, mut box_max) in boxes.iter() {
            let longest_side = get_longest_side(box_min, box_max);

            let bmin = box_min[longest_side.1];
            let half_dist = longest_side.0 / 2;
            box_min[longest_side.1] += half_dist;
            new_boxes.push((box_min, box_max));
            box_min[longest_side.1] = bmin;
            box_max[longest_side.1] -= half_dist;
            new_boxes.push((box_min, box_max));
        }

        boxes = new_boxes;
        new_boxes = Vec::new();
        let mut max_bots = 0;
        for (box_min, box_max) in boxes.iter() {
            let n_bots = bots
                .iter()
                .filter(|b| {
                    let [x, y, z] = b.position;
                    (box_min[0] <= x && x <= box_max[0] &&
                     box_min[1] <= y && y <= box_max[1] &&
                     box_min[2] <= z && z <= box_max[2]) ||
                    {
                        let mut reaches = false;
                        let [mi_x, mi_y, mi_z] = *box_min;
                        let [ma_x, ma_y, ma_z] = *box_max;
                        for corner in [*box_min, [ma_x, mi_y, mi_z], [mi_x, ma_y, mi_z], [ma_x, ma_y, mi_z], [mi_x, mi_y, ma_z], [ma_x, mi_y, ma_z], [mi_x, ma_y, ma_z], *box_max] {
                            if (corner[0] - b.position[0]).abs() + (corner[1] - b.position[1]).abs() + (corner[2] - b.position[2]).abs() <= b.radius as isize {
                                reaches = true;
                                break;
                            }
                        }

                        reaches
                    }
                })
                .count();

            #[allow(clippy::comparison_chain)]
            if n_bots > max_bots {
                max_bots = n_bots;
                new_boxes = vec![(*box_min, *box_max)];
            } else if n_bots == max_bots {
                new_boxes.push((*box_min, *box_max));
            }
        }
        boxes = new_boxes;

        let long_side =
            boxes
            .iter()
            .fold(0, |ls, b| {
                let (long, _) = get_longest_side(b.0, b.1);
                ls.max(long)
            });

        if long_side == 1 {
            let distance =
                boxes
                .iter()
                .fold(isize::MAX, |dist, (b, _)| {
                    dist.min(b.iter().sum())
                });
            break distance
        }
    };

    distance
}

fn task_a(lines: Vec<String>) {
    let bots = parse_input(&lines);
    let n_bots = calc_bots_in_range(&bots);

    println!("# of bots in range: {}", n_bots);
}

fn task_b(lines: Vec<String>) {
    let bots = parse_input(&lines);
    let mm = get_min_max(&bots);
    let dist = find_best_pos_distance(&bots, mm);

    println!("distance: {}", dist);
}

fn parse_input(lines: &[String]) -> Vec<Nanobot> {
    lines
        .iter()
        .map(|line|
            line
            .split(['<', ',', '>', ' ', '='])
            .filter_map(|s| s.parse::<isize>().ok())
            .collect::<Vec<_>>()
        )
        .map(|v| Nanobot { position: v[0 .. 3].try_into().unwrap(), radius: v[3] as usize } )
        .collect::<Vec<_>>()
}

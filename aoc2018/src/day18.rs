pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Acre {
    Open,
    Trees,
    Lumberyard,
}

#[derive(Clone, Debug)]
struct LumberCollectionArea(Vec<Vec<Acre>>);

impl LumberCollectionArea {
}

fn get_neighbors(ir: usize, ic: usize) -> [(usize, usize); 8] {
    [(ir - 1, ic - 1), (ir - 1, ic), (ir - 1, ic + 1), (ir, ic - 1), (ir, ic + 1), (ir + 1, ic - 1), (ir + 1, ic), (ir + 1, ic + 1)]
}

fn check_open_or_trees(collection: &LumberCollectionArea, ir: usize, ic: usize, to_become: Acre) -> Option<Acre> {
    let mut count_to_become = 0;
    for nbr in get_neighbors(ir, ic) {
        if collection.0[nbr.0][nbr.1] == to_become {
            count_to_become += 1;
            if count_to_become == 3 {
                return Some(to_become);
            }
        }
    }

    None
}

fn check_lumberyard(collection: &LumberCollectionArea, ir: usize, ic: usize) -> Option<Acre> {
    let mut found_lyd = false;
    let mut found_trees = false;
    for nbr in get_neighbors(ir, ic) {
        match collection.0[nbr.0][nbr.1] {
            Acre::Open => (),
            Acre::Trees => found_trees = true,
            Acre::Lumberyard => found_lyd = true,
        }

        if found_lyd && found_trees {
            return None;
        }
    }

    Some(Acre::Open)
}

fn count_acres(lumber_collection: &LumberCollectionArea) -> (usize, usize, usize) {
    let mut counter = (0, 0, 0);
    for n_row in 1 ..= lumber_collection.0.len() - 2 {
        for n_col in 1 ..= lumber_collection.0[0].len() - 2 {
            match lumber_collection.0[n_row][n_col] {
                Acre::Open => counter.0 += 1,
                Acre::Trees => counter.1 += 1,
                Acre::Lumberyard => counter.2 += 1,
            }
        }
    }

    counter
}

fn detect_pattern(patterns: &[(usize, usize, usize)]) -> Option<(usize, usize)> {
    let test_pattern = patterns.last().unwrap();
    let ind_o = patterns.iter().take(patterns.len()-1).rposition(|e| e == test_pattern);
    let mut ind = ind_o?;

    let segment_len = patterns.len() - ind - 1;
    if segment_len > ind ||
        &patterns[ind - segment_len] != test_pattern
    {
        return None;
    }

    ind -= 1;
    while patterns[ind] == patterns[ind + segment_len] {
        ind -= 1;
    }

    Some((ind + 1, segment_len))
}

fn update_landscape(lumber_collection_in: &LumberCollectionArea, n_minutes: usize) -> (usize, usize, usize) {
    let mut lumber_collection = lumber_collection_in.clone();
    let n_rows = lumber_collection_in.0.len() - 2;
    let n_cols = lumber_collection_in.0[0].len() - 2;
    let mut patterns = Vec::new();

    for min in 0 .. n_minutes {
        let mut next_collection = lumber_collection.clone();
        for n_row in 1 ..= n_rows {
            for n_col in 1 ..= n_cols {
                let next = match lumber_collection.0[n_row][n_col] {
                    Acre::Open => check_open_or_trees(&lumber_collection, n_row, n_col, Acre::Trees),
                    Acre::Trees => check_open_or_trees(&lumber_collection, n_row, n_col, Acre::Lumberyard),
                    Acre::Lumberyard => check_lumberyard(&lumber_collection, n_row, n_col),
                };

                if let Some(nxt_a) = next {
                    next_collection.0[n_row][n_col] = nxt_a;
                }
            }
        }

        lumber_collection = next_collection;

        patterns.push(count_acres(&lumber_collection));
        if min % 100 == 0 {
            let ind_o = detect_pattern(&patterns);
            if let Some((index, len)) = ind_o {
                let n_of_segment = (n_minutes - index) % len;
                return patterns[index + n_of_segment - 1];
            }
        }
    }

    *patterns.last().unwrap()
}

fn task_a(lines: Vec<String>) {
    let lumber_coll = parse_input(&lines);
    let (_no, nt, nl) = update_landscape(&lumber_coll, 10);
    println!("Resource value = {}", nt * nl);
}

fn task_b(lines: Vec<String>) {
    let lumber_coll = parse_input(&lines);
    let (_no, nt, nl) = update_landscape(&lumber_coll, 1000000000);
    println!("Resource value = {}", nt * nl);
}

fn parse_input(lines: &[String]) -> LumberCollectionArea {
    let mut lumber_collection = LumberCollectionArea(Vec::new());

    for _ in 0 .. lines.len() + 2 {
        lumber_collection.0.push(vec![Acre::Open; lines[0].len() + 2]);
    }

    for (row, line) in lines.iter().enumerate() {
        for (col, c) in line.char_indices() {
            match c {
                '|' => lumber_collection.0[row + 1][col + 1] = Acre::Trees,
                '#' => lumber_collection.0[row + 1][col + 1] = Acre::Lumberyard,
                _ => (),
            }
        }
    }

    lumber_collection
}

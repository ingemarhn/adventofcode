pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

#[derive(Debug, Hash, PartialEq, Eq)]
struct PointData {
    column: isize,
    row: isize,
}

#[derive(Debug, Hash, PartialEq, Eq)]
struct Point {
    position: PointData,
    velocity: PointData,
}

#[allow(clippy::type_complexity)]
fn solve(lines: &[String]) -> (isize, (isize, isize, isize, isize), Vec<(isize, isize)>) {
    let is_sample = lines.len() == 31;
    let points = parse_input(lines);

    let mut c_diff = (0,-99999);
    for p in points.iter() {
        if p.velocity.column != 0 {
            let div = p.position.column / p.velocity.column;
            c_diff.0 = c_diff.0.min(div);
            c_diff.1 = c_diff.1.max(div);
        }
    }
    if !is_sample {
        let cd = (c_diff.0.abs(), c_diff.1.abs());
        c_diff = (cd.0.min(cd.1), cd.0.max(cd.1));
    } else {
        c_diff = (0, c_diff.1)
    }

    let mut col_diff = 9999999;
    let mut use_n = 0;
    let mut use_positions = Vec::new();
    let mut col_row_min_max = (0, 0, 0, 0, );

    for n in c_diff.0 .. c_diff.1 {
        let mut col_min = 999999_isize;
        let mut col_max = -999999;
        let mut row_min = 999999_isize;
        let mut row_max = -999999;
        let positions = {
            let mut posns = Vec::new();
            for pos in points.iter() {
                let col = pos.position.column + n * pos.velocity.column;
                let row = pos.position.row + n * pos.velocity.row;
                col_min = col_min.min(col);
                col_max = col_max.max(col);
                row_min = row_min.min(row);
                row_max = row_max.max(row);
                posns.push((col, row));
            }

            posns
        };
        let colu_diff = col_max - col_min + 1;
        if colu_diff < col_diff {
            col_diff = colu_diff;
            col_row_min_max = (col_min, col_max, row_min, row_max);
            use_n = n;
            use_positions = positions;
        }
    }

    (use_n, col_row_min_max, use_positions)
}

fn task_a(lines: Vec<String>) {
    let (_, col_row_min_max, positions) = solve(&lines);
    let row_templ = vec!['.'; (col_row_min_max.1 - col_row_min_max.0 + 1) as usize];
    let mut message = vec![row_templ; (col_row_min_max.3 - col_row_min_max.2 + 1) as usize];

    for &(col, row) in positions.iter() {
        message[(row - col_row_min_max.2) as usize][(col - col_row_min_max.0) as usize] = '#';
    }

    for row in message.iter() {
        println!("{}", row.iter().collect::<String>());
    }
}

fn task_b(lines: Vec<String>) {
    let (n, _, _) = solve(&lines);

    println!("Seconds needed: {n}");
}

fn parse_input(lines: &[String]) -> Vec<Point> {
    lines
        .iter()
        .map(|line|
            line
            .split(&['<', '>', ','])
            .filter_map(|s| s.trim().parse().ok())
            .collect::<Vec<_>>()
        )
        .map(|point_vals|
            Point {
                position: PointData { column: point_vals[0], row: point_vals[1] },
                velocity: PointData { column: point_vals[2], row: point_vals[3] },
            }
        )
        .collect::<Vec<_>>()
}

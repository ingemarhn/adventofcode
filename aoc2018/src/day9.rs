use std::collections::VecDeque;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn solve(n_players: u16, value_last_marble: u32) {
    let marbles_part_approx_max_len = (value_last_marble.max(196) as f32).sqrt() as u32;
    let mut marbles = Vec::new();
    marbles.push(VecDeque::from(vec![0, 1]));
    let mut marbles_vec_index = 0;

    let mut players = VecDeque::from(vec![0; n_players as usize]);
    let mut current_marble = 1;

    for mrbl in 2 ..= value_last_marble {
        let player_index = (mrbl % n_players as u32) as usize;
        let mut mrbls_part_len = marbles[marbles_vec_index].len();

        if mrbl % 23 != 0 {
            current_marble += 2;

            if mrbls_part_len > marbles_part_approx_max_len as usize {
                // split in two
                let half2 = marbles[marbles_vec_index].split_off(mrbls_part_len / 2);
                marbles.insert(marbles_vec_index + 1, half2);
                mrbls_part_len = marbles[marbles_vec_index].len();
            }
            if current_marble >= mrbls_part_len {
                // move to next part
                current_marble -= marbles[marbles_vec_index].len();
                marbles_vec_index += 1;
                if marbles_vec_index >= marbles.len() {
                    marbles_vec_index = 0;
                }
            }

            marbles[marbles_vec_index].insert(current_marble, mrbl);
        } else {
            current_marble = match current_marble {
                7.. => current_marble,
                _   => {
                    // go to prev part which might be the last one
                    marbles_vec_index = match marbles_vec_index {
                        0 => marbles.len(),
                        _ => marbles_vec_index,
                    } - 1;

                    current_marble +  marbles[marbles_vec_index].len()
                }
            } - 7;

            let mrbl_val = marbles[marbles_vec_index].remove(current_marble).unwrap();
            players[player_index] += mrbl + mrbl_val;
        }
    }

    println!("Winner score = {}", players.iter().max().unwrap());
}

fn task_a(lines: Vec<String>) {
    let (n_players, value_last_marble) = parse_input(&lines);

    solve(n_players, value_last_marble);
}

fn task_b(lines: Vec<String>) {
    let (n_players, value_last_marble) = parse_input(&lines);

    solve(n_players, value_last_marble * 100);
}

fn parse_input(lines: &[String]) -> (u16, u32) {
    let mut line_it = lines[0].split_ascii_whitespace();
    let n_plyrs = line_it.next().unwrap().parse().unwrap();
    let marble_val = line_it.nth(5).unwrap().parse().unwrap();

    (n_plyrs, marble_val)
}

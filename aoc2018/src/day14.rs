use std::fmt::Write;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn calc_recipes(n_recipes: usize) -> String {
    let mut recipes = vec![3, 7];
    let mut elves = [0, 1];

    while recipes.len() < n_recipes + 10 {
        let rec_sum = recipes[elves[0]] + recipes[elves[1]];
        if rec_sum > 9 {
            recipes.push(1);
        }
        recipes.push(rec_sum % 10);
        elves[0] = (elves[0] + recipes[elves[0]] + 1) % recipes.len();
        elves[1] = (elves[1] + recipes[elves[1]] + 1) % recipes.len();
    }

    // recipes[n_recipes .. n_recipes + 10].iter().map(|&r| format!("{r}")).collect()
    recipes[n_recipes .. n_recipes + 10].iter().fold(String::new(), |mut buff, &r| {
        write!(buff, "{r}").unwrap();
        buff
    })
}

fn get_n_recipes(score_seq: usize) -> usize {
    let mut recipes = vec![3, 7];
    let mut elves = [0, 1];
    let (_, score_sequence) = (0 ..= score_seq.ilog10())
        .fold((score_seq, Vec::new()), |(num, mut v), _| {
            v.insert(0, num % 10);
            (num / 10, v)
        })
    ;

    let score_seq_check = |ind: &mut usize, dig| {
        if score_sequence[*ind] == dig {
            *ind += 1;
        } else {
            *ind = 0;
        }
    };

    let mut sc_sq_index = 0;
    loop {
        let rec_sum = recipes[elves[0]] + recipes[elves[1]];
        if rec_sum > 9 {
            recipes.push(1);
            score_seq_check(&mut sc_sq_index, 1);
            if sc_sq_index == score_sequence.len() {
                break;
            }
        }
        let dig = rec_sum % 10;
        recipes.push(dig);
        score_seq_check(&mut sc_sq_index, dig);
        if sc_sq_index == score_sequence.len() {
            break;
        }
        elves[0] = (elves[0] + recipes[elves[0]] + 1) % recipes.len();
        elves[1] = (elves[1] + recipes[elves[1]] + 1) % recipes.len();
    }

    recipes.len() - score_sequence.len()
}

fn task_a(lines: Vec<String>) {
    let n_recipes = parse_input(&lines);

    let score = calc_recipes(n_recipes);

    println!("Score is {}", score);
}

fn task_b(lines: Vec<String>) {
    let score_seq = parse_input(&lines);

    let n_recip = get_n_recipes(score_seq);

    println!("# of recipes: {}", n_recip);
}

fn parse_input(lines: &[String]) -> usize {
    lines[0].parse().unwrap()
}

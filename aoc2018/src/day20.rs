use std::collections::HashMap;
use std::iter::Peekable;
use std::slice::Iter;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Pos = i32;
type Position = (Pos, Pos);
type Path = Vec<PathElem>;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum PathElem {
    RelativePos(Pos, Pos),
    StartPath,
    EndPath,
    StartBranch,
    SwitchBranch,
    EndBranch,
    StartDeadBranch,
    EndDeadBranch,
}

fn walk_path(path: &[PathElem]) -> HashMap<(i32, i32), (u32, u32)> {
    let mut path_it = path.iter().peekable();
    let mut prev_pos = (0, 0);
    let mut branch_ends = HashMap::new();
    branch_ends.insert(prev_pos, (0, 0));

    while path_it.peek() != Some(&&PathElem::EndPath) {
        match path_it.peek().unwrap() {
            PathElem::StartPath => {
                path_it.next();
            },
            PathElem::RelativePos(_, _) => {
                let ret = walk_branch(&mut path_it, &mut branch_ends, prev_pos);
                prev_pos = ret.1;
            },
            PathElem::StartDeadBranch => {
                path_it.next();
                walk_dead_branch(&mut path_it, &mut branch_ends, prev_pos);
            },
            _ => panic!("{:?}", path_it.next())
        }
    }

    branch_ends
}

fn walk_branch(path_iter: &mut Peekable<Iter<PathElem>>, branch_ends: &mut HashMap<Position, (u32, u32)>, prev_pos: Position) -> (u32, Position) {
    let mut branch_len = 0;
    let mut current_pos = prev_pos;

    let mut tot_len = branch_ends[&prev_pos].1;
    loop {
        while let Some(PathElem::RelativePos(ry, rx)) = path_iter.next_if(|&p| matches!(p, PathElem::RelativePos(_, _))) {
            current_pos = (current_pos.0 + ry, current_pos.1 + rx);
            branch_len += 1;
            tot_len += 1;
            branch_ends
                .entry(current_pos)
                .and_modify(|be| if be.1 > tot_len { *be = (branch_len, tot_len) })
                .or_insert((branch_len, tot_len));
        }

        let next = path_iter.peek();
        if next == Some(&&PathElem::StartBranch) {
            let ret = walk_branches(path_iter, branch_ends, current_pos);
            branch_len += ret.0;
            current_pos = ret.1;
        } else if next == Some(&&PathElem::StartDeadBranch) {
            path_iter.next();
            walk_dead_branch(path_iter, branch_ends, current_pos);
            continue;
        }

        break;
    }

    (branch_len, current_pos)
}

fn walk_dead_branch(path_iter: &mut Peekable<Iter<PathElem>>, branch_ends: &mut HashMap<Position, (u32, u32)>, prev_pos: Position) {
    let mut branch_len = 0;
    let mut current_pos = prev_pos;

    let mut tot_len = branch_ends[&prev_pos].1;
    while let Some(PathElem::RelativePos(ry, rx)) = path_iter.next_if(|&p| matches!(p, PathElem::RelativePos(_, _))) {
        current_pos = (current_pos.0 + ry, current_pos.1 + rx);
        branch_len += 1;
        tot_len += 1;
        branch_ends
            .entry(current_pos)
            .and_modify(|be| if be.1 > tot_len { *be = (branch_len, tot_len) }
            )
            .or_insert((branch_len, tot_len));
    }

    path_iter.next();
    path_iter.next();
}

fn walk_branches(path_iter: &mut Peekable<Iter<PathElem>>, branch_ends: &mut HashMap<Position, (u32, u32)>, prev_pos: Position) -> (u32, Position) {
    let mut min_branch_len = u32::MAX;
    let mut position_for_min_len = (0, 0);

    loop {
        match path_iter.peek().unwrap() {
            PathElem::StartBranch |
            PathElem::SwitchBranch => {
                path_iter.next();
                let (br_len, br_pos) = walk_branch(path_iter, branch_ends, prev_pos);
                if br_len < min_branch_len {
                    min_branch_len = br_len;
                    position_for_min_len = br_pos;
                }
            },
            PathElem::RelativePos(_, _) => {
                break;
            },
            PathElem::EndBranch => {
                path_iter.next();
                break;
            },
            _ => panic!("{:?}", path_iter.next())
        }
    }

    (min_branch_len, position_for_min_len)
}

fn task_a(lines: Vec<String>) {
    let path = parse_input(&lines);
    let branch_lens = walk_path(&path);

    println!("Number of doors to pass = {}", branch_lens.iter().max_by(|a, b| a.1.1.cmp(&b.1.1)).unwrap().1.1);
}

fn task_b(lines: Vec<String>) {
    let path = parse_input(&lines);
    let branch_lens = walk_path(&path);

    let thousand_doors =
        branch_lens
        .iter()
        .filter(|(_, &v)| v.1 >= 1000);

    println!("Number of doors to pass = {}", thousand_doors.count());
}

fn parse_input(lines: &[String]) -> Path {
    use regex::{Captures, Regex};

    let re = Regex::new(r"\(([WESN]+\|)\)").unwrap();
    let line = re.replace_all(&lines[0], |caps: &Captures| {
        format!("[{}]", &caps[1])
    });
    line
        .chars()
        .map(|c| match c {
            'N' => PathElem::RelativePos(-1, 0),
            'E' => PathElem::RelativePos(0, 1),
            'S' => PathElem::RelativePos(1, 0),
            'W' => PathElem::RelativePos(0, -1),
            '(' => PathElem::StartBranch,
            '|' => PathElem::SwitchBranch,
            ')' => PathElem::EndBranch,
            '[' => PathElem::StartDeadBranch,
            ']' => PathElem::EndDeadBranch,
            '^' => PathElem::StartPath,
            '$' => PathElem::EndPath,
            _ => panic!()
        })
        .collect()
}

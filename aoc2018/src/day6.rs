use std::collections::{HashMap, HashSet};
use itertools::Itertools;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    use std::cmp::Ordering;
    use std::collections::HashMap;

    type CoordIdx = u16;
    pub type Coordinate = (CoordIdx, CoordIdx);
    pub type Coordinates = Vec<Coordinate>;

    #[derive(Clone, Debug)]
    pub enum CS {
        NotSet,
        Closest(CoordIdx, usize),
        IsDraw(CoordIdx),
    }

    impl CS {
        pub fn new() -> Self {
            Self::NotSet
        }

        pub fn set_closest(&mut self, dist: CoordIdx, coord_index: usize) -> bool {
            match self {
                Self::NotSet => {
                    *self = Self::Closest(dist, coord_index);
                    true
                },
                Self::Closest(d, _) |
                Self::IsDraw(d) =>
                    match dist.cmp(d) {
                        Ordering::Less => {
                            *self = Self::Closest(dist, coord_index);
                            true
                        },
                        Ordering::Equal => {
                            *self = CS::IsDraw(dist);
                            true
                        },
                        Ordering::Greater => false,
                    },
            }
        }

        pub fn get_closest_idx(&self) -> Option<usize> {
            match self {
                Self::NotSet => None,
                Self::Closest(_, idx) => Some(*idx),
                Self::IsDraw(_) => None,
            }
        }
    }

    pub fn get_min_max(coords: &Coordinates) -> (CoordIdx, CoordIdx, CoordIdx, CoordIdx, ) {
        let mut mi_ma = [9999, 0, 9999, 0, ];
        for c in coords {
            if c.0 < mi_ma[0] { mi_ma[0] = c.0 }
            if c.0 > mi_ma[1] { mi_ma[1] = c.0 }
            if c.1 < mi_ma[2] { mi_ma[2] = c.1 }
            if c.1 > mi_ma[3] { mi_ma[3] = c.1 }
        }

        (mi_ma[0], mi_ma[1], mi_ma[2], mi_ma[3], )
    }

    pub fn get_tot_area(min_x: CoordIdx, max_x: CoordIdx, min_y: CoordIdx, max_y: CoordIdx) -> HashMap<Coordinate, CS> {
        let mut t_a = HashMap::new();

        for x in min_x .. max_x {
            for y in min_y .. max_y {
                t_a.insert((x, y), CS::new());
            }
        }
        t_a
    }

    pub fn calc_distance(c1: &Coordinate, c2: &Coordinate) -> CoordIdx {
        c1.0.abs_diff(c2.0) + c1.1.abs_diff(c2.1)
    }
}
use internal::*;

fn task_a(lines: Vec<String>) {
    let coordinates = parse_input(&lines);

    let (min_x, max_x, min_y, max_y) = get_min_max(&coordinates);

    let mut tot_area = get_tot_area(min_x, max_x, min_y, max_y);

    for (i, coord) in coordinates.iter().enumerate() {
        for k in tot_area.clone().keys() {
            let dist = calc_distance(coord, k);
            let e = tot_area.entry(*k);
            e.and_modify(|cs| { cs.set_closest(dist, i); });
        }
    }

    let mut areas = HashMap::new();
    let mut areas_inf = HashSet::new();
    for k in tot_area.keys() {
        let close = &tot_area[k];
        if let Some(ind) = close.get_closest_idx() {
            if [min_x, max_x].contains(&k.0) || [min_y, max_y].contains(&k.1) {
                areas_inf.insert(ind);
                continue;
            }

            areas.entry(ind).and_modify(|v| *v += 1).or_insert(1);
        }
    }

    let max =
        areas
        .iter()
        .fold(0, |m, k| {
            if !areas_inf.contains(k.0) && *k.1 > m {
                *k.1
            } else {
                m
            }
        });

    println!("Largest area = {}", max);
}

fn task_b(lines: Vec<String>) {
    let coordinates = parse_input(&lines);

    let (min_x, max_x, min_y, max_y) = get_min_max(&coordinates);

    let dist_lim = 10000;
    let mut locations = HashSet::new();
    for x in min_x ..= max_x {
        'yl: for y in min_y ..= max_y {
            let mut distance = 0;
            let loc = (x, y);

            for coord in coordinates.iter() {
                distance += calc_distance(&loc, coord);
                if distance > dist_lim {
                    continue 'yl;
                }
            }

            locations.insert(loc);
        }
    }

    println!("Region size = {}", locations.len());
}

fn parse_input(lines: &[String]) -> Coordinates {
    lines
        .iter()
        .map(|line|
            line
            .split(", ")
            .map(|p_elem|
                p_elem
                .parse()
                .unwrap()
            )
            .collect_tuple().unwrap()
        )
        .collect()
}

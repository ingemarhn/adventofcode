pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

type Matrix = Vec<Vec<Square>>;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum SquareType {
    Sand,
    Clay,
    WaterResting,
    WaterPassed,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct Square(SquareType);

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum RunStatus {
    CanRun,
    HitWall,
}

fn run_water(spring: (usize, usize), matrix: &mut Matrix) {
    let (mut y, x) = spring;
    matrix[y][x].0 = SquareType::WaterPassed;

    while matrix[y + 1][x].0 == SquareType::Sand {
        y += 1;
        matrix[y][x].0 = SquareType::WaterPassed;

        if y == matrix.len() - 1 {
            return;
        }
    }

    if matrix[y + 1][x].0 == SquareType::WaterPassed {
        return;
    }

    let mut walk_sideways = |d, xx: &mut usize| {
        loop {
            let xxp = xx.saturating_add_signed(d);
            if matrix[y][xxp].0 == SquareType::Clay {
                break RunStatus::HitWall
            }

            *xx = xxp;

            matrix[y][xxp].0 = SquareType::WaterPassed;
            if matrix[y + 1][xxp].0 == SquareType::Sand {
                break RunStatus::CanRun
            }
        }
    };

    // Split left and right
    let mut lx = x;
    let mut rx = x;
    let l_stat = walk_sideways(-1, &mut lx);
    let r_stat = walk_sideways( 1, &mut rx);

    if [l_stat, r_stat].iter().all(|&e| e == RunStatus::HitWall) {
        for xp in lx ..= rx {
            matrix[y][xp].0 = SquareType::WaterResting;
        }
        run_water((y - 1, x), matrix);
    } else {
        if l_stat == RunStatus::CanRun {
            run_water((y, lx), matrix);
        }
        // We might be inside a can and the left run has already filled it with water
        if r_stat == RunStatus::CanRun && matrix[y][rx].0 != SquareType::WaterResting {
            run_water((y, rx), matrix);
        }
    }
}

fn task_a(lines: Vec<String>) {
    let (mut matrix, spring_x) = parse_input(&lines);
    run_water((0, spring_x), &mut matrix);

    let mut water_run_count = 0;
    let mut water_rest_count = 0;
    for y in matrix.iter() {
        for x in y.iter() {
            match x.0 {
                SquareType::WaterPassed  => water_run_count += 1,
                SquareType::WaterResting => water_rest_count += 1,
                _ => (),
            }
        }
    }

    println!("Water passes {water_run_count} tiles");
    println!("Water rests in {water_rest_count} tiles");
    println!("Water can reach {} tiles", water_run_count + water_rest_count);
}

fn task_b(lines: Vec<String>) {
    // Task B is solved in A
    let _ = parse_input(&lines);
}

fn parse_input(lines: &[String]) -> (Matrix, usize) {
    let mut clay_x = Vec::new();
    let mut clay_y = Vec::new();
    let mut min_max = (usize::MAX, 0, usize::MAX, 0);
    let mut hlp = |c, v1, v2| {
        let clay_r = match c {
            "x" => {
                min_max.0 = min_max.0.min(v1);
                min_max.1 = min_max.1.max(v2);
                &mut clay_x
            },
            _   => {
                min_max.2 = min_max.2.min(v1);
                min_max.3 = min_max.3.max(v2);
                &mut clay_y
            }
        };
        clay_r.push(v1 ..= v2);
    };

    for line in lines {
        let mut elems_it = line
            .split(['=', ',', ' ', '.']);
        let c = elems_it.next().unwrap();
        let v: usize = elems_it.next().unwrap().parse().unwrap();
        hlp(c, v, v);

        elems_it.next();
        let c = elems_it.next().unwrap();
        let v1: usize = elems_it.next().unwrap().parse().unwrap();
        elems_it.next();
        let v2: usize = elems_it.next().unwrap().parse().unwrap();
        hlp(c, v1, v2);
    }

    let mut matrix = Vec::new();
    for _ in min_max.2 ..= min_max.3 {
        matrix.push(vec![Square(SquareType::Sand); min_max.1 - min_max.0 + 3])
    }

    for i in 0 .. clay_x.len() {
        for y in clay_y[i].clone() {
            for x in clay_x[i].clone() {
                matrix[y - min_max.2][x - min_max.0 + 1].0 = SquareType::Clay;
            }
        }
    }

    (matrix, 500 - min_max.0 + 1)
}

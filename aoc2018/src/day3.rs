use std::collections::HashMap;

use regex::Regex;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

mod internal {
    type CoordVal = usize;
    type Position = (CoordVal, CoordVal);
    pub type Claims = Vec<Claim>;

    #[derive(Clone, Copy, Debug)]
    pub struct Claim {
        id: u32,
        start_x: CoordVal,
        _start_y: CoordVal,
        width: CoordVal,
        _height: CoordVal,
        last_row: CoordVal,
        iter_x: CoordVal,
        iter_y: CoordVal,
    }

    impl Claim {
        pub fn new(id: u32, x: CoordVal, y: CoordVal, w: CoordVal, h: CoordVal) -> Self {
            Self { id, start_x: x, _start_y: y, width: w, _height: h, last_row: y + h - 1, iter_x: x, iter_y: y }
        }

        pub fn get_id(&self) -> u32 {
            self.id
        }
    }

    impl Iterator for Claim {
        type Item = Position;

        fn next(&mut self) -> Option<Self::Item> {
            // iter_x and iter_y are positioned at a coordinate, initially top left corner of the claim
            // Create a return value based on current position, then move to next (regardless if it's outside the claim or not)
            let ret = if self.iter_y <= self.last_row {
                Some((self.iter_x, self.iter_y))
            } else {
                None
            };

            self.iter_x += 1;
            if self.iter_x == self.start_x + self.width {
                self.iter_x = self.start_x;
                self.iter_y += 1;
            }

            ret
        }
    }
}
use internal::*;

fn task_a(lines: Vec<String>) {
    let claims = parse_input(&lines);

    let mut fabric = HashMap::new();
    for cl in claims {
        for clp in cl {
            fabric.entry(clp).and_modify(|counter| *counter += 1).or_insert(1);
        }
    }

    let count =
        fabric
        .iter()
        .fold(0, |count, (_, &val)| count + match val { 2..  => 1, _ => 0 });

    println!("# of overlapping square inches = {}", count);
}

fn task_b(lines: Vec<String>) {
    let claims = parse_input(&lines);

    let mut fabric = HashMap::new();
    for cl in claims.clone() {
        for clp in cl {
            fabric.entry(clp).and_modify(|counter| *counter += 1).or_insert(1);
        }
    }

    for (i, &cl) in claims.iter().enumerate() {
        let mut no_overlaps = true;
        for clp in cl {
            if fabric[&clp] != 1 {
                no_overlaps = false;
                break;
            }
        }

        if no_overlaps {
            println!("ID of claim with no overlaps = {}", claims[i].get_id());
        }
    }
}

// #1 @ 286,440: 19x24
fn parse_input(lines: &[String]) -> Claims {
    let re = Regex::new(r"#(?P<id>\d+) @ (?P<x>\d+),(?P<y>\d+): (?P<w>\d+)x(?P<h>\d+)").unwrap();

    let pars = |s: &str| {
        s.parse().unwrap()
    };

    lines
        .iter()
        .map(|line| {
            let caps = re.captures(line).unwrap();
            Claim::new(caps["id"].parse().unwrap(), pars(&caps["x"]), pars(&caps["y"]), pars(&caps["w"]), pars(&caps["h"]), )
        }).collect::<Vec<_>>()
}

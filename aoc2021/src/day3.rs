pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let n_bits = lines[0].len();
    let bits_count = lines.iter().fold(vec![0; n_bits], |mut bit_counter, bit_string| {
        let mut bits = bit_string.chars();
        for bc in bit_counter.iter_mut().take(n_bits) {
            if bits.next().unwrap() == '1' {
                *bc += 1;
            }
        }
        bit_counter
    });

    let half_n_numbers = lines.len() / 2;
    let (gamma, epsilon) = bits_count.iter().fold((0, 0), |(mut g, mut e), cnt| {
        g <<= 1;
        e <<= 1;
        if *cnt >= half_n_numbers {
            g += 1;
        } else {
            e += 1;
        }
        (g, e)
    });

    println!("Power product = {}", gamma * epsilon);
}

fn task_b(lines: Vec<String>) {
    let split_on_bits = |nbrs: &[String], bit: usize| -> (Vec<String>, Vec<String>) {
        nbrs.iter().fold((Vec::new(), Vec::new()), |(mut zeros, mut ones), bit_string| {
            let bit = bit_string.chars().nth(bit).unwrap();
            if bit == '0' {
                zeros.push(bit_string.to_string());
            } else {
                ones.push(bit_string.to_string());
            }

            (zeros, ones)
        })
    };
    let select = |z: &[String], o: &[String], longest: bool| -> Vec<String> {
        let op = if longest { PartialOrd::gt } else { PartialOrd::le };
        if op(&z.len(), &o.len()) {
            z.to_vec()
        } else {
            o.to_vec()
        }
    };

    let (zrs, ons) = split_on_bits(&lines, 0);
    let mut nbrs = select(&zrs, &ons, true);
    let mut bi = 0;
    let o2_rating = loop {
        bi += 1;
        let (z, o) = split_on_bits(&nbrs, bi);
        nbrs = select(&z, &o, true);
        if nbrs.len() == 1 {
            break nbrs[0].clone();
        }
    };

    nbrs = select(&zrs, &ons, false);
    let mut bi = 0;
    let co2_rating = loop {
        bi += 1;
        let (z, o) = split_on_bits(&nbrs, bi);
        nbrs = select(&z, &o, false);
        if nbrs.len() == 1 {
            break &nbrs[0];
        }
    };

    let supp_rating = isize::from_str_radix(&o2_rating, 2).unwrap() * isize::from_str_radix(co2_rating, 2).unwrap();
    println!("Life support rating: {}", supp_rating);
}

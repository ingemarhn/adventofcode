pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut octopuses = parse_input(lines);

    let mut flash_count = 0;
    for _ in 1..=100 {
        for octopuses_r in octopuses.iter_mut().take(10) {
            for octopuses_r_c in octopuses_r.iter_mut().take(10) {
                *octopuses_r_c += 1;
            }
        }

        let mut ri = 0;
        let mut ci = 0;
        'out: loop {
            loop {
                if octopuses[ri][ci] > 9 {
                    octopuses[ri][ci] = 0;
                    flash_count += 1;
                    let rsi = if ri > 0 { ri - 1 } else { ri };
                    let rei = if ri < 9 { ri + 1 } else { ri };
                    let csi = if ci > 0 { ci - 1 } else { ci };
                    let cei = if ci < 9 { ci + 1 } else { ci };

                    for octopuses_pri in octopuses.iter_mut().take(rei + 1).skip(rsi) {
                        for octopuses_pri_pci in octopuses_pri.iter_mut().take(cei + 1).skip(csi) {
                            if *octopuses_pri_pci != 0 {
                                *octopuses_pri_pci += 1;
                            }
                        }
                    }

                    ri = rsi;
                    ci = csi;
                    continue 'out;
                }
                ci += 1;
                if ci == 10 {
                    break
                }
            }
            ri += 1;
            if ri == 10 {
                break
            }
            ci = 0;
        }
    }
    println!("Total flashes: {}", flash_count);
}

fn task_b(lines: Vec<String>) {
    let mut octopuses = parse_input(lines);

    let mut step_count = 0;
    loop {
        for octopuses_r in octopuses.iter_mut().take(10) {
            for octopuses_r_c in octopuses_r.iter_mut().take(10) {
                *octopuses_r_c += 1;
            }
        }

        let mut ri = 0;
        let mut ci = 0;
        'out: loop {
            loop {
                if octopuses[ri][ci] > 9 {
                    octopuses[ri][ci] = 0;
                    let rsi = if ri > 0 { ri - 1 } else { ri };
                    let rei = if ri < 9 { ri + 1 } else { ri };
                    let csi = if ci > 0 { ci - 1 } else { ci };
                    let cei = if ci < 9 { ci + 1 } else { ci };

                    for octopuses_pri in octopuses.iter_mut().take(rei + 1).skip(rsi) {
                        for octopuses_pri_pci in octopuses_pri.iter_mut().take(cei + 1).skip(csi) {
                            if *octopuses_pri_pci != 0 {
                                *octopuses_pri_pci += 1;
                            }
                        }
                    }

                    ri = rsi;
                    ci = csi;
                    continue 'out;
                }
                ci += 1;
                if ci == 10 {
                    break
                }
            }
            ri += 1;
            if ri == 10 {
                break
            }
            ci = 0;
        }

        step_count += 1;
        let sum: u32 = octopuses.iter().map(|or| or.iter().sum::<u32>()).sum();
        if sum == 0 {
            break
        }
    }
    println!("First simulatenous flash at step: {}", step_count);
}

type OctopusRow = Vec<u32>;
type OctopusMap = Vec<OctopusRow>;

fn parse_input(lines: Vec<String>) -> OctopusMap {
    lines
        .iter()
        .map(|l| l.chars().map(|n| n.to_digit(10).unwrap()).collect())
        .collect()
}

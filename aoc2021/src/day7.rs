pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut levels: Vec<i32> = lines[0].split(',').map(|n| n.parse().unwrap()).collect();
    levels.sort();

    let mut lowest_fuel = u32::MAX;
    for lev in levels[0]..=*levels.last().unwrap() {
        let mut this_lev_fuel = 0u32;
        for t_lev in &levels {
            this_lev_fuel += (lev - t_lev).unsigned_abs();
        }

        if this_lev_fuel < lowest_fuel {
            lowest_fuel = this_lev_fuel;
        }
    }

    println!("Fuel cost: {}", lowest_fuel);
}

fn task_b(lines: Vec<String>) {
    let mut levels: Vec<i32> = lines[0].split(',').map(|n| n.parse().unwrap()).collect();
    levels.sort();

    let mut lev_costs = Vec::new();
    for lev in levels[0]..=*levels.last().unwrap() {
        lev_costs.push((0..=lev).sum::<i32>() as u32);
    }

    let mut lowest_fuel = u32::MAX;
    for lev in levels[0]..=*levels.last().unwrap() {
        let mut this_lev_fuel = 0u32;
        for t_lev in &levels {
            let lev_diff = (lev - t_lev).unsigned_abs();
            this_lev_fuel += lev_costs[lev_diff as usize];
        }

        if this_lev_fuel < lowest_fuel {
            lowest_fuel = this_lev_fuel;
        }
    }

    println!("Fuel cost: {}", lowest_fuel);
}

use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let caves = parse_input(lines);
    let count = find_paths(&caves);
    println!("# of paths: {}", count);
}

fn task_b(lines: Vec<String>) {
    let caves = parse_input(lines);
    let count = find_paths_b(&caves);
    println!("# of paths: {}", count);
}

#[derive(Debug, PartialEq)]
enum CaveKind {
    Small,
    Big
}

type Caves = HashMap<String, Cave>;

#[derive(Debug)]
struct Cave {
    kind: CaveKind,
    neighbors: Vec<String>,
}

fn parse_input(lines: Vec<String>) -> Caves {
    let mut caves = HashMap::new();
    for l in lines{
        let cave_names: Vec<String> = l.split('-').map(|c| c.to_string()).collect();
        for cn in &cave_names {
            let kind = if cn.chars().next().unwrap().is_lowercase() { CaveKind::Small } else { CaveKind::Big };
            if !caves.contains_key(cn) {
                caves.insert(cn.to_string(), Cave {kind, neighbors: Vec::new()});
            }
        }
        caves.entry(cave_names[0].clone()).and_modify(|n| {n.neighbors.push(cave_names[1].clone())});
        caves.entry(cave_names[1].clone()).and_modify(|n| {n.neighbors.push(cave_names[0].clone())});
    }

    caves
}

// DFS
fn find_paths(caves: &Caves) -> usize {
    fn dfs(caves: &Caves, current_cave_name: String, path_in: Vec<String>, count_in: usize) -> usize {
        let mut path = path_in;
        let mut count = count_in;

        path.push(current_cave_name.to_string());
        if current_cave_name == "end" {
            return count + 1;
        }

        for cave_name in &caves[&current_cave_name].neighbors {
            if caves[cave_name].kind == CaveKind::Big || !path.contains(cave_name) {
                count = dfs(caves, cave_name.to_string(), path.clone(), count);
            }
        }

        count
    }

    dfs(caves, "start".to_string(), Vec::new(), 0)
}

fn find_paths_b(caves: &Caves) -> usize {
    fn dfs(caves: &Caves, current_cave_name: String, path_in: Vec<String>, small_twice: bool, count_in: usize) -> usize {
        let mut path = path_in;
        let mut count = count_in;

        path.push(current_cave_name.to_string());
        if current_cave_name == "end" {
            return count + 1;
        }

        for cave_name in &caves[&current_cave_name].neighbors {
            if cave_name == "start" {
                continue;
            }
            let (go_deeper, small_tw_p) = if caves[cave_name].kind == CaveKind::Big || !path.contains(cave_name) {
                (true, small_twice)
            } else if path.contains(cave_name) && !small_twice {
                (true, true)
            } else {
                (false, false)
            };

            if go_deeper {
                count = dfs(caves, cave_name.to_string(), path.clone(), small_tw_p, count);
            }
        }

        count
    }

    dfs(caves, "start".to_string(), Vec::new(), false, 0)
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut grid = vec![ vec![0; 1000]; 1000];
    let vents = parse_input(lines);
    let straight_vents: Vec<Vent> = vents.iter().filter(|v| v.kind != Diagonal).cloned().collect();
    let get_range = |s: u32, e: u32| {
        if s <= e {
            s..=e
        } else {
            e..=s
        }
    };
    for v in &straight_vents {
        if v.kind == Horizontal {
            for x in get_range(v.start.0, v.end.0) {
                grid[v.start.1 as usize][x as usize] += 1;
            }
        } else {
            for y in get_range(v.start.1, v.end.1) {
                grid[y as usize][v.start.0 as usize] += 1;
            }
        }
    }

    let count = grid.iter().fold(0, |cc, r| cc + r.iter().fold(0, |c, &e| if e > 1 { c + 1 } else { c }));
    println!("Points with at least two overlaps: {}", count);
}

fn task_b(lines: Vec<String>) {
    let mut grid = vec![ vec![0; 1000]; 1000];
    let vents = parse_input(lines);

    let get_range = |s: u32, e: u32| {
        if s <= e {
            (s..=e).collect::<Vec<u32>>()
        } else {
            (e..=s).rev().collect::<Vec<u32>>()
        }
    };
    for v in &vents {
        if v.kind == Horizontal {
            for x in get_range(v.start.0, v.end.0) {
                grid[v.start.1 as usize][x as usize] += 1;
            }
        } else if v.kind == Vertical {
            for y in get_range(v.start.1, v.end.1) {
                grid[y as usize][v.start.0 as usize] += 1;
            }
        } else {
            let x_range_v = get_range(v.start.0, v.end.0);
            let mut x_range = x_range_v.iter();
            for y in get_range(v.start.1, v.end.1) {
                let &x = x_range.next().unwrap();
                grid[y as usize][x as usize] += 1;
            }
        }
    }

    let count = grid.iter().fold(0, |cc, r| cc + r.iter().fold(0, |c, &e| if e > 1 { c + 1 } else { c }));
    println!("Points with at least two overlaps: {}", count);
}

type Position = (u32, u32);

#[derive(Clone, Copy, Debug, PartialEq)]
enum VentType {
    Horizontal,
    Vertical,
    Diagonal,
    Other
}
use VentType::*;

#[derive(Debug, Clone, Copy)]
struct Vent {
    kind: VentType,
    start: Position,
    end: Position,
}

fn parse_input(lines: Vec<String>) -> Vec<Vent> {
    lines.iter().fold(Vec::new(), |mut vs, line| {
        let elems = line.split_whitespace().collect::<Vec<&str>>();
        let ind1 = elems[0].split(',').collect::<Vec<&str>>();
        let ind2 = elems[2].split(',').collect::<Vec<&str>>();
        let indices = [ind1, ind2].concat().iter().fold(Vec::new(), |mut inds, ind| {
            inds.push(ind.parse::<u32>().unwrap());

            inds
        });

        if let [x1, y1, x2, y2] = indices[0..4] {
            let k = if x1 == x2 {
                Vertical
            } else if y1 == y2 {
                Horizontal
            } else if (x1 as i32 - x2 as i32).abs() == (y1 as i32 - y2 as i32).abs() {
                Diagonal
            } else {
                Other
            };
            let v = Vent {
                kind: k,
                start: (x1, y1),
                end: (x2, y2),
            };

            vs.push(v);
        }

        vs
    })
}


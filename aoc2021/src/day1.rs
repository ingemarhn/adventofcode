pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let measurements: Vec<i32> = lines.iter().map(|line| line.parse::<i32>().unwrap()).collect();
    let count = count_gt_measmts(measurements);
    println!("# of greater measurements = {}", count);
}

fn task_b(lines: Vec<String>) {
    let measurements: Vec<i32> = lines.iter().map(|line| line.parse::<i32>().unwrap()).collect();
    let mut sliding_measurements = Vec::new();
    for i in 0..measurements.len()-2 {
        sliding_measurements.push(measurements[i] + measurements[i+1] + measurements[i+2]);
    }
    let count = count_gt_measmts(sliding_measurements);
    println!("# of greater measurements = {}", count);
}

fn count_gt_measmts(measurements: Vec<i32>) -> i32 {
    let (count, _) = measurements.iter().fold((0, 0), |(cnt, prev), m| if m > &prev { (cnt + 1, *m) } else { (cnt, *m) });

    count - 1
}
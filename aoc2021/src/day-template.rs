#![allow(dead_code,unused_variables,unused_mut)]
pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let _ = parse_input(&lines);
}

fn task_b(lines: Vec<String>) {
    let _ = parse_input(&lines);
}

fn parse_input(lines: &[String]) -> usize {
lines.len()
}

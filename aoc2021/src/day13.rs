pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (coords, folds) = parse_input(lines);
    let mut transp_sheet = TranspSheet::new(&coords);
    for c in coords {
        transp_sheet.add(c);
    }

    if folds[0].0 == 'y' {
        transp_sheet.fold_y(folds[0].1);
    } else {
        transp_sheet.fold_x(folds[0].1);
    }

    println!("# of dots: {}", transp_sheet.count_dots());
}

fn task_b(lines: Vec<String>) {
    let (coords, folds) = parse_input(lines);
    let mut transp_sheet = TranspSheet::new(&coords);
    for c in coords {
        transp_sheet.add(c);
    }

    for f in folds {
        if f.0 == 'y' {
            transp_sheet.fold_y(f.1);
        } else {
            transp_sheet.fold_x(f.1);
        }
    }

    println!("Code:");
    let dgs = [' ', '#'];
    for r in 0..transp_sheet.height {
        for c in 0..transp_sheet.width { print!("{}", dgs[transp_sheet.dots[r][c] as usize])}
        println!();
    }
}

type Coord = Vec<usize>;
type Coords = Vec<Coord>;

#[derive(Debug)]
struct TranspSheet {
    width: usize,
    height: usize,
    dots: Vec<Vec<u8>>,
}

impl TranspSheet {
    fn new(cs: &Coords) -> Self {
        let (mx, my) = cs.iter().fold((0,0), |(m0, m1), c| (m0.max(c[0]), m1.max(c[1])) );
        let mut dots = Vec::new();
        for _ in 0..=my {
            dots.push(vec![0; mx + 1]);
        }
        Self {
            width: mx + 1,
            height: my + 1,
            dots,
        }
    }

    fn add(&mut self, c: Coord) {
        let (x, y) = (c[0], c[1]);
        self.dots[y][x] = 1;
    }

    fn fold_x(&mut self, along: u32) {
        let along = along as usize;
        for y in 0..self.height {
            let mut ind = along;
            for x in along+1..self.width {
                ind -= 1;
                self.dots[y][ind] |= self.dots[y][x];
            }
        }

        self.width = along;
    }

    fn fold_y(&mut self, along: u32) {
        let along = along as usize;
        let mut ind = along;
        for y in along+1..self.height {
            ind -= 1;
            for x in 0..self.width {
                self.dots[ind][x] |= self.dots[y][x];
            }
        }

        self.height = along;
    }

    fn count_dots(&self) -> u32 {
        let mut count = 0;
        for r in 0..self.height {
            for c in 0..self.width {
                count += self.dots[r][c] as u32
            }
        }

        count
    }
}

fn parse_input(lines: Vec<String>) -> (Coords, Vec<(char, u32)>) {
    let mut l_iter = lines.iter();
    let coords: Coords = l_iter.by_ref()
        .take_while(|&l| !l.is_empty() )
        .map(|c| c.split(',')
                    .map(|ce| ce.parse::<usize>().unwrap())
                    .collect())
        .collect();

    let folds = l_iter.map(|l| {
        let num = l[13..].parse::<u32>().unwrap();
        match &l[11..12] {
            "x" => ('x', num),
            _   => ('y', num),
        }
    }).collect();

    (coords, folds)
}


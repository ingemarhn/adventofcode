use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (numbers, mut boards) = parse_input(lines);
    let mut num_iter = numbers.iter();
    'ol: loop {
        let &num = num_iter.next().unwrap();
        for b_it in &mut boards {
            if b_it.check(num) {
                let sum_unmarked = b_it.all_nums.iter().fold(0u32, |sum, (&n, &(_, _, m))|
                    if m { sum } else { sum + n as u32 }
                );

                println!("Final score: {}", sum_unmarked * num as u32);
                break 'ol;
            }
        }
    }
}

fn task_b(lines: Vec<String>) {
    let (numbers, mut boards) = parse_input(lines);
    let mut sum_unmarked = 0;
    for num in numbers {
        for b_it in &mut boards {
            if b_it.got_bingo {
                continue
            }
            if b_it.check(num) {
                sum_unmarked = b_it.all_nums.iter().fold(0u32, |sum, (&n, &(_, _, m))|
                    if m { sum } else { sum + n as u32 }
                );
                sum_unmarked *= num as u32;
            }
        }
    }

    println!("Final score: {}", sum_unmarked);
}

#[derive(Debug)]
struct Bingo {
    col_mark_count: [u8; 5],
    row_mark_count: [u8; 5],
    all_nums: HashMap<u8, (u8, u8, bool)>,
    got_bingo: bool,
}

impl Bingo {
    fn new() -> Self {
        Bingo {
            col_mark_count: [0; 5],
            row_mark_count: [0; 5],
            all_nums: HashMap::new(),
            got_bingo: false,
        }
    }

    fn add(&mut self, n: u8, c: usize, r: u8) {
        self.col_mark_count[c] = 0;
        self.row_mark_count[r as usize] = 0;
        self.all_nums.insert(n, (c as u8, r, false));
    }

    fn check(&mut self, n: u8) -> bool {
        if let Some(&(c, r, _)) = self.all_nums.get(&n) {
            self.col_mark_count[c as usize] += 1;
            self.row_mark_count[r as usize] += 1;
            self.all_nums.insert(n, (c, r, true));
            self.got_bingo = self.col_mark_count[c as usize] == 5 || self.row_mark_count[r as usize] == 5;
            self.got_bingo
        } else {
            false
        }
    }
}

fn parse_input(lines: Vec<String>) -> (Vec<u8>, Vec<Bingo>) {
    let play_nbrs: Vec<u8> = lines[0].split(',').map(|n| n.parse().unwrap()).collect();
    let (boards, _) = lines[1..].iter().fold((Vec::new(), 0), |(mut brds, rowcnt), row| {
        if row.is_empty() {
            brds.push(Bingo::new());
            (brds, 0)
        } else {
            let i = brds.len() - 1;
            let board_nbrs: Vec<u8> = row.split_whitespace().map(|n| n.parse().unwrap()).collect();
            for (j, bn_it) in board_nbrs.iter().enumerate() {
                brds[i].add(*bn_it, j, rowcnt);
            }
            (brds, rowcnt + 1)
        }
    });

    (play_nbrs, boards)
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let l_len = lines[0].len() + 1;
    let map = parse_input(lines);
    let mut sum = 0;
    for l in 1..map.len()-1 {
        for p in 1..l_len {
            let count = [(l-1,p), (l,p+1), (l+1,p), (l,p-1)].iter().fold(0, |cnt, (r,i)| if map[*r][*i] > map[l][p] { cnt + 1 } else { cnt });
            if count == 4 {
                sum += 1 + map[l][p].to_digit(10).unwrap();
            }
        }
    }
    println!("Sum of risk levels: {}", sum);
}

fn task_b(lines: Vec<String>) {
    let l_len = lines[0].len() + 1;
    let map = parse_input(lines);
    let mut low_points = Vec::new();
    for l in 1..map.len()-1 {
        for p in 1..l_len {
            let count = [(l-1,p), (l,p+1), (l+1,p), (l,p-1)].iter().fold(0, |cnt, (r,i)| if map[*r][*i] > map[l][p] { cnt + 1 } else { cnt });
            if count == 4 {
                low_points.push((l, p));
            }
        }
    }

    let mut sizes: Vec<_> = low_points.iter().map(|pos| find_basin(&map, pos)).collect();
    sizes.sort();

    let sum: usize = sizes[sizes.len()-3..].iter().product();
    println!("Sum of risk levels: {}", sum);
}

type HeightRow = Vec<char>;
type HeightMap = Vec<HeightRow>;
type NodePos = (usize, usize);
type NodePositions = Vec<NodePos>;

fn parse_input(lines: Vec<String>) -> HeightMap {
    let topbot = vec!["9".repeat(lines[0].len())];
    let z = "9";
    [topbot.clone(), lines, topbot].concat().iter()
        .map(|l| [z, l, z].concat().chars().collect())
        .collect()
}

// BFS - Dijkstra
fn find_basin(nodes: &HeightMap, start_pos: &NodePos) -> usize {
    use std::collections::VecDeque;

    let get_nbr = |(yi, xi): &NodePos| -> NodePositions {
        let mut nbrs = Vec::new();
        for (xm, ym) in &[(1,  0), (-1,  0), (0,  1), (0, -1)] {
            let y = (*yi as i32 + ym) as usize;
            let x = (*xi as i32 + xm) as usize;
            if nodes[y][x] != '9' {
                nbrs.push((y, x));
            }
        }

        nbrs
    };

    let mut visited = NodePositions::new();
    let mut queue = VecDeque::new();

    queue.push_back(*start_pos);
    visited.push(*start_pos);

    let mut count = 1;
    while !queue.is_empty() {
        let current_pos = queue.pop_front().unwrap();
        let nbrs = get_nbr(&current_pos);

        if nbrs.is_empty() {
            continue;
        }

        for pos in &nbrs {
            if !visited.contains(pos) {
                queue.push_back(*pos);
                visited.push(*pos);
                count += 1;
            }
        }
    }

    count
}

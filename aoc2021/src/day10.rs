pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    use std::collections::HashMap;
    let pairs = HashMap::from([
        (')', '('),
        (']', '['),
        ('}', '{'),
        ('>', '<'),
    ]);
    let points = HashMap::from([
        (')', 3),
        (']', 57),
        ('}', 1197),
        ('>', 25137),
    ]);

    let err_score = parse_input(lines)
        .iter()
        .fold(0, |score, line| {
            let mut stack = Vec::new();
            let mut l_score = score;
            for c in line {
                match c {
                    '(' | '[' | '{' | '<' => stack.push(c),
                    ')' | ']' | '}' | '>' => {
                        let b = stack.pop().unwrap();
                        if pairs[c] != *b {
                            l_score += points[c];
                            break;
                        }
                    },
                    _ => {}
                }
            }
        l_score});

    println!("Syntax error score is: {}", err_score);
}

fn task_b(lines: Vec<String>) {
    use std::collections::HashMap;
    let pairs1 = HashMap::from([
        ('(', ')'),
        ('[', ']'),
        ('{', '}'),
        ('<', '>'),
    ]);
    let pairs2 = HashMap::from([
        (')', '('),
        (']', '['),
        ('}', '{'),
        ('>', '<'),
    ]);
    let points = HashMap::from([
        (')', 1),
        (']', 2),
        ('}', 3),
        ('>', 4),
    ]);

    let mut err_scores: Vec<_> = parse_input(lines)
        .iter()
        .map(|line| {
            let mut stack = Vec::new();
            let mut corrupt = false;
            for c in line {
                match c {
                    '(' | '[' | '{' | '<' => stack.push(c),
                    ')' | ']' | '}' | '>' => {
                        let b = stack.pop().unwrap();
                        if pairs2[c] != *b {
                            corrupt = true;
                            break;
                        }
                    },
                    _ => {}
                }
            }
            if !corrupt {
                let mut l_score = 0u64;
                for i in (0..stack.len()).rev() {
                    let cb = pairs1[stack[i]];
                    l_score = l_score * 5 + points[&cb];
                }
                l_score
            } else {
                0
            }
        })
        .filter(|&s| s != 0).collect();
    err_scores.sort();

    println!("Middle score is: {}", err_scores[err_scores.len() / 2]);
}

type NavRow = Vec<char>;
type NavMap = Vec<NavRow>;

fn parse_input(lines: Vec<String>) -> NavMap {
    lines
        .iter()
        .map(|l| l.chars().collect())
        .collect()
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let m_values = parse_input(&lines);
    let lv = find_value(m_values, true);
    println!("Largest value: {}", lv);
}

fn task_b(lines: Vec<String>) {
    let m_values = parse_input(&lines);
    let mv = find_value(m_values, false);
    println!("Smallest value: {}", mv);
}

const VI1: usize = 3;
const VI2: usize = 14;
type ValType = i64;
type Values = [[ValType; VI1]; VI2];

fn find_value(values: Values, find_largest: bool) -> String {
    use either::*;

    let mut prev_z_values = vec![vec![0]];
    let mut prev_zw_values: Vec<Vec<(ValType, ValType, ValType)>> = Vec::new();
    let mut figures = [0; VI2];

    for val_it in values.iter() {
        let mut next_z_values = Vec::new();
        let mut next_zw_values = Vec::new();
        let mut w_vals = Vec::new();
        for zv in &prev_z_values[prev_z_values.len() - 1] {
            let rng = if find_largest { Either::Left((1..=9).rev()) } else { Either::Right(1..=9) };
            for w in rng {
                let mut z = *zv;
                let x = if z % 26 + val_it[1] == w { 0 } else { 1 };
                let z_div = val_it[0];
                if z_div == 26 && x != 0 {
                    continue
                }

                z /= z_div;
                z *= 25 * x + 1;
                z += (w + val_it[2]) * x;
                if !next_z_values.contains(&z) {
                    next_z_values.push(z);
                    next_zw_values.push((z,w,*zv));
                    if !w_vals.contains(&w) {
                        w_vals.push(w);
                    }
                    if z == 0 {
                        figures[VI2 - 1] = w;
                        let mut pzv = *zv;
                        for i in (0..VI2 - 1).rev() {
                            let ind = prev_z_values[i + 1].iter().position(|&pz| pz == pzv).unwrap();
                            let (_, pz, ppz) = prev_zw_values[i][ind];
                            pzv = ppz;
                            figures[i] = pz;
                        }
                        return figures.iter().fold(String::new(), |mut s, f| {s.push_str(format!("{}", f).as_str()); s});
                    }
                }
            }
        }

        prev_z_values.push(next_z_values);
        prev_zw_values.push(next_zw_values);
    }

    String::new()
}

fn parse_input(lines: &[String]) -> Values {
    let mut values = [[0; VI1]; VI2];

    let pars = |l: &String| {
        l[6..].parse().unwrap()
    };
    for (c, l) in lines.iter().enumerate() {
        let i = c / 18;
        match c % 18 {
             4 => values[i][0] = pars(l),
             5 => values[i][1] = pars(l),
            15 => values[i][2] = pars(l),
            _  => {},
        }
    }

    values
}

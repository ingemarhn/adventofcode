use regex::Regex;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (_, _, y_low, _) = parse_input(lines);

    let max_y: i32 = (1..(y_low.abs())).sum();

    println!("Max y: {}", max_y);
}

fn task_b(lines: Vec<String>) {
    use std::collections::HashSet;

    let (x_low, x_high, y_low, y_high) = parse_input(lines);

    let max_xv = x_high;
    let min_xv = 'l: loop {
        let mut x_sum = 0;
        for n in 1.. {
            x_sum += n;
            if x_sum > x_low {
                break 'l n;
            }
        }
    };
    let max_yv = y_low.abs() - 1;
    let min_yv = y_low;

    let mut distinct_vels = HashSet::new();
    for xv in min_xv..=max_xv {
        let mut x_values_n_steps = Vec::new();
        let mut x = 0;
        for xa in (1..=xv).rev() {
            x += xa;
            if x >= x_low {
                if x > x_high {
                    break
                }
                let steps = if xa > 1 {xv - xa + 1} else {i32::MAX};
                x_values_n_steps.push((x, steps, xv));
            }
        }
        if x_values_n_steps.is_empty() { continue }

        for yv in min_yv..=max_yv {
            let (mut y_steps, mut next_v) = if yv > 0 { (yv * 2 + 1, -yv - 1) } else { (0, yv) };
            let x_min = x_values_n_steps[0].1;
            let x_max = (x_values_n_steps.last().unwrap()).1;
            if y_steps > x_max { continue }

            let mut y = 0;
            let mut y_values_n_steps = Vec::new();
            loop {
                y += next_v;
                next_v -= 1;
                y_steps += 1;
                if y <= y_high {
                    if y < y_low { break }
                    if (x_min..=x_max).contains(&y_steps) {
                        y_values_n_steps.push((y, y_steps, yv));
                    }
                }
            }
            if y_values_n_steps.is_empty() { continue }

            let mut xvs_i = x_values_n_steps.iter();
            let mut xvs = *xvs_i.next().unwrap();
            while xvs.1 < y_values_n_steps[0].1 {
                xvs = *xvs_i.next().unwrap();
            }
            for yvs in &y_values_n_steps {
                let mut xvs_ic = xvs_i.clone();
                loop {
                    distinct_vels.insert((xvs.2, yvs.2));
                    let xvs_o = xvs_ic.next();
                    if xvs_o.is_none() { break }
                    xvs = *xvs_o.unwrap();
                }
            }
        }
    }

    println!("# of distinct initial velocity values: {}", distinct_vels.len());
}

fn parse_input(lines: Vec<String>) -> (i32, i32, i32, i32) {
    // target area: x=138..184, y=-125..-71
    let rx: Regex = Regex::new(r"target area: x=(?P<x0>-?\d+)\.\.(?P<x1>-?\d+), y=(?P<y0>-?\d+)\.\.(?P<y1>-?\d+)").unwrap();
    let captures = rx.captures(&lines[0]).unwrap();

    let parse = |name: &str| -> i32 {
        captures.name(name).unwrap().as_str().parse().unwrap()
    };

    (parse("x0"), parse("x1"), parse("y0"), parse("y1"))
}
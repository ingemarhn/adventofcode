pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (fwd, dep) = lines.iter().fold((0, 0), |(f, d), ins| {
        let inst_parts = ins.split_whitespace().collect::<Vec<&str>>();
        let steps: i32 = inst_parts[1].parse().unwrap();
        match inst_parts[0] {
            "forward" => (f + steps, d),
            "down" => (f, d + steps),
            "up" => (f, d - steps),
            _ => (0, 0)
        }
    });
    println!("Product: {}", fwd * dep);
}

fn task_b(lines: Vec<String>) {
    let (fwd, dep, _) = lines.iter().fold((0, 0, 0), |(f, d, a), ins| {
        let inst_parts = ins.split_whitespace().collect::<Vec<&str>>();
        let steps: i32 = inst_parts[1].parse().unwrap();
        match inst_parts[0] {
            "forward" => (f + steps, d + a * steps, a),
            "down" => (f, d, a + steps),
            "up" => (f, d, a - steps),
            _ => (0, 0, 0)
        }
    });
    println!("Product: {}", fwd * dep);
}

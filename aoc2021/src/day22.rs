use regex::Regex;
use std::collections::HashSet;
use std::ops::Not;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let cuboids = parse_input(lines, true);
    let mut grid = HashSet::new();
    for cube in cuboids {
        for x in cube.clone().x_range {
            for y in cube.clone().y_range {
                for z in cube.clone().z_range {
                    if cube.kind == On {
                        grid.insert((x, y, z));
                    } else {
                        grid.remove(&(x, y, z));
                    }
                }
            }
        }
    }

    println!("# of cubes on: {}", grid.len());
}

fn task_b(lines: Vec<String>) {
    let cuboids = parse_input(lines, false);

    // Calculate intersection for one dimension
    let get_intersect = |r1: &CubeRange, r2: &CubeRange| {
        let r1s = r1.start();
        let r1e = r1.end();
        let r2s = r2.start();
        let r2e = r2.end();
        if r1.contains(r2s) {
            Some((*r2s, *r1e.min(r2e)))
        } else if r2.contains(r1s) {
            Some((*r1s, *r2e.min(r1e)))
        } else {
            None
        }
    };

    let mut reactor: Cuboids = Vec::new();
    for cube in cuboids {
        let mut new_cubes: Cuboids = Vec::new();
        for test_cube in &reactor {

            // Find intersection
            if let Some(xi) = get_intersect(&cube.x_range, &test_cube.x_range) {
                if let Some(yi) = get_intersect(&cube.y_range, &test_cube.y_range) {
                    if let Some(zi) = get_intersect(&cube.z_range, &test_cube.z_range) {
                        // Create a new cube with the opposite state of the test cube and the volume is the intersection of the two.
                        // If we have a save cube with Off state that matches an input cube that is Off it means that we already have turned off
                        // this intersection. To get the count right we need to turn it on again.
                        new_cubes.push(Cuboid::new(!test_cube.kind, xi.0, xi.1, yi.0, yi.1, zi.0, zi.1));
                    }
                }
            }
        }

        if cube.kind == On {
            // Don't push "Off" cubes from the input. We're only interested in the intersections with the other cubes and they are pushed below.
            reactor.push(cube);
        }
        for nc in new_cubes {
            reactor.push(nc);
        }
    }

    let mut count = 0;
    for cube in &reactor {
        if cube.kind == On {
            count += cube.volume();
        } else {
            count -= cube.volume();
        }
    }
    println!("# of cubes on: {}", count);
}

type Cuboids = Vec<Cuboid>;
type IType = i64;
type CubeRange = std::ops::RangeInclusive<IType>;

#[derive(Debug, Clone, Copy, PartialEq)]
enum State {
    On,
    Off
}
use State::*;

impl State {
    fn parse(kind: &str) -> Self {
        match kind {
            "on" => On,
            _ => Off,
        }
    }
}

impl Not for State {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            On => Off,
            Off => On
        }
    }
}

#[derive(Debug, Clone)]
struct Cuboid {
    kind: State,
    x: (IType, IType),
    y: (IType, IType),
    z: (IType, IType),
    x_range: CubeRange,
    y_range: CubeRange,
    z_range: CubeRange,
}

impl Cuboid {
    fn new(kind: State, x0: IType, x1: IType, y0: IType, y1: IType, z0: IType, z1: IType ) -> Self {
        let x_range = std::ops::RangeInclusive::new(x0, x1);
        let y_range = std::ops::RangeInclusive::new(y0, y1);
        let z_range = std::ops::RangeInclusive::new(z0, z1);
        Cuboid { kind, x: (x0, x1), y: (y0, y1), z: (z0, z1), x_range, y_range, z_range }
    }

    fn volume(&self) -> IType {
        (self.x.1 - self.x.0 + 1) * (self.y.1 - self.y.0 + 1) * (self.z.1 - self.z.0 + 1)
    }
}

fn parse_input(lines: Vec<String>, with_limit: bool) -> Cuboids {
    let rx: Regex = Regex::new(r"(?P<onoff>(?:on|off)) x=(?P<x0>-?\d+)\.\.(?P<x1>-?\d+),y=(?P<y0>-?\d+)\.\.(?P<y1>-?\d+),z=(?P<z0>-?\d+)\.\.(?P<z1>-?\d+)").unwrap();
    let p = |capts: &regex::Captures, name: &str| -> IType {
        capts.name(name).unwrap().as_str().parse().unwrap()
    };

    let parsed = lines
        .iter()
        .map(|l| rx.captures(l).unwrap())
        .map(|c| (c.name("onoff").unwrap().as_str(),
                  p(&c, "x0"), p(&c, "x1"), p(&c, "y0"), p(&c, "y1"), p(&c, "z0"), p(&c, "z1")));
/* let mm = parsed.clone()
    .fold((0,0,0,0,0,0), |mut tup, (_,x0,x1,y0,y1,z0,z1)| {
        if x0 < tup.0 { tup.0 = x0; }
        if x1 > tup.1 { tup.1 = x1; }
        if y0 < tup.2 { tup.2 = y0; }
        if y1 > tup.3 { tup.3 = y1; }
        if z0 < tup.4 { tup.4 = z0; }
        if z1 > tup.5 { tup.5 = z1; }
        tup
    }) ; println!("mm = {:?}", mm);
 */
    if with_limit {
        let chk = |v0, v1| {
            v0 <= 50 && v1 >= -50
        };
        parsed
            .filter(|p| chk(p.1, p.2) && chk(p.3, p.4) && chk(p.5, p.6))
            .map(|p| Cuboid::new(State::parse(p.0), p.1.max(-50), p.2.min(50), p.3.max(-50), p.4.min(50), p.5.max(-50), p.6.min(50), ))
            .collect()
    } else {
        parsed
            .map(|p| Cuboid::new(State::parse(p.0), p.1, p.2, p.3, p.4, p.5, p.6))
            .collect()
    }
}

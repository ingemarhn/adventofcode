use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::collections::HashSet;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let nodes = parse_input(lines);
    let cost = find_target(&nodes);

    println!("Lowest risk: {}", cost);
}

fn task_b(lines: Vec<String>) {
    let nodes = parse_input(lines);
    let nodes = expand_nodes(&nodes);
    let cost = find_target(&nodes);

    println!("Lowest risk: {}", cost);
}
type NodePos = (u16, u16);
type NodePositions = HashSet<NodePos>;
type NodesList = HashMap<NodePos, Node>;

#[derive(Debug, Clone)]
struct Node {
    y: u16,
    x: u16,
    cost: u16,
    distance: u32,
    predecessor: Option<NodePos>,
}

impl Node {
    fn new(y: u16, x: u16, cost: u16) -> Self {
        Self {y, x, cost, distance: u32::MAX, predecessor: None}
    }

    fn check_n_set(&mut self, predecessor: &Node) {
        let dist = predecessor.distance + self.cost as u32;
        if dist < self.distance {
            self.set(predecessor);
        }
    }

    fn set(&mut self, predecessor: &Node) {
        self.distance = predecessor.distance + self.cost as u32;
        self.predecessor = Some((predecessor.y, predecessor.x));
    }

    fn get_nbrs(&self, nodes: &NodesList) -> NodePositions {
        let mut nbrs = NodePositions::new();
        for (mv_y, mv_x) in [(-1i16, 0i16), (1, 0), (0, -1), (0, 1)] {
            let (ny, nx) = (self.y as i16 + mv_y, self.x as i16 + mv_x);
            if ny >= 0 && nx >= 0 {
                let new_pos = (ny as u16, nx as u16);
                if nodes.contains_key(&new_pos) {
                    nbrs.insert(new_pos);
                }
            }
        }

        nbrs
    }
}

fn parse_input(lines: Vec<String>) -> NodesList {
    let (nodes, _) = lines
        .iter()
        .fold((NodesList::new(), 0), |(mut nodes, y), line| {
            line
                .chars()
                .fold(0, |x, c| {
                    nodes.insert((y, x), Node::new(y, x, c.to_digit(10).unwrap() as u16));
                    x + 1
                });

            (nodes, y + 1)
        });

    nodes
}

fn expand_nodes(nodes_in: &NodesList) -> NodesList {
    let mut nodes = nodes_in.clone();
    let (max_y_in, max_x_in) = nodes_in.keys().fold((0, 0), |(my, mx), (y, x)| { ( my.max(*y), mx.max(*x) ) });
    let (max_y, max_x) = ((max_y_in + 1) * 5 - 1, (max_x_in + 1) * 5 - 1);

    let get_cost = |y: u16, x: u16, n_in: &NodesList| {
        let cost = n_in[&(y, x)].cost;
        match cost {
            9 => 1,
            _ => cost + 1,
        }
    };

    for y in 0..=max_y_in {
        for x in max_x_in+1..=max_x {
            nodes.insert((y, x), Node::new(y, x, get_cost(y, x - max_x_in - 1, &nodes)));
        }
    }

    for y in max_y_in+1..=max_y {
        for x in 0..=max_x {
            nodes.insert((y, x), Node::new(y, x, get_cost(y - max_y_in - 1, x, &nodes)));
        }
    }

    nodes
}

// Dijkstra
fn find_target(nodes_in: &NodesList) -> u32 {
    let mut visited = NodePositions::new();
    let mut adjacent = HashSet::new();
    let mut nodes = nodes_in.clone();
    let start_pos = (0, 0);
    let target_pos = nodes_in.keys().fold((0, 0), |(my, mx), (y, x)| { ( my.max(*y), mx.max(*x) ) });

    let set_adjacent = |adj: &mut HashSet<(_, _)>, nds: &mut NodesList, p: &NodePos, pred: &NodePos| {
        adj.insert(*p);
        let a_node = nds[pred].clone();
        nds
            .entry(*p)
            .and_modify(|e| { e.check_n_set(&a_node) });
    };

    visited.insert(start_pos);
    if let Entry::Occupied(mut node) = nodes.entry(start_pos) {
        let node = node.get_mut();
        node.distance = 0;
        node.get_nbrs(nodes_in).iter().for_each(|p| {
            set_adjacent(&mut adjacent, &mut nodes, p, &start_pos);
        });
    }

    while !adjacent.is_empty() {
        let cheap = *adjacent
            .iter()
            .reduce(|sp, p| if nodes[p].distance < nodes[sp].distance { p } else { sp } )
            .unwrap();

        visited.insert(cheap);
        adjacent.remove(&cheap);
        for pos in &nodes_in[&cheap].get_nbrs(nodes_in) {
            if visited.contains(pos) { continue }
            set_adjacent(&mut adjacent, &mut nodes, pos, &cheap);
        }
    }

    nodes[&target_pos].distance
}

/*
The "Manhattan distance between a distinct pair from N coordinates" technique should have been used. Like below ...

use std::collections::{HashMap, BinaryHeap};
use std::cmp::Reverse;

const SIZE: usize = 100;
const PAD: u32 = u32::MAX/2;

fn adj_pos(pos: (usize, usize)) -> [(usize, usize); 4] {
    let (i, j) = pos;
    [
        (i+1,j), (i-1,j), (i,j+1), (i,j-1)
    ]
}

struct MinHeap {
    max_heap: BinaryHeap<(Reverse<u32>, (usize, usize))>
}
impl MinHeap {
    fn new() -> MinHeap {
        let max_heap = BinaryHeap::new();
        MinHeap{max_heap}
    }
    fn push(&mut self, pos: (usize, usize), dist: u32) {
        self.max_heap.push( (Reverse(dist), pos) );
    }
    fn pop(&mut self) -> Option<((usize, usize), u32)> {
        let result = self.max_heap.pop();
        match result {
            Some((Reverse(dist), pos)) => Some((pos, dist)),
            None => None,
        }
    }
    fn is_empty(&self) -> bool {self.max_heap.is_empty()}
}

fn main() {
    let mut lines = include_str!("/home/ingemar/adventofcode/aoc2021/input/day-15.txt").lines();
    let cave_map = {
        let mut tmp = Vec::new();
        tmp.push(vec![PAD; SIZE+2]);
        for _ in 0..SIZE {
            let mut row = Vec::new();
            row.push(PAD);
            for c in lines.next().unwrap().trim_end().chars() {
                row.push(c.to_digit(10).unwrap() as u32);
            }
            row.push(PAD);
            tmp.push(row);
        }
        tmp.push(vec![PAD; SIZE+2]);
        tmp
    };

    //// Part 1
    {
    let mut exploration_queue = MinHeap::new();
    let mut distance_map: HashMap<(usize, usize), u32> = HashMap::new();
    //let mut came_from_map: HashMap<(usize, usize), (usize, usize)> = HashMap::new();

    let start_pos = (1,1);
    let end_pos = (SIZE,SIZE);

    let manhattan = |pos: (usize, usize)| {
        (end_pos.0 + end_pos.1 - pos.0 - pos.1) as u32
    };

    distance_map.insert(start_pos, 0);
    exploration_queue.push(start_pos, 0);
    while !exploration_queue.is_empty() {
        let (current_pos, _) = exploration_queue.pop().unwrap();
        let dist_to_current = *distance_map.get(&current_pos).unwrap();
        //println!("Currently in {:?} at distance {}", current_pos, dist_to_current);
        if current_pos == end_pos {
            println!("Shortest distance from start to end: {}", dist_to_current);
            break
        }
        for (i,j) in adj_pos(current_pos) {
            let dist_to_next = dist_to_current + &cave_map[i][j];
            let heuristic_dist_to_next = dist_to_next + manhattan((i,j));
            match distance_map.get_mut(&(i,j)) {
                Some(neighbor_dist) => {
                    if dist_to_next < *neighbor_dist {
                        *neighbor_dist = dist_to_next;
                        exploration_queue.push((i,j), heuristic_dist_to_next);
                        //came_from_map.insert((i,j), current_pos);
                    }
                },
                None => {
                    distance_map.insert((i,j), dist_to_next);
                    exploration_queue.push((i,j), heuristic_dist_to_next);
                    //came_from_map.insert((i,j), current_pos);
                }
            }
        }
    }
    /*
    let mut next_on_path = came_from_map.get(&end_pos).unwrap();
    print!("end <- ");
    loop {
        print!("{:?} <- ", next_on_path);
        if let Some(next) = came_from_map.get(&next_on_path) {
            next_on_path = next;
        } else {break}
    }
    */
    }

    //// Part 2
    {
        let adjusted_risk = |i: usize, j: usize, i_chunk: u32, j_chunk: u32| {
            let risk = cave_map[i][j];
            (risk-1 + i_chunk + j_chunk) % 9 + 1
        };
        let big_map = {
            let mut tmp_map = Vec::new();
            tmp_map.push(vec![PAD; 5*SIZE + 2]);
            for i in 0..5*SIZE {
                let i_chunk = (i / SIZE) as u32;
                let i_idx = (i%SIZE) + 1;
                let mut new_row = Vec::new();
                new_row.push(PAD);
                for j in 0..5*SIZE {
                    let j_chunk = (j / SIZE) as u32;
                    let j_idx = (j%SIZE) + 1;
                    new_row.push(adjusted_risk(i_idx, j_idx, i_chunk, j_chunk));
                }
                new_row.push(PAD);
                tmp_map.push(new_row);
            }
            tmp_map.push(vec![PAD; 5*SIZE + 2]);
            tmp_map
        };


        let mut exploration_queue = MinHeap::new();
        let mut distance_map: HashMap<(usize, usize), u32> = HashMap::new();
        //let mut came_from_map: HashMap<(usize, usize), (usize, usize)> = HashMap::new();

        let start_pos = (1,1);
        let end_pos = (5*SIZE,5*SIZE);

        let manhattan = |pos: (usize, usize)| {
            (end_pos.0 + end_pos.1 - pos.0 - pos.1) as u32
        };

        distance_map.insert(start_pos, 0);
        exploration_queue.push(start_pos, 0);
        while !exploration_queue.is_empty() {
            let (current_pos, _) = exploration_queue.pop().unwrap();
            let dist_to_current = *distance_map.get(&current_pos).unwrap();
            //println!("Currently in {:?} at distance {}", current_pos, dist_to_current);
            if current_pos == end_pos {
                println!("Shortest distance from start to end: {}", dist_to_current);
                break
            }
            for (i,j) in adj_pos(current_pos) {
                let dist_to_next = dist_to_current + big_map[i][j];
                let heuristic_dist_to_next = dist_to_next + manhattan((i,j));
                match distance_map.get_mut(&(i,j)) {
                    Some(neighbor_dist) => {
                        if dist_to_next < *neighbor_dist {
                            *neighbor_dist = dist_to_next;
                            exploration_queue.push((i,j), heuristic_dist_to_next);
                            //came_from_map.insert((i,j), current_pos);
                        }
                    },
                    None => {
                        distance_map.insert((i,j), dist_to_next);
                        exploration_queue.push((i,j), heuristic_dist_to_next);
                        //came_from_map.insert((i,j), current_pos);
                    }
                }
            }
        }
    }
}

*/

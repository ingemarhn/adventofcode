pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let bits = parse_input(lines);

    let top_pack = parse_packet(&mut bits.iter());
    println!("Version sum: {}", top_pack.fold_ver(0));
}

fn task_b(lines: Vec<String>) {
    let bits = parse_input(lines);

    let mut top_pack = parse_packet(&mut bits.iter());

    top_pack.calc_n_get_val();

    println!("Packet value: {}", top_pack.val);
}

type Packets = Vec<Packet>;

#[derive(Debug, Clone)]
struct Packet {
    version: u32,
    type_id: u32,
    val: u128,
    sub_packs: Packets,
}

impl Packet {
    fn new(v: u32, t: u32) -> Self {
        Packet {version: v, type_id: t, val: 0, sub_packs: Vec::new()}
    }

    fn add(&mut self, p: Packet) {
        self.sub_packs.push(p);
    }

    fn set_val(&mut self, v: u128) {
        self.val = v;
    }

    fn fold_ver(&self, sum: u32) -> u32 {
        self.sub_packs.iter().fold(sum + self.version, |s, p| p.fold_ver(s))
    }

    fn calc_n_get_val(&mut self) -> u128 {
        let v = match self.type_id {
            0 => self.sub_packs.iter_mut().fold(0, |pr, p| pr + p.calc_n_get_val()),
            1 => self.sub_packs.iter_mut().fold(1, |pr, p| pr * p.calc_n_get_val()),
            2 => self.sub_packs.iter_mut().fold(u128::MAX, |m, p| m.min(p.calc_n_get_val())),
            3 => self.sub_packs.iter_mut().fold(0, |m, p| m.max(p.calc_n_get_val())),
            5 => {
                for p in self.sub_packs.iter_mut() { p.calc_n_get_val(); }
                if self.sub_packs[0].val > self.sub_packs[1].val { 1 } else { 0 }
            },
            6 => {
                for p in self.sub_packs.iter_mut() { p.calc_n_get_val(); }
                if self.sub_packs[0].val < self.sub_packs[1].val { 1 } else { 0 }
            },
            7 => {
                for p in self.sub_packs.iter_mut() { p.calc_n_get_val(); }
                if self.sub_packs[0].val == self.sub_packs[1].val { 1 } else { 0 }
            },
            _ => 0
        };

        if self.type_id != 4 {
            self.set_val(v);
        }

        self.val
    }
}

fn parse_packet<'a, I: Iterator<Item = &'a u8>>(i_bits: &mut I) -> Packet {
    let ver = get_n_bits_val(i_bits, 3);
    let t_id = get_n_bits_val(i_bits, 3);
    let mut pack = Packet::new(ver, t_id);

    if t_id == 4 {
        let val = get_lit_val(i_bits);
        pack.set_val(val);
    } else {
        let length_type_id = i_bits.next().unwrap();

        if *length_type_id == 0 {
            let n_bits = get_n_bits_val(i_bits, 15) as usize;
            let s_bits: Vec<u8> = i_bits.take(n_bits).copied().collect();
            let mut s_iter = s_bits.iter().peekable();
            while s_iter.peek().is_some() {
                let p = parse_packet(&mut s_iter);
                pack.add(p);
            }
        } else {
            let n_packs = get_n_bits_val(i_bits, 11);
            for _i in 0..n_packs {
                let p = parse_packet(i_bits);
                pack.add(p);
            }
        }
    }

    pack
}

fn get_n_bits_val<'a, I: Iterator<Item = &'a u8>>(bts: &mut I, n: usize) -> u32 {
    bts.take(n).fold(0, |v, b| v * 2 + *b as u32)
}

fn get_lit_val<'a, I: Iterator<Item = &'a u8>>(bts: &mut I) -> u128 {
    let mut num = 0;
    loop {
        let cont_mark = bts.next().unwrap();
        num = num * 16 + get_n_bits_val(bts, 4) as u128;

        if *cont_mark == 0 {
            break
        }
    }

    num
}

fn parse_input(lines: Vec<String>) -> Vec<u8> {
    lines[0]
        .chars()
        .flat_map(|c| {
            let d = c.to_digit(16).unwrap() as u8;
            let mut m = 0b1000;
            let mut ds = Vec::new();
            for sh in (0..4).rev() {
                ds.push((d & m) >> sh);
                m /= 2;
            }

            ds
        })
        .collect()
}

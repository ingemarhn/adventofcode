pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let players = parse_input(lines);
    let prod = play_naive(players);

    println!("Multiplied score: {}", prod);
}

fn task_b(lines: Vec<String>) {
    let players = parse_input(lines);
    let prod = play_advanced(players);

    println!("No of universes: {}", prod);
}

#[derive(Debug, Clone, Copy)]
struct Player {
    position: u32,
    points: u32,
}

impl Player {
    fn new(pos: char) -> Self {
        Self { position: pos.to_digit(10).unwrap(), points: 0 }
    }

    fn move_pos_naive(&mut self, dice_vals: Vec<u32>) -> bool {
        let moves = dice_vals.iter().sum();
        self.move_me(moves, 1000)
    }

    fn move_pos_advanced(&mut self, moves: u32) -> bool {
        self.move_me(moves, 21)
    }

    fn move_me(&mut self, moves: u32, win_limit: u32) -> bool {
        self.position = (self.position + moves - 1) % 10 + 1;
        self.points += self.position;

        self.points >= win_limit
    }
}

#[derive(Debug, Clone)]
struct Game {
    players: Vec<Player>,
    player_ind: usize,
    n_worlds: u64,
}

impl Game {
    fn new(players: Vec<Player>, player_ind: usize, n_worlds: u64) -> Self {
        Self { players, player_ind, n_worlds }
    }

    fn play(&mut self, moves: &u32, freq: &u32) -> bool {
        self.player_ind ^= 1;
        self.n_worlds *= *freq as u64;
        self.players[self.player_ind].move_pos_advanced(*moves)
    }
}

fn play_naive(players_in: Vec<Player>) -> u32 {
    let mut players = players_in;

    let mut index = 0;
    let mut dice = 1..;
    let mut next_3 = || (0..3).map(|_| dice.next().unwrap()).collect::<Vec<u32>>();
    let mut n_dice = 0;
    loop {
        n_dice += 3;
        if players[index].move_pos_naive(next_3()) {
            let loser_point = players[index ^ 1].points;
            return n_dice * loser_point
        }

        index ^= 1;
    }
}

fn play_advanced(players_in: Vec<Player>) -> u64 {
    // dice rolls outcome and frequency
    // 3 occurs one time (1+1+1), 4 occurs 3 times (1+1+2, 1+2+1, 2+1+1), 5 occurs 6 times ...
    // I.e. a large number of parallel worlds are identical and only need to be calculated once.
    let dice_sum_freq = vec![(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)];
    let mut games = Vec::new();
    // Create initial game. From start there is only one world.
    games.push(Game::new(players_in, 1, 1));
    let mut player_wins = [0u64, 0];
    // It doesn't matter in which order we play the worlds, so we can just push and pop games onto a vector
    while let Some(game) = games.pop() {
        for (moves, freq) in &dice_sum_freq {
            // Start each new game from the last popped game because every roll splits into new worlds.
            let mut game = game.clone();
            if game.play(moves, freq) {
                // After a win the current isn't interesting any more and can be dropped.
                player_wins[game.player_ind] += game.n_worlds;
            } else {
                // Push the new calculated game onto the stack
                games.push(game);
            }
        }
        // The current popped game isn't interesting anymore. It has either gone to a winning state or it has been splitted into new worlds.
        // Either way, the old game can be dropped.
    }

    player_wins[0].max(player_wins[1])
}

fn parse_input(lines: Vec<String>) -> Vec<Player> {
    let players = lines
        .iter()
        .map(|l| l.chars())
        .map(|mut lc| Player::new(lc.nth(28).unwrap()))
        .collect();

    players
}

mod day1; mod day2; mod day3; mod day4; mod day5; mod day6; mod day7; mod day8; mod day9; mod day10; mod day11; mod day12; mod day13; mod day14; mod day15; mod day16; mod day17; mod day18; mod day19; mod day20; mod day21; mod day22; mod day23; mod day24; mod day25;

fn main() {
    let (day, task, sample) = common::parse_command_line(std::env::args().collect());
    let (task, sample) = (&task, sample.as_ref());

    match String::as_str(&day) {
        "1" => day1::run_task(task, sample),
        "2" => day2::run_task(task, sample),
        "3" => day3::run_task(task, sample),
        "4" => day4::run_task(task, sample),
        "5" => day5::run_task(task, sample),
        "6" => day6::run_task(task, sample),
        "7" => day7::run_task(task, sample),
        "8" => day8::run_task(task, sample),
        "9" => day9::run_task(task, sample),
        "10" => day10::run_task(task, sample),
        "11" => day11::run_task(task, sample),
        "12" => day12::run_task(task, sample),
        "13" => day13::run_task(task, sample),
        "14" => day14::run_task(task, sample),
        "15" => day15::run_task(task, sample),
        "16" => day16::run_task(task, sample),
        "17" => day17::run_task(task, sample),
        "18" => day18::run_task(task, sample),
        "19" => day19::run_task(task, sample),
        "20" => day20::run_task(task, sample),
        "21" => day21::run_task(task, sample),
        "22" => day22::run_task(task, sample),
        "23" => day23::run_task(task, sample),
        "24" => day24::run_task(task, sample),
        "25" => day25::run_task(task, sample),
        _  => panic!("Day {} not implemented", day),
    }
}

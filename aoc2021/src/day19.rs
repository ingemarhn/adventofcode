use std::collections::HashSet;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut scanners = parse_input(lines);
    for i in 0..scanners.len() {
        scanners.get_mut(i).unwrap().calc_beac_dist();
    }

    match_scanners(&mut scanners);
    let beacs = scanners.iter().fold(HashSet::new(), |mut h, s|
        {
            for b in &s.detected_beacons {
                h.insert(b);
            }

            h
        }
    );

    println!("# of unique beacons: {}", beacs.len());
}

fn task_b(lines: Vec<String>) {
    let mut scanners = parse_input(lines);
    for i in 0..scanners.len() {
        scanners.get_mut(i).unwrap().calc_beac_dist();
    }

    match_scanners(&mut scanners);
    let mut max_dist = 0;
    for i in 0..scanners.len() - 1 {
        for j in i + 1..scanners.len() {
            let (x, y, z) = sub_positions(scanners[i].position.unwrap(), scanners[j].position.unwrap());
            let dist = x.abs() + y.abs() + z.abs();
            if dist > max_dist {
                max_dist = dist;
            }
        }
    }

    println!("Max beacon distance: {}", max_dist);
}

static N_ORIENTATIONS: usize = 24;
static MIN_COMMON: usize = N_ORIENTATIONS / 2;
type Coord = i32;
type Position = (Coord, Coord, Coord);
type Scanners = Vec<Scanner>;

#[derive(Clone, Debug)]
struct BeacDist {
    distance: Coord,
    beacons: (usize, usize),
}

#[derive(Clone, Debug)]
struct Scanner {
    // number: u8,
    position: Option<Position>,
    orientation: Option<usize>,
    detected_beacons: Vec<Position>,
    beac_dists: Vec<BeacDist>,
}

impl Scanner {
    fn new(_num: u8) -> Self {
        Self { /* number: num, */ position: None, orientation: None, detected_beacons: Vec::new(), beac_dists: Vec::new() }
    }

    fn set_position(&mut self, pos: Position) {
        self.position = Some(pos);
    }

    fn set_orientation(&mut self, orient: usize) {
        self.orientation = Some(orient);
    }

    fn set_beacons_pos(&mut self) {
        let o = self.orientation.unwrap();
        for i in 0..self.detected_beacons.len() {
            let b = self.detected_beacons.get_mut(i).unwrap();
            let bp = get_pos_from_orient(*b, o);
            let sp = self.position.unwrap();
            let x = sp.0 + bp.0;
            let y = sp.1 + bp.1;
            let z = sp.2 + bp.2;
            *b = (x, y, z);
        }
    }

    fn add_beacon(&mut self, coords: Vec<Coord>) {
        self.detected_beacons.push((coords[0], coords[1], coords[2]));
    }

    fn calc_beac_dist(&mut self) {
        let d_len = self.detected_beacons.len();
        for i in 0..d_len-1 {
            for j in i+1..d_len {
                let dist = (self.detected_beacons[i].0 - self.detected_beacons[j].0).pow(2) +
                           (self.detected_beacons[i].1 - self.detected_beacons[j].1).pow(2) +
                           (self.detected_beacons[i].2 - self.detected_beacons[j].2).pow(2);

                self.beac_dists.push(BeacDist { distance: dist, beacons: (i, j) });
            }
        }

        self.beac_dists.sort_by(|a, b| { a.distance.cmp(&b.distance)});
    }

    fn find_dist_index(&self, dist: &Coord) -> Option<usize> {
        if let Ok(bd) = self.beac_dists.binary_search_by(|bd| bd.distance.cmp(dist) ) {
            Some(bd)
        } else {
            None
        }
    }

    fn align(&mut self, base: &Scanner, ((b_b1i, b_b2i), (s_b1i, s_b2i)): ((usize, usize), (usize, usize))) {
        let get_pos = |i: usize, pos: Position| {
            sub_positions(base.detected_beacons[i], pos)
        };

        for o in 0..N_ORIENTATIONS {
            let b1_pos = get_pos_from_orient(self.detected_beacons[s_b1i], o);
            let b2_pos = get_pos_from_orient(self.detected_beacons[s_b2i], o);
            let self_pos_op = if get_pos(b_b1i, b1_pos) == get_pos(b_b2i, b2_pos) {
                Some(get_pos(b_b1i, b1_pos))
            } else if get_pos(b_b1i, b2_pos) == get_pos(b_b2i, b1_pos) {
                Some(get_pos(b_b1i, b2_pos))
            } else {
                None
            };
            if let Some(self_pos) = self_pos_op {
                self.set_orientation(o);
                self.set_position(self_pos);
                self.set_beacons_pos();

                break;
            }
        }
    }
}

fn get_pos_from_orient((a, b, c): Position, pos_num: usize) -> Position {
    if pos_num == 0 {
        return (a, b, c);
    }

    let mut coords = vec![a, b, c];
    let pnd4 = pos_num / 4;
    let xi = pnd4 % 3;
    let x = if pos_num < MIN_COMMON { coords.remove(xi) } else { -coords.remove(xi) };
    let pnm4 = pos_num % 4;
    let yi = pnm4 % 2;
    let y = if pnm4 < 2 { coords.remove(yi) } else { -coords.remove(yi) };
    let z = if pnd4 % 2 == 0 {
        if [0, 3].contains(&pnm4) { coords[0] } else { -coords[0] }
    } else if [1, 2].contains(&pnm4) {
        coords[0] } else { -coords[0]
    };

    (x, y, z)
}

fn match_scanners(scanners: &mut Scanners) {
    use std::collections::VecDeque;

    let scan0 = scanners.get_mut(0).unwrap();
    scan0.set_position((0, 0, 0));
    scan0.set_orientation(0);
    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();
    queue.push_back(0);
    visited.insert(0);

    let s_len = scanners.len();
    while let Some(scan_ind) = queue.pop_front() {
        let scan_base = scanners.get(scan_ind).unwrap().clone();
        for i in 0..s_len {
            if visited.contains(&i) { continue }
            let last_pairs_op = {
                let cand = scanners.get(i).unwrap();
                try_match(&scan_base, cand)
            };

            if let Some(last_pairs) = last_pairs_op {
                queue.push_back(i);
                visited.insert(i);

                let beac = scanners.get_mut(i).unwrap();
                beac.align(&scan_base, last_pairs);
            }
        }
    }
}

fn try_match(base_in: &Scanner, candidate_in: &Scanner) -> Option<((usize, usize), (usize, usize))> {
    let mut count = 0;
    let mut last_pairs = None;
    for b_dist in &candidate_in.beac_dists {
        if let Some(found_ind) = base_in.find_dist_index(&b_dist.distance) {
            count += 1;
            if count == 66 {
                last_pairs = Some((base_in.beac_dists[found_ind].beacons, b_dist.beacons));
                break;
            }
        }
    }

    last_pairs
}

fn sub_positions((x1, y1, z1): Position, (x2, y2, z2): Position) -> Position {
    let x = x1 - x2;
    let y = y1 - y2;
    let z = z1 - z2;

    (x, y, z)
}

fn parse_input(lines: Vec<String>) -> Scanners {
    let mut scanners = Vec::new();

    for line in lines {
        if line.starts_with("--- scanner ") {
            let scanner = Scanner::new(line[12..=13].trim().parse::<u8>().unwrap());
            scanners.push(scanner);
        } else if !line.is_empty() {
            let coords = line.split(',').map(|c| c.parse::<Coord>().unwrap() ).collect();
            let s_len = scanners.len();
            let s = scanners.get_mut(s_len - 1).unwrap();
            s.add_beacon(coords);
        }
    }

    scanners
}

use std::collections::VecDeque;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let mut elements = parse_input(lines);
    let elements = get_final_sum(&mut elements);
    let magnitude = calc_magnitude(elements);

    println!("Magnitude is: {}", magnitude);
}

fn task_b(lines: Vec<String>) {
    let elements = parse_input(lines);
    let mut max_magnitude = 0;

    for i1 in 0..elements.len()-1 {
        for i2 in 1..elements.len() {
            let mut e1 = elements.get(i1).unwrap().clone();
            reduce(&mut e1, elements.get(i2).unwrap().clone());
            let magnitude = calc_magnitude(e1);
            if magnitude > max_magnitude {
                max_magnitude = magnitude;
            }
        }
    }

    println!("Maximum magnitude is: {}", max_magnitude);
}

type NumT = u32;
type ElemVec = VecDeque<Element>;

#[derive(Clone, Debug, PartialEq)]
enum Element {
    Lpar,
    Rpar,
    Comma,
    Num(NumT),
}
use Element::*;

use std::fmt;
impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Lpar => "[".to_string(),
            Rpar => "]".to_string(),
            Comma => ",".to_string(),
            Num(n) => format!("{}", n),
        };
        write!(f, "{}", s)
    }
}

fn parse_input(lines: Vec<String>) -> Vec<ElemVec> {
    let mut elems_vec = Vec::new();

    for line in lines {
        let mut line_elems = VecDeque::new();
        for c in line.chars() {
            match c {
                '[' => line_elems.push_back(Lpar),
                ']' => line_elems.push_back(Rpar),
                ',' => line_elems.push_back(Comma),
                _   => line_elems.push_back(Num(c.to_digit(10).unwrap() as NumT)),
            }
        }
        elems_vec.push(line_elems);
    }

    elems_vec
}

fn reduce(elements: &mut ElemVec, add: ElemVec) {
    let referens = vec![Lpar, Num(0), Comma, Num(0)];

    let mut elems = add;
    elements.push_front(Lpar);
    elements.push_back(Comma);
    elements.append(&mut elems);
    elements.push_back(Rpar);

    'l: loop {
        let start_len = elements.len();
        let mut grp_count = 0;
        let mut sequence = Vec::new();
        let mut ind = 0;
        loop {
            match elements[ind] {
                Lpar => {
                    grp_count += 1;
                    sequence = vec![Lpar];
                },
                Rpar => {
                    grp_count -= 1;
                    if grp_count >= 4 && sequence == referens {
                        let num_pair = [ind - 3, ind - 1]
                            .iter()
                            .fold(Vec::new(), |mut v, &e| { if let Num(n) = &elements.get(e).unwrap() { v.push(*n) }; v });
                        let mut lni = ind - 5;
                        while lni > 0 {
                            if let Num(l_num) = elements.get_mut(lni).unwrap() {
                                *l_num += num_pair[0];
                                break;
                            }

                            lni -= 1;
                        }
                        lni = ind + 1;
                        while lni < elements.len() {
                            if let Num(l_num) = elements.get_mut(lni).unwrap() {
                                *l_num += num_pair[1];
                                break;
                            }

                            lni += 1;
                        }

                        *elements.get_mut(ind - 4).unwrap() = Num(0);
                        for i in (ind - 3..=ind).rev() {
                            elements.remove(i);
                        }

                        continue 'l;
                    }
                },
                Comma => sequence.push(Comma),
                Num(_) => sequence.push(Num(0)),
            }

            ind += 1;
            if ind >= elements.len() {
                break;
            }
        }

        if elements.len() != start_len {
            continue;
        }

        ind = 1;
        loop {
            // Find values larger than 9 and split them
            if let Num(num) = elements[ind] {
                if num > 9 {
                    let l_num = num / 2;
                    let r_num = num - l_num;
                    *elements.get_mut(ind).unwrap() = Lpar;
                    elements.insert(ind + 1, Num(l_num));
                    elements.insert(ind + 2, Comma);
                    elements.insert(ind + 3, Num(r_num));
                    elements.insert(ind + 4, Rpar);

                    continue 'l;
                }
            }

            ind += 1;
            if ind >= elements.len() {
                break;
            }
        }

        // No explodes or no splits found
        break;
    }
}

fn get_final_sum(elements: &mut [ElemVec]) -> ElemVec {
    let mut added_elements = elements.get_mut(0).unwrap().clone();
    for elems in elements.iter().skip(1) {
        reduce(&mut added_elements, elems.clone())
    }

    added_elements
}

fn calc_magnitude(elements: ElemVec) -> u32 {
    let referens = vec![Lpar, Num(0), Comma, Num(0)];

    let mut elements = elements;
    'l: loop {
        let start_len = elements.len();
        let mut sequence = Vec::new();
        let mut ind = 0;
        loop {
            match elements[ind] {
                Lpar => {
                    sequence = vec![Lpar];
                },
                Rpar => {
                    if sequence == referens {
                        let num_pair = [ind - 3, ind - 1]
                            .iter()
                            .fold(Vec::new(), |mut v, &e| { if let Num(n) = &elements.get(e).unwrap() { v.push(*n) }; v });

                        *elements.get_mut(ind - 4).unwrap() = Num(num_pair[0] * 3 + num_pair[1] * 2);
                        for i in (ind - 3..=ind).rev() {
                            elements.remove(i);
                        }

                        continue 'l;
                    }
                },
                Comma => sequence.push(Comma),
                Num(_) => sequence.push(Num(0)),
            }

            ind += 1;
            if ind >= elements.len() {
                break;
            }
        }

        if elements.len() == start_len {
            break;
        }
    }

    if let Num(magnitude) = elements[0] {
        return magnitude;
    }

    0
}

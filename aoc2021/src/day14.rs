use std::collections::HashMap;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (mut chr_list, pir) = parse_input(lines);
    for _ in 0..10 {
        insert_chars(&mut chr_list, &pir);
    }

    let (small, big) = count_elements(&chr_list);

    println!("Diff: {}", big - small);
}

fn task_b(lines: Vec<String>) {
    let (chr_pairs, pir) = parse_input_b(lines);

    let mut pair_count: HashMap<Pair, usize> = chr_pairs
        .iter()
        .map(|p| (*p, 1))
        .collect();

    // let get_count = |p: &Option<&usize>| -> usize {
    //     if let Some(c) = dbg!(p) { **c + 1 } else { 1 }
    // };
    for _ in 0..40 {
        pair_count = pair_count
            .iter()
            .fold(HashMap::new(), |mut pair_count_new, (pair, &count)| {
                let c = pir[pair];
                let p1 = (pair.0, c);
                let p2 = (c, pair.1);
                let pcount = pair_count_new.entry(p1).or_insert(0);
                *pcount += count;
                let pcount = pair_count_new.entry(p2).or_insert(0);
                *pcount += count;
                pair_count_new
            });
    }

    let mut count_map = pair_count
        .iter()
        .fold(HashMap::new(), |mut map, ((_, c), cnt)| {*map.entry(c).or_insert(0) += cnt; map});
    count_map.entry(&chr_pairs[0].0).and_modify(|e| { *e += 1 });
    let mut c_count = count_map
        .values()
        .collect::<Vec<&usize>>();
    c_count.sort();

    println!("Diff: {}", *c_count[c_count.len() - 1] - *c_count[0]);
}

fn insert_chars(chr_lst: &mut List<char>, pir: &Pir) {
    let mut c = chr_lst.head();
    loop {
        let p = chr_lst.peek();
        if p.is_none() {
            break
        }

        let s = [c, p].iter().fold(String::new(), |mut s, e| {s.push(e.unwrap()); s});
        if let Some(ch) = pir.get(&s) {
            chr_lst.insert(*ch);
        }

        c = chr_lst.next();
    }
}

fn count_elements(chr_lst: &List<char>) -> (usize, usize) {
    let mut counts = HashMap::new();
    for c in 'A'..='Z' {
        counts.insert(c, 0usize);
    }
    let mut lst_clone = chr_lst.clone();
    let mut c = lst_clone.head();
    while c.is_some() {
        let cc = c.unwrap();
        counts.insert(cc, counts.get(&cc).unwrap() + 1);
        c = lst_clone.next();
    }

    let mut small = (' ', usize::MAX);
    let mut big   = (' ', 0);
    for (&k, &v) in counts.iter() {
        if v < small.1 && v > 0 {
            small = (k, v);
        }
        if v > big.1 {
            big = (k, v);
        }
    }

    (small.1, big.1)
}

type Link = Option<usize>;
type Pair = (char, char);
type Pir = HashMap<String, char>;
type Pir2 = HashMap<Pair, char>;

#[derive(Debug, Clone)]
struct List<T: Copy> {
    nodes: Vec<Node<T>>,
    current: Link,
    last: Link,
}

impl<T: Copy + std::fmt::Display> List<T> {
    fn new() -> Self {
        List {
            nodes: Vec::new(),
            current: None,
            last: None,
        }
    }

    fn push(&mut self, elem: T) {
        let node = Node::new(elem, None);
        self.nodes.push(node);
        let ind = Some(self.nodes.len() - 1);
        if let Some(next) = self.last {
            self.nodes[next].next_node = ind;
        }
        self.current = ind;
        self.last = ind;
    }

    fn insert(&mut self, elem: T) {
        let cur_ind = self.current.unwrap();
        let new_node = Node::new(elem, self.nodes[cur_ind].next_node);
        self.nodes.push(new_node);
        let new_ind = Some(self.nodes.len() - 1);
        self.nodes[cur_ind].next_node = new_ind;
        self.current = new_ind;
    }

    fn head(&mut self) -> Option<T> {
        self.current = Some(0);
        self.get_curr_val()
    }

    fn next(&mut self) -> Option<T> {
        self.current = self.current
            .and_then(|current| self.nodes[current].next_node);
        self.get_curr_val()
    }

    fn get_curr_val(&self) -> Option<T> {
        self.current
            .map(|current| self.nodes[current].elem)
    }

    fn peek(&self) -> Option<T> {
        self.current
            .and_then(|current| self.nodes[current].next_node)
            .map(|next| self.nodes[next].elem)
    }

    #[allow(dead_code)]
    fn len(&self) -> usize {
        self.nodes.len()
    }

    #[allow(dead_code)]
    fn print(&self) {
        let mut s_clone = self.clone();
        let mut c = s_clone.head();
        while c.is_some() {
            print!("{}", c.unwrap());
            c = s_clone.next();
        }
        println!();
    }
}

#[derive(Debug, Clone)]
struct Node<T: Copy> {
    elem: T,
    next_node: Link,
}

impl<T: Copy> Node<T> {
    fn new(elem: T, next: Link) -> Self {
        Node { elem, next_node: next }
    }
}

fn parse_input(lines: Vec<String>) -> (List<char>, Pir) {
    let mut l_iter = lines.iter();
    let mut chrs = List::<char>::new();
    for c in l_iter.next().unwrap().chars() {
        chrs.push(c);
    }

    let pir = l_iter
        .skip(1)
        .map(|l| l.split_once(" -> ").unwrap())
        .map(|e| (e.0.to_string(), e.1.chars().next().unwrap()))
        .collect();

    (chrs, pir)
}

fn parse_input_b(lines: Vec<String>) -> (Vec<Pair>, Pir2) {
    let mut l_iter = lines.iter();
    let pairs: Vec<Pair> = l_iter
        .next().unwrap()
        .chars()
        .collect::<Vec<char>>()
        .windows(2)
        .map(|cr|
            ((*cr)[0], (*cr)[1])
        )
        .collect();

    let nchr = |s: &str, n: usize| { s.chars().nth(n).unwrap() };
    let pir = l_iter
        .skip(1)
        .map(|l| l.split_once(" -> ").unwrap())
        .map(|e| ((nchr(e.0, 0), nchr(e.0, 1)), nchr(e.1, 0)))
        .collect();

    (pairs, pir)
}

// === The most elegant solution!
/*
use std::collections::HashMap;
use std::time::Instant;

type Element = char;
type Pair = (Element, Element);

fn simulate(steps: usize) -> usize {
    // Parse input and rules.
    let mut lines = include_str!("/home/ingemar/adventofcode/aoc2021/input/day-14.txt").lines();
    let input: Vec<Element> = lines.next().unwrap().chars().collect();
    let rules: HashMap<Pair, Vec<Pair>> = lines
        .skip(1)
        .map(|line| line.split_once(" -> ").unwrap())
        .map(|(from, to)| {
            // Turn "AB -> X" into "AB -> [AX, XB]"
            let a = from.chars().next().unwrap();
            let b = from.chars().nth(1).unwrap();
            let x = to.chars().next().unwrap();

            ((a, b), vec![(a, x), (x, b)])
        })
        .collect();

    // Count initial pairs.
    let mut pair_counts: HashMap<Pair, usize> = HashMap::new();
    input
        .windows(2)
        .map(|window| (window[0], window[1])) // Turn vector of length 2 into pair.
        .for_each(|pair| *pair_counts.entry(pair).or_default() += 1);

    // Run simulation.
    for _ in 0..steps {
        pair_counts = pair_counts
            .iter()
            .fold(HashMap::new(), |mut counts, (pair, count)| {
                // AB occurs n times and maps to AX and XB -> Increase counts of AX and of XB by n.
                rules[pair]
                    .iter()
                    .for_each(|pair2| *counts.entry(*pair2).or_default() += count);
                counts
            });
    }

    // Count the first elements of each pair.
    let mut element_counts: HashMap<Element, usize> = HashMap::new();
    for (pair, count) in &pair_counts {
        *element_counts.entry(pair.0).or_default() += count;
    }
    // Count the last element of the initial (and final) input.
    let last = input.last().unwrap();
    *element_counts.entry(*last).or_default() += 1;

    // Calculate result as per problem statement.
    let min = element_counts.values().min().unwrap();
    let max = element_counts.values().max().unwrap();
    max - min
}

fn main() {
    let start_time = Instant::now();
    println!("Part 1: {}", simulate(10));
    let duration = start_time.elapsed();
    println!("Duration: {:?}", duration);
    let start_time = Instant::now();
    println!("Part 2: {}", simulate(40));
    let duration = start_time.elapsed();
    println!("Duration: {:?}", duration);
}
*/

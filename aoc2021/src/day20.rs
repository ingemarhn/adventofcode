use std::collections::HashMap;
use std::collections::VecDeque;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let (enhancer, mut images) = parse_input(lines);
    for _ in 0..4 {
        images = expand_image(images);
    }
    for _ in 0..2 {
        images = enhance_image(&enhancer, images);
    }

    for _ in 0..2 {
        images.pop_front();
        images.pop_back();
        for images_r in &mut images {
            images_r.pop_front();
            images_r.pop_back();
        }
    }

    let n_lit = images.iter().fold(0, |c, r| c + r.iter().fold(0, |c, b| c + *b));
    println!("# of lit pixels: {}", n_lit);
}

fn task_b(lines: Vec<String>) {
    let n_enhance = 50;
    let (enhancer, mut images) = parse_input(lines);

    for _ in 0..n_enhance * 2 {
        images = expand_image(images);
    }
    for _ in 0..n_enhance {
        images = enhance_image(&enhancer, images);
    }

    for _ in 0..n_enhance {
        images.pop_front();
        images.pop_back();
        for images_r in &mut images {
            images_r.pop_front();
            images_r.pop_back();
        }
    }

    let n_lit = images.iter().fold(0, |c, r| c + r.iter().fold(0, |c, b| c + *b));
    println!("# of lit pixels: {}", n_lit);
}

type Enhancer = Vec<u32>;
type Image = VecDeque<VecDeque<u32>>;

fn expand_image(image: Image) -> Image {
    let mut new_image = image;

    for new_image_i in &mut new_image {
        new_image_i.push_front(0);
        new_image_i.push_back(0);
    }
    let row_len = new_image[0].len();
    new_image.push_front(vec![0; row_len].into());
    new_image.push_back(vec![0; row_len].into());

    new_image
}

fn enhance_image(enhancer: &Enhancer, image: Image) -> Image {
    let mut new_image = image;
    let start_image = new_image.clone();

    for ri in 0..new_image.len() - 2 {
        for ci in 0..new_image[0].len() - 2 {
            let mut ind = 0;
            for rsi in 0..=2 {
                for csi in ci..=ci + 2 {
                    ind = (ind << 1) | start_image[ri + rsi][csi];
                }
            }

            new_image[ri + 1][ci + 1] = enhancer[ind as usize];
        }
    }

    new_image
}

fn parse_input(lines: Vec<String>) -> (Enhancer, Image) {
    let binary = HashMap::from([ ('.', 0), ('#', 1), ]);
    let enhancer: Enhancer = lines[0].chars().map(|c| binary[&c]).collect();
    let image = lines
        .iter()
        .skip(2)
        .map(|l| l.chars().map(|c| binary[&c]).collect::<VecDeque<u32>>() )
        .collect();

    (enhancer, image)
}

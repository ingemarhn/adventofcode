pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let n_fishes = calc_fishes(lines, 80);
    println!("# of fishes: {}", n_fishes);
}

fn task_b(lines: Vec<String>) {
    let n_fishes = calc_fishes(lines, 256);
    println!("# of fishes: {}", n_fishes);
}

fn calc_fishes(lines: Vec<String>, days: u32) -> u64 {
    let fish_states: Vec<usize> = lines[0].split(',').map(|n| n.parse().unwrap()).collect();
    let mut fish_counts = [0u64; 9];
    for f in fish_states {
        fish_counts[f] += 1;
    }

    for _i in 0..days {
        let t = fish_counts[0];
        for d in 0..8 {
            fish_counts[d] = fish_counts[d + 1];
        }
        fish_counts[8] = t;
        fish_counts[6] += t;
    }

    fish_counts.iter().cloned().sum::<u64>()
}

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let digits = parse_input(lines);

    let mut count = 0;
    for digit_line in &digits {
        for dl in digit_line.iter().take(14).skip(10) {
            if [2, 3, 4, 7].contains(&(dl.len())) {
                count += 1;
            }
        }
    }

    println!("Unique digits appear {} times", count);
}

fn task_b(lines: Vec<String>) {
    use std::collections::HashMap;

    let digit_patterns = parse_input(lines);
    let mut added_values = 0;

    for patterns in &digit_patterns {
        let mut digit_connections = [' '; 7];
        let mut dig_pairs = HashMap::new();

        // 1
        dig_pairs.insert((2, 5), patterns[0].chars().collect::<Vec<char>>());

        // 7
        for c in patterns[1].chars() {
            if !dig_pairs[&(2, 5)].contains(&c) {
                digit_connections[0] = c;
                break;
            }
        }

        // 4
        let mut vc = Vec::new();
        for c in patterns[2].chars() {
            if !dig_pairs[&(2, 5)].contains(&c) {
                vc.push(c);
            }
        }
        dig_pairs.insert((1, 3), vc);

        // 2, 3, 5
        let cnt_chrs = |chrs: &Vec<char>, s: &String| -> usize {
            chrs.iter().fold(0, |cnt, c| if s.contains(*c) { cnt + 1 } else { cnt })
        };

        let mut two = String::new();
        for p_i in patterns.iter().take(5 + 1).skip(3) {
            if cnt_chrs(&dig_pairs[&(2, 5)], p_i) == 2 {
                // 3
                for c in p_i.chars() {
                    if digit_connections[0] == c { continue }
                    if dig_pairs[&(2, 5)].contains(&c) { continue }
                    if dig_pairs[&(1, 3)].contains(&c) {
                        digit_connections[3] = c;
                        continue;
                    }
                    digit_connections[6] = c;
                }
            } else if cnt_chrs(&dig_pairs[&(1, 3)], p_i) == 2 {
                // 5
                for c in p_i.chars() {
                    if digit_connections[0] == c { continue }
                    if dig_pairs[&(1, 3)].contains(&c) { continue }
                    if dig_pairs[&(2, 5)].contains(&c) {
                        digit_connections[5] = c;
                        continue;
                    }
                }
            } else {
                // 2
                two.clone_from(p_i);
                for c in p_i.chars() {
                    if dig_pairs[&(2, 5)].contains(&c) {
                        digit_connections[2] = c;
                        break;
                    }
                }
            }
        }

        for c in two.chars() {
            if digit_connections[0] != c && !dig_pairs[&(1, 3)].contains(&c) && !dig_pairs[&(2, 5)].contains(&c) && digit_connections[6] != c {
                digit_connections[4] = c;
            }
        }

        for c in patterns[2].chars() {
            if !dig_pairs[&(2, 5)].contains(&c) && digit_connections[3] != c {
                digit_connections[1] = c;
            }
        }

        let dc_string = |ixs: Vec<usize>| -> String {
            let mut v = Vec::new();
            for i in ixs {
                v.push(digit_connections[i]);
            }
            v.sort();
            v.into_iter().collect()
        };

        let digits = vec![
            dc_string(vec![0,1,2,4,5,6]),
            dc_string(vec![2,5]),
            dc_string(vec![0,2,3,4,6]),
            dc_string(vec![0,2,3,5,6]),
            dc_string(vec![1,2,3,5]),
            dc_string(vec![0,1,3,5,6]),
            dc_string(vec![0,1,3,4,5,6]),
            dc_string(vec![0,2,5]),
            dc_string(vec![0,1,2,3,4,5,6]),
            dc_string(vec![0,1,2,3,5,6]),
        ];

        let mut val = 0;
        for s in &patterns[10..] {
            let d = digits.iter().position(|n| n == s).unwrap();
            val = val * 10 + d;
        }
        added_values += val;
    }

    println!("Added values = {}", added_values);
}

type Digits = Vec<Vec<String>>;

fn parse_input(lines: Vec<String>) -> Digits {
    let mut digits = Vec::new();

    for line in lines {
        let mut digs: Vec<String> = Vec::new();
        let mut patterns = line.split(' ');
        for _ in 0..10 {
            let digit = patterns.next().unwrap();
            let mut chrs = digit.chars().collect::<Vec<char>>();
            chrs.sort();
            digs.push(chrs.into_iter().collect());
        }
        digs.sort_unstable_by_key(|a| a.len());

        patterns.next();
        for _ in 0..4 {
            let digit = patterns.next().unwrap();
            let mut chrs = digit.chars().collect::<Vec<char>>();
            chrs.sort();
            digs.push(chrs.into_iter().collect());
        }

        digits.push(digs);
    }

    digits
}
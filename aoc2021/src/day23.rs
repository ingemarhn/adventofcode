use std::collections::HashSet;
use std::char;

pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn exam_positions(burrow_in: &Burrow, amphipods_in: &Amphipods, min_cost: &mut CostType, failed: &mut HashSet<Matrix>) -> Option<CostType> {
    const VALID_COLS: [usize; 7] = [0, 1, 3, 5, 7, 9, 10];

    if failed.contains(&burrow_in.matrix) {
        return None
    }

    if burrow_in.is_cost_too_high(min_cost) {
        return None
    }

    let mut all_failed = true;
    for ap_i in 0..amphipods_in.len() {
        if burrow_in.is_in_dest_pos(amphipods_in, ap_i) {
            continue
        }

        {
            // Try to move to final room
            let mut burrow = burrow_in.clone();
            let mut amphipods = amphipods_in.clone();
            if burrow.move_to_final(&mut amphipods, ap_i) {
                if let Some(cost) = burrow.get_full_cost() {
                    *min_cost = (*min_cost).min(cost);
                    return Some(*min_cost)
                }

                if let Some(cost) = exam_positions(&burrow, &amphipods, min_cost, failed) {
                    *min_cost = (*min_cost).min(cost);
                }
                all_failed = false;

                continue
            }
        }

        if let Some(room_pos) = amphipods_in[ap_i].current_room_pos {
            for c in VALID_COLS {
                let mut burrow = burrow_in.clone();
                let mut amphipods = amphipods_in.clone();
                if burrow.can_move_to(room_pos, (0, c)) {
                    burrow.do_move(amphipods.get_mut(ap_i).unwrap(), 0, c);

                    if let Some(cost) = exam_positions(&burrow, &amphipods, min_cost, failed) {
                        *min_cost = (*min_cost).min(cost);
                    }
                    all_failed = false;
                }
            }
        }
    }

    if all_failed {
        failed.insert(burrow_in.matrix.clone());
    }

    None
}

fn task_a(lines: Vec<String>) {
    let (burrow, amphipods) = parse_input(lines);

    let mut cost = CostType::MAX;
    let mut fail = HashSet::new();
    exam_positions(&burrow, &amphipods, &mut cost, &mut fail);
    println!("cost = {}", cost);
}

fn task_b(lines: Vec<String>) {
    let mut lines = lines;
    let l = lines[3].clone();
    lines[3] = "  #D#C#B#A#".to_string();
    lines[4] = "  #D#B#A#C#".to_string();
    lines.push(l);
    lines.push("".to_string());

    let (burrow, amphipods) = parse_input(lines);

    let mut cost = CostType::MAX;
    let mut fail = HashSet::new();
    exam_positions(&burrow, &amphipods, &mut cost, &mut fail);
    println!("cost = {}", cost);
}

type Id = u8;
type Amphipods = Vec<Amphipod>;
type PosType = usize;
type CostType = u32;
type Position = (PosType, PosType);
type MaybePos = Option<Position>;
type Matrix = Vec<[Option<Id>; 11]>;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
enum APtype {
    Amber,
    Bronze,
    Copper,
    Desert,
}
use APtype::*;

#[derive(Clone, Debug, Eq, PartialEq)]
struct Amphipod {
    id: Id,
    ap_type: APtype,
    // destinations: VecDeque<Position>,
    current_room_pos: MaybePos,
    destination_room_col: PosType,
    cost: CostType,
}

impl Amphipod {
    fn new(id: Id, ch: char, room_pos: Position) -> Self {
        let (t, r, c) = match ch {
            'A' => (Amber, 2, 1),
            'B' => (Bronze, 4, 10),
            'C' => (Copper, 6, 100),
            _   => (Desert, 8, 1000),
        };

        Self { id, ap_type: t, /* destinations: VecDeque::new(), */ current_room_pos: Some(room_pos), destination_room_col: r, cost: c }

    }

    fn set_room(&mut self, row: PosType, col: PosType) {
        if row != 0 {
            self.current_room_pos = Some((row, col));
        } else {
            self.current_room_pos = None;
        }
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Burrow {
    matrix: Matrix,
    amphipods_pos: Vec<Position>,
    n_in_dest: u8,
    curr_cost: CostType,
    #[cfg(debug_assertions)]
    ap_chars: Vec<char>,
}

impl Burrow {
    fn new(amphipods: &Amphipods) -> Self {
        let ap_len = amphipods.len();

        let mut me = Self {
            matrix: vec![[None; 11]; ap_len / 4 + 1],
            amphipods_pos: vec![(0, 0); ap_len],
            n_in_dest: 0,
            curr_cost: 0,
            #[cfg(debug_assertions)]
            ap_chars: vec![' '; ap_len]
        };

        for ap in amphipods {
            let room_pos = ap.current_room_pos.unwrap();
            me.matrix[room_pos.0][room_pos.1] = Some(ap.id);
            me.amphipods_pos[ap.id as usize] = room_pos;
        }

        for ap_i in 0..amphipods.len() {
            if me.is_in_dest_pos(amphipods, ap_i) {
                me.n_in_dest += 1;
            }
        }

        #[cfg(debug_assertions)] {
            use std::collections::HashMap;
            let mut c_count = HashMap::from([(Amber, 0), (Bronze, 0), (Copper, 0), (Desert, 0)]);
            for ap in amphipods {
                let id = ap.id as usize;
                let c = c_count.get_mut(&ap.ap_type).unwrap();
                *c += 1;
                me.ap_chars[id] = match ap.ap_type {
                    Amber =>  match *c { 1 => 'A', 2 => 'a', 3 => 'Δ', _ => 'α',},
                    Bronze => match *c { 1 => 'B', 2 => 'b', 3 => 'ß', _ => 'β',},
                    Copper => match *c { 1 => 'C', 2 => 'c', 3 => 'Č', _ => 'ͻ',},
                    Desert => match *c { 1 => 'D', 2 => 'd', 3 => 'Д', _ => 'δ',},
                };
            }
        }

        me
    }

    fn is_room_empty(&self, room_col: PosType) -> bool {
        for i in 1..self.matrix.len() {
            if self.matrix[i][room_col].is_some() {
                return false
            }
        }

        true
    }

    fn get_dest_col(&self, ap: &Amphipod) -> usize {
        match ap.ap_type {
            Amber  => 2,
            Bronze => 4,
            Copper => 6,
            Desert => 8,
        }
    }

    fn is_in_dest_pos(&self, aps: &Amphipods, ind: usize) -> bool {
        if let Some((r, c)) = aps[ind].current_room_pos {
            if self.get_dest_col(&aps[ind]) != c {
                return false
            }

            // Check if any ap below "me" is in wrong room
            for i in r + 1..self.matrix.len() {
                if self.get_dest_col(&aps[self.matrix[i][c].unwrap() as usize]) != c {
                    return false
                }
            }

            true
        } else {
            false
        }
    }

    fn move_to_final(&mut self, aps: &mut Amphipods, ind: usize) -> bool {
        let ap = aps.get(ind).unwrap();
        let tc = ap.destination_room_col;
        let tr = if self.is_room_empty(tc) {
            self.matrix.len() - 1
        } else {
            let mut r = 0;
            for i in 1..self.matrix.len() {
                if let Some(id) = self.matrix[i][tc] {
                    if !self.is_in_dest_pos(aps, id as usize) {
                        // At least one ap present in the room is not in its final destination
                        return false
                    }
                } else {
                    r = i;
                }
            }

            r
        };

        // Check if possible to move (i.e. is the hallway free)
        let cp = self.amphipods_pos[ap.id as usize];
        if !self.can_move_to(cp, (tr, tc)) {
            return false
        }

        let apm = aps.get_mut(ind).unwrap();
        self.do_move(apm, tr, tc);
        self.n_in_dest += 1;

        true
    }

    fn do_move(&mut self, ap: &mut Amphipod, nrow: PosType, ncol: PosType) {
        let (or, oc) = self.amphipods_pos[ap.id as usize];

        self.amphipods_pos[ap.id as usize] = (nrow, ncol);
        self.matrix[or][oc] = None;
        self.matrix[nrow][ncol] = Some(ap.id);

        ap.set_room(nrow, ncol);

        // Calculate n moves from current row to hallway + n moves in hallway + n moves to destination row.
        // Multiply that with the ap's cost
        let subtr = |a: usize, b: usize| {
            (a as i32 - b as i32).unsigned_abs()
        };
        self.curr_cost += (subtr(or, 0) + subtr(oc, ncol) + subtr(0, nrow)) * ap.cost;
    }

    fn can_move_to(&self, (sr, sc): Position, (er, ec): Position) -> bool {
        use either::*;

        for r in 1..sr {
            if self.matrix[r][sc].is_some() {
                // Cannot move out of room, places above is occupied
                return false
            }
        }

        // Check hallway
        let rng = if ec > sc { Either::Left(sc + 1..=ec) } else { Either::Right((ec..sc).rev()) };
        for c in rng {
            if self.matrix[0][c].is_some() {
                return false
            }
        }

        // Is target room empty?
        for r in 1..=er {
            if self.matrix[r][ec].is_some() {
                return false
            }
        }

        true
    }

    fn get_full_cost(&self) -> Option<CostType> {
        if self.n_in_dest == self.amphipods_pos.len() as u8 {
            Some(self.curr_cost)
        } else {
            None
        }
    }

    fn is_cost_too_high(&self, min_cost: &mut CostType) -> bool {
        self.curr_cost > *min_cost
    }
}

fn parse_input(lines: Vec<String>) -> (Burrow, Amphipods) {
    let mut amphipods = Vec::new();

    for (i, lines_it) in lines.iter().enumerate().take(lines.len() - 1).skip(2) {
        let chrs = lines_it.chars().skip(3).step_by(2);
        let mut burrow_col = 2;
        for c in chrs.take(4) {
            let id = amphipods.len() as Id;
            amphipods.push(Amphipod::new(id, c, (i - 1, burrow_col)));

            burrow_col += 2;
        }
    }
    let burrow = Burrow::new(&amphipods);

    (burrow, amphipods)
}

#[cfg(debug_assertions)]
use std::fmt;
#[cfg(debug_assertions)]
impl fmt::Display for Burrow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let to_c = |e: Option<Id>| {
            if let Some(ev) = e { self.ap_chars[ev as usize] } else { '.' }
        };

        let mut s = String::new();
        for e in self.matrix[0] {
            s.push(to_c(e));
        }
        s.push('\n');
        for r in 1..self.matrix.len() {
            for c in 0..self.matrix[r].len() {
                if [2, 4, 6, 8].contains(&c) {
                    s.push(to_c(self.matrix[r][c]));
                } else {
                    s.push(' ');
                }
            }
            s.push('\n');
        }
        write!(f, "{}", s.trim_end() )
    }
}

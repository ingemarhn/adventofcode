pub fn run_task(task: &str, sample: Option<&String>) {
    common::run_task(file!(), sample, task, task_a, task_b);
}

fn task_a(lines: Vec<String>) {
    let maze = parse_input(&lines);
    let count = move_until_stop(&maze);
    println!("# of moves: {}", count);
}

fn task_b(lines: Vec<String>) {
    let _ = parse_input(&lines);
}

type Maze = Vec<Vec<Dir>>;

#[derive(Clone, Debug, PartialEq)]
enum Dir {
    E, S, X
}
use Dir::*;

fn move_until_stop(maze: &Maze) -> usize {
    let mut work_maze = maze.clone();
    let maze_nr = maze.len();
    let maze_nc = maze[0].len();
    let mut count = 0;

    loop {
        let mut has_moved = false;
        let mut next_maze = work_maze.clone();
        for mr in 0..maze_nr {
            for mc in 0..maze_nc {
                if work_maze[mr][mc] != E {
                    continue
                }
                let nc = if mc < maze_nc - 1 { mc + 1 } else { 0 };
                if work_maze[mr][nc] == X {
                    next_maze[mr][nc] = E;
                    next_maze[mr][mc] = X;
                    has_moved = true;
                }
            }
        }
        work_maze.clone_from(&next_maze);

        for mr in 0..maze_nr {
            for mc in 0..maze_nc {
                if work_maze[mr][mc] != S {
                    continue
                }
                let nr = if mr < maze_nr - 1 { mr + 1 } else { 0 };
                if work_maze[nr][mc] == X {
                    next_maze[nr][mc] = S;
                    next_maze[mr][mc] = X;
                    has_moved = true;
                }
            }
        }

        count += 1;

        if !has_moved {
            break
        }

        work_maze = next_maze;
    }

    count
}

fn parse_input(lines: &[String]) -> Maze {
    lines
        .iter()
        .map(|l|
            l
                .chars()
                .map(|c| match c {
                    '>' => E,
                    'v' => S,
                    _   => X,
                } )
                .collect()
        )
        .collect()
}
